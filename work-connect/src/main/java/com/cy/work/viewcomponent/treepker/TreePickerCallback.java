package com.cy.work.viewcomponent.treepker;

import java.io.Serializable;
import java.util.List;

import com.cy.work.common.vo.WkItem;

public class TreePickerCallback implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8776000555746972431L;

    /**
     * 準備所有的項目 - 回傳 Item 物件需實做相依關係
     * 
     * @return
     */
    public List<WkItem> prepareAllItems() throws Exception {
        throw new UnsupportedOperationException("TreePickerCallback: 未實做 prepareAllItem");
    }

    /**
     * 準備已選擇項目
     * 
     * @return
     */
    public List<String> prepareSelectedItemSids() throws Exception {
        throw new UnsupportedOperationException("TreePickerCallback: 未實做 prepareSelectedItemSids");
    }
    
    /**
     * 準備鎖定項目
     * 
     * @return
     */
    public List<String> prepareDisableItemSids() throws Exception {
        throw new UnsupportedOperationException("TreePickerCallback: 未實做 prepareDisableItemSids");
    }
    
    /**
     * 已選擇項目, 顯示前整理
     * @param targetItems
     * @return
     */
    public List<WkItem> beforTargetShow(List<WkItem> targetItems){
        return targetItems;
    }
}
