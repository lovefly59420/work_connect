package com.cy.work.viewcomponent.helper;

import java.io.Serializable;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.vo.WkItem;

/**
 * @author allen1214_wu
 *
 */
public class OrgUserTreePickerCallBack implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8568416974665693110L;

    /**
     * @param dep 單位資料檔
     * @return WkItem
     */
    public WkItem createOrgItem(Org dep) {
        throw new SystemDevelopException(WkMessage.DEV_NOT_IMPL);
    }

    /**
     * @param targetDep 附掛部門
     * @param user      使用者資料檔
     * @return WkItem
     */
    public WkItem createUserItem(Org targetDep, User user) {
        throw new SystemDevelopException(WkMessage.DEV_NOT_IMPL);
    }

    /**
     * 以關鍵字比對項目名稱
     * 
     * @param wkItem  WkItem
     * @param keyword 關鍵字
     * @return true:符合 , false:不符合
     */
    public boolean compareItemNameByKeyword(WkItem wkItem, String keyword) {
        throw new SystemDevelopException(WkMessage.DEV_NOT_IMPL);
    }
}
