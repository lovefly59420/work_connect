package com.cy.work.viewcomponent.treepker;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkTreeUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.viewcomponent.helper.PickerComponentHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
public class TreePickerComponent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -717664680610562527L;

    // ========================================================================
    // 工具區
    // ========================================================================
    /**
     * 
     */
    private PickerComponentHelper pickerHelper = PickerComponentHelper.getInstance();

    // ========================================================================
    // 變數區
    // ========================================================================
    /**
     * 元件說明
     */
    private String componentDesc;

    /**
     * 僅處理非停用項目 (此項設定為 true 時, 畫面不顯示『不顯示停用』勾選項目)
     */
    @Getter
    @Setter
    private boolean alwaysOnlyAvtive = false;

    /**
     * 選取時總是含以下 (此項設定為 true 時, 畫面不顯示『選取時含以下』勾選項目)
     */
    @Getter
    @Setter
    private boolean alwaysContainFollowing = false;

    /**
     * 
     */
    @Getter
    @Setter
    private TreePickerVO vo;

    /**
     * 所有項目
     */
    @Getter
    private List<WkItem> allItems;

    /**
     * 已選擇項目
     */
    private List<WkItem> selectedItems;

    /**
     * 全部項目的親子關係
     */
    Map<WkItem, List<WkItem>> childItemsMapByParentItem;

    /**
     * 外部實做的 callback 方法
     */
    private TreePickerCallback callback;

    /**
     * 項目排序器
     */
    @Getter
    @Setter
    private Comparator<WkItem> itemComparator = Comparator.comparing(WkItem::getItemSeq);

    // ========================================================================
    // 外部方法
    // ========================================================================

    /**
     * 
     * 
     * @param callback
     */
    /**
     * 建構子, 初始化時需實做 callback 方法
     * 
     * @param callback
     * @param componentDesc 元件說明, log 用
     */
    public TreePickerComponent(TreePickerCallback callback, String componentDesc) {
        super();
        this.callback = callback;
        this.componentDesc = componentDesc;
        this.vo = new TreePickerVO();
    }

    /**
     * 是否顯示停用
     * 
     * @return
     */
    public boolean isHideInactiveItem() {
        return this.vo.isHideInactiveItem();
    }

    /**
     * 設定是否顯示停用
     * 
     * @param showInActiveItem
     */
    public void setShowInActiveItem(boolean showInActiveItem) {
        this.vo.setHideInactiveItem(!showInActiveItem);
    }

    /**
     * @param propagateSelectionUp
     */
    public void setPropagateSelectionUp(boolean propagateSelectionUp) {
        this.vo.setPropagateSelectionUp(propagateSelectionUp);
    }

    /**
     * 選擇時是否含以下
     * 
     * @return
     */
    public boolean isContainFollowing() {
        return this.vo.isContainFollowing();
    }

    /**
     * 設定選擇時是否含以下
     * 
     * @param showInActiveItem
     */
    public void setContainFollowing(boolean containFollowing) {
        this.vo.setContainFollowing(containFollowing);
    }

    /**
     * 取得已選擇的項目
     * 
     * @return
     */
    public List<WkItem> getSelectedItems() {
        this.renewSelectedItem();
        return this.selectedItems;
    }

    /**
     * 取得已選擇的項目 SID
     * 
     * @return
     */
    public Set<String> getSelectedItemSids() {
        this.renewSelectedItem();

        if (WkStringUtils.isEmpty(this.selectedItems)) {
            return Sets.newHashSet();
        }

        return this.selectedItems.stream().map(WkItem::getSid).collect(Collectors.toSet());
    }

    /**
     * 取得已選擇的項目 SID (轉 Integer)
     * 
     * @return
     */
    public Set<Integer> getSelectedItemIntegerSids() {

        return this.getSelectedItemSids().stream()
                .filter(sid -> WkStringUtils.isNumber(sid))
                .map(sid -> Integer.parseInt(sid))
                .collect(Collectors.toSet());
    }

    /**
     * 重建資料
     */
    /**
     * @throws Exception
     * 
     */
    public void rebuild() throws Exception {

        // ====================================
        // 取得所有項目
        // ====================================
        // 呼叫實做方法取得所有項目
        this.allItems = this.callback.prepareAllItems();
        if (this.allItems == null) {
            this.allItems = Lists.newArrayList();
        }

        this.allItems = this.allItems.stream()
                .sorted(Comparator.comparing(WkItem::getItemSeq))
                .collect(Collectors.toList());

        // 以 SID 建立 index
        Map<String, WkItem> allItemMapBySid = this.allItems.stream()
                .collect(Collectors.toMap(WkItem::getSid, item -> item));

        // 檢查傳入資料
        this.checkItemParentChildRelation();

        // 建立所有項目的親子關係
        this.childItemsMapByParentItem = this.pickerHelper.prepareAllItemRelation(this.allItems);

        // ====================================
        // 取得已選擇項目
        // ====================================
        // 呼叫實做方法
        List<String> selectedItemSids = this.callback.prepareSelectedItemSids();

        // 檢查傳入資料
        this.checkNotInAlltem(selectedItemSids, allItemMapBySid);

        // 轉 WkItem
        this.selectedItems = Lists.newArrayList();
        if (WkStringUtils.notEmpty(selectedItemSids)) {
            this.selectedItems = Lists.newArrayList();
            for (String itemSid : selectedItemSids) {
                if (allItemMapBySid.containsKey(itemSid)) {
                    WkItem selectedItem = allItemMapBySid.get(itemSid);
                    this.selectedItems.add(selectedItem);
                    //如果已選擇項目有停用的，則預設顯示停用單位
                    if(!selectedItem.isActive()) {
                        this.vo.setHideInactiveItem(false);
                    }
                }
            }
        }

        // 依據選擇可項目, 給排序序號
        for (WkItem item : this.selectedItems) {
            if (allItemMapBySid.containsKey(item.getSid())) {
                WkItem srcItem = allItemMapBySid.get(item.getSid());
                if (srcItem != null) {
                    item.setItemSeq(srcItem.getItemSeq());
                }
            }
        }

        // 排序
        this.selectedItems = selectedItems.stream()
                .sorted(this.itemComparator)
                .collect(Collectors.toList());

        // 已選擇清單特殊規則排序
        this.selectedItems = this.callback.beforTargetShow(selectedItems);

        // ====================================
        // 標記 disable
        // ====================================
        List<String> disableItemSids = this.callback.prepareDisableItemSids();
        if (WkStringUtils.notEmpty(disableItemSids)) {

            for (WkItem item : this.allItems) {
                if (disableItemSids.contains(item.getSid())) {
                    item.setDisable(true);
                }
            }

            for (WkItem item : this.selectedItems) {
                item.setDisable(disableItemSids.contains(item.getSid()));
            }
        }

        // ====================================
        // 建立樹狀清單資料
        // ====================================
        this.rebuildTreeNode();
    }

    // ========================================================================
    // 內部方法
    // ========================================================================
    /**
     * 
     */
    private void rebuildTreeNode() {

        // ====================================
        // 判斷是否需要產生資料
        // ====================================
        if (WkStringUtils.isEmpty(this.allItems)) {
            return;
        }

        // 有任何一組 item 有設定 parent
        boolean hasAnyChildItem = this.allItems.stream()
                .anyMatch(item -> (item.getParent() != null));

        // 沒有任何一個有設定, 不需要產生樹狀模式資料
        if (!hasAnyChildItem) {
            return;
        }

        // ====================================
        // 初始化
        // ====================================
        this.vo.setRootNode(new DefaultTreeNode());
        this.vo.getTreeNodeMapByItem().clear();

        // ====================================
        // 建立父子關係資料結構
        // ====================================
        final List<WkItem> currAllItems = WkCommonUtils.safeStream(this.allItems).collect(Collectors.toList()); 
        for (WkItem item : currAllItems) {

            if (this.vo.isHideInactiveItem() && !item.isActive()) {
                // 不顯示停用, 且項目為停用時, 跳過
                continue;
            }

            // 建立項目樹節點物件
            DefaultTreeNode node = new DefaultTreeNode(item);

            // 設定 disable
            node.setSelectable(!item.isDisable());

            // 設定已選擇(disable 時, 不可選擇)
            node.setSelected(selectedItems.contains(item));

            // 設定強制展開屬性
            node.setExpanded(item.isForceExpand());

            this.vo.getTreeNodeMapByItem().put(item, node);
        }

        for (WkItem item : this.vo.getTreeNodeMapByItem().keySet()) {

            // 取得項目節點物件
            TreeNode itemTreeNode = this.vo.getTreeNodeMapByItem().get(item);

            // 取得父項目
            WkItem parentItem = item.getParent();

            TreeNode parentTreeNode = this.vo.getTreeNodeMapByItem().get(parentItem);

            if (parentItem == null) {
                // 無父項目, 指向 root
                if (!this.vo.getRootNode().getChildren().contains(itemTreeNode)) {
                    this.vo.getRootNode().getChildren().add(itemTreeNode);
                }
            } else if (parentTreeNode == null) {
                // 找不到父項目節點物件, 指向 root (應該不會發生)
                if (!this.vo.getRootNode().getChildren().contains(itemTreeNode)) {
                    this.vo.getRootNode().getChildren().add(itemTreeNode);
                }
            } else {
                // 將節點指向父節點
                if (!parentTreeNode.getChildren().contains(itemTreeNode)) {
                    parentTreeNode.getChildren().add(itemTreeNode);
                }
            }
        }

        // ====================================
        // 樹清單排序
        // ====================================
        // root
        if (this.vo.getRootNode() != null && WkStringUtils.notEmpty(this.vo.getRootNode().getChildren())) {
            List<TreeNode> sortChildrens = this.vo.getRootNode().getChildren().stream()
                    .sorted(Comparator.comparing(node -> this.getItemSeq(node)))
                    .collect(Collectors.toList());
            ((DefaultTreeNode) this.vo.getRootNode()).setChildren(sortChildrens);
        }
        // root 以下 node
        for (TreeNode treeNode : this.vo.getTreeNodeMapByItem().values()) {

            if (treeNode == null || WkStringUtils.isEmpty(treeNode.getChildren())) {
                continue;
            }

            List<TreeNode> sortChildrens = treeNode.getChildren().stream()
                    .sorted(Comparator.comparing(node -> this.getItemSeq(node)))
                    .collect(Collectors.toList());

            ((DefaultTreeNode) treeNode).setChildren(sortChildrens);
        }

        // ====================================
        // 樹節點屬性處理
        // ====================================
        // 逐筆處理
        for (WkItem targetItem : this.selectedItems) {

            // 已選擇項目, 不在節點列表中時跳過 (可能已選項目為停用, 但可選項目不顯示停用)
            if (!this.vo.getTreeNodeMapByItem().containsKey(targetItem)) {
                continue;
            }

            // 取得項目節點物件
            TreeNode itemTreeNode = this.vo.getTreeNodeMapByItem().get(targetItem);
            // 已勾選
            itemTreeNode.setSelected(true);
            // 展開上層
            this.pickerHelper.changeNodeExpandToTop(itemTreeNode, true);
        }

        // ====================================
        // 子節點全勾選時,自動勾選上層節點
        // ====================================
        // 功能開啟時才處理
        if (this.vo.isPropagateSelectionUp()) {
            this.preparePropagateSelectionUpEffect(this.vo.getRootNode());
        }
    }

    private void preparePropagateSelectionUpEffect(TreeNode node) {

        if (node == null || node.getChildCount() == 0) {
            return;
        }

        boolean ischildAllSelected = true;
        for (TreeNode childNode : node.getChildren()) {
            // 子節點遞迴處理
            this.preparePropagateSelectionUpEffect(childNode);
            // 有任一個子節點未選擇時, 記錄此節點不需自動勾選
            if (!childNode.isSelected()) {
                ischildAllSelected = false;
            }
        }
        // 自動勾選效果 (本來就有勾的, 不自動反勾選)
        if (ischildAllSelected) {
            node.setSelected(true);
        }
    }

    /**
     * 取得排序序號
     * 
     * @param treeNode
     * @return
     */
    private Long getItemSeq(TreeNode treeNode) {
        if (treeNode == null) {
            return Long.MAX_VALUE;
        }
        DefaultTreeNode defaultTreeNode = (DefaultTreeNode) treeNode;
        if (defaultTreeNode.getData() == null) {
            return Long.MAX_VALUE;
        }
        WkItem wkItem = (WkItem) defaultTreeNode.getData();
        if (wkItem.getItemSeq() == null) {
            return Long.MAX_VALUE;
        }
        return wkItem.getItemSeq();
    }

    // ========================================================================
    // 事件
    // ========================================================================
    /**
     * 事件：樹清單-勾選/反勾選
     */
    public void event_treeSelectNodeChange() {
        // 目前無需實做
    }

    /**
     * 事件：全選
     */
    public void tree_changeAllSelected() {
        List<TreeNode> treeNodelist = Lists.newArrayList();
        for (TreeNode treeNode : this.vo.getTreeNodeMapByItem().values()) {
            if (treeNode.getData() == null) {
                continue;
            }
            WkItem targetItem = (WkItem) treeNode.getData();
            // 不為鎖定時才處理
            if (!targetItem.isDisable()) {
                treeNode.setSelected(this.vo.isAllSelected());
                if (this.vo.isAllSelected()) {
                    treeNodelist.add(treeNode);
                }
            }
        }
        this.vo.setSelectedNodes(treeNodelist.stream().toArray(size -> new TreeNode[size]));
    }

    /**
     * 事件：切換顯示停用項目
     */
    public void tree_changeHideInactiveItem() {
        try {
            this.renewSelectedItem();
            this.rebuildTreeNode();
        } catch (Exception e) {
            MessagesUtils.showError("系統錯誤, 請恰資訊人員!");
            log.error("切換顯示停用項目時發生錯誤!", e);
        }
    }

    /**
     * 事件：樹清單 - 關鍵字查詢
     */
    public void tree_searchItemNameByKeyword() {
        // 重新轉換查詢關鍵字, 忽略大小寫差異
        String keyword = WkStringUtils.safeTrim(this.vo.getSerachItemNameKeyword()).toUpperCase();
        // 遞迴展開名稱符合或已選的節點
        this.pickerHelper.tree_searchItemNameByKeyword_recursive(this.vo.getRootNode(), keyword);
    }

    // ========================================================================
    //
    // ========================================================================
    private void renewSelectedItem() {
        this.selectedItems = Lists.newArrayList();
        if (this.vo.getSelectedNodes() != null) {
            for (TreeNode treeNode : this.vo.getSelectedNodes()) {
                this.selectedItems.add((WkItem) treeNode.getData());
            }
        }
    }

    // ========================================================================
    // 文字顯示
    // ========================================================================
    /**
     * 已選擇名稱
     * 
     * @return
     */
    public String prepareSelectedShowName(String emptyMsg) {
        // log.info("文字顯示 prepareSelectedShowName");
        try {
            // ====================================
            // 取得已選擇項目
            // ====================================
            if (WkStringUtils.isEmpty(this.callback.prepareSelectedItemSids())) {
                return emptyMsg;
            }

            List<String> selectedSids = this.callback.prepareSelectedItemSids();

            // ====================================
            // 取得所有項目
            // ====================================
            List<WkItem> allItem = this.callback.prepareAllItems();

            // ====================================
            // 名稱
            // ====================================
            List<String> selectedItemNames = allItem.stream()
                    .filter(item -> selectedSids.contains(item.getSid()))
                    .map(WkItem::getName)
                    .collect(Collectors.toList());

            if (WkStringUtils.isEmpty(selectedItemNames)) {
                log.warn("類別組合設定值, 找不到對應的設定檔! -> 清除類別組合查詢欄位");
                return "未設定!";
            }

            if (selectedItemNames.size() != selectedSids.size()) {
                log.warn("類別組合設定值, 有部分設定找不到對應的設定檔! -> 重設類別組合查詢欄位");
            }

            return String.join("、", selectedItemNames);

        } catch (Exception e) {
            log.error("取得 TreePickerComponent 的選取項目顯示名稱時發生錯誤!", e);
        }

        return "";
    }

    /**
     * 已選擇的類別組合 - tooltip
     * 
     * @return
     */
    public String prepareSelectedShowTooltip(String emptyMsg, Integer maxLine) {
        // log.info("文字顯示 prepareSelectedShowTooltip");
        try {

            // ====================================
            // 取得已選擇項目
            // ====================================
            if (WkStringUtils.isEmpty(this.callback.prepareSelectedItemSids())) {
                return emptyMsg;
            }

            List<String> selectedSids = this.callback.prepareSelectedItemSids();

            // ====================================
            // 查詢
            // ====================================
            List<WkItem> allItem = this.callback.prepareAllItems();

            // ====================================
            // 兜組文字
            // ====================================
            return WkTreeUtils.prepareSelectedItemNameByTreeStyle(
                    allItem, selectedSids, maxLine);

        } catch (Exception e) {
            log.error("取得 TreePickerComponent 的選取項目顯示名稱時發生錯誤!", e);
        }
        return "";
    }

    // ========================================================================
    // 開發階段檢核
    // ========================================================================
    /**
     * 略過
     */
    @Getter
    @Setter
    public boolean skipItemParentChildRelationCheck = false;

    /**
     * 所有項目的親子關係檢查
     * 
     * @param allItemMapBySid
     */
    private void checkItemParentChildRelation() {

        // 無須檢核
        if (skipItemParentChildRelationCheck || WkStringUtils.isEmpty(this.allItems)) {
            return;
        }

        if (this.allItems.stream().allMatch(item -> (item.getParent() == null))) {
            log.warn("\r\nTreePickerComponent(" + this.componentDesc + ")警告："
                    + "實做方法 prepareAllItems 所傳回的所有項目, "
                    + "\r\n"
                    + "都未設定 wkitem.parent (父項), 是否忘記實做?"
                    + "\r\n"
                    + "若確定資料無誤, 可把 skipItemParentChildRelationCheck 設為 true, 即略過此檢核");
        }
    }

    /**
     * 
     */
    private void checkNotInAlltem(
            List<String> selectedItemSids,
            Map<String, WkItem> allItemMapBySid) {

        List<String> errorItemMsgs = Lists.newArrayList();

        // ====================================
        // 檢核項目中的父代項目資料(wkitem.parent),是否在全部項目列表中(all items)
        // 1.寫log
        // 2.自動加入全部列表 (否則無法產出樹狀資料)
        // ====================================
        final List<WkItem> currAllItems = WkCommonUtils.safeStream(this.allItems).collect(Collectors.toList()); 
        for (WkItem item : currAllItems) {
            if (item == null || item.getParent() == null || WkStringUtils.isEmpty(item.getParent().getSid())) {
                continue;
            }
            if (!allItemMapBySid.containsKey(item.getParent().getSid())) {
                WkItem parentItem = item.getParent();
                errorItemMsgs.add(
                        "父代項目:"
                                + parentItem.getName() + "(" + parentItem.getSid() + ")，"
                                + "不在所有項目清單中(allItem), 已自動加入");
                this.allItems.add(parentItem);
            }
        }

        // ====================================
        // 已選擇項目(selectedItemSids),是否在全部項目列表中(allitems)
        // ====================================
        for (String itemSid : selectedItemSids) {
            if (!allItemMapBySid.containsKey(itemSid)) {
                errorItemMsgs.add(
                        "已選擇項目:"
                                + itemSid + "，"
                                + "不在所有項目清單中(allItem), 請確認邏輯是否錯誤!");
            }
        }

        // ====================================
        // 印 log
        // ====================================
        if (errorItemMsgs.size() > 0) {
            log.warn("\r\nTreePickerComponent(" + this.componentDesc + ")警告：\r\n"
                    + String.join("\r\n", errorItemMsgs));
        }
    }

}
