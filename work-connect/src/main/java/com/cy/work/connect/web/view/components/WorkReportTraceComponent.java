/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.web.logic.components.WCTraceLogicComponents;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class WorkReportTraceComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3409852842746826990L;

    @Getter
    private List<WCTraceVO> wcTraceVO;

    private String wrcId;

    private Integer userSid;
    @Getter
    private boolean showTraceTab = false;

    public WorkReportTraceComponent() {
    }

    public void loadData(String wrcId, Integer userSid) {
        this.userSid = userSid;
        this.wrcId = wrcId;
        loadViewData(userSid, wrcId);
    }

    public void loadData() {
        loadViewData(userSid, wrcId);
    }

    private void loadViewData(Integer userSid, String wrcId) {
        wcTraceVO =
            WCTraceLogicComponents.getInstance()
                .getWCTraceVOsByWcSIDAndWCTraceType(userSid, wrcId, null);
        showTraceTab = wcTraceVO != null && wcTraceVO.size() > 0;
    }
}
