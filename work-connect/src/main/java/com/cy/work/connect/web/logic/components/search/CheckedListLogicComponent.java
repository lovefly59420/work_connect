/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WaitAssignLogic;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.CheckedListColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.CheckedListColumnVO;
import com.cy.work.connect.web.view.vo.search.ProcessListVO;
import com.cy.work.connect.web.view.vo.search.query.CheckedListQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 報表【執行單據清單】 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class CheckedListLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4042498255485014484L;

    private static CheckedListLogicComponent instance;
    private final String hasRead = "已閱讀";
    private final String notRead = "未閱讀";
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.CHECKED_LIST;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WkOrgCache wkOrgCache;
    @Autowired
    private WkUserCache wkUserCache;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;

    public static CheckedListLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        CheckedListLogicComponent.instance = this;
    }

    /**
     * 建立《處理清單》(待派工)查詢結果
     *
     * @param results      《處理清單》列表資料
     * @param query        《處理清單》查詢物件
     * @param loginUserSid 登入者Sid
     * @param sid          工作聯絡單Sid
     * @param loginDepSid  登入單位Sid
     */
    private void settingWaitWorkList(
            Map<String, ProcessListVO> results,
            CheckedListQuery query,
            Integer loginUserSid,
            String wcSid) {
        // ====================================
        // 沒有勾選待派工時，不用查詢
        // ====================================
        if (!query.getSelProcessStatus().contains(WCExceDepStatus.WAITSEND.name())) {
            return;
        }

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(query.getLazyContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(wcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 取得可以分派的執行單位檔 SID
        // ====================================
        // 查詢
        Set<String> canAssignWcExecDepSids = WaitAssignLogic.getInstance().findUserCanAssignWcExecDepSids(loginUserSid);
        // 沒有任何一筆時 return
        if (WkStringUtils.isEmpty(canAssignWcExecDepSids)) {
            log.debug("執行單據清單-登入者無分派單位,故無待資料分派");
            return;
        }
        // 組成 SQL 用的 string
        String canAssignWcExecDepSidsStr = canAssignWcExecDepSids.stream()
                .collect(Collectors.joining("', '", "'", "'"));

        // ====================================
        // 組SQL
        // ====================================
        // 參數
        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT execDep.wc_exec_dep_sid, ");
        sql.append("       wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.menuTag_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       execDep.exec_user_sid, ");
        sql.append("       execDep.wc_exec_dep_setting_sid, ");
        sql.append("       execDep.dep_sid, ");
        sql.append("       execDep.exec_dep_status, ");
        sql.append("       wc.wc_status, ");
        sql.append("       wc.content, ");
        sql.append("       wc.dep_sid AS reqDepSid, ");
        sql.append("       wc.read_record, ");
        sql.append("       wc.reply_not_read ");
        sql.append("FROM   wc_master wc ");
        sql.append("       INNER JOIN wc_exec_dep execDep ");
        sql.append("               ON wc.wc_sid = execDep.wc_sid ");

        // ----------------------------
        // 限制條件：框定 wc_exec_dep
        // ----------------------------
        sql.append("WHERE  execDep.wc_exec_dep_sid IN ( " + canAssignWcExecDepSidsStr + " ) ");

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(wcSid)) {
                sql.append("  AND wc.wc_sid = '" + wcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 查詢條件：單據類別
        // ----------------------------
        if (!isForceSearch
                && WkStringUtils.notEmpty(query.getCategorySid())) {
            List<WCMenuTag> wcMenuTags = wcMenuTagManager.getWCMenuTagByCategorySid(query.getCategorySid(), null);
            if (WkStringUtils.notEmpty(wcMenuTags)) {
                String menuTagSids = wcMenuTags.stream()
                        .map(each -> each.getSid())
                        .collect(Collectors.joining("','", "'", "'"));
                sql.append(" AND wc.menuTag_sid in (" + menuTagSids + ") ");
            }
        }

        // ----------------------------
        // 建立區間
        // ----------------------------
        // 起日
        if (!isForceSearch
                && query.getStartDate() != null) {
            sql.append("AND wc.create_dt >= '")
                    .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }
        // 迄日
        if (!isForceSearch
                && query.getEndDate() != null) {
            sql.append("AND wc.create_dt <= '")
                    .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }

        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }
        
        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();

        List<Map<String, Object>> dbRowDataMaps = Lists.newArrayList();

        try {
            if (parameters.size() > 0) {
                dbRowDataMaps = this.jdbc.queryForList(sql.toString(), parameters);
            } else {
                dbRowDataMaps = this.jdbc.queryForList(sql.toString());
            }

            if (WorkParamManager.getInstance().isShowSearchSql()) {
                log.debug("執行單據清單-待派工"
                        + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                        + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                        + "\r\n");
            }

            log.debug(WkCommonUtils.prepareCostMessage(startTime, "執行單據清單-待派工，共[" + dbRowDataMaps.size() + "]筆"));

        } catch (Exception e) {
            String message = "執行單據清單-待派工！" + e.getMessage();
            log.error(message, e);
            log.error("\r\nSQL:【\r\n"
                    + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
        }

        if (WkStringUtils.isEmpty(dbRowDataMaps)) {
            return;
        }

        // ====================================
        // 封裝
        // ====================================
        for (Map<String, Object> dbRowDataMap : dbRowDataMaps) {
            this.settingObject(results, dbRowDataMap, loginUserSid);
        }

    }

    /**
     * 建立《處理清單》(執行中,確認完成,符合需求)查詢結果
     *
     * @param results      《處理清單》列表資料
     * @param query        《處理清單》查詢物件
     * @param loginUserSid 登入者Sid
     * @param wcSid        工作聯絡單Sid
     */
    private void settingCheckList(
            Map<String, ProcessListVO> results,
            CheckedListQuery query,
            Integer loginUserSid,
            String wcSid) {

        try {
            // 建立查詢 SQL
            // String sql = this.bulidSqlByCheckedList(query, execCanViewOrgSids, wcSid, loginUserSid);
            String sql = this.prepareCheckedListSQL(query, wcSid, loginUserSid);

            if (WorkParamManager.getInstance().isShowSearchSql()) {
                log.debug("執行單據清單-執行中,確認完成,符合需求"
                        + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                        + "\r\n");
            }

            if (WkStringUtils.isEmpty(sql)) {
                return;
            }

            // 查詢
            long startTime = System.currentTimeMillis();
            List<Map<String, Object>> dbRowDataMapList = jdbc.queryForList(sql);
            log.debug(WkCommonUtils.prepareCostMessage(startTime, "執行單據清單-執行中,確認完成,符合需求，共[" + dbRowDataMapList.size() + "]筆"));

            if (WkStringUtils.isEmpty(dbRowDataMapList)) {
                return;
            }

            // 封裝
            for (Map<String, Object> dbRowDataMap : dbRowDataMapList) {
                this.settingObject(results, dbRowDataMap, loginUserSid);
            }
        } catch (Exception e) {
            log.warn("取得處理清單查詢物件List ERROR!!", e);
        }
    }

    /**
     * 組合《處理清單》DB查詢物件為列表資料
     *
     * @param results      《處理清單》列表資料
     * @param each         查詢DB物件
     * @param loginUserSid 登入者Sid
     */
    private void settingObject(
            Map<String, ProcessListVO> results, Map<String, Object> each, Integer loginUserSid) {
        try {
            String wcSid = (String) each.get("wc_sid");
            ProcessListVO obj = null;
            if (!results.containsKey(wcSid)) {
                obj = new ProcessListVO(wcSid);
                obj.setWcSid((String) each.get("wc_sid"));
                obj.setWcNo((String) each.get("wc_no"));
                obj.setWcStatus(WCStatus.valueOf((String) each.get("wc_status")).getVal());
                obj.setCreateDate(new DateTime(each.get("create_dt")).toString("yyyy/MM/dd"));
                obj.setCreateDt((Date) each.get("create_dt"));
                MenuTagVO menuTag = menuTagLogicComponent.findToBySid(each.get("menuTag_sid") + "");
                obj.setReqDepName(WkOrgUtils.findNameBySid((Integer) each.get("reqDepSid")));
                obj.setMenuTagName(menuTag.getName());
                obj.setCategoryName(
                        categoryTagLogicComponent
                                .getCategoryTagEditVOBySid(menuTag.getCategorySid())
                                .getCategoryName());
                obj.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
                obj.setContent(WkStringUtils.removeCtrlChr((String) each.get("content")));
                obj.setHasReadRecord(notRead);
                if (each.get("read_record") != null) {
                    try {
                        String readRecord = (String) each.get("read_record");
                        List<RRcordTo> resultLists = WkJsonUtils.getInstance().fromJsonToList(readRecord, RRcordTo.class);
                        if (resultLists.stream().anyMatch(e -> loginUserSid.equals(Integer.valueOf(e.getReader())))) {
                            obj.setHasReadRecord(hasRead);
                        }
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }
                if (each.get("reply_not_read") != null) {
                    try {
                        String replyNotReadStr = (String) each.get("reply_not_read");
                        if (!Strings.isNullOrEmpty(replyNotReadStr)) {
                            List<ReplyRecordTo> resultList = WkJsonUtils.getInstance()
                                    .fromJsonToList(replyNotReadStr, ReplyRecordTo.class);
                            if (resultList != null) {
                                List<ReplyRecordTo> s = resultList.stream()
                                        .filter(
                                                eu -> String.valueOf(loginUserSid).equals(eu.getUser()))
                                        .collect(Collectors.toList());
                                if (s != null && !s.isEmpty()) {
                                    obj.setHasUnReadCount(true);
                                    obj.setUnReadCount(s.get(0).getUnReadCount());
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }
            } else {
                obj = results.get(wcSid);
            }
            String wc_exec_dep_setting_sid = (String) each.get("wc_exec_dep_setting_sid");
            if (!Strings.isNullOrEmpty(wc_exec_dep_setting_sid)) {
                WCExecDepSetting wcExecDepSetting = execDepSettingManager.findBySidFromCache(wc_exec_dep_setting_sid);
                if (wcExecDepSetting != null) {
                    WCTag wcTag = wcTagManager.getWCTagBySid(wcExecDepSetting.getWc_tag_sid());
                    obj.bulidExecTag(wcTag);
                }
            }
            if (each.get("exec_user_sid") != null) {
                obj.bulidExecUser(wkUserCache.findBySid((Integer) each.get("exec_user_sid")));
            }
            Integer depSid = (Integer) each.get("dep_sid");
            String execDepStatusStr = (String) each.get("exec_dep_status");
            WCExceDepStatus execDepStatus = WCExceDepStatus.valueOf(execDepStatusStr);
            Org execDep = wkOrgCache.findBySid(depSid);
            obj.buildExecDep(execDep, execDepStatus);

            results.put(wcSid, obj);
        } catch (Exception e) {
            log.warn("settingObject ERROR", e);
        }
    }

    /**
     * 取得 處理清單 查詢物件List
     *
     * @param query        《處理清單》查詢物件
     * @param loginUserSid 登入者Sid
     * @param wcSid        工作聯絡單Sid
     * @return
     */
    public List<ProcessListVO> search(CheckedListQuery query, String wcSid) {

        // 登入者 SID
        Integer loginUserSid = SecurityFacade.getUserSid();

        // ====================================
        // 查詢
        // ====================================
        Map<String, ProcessListVO> results = Maps.newHashMap();

        // 待派工
        this.settingWaitWorkList(results, query, loginUserSid, wcSid);

        // 待領單 (領單前不能看內容 by brain)
        // WORKCOMMU-534 【執行單據清單】移除『待領單』相關功能
        // this.settingWaitReceviceList(results, query, loginUserSid, sid);
        // 執行中,確認完成,符合需求
        this.settingCheckList(results, query, loginUserSid, wcSid);

        // ====================================
        // 同一張單據不能出現兩次
        // ====================================
        List<ProcessListVO> res = results.entrySet().stream().map(each -> each.getValue()).collect(Collectors.toList());

        if (res == null || res.isEmpty()) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾閱讀狀態
        // ====================================
        if (query.getHasReadRecord() != null) {
            res = res.stream()
                    .filter(each -> this.checkHasReadRecord(each, query.getHasReadRecord()))
                    .collect(Collectors.toList());
        }

        // ====================================
        // 排序
        // ====================================
        res = res.stream()
                .sorted(Comparator.comparing(ProcessListVO::getCreateDt))
                .filter(each -> this.checkHasReadRecord(each, query.getHasReadRecord()))
                .collect(Collectors.toList());

        return res;
    }

    /**
     * 判斷是否閱讀
     *
     * @param each          《處理清單》資料
     * @param hasReadRecord 是否閱讀
     * @return
     */
    private boolean checkHasReadRecord(ProcessListVO each, Boolean hasReadRecord) {
        if (hasReadRecord == null) {
            return true;
        }
        if (hasReadRecord) {
            return each.getHasReadRecord().equals(hasRead);
        } else {
            return each.getHasReadRecord().equals(notRead);
        }
    }

    private String prepareCheckedListSQL(
            CheckedListQuery query,
            String wcSid,
            Integer loginUserSid) {

        // ====================================
        // 執行狀態
        // ====================================
        // 本方法可查詢條件
        final List<WCExceDepStatus> limitStatusCondition = Lists.newArrayList(
                WCExceDepStatus.PROCEDD, // 執行中
                WCExceDepStatus.FINISH, // 確認完成
                WCExceDepStatus.CLOSE); // 符合需求

        // 計算已選取的
        Set<WCExceDepStatus> selProcessStatus = Sets.newHashSet();

        if (WkStringUtils.isEmpty(query.getSelProcessStatus())) {
            // 為空時，當成全選
            selProcessStatus = limitStatusCondition.stream().collect(Collectors.toSet());
        } else {
            for (String selProcessStatusName : query.getSelProcessStatus()) {
                WCExceDepStatus status = WCExceDepStatus.safeTransType(selProcessStatusName);
                // 選擇的狀態需在允許清單內
                if (status != null && limitStatusCondition.contains(status)) {
                    selProcessStatus.add(status);
                }
            }
        }

        // ====================================
        // 取得登入者可閱部門 (執行方可閱權限)
        // ====================================
        return SearchSqlHelper.getInstance().prepareQuerySqlForExecDep(
                loginUserSid,
                selProcessStatus,
                query.getCategorySid(),
                query.getStartDate(),
                query.getEndDate(),
                wcSid,
                query.getLazyContent());
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid 使用者Sid
     * @return
     */
    public CheckedListColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        CheckedListColumnVO vo = new CheckedListColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(CheckedListColumn.INDEX));
            vo.setCreateDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            CheckedListColumn.CREATE_DATE));
            vo.setCategoryName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            CheckedListColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            CheckedListColumn.MENUTAG_NAME));
            vo.setExecUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            CheckedListColumn.EXEC_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(CheckedListColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(CheckedListColumn.WC_NO));
            vo.setExecStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            CheckedListColumn.EXEC_DEP_NAME));
            vo.setWcStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(CheckedListColumn.WC_STATUS));
            vo.setReqDep(
                    customColumnLogicComponent.createDefaultColumnDetail(CheckedListColumn.REQ_DEP));
            vo.setHasReadRecord(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            CheckedListColumn.HAS_READRECORD));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setExecUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.EXEC_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.WC_NO, columDB.getInfo()));
            vo.setExecStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.EXEC_DEP_NAME, columDB.getInfo()));
            vo.setWcStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.WC_STATUS, columDB.getInfo()));
            vo.setReqDep(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.REQ_DEP, columDB.getInfo()));
            vo.setHasReadRecord(
                    customColumnLogicComponent.transToColumnDetailVO(
                            CheckedListColumn.HAS_READRECORD, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     *
     * @param loginCompSid 登入公司Sid
     * @return
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }
}
