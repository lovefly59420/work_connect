/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.utils.ReqularPattenUtils;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.enums.MemoTransType;
import com.cy.work.connect.vo.enums.WCReadStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.MemoColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.MemoColumnVO;
import com.cy.work.connect.web.view.vo.search.MemoVO;
import com.cy.work.connect.web.view.vo.search.query.MemoQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 備忘錄查詢 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class MemoSearchLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9188287608093730193L;

    private static MemoSearchLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.MEMO_SEARCH;

    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WCMemoCategoryLogicComponent wcMemoCategoryLogicComponent;

    public static MemoSearchLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        MemoSearchLogicComponent.instance = this;
    }

    /**
     * 取得 備忘錄查詢 建立部門List
     * 
     * @return
     * @throws UserMessageException
     */
    public List<Integer> searchCreateDepSid() throws UserMessageException {

        // ====================================
        // 查詢類別SID
        // ====================================
        String wcMemoCategorySid = this.wcMemoCategoryLogicComponent.getMemoCategorySidByLoginUser();

        // ====================================
        // 查詢群組SID
        // ====================================
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT m.dep_sid ");
        sql.append("  FROM wc_memo_master m ");
        sql.append(" WHERE m.wc_memo_category_sid = '" + wcMemoCategorySid + "' ");

        return this.jdbc.queryForList(sql.toString()).stream()
                .map(each -> (Integer) each.get("dep_sid"))
                .collect(Collectors.toList());
    }

    /**
     * 取得 備忘錄查詢 查詢物件List
     *
     * @param query 查詢條件
     * @param sid   備忘錄Sid
     * @return
     * @throws UserMessageException 
     */
    public List<MemoVO> search(
            MemoQuery query, String sid) throws UserMessageException {

        // ====================================
        // 查詢類別SID
        // ====================================
        String wcMemoCategorySid = this.wcMemoCategoryLogicComponent.getMemoCategorySidByLoginUser();

        // ====================================
        // 查詢備忘錄
        // ====================================
        List<Map<String, Object>> memos = this.jdbc.queryForList(this.bulidSqlByMemo(query, wcMemoCategorySid, sid));

        try {

            List<MemoVO> lists = memos.stream()
                    .map(
                            each -> {
                                MemoVO obj = new MemoVO((String) each.get("wc_memo_sid"));
                                obj.setMemoNo((String) each.get("wc_memo_no"));
                                obj.setCreateDate(
                                        new DateTime(each.get("create_dt")).toString("yyyy/MM/dd"));
                                obj.setCreateDepSid((Integer) each.get("dep_sid"));
                                obj.setCreateDep(
                                        WkOrgCache.getInstance()
                                                .findNameBySid((Integer) each.get("dep_sid")));
                                obj.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
                                obj.setTransWcStatus((String) each.get("trans_wc_sid"));
                                if (each.get("read_record") != null) {
                                    try {
                                        String readRecordStr = (String) each.get("read_record");
                                        List<RRcordTo> resultLists = WkJsonUtils.getInstance().fromJsonToList(readRecordStr, RRcordTo.class);
                                        List<RRcordTo> loginUserRRcordTos = resultLists.stream()
                                                .filter(rItem -> SecurityFacade.getUserSid().equals(Integer.valueOf(rItem.getReader())))
                                                .collect(Collectors.toList());
                                        if (loginUserRRcordTos != null
                                                && !loginUserRRcordTos.isEmpty()) {
                                            RRcordTo loginUserReadReceiptsMember = loginUserRRcordTos.get(0);
                                            WCReadStatus readStatus = WCReadStatus.valueOf(loginUserReadReceiptsMember.getRead());
                                            obj.setReadStatus(readStatus.getVal());
                                            obj.setReadStatusCss(this.transToColorCss(readStatus));
                                            obj.setReadStatusBlodCss(this.transToBlodCss(readStatus));
                                        } else {
                                            obj.setReadStatus(WCReadStatus.UNREAD.getVal());
                                            obj.setReadStatusCss(this.transToColorCss(WCReadStatus.UNREAD));
                                            obj.setReadStatusBlodCss(this.transToBlodCss(WCReadStatus.UNREAD));
                                        }
                                    } catch (Exception e) {
                                        log.warn("settingObject ERROR", e);
                                    }
                                } else {
                                    obj.setReadStatus(WCReadStatus.UNREAD.getVal());
                                    obj.setReadStatusCss(this.transToColorCss(WCReadStatus.UNREAD));
                                    obj.setReadStatusBlodCss(this.transToBlodCss(WCReadStatus.UNREAD));
                                }
                                return obj;
                            })
                    .collect(Collectors.toList());

            List<MemoVO> filterList = Lists.newArrayList();
            lists.forEach(
                    item -> {
                        if (!Strings.isNullOrEmpty(query.getSelTransType())) {
                            if (query.getSelTransType().equals(MemoTransType.UNTRANSED.name())) {
                                if (!Strings.isNullOrEmpty(item.getTransWcStatus())) {
                                    return;
                                }
                            } else if (query.getSelTransType().equals(MemoTransType.TRANSED.name())) {
                                if (Strings.isNullOrEmpty(item.getTransWcStatus())) {
                                    return;
                                }
                            }
                        }
                        if (!Strings.isNullOrEmpty(query.getSelReadStatus())) {
                            if (query.getSelReadStatus().equals(WCReadStatus.UNREAD.name())) {
                                if (!WCReadStatus.UNREAD.getVal().equals(item.getReadStatus())) {
                                    return;
                                }
                            } else if (query.getSelReadStatus().equals(WCReadStatus.HASREAD.name())) {
                                if (!WCReadStatus.HASREAD.getVal().equals(item.getReadStatus())) {
                                    return;
                                }
                            } else if (query.getSelReadStatus().equals(WCReadStatus.WAIT_READ.name())) {
                                if (!WCReadStatus.WAIT_READ.getVal().equals(item.getReadStatus())) {
                                    return;
                                }
                            } else if (query.getSelReadStatus().equals(WCReadStatus.NEEDREAD.name())) {
                                if (!WCReadStatus.WAIT_READ.getVal().equals(item.getReadStatus())
                                        && !WCReadStatus.UNREAD.getVal().equals(item.getReadStatus())) {
                                    return;
                                }
                            }
                        }

                        filterList.add(item);
                    });

            return filterList;
        } catch (Exception e) {
            log.warn("取得備忘錄查詢物件List ERROR!!", e);
            return Lists.newArrayList();
        }
    }

    /**
     * 建立主要 Sql
     *
     * @param query   查詢條件
     * @param results 備忘錄類別查詢結果
     * @param sid     備忘錄Sid
     * @return
     */
    private String bulidSqlByMemo(MemoQuery query, String wcMemoCategorySid, String sid) {

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append("       m.wc_memo_sid ,m.wc_memo_no ,m.create_dt ,m.dep_sid ,m.theme,if(ifnull(json_length(m.trans_wc_sid),0)>0,'Y','') trans_wc_sid,m.read_record ");
        sql.append("  FROM wc_memo_master m ");
        sql.append(" WHERE m.wc_memo_category_sid = '" + wcMemoCategorySid + "' ");

        if (!Strings.isNullOrEmpty(sid)) {
            sql.append(" AND m.wc_memo_sid = '" + sid + "' ");
        }
        if (!Strings.isNullOrEmpty(query.getSelCreateDapSid())) {
            sql.append(" AND m.dep_sid = '" + query.getSelCreateDapSid() + "' ");
        }

        if (!Strings.isNullOrEmpty(query.getTheme())) {
            sql.append("    AND m.theme LIKE '%")
                    .append(reqularUtils.replaceIllegalSqlLikeStr(query.getTheme()))
                    .append("%' ");
        }
        if (!Strings.isNullOrEmpty(query.getContent())) {
            sql.append("    AND m.content LIKE '%")
                    .append(reqularUtils.replaceIllegalSqlLikeStr(query.getContent()))
                    .append("%' ");
        }
        if (query.getStartDate() != null && query.getEndDate() != null) {
            sql.append("    AND m.create_dt BETWEEN '")
                    .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00' AND '")
                    .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                    .append(" 23:59:59' ");
        } else if (query.getStartDate() != null) {
            sql.append("    AND m.create_dt >= '")
                    .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        } else if (query.getEndDate() != null) {
            sql.append("    AND m.create_dt <= '")
                    .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }
        sql.append(" ORDER BY m.create_dt ASC ");
        return sql.toString();
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public MemoColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        MemoColumnVO vo = new MemoColumnVO();
        if (columDB == null) {
            vo.setIndex(customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.INDEX));
            vo.setCreateDate(
                    customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.CREATE_DATE));
            vo.setCreateDep(
                    customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.CREATE_DEP));
            vo.setTheme(customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.THEME));
            vo.setMemoNo(customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.MEMO_NO));
            vo.setTransWcStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.TRANSWC_STATUS));
            vo.setReadStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(MemoColumn.READ_STATUS));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(MemoColumn.INDEX,
                            columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            MemoColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCreateDep(
                    customColumnLogicComponent.transToColumnDetailVO(
                            MemoColumn.CREATE_DEP, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(MemoColumn.THEME,
                            columDB.getInfo()));
            vo.setMemoNo(
                    customColumnLogicComponent.transToColumnDetailVO(MemoColumn.MEMO_NO,
                            columDB.getInfo()));
            vo.setTransWcStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            MemoColumn.TRANSWC_STATUS, columDB.getInfo()));
            vo.setReadStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            MemoColumn.READ_STATUS, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 根據已未讀狀態判斷顯示顯色(待閱讀顯示紅色其他顯示黑色)
     *
     * @param wcReadStatus 已未讀狀態
     * @return
     */
    public String transToColorCss(WCReadStatus wcReadStatus) {
        if (WCReadStatus.WAIT_READ.equals(wcReadStatus)) {
            return "color: red;";
        }
        return "";
    }

    /**
     * 根據已未讀狀態判斷顯示字體(待閱讀及未閱讀顯示粗體)
     *
     * @param wcReadStatus 已未讀狀態
     * @return
     */
    public String transToBlodCss(WCReadStatus wcReadStatus) {
        if (WCReadStatus.UNREAD.equals(wcReadStatus) || WCReadStatus.WAIT_READ.equals(
                wcReadStatus)) {
            return "font-weight:bold;";
        }
        return "";
    }
}
