/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.commons.enums.OrgLevel;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.common.attachment.AttachmentCondition;
import com.cy.work.connect.web.common.attachment.WCMemoAttachmentCompant;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.memo.MemoDataComponent;
import com.cy.work.connect.web.view.components.memo.MemoHeaderComponent;
import com.cy.work.connect.web.view.components.memo.MemoLogicComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 備忘錄 - 新增
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Worp3Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2969426027013921534L;

    @Autowired
    private MemoLogicComponent memoLogicComponent;
    @Autowired
    private WCMemoCategoryLogicComponent wcMemoCategoryLogicComponent;
    @Autowired
    private DisplayController displayController;

    @Getter
    /** 備忘錄 表頭 */
    private MemoHeaderComponent memoHeaderComponent;

    @Getter
    /** 備忘錄 表頭 */
    private MemoDataComponent memoDataComponent;

    @Getter
    /** 備忘錄 表頭 */
    private WCMemoAttachmentCompant memoAttachemtComponent;
    
    

    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;

    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;

    @Getter
    /** 錯誤訊息 */
    private String errorMessage;

    @PostConstruct
    public void init() {
        try {
            this.initLogin();
            if (!wcMemoCategoryLogicComponent.isMemoViewPermission()) {
                this.redirectByNoPermission();
            }
            this.initComponents();
            this.initData();
        } catch (Exception e) {
            showMessage(e.getMessage());
        }
    }

    /**
     * 初始化登入資訊
     */
    private void initLogin() {
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
    }

    /**
     * 初始化元件
     */
    private void initComponents() {
        this.memoHeaderComponent = new MemoHeaderComponent();
        this.memoDataComponent = new MemoDataComponent();
        this.memoAttachemtComponent = new WCMemoAttachmentCompant(userViewVO.getSid(), depViewVo.getSid());
    }

    /**
     * 初始化預設資料
     */
    private void initData() {
        memoHeaderComponent.init(
                WkOrgUtils.prepareBreadcrumbsByDepName(
                        SecurityFacade.getPrimaryOrgSid(), OrgLevel.DIVISION_LEVEL, true, "-"),
                userViewVO.getName(),
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date()));
        AttachmentCondition ac = new AttachmentCondition("", "", false);
        memoAttachemtComponent.loadAttachment(ac);
    }

    /**
     * 進行備忘錄新增
     */
    public void save() {
        try {
            memoAttachemtComponent
                    .getOneTimeUploadAttachmentVOs()
                    .forEach(item -> {
                        memoAttachemtComponent
                                .getSelectedAttachmentVOs()
                                .forEach(selItem -> {
                                    if (item.getAttSid().equals(selItem.getAttSid())) {
                                        selItem.setAttDesc(item.getAttDesc());
                                    }
                                });
                    });
            String memoSid = memoLogicComponent.create(
                    memoDataComponent.getTheme(),
                    memoDataComponent.getContent(),
                    memoDataComponent.getMemo(),
                    memoAttachemtComponent.getSelectedAttachmentVOs());
            String url = "../worp/worp4.xhtml?memoSid=" + memoSid;
            Faces.getExternalContext().redirect(url);
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            displayController.execute("hideLoad();");
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(進行備忘錄新增失敗!!" + e.getMessage() + ")";
            log.error(errorMessage, e);
            displayController.execute("hideLoad();");
        }
    }

    /**
     * 導向無權限畫面
     */
    private void redirectByNoPermission() {
        try {
            Faces.getExternalContext().redirect("../error/illegal_read_memo.xhtml");
            Faces.getContext().responseComplete();
        } catch (Exception ex) {
            log.warn("導向讀取失敗頁面失敗..." + ex.getMessage());
        }
    }

    /**
     * 顯示錯誤訊息及彈出Dialog
     *
     * @param message
     */
    private void showMessage(String message) {
        this.errorMessage = message;
        displayController.update("dlg_errorMessage");
        displayController.showPfWidgetVar("dlg_errorMessage_wv");
    }
}
