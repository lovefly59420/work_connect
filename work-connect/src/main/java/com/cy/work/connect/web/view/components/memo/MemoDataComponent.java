/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components.memo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 備忘錄 內容資料
 *
 * @author kasim
 */
public class MemoDataComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8953961864090881748L;

    @Getter
    @Setter
    /** 主題 */
    private String theme;

    @Getter
    @Setter
    /** 內容 */
    private String content;

    @Getter
    @Setter
    /** 備註 */
    private String memo;

    @Getter
    /** disabled 編輯相關欄位 */
    private Boolean disabledEdit = false;

    public MemoDataComponent() {
    }

    /**
     * 初始化資料
     *
     * @param theme
     * @param content
     * @param memo
     */
    public void init(String theme, String content, String memo) {
        this.disabledEdit = true;
        this.theme = theme;
        this.content = content;
        this.memo = memo;
    }

    /**
     * 開啟編輯欄位
     */
    public void openEditField() {
        this.disabledEdit = false;
    }
}
