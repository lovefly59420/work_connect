package com.cy.work.connect.web.view;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.cy.work.connect.logic.helper.signflow.ReqFlowActionHelper;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.common.attachment.AttachmentCondition;
import com.cy.work.connect.web.common.attachment.WPAttachmentCompant;
import com.cy.work.connect.web.listener.SignInfoMemberCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.listener.WorkReportDataCallBack;
import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.CategoryItemComponent;
import com.cy.work.connect.web.view.components.ManagerSignInfoMemberComponent;
import com.cy.work.connect.web.view.components.WorkReportDataComponent;
import com.cy.work.connect.web.view.components.WorkReportHeaderComponent;
import com.cy.work.connect.web.view.components.memo.MemoLogicComponent;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.to.MemoMasterTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */

@Slf4j
@Scope("view")
@ManagedBean
@Controller
public class Worp1Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6963452725941109809L;

    public final TabCallBack tabCallBack = new TabCallBack() {

        /** */
        private static final long serialVersionUID = -5274131640504989L;

        @Override
        public void reloadBpmTab() {
        }

        @Override
        public void reloadExecBpmTab() {
        }

        @Override
        public void reloadTraceTab() {
        }

        @Override
        public void reloadReplyTab() {
        }

        @Override
        public void reloadFinishReplyTab() {
        }

        @Override
        public void reloadCorrectReplyTab() {
        }

        @Override
        public void reloadDirectiveTab() {
        }

        @Override
        public void reloadAttTab() {
        }

        @Override
        public void reloadRelevanceTab() {
        }

        @Override
        public void reloadTabView(TabType tabType) {
        }

        @Override
        public void reloadWCMasterData() {
        }
    };
    @Autowired
    private WkOrgCache orgManger;
    @Autowired
    private DisplayController displayController;
    @Autowired
    private WCMasterLogicComponents wcMasterLogicComponents;
    @Autowired
    private ReqFlowActionHelper reqFlowActionHelper;

    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;

    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;
    @Getter
    @Setter
    private Boolean edit = true;

    @Getter
    private final String CREATE_CONFIRM_DLG_ID = this.getClass().getSimpleName() + "_" + "CREATE_CONFIRM_DLG_ID";

    @Getter
    @Setter
    /** 表頭資訊 */
    private WorkReportHeaderComponent workReportHeaderComponent;

    @Getter
    /** 類別資訊 */
    private CategoryItemComponent categoryItemComponent;

    @Getter
    @Setter
    /** 內容資訊 */
    private WorkReportDataComponent workReportDataComponent;

    public final WorkReportDataCallBack workReportDataCallBack = new WorkReportDataCallBack() {

        @Override
        public String getWorkReportData() { return workReportDataComponent.getContent(); }

        @Override
        public void updateWorkReportData(String content, String memo) {
            workReportDataComponent.setContent(content);
            workReportDataComponent.setRemark(memo);
        }

        @Override
        public String getContent() { return workReportDataComponent.getContent(); }

        @Override
        public String getMemo() { return workReportDataComponent.getRemark(); }
    };

    @Getter
    /** 附加檔案資訊 */
    private WPAttachmentCompant wpAttachmentCompant;

    @Autowired
    private WCMemoCategoryLogicComponent wcMemoCategoryLogicComponent;

    @Getter
    /** 錯誤訊息 */
    private String errorMessage;

    private String memoSid;
    /**
     * 會簽人員
     */
    private List<UserViewVO> selSignUserViews;
    /**
     * 需求簽核物件
     */
    @Getter
    private ManagerSignInfoMemberComponent managerSignInfoMemberComponent;
    /**
     * Org tree 挑選部門
     */
    private List<Org> selectOrgs;
    /**
     * 會簽元件 CallBack
     */
    @Getter
    public final SignInfoMemberCallBack signInfoMemberCallBack = new SignInfoMemberCallBack() {

        /** */
        private static final long serialVersionUID = 7616349256489258454L;

        /*
         * 開啟視窗
         * @see
         * com.cy.work.connect.web.listener.SignInfoMemberCallBack#openReqSignInfo()
         */
        @Override
        public void openReqSignInfo() {
            try {
                selectOrgs = Lists.newArrayList();
                // 準備待選和已選清單資料
                managerSignInfoMemberComponent.loadUserPicker(selSignUserViews);
                // 更新顯示區域
                DisplayController.getInstance().update("reqCounterSignPanel");
                // 開啟dialog
                DisplayController.getInstance().showPfWidgetVar("reqCounterSignDlg");
            } catch (Exception e) {
                String errormessage = WkMessage.EXECTION + e.getMessage();
                log.warn(errormessage, e);
                MessagesUtils.showError(errormessage);
                return;
            }
        }

        @Override
        public void saveReqSignInfoMember(List<UserViewVO> objs) {
            try {
                selSignUserViews.clear();
                selSignUserViews.addAll(objs);

                // 簽核者字串
                workReportDataComponent.setSignUser(wcMasterLogicComponents.transToSignInfosText(selSignUserViews, Lists.newArrayList()));

                // 會簽資訊 (畫面資訊)
                workReportDataComponent.getSignRoleInfos().clear();
                workReportDataComponent.getSignRoleInfos().addAll(wcMasterLogicComponents.transToSignInfos(selSignUserViews, Lists.newArrayList()));

                displayController.update("workReportDataPanel");
                DisplayController.getInstance().hidePfWidgetVar("reqCounterSignDlg");
            } catch (Exception e) {
                String errormessage = WkMessage.EXECTION + e.getMessage();
                log.warn(errormessage, e);
                MessagesUtils.showError(errormessage);
                return;
            }
        }
    };

    @Getter
    private DepTreeComponent depTreeComponent;

    @PostConstruct
    public void init() {
        try {
            this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
            this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
            this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());

            // 初始化 類別元件及選項
            this.initCategoryItem();

            workReportHeaderComponent = new WorkReportHeaderComponent(
                    WkOrgUtils.prepareBreadcrumbsByDepName(depViewVo.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"),
                    userViewVO.getName());

            // 需求單位加簽 Component
            this.managerSignInfoMemberComponent = new ManagerSignInfoMemberComponent(
                    userViewVO.getSid(), tabCallBack, signInfoMemberCallBack);
            // 初始化加簽名單
            this.selSignUserViews = Lists.newArrayList();

            // 初始化 WorkReportDataComponent
            this.initDataComponent();

            // 初始化 附加檔案元件
            this.initAttachment();

            this.initOrgTree();

            // 網址有帶 memoSid時，開啟備忘錄
            this.loadMemo();

        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + e.getMessage();
            log.error(errorMessage);
            MessagesUtils.showError(errorMessage);
            return;
        }
    }

    private void initOrgTree() {
        depTreeComponent = new DepTreeComponent();
        Org group = new Org(compViewVo.getSid());
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * 載入挑選部門組織樹
     */
    public void loadDepTree() {
        try {
            Org group = new Org(compViewVo.getSid());
            // 修改,故切換顯示停用部門時,需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> allOrg = this.orgManger.findAllDepByCompSid(group.getSid());
            depTreeComponent.init(
                    allOrg,
                    allOrg,
                    group,
                    selectOrgs,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);

            this.displayController.update("dep");
            this.displayController.showPfWidgetVar("deptUnitDlg");
        } catch (Exception e) {
            log.warn("loadDepTree ERROR", e);
        }
    }

    /**
     * 挑選部門組織樹
     */
    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            managerSignInfoMemberComponent.loadSelectDepartment(selectOrgs);
            this.displayController.update("reqCounterSignPanel");
            this.displayController.hidePfWidgetVar("deptUnitDlg");
        } catch (IllegalStateException | IllegalArgumentException e) {
            String errormessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.warn(errormessage, e);
            MessagesUtils.showError(errormessage);
            return;
        } catch (Exception e) {
            String errormessage = WkMessage.EXECTION + e.getMessage();
            log.warn(errormessage, e);
            MessagesUtils.showError(errormessage);
            return;
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() { return depTreeComponent.getMultipleDepTreeManager(); }

    private void loadMemo() {
        // 檢查是否有帶 memoSid 參數
        if (!Faces.getRequestParameterMap().containsKey("memoSid")) {
            return;
        }
        String tempMemoSid = Faces.getRequestParameterMap().get("memoSid");

        if (Strings.isNullOrEmpty(tempMemoSid)) {
            return;
        }

        //
        MemoMasterTo memoMasterTo = MemoLogicComponent.getInstance().findToBySid(tempMemoSid);
        if (memoMasterTo == null) {
            log.warn("無效資料 無法開啟備忘錄!! memoSid：" + tempMemoSid);
            Preconditions.checkState(false, "無效資料 無法開啟備忘錄!!");
        }
        Preconditions.checkState(
                wcMemoCategoryLogicComponent.isMemoViewPermission(memoMasterTo.getWcMemoCategorySid()),
                "無權限取得該備忘錄!!");
        this.memoSid = tempMemoSid;
        wpAttachmentCompant.copyAttachment(memoSid);
        workReportDataComponent.setTitle(memoMasterTo.getTheme());
        workReportDataComponent.setContent(memoMasterTo.getContentCss());
        workReportDataComponent.setRemark(memoMasterTo.getMemoCss());
    }

    /**
     * 初始化 類別元件及選項
     */
    private void initCategoryItem() {
        if (!Faces.getRequestParameterMap().containsKey("categorySid")) {
            Preconditions.checkState(false, "無效參數 無法新增資料!!");
        }
        categoryItemComponent = new CategoryItemComponent();
        categoryItemComponent.setWorkReportDataCallBack(workReportDataCallBack);

        // 取得使用者對應單位，for 聯絡單單據類別可使用權限
        Set<Integer> loginUserMatchDepSid = RelationDepHelper.findMatchOrgSidsForWcForm(SecurityFacade.getUserSid());

        try {
            categoryItemComponent.initItems(
                    Faces.getRequestParameterMap().get("categorySid"),
                    loginUserMatchDepSid,
                    compViewVo.getSid(),
                    SecurityFacade.getUserSid());
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        }
        Preconditions.checkState(categoryItemComponent.checkEffective(), "無效參數 無法新增資料!!");
    }

    /**
     * 初始化 WorkReportDataComponent
     */
    private void initDataComponent() {
        workReportDataComponent = new WorkReportDataComponent();
        workReportDataComponent.setTitle(categoryItemComponent.getTitle());
        workReportDataComponent.setContent(categoryItemComponent.getContent());
        workReportDataComponent.setRemark(categoryItemComponent.getRemark());
    }

    /**
     * 初始化 附加檔案元件
     */
    private void initAttachment() {
        wpAttachmentCompant = new WPAttachmentCompant(userViewVO.getSid(), depViewVo.getSid());
        AttachmentCondition ac = new AttachmentCondition("", "", false);
        wpAttachmentCompant.loadAttachment(ac);
    }

    /**
     * 按鈕事件 - 確定(新增單據)
     */
    public void beforeCreateConfirm() {

        // ====================================
        // 清除控制字元
        // ====================================
        this.workReportDataComponent.setContent(
                WkStringUtils.removeCtrlChr(workReportDataComponent.getContent()));
        this.workReportDataComponent.setTitle(
                WkStringUtils.removeCtrlChr(workReportDataComponent.getTitle()));
        this.workReportDataComponent.setRemark(
                WkStringUtils.removeCtrlChr(workReportDataComponent.getRemark()));

        // ====================================
        // 檢核輸入資料
        // ====================================
        try {
            this.wcMasterLogicComponents.checkCreateMaster(
                            null,
                            workReportDataComponent,
                            categoryItemComponent,
                            wpAttachmentCompant.getSelectedAttachmentVOs());
        } catch (IllegalStateException e) {
            MessagesUtils.showWarn(e.getMessage());
            return;
        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "[" + e.getMessage() + "]";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return;
        }

        // 開啟確認窗
        this.displayController.showPfWidgetVar(this.CREATE_CONFIRM_DLG_ID);;

    }

    public void createConfirm(boolean isSubmit) {

        // ====================================
        // 建立工作聯絡單
        // ====================================
        WCMaster wcMaster = null;
        try {
            wcMaster = this.wcMasterLogicComponents.createMaster(
                    workReportDataComponent,
                    workReportHeaderComponent,
                    wpAttachmentCompant.getSelectedAttachmentVOs(),
                    categoryItemComponent,
                    userViewVO.getSid(),
                    depViewVo.getSid(),
                    compViewVo.getSid(),
                    selSignUserViews,
                    memoSid);

        } catch (UserMessageException e) {
            MessagesUtils.show(e);
            this.displayController.execute("hideLoad();");
            return;
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + e.getMessage();
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            this.displayController.execute("hideLoad();");
            return;
        }

        // ====================================
        // 簽名
        // ====================================
        if (isSubmit) {
            User loginUser = WkUserCache.getInstance().findBySid(SecurityFacade.getUserSid());
            try {
                this.reqFlowActionHelper.doReqflowSign(wcMaster.getSid(), loginUser.getSid());
            } catch (UserMessageException e) {
                MessagesUtils.showError("【處理成功，但簽名失敗!】");
                MessagesUtils.show(e);
                this.displayController.execute("hideLoad();");
                return;
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "\r\n【處理成功，但簽名失敗!】\r\n" + e.getMessage();
                log.error(errorMessage, e);
                MessagesUtils.showError(errorMessage);
                this.displayController.execute("hideLoad();");
                return;
            }
        }

        displayController.execute("showLoad();loadConnect('" + wcMaster.getSid() + "');");

    }

}
