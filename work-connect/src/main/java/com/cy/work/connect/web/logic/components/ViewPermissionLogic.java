/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.PermissionLogicForCanUse;
import com.cy.work.connect.logic.helper.PermissionLogicForExecDep;
import com.cy.work.connect.logic.helper.SignFlowForExecDepLogic;
import com.cy.work.connect.logic.helper.SignFlowForRequireLogic;
import com.cy.work.connect.logic.manager.WCExecDepHistoryManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoDetailManager;
import com.cy.work.connect.logic.manager.WCReadReceiptsManager;
import com.cy.work.connect.logic.manager.WCRollbackHistoryManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.web.config.WCProperties;
import com.cy.work.connect.web.logic.components.search.RollBackListLogicComponent;

import lombok.extern.slf4j.Slf4j;

/**
 * 可閱權限邏輯
 */
@Service
@Slf4j
public class ViewPermissionLogic implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2466611218356345863L;

    private static ViewPermissionLogic instance;

    public static ViewPermissionLogic getInstance() { return instance; }

    @Autowired
    private transient WCReadReceiptsManager wcReadReceiptsManager;
    @Autowired
    private transient WorkParamManager workParamManager;
    @Autowired
    private transient WCExecDepHistoryManager wcExecDepHistoryManager;
    @Autowired
    private transient WCManagerSignInfoDetailManager managerSignInfoDetailManager;
    @Autowired
    private transient WCRollbackHistoryManager wcRollbackHistoryManager;
    @Autowired
    private transient WCProperties wcProperties;

    @Override
    public void afterPropertiesSet() throws Exception {
        ViewPermissionLogic.instance = this;
    }

    public CanViewType checkWorkConnectPermission(
            WCMaster wcMaster) {

        Integer loginUserSid = SecurityFacade.getUserSid();

        String logContent = String.format(
                "[%s], 可檢視權限:",
                wcMaster.getWc_no());

        // ====================================
        // 建單者
        // ====================================
        if (WkCommonUtils.compareByStr(loginUserSid, wcMaster.getCreate_usr())) {
            log.debug(logContent + "單據申請者");
            return CanViewType.ALL;
        }

        // ====================================
        // 建單部門主管
        // ====================================
        // 查詢使用者所有管理的單位, 包含管理單位子部門
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
        // 比對單據建立部門
        if (managerOrgSids.contains(wcMaster.getDep_sid())) {
            log.debug(logContent + " 單據申請單位主管");
            return CanViewType.ALL;
        }

        // ====================================
        // 執行單位相關人員
        // ====================================
        boolean isCanViewByExecDep = PermissionLogicForExecDep.getInstance().isCanViewByExecDep(
                wcMaster.getSid(), loginUserSid);

        if (isCanViewByExecDep) {
            log.debug(logContent + "執行單位相關人員");
            return CanViewType.ALL;
        }

        // ====================================
        // 可閱名單
        // ====================================
        if (this.wcReadReceiptsManager.isReader(wcMaster.getSid(), loginUserSid)) {
            log.debug(logContent + "可閱名單 (wc_read_receipts)");
            return CanViewType.ALL;
        }

        // ====================================
        // 可派工
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideAssignExecutor(wcMaster, loginUserSid)) {
            log.debug(logContent + "可派工人員");
            return CanViewType.ALL;
        }

        // ====================================
        // 簽名歷史記錄
        // ====================================
        boolean isHistorySign = this.managerSignInfoDetailManager.isHistorySigned(loginUserSid, wcMaster.getSid());
        if (isHistorySign) {
            log.debug(logContent + "有流程簽名記錄 (曾經簽過)");
            return CanViewType.ALL;
        }

        // ====================================
        // 需求方簽核流程人員 (已簽或待簽)
        // ====================================
        boolean isReqSignPermission = SignFlowForRequireLogic.getInstance().isRequireFlowCanSignOrSignedUser(
                wcMaster.getSid(), loginUserSid);

        if (isReqSignPermission) {
            log.debug(logContent + "需求方流程簽核人員 (已簽或待簽)");
            return CanViewType.ALL;
        }

        // ====================================
        // 執行方簽核流程人員 (已簽或待簽)
        // ====================================
        boolean isExecDepFlowCanSignOrSignedUser = SignFlowForExecDepLogic.getInstance().isExecDepFlowCanSignOrSignedUser(
                wcMaster.getSid(), loginUserSid);

        if (isExecDepFlowCanSignOrSignedUser) {
            log.debug(logContent + "執行方流程簽核人員 (已簽或待簽)");
            return CanViewType.ALL;
        }

        // ====================================
        // (曾經)為執行人員
        // ====================================
        boolean isExecDepHistory = wcExecDepHistoryManager.isExecDepHistory(wcMaster.getSid(), loginUserSid);
        if (isExecDepHistory) {
            log.debug(logContent + "(曾經)為執行人員");
            return CanViewType.ALL;
        }

        // ====================================
        // 曾經退回
        // ====================================
        if (this.wcRollbackHistoryManager.hasRollbackHistory(loginUserSid, wcMaster.getSid())) {
            log.debug(logContent + "有申請流程退回記錄");
            return CanViewType.ALL;
        }

        if (RollBackListLogicComponent.getInstance().isViewPermissionByRollBack(wcMaster.getSid())) {
            log.debug(logContent + "有執行方單據退回記錄 (個人或部門)");
            return CanViewType.ALL;
        }

        // ====================================
        // BigBoss Default
        // ====================================
        if (this.workParamManager.isBigBoss((loginUserSid))) {
            log.debug(logContent + "登入者為BigBoss,故可檢閱");
            return CanViewType.ALL;
        }

        // ====================================
        // 管理者檢視模式
        // ====================================
        boolean isAdmin = WkUserUtils.isUserHasRole(loginUserSid, SecurityFacade.getCompanyId(), "工作聯絡單系統管理員:*");
        if (isAdmin) {
            log.debug(logContent + "登入者為『工作聯絡單系統管理員』，開啟特殊檢閱模式");
            if (wcProperties.isSuperAdmin(SecurityFacade.getUserId())) {
                log.debug("SuperAdmin");
                return CanViewType.ADMIN_SPECIAL_ALL_VIEW_MODE;
            } else {
                return CanViewType.ADMIN_SPECIAL_MASK_MODE;
            }
        }

        return CanViewType.ACCESS_DENIED;
    }
}
