/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.manager.WorkGroupSetupPrivateManager;
import com.cy.work.connect.vo.WCGroupSetupPrivate;
import com.cy.work.connect.vo.converter.to.GroupInfo;
import com.cy.work.connect.vo.converter.to.GroupInfoTo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkGroupVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 群組邏輯元件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkGroupSetupPrivateLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6259327175176051817L;

    private static WorkGroupSetupPrivateLogicComponents instance;
    @Autowired
    private WorkGroupSetupPrivateManager workGroupSetupPrivateManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;

    public static WorkGroupSetupPrivateLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkGroupSetupPrivateLogicComponents.instance = this;
    }

    /**
     * 刪除群組
     *
     * @param sid 群組Sid
     */
    public void deleteWorkGroupVO(String sid) {
        if (Strings.isNullOrEmpty(sid)) {
            Preconditions.checkState(false, "無傳入刪除群組ID！");
        }
        WCGroupSetupPrivate workGroupSetupPrivate =
            workGroupSetupPrivateManager.getWorkGroupSetupPrivateBySid(sid);
        if (workGroupSetupPrivate == null) {
            Preconditions.checkState(false, "該群組已被刪除！");
        }

        try {
            workGroupSetupPrivateManager.delete(sid);
        } catch (Exception e) {
            log.error("deleteWorkGroupVO", e);
            Preconditions.checkState(false, "刪除群組失敗！");
        }
    }

    /**
     * 儲存群組
     *
     * @param workGroupVO  群組物件
     * @param loginUserSid 登入者Sid
     */
    public void saveWorkGroupVO(WorkGroupVO workGroupVO, Integer loginUserSid) {
        if (workGroupVO == null) {
            Preconditions.checkState(false, "欲存檔物件為空！");
        }

        if (Strings.isNullOrEmpty(workGroupVO.getGroup_name())) {
            Preconditions.checkState(false, "群組名稱為必要輸入欄位！！");
        }
        if (Strings.isNullOrEmpty(workGroupVO.getStatusID())) {
            Preconditions.checkState(false, "狀態為必要輸入欄位！！");
        }

        WCGroupSetupPrivate workGroupSetupPrivate = null;
        if (Strings.isNullOrEmpty(workGroupVO.getGroup_sid())) {
            workGroupSetupPrivate = new WCGroupSetupPrivate();
        } else {
            workGroupSetupPrivate =
                workGroupSetupPrivateManager.getWorkGroupSetupPrivateBySid(
                    workGroupVO.getGroup_sid());
            // 資料取得失敗
            if (workGroupSetupPrivate == null) {
                log.warn("資料取得失敗,無法進行更新,WorkGroupSetupPrivate ID : " + workGroupVO.getGroup_sid());
                Preconditions.checkState(
                    false,
                    "資料取得失敗,無法進行更新,WorkGroupSetupPrivate ID : " + workGroupVO.getGroup_sid());
            }
        }
        workGroupSetupPrivate.setGroup_name(workGroupVO.getGroup_name());
        workGroupSetupPrivate.setNote(workGroupVO.getNote());
        workGroupSetupPrivate.setStatus(Activation.valueOf(workGroupVO.getStatusID()));
        GroupInfo groupInfo = new GroupInfo();

        workGroupVO
            .getUserViewVO()
            .forEach(
                item -> {
                    GroupInfoTo git = new GroupInfoTo();
                    git.setUser(String.valueOf(item.getSid()));
                    groupInfo.getGroupInfoTos().add(git);
                });
        workGroupSetupPrivate.setGroup_info(groupInfo);
        if (Strings.isNullOrEmpty(workGroupVO.getGroup_sid())) {
            workGroupSetupPrivateManager.create(workGroupSetupPrivate, loginUserSid);
        } else {
            workGroupSetupPrivateManager.update(workGroupSetupPrivate, loginUserSid);
        }
    }

    /**
     * 取得群組List
     *
     * @param loginUserSid 登入者Sid
     * @return
     */
    public List<WorkGroupVO> getWorkGroupVO(Integer loginUserSid) {
        try {
            List<WorkGroupVO> workGroups = Lists.newArrayList();
            List<WCGroupSetupPrivate> workGroupSetupPrivates =
                workGroupSetupPrivateManager.getWorkFunItemByCategoryModel(loginUserSid);

            workGroupSetupPrivates.forEach(
                item -> {
                    List<UserViewVO> userViewVOs = Lists.newArrayList();
                    StringBuilder sb = new StringBuilder();
                    if (item.getGroup_info() != null
                        && item.getGroup_info().getGroupInfoTos() != null
                        && !item.getGroup_info().getGroupInfoTos().isEmpty()) {
                        item.getGroup_info()
                            .getGroupInfoTos()
                            .forEach(
                                userSid -> {
                                    User user = userManager.findBySid(
                                        Integer.valueOf(userSid.getUser()));
                                    Org dep = orgManager.findBySid(user.getPrimaryOrg().getSid());
                                    sb.append(
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"))
                                        .append("-" + user.getName())
                                        .append("\r\n");
                                    userViewVOs.add(
                                        new UserViewVO(
                                            user.getSid(),
                                            user.getName(),
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                                });
                    }
                    String statusStr = "";
                    if (item.getStatus().equals(Activation.ACTIVE)) {
                        statusStr = "正常";
                    } else {
                        statusStr = "停用";
                    }
                    WorkGroupVO wg =
                        new WorkGroupVO(
                            item.getSid(),
                            item.getGroup_name(),
                            userViewVOs,
                            statusStr,
                            item.getStatus().name(),
                            item.getNote(),
                            sb.toString());
                    workGroups.add(wg);
                });
            return workGroups;
        } catch (Exception e) {
            log.warn("getWorkGroupVO", e);
        }
        return Lists.newArrayList();
    }
}
