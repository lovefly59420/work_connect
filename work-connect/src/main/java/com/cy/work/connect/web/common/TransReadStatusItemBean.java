/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.work.connect.web.common.setting.TransReadStatusSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 轉寄已未讀狀態Item Bean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("request")
public class TransReadStatusItemBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3559304375436510288L;

    /**
     * 取得 轉寄已未讀狀態SelectItems
     */
    public List<SelectItem> getReadStatusItems() {
        return TransReadStatusSetting.getTransReadStatusItems();
    }
}
