package com.cy.work.connect.web.view.vo;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.FlowType;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * 執行項目-查詢結果VO
 *
 * @author brain0925_liao
 */
public class TagSearchVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2864252674814892092L;

    @Getter
    private final String sid;
    @Getter
    private String createTime;
    @Getter
    private String creatUser;
    @Getter
    private String tagName;
    @Getter
    private String reqFlowTypeShow;
    @Getter
    private String reqFlowTypeShowTitle;
    @Getter
    private String status;
    @Getter
    private String modifyTime;
    @Getter
    private String modifyUser;
    @Getter
    private String seq;
    /**
     * 執行單位名稱(多筆合成)
     */
    @Getter
    private String execDepsName;
    /**
     * 執行單位名稱 (for tooltip)
     */
    @Getter
    private String execDepsNameTooltip;

    @Getter
    @Setter
    private Activation statusObject;
    /**
     * 可使用單位：顯示文字
     */
    @Setter
    @Getter
    private String canUserDepsShow;
    /**
     * 可使用單位：顯示文字 - ToolTip
     */
    @Setter
    @Getter
    private String canUserDepsShowToolTip;
    /**
     * 可閱部門：顯示文字
     */
    @Setter
    @Getter
    private String useDepShow;
    /**
     * 可閱部門：顯示文字 - ToolTip
     */
    @Setter
    @Getter
    private String useDepShowToolTip;
    /**
     * 可閱人員
     */
    @Getter
    private String useUserNames;
    /**
     * 顯示分類
     */
    @Getter
    private String tagType;

    public TagSearchVO(String sid) {
        this.sid = sid;
    }

    /**
     * 建構子
     *
     * @param sid
     * @param createTime
     * @param creatUser
     * @param tagName
     * @param reqFlowType
     * @param status
     * @param modifyTime
     * @param modifyUser
     * @param seq
     * @param canUserDeps
     * @param canUserDepsTempletSid
     * @param isUserContainFollowing
     * @param useDeps
     * @param useDepsTempletSid
     * @param isViewContainFollowing
     * @param execDepsName
     * @param execDepsNameTooltip
     * @param useUser
     * @param tagType
     */
    public TagSearchVO(
        String sid,
        String createTime,
        String creatUser,
        String tagName,
        FlowType reqFlowType,
        String status,
        String modifyTime,
        String modifyUser,
        String seq,
        UserDep canUserDeps,
        Long canUserDepsTempletSid,
        boolean isUserContainFollowing,
        UserDep useDeps,
        Long useDepsTempletSid,
        boolean isViewContainFollowing,
        String execDepsName,
        String execDepsNameTooltip,
        UsersTo useUser,
        String tagType) {
        this.sid = sid;
        this.createTime = createTime;
        this.creatUser = creatUser;
        this.tagName = tagName;
        this.reqFlowTypeShow = reqFlowType.getShortDesc();
        this.reqFlowTypeShowTitle = reqFlowType.getDesc();
        this.status = status;
        this.modifyTime = modifyTime;
        this.modifyUser = modifyUser;
        this.seq = seq;
        this.execDepsName = execDepsName;
        this.execDepsNameTooltip = execDepsNameTooltip;
        this.tagType = tagType;

        WCConfigTempletManager templetManager = WCConfigTempletManager.getInstance();

        // ====================================
        // 準備 可使用單位 顯示文字
        // ====================================
        // 依條件組裝
        String[] showContentAry =
            templetManager.prepareDepSettingShowContent(
                canUserDeps, isUserContainFollowing, canUserDepsTempletSid);

        // 可使用單位 - 顯示標籤
        this.canUserDepsShow = showContentAry[0];

        // 可使用單位 - Tooltip (未設定代表全部單位)
        this.canUserDepsShowToolTip =
            WkStringUtils.isEmpty(showContentAry[1]) ? "全部單位" : showContentAry[1];

        // ====================================
        // 準備 預設可閱部門 顯示文字
        // ====================================
        // 依條件組裝
        showContentAry =
            templetManager.prepareDepSettingShowContent(
                useDeps, isViewContainFollowing, useDepsTempletSid);

        // 預設可閱部門 - 顯示標籤
        this.useDepShow = showContentAry[0];
        // 預設可閱部門 - Tooltip
        this.useDepShowToolTip = WkStringUtils.isEmpty(showContentAry[1]) ? "無" : showContentAry[1];

        WkUserCache userManager = WkUserCache.getInstance();
        // ====================================
        // 準備 預設可閱人員 顯示文字
        // ====================================
        StringBuilder useUserNamesSb = new StringBuilder();
        if (useUser != null && useUser.getUserTos() != null) {
            useUser
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (WkStringUtils.notEmpty(useUserNamesSb.toString())) {
                            useUserNamesSb.append("\n");
                        }

                        User user = userManager.findBySid(subItem.getSid());
                        useUserNamesSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            useUserNamesSb.append("<span style='color:red;'>(停用)</span>");
                        }
                    });
        }
        this.useUserNames = useUserNamesSb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TagSearchVO other = (TagSearchVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
