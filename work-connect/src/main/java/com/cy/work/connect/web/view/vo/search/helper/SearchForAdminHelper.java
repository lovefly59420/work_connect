/**
 * 
 */
package com.cy.work.connect.web.view.vo.search.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.common.vo.WkItem;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.manager.WCCategoryTagManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.CategoryTagToConverter;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.search.SearchForAdminExecInfoVO;
import com.cy.work.connect.web.view.vo.search.SearchForAdminQueryCondition;
import com.cy.work.connect.web.view.vo.search.SearchForAdminVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rbt.util.exceloperate.ExcelExporter;
import com.rbt.util.exceloperate.bean.expt.ColumnDataSet;
import com.rbt.util.exceloperate.bean.expt.ExportDataSet;
import com.rbt.util.exceloperate.bean.expt.config.ExportConfigInfo;
import com.rbt.util.exceloperate.config.ExportConfigReader;
import com.rbt.util.exceloperate.exception.ExcelOperateException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SearchForAdminHelper {

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WCCategoryTagManager wcCategoryTagManager;
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private NativeSqlRepository nativeSqlRepository;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private transient ServletContext servletContext;

    // ========================================================================
    // 變數
    // ========================================================================
    /**
     * 大類排序器
     */
    private static Comparator<WCCategoryTag> categoryTagComparator;
    /**
     * 中類排序器
     */
    private static Comparator<WCMenuTag> menuTagComparator;
    /**
     * 小類排序器
     */
    private static Comparator<WCTag> tagComparator;
    /**
     * 固定參數 NO_TAG_TYPE
     */
    private static final String NO_TAG_TYPE = "##NO_TAG_TYPE##";
    /**
     * 固定參數 ADD_ASSIGN
     */
    private static final String ADD_ASSIGN = "##ADD_ASSIGN##";
    /**
     * CategoryTagToConverter
     */
    private static final CategoryTagToConverter categoryTagToConverter = new CategoryTagToConverter();
    /**
     * StringBlobConverter
     */
    private static final StringBlobConverter stringBlobConverter = new StringBlobConverter();

    static {
        // 大類排序器
        categoryTagComparator = Comparator.comparing(WCCategoryTag::getStatus);
        categoryTagComparator = categoryTagComparator.thenComparing(Comparator.comparing(WCCategoryTag::getSeq));
        categoryTagComparator = categoryTagComparator.thenComparing(Comparator.comparing(WCCategoryTag::getCategoryName));

        // 中類排序器
        menuTagComparator = Comparator.comparing(WCMenuTag::getStatus);
        menuTagComparator = menuTagComparator.thenComparing(Comparator.comparing(WCMenuTag::getSeq));
        menuTagComparator = menuTagComparator.thenComparing(Comparator.comparing(WCMenuTag::getMenuName));

        // 小類排序器
        tagComparator = Comparator.comparing(WCTag::getStatus);
        tagComparator = tagComparator.thenComparing(Comparator.comparing(WCTag::getSeq));
        tagComparator = tagComparator.thenComparing(Comparator.comparing(WCTag::getTagName));
    }

    // ========================================================================
    // 方法
    // ========================================================================
    public List<SearchForAdminVO> search(SearchForAdminQueryCondition condition, Integer allItemSize) {

        Org comp = WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId());
        Integer compSid = -100;
        if (comp != null) {
            compSid = comp.getSid();
        }

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        String searchKeyword = WkStringUtils.safeTrim(condition.getSearchKeyword());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 兜組 SQL
        // ====================================
        Map<String, Object> parameters = Maps.newHashMap();

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT master.wc_sid          AS wcSid, ");
        sql.append("       master.wc_no           AS wcNo, ");
        sql.append("       master.dep_sid         AS createDepSid, ");
        sql.append("       master.create_usr      AS createUserSid, ");
        sql.append("       master.create_dt       AS createDate, ");
        sql.append("       master.wc_status       AS wcStatusCode, ");
        sql.append("       master.theme           AS theme, ");
        sql.append("       master.content_css     AS contentCssBlob, ");
        sql.append("       category.category_name AS categoryName, ");
        sql.append("       menu.menu_name         AS menuName, ");
        sql.append("       master.category_tag    AS categoryTagJsonStr ");
        sql.append("FROM   wc_master master ");
        sql.append("       LEFT JOIN wc_menu_tag menu ");
        sql.append("              ON menu.wc_menu_tag_sid = master.menuTag_sid ");
        sql.append("       LEFT JOIN wc_category_tag category ");
        sql.append("              ON category.wc_category_tag_sid = menu.wc_category_tag_sid ");

        sql.append("WHERE  master.comp_sid = " + compSid + " ");

        if (WkStringUtils.isEmpty(forceWcNo)) {

            // 申請人
            if (WkStringUtils.notEmpty(condition.getCreateUserName())) {
                List<Integer> userSids = WkUserUtils.findByNameLike(
                        condition.getCreateUserName(),
                        SecurityFacade.getCompanyId());

                if (WkStringUtils.isEmpty(userSids)) {
                    MessagesUtils.showWarn("找不到符合的申請人：【" + condition.getCreateUserName() + "】，請調整查詢條件!");
                    return Lists.newArrayList();
                }

                sql.append("       AND master.create_usr IN ( :createUserSids ) ");
                parameters.put("createUserSids", userSids);
            }

            // 單據狀態
            if (WkStringUtils.notEmpty(condition.getWcStatus())) {
                // 全選時也不過濾
                if (WCStatus.values().length != condition.getWcStatus().size()) {
                    sql.append("       AND master.wc_status IN ( :wcStatus ) ");
                    parameters.put("wcStatus", condition.getWcStatus());
                }
            }
            // 起日
            if (condition.getStartDate() != null) {
                sql.append("       AND master.create_dt >= :startDate ");
                parameters.put("startDate", WkDateUtils.toStartDay(condition.getStartDate()));
            }

            // 迄日
            if (condition.getEndDate() != null) {
                sql.append("       AND master.create_dt <= :endDate ");
                parameters.put("endDate", WkDateUtils.toEndDay(condition.getEndDate()));
            }

            if (WkStringUtils.notEmpty(condition.getSearchKeyword())) {
                String searchText = WkStringUtils.safeTrim(condition.getSearchKeyword()).toLowerCase();
                sql.append(" AND  (  ");
                // 主題
                sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(master.theme)", searchText));
                // 內容
                sql.append("         OR  ");
                sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(master.content)", searchText));
                // 追蹤內容
                sql.append("         OR  ");
                Map<String, String> bindColumns = Maps.newHashMap();
                bindColumns.put("wc_sid", "master.wc_sid");
                sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                        "wc_trace", bindColumns, "wc_trace_content", searchText));
                sql.append("      ) ");
            }

            sql.append(" ORDER BY master.wc_no DESC ");
        } else {
            sql.append("       AND master.wc_no = :wcNo ");
            parameters.put("wcNo", forceWcNo);
        }

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("管理者查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        long startTime = System.currentTimeMillis();
        List<SearchForAdminVO> searchForAdminVOs = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, SearchForAdminVO.class);

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "主查詢, 共:[" + searchForAdminVOs.size() + "]筆"));

        if (WkStringUtils.isEmpty(searchForAdminVOs)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢小類資料
        // ====================================
        startTime = System.currentTimeMillis();
        // 收集小類sid
        Set<String> wcTagSids = Sets.newHashSet();
        for (SearchForAdminVO vo : searchForAdminVOs) {
            // 解析小類
            this.parserCategoryTag(vo);
            if (WkStringUtils.notEmpty(vo.getTagSids())) {
                wcTagSids.addAll(vo.getTagSids());
            }
        }

        // 查詢
        List<WCTag> wcTags = this.wcTagManager.findBySidIn(Lists.newArrayList(wcTagSids));
        // 以wcTagSid進行index
        Map<String, WCTag> wcTagNameMapByTagSid = wcTags.stream()
                .collect(Collectors.toMap(WCTag::getSid, wcTag -> wcTag));

        log.debug(WkCommonUtils.prepareCostMessage(startTime, "查詢小類"));
        // ====================================
        // 過濾單據項目
        // ====================================
        if (WkStringUtils.isEmpty(forceWcNo) &&
                WkStringUtils.notEmpty(condition.getSelectedItemSids())) {
            if (condition.getSelectedItemSids().size() != allItemSize) {

                startTime = System.currentTimeMillis();

                List<SearchForAdminVO> filteredVOs = Lists.newArrayList();
                // 逐一比對
                for (SearchForAdminVO searchForAdminVO : searchForAdminVOs) {
                    if (WkStringUtils.isEmpty(searchForAdminVO.getTagSids())) {
                        continue;
                    }
                    for (String tagSid : searchForAdminVO.getTagSids()) {
                        if (condition.getSelectedItemSids().contains(tagSid)) {
                            filteredVOs.add(searchForAdminVO);
                            break;
                        }
                    }
                }
                if (filteredVOs.size() != searchForAdminVOs.size()) {
                    log.debug("項目過濾, 移除:[{}]", searchForAdminVOs.size() - filteredVOs.size());
                }
                // 置換為過濾後結果
                searchForAdminVOs = filteredVOs;

                log.debug(WkCommonUtils.prepareCostMessage(startTime, "過濾單據項目"));
            }
        }

        if (WkStringUtils.isEmpty(searchForAdminVOs)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 解析
        // ====================================
        startTime = System.currentTimeMillis();
        for (SearchForAdminVO searchForAdminVO : searchForAdminVOs) {

            // 建單時間
            searchForAdminVO.setCreateDateDescr(
                    WkDateUtils.formatDate(
                            searchForAdminVO.getCreateDate(),
                            WkDateUtils.YYYY_MM_DD));

            // 申請部門
            searchForAdminVO.setCreateDepName(this.prepareDepInfo(searchForAdminVO.getCreateDepSid()));

            // 申請人
            searchForAdminVO.setCreateUserName(this.prepareUserInfo(searchForAdminVO.getCreateUserSid()));

            // 解析單據狀態
            this.parserWCStatusCode(searchForAdminVO);

            // 小類顯示
            if (WkStringUtils.notEmpty(searchForAdminVO.getTagSids())) {
                String tagNames = searchForAdminVO.getTagSids().stream()
                        .filter(tagSid -> wcTagNameMapByTagSid.containsKey(tagSid))
                        .map(tagSid -> wcTagNameMapByTagSid.get(tagSid))
                        .sorted(tagComparator)
                        .map(WCTag::getTagName)
                        .collect(Collectors.joining("<br/>"));
                searchForAdminVO.setTagNames(tagNames);
            }

        }
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "解析資料"));

        return searchForAdminVOs;
    }

    // ========================================================================
    // EXCEL
    // ========================================================================
    public DefaultStreamedContent exportEXCEL(List<SearchForAdminVO> searchForAdminVOs) {

        // ====================================
        // 整理匯出資料
        // ====================================
        ExportDataSet exportDataSet = this.prepareExportDataSet(searchForAdminVOs);

        // ====================================
        // 取得EXCEL格式定義
        // ====================================
        File excelExportDefFile = new File(servletContext.getRealPath("/WEB-INF/excel/excel-export.xml"));
        if (!excelExportDefFile.exists()) {
            log.error("excel-export.xml 檔案不存在!");
            MessagesUtils.showInfo("excel-export.xml 檔案不存在!");
            return null;
        }
        String path = servletContext.getRealPath("/WEB-INF/excel/excel-export.xml");
        ExportConfigInfo exportConfigInfo = null;
        try {
            exportConfigInfo = new ExportConfigReader().read(path, "SearchForAdmin");
        } catch (Exception e) {
            String errorMessage = "讀取 excel-export.xml 失敗![" + e.getMessage() + "]";
            log.error(errorMessage, e);
            MessagesUtils.showError(errorMessage);
            return null;
        }

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        try {
            new ExcelExporter().export(exportConfigInfo, exportDataSet, buffer);
        } catch (ExcelOperateException e) {
            log.error(e.getMessage(), e);
            return null;
        }

        byte[] bytes = buffer.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);

        return DefaultStreamedContent.builder()
                .stream(() -> inputStream)
                .contentType("application/vnd.ms-excel")
                .name(exportConfigInfo.getFileName())
                .build();

    }

    public ExportDataSet prepareExportDataSet(List<SearchForAdminVO> searchForAdminVOs) {

        // ====================================
        // 查詢小類資料
        // ====================================
        // 收集小類sid
        Set<String> wcTagSids = Sets.newHashSet();
        for (SearchForAdminVO vo : searchForAdminVOs) {
            // 解析小類
            this.parserCategoryTag(vo);
            if (WkStringUtils.notEmpty(vo.getTagSids())) {
                wcTagSids.addAll(vo.getTagSids());
            }
        }

        // 查詢
        List<WCTag> wcTags = this.wcTagManager.findBySidIn(Lists.newArrayList(wcTagSids));
        // 以wcTagSid進行index
        Map<String, WCTag> wcTagMapByTagSid = wcTags.stream()
                .collect(Collectors.toMap(WCTag::getSid, wcTag -> wcTag));

        // ====================================
        // 查詢執行單位資料
        // ====================================
        Map<String, List<SearchForAdminExecInfoVO>> execInfoVOsMapByWcSid = this.findExecInfo(searchForAdminVOs);

        // ====================================
        // 取得org 排序
        // ====================================
        Map<Integer, Integer> orgSortNumberMapByDepSid = WkOrgCache.getInstance().findOrgSortNumberMapByDepSid();

        // ====================================
        // 兜組 ExportDataSet
        // ====================================
        ExportDataSet exportDataSet = new ExportDataSet();
        exportDataSet.setContext("header", new HashMap<>());

        List<ColumnDataSet> detailDataSetList = Lists.newArrayList();
        exportDataSet.setDetailDataSet("detail", detailDataSetList);

        for (SearchForAdminVO searchForAdminVO : searchForAdminVOs) {
            ColumnDataSet mainColumnDataSet = new ColumnDataSet();
            detailDataSetList.add(mainColumnDataSet);

            // 單一欄位
            mainColumnDataSet.setColumn("wcNo", searchForAdminVO.getWcNo());
            mainColumnDataSet.setColumn("createDepName", searchForAdminVO.getCreateDepName());
            mainColumnDataSet.setColumn("createUserName", searchForAdminVO.getCreateUserName());
            mainColumnDataSet.setColumn("createDateDescr", searchForAdminVO.getCreateDateDescr());
            mainColumnDataSet.setColumn("wcStatusDescr", searchForAdminVO.getWcStatusDescr());
            mainColumnDataSet.setColumn("theme", searchForAdminVO.getTheme());
            mainColumnDataSet.setColumn("formTypeName", searchForAdminVO.getCategoryName() + "-" + searchForAdminVO.getMenuName());

            String contentText = "";
            if (searchForAdminVO.getContentCssBlob() != null) {
                contentText = SearchForAdminHelper.stringBlobConverter.convertToEntityAttribute(searchForAdminVO.getContentCssBlob());
                contentText = WkJsoupUtils.getInstance().htmlToText(contentText);
            }
            mainColumnDataSet.setColumn("contentText", contentText);

            // 取得所有小類資料
            List<WCTag> currTags = this.prepareWcTagForOne(searchForAdminVO, wcTagMapByTagSid);

            // 取得所有執行單位檔資料
            // 並以 TagSid 進行分組 (為加派者 key 為 ADD_ASSIGN)
            // [<TagSid>, List<ExecDepInfo>]
            Map<String, List<SearchForAdminExecInfoVO>> execInfosMapByTagSid = Maps.newHashMap();
            if (execInfoVOsMapByWcSid.containsKey(searchForAdminVO.getWcSid())) {
                List<SearchForAdminExecInfoVO> execInfos = execInfoVOsMapByWcSid.get(searchForAdminVO.getWcSid());
                execInfosMapByTagSid = execInfos.stream()
                        .collect(Collectors.groupingBy(
                                vo -> WkStringUtils.isEmpty(vo.getTagSid()) ? ADD_ASSIGN : vo.getTagSid(),
                                Collectors.mapping(
                                        vo -> vo,
                                        Collectors.toList())));
            }

            // 處理 tagInfo
            this.prepareTagInfoColumnDataSet(mainColumnDataSet, currTags, execInfosMapByTagSid, orgSortNumberMapByDepSid);
        }

        return exportDataSet;
    }

    /**
     * @param mainColumnDataSet
     * @param currTags
     * @param execInfosMapByTagSid
     * @param orgSortNumberMapByDepSid
     */
    private void prepareTagInfoColumnDataSet(
            ColumnDataSet mainColumnDataSet,
            List<WCTag> currTags,
            Map<String, List<SearchForAdminExecInfoVO>> execInfosMapByTagSid,
            Map<Integer, Integer> orgSortNumberMapByDepSid) {

        // ====================================
        // 加派單位
        // ====================================
        if (execInfosMapByTagSid.containsKey(ADD_ASSIGN)) {
            WCTag wcTag = new WCTag();
            wcTag.setSid(ADD_ASSIGN);
            wcTag.setTagName("被加派單位");
            currTags.add(wcTag);
        }

        // ====================================
        // 檢查沒有對應tag的執行單位
        // ====================================
        Set<String> wcTagSids = currTags.stream()
                .map(WCTag::getSid)
                .collect(Collectors.toSet());

        for (String tagSid : execInfosMapByTagSid.keySet()) {
            if (!wcTagSids.contains(tagSid)) {
                WCTag wcTag = new WCTag();
                wcTag.setSid(tagSid);
                wcTag.setTagName("未對應的項目[" + tagSid + "]");
                currTags.add(wcTag);
                log.error("有未對應項目的執行單位檔 tagSid:[{}]", tagSid);
            }
        }

        // ====================================
        // 逐筆處理小類
        // ====================================
        List<ColumnDataSet> tagSetList = Lists.newArrayList();
        mainColumnDataSet.setArrayColumnDataSet("tagInfo", tagSetList);

        for (WCTag wcTag : currTags) {

            ColumnDataSet tagColumnDataSet = new ColumnDataSet();
            tagSetList.add(tagColumnDataSet);

            tagColumnDataSet.setColumn("tagName", wcTag.getTagName());

            // 取得所有執行資訊
            List<SearchForAdminExecInfoVO> execInfos = execInfosMapByTagSid.get(wcTag.getSid());

            List<ColumnDataSet> execDepInfoSetList = Lists.newArrayList();
            tagColumnDataSet.setArrayColumnDataSet("execDepInfo", execDepInfoSetList);

            if (WkStringUtils.notEmpty(execInfos)) {
                // 依據Org樹狀圖排序
                execInfos = execInfos.stream()
                        .sorted(Comparator.comparing(execInfo -> orgSortNumberMapByDepSid.get(execInfo.getExecDepSid())))
                        .collect(Collectors.toList());

                for (SearchForAdminExecInfoVO execInfoVO : execInfos) {
                    execDepInfoSetList.add(this.prepareExecInfoColumnDataSet(execInfoVO));
                }
            } else {
                // 未有執行資訊時，塞一筆空的
                execDepInfoSetList.add(new ColumnDataSet());
            }
        }
    }

    /**
     * @param execInfoVO
     * @return
     */
    private ColumnDataSet prepareExecInfoColumnDataSet(SearchForAdminExecInfoVO execInfoVO) {

        ColumnDataSet execInfoColumnDataSet = new ColumnDataSet();
        // 執行單位
        execInfoColumnDataSet.setColumn("execDepInfo", this.prepareDepInfo(execInfoVO.getExecDepSid()));
        // 執行狀況
        String execStatus = "";
        for (WCExceDepStatus wcExceDepStatus : WCExceDepStatus.values()) {
            if (wcExceDepStatus.name().equals(execInfoVO.getExecStatus())) {
                execStatus = wcExceDepStatus.getValue();
                break;
            }
        }
        execInfoColumnDataSet.setColumn("execStatus", execStatus);

        // 執行人員
        execInfoColumnDataSet.setColumn("execUserInfo", this.prepareUserInfo(execInfoVO.getExecUserSid()));

        return execInfoColumnDataSet;
    }

    /**
     * @param vo
     * @param wcTagMapByTagSid
     * @return
     */
    private List<WCTag> prepareWcTagForOne(SearchForAdminVO vo, Map<String, WCTag> wcTagMapByTagSid) {

        if (WkStringUtils.isEmpty(vo.getTagSids())) {
            log.warn("單據:[{}]無小類資料!!", vo.getWcNo());
            return Lists.newArrayList();
        }

        List<WCTag> wcTags = Lists.newArrayList();
        for (String tagSid : vo.getTagSids()) {
            if (!wcTagMapByTagSid.containsKey(tagSid)) {
                log.warn("單據:[{}],小類:[{}]無對應資料!!", vo.getWcNo(), tagSid);
                continue;
            }
            wcTags.add(wcTagMapByTagSid.get(tagSid));
        }

        return wcTags.stream()
                .sorted(SearchForAdminHelper.tagComparator)
                .collect(Collectors.toList());
    }

    private Map<String, List<SearchForAdminExecInfoVO>> findExecInfo(List<SearchForAdminVO> searchForAdminVOs) {

        if (WkStringUtils.isEmpty(searchForAdminVOs)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 收集 sid
        // ====================================
        String wcSidsStr = searchForAdminVOs.stream()
                .map(SearchForAdminVO::getWcSid)
                .collect(Collectors.joining("','", "'", "'"));

        // ====================================
        // 取得執行單位資訊
        // ====================================
        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT execDep.wc_sid          AS wcSid, ");
        varname1.append("       tag.wc_tag_sid          AS tagSid, ");
        varname1.append("       execDep.dep_sid         AS execDepSid, ");
        varname1.append("       execDep.exec_dep_status AS execStatus, ");
        varname1.append("       execDep.exec_user_sid   AS execUserSid ");
        varname1.append("FROM   wc_exec_dep execDep ");
        varname1.append("       LEFT JOIN wc_exec_dep_setting depSetting ");
        varname1.append("              ON depSetting.wc_exec_dep_setting_sid = execDep.wc_exec_dep_setting_sid ");
        varname1.append("       LEFT JOIN wc_tag tag ");
        varname1.append("              ON tag.wc_tag_sid = depSetting.wc_tag_sid ");
        varname1.append("WHERE  execDep.status = 0 ");
        varname1.append("       AND execDep.wc_sid IN ( " + wcSidsStr + " )");

        List<SearchForAdminExecInfoVO> execInfoVOs = this.nativeSqlRepository.getResultList(
                varname1.toString(), null, SearchForAdminExecInfoVO.class);

        // 以WcSid進行分類
        Map<String, List<SearchForAdminExecInfoVO>> execInfoVOsMapByWcSid = execInfoVOs.stream()
                .collect(Collectors.groupingBy(
                        SearchForAdminExecInfoVO::getWcSid,
                        Collectors.mapping(
                                vo -> vo,
                                Collectors.toList())));

        return execInfoVOsMapByWcSid;

    }

    // ========================================================================
    // 取得單據類別項目
    // ========================================================================
    /**
     * 取得所有大中小類, 並轉 WkItem
     * 
     * @param compSid
     * @return
     */
    public List<WkItem> findAllItem(Integer compSid) {

        // ====================================
        // 查詢大類
        // ====================================
        // 查詢
        List<WCCategoryTag> wcCategoryTags = this.wcCategoryTagManager.findByCompSid(compSid);

        if (WkStringUtils.isEmpty(wcCategoryTags)) {
            log.warn("查無任何大類設定資料，類別樹為空 compSid:[{}]", compSid);
            return Lists.newArrayList();
        }

        // 收集sid
        List<String> wcCategoryTagSids = wcCategoryTags.stream()
                .map(WCCategoryTag::getSid)
                .collect(Collectors.toList());

        // ====================================
        // 查詢中類
        // ====================================
        // 查詢
        List<WCMenuTag> wcMenuTags = this.wcMenuTagManager.findByWcCategoryTagSidIn(wcCategoryTagSids);

        if (WkStringUtils.isEmpty(wcMenuTags)) {
            log.warn("查無任何中類設定資料，類別樹為空");
            return Lists.newArrayList();
        }
        // 收集SID
        List<String> wcMenuTagSids = wcMenuTags.stream()
                .map(WCMenuTag::getSid)
                .collect(Collectors.toList());

        // 以大類分組
        Map<String, List<WCMenuTag>> wcMenuTagsMapByCategoryTagSid = wcMenuTags.stream()
                .collect(Collectors.groupingBy(
                        WCMenuTag::getWcCategoryTagSid,
                        Collectors.mapping(
                                wcMenuTag -> wcMenuTag,
                                Collectors.toList())));

        // ====================================
        // 查詢小類
        // ====================================
        // 查詢
        List<WCTag> wcTags = this.wcTagManager.findByWcMenuTagSidIn(wcMenuTagSids);

        if (WkStringUtils.isEmpty(wcMenuTags)) {
            log.warn("查無任何小類設定資料，類別樹為空");
            return Lists.newArrayList();
        }

        // 以中類類分組
        Map<String, List<WCTag>> wcTagsMapByMenuTagSid = wcTags.stream()
                .collect(Collectors.groupingBy(
                        WCTag::getWcMenuTagSid,
                        Collectors.mapping(
                                wcTag -> wcTag,
                                Collectors.toList())));

        // ====================================
        // 處理
        // ====================================
        return this.prepareWkItemBycategoryTag(wcCategoryTags, wcMenuTagsMapByCategoryTagSid, wcTagsMapByMenuTagSid);

    }

    private List<WkItem> prepareWkItemBycategoryTag(
            List<WCCategoryTag> wcCategoryTags,
            Map<String, List<WCMenuTag>> wcMenuTagsMapByCategoryTagSid,
            Map<String, List<WCTag>> wcTagsMapByMenuTagSid) {

        // 防呆
        if (WkStringUtils.isEmpty(wcCategoryTags)) {
            return Lists.newArrayList();
        }

        // 收集所有的 WkItem
        List<WkItem> allWkItems = Lists.newArrayList();

        // ====================================
        // 大類
        // ====================================
        // 大類排序
        wcCategoryTags = wcCategoryTags.stream()
                .sorted(SearchForAdminHelper.categoryTagComparator)
                .collect(Collectors.toList());

        // 逐筆處理大類
        Integer categoryTagSeq = 0;
        for (WCCategoryTag categoryTag : wcCategoryTags) {

            // ====================================
            // 中類
            // ====================================
            // 取得中類
            List<WCMenuTag> wcMenuTags = wcMenuTagsMapByCategoryTagSid.get(categoryTag.getSid());
            // 此大類下無任何中類時不顯示此大類
            if (WkStringUtils.isEmpty(wcMenuTags)) {
                continue;
            }

            // 中類排序
            wcMenuTags = wcMenuTags.stream()
                    .sorted(SearchForAdminHelper.menuTagComparator)
                    .collect(Collectors.toList());

            // 大類轉 WkItem
            WkItem categoryTagItem = this.transToItem(categoryTag, categoryTagSeq++, allWkItems);

            // 逐筆處理大類下的中類
            long wcMenuTagSeq = 0;
            for (WCMenuTag wcMenuTag : wcMenuTags) {

                // ====================================
                // 小類
                // ====================================
                // 取得小類
                List<WCTag> wcTags = wcTagsMapByMenuTagSid.get(wcMenuTag.getSid());
                // 此大類下無任何小類時不顯示此大類 (return null)
                if (WkStringUtils.isEmpty(wcTags)) {
                    continue;
                }

                // 中類轉 WKItem
                WkItem menuTagItem = this.transToItem(wcMenuTag, categoryTagItem, wcMenuTagSeq++, allWkItems);

                // 以tagType分組
                Map<String, List<WCTag>> wcTagsMapByTagType = wcTags.stream()
                        .collect(Collectors.groupingBy(
                                wcTag -> WkStringUtils.isEmpty(wcTag.getTagType()) ? NO_TAG_TYPE : wcTag.getTagType(),
                                Collectors.mapping(
                                        wcTag -> wcTag,
                                        Collectors.toList())));

                // 取得所有的 tagType, 並以名稱排序
                List<String> tagTypes = wcTagsMapByTagType.keySet().stream()
                        .sorted()
                        .collect(Collectors.toList());

                // 逐筆處理
                long itemSeq = 0;
                for (String tagType : tagTypes) {
                    // NO_TAG_TYPE 另外處理
                    if (NO_TAG_TYPE.equals(tagType)) {
                        continue;
                    }

                    // 建立 tagType 層
                    WkItem tagTypeItem = new WkItem(
                            wcMenuTag.getSid() + "_" + tagType,
                            tagType,
                            true);
                    tagTypeItem.setItemSeq(itemSeq++);
                    tagTypeItem.setParent(menuTagItem);
                    allWkItems.add(tagTypeItem);

                    this.prepareWcTags(wcTagsMapByTagType.get(tagType), tagTypeItem, 0, allWkItems);
                    itemSeq++;
                }

                // 處理 NO_TAG_TYPE
                if (wcTagsMapByTagType.containsKey(NO_TAG_TYPE)) {
                    this.prepareWcTags(wcTagsMapByTagType.get(NO_TAG_TYPE), menuTagItem, itemSeq++, allWkItems);
                }
            }

        }

        return allWkItems;
    }

    private void prepareWcTags(List<WCTag> wcTags, WkItem parentItem, long startSeq, List<WkItem> allWkItems) {

        if (WkStringUtils.isEmpty(wcTags)) {
            return;
        }

        // 小類排序
        wcTags = wcTags.stream()
                .sorted(SearchForAdminHelper.tagComparator)
                .collect(Collectors.toList());

        // 逐筆處理
        for (WCTag wcTag : wcTags) {
            // 小類轉 WKItem
            this.transToItem(wcTag, parentItem, startSeq++, allWkItems);
        }
    }

    /**
     * WCCategoryTag to WkItem
     * 
     * @param categoryTag WCCategoryTag
     * @param seq         排序
     * @return WkItem
     */
    private WkItem transToItem(WCCategoryTag categoryTag, long seq, List<WkItem> allWkItems) {
        WkItem wkItem = new WkItem(
                categoryTag.getSid(),
                categoryTag.getCategoryName(),
                categoryTag.isActive());

        wkItem.setItemSeq(seq);
        allWkItems.add(wkItem);

        return wkItem;
    }

    /**
     * WCMenuTag to WkItem
     * 
     * @param wcMenuTag       WCMenuTag
     * @param categoryTagItem categoryTagItem
     * @param seq             排序
     * @return
     */
    private WkItem transToItem(WCMenuTag wcMenuTag, WkItem categoryTagItem, long seq, List<WkItem> allWkItems) {
        WkItem wkItem = new WkItem(
                wcMenuTag.getSid(),
                wcMenuTag.getMenuName(),
                wcMenuTag.isActive());

        // 父項停用時，子項自動停用
        if (!categoryTagItem.isActive()) {
            wkItem.setActive(false);
        }
        wkItem.setParent(categoryTagItem);
        wkItem.setItemSeq(seq);

        allWkItems.add(wkItem);

        return wkItem;
    }

    private WkItem transToItem(WCTag wcTag, WkItem parentItem, long seq, List<WkItem> allWkItems) {
        WkItem wkItem = new WkItem(
                wcTag.getSid(),
                wcTag.getTagName(),
                wcTag.isActive());

        // 父項停用時，子項自動停用
        if (!parentItem.isActive()) {
            wkItem.setActive(false);
        }
        wkItem.setParent(parentItem);
        wkItem.setItemSeq(seq);

        allWkItems.add(wkItem);

        return wkItem;
    }

    // ========================================================================
    // 其他
    // ========================================================================
    /**
     * 兜組user資訊
     * 
     * @param userSid user Sid
     * @return user info
     */
    private String prepareUserInfo(Integer userSid) {
        if (userSid == null) {
            return "";
        }

        String userInfo = WkUserUtils.prepareUserNameWithDep(
                userSid,
                OrgLevel.THE_PANEL,
                false, "－");

        if (!WkUserUtils.isActive(userSid)) {
            userInfo = "【帳號停用】" + userInfo;
        }
        return userInfo;
    }

    private String prepareDepInfo(Integer depSid) {
        if (depSid == null) {
            return "";
        }

//        String depInfo = WkOrgUtils.prepareBreadcrumbsByDepName(
//                depSid,
//                OrgLevel.THE_PANEL, true, "－");

        Org org = WkOrgCache.getInstance().findBySid(depSid);
        String depInfo = org.getOrgNameWithParentIfDuplicate();


        if (!WkOrgUtils.isActive(depSid)) {
            depInfo = "【停用單位】" + depInfo;
        }
        return depInfo;
    }

    /**
     * 解析單據狀態
     * 
     * @param searchForAdminVO
     */
    private void parserWCStatusCode(SearchForAdminVO searchForAdminVO) {
        for (WCStatus type : WCStatus.values()) {
            if (type.name().equals(searchForAdminVO.getWcStatusCode())) {
                searchForAdminVO.setWcStatusDescr(type.getVal());
                return;
            }
        }
    }

    /**
     * 解析小類
     * 
     * @param searchForAdminVO
     */
    private void parserCategoryTag(SearchForAdminVO searchForAdminVO) {

        if (WkStringUtils.isEmpty(searchForAdminVO.getCategoryTagJsonStr())) {
            return;
        }
        // JSON to VO
        CategoryTagTo categoryTagTo = SearchForAdminHelper.categoryTagToConverter.convertToEntityAttribute(
                searchForAdminVO.getCategoryTagJsonStr());

        if (categoryTagTo != null
                && WkStringUtils.notEmpty(categoryTagTo.getTagSids())) {
            searchForAdminVO.setTagSids(categoryTagTo.getTagSids());
        }
    }

}
