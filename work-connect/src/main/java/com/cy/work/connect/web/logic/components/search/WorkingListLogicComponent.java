package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.WorkingListColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.WorkingListColumnVO;
import com.cy.work.connect.web.view.vo.search.ProcessListVO;
import com.cy.work.connect.web.view.vo.search.query.CheckedListQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 報表【部門待處理清單】 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class WorkingListLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -394633044476859542L;

    private static WorkingListLogicComponent instance;
    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.WORKING_LIST;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WkOrgCache wkOrgCache;
    @Autowired
    private WkUserCache wkUserCache;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCTagManager wcTagManager;

    public static WorkingListLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WorkingListLogicComponent.instance = this;
    }

    /**
     * 組合《處理清單》DB查詢物件為列表資料
     *
     * @param results      《處理清單》列表資料
     * @param each         查詢DB物件
     * @param loginUserSid 登入者Sid
     */
    private void settingObject(
            Map<String, ProcessListVO> results, Map<String, Object> each, Integer loginUserSid) {
        try {
            String wcSid = (String) each.get("wc_sid");
            ProcessListVO obj = null;
            if (!results.containsKey(wcSid)) {
                obj = new ProcessListVO(wcSid);
                obj.setWcSid((String) each.get("wc_sid"));
                obj.setWcNo((String) each.get("wc_no"));
                obj.setWcStatus(WCStatus.valueOf((String) each.get("wc_status")).getVal());
                obj.setCreateDate(new DateTime(each.get("create_dt")).toString("yyyy/MM/dd"));
                obj.setCreateDt((Date) each.get("create_dt"));
                obj.setReqDepName(WkOrgUtils.findNameBySid((Integer) each.get("reqDepSid")));
                MenuTagVO menuTag = menuTagLogicComponent.findToBySid((String) each.get("menuTag_sid"));
                obj.setMenuTagName(menuTag.getName());
                obj.setCategoryName(categoryTagLogicComponent
                        .getCategoryTagEditVOBySid(menuTag.getCategorySid())
                        .getCategoryName());
                obj.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
                obj.setContent(WkStringUtils.removeCtrlChr((String) each.get("content")));
                if (each.get("reply_not_read") != null) {
                    try {
                        String replyNotReadStr = (String) each.get("reply_not_read");
                        if (!Strings.isNullOrEmpty(replyNotReadStr)) {
                            List<ReplyRecordTo> resultList = WkJsonUtils.getInstance()
                                    .fromJsonToList(replyNotReadStr, ReplyRecordTo.class);
                            if (resultList != null) {
                                List<ReplyRecordTo> s = resultList.stream()
                                        .filter(
                                                eu -> String.valueOf(loginUserSid).equals(eu.getUser()))
                                        .collect(Collectors.toList());
                                if (s != null && !s.isEmpty()) {
                                    obj.setHasUnReadCount(true);
                                    obj.setUnReadCount(s.get(0).getUnReadCount());
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.warn("settingObject ERROR", e);
                    }
                }
            } else {
                obj = results.get(wcSid);
            }
            String wc_exec_dep_setting_sid = (String) each.get("wc_exec_dep_setting_sid");
            if (!Strings.isNullOrEmpty(wc_exec_dep_setting_sid)) {
                WCExecDepSetting execDepSetting = execDepSettingManager.findBySidFromCache(wc_exec_dep_setting_sid);
                if (execDepSetting != null) {
                    WCTag wcTag = wcTagManager.getWCTagBySid(execDepSetting.getWc_tag_sid());
                    obj.bulidExecTag(wcTag);
                }
            }
            if (each.get("exec_user_sid") != null) {
                obj.bulidExecUser(wkUserCache.findBySid((Integer) each.get("exec_user_sid")));
            }
            Integer depSid = (Integer) each.get("dep_sid");
            String execDepStatusStr = (String) each.get("exec_dep_status");
            WCExceDepStatus execDepStatus = WCExceDepStatus.valueOf(execDepStatusStr);
            Org execDep = wkOrgCache.findBySid(depSid);
            obj.buildExecDep(execDep, execDepStatus);

            results.put(wcSid, obj);
        } catch (Exception e) {
            log.warn("settingObject ERROR", e);
        }
    }

    /**
     * 建立《處理清單》(執行中)查詢結果
     *
     * @param results      《處理清單》列表資料
     * @param query        《處理清單》查詢物件
     * @param loginUserSid 登入者Sid
     * @param wcSid        工作聯絡單Sid
     */
    private void settingCheckList(
            Map<String, ProcessListVO> results,
            CheckedListQuery query,
            Integer loginUserSid,
            String wcSid) {

        String sql = SearchSqlHelper.getInstance().prepareQuerySqlForExecDep(
                loginUserSid,
                Sets.newHashSet(WCExceDepStatus.PROCEDD),
                query.getCategorySid(),
                query.getStartDate(),
                query.getEndDate(),
                wcSid,
                query.getLazyContent());

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("部門待處理清單"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> dbResults = jdbc.queryForList(sql);
        log.debug(WkCommonUtils.prepareCostMessage(startTime, "部門待處理清單，共[" + dbResults.size() + "]筆"));
        if (WkStringUtils.isEmpty(dbResults)) {
            return;
        }

        // 解析資料
        String skypeAlertMessage = "";
        for (Map<String, Object> dbResultMap : dbResults) {
            try {
                this.settingObject(results, dbResultMap, loginUserSid);
            } catch (Exception e) {
                String message = "部門待處理清單。資料解析失敗!" + e.getMessage();
                log.error(message, e);
                log.error("row data:\r\n" + WkJsonUtils.getInstance().toPettyJson(dbResultMap));
                skypeAlertMessage = message;
            }
        }

        if (WkStringUtils.notEmpty(skypeAlertMessage)) {
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(skypeAlertMessage);
        }
    }

    /**
     * 計算查詢筆數 (聯絡單home功能清單筆數)
     * 
     * @param loginUserSid
     * @return
     */
    public int count(Integer loginUserSid) {

        CheckedListQuery query = new CheckedListQuery();
        query.init();

        String sql = SearchSqlHelper.getInstance().prepareQuerySqlForExecDep(
                loginUserSid,
                Sets.newHashSet(WCExceDepStatus.PROCEDD),
                query.getCategorySid(),
                query.getStartDate(),
                query.getEndDate(),
                "",
                "");

        sql = "SELECT Count(*) FROM ( SELECT DISTINCT wc_sid  FROM (" + sql + " ) AS BB  ) as aa;";

        return this.jdbc.queryForObject(sql, Integer.class);
    }

    /**
     * 取得 處理清單 查詢物件List
     *
     * @param query        《處理清單》查詢物件
     * @param loginUserSid 登入者Sid
     * @param sid          工作聯絡單Sid
     * @return
     */
    public List<ProcessListVO> search(CheckedListQuery query, Integer loginUserSid, String sid) {
        Map<String, ProcessListVO> results = Maps.newHashMap();

        this.settingCheckList(results, query, loginUserSid, sid);

        List<ProcessListVO> res = results.entrySet().stream()
                .map(each -> each.getValue()).collect(Collectors.toList());

        if (res == null || res.isEmpty()) {
            return Lists.newArrayList();
        }

        // 以日期排序
        return res.stream()
                .sorted(Comparator.comparing(ProcessListVO::getCreateDt))
                .collect(Collectors.toList());

    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid 登入者Sid
     * @return
     */
    public WorkingListColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        WorkingListColumnVO vo = new WorkingListColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(WorkingListColumn.INDEX));
            vo.setCreateDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WorkingListColumn.CREATE_DATE));
            vo.setCategoryName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WorkingListColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WorkingListColumn.MENUTAG_NAME));
            vo.setExecUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WorkingListColumn.EXEC_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(WorkingListColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(WorkingListColumn.WC_NO));
            vo.setExecStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WorkingListColumn.EXEC_DEP_NAME));
            vo.setWcStatus(
                    customColumnLogicComponent.createDefaultColumnDetail(WorkingListColumn.WC_STATUS));
            vo.setReqDep(
                    customColumnLogicComponent.createDefaultColumnDetail(WorkingListColumn.REQ_DEP));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setExecUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.EXEC_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.WC_NO, columDB.getInfo()));
            vo.setExecStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.EXEC_DEP_NAME, columDB.getInfo()));
            vo.setWcStatus(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.WC_STATUS, columDB.getInfo()));
            vo.setReqDep(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WorkingListColumn.REQ_DEP, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }
}
