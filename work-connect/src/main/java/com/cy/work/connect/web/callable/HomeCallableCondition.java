/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.callable;

import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WorkFunItemComponentType;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 選單計算筆數 Call able 傳遞物件
 *
 * @author brain0925_liao
 */
public class HomeCallableCondition implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7690984394644697193L;

    @Setter
    @Getter
    private WorkFunItemGroupVO workFunItemGroupVO;
    @Setter
    @Getter
    private WorkFunItemComponentType workFunItemComponentType;
    @Setter
    @Getter
    private String conntString;
    @Setter
    @Getter
    private String cssString;
}
