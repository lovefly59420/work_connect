/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.vo.User;
import com.cy.work.connect.logic.manager.WCBasePersonAccessInfoManager;
import com.cy.work.connect.vo.WCBasePersonAccessInfo;
import com.cy.work.connect.vo.converter.to.LimitBaseAccessViewPerson;
import com.cy.work.connect.vo.converter.to.OtherBaseAccessViewPerson;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位成員可閱權限設定邏輯原件
 *
 * @author brain0925_liao
 */
@Component
public class WCBasePersonAccessInfoLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9044106014124205589L;
    /**
     * WCBasePersonAccessInfoLogicComponents
     */
    private static WCBasePersonAccessInfoLogicComponents instance;
    /**
     * WCBasePersonAccessInfoManager
     */
    @Autowired
    private WCBasePersonAccessInfoManager wcBasePersonAccessInfoManager;

    public static WCBasePersonAccessInfoLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCBasePersonAccessInfoLogicComponents.instance = this;
    }

    /**
     * 取得限制基本部門成員 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getLimitBaseAccessViewUser(Integer userSid) {
        return wcBasePersonAccessInfoManager.getLimitBaseAccessViewUser(userSid);
    }

    /**
     * 取得增加基本部門成員 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getOtherBaseAccessViewUser(Integer userSid) {
        return wcBasePersonAccessInfoManager.getOtherBaseAccessViewUser(userSid);
    }

    /**
     * 取得單位成員可閱權限增加(聯集)已設定成員
     *
     * @return
     */
    public List<Integer> getSettingOtherBasePerson() {
        return wcBasePersonAccessInfoManager.getSettingOtherBasePerson();
    }

    /**
     * 儲存限制基本部門成員
     *
     * @param userSid      使用者Sid
     * @param loginUserSid 登入者Sid
     * @param accessUsers  可閱讀的限制基本單位部門成員
     */
    public void saveLimitBaseAccessViewUser(
        Integer userSid, Integer loginUserSid, List<User> accessUsers) {
        WCBasePersonAccessInfo wcBasePersonAccessInfo =
            wcBasePersonAccessInfoManager.getByUserSid(userSid);
        if (wcBasePersonAccessInfo == null) {
            wcBasePersonAccessInfo = new WCBasePersonAccessInfo();
            wcBasePersonAccessInfo.setLoginUserSid(userSid);
        }
        LimitBaseAccessViewPerson limitBaseAccessViewPerson = new LimitBaseAccessViewPerson();
        accessUsers.forEach(
            item -> {
                UserTo dt = new UserTo();
                dt.setSid(item.getSid());
                limitBaseAccessViewPerson.getUserTos().add(dt);
            });
        wcBasePersonAccessInfo.setLimitBaseAccessViewPerson(limitBaseAccessViewPerson);
        if (!Strings.isNullOrEmpty(wcBasePersonAccessInfo.getSid())) {
            wcBasePersonAccessInfoManager.updateWCBasePersonAccessInfo(
                wcBasePersonAccessInfo, loginUserSid);
        } else {
            wcBasePersonAccessInfoManager.createWCBasePersonAccessInfo(
                wcBasePersonAccessInfo, loginUserSid);
        }
    }

    /**
     * 儲存新增基本部門成員
     *
     * @param userSid      使用者Sid
     * @param loginUserSid 登入者Sid
     * @param accessUsers  可閱讀的新增基本單位部門成員
     */
    public void saveOtherBaseAccessViewUser(
        Integer userSid, Integer loginUserSid, List<User> accessUsers) {
        WCBasePersonAccessInfo wcBasePersonAccessInfo =
            wcBasePersonAccessInfoManager.getByUserSid(userSid);
        if (wcBasePersonAccessInfo == null) {
            wcBasePersonAccessInfo = new WCBasePersonAccessInfo();
            wcBasePersonAccessInfo.setLoginUserSid(userSid);
        }
        OtherBaseAccessViewPerson otherBaseAccessViewPerson = new OtherBaseAccessViewPerson();
        accessUsers.forEach(
            item -> {
                UserTo dt = new UserTo();
                dt.setSid(item.getSid());
                otherBaseAccessViewPerson.getUserTos().add(dt);
            });
        wcBasePersonAccessInfo.setOtherBaseAccessViewPerson(otherBaseAccessViewPerson);
        if (!Strings.isNullOrEmpty(wcBasePersonAccessInfo.getSid())) {
            wcBasePersonAccessInfoManager.updateWCBasePersonAccessInfo(
                wcBasePersonAccessInfo, loginUserSid);
        } else {
            wcBasePersonAccessInfoManager.createWCBasePersonAccessInfo(
                wcBasePersonAccessInfo, loginUserSid);
        }
    }
}
