package com.cy.work.connect.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WCTagTempletManager;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.logic.vo.WCTagTempletVo;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.CategoryItemComponent;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.ReorderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 執行項目建議勾選設定 MBean
 *
 * @author jimmy_chou
 */

@Slf4j
@Scope("view")
@ManagedBean
@Controller
public class Setting14Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4294695750253837690L;
    /**
     * 使用別選項 list
     */
    @Getter
    @Setter
    public List<MenuTagVO> menuTags = Lists.newArrayList();
    /**
     * 查詢編輯區域組件
     */
    @Getter
    @Setter
    public Setting14EditComponent editComponent = new Setting14EditComponent();
    /**
     *
     */
    @Getter
    @Setter
    public List<WCTagTempletVo> templetList;
    /**
     *
     */
    @Getter
    @Setter
    public WCTagTempletVo selectedRow;

    @Getter
    public Map<String, String> menuTagMap;
    /**
     *
     */
    @Autowired
    private WkOrgCache orgManager;
    /**
     *
     */
    @Autowired
    private WCTagTempletManager tagTempletManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCMenuTagManager menuTagManager;
    /**
     *
     */
    @Autowired
    private DisplayController displayController;
    /**
     * 工作聯絡單 類別原件
     */
    @Getter
    private CategoryItemComponent categoryItemComponent;

    private Integer loginCompSid;

    private WCTagTempletVo tempSelectedRow;

    @PostConstruct
    public void init() {
        this.loginCompSid = Optional.ofNullable(orgManager.findById(SecurityFacade.getCompanyId()))
                .map(Org::getSid)
                .orElseGet(() -> null);
        this.menuTags = this.menuTagManager.getMutiTagMenuTags(this.loginCompSid);
        this.categoryItemComponent = new CategoryItemComponent();
        this.menuTagMap = Maps.newLinkedHashMap();
        this.displayController.execute("doSearchData()");
    }

    /**
     * 查詢
     */
    public void doSearch() {

        // ====================================
        // 查詢
        // ====================================
        this.templetList = this.tagTempletManager.findByCompSid(this.loginCompSid);

        this.menuTagMap = this.templetList.stream()
                .map(WCTagTempletVo::getMenuTagSid)
                .distinct()
                .collect(
                        LinkedHashMap::new,
                        (map, streamValue) -> map.put(streamValue, String.valueOf(map.size() % 4)),
                        (Map, map2) -> {
                        });
    }

    /**
     * 開啟模版新增視窗
     */
    public void openAddDialog() {
        // 資料初始化
        this.editComponent.init();
        // 設定為新增模式
        this.editComponent.setEditMode("A");
        // 開啟編輯視窗
        displayController.showPfWidgetVar("editDialog");
    }

    /**
     * 開啟模版編輯視窗
     */
    public void openEditDialog() {

        this.editComponent.setTempletVo(new WCTagTempletVo());

        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (this.selectedRow == null) {
            MessagesUtils.showError("找不到所選擇的資料!");
            log.error("openEditDialog：selectedRow 為空！");
            return;
        }

        WCTagTempletVo templetVo = this.tagTempletManager.findBySid(this.selectedRow.getSid());

        if (templetVo == null) {
            MessagesUtils.showError("該筆資料已經被刪除, 請重新整理頁面!");
            log.warn("該筆資料已經被刪除, 請重新整理頁面!");
            return;
        }

        // ====================================
        // 初始化
        // ====================================
        // 資料初始化
        this.editComponent.init();
        // 設定為編輯模式
        this.editComponent.setEditMode("E");
        // 放入編輯資料
        this.editComponent.setTempletVo(templetVo);
        // 開啟編輯視窗
        displayController.showPfWidgetVar("editDialog");
    }

    /**
     *
     */
    public void saveTemplet() {

        if (WkStringUtils.isEmpty(this.editComponent.getTempletVo().getMenuNameVO())) {
            MessagesUtils.showInfo("請輸入單據名稱");
            return;
        }

        if (WkStringUtils.isEmpty(this.editComponent.getTempletVo().getUsename())) {
            MessagesUtils.showInfo("請輸入使用名稱");
            return;
        }
        try {
            // ====================================
            // 儲存
            // ====================================
            this.tagTempletManager.saveSetting(
                    this.editComponent.getTempletVo(), SecurityFacade.getUserSid(), this.loginCompSid);

            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("儲存成功!");
            displayController.hidePfWidgetVar("editDialog");

        } catch (IllegalStateException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            log.warn("儲存時發生錯誤!：{}", e.getMessage());
        } catch (Exception e) {
            String msg = "儲存時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 開啟建議勾選項目設定
     *
     * @param sid
     */
    public void openCategoryItems(Long sid) {
        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (sid == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            log.error("openCategoryItems：selectedRow 為空！");
            return;
        }

        WCTagTempletVo wcTagTempletVo = this.tagTempletManager.findBySid(sid);

        if (wcTagTempletVo == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            return;
        }

        this.tempSelectedRow = wcTagTempletVo;
        this.categoryItemComponent = new CategoryItemComponent();

        try {
            CategoryTagTo ct = new CategoryTagTo();

            if (WkStringUtils.notEmpty(wcTagTempletVo.getDefaultValue())) {
                ct = wcTagTempletVo.getDefaultValue();
            }

            categoryItemComponent.loadItems(
                    wcTagTempletVo.getMenuTagSid(),
                    ct,
                    true,
                    null,
                    loginCompSid,
                    SecurityFacade.getUserSid());

            categoryItemComponent.initLegitimateRange(
                    wcTagTempletVo.getMenuTagSid(), true, null, loginCompSid, SecurityFacade.getUserSid());

            categoryItemComponent.initStartEnd(wcTagTempletVo.getMenuTagSid(), true, null,
                    loginCompSid);

            //避免先前舊資料(已停用)繼續存在default_value裡面
            if (categoryItemComponent.getSelCategoryItems().size() != 0) {
                List<String> unUseExecTags = wcTagManager.findBySidIn(categoryItemComponent.getSelCategoryItems())
                        .stream().filter(x -> Activation.INACTIVE.equals(x.getStatus()))
                        .map(WCTag::getSid)
                        .collect(Collectors.toList());
                categoryItemComponent.getSelCategoryItems().removeAll(unUseExecTags);
            }

        } catch (Exception ex) {
            log.warn("convert String to Json ERROR", ex);
        }

        // 開啟編輯視窗
        displayController.update("defaultValue_dlg_panel_id");
        displayController.showPfWidgetVar("defaultValue_dlg_wv");
    }

    public void saveDefaultValue() {
        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (this.tempSelectedRow == null || this.tempSelectedRow.getSid() == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            log.error("saveDefaultValue：tempSelectedRow 為空！");
            return;
        }

        WCTagTempletVo wcTagTempletVo = this.tagTempletManager.findBySid(
                this.tempSelectedRow.getSid());

        if (wcTagTempletVo == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            return;
        }

        if (wcTagTempletVo.getDefaultValue() != null) {
            if (!wcTagTempletVo.getDefaultValue().getTagSids().equals(categoryItemComponent.getSelCategoryItems())) {
                // 更新者
                wcTagTempletVo.setUpdateDate(new Date());
                // 更新時間
                wcTagTempletVo.setUpdateUser(SecurityFacade.getUserSid());
            }
        }

        // ====================================
        // 塞入建議勾選項目設定
        // ====================================
        wcTagTempletVo.setDefaultValue(
                new CategoryTagTo(this.categoryItemComponent.getSelCategoryItems()));

        try {
            // ====================================
            // 儲存
            // ====================================
            this.tagTempletManager.saveSetting(
                    wcTagTempletVo, SecurityFacade.getUserSid(), this.loginCompSid);

            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("儲存成功!");
            displayController.hidePfWidgetVar("editDialog");

        } catch (IllegalStateException | IllegalArgumentException e) {
            MessagesUtils.showError(e.getMessage());
            log.warn("儲存時發生錯誤!：{}", e.getMessage());
        } catch (Exception e) {
            String msg = "儲存時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }
    }

    /**
     * 刪除模版
     *
     * @param sid
     */
    public void delete(Long sid) {

        // ====================================
        // 取得要編輯的資料
        // ====================================
        if (sid == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            log.error("delete：selectedRow 為空！");
            return;
        }

        WCTagTempletVo wcTagTempletVo = this.tagTempletManager.findBySid(sid);

        if (wcTagTempletVo == null) {
            MessagesUtils.showError("找不到所選擇的資料, 請重新查詢!");
            return;
        }

        // ====================================
        // 刪除
        // ====================================
        try {
            this.tagTempletManager.deleteTemplet(wcTagTempletVo.getSid());
            // ====================================
            // 畫面操作
            // ====================================
            MessagesUtils.showInfo("刪除成功!");

        } catch (Exception e) {
            String msg = "刪除時發生錯誤!" + e.getMessage();
            MessagesUtils.showError(msg);
            log.error(msg, e);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 異動排序
     *
     * @param event
     */
    public void sort(ReorderEvent event) {

        // ====================================
        // 更新排序序號
        // ====================================
        log.info("更新顯示排序 Row MovedFrom: " + event.getFromIndex() + ", To:" + event.getToIndex());

        try {
            // 更新排序序號
            this.tagTempletManager.updateSortSeqByListIndex(templetList);

        } catch (Exception e) {
            String msg = "排序資料時發生錯誤!";
            log.error(msg, e);
            MessagesUtils.showError(msg);
        }

        // ====================================
        // 刷新查詢結果
        // ====================================
        try {

            this.doSearch();
            displayController.update("id_queryResult");
        } catch (Exception e) {
            log.error("查詢失敗!", e);
        }
    }

    /**
     * 編輯區域組件
     *
     * @author allen
     */
    public class Setting14EditComponent implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 1436519708457616560L;

        /**
         * 編輯類型 (新增/編輯)
         */
        @Getter
        @Setter
        private String editMode;

        /**
         * 模版資料
         */
        @Getter
        @Setter
        private WCTagTempletVo templetVo = new WCTagTempletVo();

        @Getter
        private List<SelectItem> useNameItems;

        /**
         * 資料初始化
         */
        public void init() {
            this.templetVo = new WCTagTempletVo();
            this.useNameItems = tagTempletManager.findDistinctUseNameByCompSid(loginCompSid);
        }
    }
}
