/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.search;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.enums.MemoTransType;
import com.cy.work.connect.vo.enums.WCReadStatus;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.MemoColumn;
import com.cy.work.connect.web.listener.CategoryTreeCallBack;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.TableUpDownListener;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.logic.components.search.MemoSearchLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.TableUpDownBean;
import com.cy.work.connect.web.view.components.CategoryTreeComponent;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.components.memo.MemoLogicComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkReportSidTo;
import com.cy.work.connect.web.view.vo.column.search.MemoColumnVO;
import com.cy.work.connect.web.view.vo.search.MemoVO;
import com.cy.work.connect.web.view.vo.search.query.MemoQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 備忘錄查詢
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Search6Bean implements Serializable, TableUpDownListener {

    /**
     *
     */
    private static final long serialVersionUID = -8811976173436943523L;

    @Getter
    /** dataTable Id */
    private final String dataTableID = "dataTableWorkReport";

    @Getter
    /** dataTable widgetVar */
    private final String dataTableWv = "dataTableWorkReportWv";
    private final transient Comparator<Org> orgComparator = new Comparator<Org>() {
        @Override
        public int compare(Org obj1, Org obj2) {
            final String seq1 = obj1.getName();
            final String seq2 = obj2.getName();
            return seq1.compareTo(seq2);
        }
    };
    @Autowired
    private TableUpDownBean tableUpDownBean;
    @Autowired
    private MemoSearchLogicComponent searchLogicComponent;
    @Autowired
    private WCMemoCategoryLogicComponent wcMemoCategoryLogicComponent;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;
    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;
    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;
    @Getter
    /** view 顯示錯誤訊息 */
    private String errorMessage;
    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = -1897093619081769804L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    };
    /**
     * 訊息呼叫
     */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /** */
        private static final long serialVersionUID = -5110875694100710406L;

        @Override
        public void showMessage(String message) {
            messageCallBack.showMessage(message);
        }

        @Override
        public void onNodeSelect() {
        }
    };
    @Setter
    @Getter
    /** 是否版面切換(顯示Frame畫面) */
    private boolean showFrame = false;
    @Getter
    /** 列表高度 */
    private String dtScrollHeight = "400";
    @Getter
    /** dataTable 欄位資訊 */
    private MemoColumnVO columnVO;
    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 6347075985175686866L;

        @Override
        public void reload() {
            columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
            DisplayController.getInstance().update(dataTableID);
        }
    };
    @Setter
    @Getter
    /** 列表資料 */
    private List<MemoVO> allVos;
    @Setter
    @Getter
    /** 列表選取資料 */
    private MemoVO selVo;
    /**
     * 暫存資料
     */
    private List<MemoVO> tempVOs;
    /**
     * 暫存資料
     */
    private String tempSelSid;
    @Getter
    /** frame 連結路徑 */
    private String iframeUrl = "";
    @Getter
    /** 查詢物件 */
    private MemoQuery query;
    @Autowired
    private MemoLogicComponent memoLogicComponent;
    @Getter
    private CategoryTreeComponent categoryTreeComponent;
    private String selMemoSid;
    @Getter
    /** 執行狀態 選項 */
    private List<SelectItem> createDepItems;
    @Getter
    private List<SelectItem> transWorkConnectItems;
    @Getter
    private List<SelectItem> readStatusItems;
    @Getter
    private CustomColumnComponent customColumn;

    @PostConstruct
    public void init() {
        this.initLogin();
        if (!this.isMemoViewPermission()) {
            this.redirectByNoPermission();
        }
        this.initComponents();
        this.clear();
    }

    public Boolean isMemoViewPermission() { return wcMemoCategoryLogicComponent.isMemoViewPermission(); }

    /**
     * 導向無權限畫面
     */
    private void redirectByNoPermission() {
        try {
            Faces.getExternalContext().redirect("../error/illegal_read_memo.xhtml");
            Faces.getContext().responseComplete();
        } catch (Exception ex) {
            log.warn("導向讀取失敗頁面失敗..." + ex.getMessage());
        }
    }

    /**
     * 初始化 登入者資訊
     */
    private void initLogin() {
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
    }

    /**
     * 初始化 元件
     */
    private void initComponents() {
        columnVO = searchLogicComponent.getColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(
                WCReportCustomColumnUrlType.MEMO_SEARCH,
                Arrays.asList(MemoColumn.values()),
                columnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);
        createDepItems = Lists.newArrayList();
        transWorkConnectItems = Lists.newArrayList();
        transWorkConnectItems.add(
                new SelectItem(MemoTransType.UNTRANSED, MemoTransType.UNTRANSED.getValue()));
        transWorkConnectItems.add(
                new SelectItem(MemoTransType.TRANSED, MemoTransType.TRANSED.getValue()));

        readStatusItems = Lists.newArrayList();
        readStatusItems.add(
                new SelectItem(WCReadStatus.UNREAD.name(), WCReadStatus.UNREAD.getVal()));
        readStatusItems.add(
                new SelectItem(WCReadStatus.HASREAD.name(), WCReadStatus.HASREAD.getVal()));
        readStatusItems.add(
                new SelectItem(WCReadStatus.WAIT_READ.name(), WCReadStatus.WAIT_READ.getVal()));
        readStatusItems.add(
                new SelectItem(WCReadStatus.NEEDREAD.name(), WCReadStatus.NEEDREAD.getVal()));

        query = new MemoQuery();
        this.categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        this.categoryTreeComponent.bulidTree(
                categoryTagLogicComponent.getCategoryTags(
                        compViewVo.getSid(), userViewVO.getSid()));
    }

    /**
     * 清除並查詢
     */
    public void clear() {
        this.query.init();
        this.doSearch();
    }

    /**
     * 執行查詢
     */
    public void doSearch() {
        try {
            allVos = this.search("");
            if (allVos.size() > 0) {
                DisplayController.getInstance()
                        .execute("selectDataTablePage('" + dataTableWv + "',0);");
            }
            if (createDepItems == null || createDepItems.isEmpty()) {
                List<Integer> all = searchLogicComponent.searchCreateDepSid();
                createDepItems.clear();
                createDepItems.add(new SelectItem("", "全部"));
                List<Org> orgs = Lists.newArrayList();
                all.forEach(item -> {
                    Org dep = orgManager.findBySid(item);
                    if (!orgs.contains(dep)) {
                        orgs.add(dep);
                    }
                });
                try {
                    Collections.sort(orgs, orgComparator);
                } catch (Exception e) {
                    log.warn("search ERROR", e);
                }
                orgs.forEach(item -> {
                    createDepItems.add(new SelectItem(String.valueOf(item.getSid()), item.getName()));
                });
            }

        } catch (Exception e) {
            log.warn("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行查詢
     *
     * @param sid
     * @return
     * @throws UserMessageException
     */
    private List<MemoVO> search(String sid) throws UserMessageException {
        return searchLogicComponent.search(query, sid);
    }

    /**
     * 回到列表查詢
     */
    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.warn("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    @Override
    public void openerByBtnUp() {
        try {
            if (!allVos.isEmpty()) {
                this.toUp();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
                settingReaded();
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    private void settingReaded() {
        allVos.forEach(
                item -> {
                    if (this.selVo.getSid().equals(item.getSid())) {
                        this.selVo.replaceReadStatus(WCReadStatus.HASREAD);
                        item.replaceReadStatus(WCReadStatus.HASREAD);
                    }
                });
    }

    /**
     * 執行上一筆
     */
    private void toUp() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index > 0) {
                    selVo = tempVOs.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index > 0) {
                    selVo = allVos.get(index - 1);
                } else {
                    selVo = allVos.get(0);
                }
            }
        } catch (Exception e) {
            log.warn("toUp", e);
            this.clearTemp();
            if (!allVos.isEmpty()) {
                selVo = allVos.get(0);
            }
        }
    }

    /**
     * 執行下一筆
     */
    @Override
    public void openerByBtnDown() {
        try {
            if (!allVos.isEmpty()) {
                this.toDown();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
                settingReaded();
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("工作聯絡單查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行下一筆
     */
    private void toDown() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index >= 0) {
                    selVo = tempVOs.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
                this.clearTemp();
            } else {
                int index = allVos.indexOf(selVo);
                if (index >= 0) {
                    selVo = allVos.get(index + 1);
                } else {
                    selVo = allVos.get(allVos.size() - 1);
                }
            }
        } catch (Exception e) {
            log.warn("toDown", e);
            this.clearTemp();
            if (allVos.size() > 0) {
                selVo = allVos.get(allVos.size() - 1);
            }
        }
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNextAndReaded() {
        this.settingPreviousAndNext();
        tableUpDownBean.setSession_now_sid(selVo.getSid());
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNext() {
        int index = allVos.indexOf(selVo);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(allVos.get(index - 1).getSid());
        }
        if (index == allVos.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(allVos.get(index + 1).getSid());
        }
    }

    /**
     * 判斷是否有暫存資訊
     */
    private boolean checkTempVOs() {
        return tempVOs != null && !tempVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid);
    }

    /**
     * 開啟分頁
     *
     * @param sid
     */
    public void btnOpenUrl(String sid) {
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp4");
            settingReaded();
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void clickTrans(String sid) {
        try {
            this.selMemoSid = sid;
            categoryTreeComponent.bulidTree(
                    categoryTagLogicComponent.getCategoryTags(
                            compViewVo.getSid(), userViewVO.getSid()));
            DisplayController.getInstance().update("transDlgPanel");
            DisplayController.getInstance().showPfWidgetVar("transDlg");
        } catch (Exception e) {
            log.warn("執行備忘錄轉工作聯絡單按鈕動作 Error！！", e);
            messageCallBack.showMessage("執行備忘錄轉工作聯絡單按鈕動作 Error！！");
        }
    }

    public void clickToTransWorkConnect() {
        try {
            String categorySid = categoryTreeComponent.getSelectedNodeSid();
            if (Strings.isNullOrEmpty(categorySid)) {
                messageCallBack.showMessage("尚未選取資料!!");
                return;
            }
            if (memoLogicComponent.isTraned(selMemoSid)) {
                DisplayController.getInstance().showPfWidgetVar("confimDlgTrans");
            } else {
                doClickToTransWorkConnect();
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("clickToTransWorkConnect ERROR：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("clickToTransWorkConnect ERROR", e);
        }
    }

    public void doClickToTransWorkConnect() {
        try {
            String categorySid = categoryTreeComponent.getSelectedNodeSid();
            if (Strings.isNullOrEmpty(categorySid)) {
                messageCallBack.showMessage("尚未選取資料!!");
                return;
            }
            DisplayController.getInstance()
                    .execute(
                            "window.open('"
                                    + "../worp/worp1.xhtml?categorySid="
                                    + categorySid
                                    + "&memoSid="
                                    + selMemoSid
                                    + "', '_blank');");
            DisplayController.getInstance().hidePfWidgetVar("confimDlgTrans");
            DisplayController.getInstance().hidePfWidgetVar("transDlg");
        } catch (Exception e) {
            log.warn("clickToTransWorkConnect ERROR", e);
        }
    }

    /**
     * 載入滿版
     */
    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            settingReaded();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp4.xhtml?memoSid=" + selVo.getSid();
            tableUpDownBean.setWorp_path("worp4");
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 變更 selVo
     */
    private void settintSelVo(String sid) {
        MemoVO sel = new MemoVO(sid);
        int index = allVos.indexOf(sel);
        if (index > 0) {
            selVo = allVos.get(index);
        } else {
            selVo = allVos.get(0);
        }
    }

    /**
     * ?????
     */
    public void reloadDataByUpdate() {
        this.doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByFavorite() {
    }

    /**
     * ?????
     */
    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.warn("doReloadInfo", ex);
        }
    }

    /**
     * ?????
     *
     * @param sid
     */
    private void updateInfoToDataTable(String sid) {
        try {
            List<MemoVO> result = search(sid);
            if (result == null || result.isEmpty()) {
                tempVOs = Lists.newArrayList();
                if (allVos != null && !allVos.isEmpty()) {
                    allVos.forEach(
                            item -> {
                                tempVOs.add(item);
                            });
                    allVos.remove(new MemoVO(sid));
                    tempSelSid = sid;
                }
            } else {
                MemoVO updateDeail = result.get(0);
                allVos.forEach(
                        item -> {
                            if (item.getSid().equals(updateDeail.getSid())) {
                                item.replaceValue(updateDeail);
                            }
                        });
            }
            DisplayController.getInstance().update(dataTableID);
        } catch (Exception e) {
            log.warn("updateInfoToDataTable Error", e);
        }
    }

    /**
     * 清除暫存
     */
    private void clearTemp() {
        tempSelSid = "";
        tempVOs = null;
    }

    /**
     * 取得暫存索引
     */
    private int getTempIndex() { return tempVOs.indexOf(new MemoVO(tempSelSid)); }

    /**
     * 更新dtScrollHeight
     */
    public void updateDtScrollHeight() {
        String height = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("dtScrollHeight");
        if (!Strings.isNullOrEmpty(height)) {
            dtScrollHeight = height;
        }
    }
}
