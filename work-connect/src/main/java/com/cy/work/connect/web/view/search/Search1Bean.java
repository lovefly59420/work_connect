/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.search;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.WaitAssignLogic;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCMenuTagManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.column.search.WaitWorkListColumn;
import com.cy.work.connect.web.listener.DataTableReLoadCallBack;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.listener.TableUpDownListener;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.TableUpDownBean;
import com.cy.work.connect.web.view.components.CustomColumnComponent;
import com.cy.work.connect.web.view.components.ExecSendPersonComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkReportSidTo;
import com.cy.work.connect.web.view.vo.column.search.WaitWorkListColumnVO;
import com.cy.work.connect.web.view.vo.search.WaitWorkListVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.joda.time.DateTime;
import org.omnifaces.util.Faces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.FilterMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;

/**
 * 待派工清單
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class Search1Bean implements Serializable, TableUpDownListener {

    /**
     *
     */
    private static final long serialVersionUID = 2368037749610993912L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WCMenuTagManager wcMenuTagManager;
    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    // ========================================================================
    //
    // ========================================================================
    @Getter
    /** dataTable Id */
    private final String dataTableID = "dataTableWorkReport";

    @Getter
    /** dataTable widgetVar */
    private final String dataTableWv = "dataTableWorkReportWv";

    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.WAIT_WORK_LIST;

    @Autowired
    private TableUpDownBean tableUpDownBean;

    @Getter
    /** 登入者 */
    private UserViewVO userViewVO;

    @Getter
    /** 登入單位 */
    private OrgViewVo depViewVo;

    @Getter
    /** 登入公司 */
    private OrgViewVo compViewVo;

    @Getter
    /** view 顯示錯誤訊息 */
    private String errorMessage;

    private final MessageCallBack messageCallBack = new MessageCallBack() {
        /** */
        private static final long serialVersionUID = 4572126998857590709L;

        @Override
        public void showMessage(String m) {
            errorMessage = m;
            DisplayController.getInstance().update("confimDlgTemplate");
            DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
        }
    };

    @Setter
    @Getter
    /** 是否版面切換(顯示Frame畫面) */
    private boolean showFrame = false;

    @Getter
    /** dataTable 欄位資訊 */
    private WaitWorkListColumnVO columnVO;

    private final DataTableReLoadCallBack dataTableReLoadCallBack = new DataTableReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 5661742613447899803L;

        @Override
        public void reload() {
            try {
                columnVO = getColumnSetting(userViewVO.getSid());
                DisplayController.getInstance().update(dataTableID);
                DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
            } catch (Exception e) {
                log.warn("reload ERROR", e);
            }
        }
    };

    @Setter
    @Getter
    /** 列表資料 */
    private List<WaitWorkListVO> allVos;
    /**
     * 列表資料
     */
    @Setter
    @Getter
    private List<WaitWorkListVO> filterDatas;

    @Setter
    @Getter
    /** 列表選取資料 */
    private WaitWorkListVO selVo;
    /**
     * 暫存資料
     */
    private List<WaitWorkListVO> tempVOs;
    /**
     * 暫存資料
     */
    private String tempSelSid;

    @Getter
    /** frame 連結路徑 */
    private String iframeUrl = "";

    @Getter
    private ExecSendPersonComponent execSendPersonComponent;

    @Getter
    /** 單據名稱 選項 */
    private List<SelectItem> menuTagItems;
    /**
     * 執行分派後,CallBack
     */
    private final ReLoadCallBack execSendPersonReLoadCallBack = new ReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 7240049020587654258L;

        @Override
        public void onReload() {
            try {
                allVos = search("");
                DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
            } catch (Exception e) {
                MessagesUtils.showError(e.getMessage());
                log.warn("onReload ERROR", e);
                return;
            }
        }
    };

    @Getter
    private CustomColumnComponent customColumn;

    @PostConstruct
    public void init() {
        this.initLogin();
        this.initComponents();
        filterDatas = Lists.newArrayList();
        this.doSearch();
        this.execSendPersonComponent = new ExecSendPersonComponent(
                compViewVo.getSid(),
                userViewVO.getSid(),
                execSendPersonReLoadCallBack);
    }

    /**
     * 初始化 登入者資訊
     */
    private void initLogin() {
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
    }

    /**
     * 初始化 元件
     */
    private void initComponents() {
        columnVO = this.getColumnSetting(userViewVO.getSid());
        customColumn = new CustomColumnComponent(
                WCReportCustomColumnUrlType.WAIT_WORK_LIST,
                Arrays.asList(WaitWorkListColumn.values()),
                columnVO.getPageCount(),
                userViewVO.getSid(),
                messageCallBack,
                dataTableReLoadCallBack);
    }

    /**
     * 執行查詢
     */
    public void doSearch() {
        try {
            allVos = this.search("");
            if (allVos.size() > 0) {
                DisplayController.getInstance()
                        .execute("selectDataTablePage('" + dataTableWv + "',0);");
            }

        } catch (Exception e) {
            log.warn("doSearch", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行查詢
     *
     * @param sid
     * @return
     */
    private List<WaitWorkListVO> search(String sid) {
        List<WaitWorkListVO> result = this.searchx(sid);
        menuTagItems = result.stream()
                .map(each -> each.getMenuTagName())
                .distinct()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());
        DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        return result;
    }

    /**
     * 回到列表查詢
     */
    public void closeIframe() {
        showFrame = false;
        try {
            iframeUrl = "";
        } catch (Exception e) {
            log.warn("closeIframe", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    @Override
    public void openerByBtnUp() {
        try {
            if (!allVos.isEmpty()) {
                this.toUp();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            log.warn("工作聯絡單進行上一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    public void openSendDialog(String sid) {
        try {
            WaitWorkListVO se = new WaitWorkListVO(sid);
            int index = allVos.indexOf(se);
            this.selVo = allVos.get(index);
            DisplayController.getInstance().update(dataTableID);
            execSendPersonComponent.loadData(sid);
            DisplayController.getInstance().update("dlg_sendExecPerson_view");
            DisplayController.getInstance().showPfWidgetVar("dlg_sendExecPerson");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("openSendDialog Error：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("openSendDialog Error", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行上一筆
     */
    private void toUp() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index > 0) {
                    selVo = tempVOs.get(index - 1);
                } else {
                    selVo = filterDatas.get(0);
                }
                this.clearTemp();
            } else {
                int index = filterDatas.indexOf(selVo);
                if (index > 0) {
                    selVo = filterDatas.get(index - 1);
                } else {
                    selVo = filterDatas.get(0);
                }
            }
        } catch (Exception e) {
            log.warn("toUp", e);
            this.clearTemp();
            if (!allVos.isEmpty()) {
                selVo = allVos.get(0);
            }
        }
    }

    /**
     * 執行下一筆
     */
    @Override
    public void openerByBtnDown() {
        try {
            if (!allVos.isEmpty()) {
                this.toDown();
                if (selVo != null) {
                    this.settingPreviousAndNextAndReaded();
                }
            }
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            log.warn("工作聯絡單查詢進行下一筆動作-失敗", e);
        } finally {
            tableUpDownBean.setSessionSettingOver();
        }
    }

    /**
     * 執行下一筆
     */
    private void toDown() {
        try {
            if (this.checkTempVOs()) {
                int index = this.getTempIndex();
                if (index >= 0) {
                    selVo = tempVOs.get(index + 1);
                } else {
                    selVo = filterDatas.get(filterDatas.size() - 1);
                }
                this.clearTemp();
            } else {
                int index = filterDatas.indexOf(selVo);
                if (index >= 0) {
                    selVo = filterDatas.get(index + 1);
                } else {
                    selVo = filterDatas.get(filterDatas.size() - 1);
                }
            }
        } catch (Exception e) {
            log.warn("toDown", e);
            this.clearTemp();
            if (allVos.size() > 0) {
                selVo = allVos.get(allVos.size() - 1);
            }
        }
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNextAndReaded() {
        this.settingPreviousAndNext();
        tableUpDownBean.setSession_now_sid(selVo.getWcSid());
    }

    /**
     * 傳遞上下筆資訊
     */
    private void settingPreviousAndNext() {
        int index = filterDatas.indexOf(selVo);
        if (index <= 0) {
            tableUpDownBean.setSession_previous_sid("");
        } else {
            tableUpDownBean.setSession_previous_sid(filterDatas.get(index - 1).getSid());
        }
        if (index == filterDatas.size() - 1) {
            tableUpDownBean.setSession_next_sid("");
        } else {
            tableUpDownBean.setSession_next_sid(filterDatas.get(index + 1).getSid());
        }
    }

    /**
     * 判斷是否有暫存資訊
     */
    private boolean checkTempVOs() {
        return tempVOs != null && !tempVOs.isEmpty() && !Strings.isNullOrEmpty(tempSelSid);
    }

    /**
     * 開啟分頁
     *
     * @param sid
     */
    public void btnOpenUrl(String sid) {
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setWorp_path("worp_full");
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            log.warn("btnOpenUrl", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入滿版
     */
    public void btnOpenFrame(String sid) {
        showFrame = true;
        try {
            this.settintSelVo(sid);
            this.settingPreviousAndNext();
            tableUpDownBean.setSession_show_home("1");
            iframeUrl = "../worp/worp_iframe.xhtml?wrcId=" + selVo.getWcSid();
            tableUpDownBean.setWorp_path("worp_iframe");
        } catch (Exception e) {
            log.warn("btnOpenFrame", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 變更 selVo
     */
    private void settintSelVo(String sid) {
        WaitWorkListVO sel = new WaitWorkListVO(sid);
        int index = allVos.indexOf(sel);
        if (index > 0) {
            selVo = allVos.get(index);
        } else {
            selVo = allVos.get(0);
        }
    }

    /**
     * ?????
     */
    public void reloadDataByUpdate() {
        this.doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByEditContent() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByInvalid() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByTrace() {
        doReloadInfo();
    }

    /**
     * ?????
     */
    @Override
    public void openerByFavorite() {
    }

    /**
     * ?????
     */
    private void doReloadInfo() {
        String value = Faces.getRequestParameterMap().get("wrc_sid");
        List<WorkReportSidTo> workReportSidTos;
        try {
            workReportSidTos = jsonUtils.fromJsonToList(value, WorkReportSidTo.class);
            updateInfoToDataTable(workReportSidTos.get(0).getSid());
        } catch (Exception ex) {
            log.warn("doReloadInfo", ex);
        }
        // try {
        // allVos = this.search("");
        // DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        // } catch (Exception e) {
        // log.error("doReloadInfo", e);
        // messageCallBack.showMessage(e.getMessage());
        // }
    }

    /**
     * ?????
     *
     * @param sid
     */
    private void updateInfoToDataTable(String sid) {
        try {
            List<WaitWorkListVO> result = search(sid);
            if (result == null || result.isEmpty()) {
                tempVOs = Lists.newArrayList();
                if (allVos != null && !allVos.isEmpty()) {
                    allVos.forEach(
                            item -> {
                                tempVOs.add(item);
                            });
                    allVos.remove(new WaitWorkListVO(sid));
                    tempSelSid = sid;
                }
            } else {
                WaitWorkListVO updateDeail = result.get(0);
                allVos.forEach(
                        item -> {
                            if (item.getWcSid().equals(updateDeail.getWcSid())) {
                                item.replaceValue(updateDeail);
                            }
                        });
            }
            DisplayController.getInstance().update(dataTableID);
            DisplayController.getInstance().execute("PF('" + dataTableWv + "').filter();");
        } catch (Exception e) {
            log.warn("updateInfoToDataTable Error", e);
        }
    }

    public void doSort() {
    }

    public void sortDataTable() {
        try {
            List<WaitWorkListVO> temp = Lists.newArrayList();
            allVos.forEach(
                    item -> {
                        if (filterDatas.contains(item)) {
                            temp.add(item);
                        }
                    });
            filterDatas.clear();
            filterDatas.addAll(temp);
        } catch (Exception e) {
            log.warn("sortDataTable ERROR", e);
        }
    }

    public Map<String, FilterMeta> onFilter(AjaxBehaviorEvent event) {
        DataTable table = (DataTable) event.getSource();
        Map<String, FilterMeta> filters = table.getFilterBy();
        try {
            filterDatas.clear();
            allVos.forEach(
                    item -> {
                        if (filters.get("requireDepName") != null) {
                            String filterValue = String.valueOf(filters.get("requireDepName"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getRequireDepName().toLowerCase()
                                            .contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        if (filters.get("requireUserName") != null) {
                            String filterValue = String.valueOf(filters.get("requireUserName"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getRequireUserName().toLowerCase()
                                            .contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        if (filters.get("menuTagName") != null) {
                            String filterValue = String.valueOf(filters.get("menuTagName"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getMenuTagName().equalsIgnoreCase(filterValue)) {
                                return;
                            }
                        }
                        if (filters.get("theme") != null) {
                            String filterValue = String.valueOf(filters.get("theme"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getTheme().toLowerCase().contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        if (filters.get("wcNo") != null) {
                            String filterValue = String.valueOf(filters.get("wcNo"));
                            if (!Strings.isNullOrEmpty(filterValue)
                                    && !item.getWcNo().toLowerCase().contains(filterValue.toLowerCase())) {
                                return;
                            }
                        }
                        filterDatas.add(item);
                    });
        } catch (Exception e) {
            log.warn("onFilter ERROR", e);
        }
        return filters;
    }

    /**
     * 清除暫存
     */
    private void clearTemp() {
        tempSelSid = "";
        tempVOs = null;
    }

    /**
     * 取得暫存索引
     */
    private int getTempIndex() { return tempVOs.indexOf(new WaitWorkListVO(tempSelSid)); }

    private List<WaitWorkListVO> searchx(String wcSid) {

        // ====================================
        // 取得登入者可以分派的執行單位檔 SID
        // ====================================
        // 查詢
        Set<String> canAssignWcExecDepSids = WaitAssignLogic.getInstance().findUserCanAssignWcExecDepSids(
                SecurityFacade.getUserSid());

        // 沒有任何一筆時 return
        if (WkStringUtils.isEmpty(canAssignWcExecDepSids)) {
            return Lists.newArrayList();
        }
        // 組成 SQL 用的 string
        String canAssignWcExecDepSidsStr = canAssignWcExecDepSids.stream()
                .collect(Collectors.joining("', '", "'", "'"));

        // ====================================
        // 建立查詢 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.require_establish_dt, ");
        sql.append("       wc.dep_sid, ");
        sql.append("       wc.create_usr, ");
        sql.append("       wc.menuTag_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       execDep.dep_sid AS execDepSid, ");
        sql.append("       execDep.wc_exec_dep_setting_sid ");
        sql.append("FROM   wc_master wc ");
        sql.append("       INNER JOIN wc_exec_dep execDep ");
        sql.append("               ON wc.wc_sid = execDep.wc_sid ");
        sql.append("WHERE  execDep.wc_exec_dep_sid IN ( " + canAssignWcExecDepSidsStr + " ) ");

        // 有指定聯絡單 sid
        if (WkStringUtils.notEmpty(wcSid)) {
            sql.append("       AND wc.wc_sid = '" + wcSid + "' ");
        }

        // ====================================
        // 查詢
        // ====================================
        List<Map<String, Object>> dbRowDataMapList = this.jdbc.queryForList(sql.toString());

        if (WkStringUtils.isEmpty(dbRowDataMapList)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 封裝
        // ====================================
        Map<String, WaitWorkListVO> results = Maps.newHashMap();
        for (Map<String, Object> each : dbRowDataMapList) {

            String currWcSid = (String) each.get("wc_sid");

            WaitWorkListVO waitWorkListVO = results.get(currWcSid);

            if (waitWorkListVO == null) {
                waitWorkListVO = new WaitWorkListVO(currWcSid);
                waitWorkListVO.setWcNo((String) each.get("wc_no"));
                waitWorkListVO.setCreateDate((Date) each.get("create_dt"));
                waitWorkListVO.setRequireEstablishDate(new DateTime(each.get("require_establish_dt")).toString("yyyy/MM/dd"));
                waitWorkListVO.setRequireDepName(WkOrgUtils.findNameBySid((Integer) each.get("dep_sid")));
                waitWorkListVO.setRequireUserName(WkUserUtils.findNameBySid((Integer) each.get("create_usr")));
                waitWorkListVO.setMenuTagName(this.wcMenuTagManager.findMenuTagName(each.get("menuTag_sid") + ""));
                waitWorkListVO.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
            }

            // 取得項目名稱
            String wc_exec_dep_setting_sid = each.get("wc_exec_dep_setting_sid") + "";

            if (WkStringUtils.notEmpty(wc_exec_dep_setting_sid)) {
                //由快取中取得設定檔
                WCExecDepSetting wcExecDepSetting = this.wcExecDepSettingManager.findBySidFromCache(wc_exec_dep_setting_sid);
                if (wcExecDepSetting != null) {
                    WCTag wcTag = wcTagManager.getWCTagBySid(wcExecDepSetting.getWc_tag_sid());
                    waitWorkListVO.bulidExecTag(wcTag);
                }
            }
            results.put(currWcSid, waitWorkListVO);
        }

        // ====================================
        // 以建立日期排序
        // ====================================
        Comparator<WaitWorkListVO> comparator = Comparator.comparing(WaitWorkListVO::getCreateDate);

        return results.entrySet().stream()
                .map(each -> each.getValue())
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public WaitWorkListColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        WaitWorkListColumnVO vo = new WaitWorkListColumnVO();
        if (columDB == null) {
            vo.setIndex(
                    customColumnLogicComponent.createDefaultColumnDetail(WaitWorkListColumn.INDEX));
            vo.setRequireEstablishDate(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkListColumn.REQUIRE_ESTABLISH_DATE));
            vo.setRequireDepName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkListColumn.REQUIRE_DEP_NAME));
            vo.setMenuTagName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkListColumn.MENUTAG_NAME));
            vo.setRequireUserName(
                    customColumnLogicComponent.createDefaultColumnDetail(
                            WaitWorkListColumn.REQUIRE_USER_NAME));
            vo.setTheme(
                    customColumnLogicComponent.createDefaultColumnDetail(WaitWorkListColumn.THEME));
            vo.setWcNo(
                    customColumnLogicComponent.createDefaultColumnDetail(WaitWorkListColumn.WC_NO));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.INDEX, columDB.getInfo()));
            vo.setRequireEstablishDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.REQUIRE_ESTABLISH_DATE, columDB.getInfo()));
            vo.setRequireDepName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.REQUIRE_DEP_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setRequireUserName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.REQUIRE_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.THEME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            WaitWorkListColumn.WC_NO, columDB.getInfo()));
        }
        return vo;
    }
}
