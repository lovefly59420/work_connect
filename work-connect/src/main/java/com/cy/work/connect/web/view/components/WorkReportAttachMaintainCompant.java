/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.omnifaces.util.Faces;
import org.primefaces.model.StreamedContent;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.AttachmentService;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.logic.components.WCAttachmentLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportAttachMaintainCompant implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3697807389275847244L;
    private final Integer userSid;
    private final TabCallBack workReportTabCallBack;
    @Getter
    private List<AttachmentVO> attachmentVOs;
    @Getter
    private List<AttachmentVO> selectAttachmentVOs;
    private String wc_Sid;
    @SuppressWarnings("unused")
    private String wc_No;
    @Getter
    private AttachmentVO selDelAtt;
    @Getter
    private boolean showAttTab = false;

    public WorkReportAttachMaintainCompant(
            Integer userSid, TabCallBack workReportTabCallBack) {
        this.userSid = userSid;
        this.selDelAtt = new AttachmentVO("", "", "", "", false, "");
        this.workReportTabCallBack = workReportTabCallBack;
    }

    public void loadData(String wc_Sid, String wc_No) {
        this.wc_Sid = wc_Sid;
        this.wc_No = wc_No;
        selectAttachmentVOs = Lists.newArrayList();
        attachmentVOs = Lists.newArrayList();
        loadData();
    }

    public void changeToEditMode(String sid) {
        attachmentVOs.forEach(
                item -> {
                    if (item.getAttSid().equals(sid)) {
                        item.setEditMode(true);
                    }
                });
    }

    public void loadData() {
        // ====================================
        // 取得管理部門
        // ====================================
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(userSid);

        // ====================================
        // 查詢所有附加檔案
        // ====================================
        List<AttachmentVO> dbAttachmentVOs = WCAttachmentLogicComponents.getInstance().getAttachmentsByWCSid(wc_Sid);
        for (AttachmentVO dbAttachmentVO : dbAttachmentVOs) {
            dbAttachmentVO.setChecked(false);
            dbAttachmentVO.setShowEditBtn(this.isCanEdit(dbAttachmentVO, managerDepSids));
            dbAttachmentVO.setEditMode(false);
        }

        // 根據檔案上傳時間排序
        dbAttachmentVOs = dbAttachmentVOs.stream()
                .sorted(Comparator.comparingLong(t -> convertTimeToLong(t.getParamCreateTime())))
                .collect(Collectors.toList());

        // 置換
        this.attachmentVOs.clear();
        this.attachmentVOs = dbAttachmentVOs;

        // 是否顯示附加檔案tab
        this.showAttTab = attachmentVOs.size() > 0;
    }

    /**
     * 判斷是否可編輯附加檔案
     * 
     * @param attachmentVO
     * @param managerDepSids
     * @return
     */
    private boolean isCanEdit(AttachmentVO attachmentVO, Set<Integer> managerDepSids) {
        if (WkCommonUtils.compareByStr(attachmentVO.getCreateUserSId(), SecurityFacade.getUserSid())) {
            return true;
        }
        if (WkStringUtils.notEmpty(managerDepSids) && managerDepSids.contains(attachmentVO.getDepSid())) {
            return true;
        }
        return false;
    }

    public void loadDeleteAtt(String sid) {
        WCMaster wcMaster = WCMasterManager.getInstance().findBySid(wc_Sid);
        List<WCTag> wcTags = WCTagManager.getInstance().findBySidIn(wcMaster.getCategoryTagTo().getTagSids());
        Optional<WCTag> wcTagNeedUploadFileAny = wcTags.stream().filter(x -> x.getUploadFile() == true).findAny();
        if (wcTagNeedUploadFileAny.isPresent()) {
            if (attachmentVOs.size() <= 1) {
                MessagesUtils.showWarn("請至少保留一個附件！");
                return;
            }
        }
        attachmentVOs.forEach(
                item -> {
                    if (item.getAttSid().equals(sid)) {
                        this.selDelAtt = item;
                    }
                });
        DisplayController.getInstance().showPfWidgetVar("attach_del_confirm_wv");
    }

    public void deleteAtt() {
        if (selDelAtt == null || Strings.isNullOrEmpty(selDelAtt.getAttSid())) {
            return;
        }
        List<AttachmentVO> tempAv = Lists.newArrayList(attachmentVOs);
        tempAv.forEach(
                item -> {
                    if (item.getAttSid().equals(selDelAtt.getAttSid())) {
                        try {
                            List<AttachmentVO> attachment = WCAttachmentLogicComponents.getInstance().getAttachmentsByWCSid(wc_Sid);
                            if (attachment.contains(selDelAtt)) {

                                WCAttachmentLogicComponents.getInstance()
                                        .deleteAttachment(item, userSid, wc_Sid);
                                workReportTabCallBack.reloadTraceTab();
                                workReportTabCallBack.reloadAttTab();
                                if (showAttTab) {
                                    workReportTabCallBack.reloadTabView(TabType.Attachment);
                                } else {
                                    workReportTabCallBack.reloadTabView(TabType.Trace);
                                }
                            }
                            attachment = null;
                        } catch (Exception e) {
                            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                            MessagesUtils.showError(errorMessage);
                            log.error(errorMessage, e);
                        }
                    }
                });
    }

    public void saveAttDesc(String sid) {
        attachmentVOs.forEach(
                item -> {
                    if (item.getAttSid().equals(sid)) {
                        try {
                            WCAttachmentLogicComponents.getInstance()
                                    .updateAttachment(sid, item.getAttDesc(), userSid);
                            item.setEditMode(false);
                        } catch (Exception e) {
                            log.warn("saveAttDesc Error :", e);
                        }
                    }
                });
    }

    public void selectAll() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(
                item -> {
                    selectAttachmentVOs.add(item);
                    item.setChecked(true);
                });
    }

    public void changeCheckBox() {
        selectAttachmentVOs.clear();
        attachmentVOs.forEach(
                item -> {
                    if (item.isChecked()) {
                        selectAttachmentVOs.add(item);
                    }
                });
    }

    public StreamedContent downloadFile() throws IOException {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        // 下載單一檔案，預設檔名使用該檔案的原始檔名（但是要經過編碼）
        if (selectAttachmentVOs.size() == 1) {
            return AttachmentUtils.createStreamedContent(selectAttachmentVOs.get(0), "wcMaster");
        }
        // 將多檔案壓縮成單一 zip 檔案再輸出
        StreamedContent streamedContent = AttachmentUtils.createWrappedStreamedContent(selectAttachmentVOs, "wcMaster");
        return streamedContent;
    }

    public String downloadFileContentDisposition() {
        if (selectAttachmentVOs.isEmpty()) {
            return null;
        }
        String encodedFileName = (selectAttachmentVOs.size() == 1)
                ? AttachmentUtils.encodeFileName(selectAttachmentVOs.get(0).getAttName())
                : AttachmentService.MULTIFILES_ZIP_FILENAME;
        String contentDispositionPattern = AttachmentUtils.isMSIE(Faces.getRequest())
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''{1}";
        return MessageFormat.format(contentDispositionPattern, "attachment", encodedFileName);
    }

    private Long convertTimeToLong(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime parse = LocalDateTime.parse(time, formatter);
        return LocalDateTime.from(parse).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public String attachmentInfo() {
        String info = "附加檔案數量:[%s]";
        Integer count = 0;
        if (WkStringUtils.notEmpty(this.attachmentVOs)) {
            count = this.attachmentVOs.size();
        }

        return String.format(info, count);
    }
}
