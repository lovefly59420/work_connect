/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 * 匯出套件
 *
 * @author brain0925_liao
 */
@Component
public class ExportExcelComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8113641092769940765L;

    /**
     * 匯出excel
     *
     * @param document 匯出報表物件
     * @param start    搜尋起始日期
     * @param end      搜尋結束日期
     * @param type     報表類型字串
     */
    public void exportExcel(
        Object document, Date start, Date end, String type, Boolean showCreateDate) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFCellStyle cellStyle = borderStyle(wb);
        int row = 5;
        int rowIndex = 0;
        if (!showCreateDate) {
            row = 4;
        }
        String[] strs = new String[row];
        strs[rowIndex++] = "報表產生日：" + new LocalDate().toString("yyyy/MM/dd");
        if (showCreateDate && start != null && end != null) {
            strs[rowIndex++] =
                "立單日期："
                    + new LocalDate(start).toString("yyyy/MM/dd")
                    + "~"
                    + new LocalDate(end).toString("yyyy/MM/dd");
        }
        strs[rowIndex++] = "報表類型：" + type;

        for (int i = 0; i < row; i++) {
            int totalRows = sheet.getLastRowNum();
            sheet.shiftRows(i, totalRows, 1);
            HSSFRow headerRow = sheet.getRow(i);
            if (headerRow == null) {
                headerRow = sheet.createRow(i);
            }
            HSSFCell cell = headerRow.getCell(0);
            if (cell == null) {
                cell = headerRow.createCell(0);
            }
            cell.setCellValue(strs[i]);
        }

        Iterator<Row> rowIterator = sheet.iterator();
        int nowRow = 0;
        Row rowI;
        while (rowIterator.hasNext()) {
            rowI = rowIterator.next();
            if (nowRow < row) {
                nowRow++;
                continue;
            }
            nowRow++;
            Iterator<Cell> cellIterator = rowI.cellIterator();
            Cell cell;
            while (cellIterator.hasNext()) {
                cell = cellIterator.next();
                cell.setCellStyle(cellStyle);
            }
        }
    }

    /**
     * 匯出excel
     *
     * @param document 匯出報表物件
     */
    public void exportExcelForCheckAuthoritySearch(
        Object document, LinkedHashMap<String, Boolean> columnFields) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        sheet.createFreezePane(0, 1);
        HSSFCellStyle cellStyle = borderStyle(wb);

        HSSFFont redFont = wb.createFont();
        redFont.setColor(IndexedColors.RED.getIndex());

        HSSFFont skyBlueFont = wb.createFont();
        HSSFPalette palette = wb.getCustomPalette();
        HSSFColor skyBlue = palette.findSimilarColor(0, 156, 235);
        skyBlueFont.setColor(skyBlue.getIndex());

        Iterator<Row> rowIterator = sheet.iterator();
        int nowRow = 0;
        Row rowI;
        while (rowIterator.hasNext()) {
            rowI = rowIterator.next();
            nowRow++;

            Iterator<Cell> cellIterator = rowI.cellIterator();
            Cell cell;

            while (cellIterator.hasNext()) {
                cell = cellIterator.next();
                cell.setCellStyle(cellStyle);

                if (nowRow == 1) {
                    rowI.setHeight((short) 1200);
                    HSSFCellStyle converStyle = this.headerStyle(wb);
                    cell.setCellStyle(converStyle);
                }

                // 取代html空白
                Pattern pattern = Pattern.compile("&nbsp", Pattern.MULTILINE);
                Matcher matcher = pattern.matcher(cell.getStringCellValue());
                String cellStr = cell.getStringCellValue().replaceAll("&nbsp", " ");
                while (matcher.find()) {
                    cell.setCellValue(cellStr);
                }

                // 同時 除外變紅色 & 含以下變天空藍
                if (cell.getStringCellValue().indexOf("<span class='WS1-1-2'>") > 0
                    && cell.getStringCellValue().indexOf("<span class='WS1-1-3'>") > 0) {
                    Pattern redPattern =
                        Pattern.compile(
                            "<span class='WS1-1-2'>([^ \\f\\n\\r\\t\\v<]*)</span>",
                            Pattern.MULTILINE);
                    Pattern skyBluePattern =
                        Pattern.compile(
                            "<span class='WS1-1-3'>([^ \\f\\n\\r\\t\\v<]*)</span>",
                            Pattern.MULTILINE);
                    Matcher redMatcher = redPattern.matcher(cell.getStringCellValue());
                    Matcher skyBlueMatcher = skyBluePattern.matcher(cell.getStringCellValue());
                    cellStr =
                        cell.getStringCellValue()
                            .replaceAll("<span class='WS1-1-2'>", "")
                            .replaceAll("<span class='WS1-1-3'>", "")
                            .replaceAll("</span>", "");
                    HSSFRichTextString richString = new HSSFRichTextString(cellStr);
                    while (redMatcher.find()) {
                        int index = 0;
                        while ((index = cellStr.indexOf(redMatcher.group(1), index)) != -1) {
                            richString.applyFont(index, index + redMatcher.group(1).length(),
                                redFont);
                            index = index + redMatcher.group(1).length();
                        }
                    }
                    while (skyBlueMatcher.find()) {
                        int index = 0;
                        while ((index = cellStr.indexOf(skyBlueMatcher.group(1), index)) != -1) {
                            richString.applyFont(index, index + skyBlueMatcher.group(1).length(),
                                skyBlueFont);
                            index = index + skyBlueMatcher.group(1).length();
                        }
                    }
                    cell.setCellValue(richString);
                }

                // 同時 停用項目變紅色 & 含以下變天空藍
                if (cell.getStringCellValue().indexOf("<span style='color:red;'>") > 0
                    && cell.getStringCellValue().indexOf("<span class='WS1-1-3'>") > 0) {
                    Pattern redPattern =
                        Pattern.compile(
                            "<span style='color:red;'>([^ \\f\\n\\r\\t\\v<]*)</span>",
                            Pattern.MULTILINE);
                    Pattern skyBluePattern =
                        Pattern.compile(
                            "<span class='WS1-1-3'>([^ \\f\\n\\r\\t\\v<]*)</span>",
                            Pattern.MULTILINE);
                    Matcher redMatcher = redPattern.matcher(cell.getStringCellValue());
                    Matcher skyBlueMatcher = skyBluePattern.matcher(cell.getStringCellValue());
                    cellStr =
                        cell.getStringCellValue()
                            .replaceAll("<span style='color:red;'>", "")
                            .replaceAll("<span class='WS1-1-3'>", "")
                            .replaceAll("</span>", "");
                    HSSFRichTextString richString = new HSSFRichTextString(cellStr);
                    while (redMatcher.find()) {
                        int index = 0;
                        while ((index = cellStr.indexOf(redMatcher.group(1), index)) != -1) {
                            richString.applyFont(index, index + redMatcher.group(1).length(),
                                redFont);
                            index = index + redMatcher.group(1).length();
                        }
                    }
                    while (skyBlueMatcher.find()) {
                        int index = 0;
                        while ((index = cellStr.indexOf(skyBlueMatcher.group(1), index)) != -1) {
                            richString.applyFont(index, index + skyBlueMatcher.group(1).length(),
                                skyBlueFont);
                            index = index + skyBlueMatcher.group(1).length();
                        }
                    }
                    cell.setCellValue(richString);
                }

                // 停用項目變紅色
                pattern =
                    Pattern.compile(
                        "<span style='color:red;'>([^ \\f\\n\\r\\t\\v<]*)</span>",
                        Pattern.MULTILINE);
                matcher = pattern.matcher(cell.getStringCellValue());
                cellStr =
                    cell.getStringCellValue()
                        .replaceAll("<span style='color:red;'>", "")
                        .replaceAll("</span>", "");
                HSSFRichTextString richString = new HSSFRichTextString(cellStr);
                while (matcher.find()) {
                    int index = 0;
                    while ((index = cellStr.indexOf(matcher.group(1), index)) != -1) {
                        richString.applyFont(index, index + matcher.group(1).length(), redFont);
                        index = index + matcher.group(1).length();
                    }
                    cell.setCellValue(richString);
                }

                // 含以下變天空藍
                pattern =
                    Pattern.compile(
                        "<span class='WS1-1-3'>([^ \\f\\n\\r\\t\\v<]*)</span>", Pattern.MULTILINE);
                matcher = pattern.matcher(cell.getStringCellValue());
                cellStr =
                    cell.getStringCellValue()
                        .replaceAll("<span class='WS1-1-3'>", "")
                        .replaceAll("</span>", "")
                        .replaceAll("&nbsp", " ");
                richString = new HSSFRichTextString(cellStr);
                while (matcher.find()) {
                    int index = 0;
                    while ((index = cellStr.indexOf(matcher.group(1), index)) != -1) {
                        richString.applyFont(index, index + matcher.group(1).length(), skyBlueFont);
                        index = index + matcher.group(1).length();
                    }
                    cell.setCellValue(richString);
                }

                // 取代span
                pattern = Pattern.compile("</span>", Pattern.MULTILINE);
                matcher = pattern.matcher(cell.getStringCellValue());
                cellStr = cell.getStringCellValue().replaceAll("</span>", " ");
                while (matcher.find()) {
                    cell.setCellValue(cellStr);
                }

                // 取代換行
                pattern = Pattern.compile("</br>", Pattern.MULTILINE);
                matcher = pattern.matcher(cell.getStringCellValue());
                cellStr = cell.getStringCellValue().replaceAll("</br>", "\r\n");
                while (matcher.find()) {
                    cell.setCellValue(cellStr);
                }
            }
        }
        // 設定【類別】欄位寬度
        sheet.setColumnWidth(0, (int) ((19 + 0.72) * 256));
        // 設定【單據名稱】欄位寬度
        sheet.setColumnWidth(1, (int) ((19 + 0.72) * 256));
        // 設定【可使用單位】欄位寬度
        sheet.setColumnWidth(2, (int) ((24 + 0.72) * 256));
        // 設定【可使用人員】欄位寬度
        sheet.setColumnWidth(3, (int) ((18 + 0.72) * 256));
        // 設定【強制簽核至上層主管】欄位寬度
        sheet.setColumnWidth(4, (int) ((19 + 0.72) * 256));
        // 設定【顯示分類】欄位寬度
        sheet.setColumnWidth(5, (int) ((18 + 0.72) * 256));
        // 設定【執行項目】欄位寬度
        sheet.setColumnWidth(6, (int) ((18 + 0.72) * 256));
        // 設定【簽核層級】欄位寬度
        sheet.setColumnWidth(7, (int) ((12.5 + 0.72) * 256));
        // 設定【可閱部門】欄位寬度
        sheet.setColumnWidth(8, (int) ((15 + 0.72) * 256));
        // 設定【可閱人員】欄位寬度
        sheet.setColumnWidth(9, (int) ((18 + 0.72) * 256));
        // 設定【領單單位】欄位寬度
        sheet.setColumnWidth(10, (int) ((15 + 0.72) * 256));
        // 設定【執行單位簽核人員】欄位寬度
        sheet.setColumnWidth(11, (int) ((15 + 0.72) * 256));
        // 設定【執行模式】欄位寬度
        sheet.setColumnWidth(12, (int) ((15 + 0.72) * 256));
        // 設定【指派人員】欄位寬度
        sheet.setColumnWidth(13, (int) ((18 + 0.72) * 256));
        // 設定【指派單位】欄位寬度
        sheet.setColumnWidth(14, (int) ((15 + 0.72) * 256));
        // 設定【管理領單人員】欄位寬度
        sheet.setColumnWidth(15, (int) ((18 + 0.72) * 256));
        // 設定【領單人員】欄位寬度
        sheet.setColumnWidth(16, (int) ((18 + 0.72) * 256));
        // 設定【合法區間】欄位寬度
        sheet.setColumnWidth(17, (int) ((21 + 0.72) * 256));
        // 設定【起訖日】欄位寬度
        sheet.setColumnWidth(18, (int) ((19 + 0.72) * 256));

        this.mergeSameName(wb);

        // 判斷是否隱藏欄位
        rowI = sheet.getRow(0); // 取得標題列
        Iterator<Cell> cellIterator = rowI.cellIterator();
        Cell cell;
        while (cellIterator.hasNext()) {
            cell = cellIterator.next();
            if (columnFields.containsKey(cell.getStringCellValue())
                && columnFields.get(cell.getStringCellValue()) == false) {
                sheet.setColumnHidden(cell.getColumnIndex(), true);
            }
        }
    }

    /**
     * 合併特定表格excel. for CheckAuthoritySearch
     *
     * @param document 匯出報表物件
     */
    private void mergeSameName(HSSFWorkbook document) {

        HSSFSheet sheet = document.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        List<Row> checkAuthoritylist = Lists.newArrayList();

        if (rowIterator.hasNext()) {
            checkAuthoritylist.add(rowIterator.next());
        }

        int nowRow = 0;

        int tagMerge = 0;
        int menuTagMerge = 0;
        int subTagMerge = 0;
        int canUseMerge = 0;
        int useUserNameMerge = 0;
        int tagType = 0;
        Row rowI;

        while (rowIterator.hasNext()) {

            Row last = checkAuthoritylist.get(checkAuthoritylist.size() - 1);
            rowI = rowIterator.next();

            if (rowI.getCell(0).getStringCellValue().equals(last.getCell(0).getStringCellValue())) {
                tagMerge++;
            } else {
                // 類別
                if (nowRow - tagMerge != nowRow) {
                    CellRangeAddress cra = new CellRangeAddress(nowRow - tagMerge, nowRow, 0, 0);
                    sheet.addMergedRegion(cra);
                }
                tagMerge = 0;
            }

            if (rowI.getCell(0).getStringCellValue().equals(last.getCell(0).getStringCellValue())
                && rowI.getCell(1).getStringCellValue()
                .equals(last.getCell(1).getStringCellValue())) {
                menuTagMerge++;
            } else {
                // 單據名稱
                if (nowRow - menuTagMerge != nowRow) {
                    CellRangeAddress cra1 = new CellRangeAddress(nowRow - menuTagMerge, nowRow, 1,
                        1);
                    sheet.addMergedRegion(cra1);
                }
                menuTagMerge = 0;
            }

            if (rowI.getCell(0).getStringCellValue().equals(last.getCell(0).getStringCellValue())
                && rowI.getCell(1).getStringCellValue().equals(last.getCell(1).getStringCellValue())
                && rowI.getCell(2).getStringCellValue()
                .equals(last.getCell(2).getStringCellValue())) {
                canUseMerge++;
            } else {
                // 可使用單位
                if (nowRow - canUseMerge != nowRow) {
                    CellRangeAddress cra2 = new CellRangeAddress(nowRow - canUseMerge, nowRow, 2,
                        2);
                    sheet.addMergedRegion(cra2);
                }
                canUseMerge = 0;
            }

            if (rowI.getCell(0).getStringCellValue().equals(last.getCell(0).getStringCellValue())
                && rowI.getCell(1).getStringCellValue().equals(last.getCell(1).getStringCellValue())
                && rowI.getCell(2).getStringCellValue().equals(last.getCell(2).getStringCellValue())
                && rowI.getCell(3).getStringCellValue()
                .equals(last.getCell(3).getStringCellValue())) {
                useUserNameMerge++;
            } else {
                if (nowRow - useUserNameMerge != nowRow) {
                    // 可使用人員
                    CellRangeAddress cra3 = new CellRangeAddress(nowRow - useUserNameMerge, nowRow,
                        3, 3);
                    // 強制簽核至上層主管
                    CellRangeAddress cra4 = new CellRangeAddress(nowRow - useUserNameMerge, nowRow,
                        4, 4);
                    sheet.addMergedRegion(cra3);
                    sheet.addMergedRegion(cra4);
                }
                useUserNameMerge = 0;
            }

            if (rowI.getCell(0).getStringCellValue().equals(last.getCell(0).getStringCellValue())
                && rowI.getCell(1).getStringCellValue().equals(last.getCell(1).getStringCellValue())
                && rowI.getCell(2).getStringCellValue().equals(last.getCell(2).getStringCellValue())
                && rowI.getCell(3).getStringCellValue().equals(last.getCell(3).getStringCellValue())
                && rowI.getCell(5).getStringCellValue()
                .equals(last.getCell(5).getStringCellValue())) {
                tagType++;
            } else {
                // 顯示分類
                if (nowRow - tagType != nowRow) {
                    CellRangeAddress cra5 = new CellRangeAddress(nowRow - tagType, nowRow, 5, 5);
                    sheet.addMergedRegion(cra5);
                }
                tagType = 0;
            }

            if (rowI.getCell(6).getStringCellValue().equals(last.getCell(6).getStringCellValue())
                && rowI.getCell(7).getStringCellValue().equals(last.getCell(7).getStringCellValue())
                && rowI.getCell(8).getStringCellValue().equals(last.getCell(8).getStringCellValue())
                && rowI.getCell(9).getStringCellValue()
                .equals(last.getCell(9).getStringCellValue())) {
                subTagMerge++;
            } else {
                if (nowRow - subTagMerge != nowRow) {
                    // 執行項目
                    CellRangeAddress cra6 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 6,
                        6);
                    // 簽核層級
                    CellRangeAddress cra7 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 7,
                        7);
                    // 可閱部門
                    CellRangeAddress cra8 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 8,
                        8);
                    // 可閱人員
                    CellRangeAddress cra9 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 9,
                        9);

                    sheet.addMergedRegion(cra6);
                    sheet.addMergedRegion(cra7);
                    sheet.addMergedRegion(cra8);
                    sheet.addMergedRegion(cra9);
                }
                subTagMerge = 0;
            }

            nowRow++;
            checkAuthoritylist.add(rowI);
        }
        if (rowIterator.hasNext()) {
            checkAuthoritylist.remove(0);
        } else {
            // ========================================================================
            // 處理最後一列
            // ========================================================================
            if (tagMerge > 0) {
                // 類別
                CellRangeAddress cra = new CellRangeAddress(nowRow - tagMerge, nowRow, 0, 0);
                sheet.addMergedRegion(cra);
            }

            if (menuTagMerge > 0) {
                // 單據名稱
                CellRangeAddress cra1 = new CellRangeAddress(nowRow - menuTagMerge, nowRow, 1, 1);
                sheet.addMergedRegion(cra1);
            }

            if (canUseMerge > 0) {
                // 可使用單位
                CellRangeAddress cra2 = new CellRangeAddress(nowRow - canUseMerge, nowRow, 2, 2);
                sheet.addMergedRegion(cra2);
            }

            if (useUserNameMerge > 0) {
                // 可使用人員
                CellRangeAddress cra3 = new CellRangeAddress(nowRow - useUserNameMerge, nowRow, 3,
                    3);
                sheet.addMergedRegion(cra3);
                // 強制簽核至上層主管
                CellRangeAddress cra4 = new CellRangeAddress(nowRow - useUserNameMerge, nowRow, 4,
                    4);
                sheet.addMergedRegion(cra4);
            }

            if (tagType > 0) {
                // 顯示分類
                CellRangeAddress cra5 = new CellRangeAddress(nowRow - tagType, nowRow, 5, 5);
                sheet.addMergedRegion(cra5);
            }

            if (subTagMerge > 0) {
                // 執行項目
                CellRangeAddress cra6 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 6, 6);
                // 簽核層級
                CellRangeAddress cra7 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 7, 7);
                // 可閱部門
                CellRangeAddress cra8 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 8, 8);
                // 可閱人員
                CellRangeAddress cra9 = new CellRangeAddress(nowRow - subTagMerge, nowRow, 9, 9);

                sheet.addMergedRegion(cra6);
                sheet.addMergedRegion(cra7);
                sheet.addMergedRegion(cra8);
                sheet.addMergedRegion(cra9);
            }
        }
    }

    /**
     * 建立Header樣式 for for CheckAuthoritySearch
     *
     * @param wb
     * @return
     */
    @SuppressWarnings("deprecation")
    private HSSFCellStyle headerStyle(HSSFWorkbook wb) {
        HSSFCellStyle style = wb.createCellStyle();
        HSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setColor(IndexedColors.WHITE.getIndex());
        style.setFont(font);

        @SuppressWarnings("resource")
        HSSFWorkbook hwb = new HSSFWorkbook();
        HSSFPalette palette = hwb.getCustomPalette();
        HSSFColor myColor = palette.findSimilarColor(118, 147, 60);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(myColor.getIndex());

        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setWrapText(true);

        return style;
    }

    /**
     * 建立邊線格式
     *
     * @param wb
     * @return
     */
    @SuppressWarnings("deprecation")
    private HSSFCellStyle borderStyle(HSSFWorkbook wb) {
        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        cellStyle.setWrapText(true);
        return cellStyle;
    }
}
