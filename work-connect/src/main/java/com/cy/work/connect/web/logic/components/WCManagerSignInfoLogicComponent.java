/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.web.view.vo.SignInfoVO;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCManagerSignInfoLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8018860478696561572L;

    private static WCManagerSignInfoLogicComponent instance;

    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;

    public static WCManagerSignInfoLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCManagerSignInfoLogicComponent.instance = this;
    }

    public boolean isReqDepsByWC(WCMaster wcMaster, User user) {
        boolean result = false;
        if (wcMaster == null || user == null) {
            return result;
        }
        try {
            Org createOrg = orgManager.findBySid(wcMaster.getDep_sid());
            if (createOrg == null) {
                return result;
            }
            List<Org> all = Lists.newArrayList();
            all.addAll(
                    orgManager.findAllChild(createOrg.getSid()).stream()
                            .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                            .collect(Collectors.toList()));
            all.addAll(orgManager.findAllParent(createOrg.getSid()));
            all.add(createOrg);
            Org loginOrg = orgManager.findBySid(user.getPrimaryOrg().getSid());
            if (all.contains(loginOrg)) {
                result = true;
            }

        } catch (Exception e) {
            log.warn("isReqDepsByWcID ERROR", e);
        }
        return result;
    }

//    public boolean isCanSignBtn(String wc_ID, User user) {
//        return isCanSignBtn(wc_ID, user, SignType.SIGN);
//    }
//
//    public boolean isCanSignBtn(String wc_ID, User user, SignType signType) {
//        try {
//            List<GroupManagerSignInfo> groupManagerSignInfos = wcManagerSignInfoManager.getGroupManagerSignInfo(wc_ID);
//            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
//                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
//                    List<String> ids = BpmManager.getInstance()
//                            .findTaskCanSignUserIds(si.getInstanceID());
//                    if (signType != null && !signType.equals(si.getSignType())) {
//                        continue;
//                    }
//                    if (ids.contains(user.getId())) {
//                        return true;
//                    }
//                }
//            }
//        } catch (Exception e) {
//            log.warn("isCanSignBtn ERROR", e);
//        }
//        return false;
//    }

    /**
     * 取得該使用者目前可使用節點物件
     */
    public List<SingleManagerSignInfo> getSingleManagerSignInfo(
            String wcSid, User user, SignType signType) {
        List<SingleManagerSignInfo> result = Lists.newArrayList();
        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcManagerSignInfoManager.getGroupManagerSignInfo(wcSid);
            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                    List<String> ids = BpmManager.getInstance()
                            .findTaskCanSignUserIds(si.getInstanceID());
                    if (signType != null && !signType.equals(si.getSignType())) {
                        continue;
                    }
                    if (ids.contains(user.getId())) {
                        result.add(si);
                    }
                }
            }
        } catch (Exception e) {
            log.warn("getSingleManagerSignInfo ERROR", e);
        }
        return result;
    }

    public SignInfoVO getReqSignInfo(
            String wc_Id, List<UserViewVO> selUserViewVOs, Integer loginUserSid, Integer loginDepSid) {
        SignInfoVO sv = new SignInfoVO();
        List<SingleManagerSignInfo> oldCounterSignInfos = WCManagerSignInfoManager.getInstance().getCounterSignInfos(wc_Id);
        if ((oldCounterSignInfos == null || oldCounterSignInfos.isEmpty())
                && (selUserViewVOs == null || selUserViewVOs.isEmpty())) {
            Preconditions.checkState(false, "尚未挑選任何資訊,請確認！");
        }

        // 過濾出已簽核人員
        List<SingleManagerSignInfo> counterSignInfosSigned = oldCounterSignInfos.stream()
                .filter(
                        each -> !each.getStatus().equals(BpmStatus.NEW_INSTANCE)
                                && !each.getStatus().equals(BpmStatus.WAIT_CREATE))
                .collect(Collectors.toList());

        // 檢測是否有移除已簽名的人員
        StringBuilder moveErrorMessage = new StringBuilder();
        counterSignInfosSigned.forEach(
                item -> {
                    List<UserViewVO> signUser = selUserViewVOs.stream()
                            .filter(each -> each.getSid().equals(item.getUserSid()))
                            .collect(Collectors.toList());
                    if (signUser == null || signUser.isEmpty()) {
                        if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
                            moveErrorMessage.append("\n");
                        }
                        User movedUser = WkUserCache.getInstance().findBySid(item.getUserSid());
                        moveErrorMessage.append(
                                "成員["
                                        + movedUser.getPrimaryOrg().getName()
                                        + "-"
                                        + movedUser.getName()
                                        + "]已簽名,不可移除");
                    }
                });
        if (!Strings.isNullOrEmpty(moveErrorMessage.toString())) {
            Preconditions.checkState(false, moveErrorMessage.toString());
        }

        // 檢測是否有需要移除作廢的流程
        List<SingleManagerSignInfo> removeSingleManagerSignInfos = Lists.newArrayList();
        oldCounterSignInfos.forEach(
                item -> {
                    List<UserViewVO> selUsers = selUserViewVOs.stream()
                            .filter(each -> each.getSid().equals(item.getUserSid()))
                            .collect(Collectors.toList());
                    if (selUsers == null || selUsers.isEmpty()) {
                        removeSingleManagerSignInfos.add(item);
                    }
                });

        // 檢測是否有需要新增的流程
        List<Integer> addUserFlowSid = Lists.newArrayList();
        selUserViewVOs.forEach(
                item -> {
                    List<SingleManagerSignInfo> createedSingleManagerSignInfo = oldCounterSignInfos.stream()
                            .filter(each -> item.getSid().equals(each.getUserSid()))
                            .collect(Collectors.toList());
                    if (createedSingleManagerSignInfo == null
                            || createedSingleManagerSignInfo.isEmpty()) {
                        addUserFlowSid.add(item.getSid());
                    }
                });
        sv.getRemoveSingleManagerSignInfos().addAll(removeSingleManagerSignInfos);
        sv.getAddUserFlowSid().addAll(addUserFlowSid);
        return sv;
    }
}
