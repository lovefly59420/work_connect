/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.controller;

import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.primefaces.component.editor.Editor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component("editorConverter")
public class EditorConverter implements Converter {

    @Autowired
    private WkJsoupUtils JsoupUtils;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        if (value instanceof String && component instanceof Editor) {
            return JsoupUtils.removeRegexpByEditor(value)
                .replaceAll("&lt;", "&amp;lt;")
                .replaceAll("&gt;", "&amp;gt;");
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (component instanceof Editor && value instanceof String) {
            return (String) value;
        }
        return null;
    }
}
