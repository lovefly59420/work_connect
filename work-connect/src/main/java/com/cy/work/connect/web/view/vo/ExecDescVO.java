/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.commons.vo.Org;
import java.io.Serializable;
import lombok.Getter;

/**
 * @author jimmy_chou
 */
public class ExecDescVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8789790569220704437L;

    @Getter
    private final String sid;
    @Getter
    private final String tagName;
    @Getter
    private final Org execDep;
    @Getter
    private final String execDesc;

    public ExecDescVO(String sid, String tagName, Org execDep, String execDesc) {
        this.sid = sid;
        this.tagName = tagName;
        this.execDep = execDep;
        this.execDesc = execDesc;
    }
}
