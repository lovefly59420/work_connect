/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.setting;

import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.stereotype.Component;

/**
 * 工作聯絡單單據狀態靜態資料
 *
 * @author brain0925_liao
 */
@Component
public class WCStatusSetting implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6988256506253557798L;

    /**
     * 工作聯絡單單據狀態選項List
     */
    private List<SelectItem> wc_workSearchs;

    private List<SelectItem> all_wc_workSearchs;

    /**
     * 取得工作聯絡單單據狀態選項資料,若記憶體已有將不再行建立
     */
    public List<SelectItem> getWorkSearchItems() {
        if (wc_workSearchs != null) {
            return wc_workSearchs;
        }
        wc_workSearchs = Lists.newArrayList();
        Lists.newArrayList(WCStatus.NEW_INSTANCE, WCStatus.WAIT_EXEC, WCStatus.EXEC, WCStatus.CLOSE)
            .forEach(
                each -> {
                    wc_workSearchs.add(new SelectItem(each.name(), each.getVal()));
                });
        return wc_workSearchs;
    }

    /**
     * 取得工作聯絡單單據狀態選項資料,若記憶體已有將不再行建立
     */
    public List<SelectItem> getAllWorkSearchItems() {
        if (all_wc_workSearchs != null) {
            return all_wc_workSearchs;
        }
        all_wc_workSearchs = Lists.newArrayList();
        Lists.newArrayList(
                WCStatus.NEW_INSTANCE,
                WCStatus.WAIT_EXEC,
                WCStatus.EXEC,
                WCStatus.CLOSE,
                WCStatus.INVALID)
            .forEach(
                each -> {
                    all_wc_workSearchs.add(new SelectItem(each.name(), each.getVal()));
                });
        return all_wc_workSearchs;
    }
}
