/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.vo.User;
import com.cy.work.connect.vo.WCMaster;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;

/**
 * BPM 元件 相關使用資料
 *
 * @author kasim
 */
public class BpmVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8745716262429056253L;
    /**
     * 登入者sid
     */
    @SuppressWarnings("unused")
    private final Integer loginUserSid;

    @Getter
    /** 登入者Id */
    private final String loginUserId;

    @Getter
    /** 登入者Name */
    private final String loginUserName;
    /**
     * 需求單位流程
     */
    @Getter
    /** 是否顯示簽名按鈕 */
    private Boolean showSign = Boolean.FALSE;

    @Getter
    /** 是否顯示復原按鈕 */
    private Boolean showRecovery = Boolean.FALSE;

    @Getter
    /** 是否顯示退回按鈕 */
    private Boolean showRollBack = Boolean.FALSE;

    @Getter
    /** 是否顯示作廢按鈕 */
    private Boolean showInvalid = Boolean.FALSE;
    /**
     * 需求單位加簽 ManagerSignInfo
     */
    @Getter
    /** 是否顯示簽名按鈕 */
    private boolean showSignManagerSignInfo = false;

    @Getter
    /** 是否顯示復原按鈕 */
    private boolean showRecoveryManagerSignInfo = false;

    @Getter
    /** 是否disabled按鈕 */
    private Boolean disabledBtn = Boolean.TRUE;

    @Getter
    /** 是否顯示頁籤 */
    private boolean showTab = Boolean.TRUE;

    @Getter
    /** 聯絡單sid */
    private String sid;

    @Getter
    /** 聯絡單no */
    private String no;

    @Getter
    /** BPM 水管圖 */
    private List<FlowNodeVO> flowNodeVOs;

    @Getter
    /** 退回選項 */
    private SelectItem[] rollbackItems;

    @Getter
    @Setter
    /** 選擇的退回物件 Sid*/
    private String rollbackSelectTaskSid;

    @Getter
    @Setter
    /** 退回理由 */
    private String rollbackComment;

    public BpmVo(User user) {
        this.loginUserSid = user.getSid();
        this.loginUserId = user.getId();
        this.loginUserName = user.getName();
        this.flowNodeVOs = Lists.newArrayList();
    }

    /**
     * 初始化資料
     *
     * @param obj 主檔單據
     */
    public void load(WCMaster obj) {
        this.sid = obj.getSid();
        this.no = obj.getWc_no();
        this.showTab = Boolean.FALSE;
        this.showSign = Boolean.FALSE;
        this.showRecovery = Boolean.FALSE;
        this.showRollBack = Boolean.FALSE;
        this.showInvalid = Boolean.FALSE;
        this.disabledBtn = Boolean.TRUE;
        //        this.signSid = "";
        //       this.bpmId = "";
        //        this.bpmStatus = null;
        this.flowNodeVOs = Lists.newArrayList();
    }

    /**
     * 讀取水管圖 及 變更按鈕顯示
     * @param flowNodeVOs
     * @param showSign
     * @param showRecovery
     * @param showRollBack
     * @param showInvalid
     * @param showSignManagerSignInfo
     * @param showRecoveryManagerSignInfo
     */
    public void loadTasksAndShowBtn(
        List<FlowNodeVO> flowNodeVOs,
        Boolean showSign,
        Boolean showRecovery,
        Boolean showRollBack,
        Boolean showInvalid,
        boolean showSignManagerSignInfo,
        boolean showRecoveryManagerSignInfo
        ) {
        this.showTab = Boolean.TRUE;
        this.flowNodeVOs = flowNodeVOs;
        this.showSign = showSign;
        this.showRecovery = showRecovery;
        this.showRollBack = showRollBack;
        this.showInvalid = showInvalid;
        this.showSignManagerSignInfo = showSignManagerSignInfo;
        this.showRecoveryManagerSignInfo = showRecoveryManagerSignInfo;
        this.disabledBtn = Boolean.FALSE;
    }

    /**
     * 建立退回選項
     *
     * @param items
     */
    public void initRollbackItems(SelectItem[] items) {
        this.rollbackItems = items;
        this.rollbackSelectTaskSid = "";
        this.rollbackComment = "";
    }

    /**
     * 取得 欲退回的節點
     *
     * @return
     */
    public ProcessTaskHistory getRollBackTask() {
        return null;
    }

    /**
     * 變更按鈕disabled狀態
     *
     * @param value
     */
    public void updateDisabledBtn(Boolean value) {
        this.disabledBtn = value;
    }
}
