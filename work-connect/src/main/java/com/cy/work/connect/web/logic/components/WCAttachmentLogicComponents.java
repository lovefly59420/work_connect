/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.work.common.vo.basic.BasicAttachmentService;
import com.cy.work.connect.logic.manager.WCAttachmentManager;
import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.vo.WCAttachment;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 工作聯絡單附件邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WCAttachmentLogicComponents
    implements BasicAttachmentService<AttachmentVO, WCAttachment, String>,
    InitializingBean,
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2905398956531019250L;

    private static WCAttachmentLogicComponents instance;
    @Autowired
    private WCAttachmentManager wcAttachmentManager;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;

    public static WCAttachmentLogicComponents getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCAttachmentLogicComponents.instance = this;
    }

    @Override
    public File getServerSideFile(AttachmentVO attachment) {
        return AttachmentUtils.getServerSideFile(attachment, "wcMaster");
    }

    /**
     * 取得附件物件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public WCAttachment getWCAttachmentBySid(String att_sid) {
        return wcAttachmentManager.findBySid(att_sid);
    }

    /**
     * 取得附件介面物件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public AttachmentVO getAttachmentVOBySid(String att_sid) {

        WCAttachment wa = wcAttachmentManager.findBySid(att_sid);
        return wcAttachmentManager.transWCAttachmentToAttachmentVO(wa);
    }

    /**
     * 取得附件界面物件By 工作聯絡單Sid
     *
     * @param wc_sid 工作聯絡單Sid
     * @return
     */
    public List<AttachmentVO> getAttachmentsByWCSid(String wc_sid) {
        return wcAttachmentManager.getAttachmentsByWCSid(wc_sid);
    }

    /**
     * 建立附件
     *
     * @param wc_id         工作聯絡單Sid
     * @param wc_no         工作聯絡單單號
     * @param fileName      附件名稱
     * @param create_uerSid 建立者Sid
     * @param departmetSid  建立者部門Sid
     * @return
     */
    public AttachmentVO createAttachment(
        String wc_id, String wc_no, String fileName, Integer create_uerSid, Integer departmetSid) {
        WCAttachment create = new WCAttachment();
        create.setStatus(Activation.ACTIVE);
        create.setFile_name(fileName);
        create.setWc_sid(wc_id);
        create.setWc_no(wc_no);
        create.setDescription("");
        create.setDepartment_sid(departmetSid);
        WCAttachment wc = wcAttachmentManager.create(create, create_uerSid);
        if (wc == null) {
            return null;
        }
        return new AttachmentVO(
            wc.getSid(),
            wc.getFile_name(),
            wc.getDescription(),
            wc.getFile_name(),
            true,
            String.valueOf(wc.getCreate_usr_sid()));
    }

    /**
     * 更新附件資訊
     *
     * @param wc_sid         工作聯絡單Sid
     * @param wc_no          工作聯絡單單號
     * @param description    描述
     * @param userSid        更新者Sid
     * @param attachment_sid 附件Sid
     */
    public void updateWCMasterMappingInfo(
        String wc_sid, String wc_no, String description, Integer userSid, String attachment_sid) {
        wcAttachmentManager.updateWCMasterMappingInfo(
            wc_sid, wc_no, description, userSid, attachment_sid);
    }

    /**
     * 更新附件描述
     *
     * @param att_sid        附件Sid
     * @param desc           描述
     * @param update_userSid 更新者Sid
     * @return
     */
    public AttachmentVO updateAttachment(String att_sid, String desc, Integer update_userSid) {
        WCAttachment wa = wcAttachmentManager.findBySid(att_sid);
        if (wa == null) {
            return null;
        }
        wa.setDescription(desc);
        WCAttachment wc = wcAttachmentManager.update(wa, update_userSid);
        if (wc == null) {
            return null;
        }
        return new AttachmentVO(
            wc.getSid(),
            wc.getFile_name(),
            wc.getDescription(),
            wc.getFile_name(),
            true,
            String.valueOf(wc.getCreate_usr_sid()));
    }

    public void deleteAttachment(AttachmentVO attachmentVO, Integer loginUserSid, String wc_sid) {
        wcMasterCustomLogicManager.deleteAttachment(attachmentVO, loginUserSid, wc_sid);
    }

    @Override
    public AttachmentVO findBySid(String att_sid) {
        WCAttachment wa = wcAttachmentManager.findBySid(att_sid);
        return wcAttachmentManager.transWCAttachmentToAttachmentVO(wa);
    }

    @Override
    public List<AttachmentVO> findAttachsByLazy(WCAttachment e) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AttachmentVO> findAttachs(WCAttachment e) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDirectoryName() {
        return "WCAttachment";
    }

    @Override
    public String getAttachPath() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AttachmentVO createEmptyAttachment(String fileName, Integer executor, Integer dep) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AttachmentVO updateAttach(AttachmentVO attach) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAttach(AttachmentVO attach) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAttach(List<AttachmentVO> attachs) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String depAndUserName(AttachmentVO attachment) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void linkRelation(List<AttachmentVO> attachments, WCAttachment entity, Integer login) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void linkRelation(AttachmentVO ra, WCAttachment entity, Integer login) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeStatusToInActive(
        List<AttachmentVO> attachments, AttachmentVO attachment, WCAttachment entity,
        Integer login) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }
}
