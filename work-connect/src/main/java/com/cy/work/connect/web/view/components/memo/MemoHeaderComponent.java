/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components.memo;

import java.io.Serializable;
import lombok.Getter;

/**
 * 備忘錄 表頭
 *
 * @author kasim
 */
public class MemoHeaderComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7571060107874450489L;

    @Getter
    /** 部門名稱 */
    private String depName;

    @Getter
    /** 填寫人員 */
    private String createUserName;

    @Getter
    /** 填寫日期 */
    private String createDate;

    @Getter
    /** 單號 */
    private String no;

    public MemoHeaderComponent() {
    }

    /**
     * 初始化資料
     *
     * @param depName
     * @param createUserName
     * @param createDate
     * @param no
     */
    public void init(String depName, String createUserName, String createDate, String no) {
        this.init(depName, createUserName, createDate);
        this.no = no;
    }

    /**
     * 初始化資料
     *
     * @param depName
     * @param createUserName
     * @param createDate
     */
    public void init(String depName, String createUserName, String createDate) {
        this.depName = depName;
        this.createUserName = createUserName;
        this.createDate = createDate;
    }
}
