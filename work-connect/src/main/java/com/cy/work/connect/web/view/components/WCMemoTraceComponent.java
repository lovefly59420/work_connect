/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.web.logic.components.WCMemoTraceLogicComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * 備忘錄追蹤元件
 *
 * @author brain0925_liao
 */
public class WCMemoTraceComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7640066484646100422L;

    @Getter
    private List<WCTraceVO> memoTraceVOs;

    private String memoSid;

    private UserViewVO userViewVO;

    private OrgViewVo depViewVo;
    @Getter
    private boolean showTraceTab = false;

    public WCMemoTraceComponent() {
    }

    public void loadData(String wrcId, UserViewVO userViewVO, OrgViewVo depViewVo) {
        this.userViewVO = userViewVO;
        this.depViewVo = depViewVo;
        this.memoSid = wrcId;
        loadViewData(wrcId, userViewVO, depViewVo);
    }

    public void loadData() {
        loadViewData(memoSid, userViewVO, depViewVo);
    }

    private void loadViewData(String memoSid, UserViewVO userViewVO, OrgViewVo depViewVo) {
        memoTraceVOs =
            WCMemoTraceLogicComponent.getInstance()
                .getWCTraceVOsByMemoSid(memoSid, userViewVO, depViewVo);
        showTraceTab = memoTraceVOs != null && memoTraceVOs.size() > 0;
    }
}
