package com.cy.work.connect.web.view.vo;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UsersTo;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 * 類別選單-查詢結果VO
 *
 * @author brain0925_liao
 */
public class MenuTagSearchVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2453240407656448083L;

    @Getter
    private final String sid;
    @Getter
    private String createTime;
    @Getter
    private String creatUser;
    @Getter
    private String name;
    @Getter
    private String status;
    @Getter
    private String modifyUser;
    @Getter
    private String modifyTime;
    @Getter
    private String seq;
    @Getter
    private boolean isAttachmentType;
    @Getter
    private Long useDepTempletSid;
    /**
     * 可使用人員
     */
    @Getter
    private String useUserNames;
    /**
     * 可使用單位顯示文字
     */
    @Getter
    private String uesDepsShow;
    /**
     * 可使用單位顯示文字
     */
    @Getter
    private String uesDepsShowToolTip;

    public MenuTagSearchVO(String sid) {
        this.sid = sid;
    }

    public MenuTagSearchVO(
        String sid,
        String createTime,
        String creatUser,
        String name,
        String status,
        String modifyTime,
        String seq,
        boolean isAttachmentType,
        String modifyUser,
        UserDep useDep,
        boolean isUserContainFollowing,
        Long useDepTempletSid,
        UsersTo useUser) {

        this.sid = sid;
        this.createTime = createTime;
        this.creatUser = creatUser;
        this.name = name;
        this.status = status;
        this.modifyTime = modifyTime;
        this.seq = seq;
        this.isAttachmentType = isAttachmentType;
        this.modifyUser = modifyUser;
        this.useDepTempletSid = useDepTempletSid;

        WCConfigTempletManager templetManager = WCConfigTempletManager.getInstance();
        // ====================================
        // 準備 可使用單位 顯示文字
        // ====================================
        // 依條件組裝
        String[] showContentAry =
            templetManager.prepareDepSettingShowContent(
                useDep, isUserContainFollowing, useDepTempletSid);

        // 可使用單位 - 顯示標籤
        this.uesDepsShow = showContentAry[0];

        // 可使用單位 - Tooltip (未設定代表全部單位)
        this.uesDepsShowToolTip =
            WkStringUtils.isEmpty(showContentAry[1]) ? "全部單位" : showContentAry[1];

        WkUserCache userManager = WkUserCache.getInstance();
        // ====================================
        // 準備 可使用人員 顯示文字
        // ====================================
        StringBuilder useUserNamesSb = new StringBuilder();
        if (useUser != null && useUser.getUserTos() != null) {
            useUser
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (WkStringUtils.notEmpty(useUserNamesSb.toString())) {
                            useUserNamesSb.append("\n");
                        }

                        User user = userManager.findBySid(subItem.getSid());
                        useUserNamesSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            useUserNamesSb.append("<span style='color:red;'>(停用)</span>");
                        }
                    });
        }
        this.useUserNames = useUserNamesSb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MenuTagSearchVO other = (MenuTagSearchVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
