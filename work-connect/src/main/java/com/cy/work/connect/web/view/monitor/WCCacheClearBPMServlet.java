/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.monitor;

import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.cache.WCManagerSignInfoCache;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * @author brain0925_liao
 */
@Slf4j
@WebServlet(name = "/cache/clear/for/bpm", urlPatterns = "/cache/clear/for/bpm")
public class WCCacheClearBPMServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 8781068829978158855L;

    @Autowired
    private WCManagerSignInfoCache wcManagerSignInfoCache;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(
            this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        try {
            log.debug("receive clear cache signal.");
            try {
                wcExecManagerSignInfoCache.syncUpdateBpm();
            } catch (Exception ex) {
                log.warn("wcExecManagerSignInfoCache ERROR", ex);
            }
            try {
                wcManagerSignInfoCache.syncUpdateBpm();
            } catch (Exception ex) {
                log.warn("wcManagerSignInfoCache ERROR", ex);
            }

            log.debug("receive clear cache signal done.");
            response.getWriter().write("ok");
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
}
