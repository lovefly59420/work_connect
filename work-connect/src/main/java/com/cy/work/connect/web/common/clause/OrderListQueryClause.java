package com.cy.work.connect.web.common.clause;

import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;

public class OrderListQueryClause {

    @Getter
    @Setter
    /** wc_master sid */
    private String sid;

    @Getter
    @Setter
    /** 類別 */
    private String categorySid;

    @Getter
    @Setter
    /** 單據狀態 */
    private List<String> statusList;

    @Getter
    @Setter
    /** 模糊搜尋 */
    private String searchText;

    @Getter
    @Setter
    /** 主題、內容 */
    private String searchTextForThemeAndContent;

    @Getter
    @Setter
    /** 日期起訖區間 */
    private DateIntervalClause dateIntervalClause = new DateIntervalClause();

    public void init() {
        dateIntervalClause.init();
        categorySid = null;
        searchText = null;
        this.searchTextForThemeAndContent = null;
        statusList = Lists.newArrayList(
                WCStatus.NEW_INSTANCE,
                WCStatus.WAITAPPROVE,
                WCStatus.APPROVING,
                WCStatus.RECONSIDERATION,
                WCStatus.EXEC,
                WCStatus.EXEC_FINISH,
                WCStatus.APPROVED)
                .stream()
                .map(each -> each.name())
                .collect(Collectors.toList());
    }

    public void clear() {
        init();
    }
}
