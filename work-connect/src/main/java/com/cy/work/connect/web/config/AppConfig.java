/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.config;

import com.cy.work.connect.web.util.SpringContextHolder;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.naming.NamingException;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.io.PathResource;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author brain0925_liao
 */
@Configuration("com.cy.work.connect.web.config.AppConfig")
@ComponentScan(value = {"com.cy.common", "com.cy.work.connect", "com.cy.work.backend", "com.cy.work.viewcomponent"})
@EnableScheduling
@PropertySources({})
@Import({
    SecurityConfig.class,
    com.cy.system.rest.client.config.SystemClientSpringAppConfig.class,
    com.cy.work.common.config.CommonConfig.class,
    com.cy.bpm.rest.client.config.RestClientConfig.class,
    com.cy.commons.web.config.AppConfig.class,
    com.cy.work.common.logic.lib.config.WorkCommonLogicLibConfig.class
})
public class AppConfig {

    @Bean
    public static PropertyPlaceholderConfigurer initProp() throws NamingException, IOException {
        String configHome = System.getenv("FUSION_CONFIG_HOME");
        Path urlPath = Paths.get(configHome, "url.properties");
        Path bpmRestPath = Paths.get(configHome, "bpm-rest.properties");
        Path workConnectPath = Paths.get(configHome, "work-connect.properties");
        // for work-common 2.6.7
        Path workCommonPath = Paths.get(configHome, "work-common.properties");
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer =
            new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
            new PathResource[]{
                new PathResource(urlPath),
                new PathResource(bpmRestPath),
                new PathResource(workConnectPath),
                new PathResource(workCommonPath)
            });
        // 開啟後@Value如果有預設值則會永遠使用預設值需注意...
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

    @Bean
    public SpringContextHolder springContextHolder() {
        return new SpringContextHolder();
    }
}
