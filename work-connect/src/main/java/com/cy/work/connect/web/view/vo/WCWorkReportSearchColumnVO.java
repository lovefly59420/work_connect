/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao 工作聯絡單搜尋欄位介面物件
 */
public class WCWorkReportSearchColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6558519130809035731L;
    /**
     * 序
     */
    @Setter
    @Getter
    private ColumnDetailVO index;
    /**
     * 部
     */
    @Setter
    @Getter
    private ColumnDetailVO fowardDep;
    /**
     * 個
     */
    @Setter
    @Getter
    private ColumnDetailVO fowardMember;
    /**
     * 追
     */
    @Setter
    @Getter
    private ColumnDetailVO trace;
    /**
     * 追
     */
    @Setter
    @Getter
    private ColumnDetailVO connect;
    /**
     * 建立日期
     */
    @Setter
    @Getter
    private ColumnDetailVO createTime;
    /**
     * 異動日期
     */
    @Setter
    @Getter
    private ColumnDetailVO modifyTime;
    /**
     * 是否閱讀
     */
    @Setter
    @Getter
    private ColumnDetailVO readed;
    /**
     * 標籤
     */
    @Setter
    @Getter
    private ColumnDetailVO tag;
    /**
     * 部門
     */
    @Setter
    @Getter
    private ColumnDetailVO department;
    /**
     * 部門
     */
    @Setter
    @Getter
    private ColumnDetailVO execDepartment;
    /**
     * 建立人員
     */
    @Setter
    @Getter
    private ColumnDetailVO createUser;
    /**
     * 主題
     */
    @Setter
    @Getter
    private ColumnDetailVO theme;
    /**
     * 狀態
     */
    @Setter
    @Getter
    private ColumnDetailVO status;

    @Setter
    @Getter
    private ColumnDetailVO readRecipt;
    @Setter
    @Getter
    private ColumnDetailVO readRecipted;
    /**
     * 單號
     */
    @Setter
    @Getter
    private ColumnDetailVO no;

    @Setter
    @Getter
    private String pageCount = "50";
}
