/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.controller;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.web.view.vo.UserViewVO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
@FacesConverter("userViewVOConverter")
public class UserViewVOConverter implements Converter {

    @Autowired
    private WkUserCache userManager;

    @Autowired
    private WkOrgCache orgManager;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if ((value != null) && !"".equals(value.trim())) {
                User user = userManager.findBySid(Integer.valueOf(value));
                Org dep = orgManager.findBySid(user.getPrimaryOrg().getSid());
                return new UserViewVO(
                    user.getSid(),
                    user.getName(),
                    WkOrgUtils.prepareBreadcrumbsByDepName(
                        dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"));
            }
        } catch (Exception e) {
            log.warn("UserViewVOConverter Error", e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof UserViewVO) {
            return String.valueOf(((UserViewVO) value).getSid());
        }
        return "";
    }
}
