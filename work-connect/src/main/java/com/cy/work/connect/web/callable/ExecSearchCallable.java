/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.callable;

import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WorkFunItemComponentType;
import com.cy.work.connect.web.logic.components.search.ProcessListLogicComponent;
import com.cy.work.connect.web.view.vo.search.query.ProcessListQuery;
import com.google.common.collect.Lists;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 * 處理清單Call able
 *
 * @author brain0925_liao
 */
@Slf4j
public class ExecSearchCallable implements HomeCallable {

    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 選單需計算筆數列舉
     */
    private final WorkFunItemComponentType workFunItemComponentType;
    /**
     * 選單Group物件
     */
    private final WorkFunItemGroupVO workFunItemGroupVO;

    public ExecSearchCallable(
            WorkFunItemGroupVO workFunItemGroupVO,
            WorkFunItemComponentType workFunItemComponentType,
            Integer loginUserSid) {
        this.workFunItemGroupVO = workFunItemGroupVO;
        this.workFunItemComponentType = workFunItemComponentType;
        this.loginUserSid = loginUserSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {
        ProcessListQuery query = new ProcessListQuery();
        query.init(String.valueOf(loginUserSid));
        List<String> execUserSids = Lists.newArrayList();
        execUserSids.add(query.getExecUserSid());
        Integer count = 0;
        try {
            count = ProcessListLogicComponent.getInstance().count(loginUserSid);
        } catch (Exception e) {
            log.warn("SendSearchCallable ERROR", e);
        }
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + count + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWorkFunItemComponentType(workFunItemComponentType);
        reuslt.setWorkFunItemGroupVO(workFunItemGroupVO);
        return reuslt;
    }
}
