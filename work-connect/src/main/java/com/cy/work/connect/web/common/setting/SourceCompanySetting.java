/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.setting;

import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.util.SpringContextHolder;
import com.google.common.collect.Lists;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * 項目批次匯入-來源公司(由類別啟用項目中取得)
 *
 * @author jimmy_chou
 */
public class SourceCompanySetting {

    /**
     * 來源公司選項List
     */
    private static List<SelectItem> sourceCompany;

    /**
     * 取得來源公司選項資料
     */
    public static List<SelectItem> getSourceCompanyItems() {
        sourceCompany = Lists.newArrayList();
        WCCategoryTagLogicComponent wcCategoryTagLogicComponent =
            SpringContextHolder.getBean(WCCategoryTagLogicComponent.class);

        wcCategoryTagLogicComponent
            .findDistinctCompSidByActiveStatus()
            .forEach(
                each -> {
                    sourceCompany.add(new SelectItem(each.getSid(), each.getName()));
                });
        return sourceCompany;
    }
}
