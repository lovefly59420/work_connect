package com.cy.work.connect.web.view.vo.search.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.logic.lib.repository.NativeSqlRepository;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.helper.WaitAssignLogic;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.repository.result.OrderListResult;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.OrderListColumn;
import com.cy.work.connect.web.common.clause.DateIntervalClause;
import com.cy.work.connect.web.common.clause.OrderListQueryClause;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.OrderListColumnVO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class OrderListHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2200315853557745809L;

    @Autowired
    private transient CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private transient WCTagManager wcTagManager;
    @Autowired
    private transient NativeSqlRepository nativeSqlRepository;
    @Autowired
    private WCMasterHelper wcMasterHelper;

    /**
     * 以下列條件查詢
     *
     * @param clause     查詢條件
     * @param userViewVO UserViewVO
     * @param wcSid      主檔 sid
     * @return OrderListResult list
     */
    public List<OrderListResult> queryByCondition(
            OrderListQueryClause clause, Integer loginUserSid, String wcSid) {

        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        if (loginUser == null) {
            return Lists.newArrayList();
        }
        Org primaryOrg = WkOrgCache.getInstance().findBySid(loginUser.getPrimaryOrg().getSid());
        if (primaryOrg == null) {
            return Lists.newArrayList();
        }

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";

        // 主題、內容
        String searchKeywordForThemeAndContent = WkStringUtils.safeTrim(clause.getSearchTextForThemeAndContent());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeywordForThemeAndContent)) {
            forceWcNo = searchKeywordForThemeAndContent;
        }

        // 模糊搜尋
        String searchKeyword = WkStringUtils.safeTrim(clause.getSearchText());
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 為指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(wcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 準備固定查詢條件
        // ====================================
        // 參數
        Map<String, Object> parameters = Maps.newHashMap();

        // 登入者 SID
        parameters.put("loginUserSid", loginUserSid);

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT master.wc_sid          AS wcSid, ");
        sql.append("       master.create_dt       AS createDate, ");
        sql.append("       category.category_name AS categoryName, ");
        sql.append("       menu.menu_name         AS menuTagName, ");
        sql.append("       master.create_usr      AS userSid, ");
        sql.append("       master.theme           AS theme, ");
        sql.append("       master.wc_status       AS status, ");
        sql.append("       master.wc_no           AS wcNo, ");
        sql.append("       master.category_tag    AS wcTagSids, ");
        sql.append("       master.reply_not_read  AS replyNotRead, ");
        sql.append("       master.content ");
        sql.append("FROM   wc_master master ");
        sql.append("       LEFT JOIN wc_menu_tag menu ");
        sql.append("              ON menu.wc_menu_tag_sid = master.menuTag_sid ");
        sql.append("       LEFT JOIN wc_category_tag category ");
        sql.append("              ON category.wc_category_tag_sid = menu.wc_category_tag_sid ");
        sql.append("WHERE  master.comp_sid = " + primaryOrg.getCompany().getSid() + " ");

        // ====================================
        // 限制可閱條件
        // ====================================

        sql.append(" AND  (  ");
        // ---------------------
        // 單據申請人
        // ---------------------
        sql.append("          master.create_usr = :loginUserSid ");

        // ---------------------
        // 使用者所有管理的單位, 包含管理單位子部門
        // ---------------------
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            parameters.put("managerDepSids", managerOrgSids);
            sql.append("          OR master.dep_sid IN ( :managerDepSids ) ");
        }

        // ---------------------
        // 可閱名單 wc_read_receipts
        // ---------------------
        sql.append("          OR EXISTS (SELECT 1 ");
        sql.append("                       FROM wc_read_receipts rpt ");
        sql.append("                      WHERE rpt.wc_sid = master.wc_sid ");
        sql.append("                        AND rpt.reader = :loginUserSid ) ");

        // ---------------------
        // 可派工人員
        // ---------------------
        // 取得可派工的執行單位檔
        Set<String> canAssignWcExecDepSids = WaitAssignLogic.getInstance().findUserCanAssignWcExecDepSids(
                SecurityFacade.getUserSid());

        if (WkStringUtils.notEmpty(canAssignWcExecDepSids)) {
            // 避免sid 過多, 再成查詢錯誤，直接組成 SQL 用的 string
            String canAssignWcExecDepSidsStr = canAssignWcExecDepSids.stream()
                    .collect(Collectors.joining("', '", "'", "'"));

            sql.append("          OR EXISTS (SELECT 1 ");
            sql.append("                       FROM wc_exec_dep execdep ");
            sql.append("                       WHERE  execdep.wc_sid = master.wc_sid ");
            sql.append("                         AND  execdep.wc_exec_dep_sid IN (" + canAssignWcExecDepSidsStr + ") ");
            sql.append("                     ) ");
        }

        // ---------------------
        // 執行單位可閱 wc_exec_dep
        // ---------------------
        // 取得執行單位相關人員可閱SQL
        String execCanViewConditionSql = RelationDepHelper.prepareConditionSqlForExecCanView(loginUserSid);
        sql.append("          OR EXISTS (SELECT 1 ");
        sql.append("                       FROM wc_exec_dep execdep ");
        sql.append("                       WHERE  execdep.wc_sid = master.wc_sid ");
        sql.append("                         AND  " + execCanViewConditionSql + " ");
        sql.append("                     ) ");

        // ---------------------
        // 需求方簽核人員
        // ---------------------
        // 需求方簽核資訊 wc_manager_sign_info
        // 待簽或者是最後一個節點
        sql.append("          OR EXISTS (SELECT 1 ");
        sql.append("                       FROM wc_manager_sign_info msi ");
        sql.append("                       WHERE msi.wc_sid = master.wc_sid ");
        sql.append("                         AND msi.bpm_instance_id != '' ");
        sql.append("                         AND msi.status_code != 'DELETE' ");
        sql.append("                         AND msi.user_sid = :loginUserSid ) ");

        // 需求方簽核歷史資料 wc_manager_sign_info_detail
        // 需求方曾經簽過的人
        sql.append("          OR EXISTS (SELECT 1 ");
        sql.append("                       FROM wc_manager_sign_info msi ");
        sql.append("                       WHERE msi.wc_sid = master.wc_sid ");
        sql.append("                         AND msi.bpm_instance_id != '' ");
        sql.append("                         AND EXISTS (SELECT msid.wc_manager_sign_info_detail_sid ");
        sql.append("                                       FROM wc_manager_sign_info_detail msid ");
        sql.append("                                      WHERE msid.wc_manager_sign_info_sid = msi.wc_manager_sign_info_sid ");
        sql.append("                                        AND msid.user_sid = :loginUserSid )) ");

        // ---------------------
        // 執行方簽核人員
        // ---------------------
        // 執行方簽核資訊 wc_exec_manager_sign_info
        // 待簽或者是最後一個節點
        sql.append("           OR EXISTS (SELECT 1 ");
        sql.append("                       FROM wc_exec_manager_sign_info emsi ");
        sql.append("                       WHERE emsi.wc_sid = master.wc_sid ");
        sql.append("                         AND emsi.status_code != 'DELETE' ");
        sql.append("                         AND emsi.bpm_instance_id != '' ");
        sql.append("                         AND emsi.user_sid = :loginUserSid) ");
        // 執行方簽核歷史資料 wc_exec_manager_sign_info_detail
        // 執行方曾經簽過的人
        sql.append("          OR EXISTS (SELECT 1 ");
        sql.append("                       FROM wc_exec_manager_sign_info emsi ");
        sql.append("                       WHERE emsi.wc_sid = master.wc_sid ");
        sql.append("                         AND emsi.status_code != 'DELETE' ");
        sql.append("                         AND emsi.bpm_instance_id != '' ");
        sql.append("                         AND EXISTS (SELECT emsid.wc_exec_manager_sign_info_detail_sid ");
        sql.append("                                       FROM wc_exec_manager_sign_info_detail emsid ");
        sql.append("                                      WHERE emsid.wc_exec_manager_sign_info_sid = emsi.wc_exec_manager_sign_info_sid ");
        sql.append("                                        AND emsid.user_sid = :loginUserSid )) ");
        sql.append("      ) ");

        // ====================================
        // 準備查詢條件
        // ====================================
        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(wcSid)) {
                sql.append("  AND master.wc_sid = '" + wcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND master.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 大項
        // ----------------------------
        if (WkStringUtils.notEmpty(clause.getCategorySid())) {
            sql.append("  AND  category.wc_category_tag_sid = :categorySid ");
            parameters.put("categorySid", clause.getCategorySid());
        }

        // ----------------------------
        // 單據狀態
        // ----------------------------
        if (!isForceSearch
                && WkStringUtils.notEmpty(clause.getStatusList())) {
            String wcStatusConditionSQL = clause.getStatusList().stream()
                    .collect(Collectors.joining("','", "'", "'"));
            sql.append("  AND  master.wc_status IN ( " + wcStatusConditionSQL + " ) ");
            // parameters.put("statusList", clause.getStatusList());
        }

        // ----------------------------
        // 建立區間
        // ----------------------------
        DateIntervalClause dateIntervalClause = clause.getDateIntervalClause();
        if (!isForceSearch
                && dateIntervalClause != null) {
            // 起日
            if (dateIntervalClause.getStartDate() != null) {
                sql.append("  AND master.create_dt >= :startDate ");
                String startDate = new DateTime(dateIntervalClause.getStartDate()).toString(
                        "yyyy-MM-dd");
                startDate += " 00:00:00";
                parameters.put("startDate", startDate);
            }
            // 迄日
            if (dateIntervalClause.getEndDate() != null) {
                sql.append("  AND master.create_dt <= :endDate ");
                String endDate = new DateTime(dateIntervalClause.getEndDate()).toString(
                        "yyyy-MM-dd");
                endDate += " 23:59:59";
                parameters.put("endDate", endDate);
            }
        }

        // 主題、內容
        if (WkStringUtils.notEmpty(searchKeywordForThemeAndContent)) {
            String searchText = searchKeywordForThemeAndContent.toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(master.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(master.content)", searchText));
            sql.append("      ) ");
        }

        // 模糊查詢
        if (WkStringUtils.notEmpty(clause.getSearchText())) {
            String searchText = WkStringUtils.safeTrim(clause.getSearchText()).toLowerCase();

            sql.append(" AND  (  ");
            // 申請人
            List<Integer> userSids = WkUserUtils.findByNameLike(searchText, SecurityFacade.getCompanyId());
            if (!WkStringUtils.isEmpty(userSids)) {
                sql.append(" master.create_usr IN (:userSids) ");
                parameters.put("userSids", userSids.isEmpty() ? "" : userSids);
                sql.append("         OR  ");
            }

            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(master.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(master.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "master.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));
            sql.append("      ) ");
        }

        // ====================================
        // SQL tail
        // ====================================
        sql.append("GROUP  BY master.wc_sid ");
        sql.append("ORDER  BY master.create_dt DESC");

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("所有單據查詢"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\nPARAMs:【\r\n" + com.cy.work.common.utils.WkJsonUtils.getInstance().toPettyJson(parameters) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        // 查詢
        Long startTime = System.currentTimeMillis();
        List<OrderListResult> resultList = this.nativeSqlRepository.getResultList(
                sql.toString(), parameters, OrderListResult.class);

        log.info(WkCommonUtils.prepareCostMessage(startTime, "所有單據查詢，共[" + resultList.size() + "]筆"));
        if (WkStringUtils.isEmpty(resultList)) {
            return Lists.newArrayList();
        }

        // 整理回傳值
        startTime = System.currentTimeMillis();
        this.convert(resultList, loginUserSid);
        log.info(WkCommonUtils.prepareCostMessage(startTime, "整理回傳值"));

        // ====================================
        // 作廢只有自己能看到
        // ====================================
        return resultList.stream()
                .filter(result -> {
                    // 為作廢單據才比對
                    if (!WCStatus.INVALID.getVal().equals(result.getStatus())) {
                        return true;
                    }
                    // 比對單據申請者是否為登入者
                    return WkCommonUtils.compareByStr(result.getUserSid(), loginUserSid);
                })
                .collect(Collectors.toList());
    }

    private void convert(List<OrderListResult> resultLists, Integer loginUserSid) {

        // ====================================
        // 防呆
        // ====================================
        if (WkStringUtils.isEmpty(resultLists)) {
            return;
        }

        // ====================================
        // 最大 100 個執行序
        // ====================================
        int poolNum = 100;
        if (resultLists.size() < poolNum) {
            poolNum = resultLists.size();
        }

        // ====================================
        // 多執行序執行
        // ====================================
        List<OrderListResultConvertCallable> callables = Lists.newArrayList();
        try {
            ExecutorService pool = Executors.newFixedThreadPool(poolNum);
            try {

                CompletionService<OrderListResult> completionPool = new ExecutorCompletionService<OrderListResult>(pool);

                for (int resultIndex = 0; resultIndex < resultLists.size(); resultIndex++) {

                    OrderListResultConvertCallable callable = new OrderListResultConvertCallable(
                            resultLists.get(resultIndex), loginUserSid, resultIndex);

                    callables.add(callable);
                    completionPool.submit(callable);
                }

                // 等待全部執行完成
                IntStream.range(0, callables.size())
                        .forEach(
                                i -> {
                                    try {
                                        completionPool.take();
                                    } catch (Exception e) {
                                        log.warn("completionPool.take", e);
                                    }
                                });

            } catch (Exception e) {
                log.error("OrderListResultConvertCallable", e);
            } finally {
                pool.shutdown();
            }
        } catch (Exception e) {
            log.error("OrderListResultConvertCallable", e);
        }
    }

    /**
     * 載入data table欄位寬度
     *
     * @param userSid
     * @return
     */
    public OrderListColumnVO getOrderListColumnVO(Integer userSid) {
        WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.WORK_ORDER_LIST;
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
                userSid);
        OrderListColumnVO vo = new OrderListColumnVO();
        if (columDB != null) {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setApplicant(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.APPLICATION_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.THEME, columDB.getInfo()));
            vo.setStatusName(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.STATUS_NAME, columDB.getInfo()));
            vo.setWcNo(
                    customColumnLogicComponent.transToColumnDetailVO(
                            OrderListColumn.WC_NO, columDB.getInfo()));
        }
        return vo;
    }

    public class OrderListResultConvertCallable implements Callable<OrderListResult> {

        private final OrderListResult orderListResult;
        private final Integer loginUserSid;

        public OrderListResultConvertCallable(
                OrderListResult orderListResult, Integer loginUserSid, Integer resultIndex) {
            this.orderListResult = orderListResult;
            this.loginUserSid = loginUserSid;
            this.orderListResult.setResultIndex(resultIndex);
        }

        @Override
        public OrderListResult call() throws Exception {

            try {
                // 單據狀態
                String status = WkStringUtils.safeTrim(orderListResult.getStatus());
                WCStatus wcStatus = WCStatus.valueOf(status);
                if (wcStatus != null) {
                    orderListResult.setStatusName(wcStatus.getVal());
                } else {
                    log.error("未定義的單據狀態WCStatus[{}]", status);
                }

                // System.out.printf(
                // "【%s】status:【%s】,【%s】%n",
                // orderListResult.getWcSid(), status, WCStatus.valueOf(status).getVal());

                orderListResult.setStatus(
                        WCStatus.valueOf(WkStringUtils.safeTrim(orderListResult.getStatus())).getVal());
                // 主題
                // -- 去控制字元
                String theme = WkStringUtils.removeCtrlChr(orderListResult.getTheme());
                // -- 組小類標籤
                CategoryTagTo ct = WkJsonUtils.getInstance()
                        .fromJson(orderListResult.getWcTagSids(), CategoryTagTo.class);
                if (WkStringUtils.notEmpty(ct.getTagSids())) {
                    List<String> tagNames = Lists.newArrayList();
                    for (String sid : ct.getTagSids()) {
                        WCTag wcTag = wcTagManager.getWCTagBySid(sid);
                        tagNames.add(wcTag.getTagName());
                    }
                    theme = String.format("%s (%s)", theme,
                            tagNames.stream().collect(Collectors.joining(", ")));
                }
                orderListResult.setTheme(theme);

                // 申請人名稱
                orderListResult.setApplicant(
                        WkUserUtils.findNameBySid(orderListResult.getUserSid()));

                // 未讀
                if (WkStringUtils.notEmpty(orderListResult.getReplyNotRead())) {
                    try {
                        List<ReplyRecordTo> resultRList = WkJsonUtils.getInstance()
                                .fromJsonToList(orderListResult.getReplyNotRead(),
                                        ReplyRecordTo.class);
                        if (WkStringUtils.notEmpty(resultRList)) {
                            List<ReplyRecordTo> replyRecordTos = resultRList.stream()
                                    .filter(eu -> String.valueOf(loginUserSid).equals(eu.getUser()))
                                    .collect(Collectors.toList());
                            if (WkStringUtils.notEmpty(replyRecordTos)) {
                                orderListResult.setHasUnReadCount(true);
                                orderListResult.setUnReadCount(
                                        replyRecordTos.get(0).getUnReadCount());
                            }
                        }
                    } catch (Exception e) {
                        log.warn("convert ERROR", e);
                    }
                }
            } catch (Exception e) {
                log.warn(e.getMessage(), e);
            }

            return orderListResult;
        }
    }
}
