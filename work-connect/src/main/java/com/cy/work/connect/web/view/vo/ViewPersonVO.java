/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class ViewPersonVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6754150710414812508L;
    /**
     * User SID
     */
    @Getter
    private final Integer sid;
    /**
     * User Name
     */
    @Getter
    private final String name;
    /**
     * 部門名稱
     */
    @Getter
    private final String departmentName;

    @Getter
    private final String readStatus;
    @Getter
    private final String readStatusCss;

    public ViewPersonVO(
        Integer sid, String name, String departmentName, String readStatus, String readStatusCss) {
        this.sid = sid;
        this.name = name;
        this.departmentName = departmentName;
        this.readStatus = readStatus;
        this.readStatusCss = readStatusCss;
    }
}
