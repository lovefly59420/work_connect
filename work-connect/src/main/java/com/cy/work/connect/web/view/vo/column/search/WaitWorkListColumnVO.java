/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column.search;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Data;

/**
 * 待派工清單 - dataTable 欄位資訊
 *
 * @author kasim
 */
@Data
public class WaitWorkListColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4460259785520179229L;
    /**
     * 一頁 X 筆
     */
    private String pageCount = "50";
    /**
     * 序
     */
    private ColumnDetailVO index;
    /**
     * 需求成立日
     */
    private ColumnDetailVO requireEstablishDate;
    /**
     * 需求單位
     */
    private ColumnDetailVO requireDepName;
    /**
     * 需求人員
     */
    private ColumnDetailVO requireUserName;
    /**
     * 單據名稱(第二層)
     */
    private ColumnDetailVO menuTagName;
    /**
     * 主題
     */
    private ColumnDetailVO theme;
    /**
     * 單號
     */
    private ColumnDetailVO wcNo;
}
