/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import lombok.Data;

/**
 * @author brain0925_liao
 */
@Data
public class TagGroupSignSettingVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3324858271494032151L;

    private String sid;

    private String tagsName = "";

    private String memo;

    private String status;
}
