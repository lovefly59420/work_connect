/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.work.connect.logic.manager.WCReportCustomColumnManager;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.WCReportCustomColumnInfo;
import com.cy.work.connect.vo.converter.to.WCReportCustomColumnInfoTo;
import com.cy.work.connect.vo.enums.Column;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author kasim
 */
@Component
public class CustomColumnLogicComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1846303566678959443L;

    @Autowired
    private WCReportCustomColumnManager customColumnManager;

    /**
     * 取得自訂欄位設定 By Url 及 登入者Sid
     *
     * @param urlType
     * @param userSid
     * @return
     */
    public WCReportCustomColumn findCustomColumn(
        WCReportCustomColumnUrlType urlType, Integer userSid) {
        return customColumnManager.getWCReportCustomColumnByUrlAndUserSid(urlType, userSid);
    }

    /**
     * 建立欄位介面物件
     *
     * @param column
     * @return
     */
    public ColumnDetailVO createDefaultColumnDetail(Column column) {
        return new ColumnDetailVO(column.getVal(), column.getDefaultShow(),
            column.getDefaultWidth());
    }

    /**
     * 轉換自訂義
     *
     * @param column
     * @param info
     * @return
     */
    public ColumnDetailVO transToColumnDetailVO(Column column, WCReportCustomColumnInfo info) {
        if (info != null
            && info.getWkReportCustomColumnInfoTos() != null
            && info.getWkReportCustomColumnInfoTos().size() > 0) {
            // 搜尋資料庫是否有該欄位記錄長度資料
            List<WCReportCustomColumnInfoTo> wkReportCustomColumnInfoTo =
                info.getWkReportCustomColumnInfoTos().stream()
                    .filter(each -> each.getName().equals(column.getVal()))
                    .collect(Collectors.toList());
            if (wkReportCustomColumnInfoTo != null && wkReportCustomColumnInfoTo.size() > 0) {
                // 若資料庫有值代表,有紀錄需顯示,還需確認改欄位是否可改寬度,若不可改寬度,需顯示預設
                return new ColumnDetailVO(
                    column.getVal(),
                    true,
                    (column.getCanModifyWidth())
                        ? wkReportCustomColumnInfoTo.get(0).getWidth()
                        : column.getDefaultWidth());
            } else if (column.getCanSelectItem()) {
                // 若該欄位是可被選擇是否不顯示,在資料庫欄位未發現,代表使用者有變更將其變為不顯示
                return new ColumnDetailVO(column.getVal(), false, "");
            } else {
                // 若在資料庫欄位未發現,但該欄位是不可被選擇不顯示的,代表須顯示,寬度為預設
                return new ColumnDetailVO(column.getVal(), true, column.getDefaultWidth());
            }
        } else {
            return new ColumnDetailVO(column.getVal(), column.getDefaultShow(),
                column.getDefaultWidth());
        }
    }
}
