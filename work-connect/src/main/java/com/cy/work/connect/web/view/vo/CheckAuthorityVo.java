package com.cy.work.connect.web.view.vo;

import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.FlowType;
import java.util.List;
import lombok.Data;

/**
 * @author marlow_chen
 */
@Data
public class CheckAuthorityVo {

    /* 類別 (設定檔類別名稱) wc_category_tag - category_name*/
    private String tagId;
    private String tagName;
    private String showTagName;

    //    /* 類別--群組化的數量*/
    //    private int tagRow;
    //
    //    /* 類別--是否顯示*/
    //    private String isTagRowShow;
    /* 單據名稱 wc_menu_tag - menu_name*/
    private String menuTagId;
    private String menuTagName;
    private String showMenuTagName;

    /* 可使用單位 (可以使用的人) wc_menu_tag - use_dep*/
    private String whoCanUse;
    private List<Integer> whoCanUseSid;
    private boolean showWhoCanUseTemplet;
    private String whoCanUseTooltip;

    /* 強制簽核至上層主管 wc_menu_tag - isParentManager*/
    private String parentManager;

    /* 顯示分類 wc_tag - tagType */
    private String tagType;

    /* 勾選明細 (執行項目維護作業名稱) wc_tag - tag_name*/
    private String subTagId;
    private String subTagName;
    private String showSubTagName;

    /* 簽核層級 (設定檔預設簽核到誰--從執行項目維護設定內取得) wc_tag - reqFlowType*/
    private String reqSignInfo;
    private FlowType flowType;

    /* 合法區間 wc_tag - legitimate_range_days,legitimate_range_title,legitimate_range_files */
    private String legitimateRange;

    /* 起訖日 wc_tag - start_end_days,start_end_title,start_end_files */
    private String startEnd;

    /* 可閱部門 wc_tag - use_dep*/
    private String whoCanRead;
    private List<Integer> whoCanReadSid;
    private boolean showWhoCanReadTemplet;
    private String whoCanReadTooltip;

    /* 執行單位 (執行單位) wc_exec_dep_setting - 784*/
    private String execDep;
    private String execDepAll;
    private List<Integer> execDepSid;

    /* 執行單位簽核人員 (執行單位簽核的人) wc_exec_dep_setting - execSignMember*/
    private String execDepUsers;

    /* 執行模式 (走領單或者派工) wc_exec_dep_setting - isNeedAssigned isReadAssignSetting*/
    private String execMethod;

    /* 派工人員 (派工的人) wc_exec_dep_setting - transUser*/
    private String assignUsers;

    /* 領單人員List (判斷權限用) */
    private UsersTo receviceUserList;

    /* 領單人員 */
    private String receviceUsers;

    /* 管理領單人員設定 */
    private String receviceManager;

    /* 中佑對應窗口 (判斷權限用) */
    private String cyWindow;

    /* 指派單位 */
    private String transdep;
    private List<Integer> transdepSid;

    /* 是否顯示可閱人員多筆按鈕 */
    private boolean showViewUsers;

    /* 可閱人員名稱 */
    private String viewUserNames;

    /* 可閱人員List */
    private List<Integer> viewUserSids;

    private List<Integer> useUserSids;

    /* 是否顯示可使用人員多筆按鈕 */
    private boolean showUseUsers;

    /* 可使用人員名稱 */
    private String useUserNames;
}
