/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.setting;

import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.stereotype.Component;

/**
 * @author kasim
 */
@Component
public class ExceStatusSetting implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2754566404757275779L;
    /**
     * 工執行單位目前的執行狀況選項
     */
    private List<SelectItem> searchItems;

    public List<SelectItem> getSearchItems() {
        if (searchItems != null) {
            return searchItems;
        }
        searchItems = Lists.newArrayList();
        Lists.newArrayList(WCExceDepStatus.values())
            .forEach(
                each -> {
                    searchItems.add(new SelectItem(each.name(), each.getValue()));
                });
        return searchItems;
    }
}
