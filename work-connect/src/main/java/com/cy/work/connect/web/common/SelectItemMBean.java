package com.cy.work.connect.web.common;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.springframework.stereotype.Controller;

import com.cy.work.connect.vo.enums.FlowType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 */
@Controller
public class SelectItemMBean {

    /**
     * 簽核層級選項
     * 
     * @return
     */
    public static List<SelectItem> getFlowTypes() {
        return Lists.newArrayList(FlowType.values()).stream()
                .filter(each -> each.isEnable()) // (排除停用)
                .map(each -> new SelectItem(each, each.getDesc()))
                .collect(Collectors.toList());
    }

    /**
     * 單據狀態
     * 
     * @return
     */
    public static List<SelectItem> getWCStatus() {
        //
        return Lists.newArrayList(WCStatus.values()).stream()
                .sorted(Comparator.comparing(WCStatus::getSeq))
                .map(each -> new SelectItem(each, each.getVal()))
                .collect(Collectors.toList());
    }
}
