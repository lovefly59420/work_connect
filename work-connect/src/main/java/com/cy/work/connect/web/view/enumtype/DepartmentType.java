/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.enumtype;

/**
 * @author brain0925_liao
 */
public enum DepartmentType {
    /**
     * 讀取需求記錄
     */
    ReadReceipts,
    /**
     * 需求單位加簽
     */
    ManagerConunterSignInfo,
    /**
     * 執行單位加簽
     */
    ExecManagerConunterSignInfo,
    /**
     * 建立部門
     */
    CreateDep,
    /**
     * 執行部門
     */
    ExecDep
}
