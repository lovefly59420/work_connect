/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search;

import com.cy.work.connect.vo.WCTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 待領單據清單 - 列表資料
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"sid"})
public class WaitWorkReceiveListVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8319237880557724004L;

    /**
     * key - 聯絡單 sid
     */
    private String sid;

    /**
     * 聯絡單 sid
     */
    private String wcSid;

    /**
     * 聯絡單 單號
     */
    private String wcNo;

    /**
     * 需求成立日
     */
    private String requireEstablishDate;

    /**
     * 需求單位
     */
    private String requireDepName;

    /**
     * 需求人員
     */
    private String requireUserName;

    /**
     * 單據名稱(第二層)
     */
    private String menuTagName;

    /**
     * 主題
     */
    private String theme;

    private Date createDate;
    /**
     * 類別資訊(第三層)
     */
    private String tagInfo = "";

    private List<String> tgSids = Lists.newArrayList();

    public WaitWorkReceiveListVO(String sid) {
        this.sid = sid;
        this.wcSid = sid;
    }

    public void replaceValue(WaitWorkReceiveListVO updateObject) {
        this.sid = updateObject.getSid();
        this.wcSid = updateObject.getWcSid();
        this.wcNo = updateObject.getWcNo();
        this.requireEstablishDate = updateObject.getRequireEstablishDate();
        this.requireDepName = updateObject.getRequireDepName();
        this.requireUserName = updateObject.getRequireUserName();
        this.menuTagName = updateObject.getMenuTagName();
        this.theme = updateObject.getTheme();
        this.tagInfo = updateObject.getTagInfo();
        this.tgSids = updateObject.getTgSids();
    }

    public void bulidExecTag(WCTag tag) {
        if (tag == null) {
            return;
        }
        if (tgSids.contains(tag.getSid())) {
            return;
        }
        tgSids.add(tag.getSid());
        if (!Strings.isNullOrEmpty(tagInfo)) {
            tagInfo += "、";
        }
        tagInfo += tag.getTagName();
    }
}
