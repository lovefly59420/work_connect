/**
 * 
 */
package com.cy.work.connect.web.view.setting10DepSettingCheck;

import java.io.Serializable;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class Setting10DepSettingCheckItemVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2234527992722161238L;

    @Getter
    private DepSettingCheckSettingType settingType = DepSettingCheckSettingType.NONE;

    /**
     * 大類名稱
     */
    @Getter
    private String firstLevelName;

    /**
     * 中類名稱
     */
    @Getter
    private String secondLevelName;

    /**
     * 小類名稱
     */
    @Getter
    private String thirdLevelName;

    /**
     * 說明
     */
    @Getter
    @Setter
    private String descr;

    @Getter
    private String categorySid;

    @Getter
    private String menuSid;

    @Getter
    private String tagSid;
    
    public Setting10DepSettingCheckItemVO(
            DepSettingCheckSettingType settingType) {
        this.settingType = settingType;
    }
    
    /**
     * 建構子
     * @param settingType
     * @param descr
     */
    public Setting10DepSettingCheckItemVO(
            DepSettingCheckSettingType settingType,
            String descr) {
        this.settingType = settingType;
        this.descr = descr;
    }
            

    /**
     * 建構子
     * @param settingType
     * @param wcCategoryTag
     * @param wcMenuTag
     * @param wcTag
     */
    public Setting10DepSettingCheckItemVO(
            DepSettingCheckSettingType settingType,
            String descr,
            WCCategoryTag wcCategoryTag,
            WCMenuTag wcMenuTag,
            WCTag wcTag 
            ) {

        this.settingType = settingType;
        this.descr = descr;

        // 大類
        if (wcCategoryTag != null) {
            // sid
            this.categorySid = wcCategoryTag.getSid();
            // 名稱
            this.firstLevelName = "【" + wcCategoryTag.getCategoryName() + "】";
            if (Activation.INACTIVE.equals(wcCategoryTag.getStatus())) {
                this.firstLevelName = WkHtmlUtils.addStrikethroughStyle(this.firstLevelName, "停用");
            }
        }

        // 中類
        if (wcMenuTag != null) {
            // sid
            this.menuSid = wcMenuTag.getSid();
            // 名稱
            this.secondLevelName = "【" + wcMenuTag.getMenuName() + "】";
            if (Activation.INACTIVE.equals(wcMenuTag.getStatus())) {
                this.secondLevelName = WkHtmlUtils.addStrikethroughStyle(this.secondLevelName, "停用");
            }
        }

        // 小類名稱
        if (wcTag != null) {
            // sid
            this.tagSid = wcTag.getSid();
            // 名稱
            this.thirdLevelName = "【" + wcTag.getTagName() + "】";
            if (Activation.INACTIVE.equals(wcTag.getStatus())) {
                this.thirdLevelName = WkHtmlUtils.addStrikethroughStyle(this.thirdLevelName, "停用");
            }
        }
    }

    public String getName() { return firstLevelName
            + (WkStringUtils.isEmpty(secondLevelName) ? "" : ("→" + secondLevelName))
            + (WkStringUtils.isEmpty(thirdLevelName) ? "" : ("→" + thirdLevelName)); }

}
