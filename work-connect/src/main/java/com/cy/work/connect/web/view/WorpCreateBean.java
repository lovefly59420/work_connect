/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.web.listener.CategoryTreeCallBack;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.components.CategoryTreeComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.google.common.base.Strings;
import java.io.File;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 建立畫面
 *
 * @author kasim
 */
@Controller
@Scope("view")
@Slf4j
public class WorpCreateBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 923901768508283679L;
    /**
     * 建立頁面 URL
     */
    private final String createUrl = "../worp/worp1.xhtml?categorySid=";

    @Autowired
    private DisplayController displayController;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Getter
    private CategoryTreeComponent categoryTreeComponent;

    @Getter
    /** 錯誤訊息 */
    private String errorMessage;
    /**
     * 訊息呼叫
     */
    private final CategoryTreeCallBack categoryTreeCallBack = new CategoryTreeCallBack() {
        /** */
        private static final long serialVersionUID = 2919837648881852842L;

        @Override
        public void showMessage(String message) {
            errorMessage = message;
            displayController.update("confimDlgTemplate");
            displayController.showPfWidgetVar("confimDlgTemplate");
        }

        @Override
        public void onNodeSelect() {
            String categorySid = categoryTreeComponent.getSelectedNodeSid();
            if (Strings.isNullOrEmpty(categorySid)) {
                showMessage("尚未選取資料!!");
                return;
            }
            displayController.execute("changeUrlByFrame('" + createUrl + categorySid + "')");
        }
    };

    @PostConstruct
    public void init() {
        categoryTreeComponent = new CategoryTreeComponent(categoryTreeCallBack);
        OrgViewVo compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());

        // 建立功能清單樹
        categoryTreeComponent.bulidTree(
                categoryTagLogicComponent.getCategoryTags(
                        compViewVo.getSid(), SecurityFacade.getUserSid()));
    }

    /**
     * download file or zip
     */
    public void exportFile() {
        if (categoryTreeComponent.isAttachementType()) {
            File file = new File(categoryTreeComponent.getAttachmentPath());
            if (file.exists()) {
                AttachmentUtils.exportFolderFiles(file);
            } else {
                log.warn("download fail..folder is not exists: {}", file.getAbsolutePath());
            }
        }
    }
}
