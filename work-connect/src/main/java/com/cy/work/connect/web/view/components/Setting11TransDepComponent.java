package com.cy.work.connect.web.view.components;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCTransManager;
import com.cy.work.connect.logic.vo.view.setting11.SettingOrgTransMappingVO;
import com.cy.work.connect.logic.vo.view.setting11.subfunc.Setting11TransDepVO;
import com.cy.work.connect.web.util.SpringContextHolder;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 指派單位相關資料轉檔
 *
 * @author jimmy_chou
 */
@Component
@Scope("view")
@Slf4j
public class Setting11TransDepComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5551723685450931150L;
    /**
     *
     */
    private final transient DisplayController displayController;
    /**
     *
     */
    private final transient WCTransManager transManager;
    /**
     *
     */
    private final transient WkOrgCache orgManager;
    /**
     * 元件頁面變數容器
     */
    @Getter
    @Setter
    private Setting11TransDepComponentPageVO cpontVO;

    /**
     * 建構子
     */
    public Setting11TransDepComponent() {
        this.displayController = SpringContextHolder.getBean(DisplayController.class);
        this.transManager = SpringContextHolder.getBean(WCTransManager.class);
        this.orgManager = SpringContextHolder.getBean(WkOrgCache.class);
    }

    /**
     * 初始化
     */
    public void init() {
        // 初始化VO
        this.cpontVO = new Setting11TransDepComponentPageVO();
    }

    /**
     * 開啟《指派單位轉檔》對話視窗
     *
     * @param selVerifyDtVO
     */
    public void openTrnsWindow(SettingOrgTransMappingVO selVerifyDtVO) {

        if (selVerifyDtVO == null) {
            MessagesUtils.showError("找不到選擇的資料!");
            return;
        }

        // 初始化本功能變數
        this.init();

        // 紀錄被選擇的轉換單位
        this.cpontVO.setSelVerifyDtVO(selVerifyDtVO);

        try {
            // ====================================
            // 查詢轉換單位設定資料
            // ====================================
            // 查詢
            this.cpontVO.setShowDtVOList(
                this.transManager.queryForTransDep(selVerifyDtVO.getBeforeOrgSid()));

            // ====================================
            // 頁面操作
            // ====================================
            // 更新顯示列表
            this.displayController.update("dlgTransDepForTransform_Datatable");
            // 顯示檢視編輯視窗
            this.displayController.showPfWidgetVar("dlgTransDepForTransform");
            // 清除篩選條件
            this.displayController.execute(
                "PF('dlgTransDepForTransform_Datatable').clearFilters();");

        } catch (Exception e) {
            String errMsg = "查詢時發生錯誤：" + e.getMessage();
            log.error(errMsg, e);
            MessagesUtils.showError(errMsg);
        }
    }

    /**
     * 查詢未轉檔資料
     */
    public void btnQuery() {

        if (this.getCpontVO() == null || this.getCpontVO().getSelVerifyDtVO() == null) {
            MessagesUtils.showError("資料已遺失, 請重新刷新頁面");
            return;
        }

        // 清空
        this.cpontVO.setSelectedDtVOList(null);

        try {
            // ====================================
            // 查詢轉換單位設定資料
            // ====================================
            // 查詢
            this.cpontVO.setShowDtVOList(
                this.transManager.queryForTransDep(
                    this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid()));

            // ====================================
            // 頁面操作
            // ====================================
            // 更新顯示列表
            this.displayController.update("dlgTransDepForTransform_Datatable");

        } catch (Exception e) {
            String errMsg = "查詢時發生錯誤：" + e.getMessage();
            log.error(errMsg, e);
            MessagesUtils.showError(errMsg);
        }
    }

    /**
     * 轉檔《已選則》資料
     */
    public void btnTrnsSelected() {
        // ====================================
        // 檢查
        // ====================================
        if (this.getCpontVO() == null || this.getCpontVO().getSelVerifyDtVO() == null) {
            MessagesUtils.showError("資料已遺失, 請重新刷新頁面");
            return;
        }

        if (WkStringUtils.isEmpty(this.getCpontVO().getSelectedDtVOList())) {
            MessagesUtils.showInfo("請選擇一筆資料");
            return;
        }

        log.debug("共選擇執行項目[" + this.getCpontVO().getSelectedDtVOList().size() + "]筆");

        // ====================================
        // 批次轉換
        // ====================================
        String errMsg = "";
        // 轉換前單位 sid
        Integer beforOrgSid = this.getCpontVO().getSelVerifyDtVO().getBeforeOrgSid();
        // 轉換後單位 sid
        Integer afterOrgSid = this.getCpontVO().getSelVerifyDtVO().getAfterOrgSid();

        try {

            this.transManager.trnsTransDep(
                this.getCpontVO().getSelectedDtVOList(),
                beforOrgSid,
                afterOrgSid,
                "組織異動：指派單位轉換",
                Optional.ofNullable(this.orgManager.findById(SecurityFacade.getCompanyId()))
                    .map(Org::getSid)
                    .orElseGet(() -> null),
                SecurityFacade.getPrimaryOrgSid(),
                SecurityFacade.getUserSid());

        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("轉檔時發生錯誤：{}", e.getMessage());
        } catch (Exception e) {
            errMsg = "轉檔時發生錯誤：" + e.getMessage();
            log.error(errMsg, e);
        }

        // ====================================
        // 更新畫面
        // ====================================
        this.btnQuery();

        // ====================================
        // 執行結果訊息
        // ====================================
        if (WkStringUtils.isEmpty(errMsg)) {
            MessagesUtils.showInfo("轉檔成功!");
        } else {
            MessagesUtils.showError(errMsg);
        }
    }

    /**
     * 本組件的變數容器 (VO)
     */
    public class Setting11TransDepComponentPageVO implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 6177171164389693989L;

        @Getter
        @Setter
        private SettingOrgTransMappingVO selVerifyDtVO;

        /**
         * Data table 顯示資料
         */
        @Getter
        @Setter
        private List<Setting11TransDepVO> showDtVOList;
        /**
         * Data table 的選擇資料
         */
        @Getter
        @Setter
        private List<Setting11TransDepVO> selectedDtVOList;

        /**
         * 建構子
         */
        public Setting11TransDepComponentPageVO() {
            this.showDtVOList = Lists.newArrayList();
            this.selectedDtVOList = Lists.newArrayList();
        }
    }
}
