/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search;

import com.cy.work.connect.vo.WCTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 申請單位查詢 - 列表資料
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"sid"})
public class ApplicationDepInquireVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8476220318707426214L;

    /**
     * key - 聯絡單 sid
     */
    private String sid;

    /**
     * 聯絡單 sid
     */
    private String wcSid;

    /**
     * 聯絡單 單號
     */
    private String wcNo;

    /**
     * 建立日期
     */
    private String createDate;

    private Date createDt;

    /**
     * 類別(第一層)
     */
    private String categoryName;

    /**
     * 單據名稱(第二層)
     */
    private String menuTagName;

    /**
     * 申請人
     */
    private String applicationUserName;
    
    /**
     * 申請部門
     */
    private String depName;

    /**
     * 主題
     */
    private String theme;

    private String content;

    /**
     * 單據狀態
     */
    private String statusName;

    /**
     * 類別資訊(第三層)
     */
    private String tagInfo = "";

    private List<String> tgSids = Lists.newArrayList();

    private String unReadCount;

    private boolean hasUnReadCount = false;

    public ApplicationDepInquireVO(String sid) {
        this.sid = sid;
        this.wcSid = sid;
    }

    public void replaceValue(ApplicationDepInquireVO updateObject) {
        this.sid = updateObject.getSid();
        this.wcSid = updateObject.getWcSid();
        this.wcNo = updateObject.getWcNo();
        this.createDate = updateObject.getCreateDate();
        this.menuTagName = updateObject.getMenuTagName();
        this.categoryName = updateObject.getCategoryName();
        this.applicationUserName = updateObject.getApplicationUserName();
        this.theme = updateObject.getTheme();
        this.statusName = updateObject.getStatusName();
        this.tagInfo = updateObject.getTagInfo();
        this.tgSids = updateObject.getTgSids();
        this.createDt = updateObject.getCreateDt();
        this.unReadCount = updateObject.getUnReadCount();
        this.hasUnReadCount = updateObject.isHasUnReadCount();
    }

    public void bulidExecTag(WCTag tag) {
        if (tag == null) {
            return;
        }
        if (tgSids.contains(tag.getSid())) {
            return;
        }
        tgSids.add(tag.getSid());
        if (!Strings.isNullOrEmpty(tagInfo)) {
            tagInfo += "、";
        }
        tagInfo += tag.getTagName();
    }
}
