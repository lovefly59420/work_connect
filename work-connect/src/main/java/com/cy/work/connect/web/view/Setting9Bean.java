package com.cy.work.connect.web.view;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.DepTreePickerComponent;
import com.cy.work.connect.web.common.MultipleALLTagTreeManager;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.common.MultipleDepTreePickerManager;
import com.cy.work.connect.web.common.MultipleTagTreeManager;
import com.cy.work.connect.web.logic.components.CheckAuthorityComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCExecDepSettingLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCTagDataTableComponent;
import com.cy.work.connect.web.logic.components.WCTagLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.CategoryTagSearchVO;
import com.cy.work.connect.web.view.vo.ExecDepSettingSearchVO;
import com.cy.work.connect.web.view.vo.MenuTagSearchVO;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.TagDtVO;
import com.cy.work.connect.web.view.vo.TagSearchVO;
import com.cy.work.connect.web.view.vo.TagVO;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.search.query.CheckAuthoritySearchQuery;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIData;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

/**
 * 核決權限維護 MBean
 *
 * @author jimmy_chou
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting9Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8241676258500082812L;

    @Autowired
    private Setting3Bean setting3Bean;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private CheckAuthorityComponent checkAuthorityComponent;
    @Autowired
    private WCTagLogicComponents wCTagLogicComponents;
    @Autowired
    private WCMenuTagLogicComponent wCMenuTagLogicComponent;
    @Autowired
    private DisplayController displayController;
    @Getter
    private MultipleALLTagTreeManager allTagTreeManager;

    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Getter
    @Setter
    private Boolean showTree;
    @Getter
    @Setter
    private Boolean checkAll;
    @Getter
    @Setter
    private Boolean showDisable;

    /**
     * 核決權限維護 DataTable
     */
    @Getter
    @Setter
    private List<TagDtVO> allTagDt;
    /**
     * 查詢物件
     */
    @Getter
    private CheckAuthoritySearchQuery query;

    /**
     * 類別List
     */
    @Getter
    private List<SelectItem> categoryTagList;

    /**
     * 單據名稱List
     */
    @Getter
    private List<SelectItem> menuTagList;

    /**
     * 執行項目List
     */
    @Getter
    private List<SelectItem> subTagList;

    /**
     * 申請流程類型List
     */
    @Getter
    private List<SelectItem> flowTypeList;

    /**
     * view 顯示錯誤訊息
     */
    @Getter
    private String errorMessage;

    @Getter
    @Setter
    private TagDtVO editRow;

    private Integer editIndex;

    @Getter
    @Setter
    private TreeNode editNode;

    private String editRowKey;

    private List<Org> allOrgs;

    private List<Org> execOrgsPicker;
    private List<User> execUsersPicker;

    private List<Org> assignOrgsPicker;
    private List<User> assignUsersPicker;

    private MultipleTagTreeManager multipleTagTreeManager;
    private DepTreeComponent viewDepTreeComponent;
    private DepTreeComponent canUseDepTreeComponent;
    private DepTreeComponent execDepTreeComponent;
    private DepTreeComponent relateDepTreeComponent;
    private DepTreePickerComponent execDepTreePickerComponent;
    private DepTreePickerComponent assignDepTreePickerComponent;
    private List<Org> selectViewOrgs;

    /* 可閱人員列表 */
    @Getter
    private List<UserViewVO> viewUsers;
    /* 可使用人員列表 */
    @Getter
    private List<UserViewVO> useUsers;
    /* 可閱部門 */
    private DepTreeComponent queryViewDepTreeComponent;
    /* 指派部門 */
    private DepTreeComponent queryTransDepTreeComponent;
    /* 可使用人員 */
    private DepTreePickerComponent queryUseUserTreePickerComponent;
    /* 可閱人員 */
    private DepTreePickerComponent queryViewUserTreePickerComponent;
    /* 可使用人員-挑選部門 */
    private List<Org> queryUseUserOrgs;
    /* 可閱人員-挑選部門 */
    private List<Org> queryVieUserOrgs;

    private Map<String, List<WCSetPermission>> permissionMap;

    @Getter
    private String class_scrollable_body;

    @Getter
    private String ttId;

    @PostConstruct
    public void init() {
        this.class_scrollable_body = ".ui-datatable-scrollable-body"; // .ui-treetable-scrollable-body
        this.ttId = "id_queryResult";
        this.userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        this.depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        this.compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        this.showTree = false;
        this.checkAll = true;
        this.showDisable = false;
        this.query = new CheckAuthoritySearchQuery();
        this.query.init();
        // 準備權限判斷物件
        List<TargetType> targetTypes =
            Lists.newArrayList(
                TargetType.CATEGORY_TAG, TargetType.MENU_TAG, TargetType.TAG, TargetType.EXEC_DEP);
        List<WCSetPermission> permissionAll =
            WCSetPermissionManager.getInstance()
                .findByCompSidAndTargetTypeIn(this.compViewVo.getSid(), targetTypes);
        this.permissionMap =
            permissionAll.stream()
                .collect(
                    Collectors.groupingBy(
                        each -> each.getTargetType().name().concat(each.getTargetSid())));
        this.initItmes();
        this.initOrgData();
        this.allTagTreeManager = new MultipleALLTagTreeManager();
    }

    private void initItmes() {
        this.categoryTagList =
            checkAuthorityComponent.initCategoryTagList(depViewVo.getSid(), compViewVo.getSid());
        this.categoryTagList =
            this.categoryTagList.stream()
                .filter(
                    each -> {
                        // 判斷設定權限
                        List<WCSetPermission> categoryPermissionList =
                            this.permissionMap.get(
                                TargetType.CATEGORY_TAG.name().concat((String) each.getValue()));
                        WCSetPermission categoryPermission =
                            WkStringUtils.notEmpty(categoryPermissionList)
                                ? categoryPermissionList.get(0)
                                : null;
                        if (categoryPermission != null
                            && !categoryPermission.getPermissionGroups().isEmpty()) {
                            List<Integer> permissionUserSids =
                                WCSetPermissionManager.getInstance()
                                    .groupPermissionUserSids(
                                        this.compViewVo.getSid(),
                                        categoryPermission.getPermissionGroups());
                            return permissionUserSids.contains(SecurityFacade.getUserSid());
                        }
                        return true;
                    })
                .collect(Collectors.toList());
        this.flowTypeList = checkAuthorityComponent.initFlowTypeList();
        this.loadTags();
    }

    private void initOrgData() {
        allOrgs = Lists.newArrayList();
        allOrgs.addAll(orgManager.findAllDepByCompSid(compViewVo.getSid()));
        viewDepTreeComponent = new DepTreeComponent();
        canUseDepTreeComponent = new DepTreeComponent();
        execDepTreeComponent = new DepTreeComponent();
        relateDepTreeComponent = new DepTreeComponent();
        execDepTreePickerComponent = new DepTreePickerComponent();
        assignDepTreePickerComponent = new DepTreePickerComponent();
        queryUseUserTreePickerComponent = new DepTreePickerComponent();
        queryViewUserTreePickerComponent = new DepTreePickerComponent();
        queryViewDepTreeComponent = new DepTreeComponent();
        queryTransDepTreeComponent = new DepTreeComponent();
        intOrgTree();
    }

    private void intOrgTree() {
        selectViewOrgs = Lists.newArrayList();
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());

        viewDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectViewOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        canUseDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectViewOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        execDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectViewOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        relateDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectViewOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        execDepTreePickerComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectViewOrgs,
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        assignDepTreePickerComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectViewOrgs,
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        queryViewDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        queryTransDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        queryUseUserTreePickerComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            Lists.newArrayList(),
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);

        queryViewUserTreePickerComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            Lists.newArrayList(),
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);
    }

    public void initSearchDt(Integer loginCompSid, Boolean status,
        CheckAuthoritySearchQuery query) {
        allTagDt =
            WCTagDataTableComponent.getInstance()
                .buildTagSearchDt(loginCompSid, status ? null : Activation.ACTIVE, query);
    }

    public void doClearData() {
        this.query.clear();
        this.execOrgsPicker = Lists.newArrayList();
        this.execUsersPicker = Lists.newArrayList();
        this.assignOrgsPicker = Lists.newArrayList();
        this.assignUsersPicker = Lists.newArrayList();
        this.queryVieUserOrgs = Lists.newArrayList();
        this.menuTagList = Lists.newArrayList();
        this.subTagList = Lists.newArrayList();
        this.queryUseUserOrgs = Lists.newArrayList();

        this.editRow = null;
        this.editIndex = null;
        this.editNode = null;
        this.editRowKey = null;

        this.displayController.update("searchArea");
        if (this.showTree) {
            allTagTreeManager = new MultipleALLTagTreeManager();
            this.displayController.update("id_queryResult_tree");
        } else {
            allTagDt = Lists.newArrayList();
            this.displayController.update("id_queryResult");
        }
    }

    private TreeNode findNode(TreeNode root, String rowKey) {
        TreeNode tempNode = null;
        for (TreeNode treeNode : root.getChildren()) {
            if (treeNode.getRowKey().equals(rowKey)) {
                return treeNode;
            }
            tempNode = this.findNode(treeNode, rowKey);
            if (tempNode != null) {
                return tempNode;
            }
        }
        return null;
    }

    public void loadALLTags() {
        allTagTreeManager = new MultipleALLTagTreeManager();
        if (this.showTree) {
            allTagTreeManager.initSearchTree(this.compViewVo.getSid(), showDisable, this.query);
            toggleExpand();
            if (this.editNode != null) {
                this.editNode = this.findNode(allTagTreeManager.getRootNode(),
                    this.editNode.getRowKey());
                this.editNode.setSelected(true);
            }
            allTagTreeManager.expandUP(editNode, true);
            this.displayController.update("id_queryResult_tree");
        } else {
            this.initSearchDt(this.compViewVo.getSid(), showDisable, this.query);
            this.displayController.update("id_queryResult");
        }
    }

    public void refreshEditTag() {
        if (this.showTree) {
            if (this.editRowKey != null) {
                this.editNode = this.findNode(allTagTreeManager.getRootNode(), this.editRowKey);

                /** 更新編輯資料 */
                TagVO itemData = (TagVO) this.editNode.getData();
                CheckboxTreeNode itemNode =
                    (CheckboxTreeNode) this.findNode(allTagTreeManager.getRootNode(),
                        this.editRowKey);
                switch (this.editNode.getType()) {
                    case "CategoryTag":
                        WCCategoryTag categoryTag =
                            WCCategoryTagLogicComponent.getInstance()
                                .getWCCategoryTagBySid(itemData.getSid());
                        itemNode.setData(
                            new TagVO(
                                categoryTag.getSid(),
                                categoryTag.getCategoryName(),
                                "",
                                "",
                                categoryTag.getSeq(),
                                categoryTag.getStatus()));
                        break;
                    case "MenuTag":
                        WCMenuTag menuTag =
                            WCMenuTagLogicComponent.getInstance()
                                .getWCMenuTagBySid(itemData.getSid());
                        MenuTagSearchVO menuTagSearchVO =
                            WCMenuTagLogicComponent.getInstance()
                                .getMenuTagSearchVOByMenuTagSid(itemData.getSid());
                        itemNode.setData(
                            new TagVO(
                                menuTag.getSid(),
                                "",
                                menuTag.getMenuName(),
                                "",
                                menuTag.getSeq(),
                                menuTag.getStatus(),
                                menuTagSearchVO,
                                null,
                                null,
                                itemData.getCategorySid(),
                                menuTag.getSid()));
                        break;
                    case "Tag":
                        WCTag tag = WCTagLogicComponents.getInstance()
                            .getWCTagBySid(itemData.getSid());
                        TagSearchVO subTagSearchVO =
                            WCTagLogicComponents.getInstance()
                                .getTagSearchByTagSid(itemData.getSid());

                        ExecDepSettingSearchVO execDepSettingSearchVO = null;
                        if (WkStringUtils.notEmpty(itemData.getExecDepSettingSearchVO())
                            && WkStringUtils.notEmpty(
                            itemData.getExecDepSettingSearchVO().getWc_exec_dep_setting_sid())) {
                            execDepSettingSearchVO =
                                WCExecDepSettingLogicComponent.getInstance()
                                    .getExecDepSettingSearchVOBySid(
                                        itemData.getExecDepSettingSearchVO()
                                            .getWc_exec_dep_setting_sid());
                        }

                        if (execDepSettingSearchVO == null) {
                            itemNode.setData(
                                new TagVO(
                                    tag.getSid(),
                                    "",
                                    "",
                                    tag.getTagName(),
                                    tag.getSeq(),
                                    tag.getStatus(),
                                    null,
                                    subTagSearchVO,
                                    null,
                                    itemData.getCategorySid(),
                                    itemData.getMenuSid()));
                        } else {
                            itemNode.setData(
                                new TagVO(
                                    tag.getSid(),
                                    "",
                                    "",
                                    tag.getTagName(),
                                    tag.getSeq(),
                                    tag.getStatus(),
                                    null,
                                    subTagSearchVO,
                                    execDepSettingSearchVO,
                                    itemData.getCategorySid(),
                                    itemData.getMenuSid()));
                        }
                        break;
                    default:
                        break;
                }
            } else {
                /** 更新搜尋資料 */
                allTagTreeManager.initSearchTree(this.compViewVo.getSid(), showDisable, this.query);
            }
            toggleExpand();
            if (this.editRowKey != null) {
                // this.editNode.setSelected(true);
                if (!this.checkAll) {
                    // 若未勾選【展開】則展開編輯項目
                    this.allTagTreeManager.expandUP(editNode, true);
                }
            }

            // 僅更新編輯列 (增加效能)
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":categoryName");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":menuName");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":tagName");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":status");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":uesDeps");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":useUserNames");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":reqFlowType");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":viewDep");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":readUserNames");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":execDeps");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":execDepSignUser");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":execMode");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":assignUserName");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":assignDep");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":receviceManager");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":receviceManagerTooltip");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":receviceUser");
            Ajax.update("id_queryResult_tree:" + this.editRowKey + ":receviceUserTooltip");
            // 選取編輯項目(highlight)
            this.displayController.execute(
                "$(\"#id_queryResult_tree_data>tr\").removeClass('ui-state-highlight')");
            this.displayController.execute(
                "$(\"#id_queryResult_tree_node_"
                    + this.editRowKey
                    + "\").addClass('ui-state-highlight')");
        } else {
            if (this.editIndex == null) {
                /** 更新搜尋資料 */
                this.initSearchDt(this.compViewVo.getSid(), showDisable, this.query);
                this.displayController.update("id_queryResult");
            } else {
                this.editRow = this.allTagDt.get(this.editIndex);
                /** 更新編輯資料 */
                switch (this.editRow.getTagType()) {
                    case "CategoryTag":
                        WCCategoryTag categoryTag =
                            WCCategoryTagLogicComponent.getInstance()
                                .getWCCategoryTagBySid(this.editRow.getSid());
                        this.allTagDt.set(
                            this.editIndex,
                            new TagDtVO(
                                "CategoryTag",
                                categoryTag.getSid(),
                                categoryTag.getCategoryName(),
                                "",
                                "",
                                categoryTag.getSeq(),
                                categoryTag.getStatus()));
                        break;
                    case "MenuTag":
                        WCMenuTag menuTag =
                            WCMenuTagLogicComponent.getInstance()
                                .getWCMenuTagBySid(this.editRow.getSid());
                        MenuTagSearchVO menuTagSearchVO =
                            WCMenuTagLogicComponent.getInstance()
                                .getMenuTagSearchVOByMenuTagSid(this.editRow.getSid());
                        this.allTagDt.set(
                            this.editIndex,
                            new TagDtVO(
                                "MenuTag",
                                menuTag.getSid(),
                                "",
                                menuTag.getMenuName(),
                                "",
                                menuTag.getSeq(),
                                menuTag.getStatus(),
                                menuTagSearchVO,
                                null,
                                null,
                                this.editRow.getCategorySid(),
                                menuTag.getSid()));
                        break;
                    case "Tag":
                        WCTag tag = WCTagLogicComponents.getInstance()
                            .getWCTagBySid(this.editRow.getSid());
                        TagSearchVO subTagSearchVO =
                            WCTagLogicComponents.getInstance()
                                .getTagSearchByTagSid(this.editRow.getSid());

                        ExecDepSettingSearchVO execDepSettingSearchVO = null;
                        if (WkStringUtils.notEmpty(this.editRow.getExecDepSettingSearchVO())
                            && WkStringUtils.notEmpty(
                            this.editRow.getExecDepSettingSearchVO()
                                .getWc_exec_dep_setting_sid())) {
                            execDepSettingSearchVO =
                                WCExecDepSettingLogicComponent.getInstance()
                                    .getExecDepSettingSearchVOBySid(
                                        this.editRow.getExecDepSettingSearchVO()
                                            .getWc_exec_dep_setting_sid());
                        }

                        if (execDepSettingSearchVO == null) {
                            this.allTagDt.set(
                                this.editIndex,
                                new TagDtVO(
                                    "Tag",
                                    tag.getSid(),
                                    "",
                                    "",
                                    tag.getTagName(),
                                    tag.getSeq(),
                                    tag.getStatus(),
                                    null,
                                    subTagSearchVO,
                                    null,
                                    this.editRow.getCategorySid(),
                                    this.editRow.getMenuSid()));
                        } else {
                            this.allTagDt.set(
                                this.editIndex,
                                new TagDtVO(
                                    "Tag",
                                    tag.getSid(),
                                    "",
                                    "",
                                    tag.getTagName(),
                                    tag.getSeq(),
                                    tag.getStatus(),
                                    null,
                                    subTagSearchVO,
                                    execDepSettingSearchVO,
                                    this.editRow.getCategorySid(),
                                    this.editRow.getMenuSid()));
                        }
                        break;
                    default:
                        break;
                }
                // 僅更新編輯列 (增加效能)
                UIData table = (UIData) Faces.getViewRoot()
                    .findComponent("formTemplate:" + this.ttId);
                Ajax.updateRow(table, editIndex);
                // 選取編輯項目(highlight)
                this.displayController.execute("PF('wv_queryResult').unselectAllRows()");
                this.displayController.execute(
                    "PF('wv_queryResult').selectRow(" + editIndex + ",true)");
            }
        }
    }

    public void toggleTree() {
        if (this.showTree) {
            this.class_scrollable_body = ".ui-treetable-scrollable-body";
            this.ttId = "id_queryResult_tree";
        } else {
            this.class_scrollable_body = ".ui-datatable-scrollable-body";
            this.ttId = "id_queryResult";
        }
        this.displayController.execute(
            "updateTtId('"
                + this.ttId
                + "');updateScroolableBody(' "
                + this.class_scrollable_body
                + "');");
    }

    public void toggleExpand() {
        if (this.checkAll) {
            allTagTreeManager.expandAllTree();
        } else {
            allTagTreeManager.collapseAllTree();
        }
    }

    public void loadExecDepBySubTagSid(TagDtVO rowTagVO) {
        this.editRow = rowTagVO;
        this.editIndex = allTagDt.indexOf(this.editRow);
        this.loadExecDepBySubTagSid(
            rowTagVO.getCategorySid(), rowTagVO.getMenuSid(), rowTagVO.getSid());
    }

    public void loadExecDepBySubTagSid(TagVO nodeTagVO, CheckboxTreeNode node) {
        this.editNode = node;
        this.editRowKey = node.getRowKey();
        this.loadExecDepBySubTagSid(
            nodeTagVO.getCategorySid(), nodeTagVO.getMenuSid(), nodeTagVO.getSid());
    }

    private void loadExecDepBySubTagSid(String categorySid, String menuSid, String tagSid) {
        setting3Bean.setFullversion(false);
        setting3Bean.doSearch();
        setting3Bean.loadMenuByCategorySid(categorySid);
        setting3Bean.searchMenuItems();
        setting3Bean.loadSubTagByMenuSid(menuSid);
        setting3Bean.searchTagItems();
        setting3Bean.loadExecDepBySubTagSid(tagSid);
        this.displayController.hidePfWidgetVar("menu_list_dlg_wv");
        this.displayController.hidePfWidgetVar("tag_list_dlg_wv");
    }

    public void settingStatus(TagDtVO rowTagVO) {
        this.editRow = rowTagVO;
        this.editIndex = allTagDt.indexOf(this.editRow);
        switch (rowTagVO.getTagType()) {
            case "CategoryTag":
                setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(rowTagVO.getSid()));
                setting3Bean.loadCategoryTag();
                break;
            case "MenuTag":
                setting3Bean.setFullversion(false);
                setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(rowTagVO.getCategorySid()));
                setting3Bean.setSelMenuTagSearchVO(new MenuTagSearchVO(rowTagVO.getMenuSid()));
                setting3Bean.loadMenuTag();
                break;
            case "Tag":
                setting3Bean.setFullversion(false);
                setting3Bean.doSearch();
                setting3Bean.loadMenuByCategorySid(rowTagVO.getCategorySid());
                setting3Bean.searchMenuItems();
                setting3Bean.loadSubTagByMenuSid(rowTagVO.getMenuSid());
                setting3Bean.setSelSubTagSearchVO(new TagSearchVO(rowTagVO.getSid()));
                setting3Bean.loadSubTag();
                this.displayController.hidePfWidgetVar("menu_list_dlg_wv");
                this.displayController.hidePfWidgetVar("tag_list_dlg_wv");
                break;
            default:
                break;
        }
    }

    public void settingStatus(TagVO nodeTagVO, CheckboxTreeNode node) {
        this.editNode = node;
        this.editRowKey = node.getRowKey();
        switch (node.getType()) {
            case "CategoryTag":
                setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(nodeTagVO.getSid()));
                setting3Bean.loadCategoryTag();
                break;
            case "MenuTag":
                setting3Bean.setFullversion(false);
                setting3Bean.setSelTagSearchVO(new CategoryTagSearchVO(nodeTagVO.getCategorySid()));
                setting3Bean.setSelMenuTagSearchVO(new MenuTagSearchVO(nodeTagVO.getMenuSid()));
                setting3Bean.loadMenuTag();
                break;
            case "Tag":
                setting3Bean.setFullversion(false);
                setting3Bean.doSearch();
                setting3Bean.loadMenuByCategorySid(nodeTagVO.getCategorySid());
                setting3Bean.searchMenuItems();
                setting3Bean.loadSubTagByMenuSid(nodeTagVO.getMenuSid());
                setting3Bean.setSelSubTagSearchVO(new TagSearchVO(nodeTagVO.getSid()));
                setting3Bean.loadSubTag();
                this.displayController.hidePfWidgetVar("menu_list_dlg_wv");
                this.displayController.hidePfWidgetVar("tag_list_dlg_wv");
                break;
            default:
                break;
        }
    }

    /**
     * 載入可使用人員
     *
     * @param wCMenuTagSid 選單類別Sid
     */
    public void loadUseUsers(String wCMenuTagSid) {
        try {
            Preconditions.checkState(!Strings.isNullOrEmpty(wCMenuTagSid), "無取得選單類別Sid，請確認！！");
            this.useUsers.clear();
            this.useUsers.addAll(wCMenuTagLogicComponent.getUseUsersByMenuTagSid(wCMenuTagSid));
            this.displayController.showPfWidgetVar("useUserDilaogVar");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("loadUseUsers ERROR：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("loadUseUsers ERROR", e);
        }
    }

    /**
     * 載入預設可閱人員
     *
     * @param wcTagSid 執行項目Sid
     */
    public void loadViewUsers(String wcTagSid) {
        try {
            Preconditions.checkState(!Strings.isNullOrEmpty(wcTagSid), "無取得執行項目Sid，請確認！！");
            this.viewUsers.clear();
            this.viewUsers.addAll(wCTagLogicComponents.getViewUsersByTagSid(wcTagSid));
            this.displayController.showPfWidgetVar("viewUserDilaogVar");
        } catch (Exception e) {
            log.warn("loadViewUsers ERROR", e);
        }
    }

    public void loadViewOrgs(String orgs) {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

            if (!StringUtils.isEmpty(orgs)) {
                this.selectViewOrgs = getSelectViewOrgs(orgs);
            }
            // List<Org> reOrg = getSelectViewOrgs(orgs);
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            viewDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadViewOrgs ERROR", e);
        }
    }

    /* 載入 可閱部門 組織樹 */
    public void loadQueryViewDep() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
            List<Org> selDeps = Lists.newArrayList();
            this.query
                .getViewOrgs()
                .forEach(
                    item -> {
                        selDeps.add(item);
                    });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            queryViewDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                selDeps,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadQueryViewDep ERROR", e);
        }
    }

    /* 載入 指派部門 組織樹 */
    public void loadQueryTransDep() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
            List<Org> selDeps = Lists.newArrayList();
            this.query
                .getTransOrgs()
                .forEach(
                    item -> {
                        selDeps.add(item);
                    });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            queryTransDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                selDeps,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadQueryTransDep ERROR", e);
        }
    }

    public List<Org> getSelectViewOrgs(String selectOrgs) {
        List<Org> allOrgs = orgManager.findAll();
        allOrgs = allOrgs.stream().filter(each -> each.getName() != null)
            .collect(Collectors.toList());
        List<Org> resultOrgs = Lists.newArrayList();
        resultOrgs =
            allOrgs.stream()
                .filter(each -> selectOrgs.contains(each.getName()))
                .collect(Collectors.toList());
        return resultOrgs;
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getViewDepTreeManager() {
        return this.viewDepTreeComponent.getMultipleDepTreeManager();
    }

    /* 取得 可閱部門 組織樹 */
    public MultipleDepTreeManager getQueryViewDepTreeManager() {
        return this.queryViewDepTreeComponent.getMultipleDepTreeManager();
    }

    /* 取得 指派單位 組織樹 */
    public MultipleDepTreeManager getQueryTransDepTreeManager() {
        return this.queryTransDepTreeComponent.getMultipleDepTreeManager();
    }

    public void loadCanUseOrgs() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            canUseDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                query.getCanUseOrg(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadCanUseOrgs ERROR", e);
        }
    }

    public void loadTags() {
        multipleTagTreeManager = new MultipleTagTreeManager();
        multipleTagTreeManager.initTreeAllComp(
            compViewVo.getSid(), this.query.getSelNode(), true, true);
    }

    /**
     * 取得 多選 類別、選單、執行項目名稱 Tag樹
     *
     * @return MultipleTagTreeManager Tag樹Manager
     */
    public MultipleTagTreeManager getTagTreeManager() {
        return this.multipleTagTreeManager;
    }

    /**
     * 取得 可使用單位 組織樹
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getCanUseDepTreeManager() {
        return this.canUseDepTreeComponent.getMultipleDepTreeManager();
    }

    public void loadExecOrgs() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            execDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                query.getExecOrg(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadExecOrgs ERROR", e);
        }
    }

    /**
     * 取得 執行單位 組織樹
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getExecDepTreeManager() {
        return this.execDepTreeComponent.getMultipleDepTreeManager();
    }

    public void loadRelatedOrgs() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            relateDepTreeComponent.init(
                tempOrg,
                tempOrg,
                group,
                query.getRelatedOrg(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadRelatedOrgs ERROR", e);
        }
    }

    /**
     * 取得 相關單位 組織樹
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getRelateDepTreeManager() {
        return this.relateDepTreeComponent.getMultipleDepTreeManager();
    }

    /* 載入 可使用人員 元件 */
    public void loadUseUserOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
            if (this.queryUseUserOrgs == null) {
                this.queryUseUserOrgs = Lists.newArrayList();
            }
            List<User> execUsers = Lists.newArrayList();
            this.query
                .getUseUsers()
                .forEach(
                    each -> {
                        execUsers.add(each);
                    });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            this.queryUseUserTreePickerComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.queryUseUserOrgs,
                execUsers,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadUseUserOrgsAndPickerUsers ERROR", e);
        }
    }

    /* 載入 可閱人員 元件 */
    public void loadQueryViewUserOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });
            if (this.queryVieUserOrgs == null) {
                this.queryVieUserOrgs = Lists.newArrayList();
            }
            List<User> users = Lists.newArrayList();
            this.query
                .getViewUsers()
                .forEach(
                    each -> {
                        users.add(each);
                    });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            this.queryViewUserTreePickerComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.queryVieUserOrgs,
                users,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadQueryViewUserOrgsAndPickerUsers ERROR", e);
        }
    }

    public void loadExecOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

            if (this.execOrgsPicker == null) {
                this.execOrgsPicker = Lists.newArrayList();
            }

            if (this.execUsersPicker == null) {
                this.execUsersPicker = Lists.newArrayList();
            }

            List<User> execUsers = Lists.newArrayList();
            execUsersPicker.forEach(
                each -> {
                    execUsers.add(each);
                });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;

            Org group = new Org(compViewVo.getSid());
            execDepTreePickerComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.execOrgsPicker,
                execUsers,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadExecOrgsAndPickerUsers ERROR", e);
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getExecDepTreePickerManager() {
        return this.execDepTreePickerComponent.getMultipleDepTreePickerManager();
    }

    /**
     * 取得挑選可使用人員元件
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getUseUserDepTreePickerManager() {
        return this.queryUseUserTreePickerComponent.getMultipleDepTreePickerManager();
    }

    /**
     * 取得挑選可閱人員元件
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getQueryViewUserDepTreePickerManager() {
        return this.queryViewUserTreePickerComponent.getMultipleDepTreePickerManager();
    }

    public void loadAssignOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                item -> {
                    tempOrg.add(item);
                });

            if (this.assignOrgsPicker == null) {
                this.assignOrgsPicker = Lists.newArrayList();
            }

            if (this.assignUsersPicker == null) {
                this.assignUsersPicker = Lists.newArrayList();
            }

            List<User> assignUsers = Lists.newArrayList();
            assignUsersPicker.forEach(
                each -> {
                    assignUsers.add(each);
                });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;

            Org group = new Org(compViewVo.getSid());
            assignDepTreePickerComponent.init(
                tempOrg,
                tempOrg,
                group,
                this.assignOrgsPicker,
                assignUsers,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadAssignOrgsAndPickerUsers ERROR", e);
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getAssignDepTreePickerManager() {
        return this.assignDepTreePickerComponent.getMultipleDepTreePickerManager();
    }

    public void onSelectCategoryTagName(String categoryTagId) {
        this.menuTagList = checkAuthorityComponent.initMenuTagList(categoryTagId);
        this.menuTagList =
            this.menuTagList.stream()
                .filter(
                    each -> {
                        // 判斷設定權限
                        List<WCSetPermission> menuPermissionList =
                            permissionMap.get(
                                TargetType.MENU_TAG.name().concat((String) each.getValue()));
                        WCSetPermission menuPermission =
                            WkStringUtils.notEmpty(menuPermissionList) ? menuPermissionList.get(0)
                                : null;
                        if (menuPermission != null && !menuPermission.getPermissionGroups()
                            .isEmpty()) {
                            List<Integer> permissionUserSids =
                                WCSetPermissionManager.getInstance()
                                    .groupPermissionUserSids(
                                        this.compViewVo.getSid(),
                                        menuPermission.getPermissionGroups());
                            return permissionUserSids.contains(SecurityFacade.getUserSid());
                        }
                        return true;
                    })
                .collect(Collectors.toList());
        log.info(categoryTagId);
        this.displayController.update("menu_tag_id");
    }

    public void onSelectMenuTagName(String menuTagId) {
        this.subTagList =
            this.checkAuthorityComponent.initSubTagList(menuTagId, this.compViewVo.getSid(), this.userViewVO.getSid());
        this.subTagList =
            this.subTagList.stream()
                .filter(
                    each -> {
                        // 判斷設定權限
                        List<WCSetPermission> tagPermissionList =
                            permissionMap.get(
                                TargetType.TAG.name().concat((String) each.getValue()));
                        WCSetPermission tagPermission =
                            WkStringUtils.notEmpty(tagPermissionList) ? tagPermissionList.get(0)
                                : null;
                        if (tagPermission != null && !tagPermission.getPermissionGroups()
                            .isEmpty()) {
                            List<Integer> permissionUserSids =
                                WCSetPermissionManager.getInstance()
                                    .groupPermissionUserSids(
                                        this.compViewVo.getSid(),
                                        tagPermission.getPermissionGroups());
                            return permissionUserSids.contains(SecurityFacade.getUserSid());
                        }
                        return true;
                    })
                .collect(Collectors.toList());
        this.displayController.update("sub_tag_id");
    }

    public void saveTag() {
        try {
            query.setSelNode(
                multipleTagTreeManager.getSelNode() == null
                    ? Lists.newArrayList()
                    : Arrays.asList(multipleTagTreeManager.getSelNode()));
            this.displayController.hidePfWidgetVar("tagUnitDlg");
        } catch (Exception e) {
            log.warn("saveTag ERROR", e);
        }
    }

    public void saveCanUse() {
        try {
            query.setCanUseOrg(canUseDepTreeComponent.getSelectOrg());
            this.displayController.hidePfWidgetVar("canUseDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveCanUse ERROR", e);
        }
    }

    /**
     * 挑選 可閱單位
     */
    public void saveQueryViewOrg() {
        try {
            List<Org> queryViewSelOrgs = this.queryViewDepTreeComponent.getSelectOrg();
            query.getViewOrgs().clear();
            queryViewSelOrgs.forEach(
                item -> {
                    query.getViewOrgs().add(item);
                });
            this.displayController.hidePfWidgetVar("queryViewDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveQueryViewOrg ERROR", e);
        }
    }

    /**
     * 挑選 指派單位
     */
    public void saveQueryTransOrg() {
        try {
            List<Org> queryTransSelOrgs = this.queryTransDepTreeComponent.getSelectOrg();
            query.getTransOrgs().clear();
            queryTransSelOrgs.forEach(
                item -> {
                    query.getTransOrgs().add(item);
                });
            this.displayController.hidePfWidgetVar("queryTransDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveQueryViewOrg ERROR", e);
        }
    }

    /**
     * 挑選 執行單位
     */
    public void saveExecOrg() {
        try {
            query.setExecOrg(execDepTreeComponent.getSelectOrg());
            this.displayController.hidePfWidgetVar("execDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveExecOrg ERROR", e);
        }
    }

    public void saveExecOrgPicker() {
        try {
            this.execOrgsPicker = execDepTreePickerComponent.getSelectOrg();
            this.execUsersPicker =
                execDepTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected();
            query.setExecSignUser(execUsersPicker);
            this.displayController.hidePfWidgetVar("execDepPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveExecOrgPicker ERROR", e);
        }
    }

    /**
     * 挑選 相關單位
     */
    public void saveRelatedOrg() {
        try {
            query.setRelatedOrg(relateDepTreeComponent.getSelectOrg());
            this.displayController.hidePfWidgetVar("relateDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveRelatedOrg ERROR", e);
        }
    }

    /* 選擇 可使用人員 */
    public void saveUseUserPicker() {
        try {
            this.queryUseUserOrgs = this.queryUseUserTreePickerComponent.getSelectOrg();
            this.query.getUseUsers().clear();
            if (queryUseUserTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected()
                != null) {
                queryUseUserTreePickerComponent
                    .getMultipleDepTreePickerManager()
                    .getAllSelected()
                    .forEach(
                        item -> {
                            this.query.getUseUsers().add(item);
                        });
            }
            this.displayController.hidePfWidgetVar("useUserPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveUseUserPicker ERROR", e);
        }
    }

    /* 選擇 可閱人員 */
    public void saveQueryViewUserPicker() {
        try {
            this.queryVieUserOrgs = this.queryViewUserTreePickerComponent.getSelectOrg();
            this.query.getViewUsers().clear();
            if (queryViewUserTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected()
                != null) {
                queryViewUserTreePickerComponent
                    .getMultipleDepTreePickerManager()
                    .getAllSelected()
                    .forEach(
                        item -> {
                            this.query.getViewUsers().add(item);
                        });
            }
            this.displayController.hidePfWidgetVar("queryViewUserPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveQueryViewUserPicker ERROR", e);
        }
    }

    public void saveAssignOrgPicker() {
        try {
            this.assignOrgsPicker = assignDepTreePickerComponent.getSelectOrg();
            this.assignUsersPicker =
                assignDepTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected();
            query.setAssignUser(assignUsersPicker);
            this.displayController.hidePfWidgetVar("assignDepPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveAssignOrgPicker ERROR", e);
        }
    }
}
