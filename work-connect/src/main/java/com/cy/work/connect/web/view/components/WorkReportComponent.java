/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.vo.WCMaster;
import java.io.Serializable;
import lombok.Data;

/**
 * @author brain0925_liao
 */
@Data
public class WorkReportComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -685544971917070322L;

    private WorkReportDataComponent workReportDataComponent;
    private WorkReportHeaderComponent workReportHeaderComponent;
    private CategoryItemComponent categoryItemComponent;
    private ExecDescComponent execDescComponent;
    private WCMaster wcMaster;
}
