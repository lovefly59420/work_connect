/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import java.util.Arrays;
import org.primefaces.model.TreeNode;

/**
 * 單選部門元件 Brain_liao
 */
public class SingleDepTreeManager extends BaseDepTreeManager {

    /**
     *
     */
    private static final long serialVersionUID = 6209376110848491817L;

    public SingleDepTreeManager() {
    }

    public TreeNode getSelNode() {
        if (selNode.size() > 0) {
            return selNode.get(0);
        } else {
            return null;
        }
    }

    public void setSelNode(TreeNode selNode) {
        if (selNode == null) {
            return;
        }
        this.selNode = Arrays.asList(selNode);
    }
}
