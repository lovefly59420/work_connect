/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.WCExecDepSettingHelper;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.view.vo.ExecDepSettingSearchVO;
import com.cy.work.connect.web.view.vo.ManagerExecDepSettingVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 執行單位設定LogicComponent
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCExecDepSettingLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8507581103467375476L;

    private static WCExecDepSettingLogicComponent instance;

    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCExecDepSettingHelper execDepSettingHelper;
    @Autowired
    private WCTagManager wcTagManager;

    public static WCExecDepSettingLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCExecDepSettingLogicComponent.instance = this;
    }

    /**
     * 刪除執行單位設定
     *
     * @param wc_exec_dep_setting_sid 執行單位設定Sid
     * @param loginUserSid            執行者Sid
     */
    public void doDeleteExecDepSetting(String wc_exec_dep_setting_sid, Integer loginUserSid) {
        execDepSettingManager.deleteExecDepSetting(wc_exec_dep_setting_sid, loginUserSid);
    }

    /**
     * 儲存執行單位設定
     *
     * @param wc_exec_dep_setting_sid    執行單位設定Sid
     * @param loginUserSid               執行者Sid
     * @param execDep                    挑選的執行單位
     * @param signUserSids               欲簽核人員Sid List
     * @param isNeedAssign               是否需指派
     * @param isReadAssignSetting        是否讀取指派人員設定
     * @param selectAssignOrgs           指派單位
     * @param assignUserSids             指派人員Sids
     * @param managerReceviceUserSids    管理領單人員
     * @param receviceUserSids           領單人員
     * @param isOnlyReceviceUser         是否僅領單人員可閱
     * @param wc_tag_Sid                 執行項目Sid
     * @param cyWindow                   中佑對應窗口
     * @param isTransDepContainFollowing 指派單位含以下單位
     * @param execDesc                   執行說明
     */
    public void saveExecDepSetting(
        String wc_exec_dep_setting_sid,
        Integer loginUserSid,
        Org execDep,
        List<Integer> signUserSids,
        boolean isNeedAssign,
        boolean isReadAssignSetting,
        List<String> selectAssignOrgs,
        List<Integer> assignUserSids,
        List<Integer> managerReceviceUserSids,
        List<Integer> receviceUserSids,
        boolean isOnlyReceviceUser,
        String wc_tag_Sid,
        String cyWindow,
        boolean isTransDepContainFollowing,
        String execDesc) {

        Preconditions.checkState(execDep != null, "請挑選執行單位");
        Preconditions.checkState(signUserSids.size() < 2, "簽核人員僅可一人");
        if (isNeedAssign) {
            if (isReadAssignSetting) {
                Preconditions.checkState(
                    (assignUserSids != null && !assignUserSids.isEmpty())
                        || (selectAssignOrgs != null && !selectAssignOrgs.isEmpty()),
                    "請挑選指派人員");
            }
            Preconditions.checkState(
                receviceUserSids == null || receviceUserSids.isEmpty(), "派工不可挑選領單人員");
        } else {
            if (isOnlyReceviceUser) {
                Preconditions.checkState(
                    (receviceUserSids != null && !receviceUserSids.isEmpty()), "請挑選領單人員");
            }
        }
        if (!isNeedAssign || !isReadAssignSetting) {
            assignUserSids = Lists.newArrayList();
        }
        List<Org> orgs = Lists.newArrayList(execDep);
        orgs.addAll(
            orgManager.findAllChild(execDep.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList()));
        LinkedHashSet<User> canReceviceUsers = Sets.newLinkedHashSet();
        orgs.forEach(
            each -> {
                canReceviceUsers.addAll(
                    userManager
                        .findUserWithManagerByOrgSids(Lists.newArrayList(each.getSid()), Activation.ACTIVE)
                        .stream()
                        .distinct()
                        .collect(Collectors.toList()));
            });
        List<User> receverData = Lists.newArrayList();
        if (receviceUserSids != null && !receviceUserSids.isEmpty()) {
            receviceUserSids.forEach(
                each -> {
                    receverData.add(userManager.findBySid(each));
                });
        }
        List<User> ngUsers = Lists.newArrayList();
        receverData.forEach(
            each -> {
                if (!canReceviceUsers.contains(each)) {
                    ngUsers.add(each);
                }
            });
        if (!ngUsers.isEmpty()) {
            String ngUserString = ngUsers.stream().map(User::getName)
                .collect(Collectors.joining(","));
            Preconditions.checkState(
                false, "請移除下述領單人員，因已不在執行部門(" + execDep.getName() + ") : " + ngUserString);
        }
        execDepSettingHelper.checkOtherExecDepSetting(
            wc_exec_dep_setting_sid, execDep, signUserSids, wc_tag_Sid);
        execDepSettingManager.saveExecDepSetting(
            wc_exec_dep_setting_sid,
            loginUserSid,
            execDep,
            signUserSids,
            wc_tag_Sid,
            isNeedAssign,
            isReadAssignSetting,
            selectAssignOrgs,
            assignUserSids,
            managerReceviceUserSids,
            receviceUserSids,
            isOnlyReceviceUser,
            cyWindow,
            isTransDepContainFollowing,
            execDesc);
    }

    public void saveExecDepSetting(
        String wc_exec_dep_setting_sid, Integer loginUserSid, List<Integer> receviceUserSids) {

        execDepSettingManager.saveExecDepSetting(
            wc_exec_dep_setting_sid, loginUserSid, receviceUserSids);
    }

    public WCExecDepSetting getWCExecDepSettingBySid(String wc_exec_dep_setting_sid) {
        return execDepSettingManager.findBySidFromCache(wc_exec_dep_setting_sid);
    }

    public List<WCExecDepSetting> getWCExecDepSettingByWcTagSid(
        String wc_tag_sid, Activation activation) {
        List<WCExecDepSetting> results = Lists.newArrayList();
        if (wc_tag_sid == null) {
            return results;
        }
        try {
            results = execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(wc_tag_sid, activation);
        } catch (Exception ex) {
            log.warn("getWCExecDepSettingByWcTagSid ERROR", ex);
        }
        return results;
    }

    public List<WCExecDepSetting> getWCExecDepSettingByWcTagSidList(List<String> wc_tag_sids) {
        return wc_tag_sids.stream()
            .flatMap(
                each ->
                    execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(each, Activation.ACTIVE)
                        .stream())
            .collect(Collectors.toList());
    }

    /**
     * 取得執行單位設定 By 類別TagSid And 執行單位depSid
     *
     * @param wc_tag_Sid 類別Sid
     * @param depSid
     * @return
     */
    public WCExecDepSetting getExecDepSettingByWcTagSidAndDepSid(String wc_tag_Sid,
        Integer depSid) {
        List<WCExecDepSetting> result =
            execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(wc_tag_Sid, Activation.ACTIVE);
        List<WCExecDepSetting> execDepSetting =
            result.stream()
                .filter(
                    each ->
                        each.getStatus().equals(Activation.ACTIVE)
                            && each.getExec_dep_sid().equals(depSid))
                .collect(Collectors.toList());
        if (execDepSetting != null && !execDepSetting.isEmpty()) {
            return execDepSetting.get(0);
        }
        return null;
    }

    /**
     * 取得執行單位設定(For 類別管理人員使用)
     *
     * @param wcMenuSid
     * @param loginUserSid
     * @return
     */
    public List<ManagerExecDepSettingVO> getManagerExecDepSettingVOs(
        String wcMenuSid, Integer loginUserSid) {
        List<ManagerExecDepSettingVO> result = Lists.newArrayList();
        wcTagManager
            .getWCTagActiveForManager(wcMenuSid, loginUserSid)
            .forEach(
                item -> {
                    execDepSettingManager
                        .getExecDepSettingForManager(item.getSid(), loginUserSid)
                        .forEach(
                            eItem -> {
                                StringBuilder userNameSb = new StringBuilder();
                                StringBuilder userNameListSb = new StringBuilder();
                                this.transUserTo(userNameSb, userNameListSb,
                                    eItem.getReceviceUser());
                                String memo = item.getMemo();
                                if (!Strings.isNullOrEmpty(memo)) {
                                    memo = "(" + memo + ")";
                                }
                                Org dep = orgManager.findBySid(eItem.getExec_dep_sid());
                                String cssStr = "";
                                if (!eItem.isNeedAssigned()
                                    && Strings.isNullOrEmpty(userNameSb.toString())) {
                                    userNameSb.append("無指定領單人員(預設部門人員皆可領單)");
                                    cssStr = "color: red;";
                                }
                                ManagerExecDepSettingVO ev =
                                    new ManagerExecDepSettingVO(
                                        eItem.getSid(),
                                        item.getTagName(),
                                        memo,
                                        dep.getName(),
                                        (eItem.isNeedAssigned()) ? "派工" : "領單",
                                        userNameSb.toString(),
                                        userNameListSb.toString(),
                                        cssStr);

                                result.add(ev);
                            });
                });
        return result;
    }

    private void transUserTo(
        StringBuilder userNameSb, StringBuilder userNameListSb, UsersTo usersTo) {
        if (usersTo != null && usersTo.getUserTos() != null) {
            usersTo
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (!Strings.isNullOrEmpty(userNameSb.toString())) {
                            userNameSb.append(",");
                        }
                        if (!Strings.isNullOrEmpty(userNameListSb.toString())) {
                            userNameListSb.append("\n");
                        }
                        User user = userManager.findBySid(subItem.getSid());
                        userNameSb.append(user.getName());
                        userNameListSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            userNameSb.append("<span style=\"color:red\">(停用)</span>");
                        }
                    });
        }
    }

    public List<ExecDepSettingSearchVO> getExecDepSettingSearchVO(String wc_tag_Sid) {
        return this.getExecDepSettingSearchVO(wc_tag_Sid, true);
    }

    /**
     * 取得執行單位設定(For 類別管理人員使用)
     *
     * @param wc_tag_Sid
     * @param fullFlg    核決權限維護使用為false; 類別維護使用為 true
     * @return
     */
    public List<ExecDepSettingSearchVO> getExecDepSettingSearchVO(
        String wc_tag_Sid, boolean fullFlg) {
        // 準備權限判斷物件
        Integer loginCompSid =
            WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()).getSid();
        List<TargetType> targetTypes = Lists.newArrayList(TargetType.EXEC_DEP);
        List<WCSetPermission> permissionAll =
            WCSetPermissionManager.getInstance()
                .findByCompSidAndTargetTypeIn(loginCompSid, targetTypes);
        Map<String, List<WCSetPermission>> permissionMap =
            permissionAll.stream()
                .collect(
                    Collectors.groupingBy(
                        each -> each.getTargetType().name().concat(each.getTargetSid())));

        List<ExecDepSettingSearchVO> execDepSettingSearchVOs = Lists.newArrayList();
        List<WCExecDepSetting> result =
            execDepSettingManager.getExecDepSettingFromCacheByWcTagSid(wc_tag_Sid, Activation.ACTIVE);
        result.forEach(
            item -> {
                // 判斷設定權限
                if (fullFlg == false) {
                    List<WCSetPermission> execDepPermissionList =
                        permissionMap.get(TargetType.EXEC_DEP.name().concat(item.getSid()));
                    WCSetPermission execDepPermission =
                        WkStringUtils.notEmpty(execDepPermissionList) ? execDepPermissionList.get(0)
                            : null;
                    if (execDepPermission != null && !execDepPermission.getPermissionGroups()
                        .isEmpty()) {
                        List<Integer> permissionUserSids =
                            WCSetPermissionManager.getInstance()
                                .groupPermissionUserSids(
                                    loginCompSid, execDepPermission.getPermissionGroups());
                        if (!permissionUserSids.contains(SecurityFacade.getUserSid())) {
                            return;
                        }
                    }
                }

                ExecDepSettingSearchVO execDepSettingSearchVO =
                    this.getExecDepSettingSearchVOByItem(item);
                execDepSettingSearchVOs.add(execDepSettingSearchVO);
            });
        return execDepSettingSearchVOs;
    }

    /**
     * 取得執行單位設定 By exec_dep_setting_sid
     *
     * @param exec_dep_setting_sid
     * @return
     */
    public ExecDepSettingSearchVO getExecDepSettingSearchVOBySid(String exec_dep_setting_sid) {

        ExecDepSettingSearchVO execDepSettingSearchVO = null;
        WCExecDepSetting item = execDepSettingManager.findBySidFromCache(exec_dep_setting_sid);

        if (WkStringUtils.isEmpty(item)) {
            return execDepSettingSearchVO;
        }

        execDepSettingSearchVO = this.getExecDepSettingSearchVOByItem(item);

        return execDepSettingSearchVO;
    }

    /**
     * 取得執行單位設定 By WCExecDepSetting
     *
     * @param item
     * @return
     */
    private ExecDepSettingSearchVO getExecDepSettingSearchVOByItem(WCExecDepSetting item) {
        ExecDepSettingSearchVO execDepSettingSearchVO = null;

        if (WkStringUtils.isEmpty(item)) {
            return execDepSettingSearchVO;
        }

        /** 執行單位簽核人員 */
        StringBuilder userNameSb = new StringBuilder();
        StringBuilder userNameListSb = new StringBuilder();
        if (item.getExecSignMember() != null && item.getExecSignMember().getUserTos() != null) {
            item.getExecSignMember()
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (!Strings.isNullOrEmpty(userNameSb.toString())) {
                            userNameSb.append(",");
                        }
                        if (!Strings.isNullOrEmpty(userNameListSb.toString())) {
                            userNameListSb.append("\n");
                        }
                        User user = userManager.findBySid(subItem.getSid());
                        userNameSb.append(user.getName());
                        userNameListSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            userNameListSb.append("<span style='color:red;'>(停用)</span>");
                        }
                    });
        }

        /** 指派人員 */
        StringBuilder assignUserNameSb = new StringBuilder();
        StringBuilder assignUserNameListSb = new StringBuilder();
        if (item.getTransUser() != null && item.getTransUser().getUserTos() != null) {
            item.getTransUser()
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (!Strings.isNullOrEmpty(assignUserNameSb.toString())) {
                            assignUserNameSb.append(",");
                        }
                        if (!Strings.isNullOrEmpty(assignUserNameListSb.toString())) {
                            assignUserNameListSb.append("\n");
                        }
                        User user = userManager.findBySid(subItem.getSid());
                        assignUserNameSb.append(user.getName());
                        assignUserNameListSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            assignUserNameListSb.append("<span style='color:red;'>(停用)</span>");
                        }
                    });
        }

        /** 指派單位 */
        StringBuilder assignDepNameSb = new StringBuilder();
        List<Integer> assignDepSids = Lists.newArrayList();
        if (item.getTransdep() != null && item.getTransdep().getUserDepTos() != null) {
            item.getTransdep()
                .getUserDepTos()
                .forEach(
                    subItem -> {
                        if (!Strings.isNullOrEmpty(assignDepNameSb.toString())) {
                            assignDepNameSb.append("\n");
                        }

                        Org org = orgManager.findBySid(Integer.valueOf(subItem.getDepSid()));
//                        assignDepNameSb.append(org.getName());
                        assignDepNameSb.append(org.getOrgNameWithParentIfDuplicate());
                        assignDepSids.add(org.getSid());
                    });
        }

        /** 管理領單人員 */
        StringBuilder receviceManagerListSb = new StringBuilder();
        if (item.getManagerRecevice() != null && item.getManagerRecevice().getUserTos() != null) {
            item.getManagerRecevice()
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (!Strings.isNullOrEmpty(receviceManagerListSb.toString())) {
                            receviceManagerListSb.append("\n");
                        }

                        User user = userManager.findBySid(subItem.getSid());
                        receviceManagerListSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            receviceManagerListSb.append("<span style='color:red;'>(停用)</span>");
                        }
                    });
        }

        /** 領單人員 */
        StringBuilder receviceUserListSb = new StringBuilder();
        if (item.getReceviceUser() != null && item.getReceviceUser().getUserTos() != null) {
            item.getReceviceUser()
                .getUserTos()
                .forEach(
                    subItem -> {
                        if (!Strings.isNullOrEmpty(receviceUserListSb.toString())) {
                            receviceUserListSb.append("\n");
                        }

                        User user = userManager.findBySid(subItem.getSid());
                        receviceUserListSb.append(user.getName());
                        if (user.getStatus().equals(Activation.INACTIVE)) {
                            receviceUserListSb.append("<span style='color:red;'>(停用)</span>");
                        }
                    });
        }

        Org dep = orgManager.findBySid(item.getExec_dep_sid());
        execDepSettingSearchVO =
            new ExecDepSettingSearchVO(
                item.getSid(),
//                    dep.getName(),
                dep.getOrgNameWithParentIfDuplicate(),
                userNameSb.toString(),
                userNameListSb.toString(),
                ((item.isNeedAssigned()) ? "是" : "否"),
                ((item.isReadAssignSetting()) ? "是" : "否"),
                assignUserNameSb.toString(),
                assignUserNameListSb.toString(),
                receviceManagerListSb.toString(),
                receviceUserListSb.toString(),
                item.getContact(),
                assignDepNameSb.toString(),
                assignDepSids);

        return execDepSettingSearchVO;
    }
}
