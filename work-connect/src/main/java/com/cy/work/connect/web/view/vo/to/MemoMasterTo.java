/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.to;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * WCMemoMaster 自訂物件
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"sid"})
public class MemoMasterTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6891513797164957443L;

    private String sid;
    /**
     * 備忘錄單號
     */
    private String memoNo;
    /**
     * 建單者部門Sid
     */
    private Integer createDep;
    /**
     * 建單者Sid
     */
    private Integer createUser;
    /**
     * 建單時間
     */
    private Date createDate;
    /**
     * 更新者Sid
     */
    private Integer updateUser;
    /**
     * 更新時間
     */
    private Date updateDate;
    /**
     * 主題
     */
    private String theme;
    /**
     * 內容
     */
    private String contentCss;
    /**
     * 備註
     */
    private String memoCss;
    /**
     * 鎖定者Sid
     */
    private Integer lockUser;
    /**
     * 鎖定時間
     */
    private Date lockDate;
    /**
     * 備忘錄類別Sid
     */
    private String wcMemoCategorySid;
}
