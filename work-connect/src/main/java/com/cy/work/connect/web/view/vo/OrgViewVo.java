/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import java.util.Objects;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;

import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao 部門介面物件
 */
public class OrgViewVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4634957340158445807L;
    /**
     * 部門SID
     */
    @Getter
    private final Integer sid;
    /**
     * 部門Name
     */
    @Getter
    private final String name;
    /**
     * 公司Name
     */
    @Getter
    private final String companyName;

    @Getter
    private String depName = "";
    @Getter
    @Setter
    private boolean canMove = true;
    @Getter
    @Setter
    private boolean modifyed = false;

    @Getter
    @Setter
    /** id */
    private String id;

    public OrgViewVo() {
        this.sid = null;
        this.name = "";
        this.companyName = "";
    }

    public OrgViewVo(Integer sid) {
        this.sid = sid;
        Org org = WkOrgCache.getInstance().findBySid(sid);

        this.name = org != null ? org.getName() : "";
        this.id = org != null ? org.getId() : "";
        this.companyName = (org != null && org.getParent() != null) ? org.getParent().getName() : "";
        
    }

    public OrgViewVo(Integer sid, String name) {
        this.sid = sid;
        this.name = name;
        this.companyName = "";
    }

    public OrgViewVo(Integer sid, String name, String companyName) {
        this.sid = sid;
        this.name = name;
        this.companyName = companyName;
    }

    public OrgViewVo(Integer sid, String depName, String name, String companyName) {
        this.sid = sid;
        this.name = name;
        this.depName = depName;
        this.companyName = companyName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrgViewVo other = (OrgViewVo) obj;
        return Objects.equals(this.sid,
                other.sid);
    }
}
