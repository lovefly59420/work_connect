/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.attachment;

import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.web.logic.components.WCAttachmentLogicComponents;
import com.cy.work.connect.web.logic.components.WCMemoAttachmentLogicComponent;
import java.io.File;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.StreamedContent;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WPAttachmentCompant extends AttachmentCompant {

    /**
     *
     */
    private static final long serialVersionUID = -9003625642199616851L;

    private final Integer userSid;

    private final Integer departmetSid;

    public WPAttachmentCompant(Integer userSid, Integer departmetSid) {
        this.userSid = userSid;
        this.departmetSid = departmetSid;
    }

    @Override
    protected List<AttachmentVO> getPersistedAttachments(String wc_Sid) {
        return WCAttachmentLogicComponents.getInstance().getAttachmentsByWCSid(wc_Sid);
    }

    public void copyAttachment(String wcMemoSid) {
        List<AttachmentVO> memoAtts =
            WCMemoAttachmentLogicComponent.getInstance().getAttachmentsByWCMemoSid(wcMemoSid);
        memoAtts.forEach(
            item -> {
                try {
                    String fileName = FilenameUtils.getName(item.getAttName());
                    String fileNameExtension = FilenameUtils.getExtension(fileName);
                    if (!super.validateByFileNameAndResponse(fileName)) {
                        return;
                    }
                    File tempFile = null;
                    AttachmentUtils.createDirectoryIfNotExist(getDirectoryName());
                    StreamedContent sd = AttachmentUtils.createStreamedContent(item,
                        "wcMemoMaster");
                    tempFile =
                        AttachmentUtils.saveToTemporaryFile(
                            sd.getStream(), getDirectoryName(), fileNameExtension);
                    AttachmentCondition ac = new AttachmentCondition("", "", false);
                    AttachmentVO attachment = this.createAndPersistAttachment(item.getAttName(),
                        ac);
                    File serverSideFile = AttachmentUtils.getServerSideFile(attachment,
                        getDirectoryName());
                    if (!rewrite(tempFile, serverSideFile, fileNameExtension)) {
                        tempFile.renameTo(serverSideFile);
                    }
                    this.getOneTimeUploadAttachmentVOs().add(attachment);
                    this.getSelectedAttachmentVOs().add(attachment);
                } catch (Exception e) {
                    log.warn("copyAttachment ERROR", e);
                }
            });
    }

    @Override
    public String getDirectoryName() {
        return "wcMaster";
    }

    @Override
    protected AttachmentVO createAndPersistAttachment(
        String fileName, AttachmentCondition attachmentCondition) {
        if (attachmentCondition.isAutoMappingEntity()) {
            AttachmentVO result =
                WCAttachmentLogicComponents.getInstance()
                    .createAttachment(
                        attachmentCondition.getEntity_sid(),
                        attachmentCondition.getEntity_no(),
                        fileName,
                        userSid,
                        departmetSid);
            if (attachmentCondition.getUploadAttCallBack() != null) {
                attachmentCondition.getUploadAttCallBack().doUploadAtt(result);
            }
            return result;
        } else {
            return WCAttachmentLogicComponents.getInstance()
                .createAttachment("", "", fileName, userSid, departmetSid);
        }
    }

    @Override
    public Integer getFileSizeLimit() {
        return 50;
    }
}
