/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.config;

import com.cy.work.connect.logic.helper.ClearCacheHelper;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class StartupAware implements ApplicationContextAware {


    //一定得加，強制初始化此 class 之後，先初始化 ClearCacheHelper 再執行，避免出現 NullPointerException
    @SuppressWarnings("unused")
    @Autowired
    private ClearCacheHelper clearCacheHelper;

    @Override
    public void setApplicationContext(ApplicationContext actx) throws BeansException {
        ClearCacheHelper.process();
    }
}
