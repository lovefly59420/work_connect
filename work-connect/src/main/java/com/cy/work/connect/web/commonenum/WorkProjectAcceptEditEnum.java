/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.commonenum;

import lombok.Getter;

/**
 * 編輯權限列舉
 *
 * @author brain0925_liao
 */
public enum WorkProjectAcceptEditEnum {
    WorkProjectAcceptEditEnum1("1", "部門"),

    WorkProjectAcceptEditEnum2("2", "個人");

    @Getter
    private final String id;
    @Getter
    private final String name;

    WorkProjectAcceptEditEnum(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
