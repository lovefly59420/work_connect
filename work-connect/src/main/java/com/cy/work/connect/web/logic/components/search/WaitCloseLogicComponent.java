/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components.search;

import com.cy.commons.enums.Activation;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.utils.ReqularPattenUtils;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.column.search.WaitCloseListColumn;
import com.cy.work.connect.web.logic.components.CustomColumnLogicComponent;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.view.vo.column.search.WaitCloseColumnVO;
import com.cy.work.connect.web.view.vo.search.ApplicationDepInquireVO;
import com.cy.work.connect.web.view.vo.search.query.WaitCloseQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 申請單位查詢 查詢邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class WaitCloseLogicComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7539974589632633151L;

    private final WCReportCustomColumnUrlType urlType = WCReportCustomColumnUrlType.CLOSE_LIST;
    private final transient Comparator<ApplicationDepInquireVO> applicationDepInquireVOComparator =
        new Comparator<ApplicationDepInquireVO>() {
            @Override
            public int compare(ApplicationDepInquireVO obj1, ApplicationDepInquireVO obj2) {
                final Date seq1 = obj1.getCreateDt();
                final Date seq2 = obj2.getCreateDt();
                return seq1.compareTo(seq2);
            }
        };
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;
    @Autowired
    private CustomColumnLogicComponent customColumnLogicComponent;
    @Autowired
    private WCMenuTagLogicComponent menuTagLogicComponent;
    @Autowired
    private WCCategoryTagLogicComponent categoryTagLogicComponent;
    @Autowired
    private ReqularPattenUtils reqularUtils;
    @Autowired
    private WCTagManager wcTagManager;

    /**
     * 取得 申請單位 查詢物件List
     *
     * @param query
     * @param userSids
     * @param sid      工作聯絡單Sid
     * @return
     */
    public List<ApplicationDepInquireVO> search(
        WaitCloseQuery query, List<Integer> userSids, String sid) {
        try {
            if (query.getStatusList() == null || query.getStatusList().isEmpty()) {
                return Lists.newArrayList();
            }

            List<ApplicationDepInquireVO> result =
                jdbc.queryForList(this.bulidSqlByWaitClose(query, userSids, sid)).stream()
                    .map(
                        each -> {
                            ApplicationDepInquireVO obj =
                                new ApplicationDepInquireVO((String) each.get("wc_sid"));
                            obj.setWcNo((String) each.get("wc_no"));
                            obj.setCreateDate(
                                new DateTime(each.get("create_dt")).toString("yyyy/MM/dd"));
                            obj.setCreateDt((Date) each.get("create_dt"));
                            MenuTagVO menuTag =
                                menuTagLogicComponent.findToBySid((String) each.get("menuTag_sid"));
                            obj.setMenuTagName(menuTag.getName());
                            obj.setCategoryName(
                                categoryTagLogicComponent
                                    .getCategoryTagEditVOBySid(menuTag.getCategorySid())
                                    .getCategoryName());
                            obj.setApplicationUserName(
                                WkUserUtils.findNameBySid((Integer) each.get("create_usr")));
                            obj.setTheme(WkStringUtils.removeCtrlChr((String) each.get("theme")));
                            WCStatus wcStatus = WCStatus.valueOf((String) each.get("wc_status"));
                            obj.setStatusName(wcStatus.getVal());
                            if (each.get("category_tag") != null) {
                                try {
                                    String categoryTagStr = (String) each.get("category_tag");
                                    CategoryTagTo ct =
                                        WkJsonUtils.getInstance()
                                            .fromJson(categoryTagStr, CategoryTagTo.class);
                                    ct.getTagSids()
                                        .forEach(
                                            item -> {
                                                obj.bulidExecTag(wcTagManager.getWCTagBySid(item));
                                            });
                                } catch (Exception e) {
                                    log.warn("settingObject ERROR", e);
                                }
                            }
                            return obj;
                        })
                    .collect(Collectors.toList());

            if (!Strings.isNullOrEmpty(query.getTheme())) {
                result =
                    result.stream()
                        .filter(
                            each ->
                                (each.getTheme()
                                    + ((each.getTgSids() != null && !each.getTgSids().isEmpty())
                                    ? "("
                                    : "")
                                    + each.getTagInfo()
                                    + ((each.getTgSids() != null && !each.getTgSids().isEmpty())
                                    ? ")"
                                    : ""))
                                    .toLowerCase()
                                    .contains(query.getTheme().toLowerCase()))
                        .collect(Collectors.toList());
            }
            try {
                Collections.sort(result, applicationDepInquireVOComparator);
            } catch (Exception e) {
                log.warn("search ERROR", e);
            }
            return result;
        } catch (Exception e) {
            log.warn("取得申請單位查詢物件List ERROR!!", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 建立主要 Sql
     *
     * @param query
     * @param userSids
     * @return
     */
    private String bulidSqlByWaitClose(WaitCloseQuery query, List<Integer> userSids, String sid) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ").append("\r\n");
        sql.append("        wc.wc_sid ,wc.wc_no ,wc.create_dt ,wc.menuTag_sid ,create_usr ")
            .append("\r\n");
        sql.append("        ,wc.theme ,wc.wc_status,wc.category_tag ").append("\r\n");
        sql.append("FROM wc_master wc ").append("\r\n");
        sql.append("WHERE wc.create_usr in (")
            .append(
                userSids.stream().map(each -> String.valueOf(each))
                    .collect(Collectors.joining(",")))
            .append(") ")
            .append("\r\n");
        if (!Strings.isNullOrEmpty(sid)) {
            sql.append(" AND wc.wc_sid = '" + sid + "'");
        }
        if (!Strings.isNullOrEmpty(query.getCategorySid())) {
            sql.append("AND wc.menuTag_sid in (")
                .append(
                    menuTagLogicComponent.getMenuTagSearchVO("", query.getCategorySid(), null)
                        .stream()
                        .map(each -> each.getSid())
                        .collect(Collectors.joining("','", "'", "'")))
                .append(") ")
                .append("\r\n");
        }
        if (query.getStatusList() != null && !query.getStatusList().isEmpty()) {
            sql.append("AND wc.wc_status in (")
                .append(query.getStatusList().stream().collect(Collectors.joining("','", "'", "'")))
                .append(") ")
                .append("\r\n");
        }
        if (!Strings.isNullOrEmpty(query.getApplicationUserName())) {
            sql.append("AND wc.create_usr in (")
                .append(
                    WkUserUtils.findByNameLike(query.getApplicationUserName(), SecurityFacade.getCompanyId()).stream()
                        .map(each -> String.valueOf(each))
                        .collect(Collectors.joining(",")))
                .append(") ")
                .append("\r\n");
        }
        
        //        if (!Strings.isNullOrEmpty(query.getTheme())) {
        //            sql.append("AND wc.theme LIKE
        // '%").append(reqularUtils.replaceIllegalSqlLikeStr(query.getTheme())).append("%'
        // ").append("\r\n");
        //        }
        if (!Strings.isNullOrEmpty(query.getContent())) {
            sql.append("AND wc.content LIKE '%")
                .append(reqularUtils.replaceIllegalSqlLikeStr(query.getContent()))
                .append("%' ")
                .append("\r\n");
        }
        if (query.getStartDate() != null && query.getEndDate() != null) {
            sql.append("AND wc.create_dt BETWEEN '")
                .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                .append(" 00:00:00' AND '")
                .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                .append(" 23:59:59' ")
                .append("\r\n");
        } else if (query.getStartDate() != null) {
            sql.append("AND wc.create_dt >= '")
                .append(new DateTime(query.getStartDate()).toString("yyyy-MM-dd"))
                .append(" 00:00:00'  ")
                .append("\r\n");
        } else if (query.getEndDate() != null) {
            sql.append("AND wc.create_dt <= '")
                .append(new DateTime(query.getEndDate()).toString("yyyy-MM-dd"))
                .append("  23:59:59'  ")
                .append("\r\n");
        }
        sql.append(" ORDER BY wc.create_dt ").append("\r\n");
        return sql.toString();
    }

    /**
     * * 取得欄位介面物件
     *
     * @param userSid
     * @return
     */
    public WaitCloseColumnVO getColumnSetting(Integer userSid) {
        WCReportCustomColumn columDB = customColumnLogicComponent.findCustomColumn(urlType,
            userSid);
        WaitCloseColumnVO vo = new WaitCloseColumnVO();
        if (columDB == null) {
            vo.setIndex(
                customColumnLogicComponent.createDefaultColumnDetail(WaitCloseListColumn.INDEX));
            vo.setCreateDate(
                customColumnLogicComponent.createDefaultColumnDetail(
                    WaitCloseListColumn.CREATE_DATE));
            vo.setCategoryName(
                customColumnLogicComponent.createDefaultColumnDetail(
                    WaitCloseListColumn.CATEGORY_NAME));
            vo.setMenuTagName(
                customColumnLogicComponent.createDefaultColumnDetail(
                    WaitCloseListColumn.MENUTAG_NAME));
            vo.setApplicationUserName(
                customColumnLogicComponent.createDefaultColumnDetail(
                    WaitCloseListColumn.APPLICATION_USER_NAME));
            vo.setTheme(
                customColumnLogicComponent.createDefaultColumnDetail(WaitCloseListColumn.THEME));
            vo.setStatusName(
                customColumnLogicComponent.createDefaultColumnDetail(
                    WaitCloseListColumn.STATUS_NAME));
            vo.setWcNo(
                customColumnLogicComponent.createDefaultColumnDetail(WaitCloseListColumn.WC_NO));
        } else {
            vo.setPageCount(String.valueOf(columDB.getPagecnt()));
            vo.setIndex(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.INDEX, columDB.getInfo()));
            vo.setCreateDate(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.CREATE_DATE, columDB.getInfo()));
            vo.setCategoryName(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.CATEGORY_NAME, columDB.getInfo()));
            vo.setMenuTagName(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.MENUTAG_NAME, columDB.getInfo()));
            vo.setApplicationUserName(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.APPLICATION_USER_NAME, columDB.getInfo()));
            vo.setTheme(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.THEME, columDB.getInfo()));
            vo.setStatusName(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.STATUS_NAME, columDB.getInfo()));
            vo.setWcNo(
                customColumnLogicComponent.transToColumnDetailVO(
                    WaitCloseListColumn.WC_NO, columDB.getInfo()));
        }
        return vo;
    }

    /**
     * 取得 類別(大類) 選項
     *
     * @param loginCompSid
     * @return
     */
    public List<SelectItem> getCategoryItems(Integer loginCompSid) {
        return categoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
            .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
            .collect(Collectors.toList());
    }

    /**
     * 取得 單據狀態 選項
     *
     * @return
     */
    public List<SelectItem> getStatusItems() {
        return Lists.newArrayList(
                WCStatus.NEW_INSTANCE,
                WCStatus.WAITAPPROVE,
                WCStatus.APPROVING,
                WCStatus.RECONSIDERATION,
                WCStatus.APPROVED,
                WCStatus.EXEC,
                WCStatus.EXEC_FINISH,
                WCStatus.CLOSE,
                WCStatus.CLOSE_STOP,
                WCStatus.INVALID)
            .stream()
            .map(each -> new SelectItem(each.name(), each.getVal()))
            .collect(Collectors.toList());
    }
}
