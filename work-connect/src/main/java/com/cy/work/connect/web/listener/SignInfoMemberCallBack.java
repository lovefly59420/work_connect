/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import com.cy.work.connect.web.view.vo.UserViewVO;
import java.io.Serializable;
import java.util.List;

/**
 * @author brain0925_liao
 */
public class SignInfoMemberCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1724541067220081529L;

    public void openReqSignInfo() {
    }

    public void openCheckSignInfo() {
    }

    public void saveReqSignInfoMember(List<UserViewVO> selUserViews) {
    }

    public void saveCheckSignInfoMember(List<UserViewVO> selUserViews) {
    }
}
