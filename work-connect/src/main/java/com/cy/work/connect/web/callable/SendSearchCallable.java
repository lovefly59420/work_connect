/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.callable;

import com.cy.work.connect.logic.helper.WaitAssignLogic;
import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.enums.WorkFunItemComponentType;

import lombok.extern.slf4j.Slf4j;

/**
 * 待派工Call able
 *
 * @author brain0925_liao
 */
@Slf4j
public class SendSearchCallable implements HomeCallable {

    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 選單需計算筆數列舉
     */
    private final WorkFunItemComponentType workFunItemComponentType;
    /**
     * 選單Group物件
     */
    private final WorkFunItemGroupVO workFunItemGroupVO;

    public SendSearchCallable(
        WorkFunItemGroupVO workFunItemGroupVO,
        WorkFunItemComponentType workFunItemComponentType,
        Integer loginUserSid) {
        this.workFunItemGroupVO = workFunItemGroupVO;
        this.workFunItemComponentType = workFunItemComponentType;
        this.loginUserSid = loginUserSid;
    }

    @Override
    public HomeCallableCondition call() throws Exception {

        long count = 0;
        try {
            count = WaitAssignLogic.getInstance().queryCanAssignCount(loginUserSid);
        } catch (Exception e) {
            log.warn("SendSearchCallable ERROR", e);
        }
        HomeCallableCondition reuslt = new HomeCallableCondition();
        if (count > 0) {
            reuslt.setConntString("(" + count + ")");
            reuslt.setCssString("color: red");
        }
        reuslt.setWorkFunItemComponentType(workFunItemComponentType);
        reuslt.setWorkFunItemGroupVO(workFunItemGroupVO);
        return reuslt;
    }
}
