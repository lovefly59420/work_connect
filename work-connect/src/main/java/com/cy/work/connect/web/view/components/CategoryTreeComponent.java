/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.vo.CategoryTagVO;
import com.cy.work.connect.web.listener.CategoryTreeCallBack;
import com.cy.work.connect.web.view.vo.to.CategoryTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * 類別樹 元件
 *
 * @author kasim
 */
@Slf4j
public class CategoryTreeComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4249834941501561018L;

    private final WkStringUtils stringUtils;

    private final CategoryTreeCallBack categoryTreeCallBack;

    @Getter
    @Setter
    /** 類別樹 */
    private TreeNode categoryTree;
    /**
     * 畫面選取
     */
    @Getter
    @Setter
    private TreeNode selectedNode;
    /**
     * 名稱查詢
     */
    @Getter
    @Setter
    private String categoryName;

    /**
     * 查詢時標註顏色
     */
    // private final String searchClz = "ui-state-highlight";
    public CategoryTreeComponent(CategoryTreeCallBack categoryTreeCallBack) {
        this.stringUtils = WkStringUtils.getInstance();
        this.categoryTreeCallBack = categoryTreeCallBack;
    }

    /**
     * 建立類別樹
     *
     * @param categorys
     */
    public void bulidTree(List<CategoryTagVO> categorys) {
        this.categoryTree = new DefaultTreeNode();
        this.selectedNode = null;
        try {
            categorys.forEach(
                bigObj -> {
                    TreeNode bigNode =
                        new DefaultTreeNode(
                            new CategoryTo(bigObj.getSid(), bigObj.getName()), categoryTree);
                    bigObj
                        .getMenuTagVOs()
                        .forEach(
                            middleObj -> {
                                new DefaultTreeNode(
                                    new CategoryTo(
                                        middleObj.getSid(),
                                        middleObj.getName(),
                                        middleObj.isAttachmentType(),
                                        middleObj.getPath()),
                                    bigNode);
                            });
                });
        } catch (Exception e) {
            log.warn("建立類別樹 ERROR!!", e);
            categoryTreeCallBack.showMessage("建立類別樹 ERROR!!");
        }
    }

    /**
     * 依照名稱進行搜尋
     */
    public void searchByCategoryName() {
        this.selectedNode = null;
        this.expandAll(categoryTree, false, true);
        List<TreeNode> nodeList = Lists.newArrayList();
        this.allNode(nodeList, categoryTree);
        nodeList.forEach(
            each -> {
                CategoryTo to = (CategoryTo) each.getData();
                if (!Strings.isNullOrEmpty(categoryName)
                    && stringUtils.contains(to.getName(), categoryName)) {
                    // to.setClz(searchClz);
                    this.expandForChildren(each);
                } else {
                    to.setClz("");
                }
            });
    }

    /**
     * 展開該節點下的所有節點
     *
     * @param root     : 根節點
     * @param isExpand : 設定為 true 則展開全部，反之則收起全部
     * @param isCheck  : 設定為 true 則全部取消勾選，反之則不異動
     */
    protected void expandAll(TreeNode root, boolean isExpand, boolean isCheck) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren()
            .forEach(
                each -> {
                    each.setExpanded(isExpand);
                    if (!isExpand && isCheck) {
                        each.setSelected(false);
                    }
                    this.expandAll(each, isExpand, isCheck);
                });
    }

    /**
     * 將所有樹狀節點加入list中
     *
     * @param list
     * @param root
     */
    public void allNode(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren()
            .forEach(
                each -> {
                    list.add(each);
                    this.allNode(list, each);
                });
    }

    /**
     * 展開該子節點上層的父節點，若該父節點還有父節點也展開，直到根節點。
     *
     * @param children
     */
    private void expandForChildren(TreeNode children) {
        if (children.getParent() != null) {
            children.getParent().setExpanded(true);
            this.expandForChildren(children.getParent());
        }
    }

    /**
     * 選取類別樹選項
     *
     * @param event
     */
    public void onNodeSelect(NodeSelectEvent event) {
        try {
            selectedNode = null;
            if (event.getTreeNode().isLeaf()) {
                selectedNode = event.getTreeNode();
                if (!((CategoryTo) event.getTreeNode().getData()).isAttachmentType()) {
                    categoryTreeCallBack.onNodeSelect();
                }
            } else {
                event.getTreeNode().setSelected(false);
                event.getTreeNode().setExpanded(true);
            }
        } catch (Exception e) {
            log.warn("選取類別樹選項 ERROR!!", e);
            categoryTreeCallBack.showMessage("選取類別樹選項 ERROR!!");
        }
    }

    /**
     * 取得已選取的node sid
     *
     * @return
     */
    public String getSelectedNodeSid() {
        try {
            if (selectedNode == null) {
                return "";
            }
            return ((CategoryTo) selectedNode.getData()).getSid();
        } catch (Exception e) {
            log.warn("取得已選取的node sid ERROR!!", e);
            categoryTreeCallBack.showMessage("取得已選取的node sid ERROR!!");
        }
        return "";
    }

    /**
     * 取得類別類型
     *
     * @return
     */
    public boolean isAttachementType() {
        try {
            if (selectedNode == null) {
                return false;
            }
            return ((CategoryTo) selectedNode.getData()).isAttachmentType();
        } catch (Exception e) {
            log.warn("取得已選取的node sid ERROR!!", e);
            categoryTreeCallBack.showMessage("取得已選取的node sid ERROR!!");
        }
        return false;
    }

    /**
     * 取得類別類型
     *
     * @return
     */
    public String getAttachmentPath() {
        try {
            if (selectedNode == null) {
                return "";
            }
            return ((CategoryTo) selectedNode.getData()).getPath();
        } catch (Exception e) {
            log.warn("取得已選取的node sid ERROR!!", e);
            categoryTreeCallBack.showMessage("取得已選取的node sid ERROR!!");
        }
        return "";
    }
}
