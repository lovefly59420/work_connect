/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.enumtype;

/**
 * 權限類型
 *
 * @author brain0925_liao
 */
public enum PermissionType {
    /**
     * 檢視
     */
    VIEW,
    /**
     * 檢視
     */
    EDIT
}
