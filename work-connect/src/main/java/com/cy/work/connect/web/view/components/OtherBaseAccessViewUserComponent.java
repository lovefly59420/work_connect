/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.PickListCallBack;
import com.cy.work.connect.web.logic.components.WCBasePersonAccessInfoLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.SelectEvent;

/**
 * @author brain0925_liao
 */
@Slf4j
public class OtherBaseAccessViewUserComponent {

    /**
     * 可調整權限部門
     */
    @Getter
    private final LinkedHashSet<UserViewVO> otherBaseAccessViewUsers;
    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 登入部門Sid
     */
    @SuppressWarnings("unused")
    private final Integer loginDepSid;
    /**
     * 登入公司Sid
     */
    private final Integer loginCompSid;
    /**
     * 訊息CallBack
     */
    private final MessageCallBack messageCallBack;
    /**
     * 檢視其他基礎單位可閱部門(OrgTree元件)
     */
    @Getter
    private final List<UserViewVO> viewLimitUserViews;
    /**
     * 挑選其他基礎單位可閱部門(OrgTree元件)
     */
    private final DepTreeComponent selectPermissionDepTreeComponent;
    /**
     * 挑選可調整權限部門(OrgTree元件)
     */
    private final DepTreeComponent selectDepTreeComponent;

    @Getter
    private final PickUserComponent pickUserComponent;
    @Getter
    private final String selectDepDialog_Var = "otherBaseAccessUserpickUserSelDepDialogVar";
    @Getter
    private final String pickUserDialogVar = "sel_otherBaseAccessUser_pickUser_dlg_wav";
    private final LinkedHashSet<User> allUsers;
    /**
     * 挑選調整權限部門
     */
    @Getter
    @Setter
    private UserViewVO selUserViewVO;
    private final PickListCallBack pickListCallBack = new PickListCallBack() {
        /** */
        private static final long serialVersionUID = 1884927679733181817L;

        @Override
        public void saveSelectUser(List<UserViewVO> users) {
            try {
                Preconditions.checkState(
                        selUserViewVO != null && selUserViewVO.getSid() != null, "請挑選使用者！");
                List<User> saveUsers = Lists.newArrayList();
                users.forEach(
                        item -> {
                            saveUsers.add(WkUserCache.getInstance().findBySid(item.getSid()));
                        });
                WCBasePersonAccessInfoLogicComponents.getInstance()
                        .saveOtherBaseAccessViewUser(selUserViewVO.getSid(), loginUserSid,
                                saveUsers);
                loadViewUserList(selUserViewVO);
                DisplayController.getInstance().hidePfWidgetVar(pickUserDialogVar);
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("saveSelectUser ERROR：{}", e.getMessage());
                messageCallBack.showMessage(e.getMessage());
            } catch (Exception e) {
                log.warn("saveSelectUser ERROR", e);
                messageCallBack.showMessage(e.getMessage());
            }
        }
    };
    /**
     * 僅顯示已設定
     */
    @Getter
    @Setter
    private boolean onlyShowSetting;
    /**
     * 全部使用者
     */
    private List<Org> allOrgs;
    /**
     * 挑選欲調整權限部門
     */
    private List<Org> selectOrgs;
    @Getter
    @Setter
    private String userName;

    public OtherBaseAccessViewUserComponent(
            Integer loginUserSid,
            Integer loginDepSid,
            Integer loginCompSid,
            MessageCallBack messageCallBack) {
        this.otherBaseAccessViewUsers = Sets.newLinkedHashSet();
        this.messageCallBack = messageCallBack;
        this.loginUserSid = loginUserSid;
        this.loginDepSid = loginDepSid;
        this.loginCompSid = loginCompSid;
        this.viewLimitUserViews = Lists.newArrayList();
        this.selectPermissionDepTreeComponent = new DepTreeComponent();
        this.selectDepTreeComponent = new DepTreeComponent();
        this.selectOrgs = Lists.newArrayList();
        this.allUsers = Sets.newLinkedHashSet();
        this.pickUserComponent = new PickUserComponent(
                loginUserSid, loginCompSid, messageCallBack, selectDepDialog_Var, pickListCallBack);
        initOrgTree();
        loadData();
    }

    /**
     * 挑選部門
     */
    public void onRowSelect(SelectEvent<UserViewVO> event) {
        UserViewVO obj = (UserViewVO) event.getObject();
        loadViewUserList(obj);
    }

    /**
     * 載入資料
     */
    private void loadData() {
        try {
            this.otherBaseAccessViewUsers.clear();
            this.allOrgs = WkOrgCache.getInstance().findAllDepByCompSid(loginCompSid);
            this.allOrgs.forEach(
                    item -> {
                        List<User> depUsers = WkUserCache.getInstance()
                                .findUserWithManagerByOrgSids(Lists.newArrayList(item.getSid()), Activation.ACTIVE)
                                .stream()
                                .distinct()
                                .collect(Collectors.toList());
                        allUsers.addAll(depUsers);
                        depUsers.forEach(
                                userItem -> {
                                    this.otherBaseAccessViewUsers.add(
                                            new UserViewVO(
                                                    userItem.getSid(),
                                                    userItem.getName(),
                                                    WkOrgUtils.prepareBreadcrumbsByDepName(
                                                            item.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                                });
                    });

        } catch (Exception e) {
            log.warn("loadData ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void loadPickUser() {
        try {
            Preconditions.checkState(selUserViewVO != null && selUserViewVO.getSid() != null,
                    "請挑選使用者！");
            User selUser = WkUserCache.getInstance().findBySid(selUserViewVO.getSid());
            // Org baseOrg =
            // OrgLogicComponents.getInstance().getBaseOrg(selUser.getPrimaryOrg().getSid());
            // List<Org> allChildOrg =
            // OrgLogicComponents.getInstance().getAllChildOrgActiveAndInactive(baseOrg);
            // allChildOrg.add(baseOrg);

            List<Org> canSelectOrgs = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        // if (!allChildOrg.contains(item)) {
                        canSelectOrgs.add(item);
                        // }
                    });

            List<UserViewVO> ps = Lists.newArrayList();
            List<User> viewUsers = WCBasePersonAccessInfoLogicComponents.getInstance()
                    .getOtherBaseAccessViewUser(selUser.getSid());
            if (viewUsers != null) {
                viewUsers.forEach(
                        userItem -> {
                            Org dep = WkOrgCache.getInstance()
                                    .findBySid(userItem.getPrimaryOrg().getSid());
                            ps.add(
                                    new UserViewVO(
                                            userItem.getSid(),
                                            userItem.getName(),
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                    dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                        });
            }
            pickUserComponent.loadUserPicker(ps, selUserViewVO.getSid(), canSelectOrgs);
            DisplayController.getInstance()
                    .showPfWidgetVar("sel_otherBaseAccessUser_pickUser_dlg_wav");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("loadPickUser ERROR：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("loadPickUser ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 切換顯示模式
     */
    public void changeShowMode() {
        this.otherBaseAccessViewUsers.clear();
        if (this.onlyShowSetting) {
            List<Integer> settingPerson = WCBasePersonAccessInfoLogicComponents.getInstance().getSettingOtherBasePerson();
            allUsers.forEach(
                    userItem -> {
                        if (settingPerson.contains(userItem.getSid())) {
                            this.otherBaseAccessViewUsers.add(
                                    new UserViewVO(
                                            userItem.getSid(),
                                            userItem.getName(),
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                    userItem.getPrimaryOrg().getSid(), OrgLevel.DIVISION_LEVEL,
                                                    true, "-")));
                        }
                    });
        } else {
            allUsers.forEach(
                    userItem -> {
                        this.otherBaseAccessViewUsers.add(
                                new UserViewVO(
                                        userItem.getSid(),
                                        userItem.getName(),
                                        WkOrgUtils.prepareBreadcrumbsByDepName(
                                                userItem.getPrimaryOrg().getSid(), OrgLevel.DIVISION_LEVEL, true,
                                                "-")));
                    });
        }
        this.selUserViewVO = null;
        viewLimitUserViews.clear();
    }

    /**
     * 取得挑選部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectDepTreeManager() { return selectDepTreeComponent.getMultipleDepTreeManager(); }

    /**
     * 取得挑選權限部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectPermissionDepTreeManager() { return selectPermissionDepTreeComponent.getMultipleDepTreeManager(); }

    /**
     * 載入挑選部門
     */
    public void loadSelectDepTree() {
        try {
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> selectDeps = Lists.newArrayList();
            this.allOrgs.forEach(
                    item -> {
                        selectDeps.add(item);
                    });
            selectDepTreeComponent.init(
                    selectDeps,
                    selectDeps,
                    topOrg,
                    selectOrgs,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadSelectDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行挑選部門
     */
    public void doSelectDep() {
        try {
            selectOrgs = selectDepTreeComponent.getSelectOrg();
            this.otherBaseAccessViewUsers.clear();
            selectOrgs.forEach(
                    item -> {
                        List<User> depUsers = WkUserCache.getInstance()
                                .findUserWithManagerByOrgSids(Lists.newArrayList(item.getSid()), Activation.ACTIVE)
                                .stream()
                                .distinct()
                                .collect(Collectors.toList());
                        depUsers.forEach(
                                userItem -> {
                                    this.otherBaseAccessViewUsers.add(
                                            new UserViewVO(
                                                    userItem.getSid(),
                                                    userItem.getName(),
                                                    WkOrgUtils.prepareBreadcrumbsByDepName(
                                                            item.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                                });
                    });
            this.selUserViewVO = null;
            viewLimitUserViews.clear();
            DisplayController.getInstance().hidePfWidgetVar("sel_otherBaseAccessUser_dlg_wav");
        } catch (Exception e) {
            log.warn("doSelectDep ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    public void doSearchUserName() {
        try {
            this.otherBaseAccessViewUsers.clear();
            allUsers.forEach(
                    item -> {
                        if (Strings.isNullOrEmpty(userName)) {
                            Org dep = WkOrgCache.getInstance().findBySid(item.getPrimaryOrg().getSid());
                            this.otherBaseAccessViewUsers.add(
                                    new UserViewVO(
                                            item.getSid(),
                                            item.getName(),
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                    dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                        } else if (item.getName().contains(userName)) {
                            Org dep = WkOrgCache.getInstance().findBySid(item.getPrimaryOrg().getSid());
                            this.otherBaseAccessViewUsers.add(
                                    new UserViewVO(
                                            item.getSid(),
                                            item.getName(),
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                    dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                        }
                    });
            this.selUserViewVO = null;
            viewLimitUserViews.clear();
        } catch (Exception e) {
            log.warn("doSearchUserName ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入檢視權限部門
     *
     * @param selOrgViewVo 挑選的部門物件
     */
    private void loadViewUserList(UserViewVO selUserViewVO) {
        try {
            User selUser = WkUserCache.getInstance().findBySid(selUserViewVO.getSid());
            viewLimitUserViews.clear();
            List<User> viewUsers = WCBasePersonAccessInfoLogicComponents.getInstance()
                    .getOtherBaseAccessViewUser(selUser.getSid());
            if (viewUsers != null) {
                viewUsers.forEach(
                        userItem -> {
                            Org dep = WkOrgCache.getInstance()
                                    .findBySid(userItem.getPrimaryOrg().getSid());
                            this.viewLimitUserViews.add(
                                    new UserViewVO(
                                            userItem.getSid(),
                                            userItem.getName(),
                                            WkOrgUtils.prepareBreadcrumbsByDepName(
                                                    dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-")));
                        });
            }
        } catch (Exception e) {
            log.warn("loadViewUserList ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入挑選權限部門
     */
    public void loadPermissionDepTree() {
        loadSelectPermissionDepTree();
    }

    /**
     * 執行挑選權限部門
     */
    public void doSelectPermissionDepTree() {
        try {
            // Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null,
            // "請挑選部門！");
            //
            //
            // WrBaseDepAccessInfoLogicComponents.getInstance().saveLimitBaseAccessViewDep(selOrgViewVo.getSid(), loginUserSid,
            // selectPermissionDepTreeComponent.getSelectOrg());
            // loadViewDepTree(selOrgViewVo);
            //
            // DisplayController.getInstance().hidePfWidgetVar("sel_limitBaseAccessDep_permission_dlg_wav");
        } catch (Exception e) {
            log.warn("doSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行載入挑選權限部門
     */
    private void loadSelectPermissionDepTree() {
        try {
            // Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null,
            // "請挑選部門！");
            // Org topOrg = new Org(loginCompSid);
            // //僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            // boolean clearSelNodeWhenChaneDepTree = true;
            // boolean selectable = true;
            // boolean selectModeSingle = false;
            // Org baseOrg =
            // OrgLogicComponents.getInstance().getBaseOrg(selOrgViewVo.getSid());
            // List<Org> allChildOrg =
            // OrgLogicComponents.getInstance().getAllChildOrgActiveAndInactive(baseOrg);
            // allChildOrg.add(baseOrg);
            // List<Org> limitBaseAccessViewDeps =
            // WrBaseDepAccessInfoLogicComponents.getInstance().getLimitBaseAccessViewDep(selOrgViewVo.getSid());
            // List<Org> showOrgs = Lists.newArrayList();
            // showOrgs.addAll(allChildOrg);
            // allChildOrg.forEach(item -> {
            // List<Org> allParents =
            // OrgLogicComponents.getInstance().getAllParentOrgs(item);
            // if (allParents == null || allParents.isEmpty()) {
            // return;
            // }
            // showOrgs.addAll(allParents.stream()
            // .filter(each -> !showOrgs.contains(each))
            // .collect(Collectors.toList()));
            // });
            // try {
            // Collections.sort(showOrgs, new Comparator<Org>() {
            // @Override
            // public int compare(Org o1, Org o2) {
            // return o1.getSid() - o2.getSid();
            // }
            // });
            // } catch (Exception e) {
            // log.error("sort Error", e);
            // }
            //// List<Org> otherBaseAccessShowDeps = Lists.newArrayList();
            //// this.allOrgs.forEach(item -> {
            //// if (!allChildOrg.contains(item)) {
            //// otherBaseAccessShowDeps.add(item);
            //// }
            //// });
            // selectPermissionDepTreeComponent.init(showOrgs, allChildOrg,
            // topOrg, ((limitBaseAccessViewDeps == null) ? allChildOrg :
            // limitBaseAccessViewDeps),
            // clearSelNodeWhenChaneDepTree, selectable, selectModeSingle);
        } catch (Exception e) {
            log.warn("loadSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 建立Empty 挑選權限部門Tree Manager
     */
    private void initSelectPermissionDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectPermissionDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                topOrg,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * 建立Empty 挑選部門Tree Manager
     */
    private void initSelectDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                topOrg,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    private void initOrgTree() {
        initSelectPermissionDepTreeComponent();
        initSelectDepTreeComponent();
    }
}
