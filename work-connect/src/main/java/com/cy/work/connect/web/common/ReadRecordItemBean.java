/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author kasim
 */
@Controller
@Scope("request")
public class ReadRecordItemBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -671584291332123362L;

    /**
     * 建立是否閱讀選項
     *
     * @return
     */
    public List<SelectItem> getReadRecordItems() {
        return Lists.newArrayList(new SelectItem(false, "未閱讀"), new SelectItem(true, "已閱讀"));
    }
}
