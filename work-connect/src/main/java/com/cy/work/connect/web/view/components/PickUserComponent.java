/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.listener.PickListCallBack;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.DualListModel;

/**
 * @author brain0925_liao
 */
@Slf4j
public class PickUserComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9021657295198055947L;
    /**
     * 挑選可調整權限部門(OrgTree元件)
     */
    private final DepTreeComponent selectDepTreeComponent;

    private final String selectDepDialog_Var;
    @SuppressWarnings("unused")
    private final Integer loginUserSid;
    private final MessageCallBack messageCallBack;
    private final Integer compSid;
    private final LinkedHashSet<UserViewVO> allCompUser;
    private final PickListCallBack pickListCallBack;
    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;
    private List<UserViewVO> leftViewVO;
    @Getter
    @Setter
    private String filterNameValue;
    private List<Org> allCanSelectOrgs;
    private List<Org> selectOrgs;

    public PickUserComponent(
        Integer loginUserSid,
        Integer compSid,
        MessageCallBack messageCallBack,
        String selectDepDialog_Var,
        PickListCallBack pickListCallBack) {
        this.messageCallBack = messageCallBack;
        this.pickListCallBack = pickListCallBack;
        this.loginUserSid = loginUserSid;
        this.selectDepDialog_Var = selectDepDialog_Var;
        this.compSid = compSid;
        this.pickUsers = new DualListModel<>();
        this.allCompUser = Sets.newLinkedHashSet();
        this.selectDepTreeComponent = new DepTreeComponent();
        initSelectDepTreeComponent();
    }

    public void savePickList() {
        pickListCallBack.saveSelectUser(pickUsers.getTarget());
    }

    /**
     * 取得挑選部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectDepTreeManager() {
        return selectDepTreeComponent.getMultipleDepTreeManager();
    }

    /**
     * 載入挑選部門
     */
    public void loadSelectDepTree() {
        try {
            Org topOrg = new Org(compSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> showOrgs = Lists.newArrayList();
            showOrgs.addAll(allCanSelectOrgs);
            allCanSelectOrgs.forEach(
                item -> {
                    List<Org> allParents = WkOrgCache.getInstance().findAllParent(item.getSid());
                    if (allParents == null || allParents.isEmpty()) {
                        return;
                    }
                    showOrgs.addAll(
                        allParents.stream()
                            .filter(each -> !showOrgs.contains(each))
                            .collect(Collectors.toList()));
                });
            try {
                Collections.sort(
                    showOrgs,
                    new Comparator<Org>() {
                        @Override
                        public int compare(Org o1, Org o2) {
                            return o1.getSid().compareTo(o2.getSid());
                        }
                    });
            } catch (Exception e) {
                log.warn("sort Error", e);
            }

            selectDepTreeComponent.init(
                showOrgs,
                allCanSelectOrgs,
                topOrg,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        } catch (Exception e) {
            log.warn("loadSelectDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行挑選部門
     */
    public void doSelectDep() {
        try {
            selectOrgs = selectDepTreeComponent.getSelectOrg();
            loadSelectDepartment(selectOrgs);
            DisplayController.getInstance().hidePfWidgetVar(selectDepDialog_Var);
        } catch (Exception e) {
            log.warn("doSelectDep ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 建立Empty 挑選部門Tree Manager
     */
    private void initSelectDepTreeComponent() {
        Org topOrg = new Org(compSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectDepTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            topOrg,
            Lists.newArrayList(),
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);
    }

    public void loadUserPicker(
        List<UserViewVO> selUserViewVO, Integer selUserSid, List<Org> allCanSelectOrgs) {
        this.selectOrgs = Lists.newArrayList();
        this.allCanSelectOrgs = allCanSelectOrgs;
        allCompUser.clear();
        try {
            allCanSelectOrgs.forEach(
                item -> {
                    allCompUser.addAll(
                        WCUserLogic.getInstance().findByDepSid(item.getSid()));
                });
        } catch (Exception e) {
            log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Lists.newArrayList();
        leftViewVO.addAll(allCompUser);
        List<UserViewVO> rightViewVO = Lists.newArrayList();
        rightViewVO.addAll(selUserViewVO);
        pickUsers.setSource(removeTargetUserView(leftViewVO, rightViewVO));
        pickUsers.setTarget(rightViewVO);
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
        List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
            item -> {
                if (!target.contains(item)) {
                    filterSource.add(item);
                }
            });
        return filterSource;
    }

    private void loadSelectDepartment(List<Org> departments) {
        //        if (departments == null || departments.size() == 0) {
        //            return;
        //        }
        leftViewVO.clear();
        LinkedHashSet<UserViewVO> selectDepUserView = Sets.newLinkedHashSet();
        departments.forEach(
            item -> {
                selectDepUserView.addAll(
                    WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        pickUsers.setSource(
            removeTargetUserView(Lists.newArrayList(selectDepUserView), pickUsers.getTarget()));
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(
                removeTargetUserView(Lists.newArrayList(allCompUser), pickUsers.getTarget()));
        } else {
            pickUsers.setSource(
                removeTargetUserView(
                    allCompUser.stream()
                        .filter(
                            each -> each.getName().toLowerCase()
                                .contains(filterNameValue.toLowerCase()))
                        .collect(Collectors.toList()),
                    pickUsers.getTarget()));
        }
    }
}
