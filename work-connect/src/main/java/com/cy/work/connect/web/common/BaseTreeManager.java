/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.TreeNode;

/**
 * Tree 元件
 *
 * @author jimmy_chou
 */
public class BaseTreeManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2451548156155091397L;
    /**
     * 選取的節點
     */
    protected transient List<TreeNode> selNode = Lists.newArrayList();
    /**
     * 所有Tree
     */
    @Getter
    @Setter
    protected transient TreeNode rootNode;
    /**
     * 名稱(like 名稱%)
     */
    @Getter
    @Setter
    private String searchText;

    /** 預設挑選的部門List */
    // private List<Org> defaultSelOrg;

    /**
     * 清除已選擇的node
     */
    public void cleanSelNode() {
        searchText = "";
        selNode = Lists.newArrayList();
    }

    /**
     * 展開該節點下的所有節點
     *
     * @param root     : 根節點
     * @param isExpand : 設定為 true 則展開全部，反之則收起全部
     * @param isCheck  : 設定為 true 則全部取消勾選，反之則不異動
     */
    protected void expandAll(TreeNode root, boolean isExpand, boolean isCheck) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        for (TreeNode treeNode : root.getChildren()) {
            treeNode.setExpanded(isExpand);
            if (!isExpand && isCheck) {
                treeNode.setSelected(false);
            }
            expandAll(treeNode, isExpand, isCheck);
        }
    }

    /**
     * 展開該子節點上層的父節點，若該父節點還有父節點也展開，直到根節點。
     *
     * @param children : 子節點
     */
    protected void expandToParent(TreeNode children) {
        TreeNode parent = children.getParent();
        if (parent != null) {
            parent.setExpanded(true);
            expandToParent(parent);
        }
    }

    /**
     * 儲存所有子節點到list
     *
     * @param list : 存放的list
     * @param root : 根節點
     */
    public void storeAllSubNodes(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        for (TreeNode treeNode : root.getChildren()) {
            list.add(treeNode);
            storeAllSubNodes(list, treeNode);
        }
    }
}
