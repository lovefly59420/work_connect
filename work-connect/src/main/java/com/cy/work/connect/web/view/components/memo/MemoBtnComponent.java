/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components.memo;

import com.cy.work.connect.web.listener.MemoMaintainCallBack;
import java.io.Serializable;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 備忘錄 按鈕
 *
 * @author kasim
 */
@Slf4j
public class MemoBtnComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4884949582936138813L;

    /**
     * 維護相關事件
     */
    private final MemoMaintainCallBack memoMaintainCallBack;

    @Getter
    /** 顯示 編輯 按鈕 */
    private Boolean showBtnEdit;

    @Getter
    /** 顯示 附加檔案 按鈕 */
    private Boolean showBtnAtt;

    @Getter
    /** 顯示 轉工作聯絡單 按鈕 */
    private Boolean showBtnTransConnect;

    @Getter
    /** 顯示 確定 按鈕 */
    private Boolean showBtnSubmite;

    @Getter
    /** 顯示 取消 按鈕 */
    private Boolean showBtnCancel;

    @Getter
    /** 顯示 上一筆 按鈕 */
    private Boolean showBtnUp;

    @Getter
    /** 顯示 下一筆 按鈕 */
    private Boolean showBtnDown;
    /**
     * 顯示 回首頁 按鈕
     */
    @Getter
    private boolean showBtnBackList;

    public MemoBtnComponent(MemoMaintainCallBack memoMaintainCallBack) {
        this.memoMaintainCallBack = memoMaintainCallBack;
    }

    /**
     * 初始化
     *
     * @param showBtnEdit
     */
    public void init(
        Boolean hasMaintain, boolean showPreviousBtn, boolean showNextBtn,
        boolean showBackListBtn) {
        this.showBtnEdit = hasMaintain;
        this.showBtnAtt = true;
        this.showBtnTransConnect = true;
        this.showBtnSubmite = false;
        this.showBtnCancel = false;
        this.showBtnUp = showPreviousBtn;
        this.showBtnDown = showNextBtn;
        this.showBtnBackList = showBackListBtn;
    }

    /**
     * 變更為編輯模式
     */
    public void editMode() {
        this.showBtnEdit = false;
        this.showBtnAtt = false;
        this.showBtnTransConnect = false;
        this.showBtnSubmite = true;
        this.showBtnCancel = true;
        this.showBtnUp = false;
        this.showBtnDown = false;
        this.showBtnBackList = false;
    }

    /**
     * 執行編輯按鈕
     */
    public void clickEditBtn() {
        try {
            memoMaintainCallBack.clickEdit();
        } catch (Exception e) {
            log.warn("執行備忘錄編輯按鈕動作 Error！！", e);
            memoMaintainCallBack.showMessage("執行備忘錄編輯按鈕動作 Error！！");
        }
    }

    /**
     * 執行確定按鈕
     */
    public void clickSubmiteBtn() {
        try {
            memoMaintainCallBack.clickSubmite();
        } catch (Exception e) {
            log.warn("執行備忘錄更新動作 Error！！", e);
            memoMaintainCallBack.showMessage("執行備忘錄更新動作 Error！！");
        }
    }

    /**
     * 執行取消按鈕
     */
    public void clickCancelBtn() {
        try {
            memoMaintainCallBack.clickCancel();
        } catch (Exception e) {
            log.warn("執行備忘錄取消編輯按鈕動作 Error！！", e);
            memoMaintainCallBack.showMessage("執行備忘錄取消編輯按鈕動作 Error！！");
        }
    }

    public void clickAttBtn() {
        try {
            memoMaintainCallBack.clickAtt();
        } catch (Exception e) {
            log.warn("執行備忘錄上傳附件按鈕動作 Error！！", e);
            memoMaintainCallBack.showMessage("執行備忘錄上傳附件按鈕動作 Error！！");
        }
    }

    public void clickTransBtn() {
        try {
            memoMaintainCallBack.clickTrans();
        } catch (Exception e) {
            log.warn("執行備忘錄轉工作聯絡單按鈕動作 Error！！", e);
            memoMaintainCallBack.showMessage("執行備忘錄轉工作聯絡單按鈕動作 Error！！");
        }
    }

    public void clickUp() {
        try {
            memoMaintainCallBack.clickUp();
        } catch (Exception e) {
            log.warn("clickUp ERROR", e);
            memoMaintainCallBack.showMessage(e.getMessage());
        }
    }

    public void clickDown() {
        try {
            memoMaintainCallBack.clickDown();
        } catch (Exception e) {
            log.warn("clickDown ERROR", e);
            memoMaintainCallBack.showMessage(e.getMessage());
        }
    }

    public void unDone() {
    }
}
