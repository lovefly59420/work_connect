/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 處理清單 - 列表資料
 *
 * @author kasim
 */
@Data
@EqualsAndHashCode(of = {"wcSid"})
public class ProcessListVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5675754610199197464L;

    /**
     * 聯絡單 sid
     */
    private String wcSid;

    /**
     * 聯絡單 單號
     */
    private String wcNo;

    /**
     * 建立日期
     */
    private String createDate;

    private Date createDt;

    private String content;

    /**
     * 類別(第一層)
     */
    private String categoryName;

    /**
     * 單據名稱(第二層)
     */
    private String menuTagName;

    /**
     * 執行項目(第三層)
     */
    private String tagInfo = "";

    /**
     * 執行人員
     */
    private String execUserName = "";

    private String execUserNameInfo = "";

    private String exportExecDepStatus = "";

    private String execDepStatus = "";

    private String execDepStatusInfo = "";

    private String wcStatus;

    /**
     * 主題
     */
    private String theme;

    private List<String> tgSids = Lists.newArrayList();

    private List<Integer> userSids = Lists.newArrayList();

    private List<Integer> depSids = Lists.newArrayList();

    private String reqDepName;

    /**
     * 是否閱讀
     */
    private String hasReadRecord;

    private String unReadCount;

    private boolean hasUnReadCount = false;

    public ProcessListVO(String wcSid) {
        this.wcSid = wcSid;
    }

    public void replaceValue(ProcessListVO updateObject) {
        this.wcSid = updateObject.getWcSid();
        this.wcNo = updateObject.getWcNo();
        this.createDate = updateObject.getCreateDate();
        this.menuTagName = updateObject.getMenuTagName();
        this.categoryName = updateObject.getCategoryName();
        this.execUserName = updateObject.getExecUserName();
        this.execUserNameInfo = updateObject.getExecUserNameInfo();
        this.theme = updateObject.getTheme();
        this.tagInfo = updateObject.getTagInfo();
        this.wcStatus = updateObject.getWcStatus();
        this.reqDepName = updateObject.getReqDepName();
        this.hasReadRecord = updateObject.getHasReadRecord();
        this.unReadCount = updateObject.getUnReadCount();
        this.hasUnReadCount = updateObject.isHasUnReadCount();
    }

    public void bulidExecTag(WCTag tag) {
        if (tag == null) {
            return;
        }
        if (tgSids.contains(tag.getSid())) {
            return;
        }
        tgSids.add(tag.getSid());
        if (!Strings.isNullOrEmpty(tagInfo)) {
            tagInfo += "、";
        }
        tagInfo += tag.getTagName();
    }

    public void buildExecDep(Org dep, WCExceDepStatus wcExceDepStatus) {
        if (dep == null) {
            return;
        }
        if (depSids.contains(dep.getSid())) {
            return;
        }
        depSids.add(dep.getSid());
        if (!Strings.isNullOrEmpty(execDepStatus)) {
            execDepStatus += "、";
        }
        if (!Strings.isNullOrEmpty(execDepStatusInfo)) {
            execDepStatusInfo += " <br> ";
        }
        if (wcExceDepStatus.equals(WCExceDepStatus.PROCEDD)) {
            execDepStatus += "<span style=\"color:red\">";
            execDepStatusInfo += "<span style=\"color:red\">";
        }
//        exportExecDepStatus += "" + dep.getName() + "[" + wcExceDepStatus.getValue() + "]";
//        execDepStatus += "" + dep.getName() + "[" + wcExceDepStatus.getValue() + "]";
//        execDepStatusInfo += dep.getName() + "[" + wcExceDepStatus.getValue() + "]";

        exportExecDepStatus += "" + dep.getOrgNameWithParentIfDuplicate() + "[" + wcExceDepStatus.getValue() + "]";
        execDepStatus += "" + dep.getOrgNameWithParentIfDuplicate() + "[" + wcExceDepStatus.getValue() + "]";
        execDepStatusInfo += dep.getOrgNameWithParentIfDuplicate() + "[" + wcExceDepStatus.getValue() + "]";
        if (wcExceDepStatus.equals(WCExceDepStatus.PROCEDD)) {
            execDepStatus += "</span>";
            execDepStatusInfo += "</span>";
        }
    }

    public void bulidExecUser(User user) {
        if (user == null) {
            return;
        }

        if (userSids.contains(user.getSid())) {
            return;
        }
        userSids.add(user.getSid());
        if (!Strings.isNullOrEmpty(execUserName)) {
            execUserName += "、";
        }
        if (!Strings.isNullOrEmpty(execUserNameInfo)) {
            execUserNameInfo += " <br> ";
        }
        execUserName += user.getName();
        execUserNameInfo += user.getName();
    }
}
