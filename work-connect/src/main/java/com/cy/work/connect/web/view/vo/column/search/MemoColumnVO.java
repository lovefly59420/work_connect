/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column.search;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Data;

/**
 * 備忘錄查詢 - dataTable 欄位資訊
 *
 * @author kasim
 */
@Data
public class MemoColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6695442577854106195L;
    /**
     * 一頁 X 筆
     */
    private String pageCount = "50";
    /**
     * 序
     */
    private ColumnDetailVO index;
    /**
     * 建立日期
     */
    private ColumnDetailVO createDate;
    /**
     * 建立部門
     */
    private ColumnDetailVO createDep;
    /**
     * 主題
     */
    private ColumnDetailVO theme;
    /**
     * 單號
     */
    private ColumnDetailVO memoNo;
    /**
     * 轉工作聯絡單狀態
     */
    private ColumnDetailVO transWcStatus;

    private ColumnDetailVO readStatus;
}
