/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.logic.components.WCOrgLogic;
import com.cy.work.connect.web.logic.components.WCBaseDepAccessInfoLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.SelectEvent;

/**
 * @author brain0925_liao
 */
@Slf4j
public class LimitBaseAccessViewDepComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4951156805770450125L;
    /**
     * 可調整權限部門
     */
    @Getter
    private final List<OrgViewVo> limitBaseAccessViewDeps;
    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 登入部門Sid
     */
    @SuppressWarnings("unused")
    private final Integer loginDepSid;
    /**
     * 登入公司Sid
     */
    private final Integer loginCompSid;
    /**
     * 訊息CallBack
     */
    private final MessageCallBack messageCallBack;
    /**
     * 檢視其他基礎單位可閱部門(OrgTree元件)
     */
    private final DepTreeComponent viewDepTreeComponent;
    /**
     * 挑選其他基礎單位可閱部門(OrgTree元件)
     */
    private final DepTreeComponent selectPermissionDepTreeComponent;
    /**
     * 挑選可調整權限部門(OrgTree元件)
     */
    private final DepTreeComponent selectDepTreeComponent;
    /**
     * 挑選調整權限部門
     */
    @Getter
    @Setter
    private OrgViewVo selOrgViewVo;
    /**
     * 全部部門
     */
    private List<Org> allOrgs;
    /**
     * 挑選欲調整權限部門
     */
    private List<Org> selectOrgs;

    public LimitBaseAccessViewDepComponent(
            Integer loginUserSid,
            Integer loginDepSid,
            Integer loginCompSid,
            MessageCallBack messageCallBack) {
        this.limitBaseAccessViewDeps = Lists.newArrayList();
        this.messageCallBack = messageCallBack;
        this.loginUserSid = loginUserSid;
        this.loginDepSid = loginDepSid;
        this.loginCompSid = loginCompSid;
        this.viewDepTreeComponent = new DepTreeComponent();
        this.selectPermissionDepTreeComponent = new DepTreeComponent();
        this.selectDepTreeComponent = new DepTreeComponent();
        this.selectOrgs = Lists.newArrayList();
        initOrgTree();
        loadData();
    }

    /**
     * 挑選部門
     */
    public void onRowSelect(SelectEvent<OrgViewVo> event) {
        OrgViewVo obj = event.getObject();
        loadViewDepTree(obj);
    }

    /**
     * 載入資料
     */
    private void loadData() {
        try {
            this.limitBaseAccessViewDeps.clear();
            WkOrgCache.getInstance()
                    .findAllDepByCompSid(loginCompSid)
                    .forEach(
                            item -> {
                                OrgViewVo ov = new OrgViewVo(item.getSid(), item.getName());
                                List<Org> limitBaseAccessViewDeps = WCBaseDepAccessInfoLogicComponents.getInstance()
                                        .getLimitBaseAccessViewDep(item.getSid());
                                if (limitBaseAccessViewDeps != null) {
                                    ov.setModifyed(true);
                                }
                                this.limitBaseAccessViewDeps.add(ov);
                            });
            this.allOrgs = WkOrgCache.getInstance().findAllDepByCompSid(loginCompSid);
        } catch (Exception e) {
            log.warn("loadData ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 取得檢視權限部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getViewDepTreeManager() { return viewDepTreeComponent.getMultipleDepTreeManager(); }

    /**
     * 取得挑選部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectDepTreeManager() { return selectDepTreeComponent.getMultipleDepTreeManager(); }

    /**
     * 取得挑選權限部門Tree Manager
     *
     * @return
     */
    public MultipleDepTreeManager getSelectPermissionDepTreeManager() { return selectPermissionDepTreeComponent.getMultipleDepTreeManager(); }

    /**
     * 載入挑選部門
     */
    public void loadSelectDepTree() {
        try {
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            List<Org> selectDeps = Lists.newArrayList();
            this.allOrgs.forEach(
                    item -> {
                        selectDeps.add(item);
                    });
            selectDepTreeComponent.init(
                    selectDeps,
                    selectDeps,
                    topOrg,
                    selectOrgs,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadSelectDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行挑選部門
     */
    public void doSelectDep() {
        try {
            selectOrgs = selectDepTreeComponent.getSelectOrg();
            this.limitBaseAccessViewDeps.clear();
            selectOrgs.forEach(
                    item -> {
                        OrgViewVo ov = new OrgViewVo(item.getSid(), item.getName());
                        List<Org> limitBaseAccessViewDeps = WCBaseDepAccessInfoLogicComponents.getInstance()
                                .getLimitBaseAccessViewDep(item.getSid());
                        if (limitBaseAccessViewDeps != null) {
                            ov.setModifyed(true);
                        }
                        this.limitBaseAccessViewDeps.add(ov);
                    });
            this.selOrgViewVo = null;
            initViewDepTreeComponent();
            DisplayController.getInstance().hidePfWidgetVar("sel_limitBaseAccessDep_dlg_wav");
        } catch (Exception e) {
            log.warn("doSelectDep ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入檢視權限部門
     *
     * @param selOrgViewVo 挑選的部門物件
     */
    private void loadViewDepTree(OrgViewVo selOrgViewVo) {
        try {
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = false;
            boolean selectable = false;
            boolean selectModeSingle = false;
            Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(selOrgViewVo.getSid());
            List<Org> allChildOrg = WkOrgCache.getInstance().findAllChild(baseOrg.getSid());
            allChildOrg.add(baseOrg);

            List<Org> limitBaseAccessViewDeps = WCBaseDepAccessInfoLogicComponents.getInstance()
                    .getLimitBaseAccessViewDep(selOrgViewVo.getSid());
            List<Org> showOrgs = Lists.newArrayList();
            showOrgs.addAll(allChildOrg);
            allChildOrg.forEach(
                    item -> {
                        List<Org> allParents = WkOrgCache.getInstance().findAllParent(item.getSid());
                        if (allParents == null || allParents.isEmpty()) {
                            return;
                        }
                        showOrgs.addAll(
                                allParents.stream()
                                        .filter(each -> !showOrgs.contains(each))
                                        .collect(Collectors.toList()));
                    });
            try {
                Collections.sort(
                        showOrgs,
                        new Comparator<Org>() {
                            @Override
                            public int compare(Org o1, Org o2) {
                                return o1.getSid().compareTo(o2.getSid());
                            }
                        });
            } catch (Exception e) {
                log.warn("sort Error", e);
            }

            viewDepTreeComponent.init(
                    showOrgs,
                    allChildOrg,
                    topOrg,
                    ((limitBaseAccessViewDeps == null) ? allChildOrg : limitBaseAccessViewDeps),
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadViewDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 載入挑選權限部門
     */
    public void loadPermissionDepTree() {
        loadSelectPermissionDepTree();
    }

    /**
     * 執行挑選權限部門
     */
    public void doSelectPermissionDepTree() {
        try {
            Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null,
                    "請挑選部門！");
            WCBaseDepAccessInfoLogicComponents.getInstance()
                    .saveLimitBaseAccessViewDep(
                            selOrgViewVo.getSid(), loginUserSid,
                            selectPermissionDepTreeComponent.getSelectOrg());
            loadViewDepTree(selOrgViewVo);
            limitBaseAccessViewDeps.forEach(
                    item -> {
                        if (item.getSid().equals(selOrgViewVo.getSid())) {
                            item.setModifyed(true);
                        }
                    });
            DisplayController.getInstance()
                    .hidePfWidgetVar("sel_limitBaseAccessDep_permission_dlg_wav");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("doSelectPermissionDepTree ERROR：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("doSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 執行載入挑選權限部門
     */
    private void loadSelectPermissionDepTree() {
        try {
            Preconditions.checkState(selOrgViewVo != null && selOrgViewVo.getSid() != null,
                    "請挑選部門！");
            Org topOrg = new Org(loginCompSid);
            // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(selOrgViewVo.getSid());
            List<Org> allChildOrg = WkOrgCache.getInstance().findAllChild(baseOrg.getSid());
            allChildOrg.add(baseOrg);
            List<Org> limitBaseAccessViewDeps = WCBaseDepAccessInfoLogicComponents.getInstance()
                    .getLimitBaseAccessViewDep(selOrgViewVo.getSid());
            List<Org> showOrgs = Lists.newArrayList();
            showOrgs.addAll(allChildOrg);
            allChildOrg.forEach(
                    item -> {
                        List<Org> allParents = WkOrgCache.getInstance().findAllParent(item.getSid());
                        if (allParents == null || allParents.isEmpty()) {
                            return;
                        }
                        showOrgs.addAll(
                                allParents.stream()
                                        .filter(each -> !showOrgs.contains(each))
                                        .collect(Collectors.toList()));
                    });
            try {
                Collections.sort(
                        showOrgs,
                        new Comparator<Org>() {
                            @Override
                            public int compare(Org o1, Org o2) {
                                return o1.getSid().compareTo(o2.getSid());
                            }
                        });
            } catch (Exception e) {
                log.warn("sort Error", e);
            }
            // List<Org> otherBaseAccessShowDeps = Lists.newArrayList();
            // this.allOrgs.forEach(item -> {
            // if (!allChildOrg.contains(item)) {
            // otherBaseAccessShowDeps.add(item);
            // }
            // });
            selectPermissionDepTreeComponent.init(
                    showOrgs,
                    allChildOrg,
                    topOrg,
                    ((limitBaseAccessViewDeps == null) ? allChildOrg : limitBaseAccessViewDeps),
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
            DisplayController.getInstance()
                    .showPfWidgetVar("sel_limitBaseAccessDep_permission_dlg_wav");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("loadSelectPermissionDepTree ERROR：{}", e.getMessage());
            messageCallBack.showMessage(e.getMessage());
        } catch (Exception e) {
            log.warn("loadSelectPermissionDepTree ERROR", e);
            messageCallBack.showMessage(e.getMessage());
        }
    }

    /**
     * 建立Empty 檢視部門Tree Manager
     */
    private void initViewDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = false;
        boolean selectable = false;
        boolean selectModeSingle = false;
        viewDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                topOrg,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * 建立Empty 挑選權限部門Tree Manager
     */
    private void initSelectPermissionDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectPermissionDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                topOrg,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * 建立Empty 挑選部門Tree Manager
     */
    private void initSelectDepTreeComponent() {
        Org topOrg = new Org(loginCompSid);
        // 僅檢視,故切換顯示停用部門時,不需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                topOrg,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    private void initOrgTree() {
        initViewDepTreeComponent();
        initSelectPermissionDepTreeComponent();
        initSelectDepTreeComponent();
    }
}
