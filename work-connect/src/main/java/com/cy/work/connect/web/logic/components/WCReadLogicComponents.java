/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.CountUnreadReplyLogic;
import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCReadReceiptsManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCReadReceipts;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.converter.to.ReadRecord;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCReadReceiptStatus;
import com.cy.work.connect.vo.enums.WCReadStatus;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 已未讀邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCReadLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2040465307343301822L;

    private static WCReadLogicComponents instance;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    @Autowired
    private WCReadReceiptsManager readReceiptsManager;
    @Autowired
    private WkUserCache userManager;

    @Autowired
    private CountUnreadReplyLogic countUnreadReplyLogic;

    @PersistenceContext
    private EntityManager em;

    public static WCReadLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCReadLogicComponents.instance = this;
    }

    /**
     * 取得已閱讀使用者 By 工作聯絡單Sid
     *
     * @param wc_sid 工作聯絡單Sid
     * @return
     */
    public List<User> getReadedUser(String wc_sid) {
        WCMaster wc = wcMasterManager.findBySid(wc_sid);
        List<User> users = Lists.newArrayList();
        if (wc.getRead_record() != null && wc.getRead_record().getRRecord() != null) {
            wc.getRead_record()
                    .getRRecord()
                    .forEach(
                            item -> {
                                try {
                                    if (item.getRead().equals(WCReadStatus.HASREAD.name())) {
                                        users.add(userManager.findBySid(Integer.valueOf(item.getReader())));
                                    }
                                } catch (Exception e) {
                                    log.warn("getReadedUser", e);
                                }
                            });
        }
        return users;
    }

    /**
     * 將傳入的使用者該工作聯絡單的閱讀狀態變為待閱讀(除了登入者以外)[僅轉寄使用]
     *
     * @param wc_sid       工作聯絡單Sid
     * @param personsSids  使用者Sids
     * @param loginUserSid 登入者Sid
     */
    public void updateWaitReadWCMasterForPersons(
            String wc_sid, List<Integer> personsSids, Integer loginUserSid) {
        try {
            WCMaster wc = wcMasterManager.findBySid(wc_sid);
            List<RRcordTo> rRcordTos = Lists.newArrayList();
            if (wc.getRead_record() != null && wc.getRead_record().getRRecord() != null) {
                wc.getRead_record()
                        .getRRecord()
                        .forEach(
                                item -> {
                                    if (!item.getReader().equals(String.valueOf(loginUserSid))) {
                                        if (personsSids.contains(Integer.valueOf(item.getReader()))) {
                                            item.setRead(WCReadStatus.WAIT_READ.name());
                                        }
                                    }
                                    rRcordTos.add(item);
                                });
            }
            wcMasterCustomLogicManager.updateWCReadRecord(wc_sid, rRcordTos);
            em.refresh(wc);
        } catch (Exception e) {
            log.warn("updateWaitReadWCMasterForPersons Error", e);
        }
    }

    /**
     * 將傳入的部門下的所有使用者該工作聯絡單的閱讀狀態變為待閱讀(除了登入者以外)[僅轉寄使用]
     *
     * @param wc_sid       工作聯絡單Sid
     * @param depSid       部門Sid
     * @param loginUserSid 登入者Sid
     */
    public void updateWaitReadWCMasterForDep(String wc_sid, Integer depSid, Integer loginUserSid) {
        try {
            WCMaster wc = wcMasterManager.findBySid(wc_sid);
            List<RRcordTo> rRcordTos = Lists.newArrayList();
            if (wc.getRead_record() != null && wc.getRead_record().getRRecord() != null) {
                wc.getRead_record()
                        .getRRecord()
                        .forEach(
                                item -> {
                                    if (!item.getReader().equals(String.valueOf(loginUserSid))) {
                                        User user = userManager.findBySid(
                                                Integer.valueOf(item.getReader()));
                                        if (user.getPrimaryOrg().getSid().equals(depSid)) {
                                            item.setRead(WCReadStatus.WAIT_READ.name());
                                        }
                                    }
                                    rRcordTos.add(item);
                                });
            }
            wcMasterCustomLogicManager.updateWCReadRecord(wc_sid, rRcordTos);
            em.refresh(wc);
        } catch (Exception e) {
            log.warn("updateWaitReadWCMasterForDep Error", e);
        }
    }

    /**
     * 將該工作聯絡單閱讀狀態都更改為待閱讀(除了登入者以外)
     *
     * @param wc_sid       工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     */
    public void updateWaitReadWCMaster(String wc_sid, Integer loginUserSid) {
        try {
            WCMaster wc = wcMasterManager.findBySid(wc_sid);
            List<RRcordTo> rRcordTos = Lists.newArrayList();
            if (wc.getRead_record() != null && wc.getRead_record().getRRecord() != null) {
                wc.getRead_record()
                        .getRRecord()
                        .forEach(
                                item -> {
                                    if (!item.getReader().equals(String.valueOf(loginUserSid))) {
                                        item.setRead(WCReadStatus.WAIT_READ.name());
                                    } else {
                                        item.setRead(WCReadStatus.HASREAD.name());
                                        item.setReadDay(
                                                ToolsDate.transDateToString(
                                                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                                                        new Date()));
                                    }
                                    rRcordTos.add(item);
                                });
            }

            List<RRcordTo> loginReaderList = rRcordTos.stream()
                    .filter(each -> each.getReader().equals(String.valueOf(loginUserSid)))
                    .collect(Collectors.toList());
            if (loginReaderList == null || loginReaderList.isEmpty()) {
                RRcordTo rRcordTo = new RRcordTo();
                rRcordTo.setRead(WCReadStatus.HASREAD.name());
                rRcordTo.setReadDay(
                        ToolsDate.transDateToString(
                                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
                rRcordTo.setReader(String.valueOf(loginUserSid));
                rRcordTos.add(rRcordTo);
            }
            wcMasterCustomLogicManager.updateWCReadRecord(wc_sid, rRcordTos);
            em.refresh(wc);
        } catch (Exception e) {
            log.warn("updateWaitReadWCMaster Error", e);
        }
    }

    /**
     * 將該使用者在該工作聯絡單更新成已閱讀
     *
     * @param wc_sid       工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     */
    public void readWCMaster(String wc_sid, Integer loginUserSid) {
        try {
            // 更新閱讀記錄
            WCMaster wc = wcMasterManager.findBySid(wc_sid);
            wc.setRead_record(settingReadRecord(wc, loginUserSid));
            wcMasterCustomLogicManager.updateWCReadRecord(wc_sid, wc.getRead_record().getRRecord());
            em.refresh(wc);

            // 更新可閱名單
            WCReadReceipts readReceipts = readReceiptsManager.findReadReceiptsByWCSidAndReader(wc_sid, loginUserSid);
            if (readReceipts != null) {
                readReceipts.setReadtime(new Date());
                readReceipts.setReadreceipt(WCReadReceiptStatus.READ);
                readReceiptsManager.save(readReceipts);
            }
        } catch (Exception e) {
            log.warn("readWCMaster Error", e);
        }
    }

    public void readReplyReadWCMaster(String wc_sid, Integer loginUserSid) {
        try {
            WCMaster wc = wcMasterManager.findBySid(wc_sid);
            this.countUnreadReplyLogic.processClearUnreadCount(wc_sid, loginUserSid);
            em.refresh(wc);
        } catch (Exception e) {
            log.warn("readReplyReadWCMaster Error", e);
        }
    }

    private ReadRecord settingReadRecord(WCMaster wc, Integer loginUserSid) {
        List<RRcordTo> rRcordTos = Lists.newArrayList();
        if (wc.getRead_record() != null
                && WkStringUtils.notEmpty(wc.getRead_record().getRRecord())) {
            rRcordTos = wc.getRead_record().getRRecord();
        }

        // 先移除登入者原有的閱讀資訊
        rRcordTos = rRcordTos.stream()
                .filter(each -> !WkCommonUtils.compareByStr(each.getReader(), loginUserSid))
                .collect(Collectors.toList());

        // 加入一筆新的登入者閱讀資訊
        RRcordTo rRcordTo = new RRcordTo();
        rRcordTo.setRead(WCReadStatus.HASREAD.name());
        rRcordTo.setReadDay(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
        rRcordTo.setReader(String.valueOf(loginUserSid));
        rRcordTos.add(rRcordTo);

        ReadRecord readRecord = new ReadRecord();
        readRecord.setRRecord(rRcordTos);
        return readRecord;
    }
}
