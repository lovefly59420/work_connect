/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search.query;

import com.cy.work.connect.vo.enums.MemoTransType;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;

/**
 * 備忘錄查詢 查詢物件
 *
 * @author kasim
 */
@Data
@NoArgsConstructor
public class MemoQuery implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4117270220315587849L;
    /**
     * 主題
     */
    private String theme;
    /**
     * 內容
     */
    private String content;
    /**
     * 建立區間(起)
     */
    private Date startDate;
    /**
     * 建立區間(訖)
     */
    private Date endDate;

    private String selCreateDapSid;

    private String selTransType = "";

    private String selReadStatus = "";

    public void init() {
        this.theme = null;
        this.content = null;
        this.selCreateDapSid = "";
        this.selReadStatus = "";
        this.changeDateIntervaltoDay();
        this.selTransType = MemoTransType.UNTRANSED.name();
    }

    /**
     * 上個月
     */
    public void changeDateIntervalPreMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusMonths(1);
        this.startDate = lastDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = lastDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 本月份
     */
    public void changeDateIntervalThisMonth() {
        this.startDate = new LocalDate().dayOfMonth().withMinimumValue().toDate();
        this.endDate = new LocalDate().dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 下個月
     */
    public void changeDateIntervalNextMonth() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate nextDate = new LocalDate(date).plusMonths(1);
        this.startDate = nextDate.dayOfMonth().withMinimumValue().toDate();
        this.endDate = nextDate.dayOfMonth().withMaximumValue().toDate();
    }

    /**
     * 前一日
     */
    public void changeDateIntervalPreDay() {
        Date date = new Date();
        if (this.startDate != null) {
            date = this.startDate;
        }
        LocalDate lastDate = new LocalDate(date).minusDays(1);
        this.startDate = lastDate.toDate();
        this.endDate = lastDate.toDate();
    }

    /**
     * 今日
     */
    public void changeDateIntervaltoDay() {
        this.startDate = new Date();
        this.endDate = new Date();
    }
}
