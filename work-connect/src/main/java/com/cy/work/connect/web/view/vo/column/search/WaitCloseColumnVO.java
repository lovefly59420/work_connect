/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column.search;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import java.io.Serializable;
import lombok.Data;

/**
 * 申請單位查詢 - dataTable 欄位資訊
 *
 * @author kasim
 */
@Data
public class WaitCloseColumnVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4546453075483077779L;
    /**
     * 一頁 X 筆
     */
    private String pageCount = "50";
    /**
     * 序
     */
    private ColumnDetailVO index;
    /**
     * 建立日期
     */
    private ColumnDetailVO createDate;
    /**
     * 類別(第一層)
     */
    private ColumnDetailVO categoryName;
    /**
     * 單據名稱(第二層)
     */
    private ColumnDetailVO menuTagName;
    /**
     * 申請人
     */
    private ColumnDetailVO applicationUserName;
    /**
     * 主題
     */
    private ColumnDetailVO theme;
    /**
     * 單據狀態
     */
    private ColumnDetailVO statusName;
    /**
     * 單號
     */
    private ColumnDetailVO wcNo;
}
