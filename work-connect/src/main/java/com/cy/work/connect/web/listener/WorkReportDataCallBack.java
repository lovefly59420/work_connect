/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

/**
 * @author marlow_chen
 */
public interface WorkReportDataCallBack {

    String getWorkReportData();

    void updateWorkReportData(String content, String memo);
    
    String getContent();
    String getMemo();
}
