/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import com.cy.commons.enums.Activation;
import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jimmy_chou
 */
public class TagVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3441210434315231798L;

    @Getter
    private final String sid;
    @Getter
    private final String categoryName;
    @Getter
    private final String menuName;
    @Getter
    private final String name;
    @Getter
    private final Integer seq;
    @Getter
    private String categorySid;
    @Getter
    private String menuSid;
    @Getter
    private Activation status;
    @Getter
    private MenuTagSearchVO menuTagSearchVO;
    @Getter
    private TagSearchVO subTagSearchVO;
    @Getter
    private ExecDepSettingSearchVO execDepSettingSearchVO;
    /* 第一階(大類：類別)層樣式 */
    @Getter
    @Setter
    private String firstStyle;
    /* 第二階(中類：單據名稱)層樣式 */
    @Getter
    @Setter
    private String secondStyle;
    /* 第三階(小類：執行項目)層樣式 */
    @Getter
    @Setter
    private String thirdStyle;

    public TagVO(String sid, String categoryName, String menuName, String name, Integer seq) {
        this.sid = sid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.name = name;
        this.seq = seq;
    }

    public TagVO(
        String sid,
        String categoryName,
        String menuName,
        String name,
        Integer seq,
        Activation status) {
        this.sid = sid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.name = name;
        this.seq = seq;
        this.status = status;
    }

    public TagVO(
        String sid,
        String categoryName,
        String menuName,
        String name,
        Integer seq,
        Activation status,
        MenuTagSearchVO menuTagSearchVO,
        TagSearchVO subTagSearchVO,
        ExecDepSettingSearchVO execDepSettingSearchVO,
        String categorySid,
        String menuSid) {
        this.sid = sid;
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.name = name;
        this.seq = seq;
        this.status = status;
        this.menuTagSearchVO = menuTagSearchVO;
        this.subTagSearchVO = subTagSearchVO;
        this.execDepSettingSearchVO = execDepSettingSearchVO;
        this.categorySid = categorySid;
        this.menuSid = menuSid;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 28 * hash + Objects.hashCode(this.sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TagVO other = (TagVO) obj;
        return Objects.equals(this.sid,
            other.sid);
    }
}
