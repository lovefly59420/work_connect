/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import com.cy.work.connect.logic.vo.AttachmentVO;
import java.io.Serializable;

/**
 * 刪除附件Callback (刪除附件使用)
 *
 * @author brain0925_liao
 */
public class DeleteAttCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -226335045430197199L;

    public void doDeleteAtt(AttachmentVO att) {
    }
}
