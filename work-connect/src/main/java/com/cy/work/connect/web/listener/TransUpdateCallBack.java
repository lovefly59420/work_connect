/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import java.io.Serializable;

/**
 * 轉寄更新CallBack(轉寄功能使用)
 *
 * @author brain0925_liao
 */
public class TransUpdateCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4661370856844009033L;

    public void doUpdateData() {
    }
}
