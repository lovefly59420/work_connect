package com.cy.work.connect.web.view.components;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.logic.helper.PermissionLogicForCanUse;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.web.listener.WorkReportButtonCallBack;
import com.cy.work.connect.web.logic.components.WCFavoriteLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;

import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class WorkReportBtnComponent extends ButtonComponent {

    /**
     *
     */
    private static final long serialVersionUID = 2075043756831427668L;

    @Getter
    private final WorkReportButtonCallBack workReportButtonListener;
    @Getter
    private final BpmComponent bpmComponent;
    /**
     *
     */
    private final Integer loginUserSid;
    @Getter
    @Setter
    private Boolean editMode = false;
    @Getter
    @Setter
    private String invalidReson;
    @Getter
    @Setter
    private String rollbackReason;
    @Getter
    private String invalidCannotEnterReson;
    @Getter
    private String traceBtnCss;
    @Getter
    private String favoriteBtnCss;
    @Getter
    private boolean isFavrite = false;
    @Getter
    private boolean showBtnComponent = false;

    public WorkReportBtnComponent(
            BpmComponent bpmComponent,
            WorkReportButtonCallBack workReportButtonListener,
            Integer loginUserSid) {
        this.bpmComponent = bpmComponent;
        this.workReportButtonListener = workReportButtonListener;
        this.loginUserSid = loginUserSid;
    }

    private void initButton() {
        this.traceBtnCss = "";
        this.favoriteBtnCss = "";
        this.setViewPersonAble(false);
        this.setExecReplyAble(false);
        this.setExecTransAble(false);
        this.setTransPersonAble(false);
        this.setEditAble(false);
        this.setExecFinishAble(false);
        this.setReplyAble(false);
        this.setAttachAble(false);
        this.setSendAble(false);
        this.setCancalAble(false);
        this.setPreviousAble(false);
        this.setNextAble(false);
        this.setBackList(false);
        this.setDeleteAble(false);
        this.setShowDeleteBtn(false);
        this.setShowCloseAble(false);
        this.setAddExecDepAble(false);
        this.setShowReqCommontBtn(false);
        this.setShowReqContinueCommontBtn(false);
        this.setShowExecCommontBtn(false);
        this.setShowAddReqSignBtn(false);
        this.setShowAddExecSignBtn(false);
        this.setFavoriteAble(false);
        showBtnComponent = false;
        this.favoriteBtnCss = "";
    }

    private void checkFavorite(WCMaster wRMaster) {
        isFavrite = WCFavoriteLogicComponents.getInstance()
                .isFavorite(wRMaster.getSid(), loginUserSid);
        if (isFavrite) {
            this.favoriteBtnCss = "color: red";
        }
        this.setFavoriteAble(true);
        showBtnComponent = true;
    }

    public void loadButtonAble(
            WCMaster wcMaster, boolean showPreviousBtn, boolean showNextBtn, boolean showBackListBtn) {

        initButton();
        if (wcMaster == null) {
            return;
        }
        this.setPreviousAble(showPreviousBtn);
        this.setNextAble(showNextBtn);
        this.setBackList(showBackListBtn);
        if (showBackListBtn || showNextBtn || showPreviousBtn) {
            showBtnComponent = true;
        }
        if (WCStatus.INVALID.equals(wcMaster.getWc_status())) {
            this.invalidCannotEnterReson = "";
            return;
        }

        if (editMode) {
            loadEDITButton();
            this.setPreviousAble(false);
            this.setNextAble(false);
            this.setBackList(false);
            return;
        }

        checkFavorite(wcMaster);
        this.invalidCannotEnterReson = "";

        // ====================================
        // 判斷功能按鈕可使用權限
        // ====================================
        this.loadBtnStatus(wcMaster);
    }

    /**
     * 判斷各功能按鈕的使用權限
     * 
     * @param wcMaster 主檔
     */
    private void loadBtnStatus(WCMaster wcMaster) {

        // ====================================
        // 1.可使用編輯、上傳附件 (申請單位)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseReqSideEditAndUploadAttch(wcMaster, SecurityFacade.getUserSid())) {
            this.setEditAble(true);
            this.setAttachAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 2.可上傳附件 (執行單位)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanExecSideUploadAttch(wcMaster, SecurityFacade.getUserSid())) {
            this.setAttachAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 3.可編輯可閱人員名單
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanEditSpecViewPerson(wcMaster, SecurityFacade.getUserSid())) {
            this.setViewPersonAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 4.可使用回覆
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseReply(wcMaster, SecurityFacade.getUserSid())) {
            this.setExecReplyAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 5.可使用收藏
        // ====================================
        // 皆可使用

        // ====================================
        // 6.可使用執行方轉單
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideDepTransExecutor(
                wcMaster,
                SecurityFacade.getUserSid(),
                SecurityFacade.getPrimaryOrgSid())) {
            this.setExecTransAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 7.可使用執行方派工給執行人員
        // ====================================
        // 執行單位派工
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideAssignExecutor(wcMaster, SecurityFacade.getUserSid())) {
            this.setTransPersonAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 8.可使用執行方加派執行單位
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideDepAddExecDep(wcMaster, SecurityFacade.getUserSid())) {
            this.setAddExecDepAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 9.可使用執行方『執行完成』
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideComplete(wcMaster, SecurityFacade.getUserSid())) {
            this.setExecFinishAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 10.可使用需求方『確認完成』(結案)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseReqSideClose(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowCloseAble(true);
            showBtnComponent = true;
        }

        // ====================================
        // 11.可使用意見說明(申請單位)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseReqSideComments(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowReqCommontBtn(true);
            showBtnComponent = true;
        }

        //
        // ====================================
        // 12.可使用意見說明(申請單位會簽者)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseReqSideCountersignComments(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowReqContinueCommontBtn(true);
            showBtnComponent = true;
        }

        // ====================================
        // 13.可使用意見說明(執行方簽核者)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideCmments(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowExecCommontBtn(true);
            showBtnComponent = true;
        }

        // ====================================
        // 14.可使用意見說明(執行方會簽核者)
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideCountersignCmments(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowExecContinueCommontBtn(true);
            showBtnComponent = true;
        }

        // ====================================
        // 15.可使用申請方加會簽
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseReqSideAddCountersign(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowAddReqSignBtn(true);
            showBtnComponent = true;
        }

        // ====================================
        // 16.可使用執行方加會簽
        // ====================================
        if (PermissionLogicForCanUse.getInstance().isCanUseExecSideAddCountersign(wcMaster, SecurityFacade.getUserSid())) {
            this.setShowAddExecSignBtn(true);
            showBtnComponent = true;
        }
    }

    private void loadEDITButton() {
        this.setSendAble(true);
        this.setCancalAble(true);
        showBtnComponent = true;
    }
    
    public void openDlgStopExecDep() {
        this.rollbackReason = "不需執行";
        DisplayController.getInstance().showPfWidgetVar("dlg_bpm_stop_execDep");
    }
}
