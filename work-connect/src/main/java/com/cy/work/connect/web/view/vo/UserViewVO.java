/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import java.util.Set;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.collect.Sets;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
@EqualsAndHashCode(of = "sid")
public class UserViewVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5732889598824743471L;
    /**
     * User SID
     */
    @Getter
    private final Integer sid;
    /**
     * User Name
     */
    @Getter
    private final String name;
    /**
     * 部門名稱
     */
    @Setter
    @Getter
    private String departmentName;

    @Setter
    @Getter
    private boolean moveAble = true;
    @Setter
    @Getter
    private boolean modifyed = false;

    @Setter
    @Getter
    /** 是否為管理者 */
    private boolean isManager = false;

    @Setter
    @Getter
    /** 執行相關部門列表 */
    private Set<Integer> execRelatedDeps = Sets.newHashSet();

    @Setter
    @Getter
    /** 管理部門列表 */
    private Set<Integer> managedDeps = Sets.newHashSet();

    @Setter
    @Getter
    /** 部門sid */
    private Integer orgSid;

    public UserViewVO() {
        this.sid = null;
        this.name = "";
    }

    public UserViewVO(Integer sid) {
        this.sid = sid;

        User user = WkUserCache.getInstance().findBySid(sid);
        this.name = user != null ? user.getName() : "";

        // ====================================
        // 取得部門資訊
        // ====================================
        Org primaryOrg = null;
        if (user.getPrimaryOrg() != null) {
            primaryOrg = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        }
        if (primaryOrg != null) {
            this.orgSid = primaryOrg.getSid();
            this.departmentName = WkOrgUtils.prepareBreadcrumbsByDepName(
                    this.orgSid,
                    OrgLevel.DIVISION_LEVEL,
                    true,
                    "-");
        }
    }

    public UserViewVO(Integer sid, String name) {
        this.sid = sid;
        this.name = name;
    }

    public UserViewVO(Integer sid, String name, String departmentName) {
        this.sid = sid;
        this.name = name;
        this.departmentName = departmentName;
    }

}
