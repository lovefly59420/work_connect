/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class EditTraceNoteVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2005327634420451990L;

    @Getter
    private String traceId;
    @Getter
    private String title;
    @Setter
    @Getter
    private String memo;
    @Setter
    @Getter
    private Date noticeDate;

    public EditTraceNoteVO() {
    }

    public EditTraceNoteVO(String traceId, String title, String memo, Date noticeDate) {
        this.traceId = traceId;
        this.title = title;
        this.memo = memo;
        this.noticeDate = noticeDate;
    }
}
