package com.cy.work.connect.web.controller;

import com.google.common.base.Strings;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author jimmy_chou
 */
@FacesConverter(value = "converterDate")
public class ConverterDate implements Converter {

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat sdfback = new SimpleDateFormat("yyyyMMdd");
        // 主要用來判斷是否日期轉換錯誤，如轉換錯誤則在前端顯示提示訊息
        sdf.setLenient(false);
        sdfback.setLenient(false);
        if (arg2.contains("/")) {
            try {
                String[] timeStr = arg2.split("/");
                if (timeStr == null || timeStr.length != 3) {
                    arg0.getAttributes().put("converterDateFail", "converterDateFail");
                    return null;
                }
                if (Strings.isNullOrEmpty(timeStr[0])
                    || Strings.isNullOrEmpty(timeStr[1])
                    || Strings.isNullOrEmpty(timeStr[2])) {
                    arg0.getAttributes().put("converterDateFail", "converterDateFail");
                    return null;
                }
                int month = Integer.valueOf(timeStr[1]);
                int day = Integer.valueOf(timeStr[2]);
                if (month == 0 || month > 12 || day == 0 || day > 31) {
                    arg0.getAttributes().put("converterDateFail", "converterDateFail");
                    return null;
                }
                return sdf.parse(arg2);
            } catch (NumberFormatException | ParseException e) {
                arg0.getAttributes().put("converterDateFail", "converterDateFail");
                return null;
            }
        } else if (arg2.length() == 8) {
            try {
                // String yearStr = arg2.substring(0, 4);
                String monthStr = arg2.substring(4, 6);
                String dayStr = arg2.substring(6, 8);
                int month = Integer.valueOf(monthStr);
                int day = Integer.valueOf(dayStr);
                if (month == 0 || month > 12 || day == 0 || day > 31) {
                    arg0.getAttributes().put("converterDateFail", "converterDateFail");
                    return null;
                }
                return sdfback.parse(arg2);
            } catch (NumberFormatException | ParseException e) {
                arg0.getAttributes().put("converterDateFail", "converterDateFail");
                return null;
            }
        } else {
            arg0.getAttributes().put("converterDateFail", "converterDateFail");
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try {
            sdf.setLenient(false);
            return sdf.format((Date) arg2);
        } catch (Exception e) {
            return "";
        }
    }
}
