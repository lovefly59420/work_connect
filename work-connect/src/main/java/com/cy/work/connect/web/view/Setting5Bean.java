/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.connect.web.listener.MessageCallBack;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.components.LimitBaseAccessViewDepComponent;
import com.cy.work.connect.web.view.components.LimitBaseAccessViewUserComponent;
import com.cy.work.connect.web.view.components.OtherBaseAccessViewDepComponent;
import com.cy.work.connect.web.view.components.OtherBaseAccessViewUserComponent;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.component.tabview.TabView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 工作聯絡單可閱權限調整 MBean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
public class Setting5Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2896453547159285246L;
    /**
     * view TableView
     */
    @Getter
    @Setter
    private TabView permissionTabView;
    /**
     * view 錯誤訊息
     */
    @Getter
    private String errorMessage;

    private final MessageCallBack messageCallBack =
        new MessageCallBack() {
            /** */
            private static final long serialVersionUID = -4536722644148280299L;

            @Override
            public void showMessage(String m) {
                errorMessage = m;
                DisplayController.getInstance().update("confimDlgTemplate");
                DisplayController.getInstance().showPfWidgetVar("confimDlgTemplate");
            }
        };
    /**
     * 基礎單位可閱權限增加(聯集)
     */
    @Getter
    private OtherBaseAccessViewDepComponent otherBaseAccessViewDepComponent;
    /**
     * 基礎單位可閱權限減少(交集)
     */
    @Getter
    private LimitBaseAccessViewDepComponent limitBaseAccessViewDepComponent;
    /**
     * 基礎單位成員可閱權限減少(交集)
     */
    @Getter
    private LimitBaseAccessViewUserComponent limitBaseAccessViewUserComponent;
    /**
     * 基礎單位成員可閱權限增加(聯集)
     */
    @Getter
    private OtherBaseAccessViewUserComponent otherBaseAccessViewUserComponent;
    /**
     * 登入者物件
     */
    @Getter
    private UserViewVO userViewVO;
    /**
     * 登入者部門物件
     */
    @Getter
    private OrgViewVo depViewVo;
    /**
     * 登入者公司物件
     */
    @Getter
    private OrgViewVo compViewVo;

    @PostConstruct
    public void init() {
        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        
        otherBaseAccessViewDepComponent =
            new OtherBaseAccessViewDepComponent(
                userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid(), messageCallBack);
        limitBaseAccessViewDepComponent =
            new LimitBaseAccessViewDepComponent(
                userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid(), messageCallBack);
        limitBaseAccessViewUserComponent =
            new LimitBaseAccessViewUserComponent(
                userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid(), messageCallBack);
        otherBaseAccessViewUserComponent =
            new OtherBaseAccessViewUserComponent(
                userViewVO.getSid(), depViewVo.getSid(), compViewVo.getSid(), messageCallBack);
    }
}
