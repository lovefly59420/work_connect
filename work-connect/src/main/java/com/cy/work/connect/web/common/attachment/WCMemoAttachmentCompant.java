/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.attachment;

import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.web.logic.components.WCMemoAttachmentLogicComponent;
import java.util.List;

/**
 * @author brain0925_liao
 */
public class WCMemoAttachmentCompant extends AttachmentCompant {

    /**
     *
     */
    private static final long serialVersionUID = 5948614497193003593L;

    private final Integer userSid;

    private final Integer departmetSid;

    public WCMemoAttachmentCompant(Integer userSid, Integer departmetSid) {
        this.userSid = userSid;
        this.departmetSid = departmetSid;
    }

    @Override
    protected List<AttachmentVO> getPersistedAttachments(String wcMemoSid) {
        return WCMemoAttachmentLogicComponent.getInstance().getAttachmentsByWCMemoSid(wcMemoSid);
    }

    @Override
    public String getDirectoryName() {
        return "wcMemoMaster";
    }

    @Override
    protected AttachmentVO createAndPersistAttachment(
        String fileName, AttachmentCondition attachmentCondition) {
        if (attachmentCondition.isAutoMappingEntity()) {
            AttachmentVO result =
                WCMemoAttachmentLogicComponent.getInstance()
                    .createAttachment(
                        attachmentCondition.getEntity_sid(),
                        attachmentCondition.getEntity_no(),
                        fileName,
                        userSid,
                        departmetSid);
            if (attachmentCondition.getUploadAttCallBack() != null) {
                attachmentCondition.getUploadAttCallBack().doUploadAtt(result);
            }
            return result;
        } else {
            return WCMemoAttachmentLogicComponent.getInstance()
                .createAttachment("", "", fileName, userSid, departmetSid);
        }
    }

    @Override
    public Integer getFileSizeLimit() {
        return 50;
    }
}
