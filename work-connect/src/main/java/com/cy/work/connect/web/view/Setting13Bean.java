package com.cy.work.connect.web.view;

import com.cy.commons.vo.Org;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCSetGroupManager;
import com.cy.work.connect.logic.manager.WCSetPermissionManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.logic.vo.WCGroupVo;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.enums.TargetType;
import com.cy.work.connect.web.common.MultipleALLTagTreeManager;
import com.cy.work.connect.web.listener.DataTablePickListCallBack;
import com.cy.work.connect.web.listener.OrgUserTreeCallBack;
import com.cy.work.connect.web.logic.components.WCSetPermissionLogicComponent;
import com.cy.work.connect.web.util.pf.DataTablePickList;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.components.qstree.OrgUserTreeMBean;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 工作聯絡單 - 設定權限維護 MBean
 *
 * @author jimmy_chou
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class Setting13Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6659651005161761293L;
    /**
     * 組織人員選擇樹 已選擇清單判斷規則
     */
    private final OrgUserTreeCallBack orgUserTreeCallBack =
        new OrgUserTreeCallBack() {
            /** */
            private static final long serialVersionUID = -2589982220717181322L;

            @Override
            public void afterInitialize() {
                DisplayController.getInstance().update("userListPanel");
                //
                // DisplayController.getInstance().execute("permissionUserTree_outScriptObj.expandAllTreeNode('A')");
            }
        };

    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCConfigTempletManager configTempletManager;
    @Autowired
    private WCSetPermissionManager permissionManager;
    @Autowired
    private WCSetGroupManager groupManager;
    // 工作聯絡單
    @Getter
    private MultipleALLTagTreeManager allTagTreeManager;
    // 選擇的節點
    @Getter
    @Setter
    private TreeNode selectedNode;
    // Templet根節點
    @Getter
    private TreeNode templetTreeRoot;
    @Getter
    @Setter
    private boolean editable;
    @Getter
    private DataTablePickList<WCGroupVo> dataPickList;
    /**
     * 人員樹組件
     */
    @Getter
    private OrgUserTreeMBean permissionUserTree = new OrgUserTreeMBean();
    /**
     * 實做供元件呼叫取得已選清單的方法, 會將此份名單帶入右邊的【已選擇人員】清單
     */
    Function<Map<String, Object>, List<?>> getSelectedData =
        (condition) -> {
            return Lists.newArrayList(this.permissionUserTree.getSelectedDataList());
        };

    @Setter
    @Getter
    private Boolean noShowDisable = true;
    @Getter
    private Object categoryTag;
    @Getter
    private Object menuTag;
    @Getter
    private Object tag;
    @Getter
    private Object execDep;
    @Getter
    private Object templet;
    @Getter
    private boolean onoffCategoryTag = false;
    @Getter
    private boolean onoffMenuTag = false;
    @Getter
    private boolean onoffTag = false;
    @Getter
    private boolean onoffExecDep = false;
    @Getter
    private boolean onoffTemplet = false;
    private Integer compSid;
    private List<WCGroupVo> groupList;
    private WCSetPermission editWCPermission;
    private List<Integer> permissionUserSids;
    private final DataTablePickListCallBack dataTablePickListCallBack =
        new DataTablePickListCallBack() {
            /** */
            private static final long serialVersionUID = 1155447057807476472L;

            @Override
            public void changePickList() {
                try {
                    changeTarget(dataPickList.getTargetData());
                } catch (Exception e) {
                    log.warn("changePickList", e);
                    MessagesUtils.showError("更新試算權限名單錯誤，請洽系統人員! ");
                }
            }
        };

    @PostConstruct
    public void init() {
        this.permissionUserSids = Lists.newArrayList();
        this.compSid =
            Optional.ofNullable(this.orgManager.findById(SecurityFacade.getCompanyId()))
                .map(Org::getSid)
                .orElseGet(() -> null);
        initRootTreeTag();
        initRootTreeTemplet();
        this.dataPickList = new DataTablePickList<>(WCGroupVo.class);
        this.dataPickList.setDataTablePickListCallBack(dataTablePickListCallBack);
        // 設定群組清單
        this.groupList = groupManager.queryByCondition(null, null, compSid);
        // 試算權限名單
        this.permissionUserTree = new OrgUserTreeMBean();
    }

    public void selectFirstNode() {
        // 選中 工作聯絡單 第一個節點
        selectedNode = this.allTagTreeManager.getRootNode().getChildren().get(0);
        selectedNode.setSelected(true);
        readNode(selectedNode);
        this.switchOnOff(true, false, false, false, false);
        this.permissionUserTree.init(
            compSid,
            null,
            permissionUserSids,
            this.getSelectedData,
            Maps.newHashMap(),
            this.orgUserTreeCallBack);
    }

    /**
     * 初始root 工作聯絡單單據
     */
    private void initRootTreeTag() {
        this.allTagTreeManager = new MultipleALLTagTreeManager();
        this.allTagTreeManager.buildTagTree(this.compSid, true, false);
    }

    /**
     * 初始root 模版
     */
    private void initRootTreeTemplet() {

        templetTreeRoot = new DefaultTreeNode();

        List<WCConfigTempletVo> templetList =
            this.configTempletManager.queryByCondition(
                SecurityFacade.getUserSid(), null, null, this.compSid);

        for (WCConfigTempletVo templet : templetList) {
            new DefaultTreeNode("templet", templet, templetTreeRoot);
        }
    }

    private void switchOnOff(
        boolean categoryTag, boolean menuTag, boolean tag, boolean execDep, boolean templet) {
        this.onoffCategoryTag = categoryTag;
        this.onoffMenuTag = menuTag;
        this.onoffTag = tag;
        this.onoffExecDep = execDep;
        this.onoffTemplet = templet;
    }

    @SuppressWarnings("unchecked")
    private void readNode(TreeNode node) {
        editable = false;
        Object[] result = new Object[2];
        List<Long> nowPermissionList = Lists.newArrayList();
        if (node != null && node.getType() != null) {
            switch (node.getType()) {
                case "CategoryTag":
                    // 選中項目，顯示於明細中(階層顯示)
                    this.switchOnOff(true, false, false, false, false);
                    this.categoryTag = node.getData();

                    // 選中項目，顯示於明細中(群組顯示)
                    result =
                        WCSetPermissionLogicComponent.getInstance()
                            .getNowPermissionList(
                                this.editWCPermission,
                                this.categoryTag,
                                TargetType.CATEGORY_TAG,
                                this.compSid);
                    nowPermissionList = (List<Long>) result[0];
                    this.editWCPermission = (WCSetPermission) result[1];
                    loadDataToPickList(nowPermissionList);
                    break;
                case "MenuTag":
                    // 選中項目，顯示於明細中(階層顯示)
                    this.switchOnOff(true, true, false, false, false);
                    this.categoryTag = node.getParent().getData();
                    this.menuTag = node.getData();

                    // 選中項目，顯示於明細中(群組顯示)
                    result =
                        WCSetPermissionLogicComponent.getInstance()
                            .getNowPermissionList(
                                this.editWCPermission, this.menuTag, TargetType.MENU_TAG,
                                this.compSid);
                    nowPermissionList = (List<Long>) result[0];
                    this.editWCPermission = (WCSetPermission) result[1];
                    loadDataToPickList(nowPermissionList);
                    break;
                case "Tag":
                    // 選中項目，顯示於明細中(階層顯示)
                    this.switchOnOff(true, true, true, false, false);
                    this.categoryTag = node.getParent().getParent().getData();
                    this.menuTag = node.getParent().getData();
                    this.tag = node.getData();

                    // 選中項目，顯示於明細中(群組顯示)
                    result =
                        WCSetPermissionLogicComponent.getInstance()
                            .getNowPermissionList(
                                this.editWCPermission, this.tag, TargetType.TAG, this.compSid);
                    nowPermissionList = (List<Long>) result[0];
                    this.editWCPermission = (WCSetPermission) result[1];
                    loadDataToPickList(nowPermissionList);
                    break;
                case "ExecDep":
                    // 選中項目，顯示於明細中(階層顯示)
                    this.switchOnOff(true, true, true, true, false);
                    this.execDep = node.getData();
                    this.tag = node.getParent().getData();
                    this.menuTag = node.getParent().getParent().getData();
                    this.categoryTag = node.getParent().getParent().getParent().getData();

                    // 選中項目，顯示於明細中(群組顯示)
                    result =
                        WCSetPermissionLogicComponent.getInstance()
                            .getNowPermissionList(
                                this.editWCPermission, this.execDep, TargetType.EXEC_DEP,
                                this.compSid);
                    nowPermissionList = (List<Long>) result[0];
                    this.editWCPermission = (WCSetPermission) result[1];
                    loadDataToPickList(nowPermissionList);
                    break;
                case "templet":
                    // 選中項目，顯示於明細中(階層顯示)
                    this.switchOnOff(false, false, false, false, true);
                    this.templet = node.getData();

                    // 選中項目，顯示於明細中(群組顯示)
                    result =
                        WCSetPermissionLogicComponent.getInstance()
                            .getNowPermissionList(
                                this.editWCPermission, this.templet, TargetType.TAG, this.compSid);
                    nowPermissionList = (List<Long>) result[0];
                    this.editWCPermission = (WCSetPermission) result[1];
                    loadDataToPickList(nowPermissionList);
                    break;
                default:
                    MessagesUtils.showWarn("此類節點尚未定義，請洽系統人員! ");
                    break;
            }
        }
    }

    private void loadDataToPickList(List<Long> nowPermissionGroups) {
        if (nowPermissionGroups == null) {
            nowPermissionGroups = Lists.newArrayList();
        }
        List<Long> allGroupSids =
            groupList.stream().map(each -> each.getSid()).collect(Collectors.toList());

        allGroupSids.removeAll(nowPermissionGroups);

        final List<Long> source = allGroupSids;
        final List<Long> target = nowPermissionGroups;
        List<WCGroupVo> sourceList =
            groupList.stream()
                .filter(each -> source.contains(each.getSid()))
                .collect(Collectors.toList());
        List<WCGroupVo> targetList =
            groupList.stream()
                .filter(each -> target.contains(each.getSid()))
                .collect(Collectors.toList());

        dataPickList.setSourceData(sourceList);
        dataPickList.setTargetData(targetList);

        // 更新試算權限名單
        this.changeTarget(targetList);
    }

    /**
     * 更新試算權限名單
     */
    private void changeTarget(List<WCGroupVo> targetList) {
        this.permissionUserSids =
            targetList.stream()
                .map(each -> groupManager.getGroupUsers(each.getSid(), compSid))
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        this.permissionUserTree.init(
            compSid,
            null,
            permissionUserSids,
            this.getSelectedData,
            Maps.newHashMap(),
            this.orgUserTreeCallBack);
    }

    /**
     * 選取樹節點
     *
     * @param event
     */
    public void onNodeSelect(NodeSelectEvent event) {
        readNode(selectedNode);
    }

    /**
     * 開始編輯
     */
    public void prepareToEdit() {
        try {
            editable = true;
            this.orgUserTreeCallBack.afterInitialize();
        } catch (Exception ex) {
            log.error("prepareToEdit：" + ex.getMessage(), ex);
            MessagesUtils.showError("編輯失敗，請洽系統人員! " + ex.getMessage());
        }
    }

    /**
     * 執行更新
     */
    public void doUpdate() {
        try {
            List<Long> permissionList =
                this.dataPickList.getTargetData().stream()
                    .map(WCGroupVo::getSid)
                    .collect(Collectors.toList());
            this.editWCPermission.setPermissionGroups(permissionList);
            this.permissionManager.saveSetting(
                this.editWCPermission, SecurityFacade.getUserSid(), compSid);
            this.orgUserTreeCallBack.afterInitialize();
            MessagesUtils.showInfo("存檔成功");
        } catch (Exception ex) {
            log.error("doUpdate 存檔失敗：" + ex.getMessage(), ex);
            MessagesUtils.showError("存檔失敗，請洽系統人員! ");
        }
        editable = false;
    }

    /**
     * 執行取消
     */
    public void executeCancel() {
        readNode(selectedNode);
        editable = false;
    }
}
