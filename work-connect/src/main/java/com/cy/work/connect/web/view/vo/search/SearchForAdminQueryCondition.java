/**
 * 
 */
package com.cy.work.connect.web.view.vo.search;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class SearchForAdminQueryCondition implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3356365765330252205L;
    /**
     * 已選擇的單據類別
     */
    @Getter
    @Setter
    private Set<String> selectedItemSids = Sets.newHashSet();
    /**
     * 查詢條件：單據狀態
     */
    @Getter
    @Setter
    private List<String> wcStatus = Lists.newArrayList(
            WCStatus.values()).stream().map(WCStatus::name).collect(Collectors.toList());
    /**
     * 查詢條件：立案日-起日
     */
    @Getter
    @Setter
    private Date startDate = WkDateUtils.plusDays(new Date(), -365);
    /**
     * 查詢條件：立案日-迄日
     */
    @Getter
    @Setter
    private Date endDate = new Date();
    /**
     * 查詢條件：模糊查詢
     */
    @Getter
    @Setter
    private String searchKeyword = "";
    /**
     * 查詢條件：申請人
     */
    @Getter
    @Setter
    private String createUserName = "";

}
