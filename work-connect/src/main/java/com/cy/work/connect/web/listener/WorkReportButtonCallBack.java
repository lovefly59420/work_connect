/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import java.io.Serializable;

/**
 * 工作聯絡單按鈕監聽CallBack
 *
 * @author brain0925_liao
 */
public class WorkReportButtonCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7173611984470956594L;

    public void clickEdit() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickReply() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickFinishReply() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickCorrectReply() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickDirective() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickTag() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickSummit() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickDoSummit() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickSend() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickCancel() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickAtt() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickPrevious() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickNext() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickBackList() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickInvalid() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickExecRollBack() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickExecStop() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickExecDepRollBack() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickExecDepStop() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickTrace() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void clickClose() {
    }

    public void clickFavorite() {
    }

    public void clickReqCounterSign() {
    }

    public void clickExecCounterSign() {
    }

    public void clickCustomerExceDep() {
    }

    public void clickComments() {
    }

    public void clickCommentsContinueSign() {
    }

    public void clickCommentsCheckSign() {
    }

    public void clickCommentsCheckContinueSign() {
    }
}
