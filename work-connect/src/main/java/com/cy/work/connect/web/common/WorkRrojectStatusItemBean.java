/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.work.connect.web.common.setting.WCStatusSetting;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.SelectItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author brain0925_liao
 */
@Controller
@Scope("request")
public class WorkRrojectStatusItemBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1571187926983764804L;

    @Autowired
    private WCStatusSetting wcStatusSetting;

    public List<SelectItem> getWorkSearchStatusItems() {
        return wcStatusSetting.getWorkSearchItems();
    }

    public List<SelectItem> getAllWorkSearchStatusItems() {
        return wcStatusSetting.getAllWorkSearchItems();
    }
}
