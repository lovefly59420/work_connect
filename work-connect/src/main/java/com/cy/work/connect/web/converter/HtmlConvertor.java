package com.cy.work.connect.web.converter;

import com.cy.work.common.utils.WkJsoupUtils;
import com.google.common.base.Strings;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.primefaces.component.editor.Editor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

/**
 * display Escape character to
 * <p>
 * <p:editor>
 *
 * @author aken_kao
 */
@Component("htmlConverter")
public class HtmlConvertor implements Converter {

    @Autowired
    private transient WkJsoupUtils jsoupUtils;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }
        if (component instanceof Editor && value instanceof String) {
            return jsoupUtils.removeRegexpByEditor(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (component instanceof Editor && value instanceof String) {
            return HtmlUtils.htmlEscape((String) value);
        }
        return null;
    }
}
