/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.DepTreePickerComponent;
import com.cy.work.connect.web.common.ExportExcelComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.common.MultipleDepTreePickerManager;
import com.cy.work.connect.web.common.MultipleTagTreeManager;
import com.cy.work.connect.web.logic.components.CheckAuthorityComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCTagLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.CheckAuthorityVo;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.search.query.CheckAuthoritySearchQuery;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.component.api.UIColumn;
import org.primefaces.component.datatable.DataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

/**
 * 核決權限表 MBean
 *
 * @author brain0925_liao
 */
@Controller
@Scope("view")
@Slf4j
@ManagedBean
public class CheckAuthoritySearchBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3376227852564237696L;
    @Getter
    private final Boolean isManagerFlag = Boolean.FALSE;
    @Getter
    private String errorMessage;
    @Getter
    private UserViewVO userViewVO;
    @Getter
    private OrgViewVo depViewVo;
    @Getter
    private OrgViewVo compViewVo;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private CheckAuthorityComponent checkAuthorityComponent;
    @Autowired
    private ExportExcelComponent exportExcelComponent;
    @Autowired
    private WCTagLogicComponents wCTagLogicComponents;
    @Autowired
    private WCMenuTagLogicComponent wCMenuTagLogicComponent;
    /**
     * 查詢物件
     */
    @Getter
    private CheckAuthoritySearchQuery query;

    /**
     * 類別List
     */
    @Getter
    private List<SelectItem> categoryTagList;

    /**
     * 單據名稱List
     */
    @Getter
    private List<SelectItem> menuTagList;

    /**
     * 執行項目List
     */
    @Getter
    private List<SelectItem> subTagList;

    /**
     * 簽核層級List
     */
    @Getter
    private List<SelectItem> flowTypeList;

    /**
     * 匯出使用
     */
    @Getter
    private boolean hasDisplay = true;

    /**
     * 匯出報表名稱
     */
    @Getter
    private String excelFileName = "";

    private List<Org> execOrgsPicker;
    private List<User> execUsersPicker;

    private List<Org> assignOrgsPicker;
    private List<User> assignUsersPicker;

    private List<Org> allOrgs;

    private MultipleTagTreeManager multipleTagTreeManager;

    private DepTreeComponent viewDepTreeComponent;

    private DepTreeComponent canUseDepTreeComponent;

    private DepTreeComponent execDepTreeComponent;

    private DepTreeComponent relateDepTreeComponent;

    private DepTreePickerComponent execDepTreePickerComponent;

    private DepTreePickerComponent assignDepTreePickerComponent;

    private List<Org> selectViewOrgs;

    @Getter
    private List<CheckAuthorityVo> checkAuthorityList;
    /* 可閱人員列表 */
    @Getter
    private List<UserViewVO> viewUsers;
    /* 可使用人員列表 */
    @Getter
    private List<UserViewVO> useUsers;
    /* 可閱部門 */
    private DepTreeComponent queryViewDepTreeComponent;
    /* 指派部門 */
    private DepTreeComponent queryTransDepTreeComponent;
    /* 可使用人員 */
    private DepTreePickerComponent queryUseUserTreePickerComponent;
    /* 可使用人員-挑選部門 */
    private List<Org> queryUseUserOrgs;
    /* 可閱人員 */
    private DepTreePickerComponent queryViewUserTreePickerComponent;
    /* 可閱人員-挑選部門 */
    private List<Org> queryVieUserOrgs;

    /**
     * 自訂欄位
     */
    @Getter
    @Setter
    private LinkedHashMap<String, Boolean> columnFields;

    @PostConstruct
    public void init() {        
        userViewVO = new UserViewVO(SecurityFacade.getUserSid());
        depViewVo = new OrgViewVo(SecurityFacade.getPrimaryOrgSid());
        compViewVo = new OrgViewVo(SecurityFacade.getCompanySid());
        
        query = new CheckAuthoritySearchQuery();
        this.query.init();
        this.initItmes();
        this.initOrgData();
        this.columnFields = Maps.newLinkedHashMap();
    }

    public void doSearch() {
        try {
            List<CheckAuthorityVo> allChecks = Lists.newArrayList();
            allChecks = checkAuthorityComponent.getAllCheckAuthority(
                    compViewVo.getSid(),
                    depViewVo.getSid(),
                    userViewVO.getSid());
            allChecks = checkAuthorityComponent.permissionCheckAuthority(
                    allChecks, userViewVO.getSid(), depViewVo.getSid());
            this.checkAuthorityList = checkAuthorityComponent.queryCheckAuthority(allChecks, query);
        } catch (Exception e) {
            log.warn("doSearch ERROR", e);
        }
    }

    /**
     * 匯出excel
     *
     * @param document
     */
    public void exportExcel(Object document) {
        try {
            exportExcelComponent.exportExcelForCheckAuthoritySearch(document, columnFields);
        } catch (Exception e) {
            log.warn("exportExcel", e);
        } finally {
            hasDisplay = true;
        }
    }

    /**
     * 執行匯出前置
     */
    public void hideColumnContent() {
        Date date = new Date();
        String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        excelFileName = compViewVo.getName() + "-核決權限表-" + modifiedDate;
        // excelFileName = UrlEscapers.urlFragmentEscaper().escape(excelFileName);
        hasDisplay = false;
    }

    public void doClearData() {
        query.clear();
        this.execOrgsPicker = Lists.newArrayList();
        this.execUsersPicker = Lists.newArrayList();
        this.assignOrgsPicker = Lists.newArrayList();
        this.assignUsersPicker = Lists.newArrayList();
        this.queryVieUserOrgs = Lists.newArrayList();
        menuTagList = Lists.newArrayList();
        subTagList = Lists.newArrayList();
        queryUseUserOrgs = Lists.newArrayList();
        this.checkAuthorityList = Lists.newArrayList();
    }

    private void initItmes() {
        this.categoryTagList = checkAuthorityComponent.initCategoryTagList(depViewVo.getSid(), compViewVo.getSid());
        this.flowTypeList = checkAuthorityComponent.initFlowTypeList();
        this.loadTags();
        viewUsers = Lists.newArrayList();
        useUsers = Lists.newArrayList();
    }

    private void initOrgData() {
        allOrgs = Lists.newArrayList();
        allOrgs.addAll(orgManager.findAllDepByCompSid(compViewVo.getSid()));
        viewDepTreeComponent = new DepTreeComponent();
        canUseDepTreeComponent = new DepTreeComponent();
        execDepTreeComponent = new DepTreeComponent();
        relateDepTreeComponent = new DepTreeComponent();
        execDepTreePickerComponent = new DepTreePickerComponent();
        assignDepTreePickerComponent = new DepTreePickerComponent();
        queryUseUserTreePickerComponent = new DepTreePickerComponent();
        queryViewUserTreePickerComponent = new DepTreePickerComponent();
        queryViewDepTreeComponent = new DepTreeComponent();
        queryTransDepTreeComponent = new DepTreeComponent();
        intOrgTree();
    }

    private void intOrgTree() {
        selectViewOrgs = Lists.newArrayList();
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        Org group = new Org(compViewVo.getSid());

        viewDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        canUseDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        execDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        relateDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        execDepTreePickerComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        assignDepTreePickerComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectViewOrgs,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        queryViewDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        queryTransDepTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        queryUseUserTreePickerComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                Lists.newArrayList(),
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);

        queryViewUserTreePickerComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                Lists.newArrayList(),
                Lists.newArrayList(),
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
    }

    /**
     * 載入可使用人員
     *
     * @param wCMenuTagSid 單據名稱Sid
     */
    public void loadUseUsers(String wCMenuTagSid) {
        try {
            Preconditions.checkState(!Strings.isNullOrEmpty(wCMenuTagSid), "無取得單據名稱Sid，請確認！！");
            this.useUsers.clear();
            this.useUsers.addAll(wCMenuTagLogicComponent.getUseUsersByMenuTagSid(wCMenuTagSid));
            DisplayController.getInstance().showPfWidgetVar("useUserDilaogVar");
        } catch (Exception e) {
            log.warn("loadUseUsers ERROR", e);
        }
    }

    /**
     * 載入預設可閱人員
     *
     * @param wcTagSid 執行項目Sid
     */
    public void loadViewUsers(String wcTagSid) {
        try {
            Preconditions.checkState(!Strings.isNullOrEmpty(wcTagSid), "無取得執行項目Sid，請確認！！");
            this.viewUsers.clear();
            this.viewUsers.addAll(wCTagLogicComponents.getViewUsersByTagSid(wcTagSid));
            DisplayController.getInstance().showPfWidgetVar("viewUserDilaogVar");
        } catch (Exception e) {
            log.warn("loadViewUsers ERROR", e);
        }
    }

    public void loadViewOrgs(String orgs) {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });

            if (!StringUtils.isEmpty(orgs)) {
                this.selectViewOrgs = getSelectViewOrgs(orgs);
            }
            // List<Org> reOrg = getSelectViewOrgs(orgs);
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            viewDepTreeComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    this.selectViewOrgs,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadViewOrgs ERROR", e);
        }
    }

    /* 載入 可閱部門 組織樹 */
    public void loadQueryViewDep() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });
            List<Org> selDeps = Lists.newArrayList();
            this.query
                    .getViewOrgs()
                    .forEach(
                            item -> {
                                selDeps.add(item);
                            });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            queryViewDepTreeComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    selDeps,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadQueryViewDep ERROR", e);
        }
    }

    /* 載入 指派部門 組織樹 */
    public void loadQueryTransDep() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });
            List<Org> selDeps = Lists.newArrayList();
            this.query
                    .getTransOrgs()
                    .forEach(
                            item -> {
                                selDeps.add(item);
                            });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            queryTransDepTreeComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    selDeps,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadQueryTransDep ERROR", e);
        }
    }

    public List<Org> getSelectViewOrgs(String selectOrgs) {
        List<Org> allOrgs = orgManager.findAll();
        allOrgs = allOrgs.stream().filter(each -> each.getName() != null)
                .collect(Collectors.toList());
        List<Org> resultOrgs = Lists.newArrayList();
        resultOrgs = allOrgs.stream()
                .filter(each -> selectOrgs.contains(each.getName()))
                .collect(Collectors.toList());
        return resultOrgs;
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getViewDepTreeManager() { return this.viewDepTreeComponent.getMultipleDepTreeManager(); }

    /* 取得 可閱部門 組織樹 */
    public MultipleDepTreeManager getQueryViewDepTreeManager() { return this.queryViewDepTreeComponent.getMultipleDepTreeManager(); }

    /* 取得 指派單位 組織樹 */
    public MultipleDepTreeManager getQueryTransDepTreeManager() { return this.queryTransDepTreeComponent.getMultipleDepTreeManager(); }

    public void loadCanUseOrgs() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            canUseDepTreeComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    query.getCanUseOrg(),
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadCanUseOrgs ERROR", e);
        }
    }

    public void loadTags() {
        multipleTagTreeManager = new MultipleTagTreeManager();
        multipleTagTreeManager.initTreeAllComp(compViewVo.getSid(), this.query.getSelNode());
    }

    /**
     * 取得 多選 類別、單據、執行項目名稱 Tag樹
     *
     * @return MultipleTagTreeManager Tag樹Manager
     */
    public MultipleTagTreeManager getTagTreeManager() { return this.multipleTagTreeManager; }

    /**
     * 取得 可使用單位 組織樹
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getCanUseDepTreeManager() { return this.canUseDepTreeComponent.getMultipleDepTreeManager(); }

    public void loadExecOrgs() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            execDepTreeComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    query.getExecOrg(),
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadExecOrgs ERROR", e);
        }
    }

    /**
     * 取得 執行單位 組織樹
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getExecDepTreeManager() { return this.execDepTreeComponent.getMultipleDepTreeManager(); }

    public void loadRelatedOrgs() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            relateDepTreeComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    query.getRelatedOrg(),
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadRelatedOrgs ERROR", e);
        }
    }

    /**
     * 取得 相關單位 組織樹
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getRelateDepTreeManager() { return this.relateDepTreeComponent.getMultipleDepTreeManager(); }

    /* 載入 可使用人員 元件 */
    public void loadUseUserOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });
            if (this.queryUseUserOrgs == null) {
                this.queryUseUserOrgs = Lists.newArrayList();
            }
            List<User> execUsers = Lists.newArrayList();
            this.query
                    .getUseUsers()
                    .forEach(
                            each -> {
                                execUsers.add(each);
                            });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            this.queryUseUserTreePickerComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    this.queryUseUserOrgs,
                    execUsers,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadUseUserOrgsAndPickerUsers ERROR", e);
        }
    }

    /* 載入 可閱人員 元件 */
    public void loadQueryViewUserOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });
            if (this.queryVieUserOrgs == null) {
                this.queryVieUserOrgs = Lists.newArrayList();
            }
            List<User> users = Lists.newArrayList();
            this.query
                    .getViewUsers()
                    .forEach(
                            each -> {
                                users.add(each);
                            });
            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;
            Org group = new Org(compViewVo.getSid());
            this.queryViewUserTreePickerComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    this.queryVieUserOrgs,
                    users,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadQueryViewUserOrgsAndPickerUsers ERROR", e);
        }
    }

    public void loadExecOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });

            if (this.execOrgsPicker == null) {
                this.execOrgsPicker = Lists.newArrayList();
            }

            if (this.execUsersPicker == null) {
                this.execUsersPicker = Lists.newArrayList();
            }

            List<User> execUsers = Lists.newArrayList();
            execUsersPicker.forEach(
                    each -> {
                        execUsers.add(each);
                    });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;

            Org group = new Org(compViewVo.getSid());
            execDepTreePickerComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    this.execOrgsPicker,
                    execUsers,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadExecOrgsAndPickerUsers ERROR", e);
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getExecDepTreePickerManager() { return this.execDepTreePickerComponent.getMultipleDepTreePickerManager(); }

    /**
     * 取得挑選可使用人員元件
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getUseUserDepTreePickerManager() { return this.queryUseUserTreePickerComponent.getMultipleDepTreePickerManager(); }

    /**
     * 取得挑選可閱人員元件
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getQueryViewUserDepTreePickerManager() { return this.queryViewUserTreePickerComponent.getMultipleDepTreePickerManager(); }

    public void loadAssignOrgsAndPickerUsers() {
        try {
            List<Org> tempOrg = Lists.newArrayList();
            allOrgs.forEach(
                    item -> {
                        tempOrg.add(item);
                    });

            if (this.assignOrgsPicker == null) {
                this.assignOrgsPicker = Lists.newArrayList();
            }

            if (this.assignUsersPicker == null) {
                this.assignUsersPicker = Lists.newArrayList();
            }

            List<User> assignUsers = Lists.newArrayList();
            assignUsersPicker.forEach(
                    each -> {
                        assignUsers.add(each);
                    });

            boolean clearSelNodeWhenChaneDepTree = true;
            boolean selectable = true;
            boolean selectModeSingle = false;

            Org group = new Org(compViewVo.getSid());
            assignDepTreePickerComponent.init(
                    tempOrg,
                    tempOrg,
                    group,
                    this.assignOrgsPicker,
                    assignUsers,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        } catch (Exception e) {
            log.warn("loadAssignOrgsAndPickerUsers ERROR", e);
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreePickerManager 組織樹Manager
     */
    public MultipleDepTreePickerManager getAssignDepTreePickerManager() { return this.assignDepTreePickerComponent.getMultipleDepTreePickerManager(); }

    public boolean showOrgLook(String allOrgs) {
        // 此為前端merge使用,空字串並不會merge
        return !StringUtils.isEmpty(allOrgs) && !"全單位".equals(allOrgs) && allOrgs.contains("\n");
    }

    public void onSelectCategoryTagName(String categoryTagId) {
        menuTagList = checkAuthorityComponent.initMenuTagList(categoryTagId);
        log.info(categoryTagId);
        DisplayController.getInstance().update("menu_tag_id");
    }

    public void onSelectMenuTagName(String menuTagId) {
        this.subTagList = this.checkAuthorityComponent.initSubTagList(
                menuTagId, 
                this.compViewVo.getSid(),
                this.userViewVO.getSid());
        DisplayController.getInstance().update("sub_tag_id");
    }

    public void saveTag() {
        try {
            query.setSelNode(
                    multipleTagTreeManager.getSelNode() == null
                            ? Lists.newArrayList()
                            : Arrays.asList(multipleTagTreeManager.getSelNode()));
            DisplayController.getInstance().hidePfWidgetVar("tagUnitDlg");
        } catch (Exception e) {
            log.warn("saveTag ERROR", e);
        }
    }

    public void saveCanUse() {
        try {
            query.setCanUseOrg(canUseDepTreeComponent.getSelectOrg());
            DisplayController.getInstance().hidePfWidgetVar("canUseDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveCanUse ERROR", e);
        }
    }

    /**
     * 挑選 可閱單位
     */
    public void saveQueryViewOrg() {
        try {
            List<Org> queryViewSelOrgs = this.queryViewDepTreeComponent.getSelectOrg();
            query.getViewOrgs().clear();
            queryViewSelOrgs.forEach(
                    item -> {
                        query.getViewOrgs().add(item);
                    });
            DisplayController.getInstance().hidePfWidgetVar("queryViewDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveQueryViewOrg ERROR", e);
        }
    }

    /**
     * 挑選 指派單位
     */
    public void saveQueryTransOrg() {
        try {
            List<Org> queryTransSelOrgs = this.queryTransDepTreeComponent.getSelectOrg();
            query.getTransOrgs().clear();
            queryTransSelOrgs.forEach(
                    item -> {
                        query.getTransOrgs().add(item);
                    });
            DisplayController.getInstance().hidePfWidgetVar("queryTransDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveQueryViewOrg ERROR", e);
        }
    }

    /**
     * 挑選 執行單位
     */
    public void saveExecOrg() {
        try {
            query.setExecOrg(execDepTreeComponent.getSelectOrg());
            DisplayController.getInstance().hidePfWidgetVar("execDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveExecOrg ERROR", e);
        }
    }

    public void saveExecOrgPicker() {
        try {
            this.execOrgsPicker = execDepTreePickerComponent.getSelectOrg();
            this.execUsersPicker = execDepTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected();
            query.setExecSignUser(execUsersPicker);
            DisplayController.getInstance().hidePfWidgetVar("execDepPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveExecOrgPicker ERROR", e);
        }
    }

    /**
     * 挑選 相關單位
     */
    public void saveRelatedOrg() {
        try {
            query.setRelatedOrg(relateDepTreeComponent.getSelectOrg());
            DisplayController.getInstance().hidePfWidgetVar("relateDeptUnitDlg");
        } catch (Exception e) {
            log.warn("saveRelatedOrg ERROR", e);
        }
    }

    /* 選擇 可使用人員 */
    public void saveUseUserPicker() {
        try {
            this.queryUseUserOrgs = this.queryUseUserTreePickerComponent.getSelectOrg();
            this.query.getUseUsers().clear();
            if (queryUseUserTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected() != null) {
                queryUseUserTreePickerComponent
                        .getMultipleDepTreePickerManager()
                        .getAllSelected()
                        .forEach(
                                item -> {
                                    this.query.getUseUsers().add(item);
                                });
            }
            DisplayController.getInstance().hidePfWidgetVar("useUserPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveUseUserPicker ERROR", e);
        }
    }

    /* 選擇 可閱人員 */
    public void saveQueryViewUserPicker() {
        try {
            this.queryVieUserOrgs = this.queryViewUserTreePickerComponent.getSelectOrg();
            this.query.getViewUsers().clear();
            if (queryViewUserTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected() != null) {
                queryViewUserTreePickerComponent
                        .getMultipleDepTreePickerManager()
                        .getAllSelected()
                        .forEach(
                                item -> {
                                    this.query.getViewUsers().add(item);
                                });
            }
            DisplayController.getInstance().hidePfWidgetVar("queryViewUserPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveQueryViewUserPicker ERROR", e);
        }
    }

    public void saveAssignOrgPicker() {
        try {
            this.assignOrgsPicker = assignDepTreePickerComponent.getSelectOrg();
            this.assignUsersPicker = assignDepTreePickerComponent.getMultipleDepTreePickerManager().getAllSelected();
            query.setAssignUser(assignUsersPicker);
            DisplayController.getInstance().hidePfWidgetVar("assignDepPickerUnitDlg");
        } catch (Exception e) {
            log.warn("saveAssignOrgPicker ERROR", e);
        }
    }

    /**
     * 取得不包含各製化欄位名稱
     *
     * @return
     */
    private List<String> getExcludeHeaderText() {
        List<String> excludeHeaderText = Lists.newArrayList(null, "類別", "單據名稱", "可使用單位", "執行項目", "簽核層級", "執行單位", "執行單位簽核人員");

        return excludeHeaderText;
    }

    /**
     * 開啟個人化設定欄位
     *
     * @param tableId
     */
    public void btnDefineField(String tableId) {
        try {
            // allDefineField = Boolean.TRUE;
            // unAllDefineField = Boolean.TRUE;
            this.updateColumnFields(
                    tableId,
                    // userDefineFieldManager.fromJsonToFieldName(userCustom.getFieldName()),
                    this.getExcludeHeaderText());

            DisplayController.getInstance().showPfWidgetVar("dlgDefineField");
            DisplayController.getInstance().execute("initSelectAllCheckBox()");
        } catch (Exception e) {
            log.warn("btnDefineField Error", e);
        }
    }

    /**
     * 更新columnFields
     *
     * @param tableId
     * @param excludeHeaderText
     */
    private void updateColumnFields(String tableId, List<String> excludeHeaderText) {
        DataTable dataTable = (DataTable) DisplayController.getInstance().findComponent(tableId);
        List<UIColumn> columns = dataTable.getColumns();

        for (UIColumn column : columns) {

            // 欄位名稱
            String headerText = column.getHeaderText();

            // 是否勾選
            boolean ckField = true;

            // 上次勾選的欄位
            if (this.columnFields.containsKey(headerText)) {
                ckField = columnFields.get(headerText);
            }

            // 包含在排除名單的欄位, 不可編輯
            if ((excludeHeaderText.contains(headerText))) {
                continue;
            }

            columnFields.put(column.getHeaderText(), ckField);
        }
    }

    /**
     * dataTable顯示欄位判斷
     *
     * @param headerText
     */
    public boolean showDataTableColumn(String headerText) {

        // 還未設定資料時, 預設欄位全顯示 (除上方條件)
        if (this.columnFields == null) {
            return true;
        }

        // 比對欄位是否在設定顯示欄位內
        if (this.columnFields.containsKey(headerText)) {
            return columnFields.get(headerText);
        }

        return true;
    }

    /**
     * 依據欄位名稱，取得 style 屬性 (是否顯示)
     *
     * @param headerText 欄位名稱
     * @return
     */
    public String getStyleByDataTableColumn(String headerText) {
        if (showDataTableColumn(headerText)) {
            return "";
        }

        return "display:none;";
    }
}
