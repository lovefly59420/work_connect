/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.List;

import com.cy.work.connect.web.view.vo.SignRole;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;

/**
 * 工作聯絡單Data - 主題,內容,備註
 *
 * @author brain0925_liao
 */
public class WorkReportDataComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7146498850793656805L;
    /**
     * 主題
     */
    @Getter
    @Setter
    private String title;
    /**
     * 內容
     */
    @Getter
    @Setter
    private String content;

    /**
     * @param content the content to set
     */
//    public void setContent(String content) {
//        WkCommonUtils.logWithStackTrace(InfomationLevel.DEBUG, "content:[" + content + "]", 10);
//        this.content = content;
//    }

    @Getter
    @Setter
    private String labelcontent;
    /**
     * 備註
     */
    @Getter
    @Setter
    private String remark;

    @Getter
    @Setter
    private String labelRemark;
    /**
     * 會簽者
     */
    @Getter
    @Setter
    private String signUser;

    @Getter
    @Setter
    private List<SignRole> signRoleInfos = Lists.newArrayList();

    public String getDataWidth(boolean isEditMode, boolean isShow) {
        if (!isShow) {
            return "width: 0px;height: 0px;";
        }

        if (isEditMode) {
            return "width: 835px;";
        }
        return "width: 905px;";
    }
}
