/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import java.io.Serializable;

/**
 * 類別樹 元件 CallBack
 *
 * @author kasim
 */
public class CategoryTreeCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4244696974326867540L;

    public void showMessage(String message) {
    }

    public void onNodeSelect() {
    }
}
