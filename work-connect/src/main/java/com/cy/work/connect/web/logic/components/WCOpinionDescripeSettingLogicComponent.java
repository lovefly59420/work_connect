/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCOpinionDescripeSettingManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCOpinionDescripeSettingLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6037013650022691546L;

    private static WCOpinionDescripeSettingLogicComponent instance;
    /**
     * 意見說明Manager
     */
    @Autowired
    private WCOpinionDescripeSettingManager wcOpinionDescripeSettingManager;

    public static WCOpinionDescripeSettingLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCOpinionDescripeSettingLogicComponent.instance = this;
    }

    /**
     * 儲存意見說明
     *
     * @param managerSingInfoType    簽核類型-需求或者核准
     * @param singleManagerSignInfos 簽核物件Sid
     * @param opinion_usr            意見說明的User Sid
     * @param commont                意見說明
     * @param wcSid                  簽呈Sid
     */
    public void saveOpinionDescripe(
            ManagerSingInfoType managerSingInfoType,
            List<SingleManagerSignInfo> singleManagerSignInfos,
            Integer opinion_usr,
            String commont,
            String wcSid)
            throws UnsupportedEncodingException {
        singleManagerSignInfos.forEach(
                singleManagerSignInfo -> {
                    WCOpinionDescripeSetting opinionDescripeSetting = wcOpinionDescripeSettingManager.getOpinionDescripeSetting(
                            managerSingInfoType, singleManagerSignInfo.getSignInfoSid(), opinion_usr);
                    if (opinionDescripeSetting == null) {
                        opinionDescripeSetting = new WCOpinionDescripeSetting();
                        opinionDescripeSetting.setOpinion_usr(opinion_usr);
                        opinionDescripeSetting.setSourceType(managerSingInfoType);
                        opinionDescripeSetting.setSourceSid(singleManagerSignInfo.getSignInfoSid());
                        opinionDescripeSetting.setStatus(Activation.ACTIVE);
                    }
                    opinionDescripeSetting.setOpinion_descripe(commont);
                    if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
                        wcOpinionDescripeSettingManager.create(opinionDescripeSetting, opinion_usr,
                                wcSid);
                    } else {
                        wcOpinionDescripeSettingManager.update(opinionDescripeSetting, opinion_usr);
                    }
                });
    }

    /**
     * 取得意見說明
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @return
     */
    public String getOpinionDescripe(
            ManagerSingInfoType managerSingInfoType, String sourceSid, Integer opinion_usr) {

        WCOpinionDescripeSetting opinionDescripeSetting = wcOpinionDescripeSettingManager.getOpinionDescripeSetting(
                managerSingInfoType, sourceSid, opinion_usr);
        if (opinionDescripeSetting == null) {
            return "";
        }
        return (opinionDescripeSetting.getOpinion_descripe() == null)
                ? ""
                : opinionDescripeSetting.getOpinion_descripe();
    }

    /**
     * 取得作廢原因
     * @param managerSingInfoType
     * @param sourceSid
     * @param opinion_usr
     * @return
     */
    public String getInvaildReason(
            ManagerSingInfoType managerSingInfoType, String sourceSid, Integer opinion_usr) {

        WCOpinionDescripeSetting opinionDescripeSetting = wcOpinionDescripeSettingManager.getOpinionDescripeSetting(
                managerSingInfoType, sourceSid, opinion_usr);
        if (opinionDescripeSetting == null) {
            return "";
        }
        
        String invaildReason =  WkStringUtils.safeTrim(opinionDescripeSetting.getInvaildReason());
        if(WkStringUtils.isEmpty(invaildReason)) {
            
        }

        return WkHtmlUtils.addRedClass(invaildReason);
    }

    /**
     * 取得退回理由及作廢原因
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @param descripe            意見說明
     * @return
     */
    public String getFlowOpinionDescripe(
            ManagerSingInfoType managerSingInfoType,
            String sourceSid,
            Integer opinion_usr,
            String descripe) {
        WCOpinionDescripeSetting opinionDescripeSetting = wcOpinionDescripeSettingManager.getOpinionDescripeSetting(
                managerSingInfoType, sourceSid, opinion_usr);
        if (opinionDescripeSetting == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (!Strings.isNullOrEmpty(opinionDescripeSetting.getInvaildReason())) {
            if (!Strings.isNullOrEmpty(sb.toString()) || !Strings.isNullOrEmpty(descripe)) {
                sb.append("<br/>");
            }
            sb.append("<font color=\"red\">");
            sb.append(opinionDescripeSetting.getInvaildReason());
            sb.append("</font>");
        }
        if (!Strings.isNullOrEmpty(opinionDescripeSetting.getRollBackReason())) {
            if (!Strings.isNullOrEmpty(sb.toString()) || !Strings.isNullOrEmpty(descripe)) {
                sb.append("<br/>");
            }
            sb.append("<font color=\"red\">");
            sb.append(opinionDescripeSetting.getRollBackReason());
            sb.append("</font>");
        }
        return sb.toString();
    }
}
