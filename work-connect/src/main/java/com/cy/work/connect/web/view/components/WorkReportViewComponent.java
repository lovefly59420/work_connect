/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.omnifaces.util.Faces;
import org.primefaces.event.TabChangeEvent;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.PermissionLogicForCanUse;
import com.cy.work.connect.logic.helper.SignFlowForExecDepLogic;
import com.cy.work.connect.logic.helper.LogHelper;
import com.cy.work.connect.logic.manager.WCAlertManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCAlert;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.callable.CommentsComponentCallBack;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.common.attachment.AttachmentCondition;
import com.cy.work.connect.web.common.attachment.WPAttachmentCompant;
import com.cy.work.connect.web.listener.BpmtCallBack;
import com.cy.work.connect.web.listener.ExecReplyCallBack;
import com.cy.work.connect.web.listener.PreviousAndNextCallBack;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.listener.SignInfoMemberCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.listener.UploadAttCallBack;
import com.cy.work.connect.web.listener.WorkReportButtonCallBack;
import com.cy.work.connect.web.listener.WorkReportDataCallBack;
import com.cy.work.connect.web.logic.components.CanViewType;
import com.cy.work.connect.web.logic.components.ViewPermissionLogic;
import com.cy.work.connect.web.logic.components.WCExceDepLogicComponent;
import com.cy.work.connect.web.logic.components.WCExecManagerSignInfoLogicComponent;
import com.cy.work.connect.web.logic.components.WCFavoriteLogicComponents;
import com.cy.work.connect.web.logic.components.WCManagerSignInfoLogicComponent;
import com.cy.work.connect.web.logic.components.WCMasterLogicComponents;
import com.cy.work.connect.web.logic.components.WCOpinionDescripeSettingLogicComponent;
import com.cy.work.connect.web.logic.components.WCReadLogicComponents;
import com.cy.work.connect.web.logic.components.WCTraceLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.enumtype.DepartmentType;
import com.cy.work.connect.web.view.enumtype.TabType;
import com.cy.work.connect.web.view.vo.OrgViewVo;
import com.cy.work.connect.web.view.vo.SignInfoVO;
import com.cy.work.connect.web.view.vo.UserModifyVO;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WCAgainTraceVO;
import com.cy.work.connect.web.view.vo.WCTraceVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作聯絡單-維護元件
 *
 * @author brain0925_liao
 */
@Slf4j
public class WorkReportViewComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3464969403214049005L;
    /**
     * 工作聯絡單 執行回覆原件(舊)
     */
    @Getter
    private final ExecReplyComponent execReplyComponent;
    /**
     * 工作聯絡單 追蹤原件
     */
    @Getter
    private final WorkReportTraceComponent workReportTraceComponent;
    /**
     * 導向頁面 - 權限不足
     */
    private final String illegal_readPath = "../error/illegal_read.xhtml";
    /**
     * 導向頁面 - 無效的連結網址
     */
    private final String illegal_ulrPath = "../error/illegal_url.xhtml";
    @Getter
    /** 操作者 */
    private final UserViewVO userViewVO;
    @Getter
    /** 操作者單位 */
    private final OrgViewVo depViewVo;
    @Getter
    /** 操作者公司 */
    private final OrgViewVo compViewVo;
    /**
     * 工作聯絡單 上傳附件原件
     */
    @Getter
    private final WPAttachmentCompant wpAttachmentCompant;
    /**
     * 工作聯絡單 執行回覆原件(新)
     */
    @Getter
    private final WorkReportReplyComponent workReportReplyComponent;
    /**
     * 工作聯絡單 執行完成回覆(舊)
     */
    @Getter
    private final WorkReportFinishReplyComponent workReportFinishReplyComponent;
    /**
     * 工作聯絡單 執行完成回覆(舊)
     */
    @Getter
    private final WorkReportCorrectReplyComponent workReportCorrectReplyComponent;
    /**
     * 工作聯絡單 執行完成回覆(舊)
     */
    @Getter
    private final WorkReportDirectiveComponent workReportDirectiveComponent;
    /**
     * 工作聯絡單 附件維護原件
     */
    @Getter
    private final WorkReportAttachMaintainCompant workReportAttachMaintainCompant;
    /**
     * 工作聯絡單 閱讀記錄元件
     */
    @Getter
    private final WorkReportReadReceiptsMemberComponent workReportReadReceiptsMemberComponent;
    /**
     * 部門相關邏輯Component
     */
    @Getter
    private final DepTreeComponent depTreeComponent;
    /**
     * 上下筆按鈕 CallBack
     */
    private final PreviousAndNextCallBack previousAndNextCallBack;
    /**
     * 編輯倒數元件
     */
    @Getter
    private final LockCountDownComponent lockCountDownComponent;
    /**
     * 工作聯絡單 簽核資訊
     */
    @Getter
    private final BpmComponent bpmComponent;
    /**
     * 工作聯絡單 需求單位會簽元件
     */
    @Getter
    private final ManagerSignInfoMemberComponent managerSignInfoMemberComponent;
    /**
     * 工作聯絡單 執行單位會簽元件
     */
    @Getter
    private final ExecManagerSignInfoMemberComponent execManagerSignInfoMemberComponent;
    /**
     * 工作聯絡單 執行單位加派元件
     */
    @Getter
    private final WCExceDepComponent wcExceDepComponent;
    /**
     * 工作聯絡單 可閱人員元件
     */
    @Getter
    private final ViewPersonComponent viewPersonComponent;
    /**
     * 工作聯絡單 分派元件
     */
    @Getter
    private final ExecSendPersonComponent execSendPersonComponent;
    /**
     * 工作聯絡單 轉單元件
     */
    @Getter
    private final ExecTransPersonComponent execTransPersonComponent;
    /**
     * 工作聯絡單 確認完成元件
     */
    @Getter
    private final ExecFinishComponent execFinishComponent;
    /**
     * 意見說明物件
     */
    @Getter
    private final CommentsComponent commentsComponent;
    /**
     * 是否顯示上一筆按鈕
     */
    boolean showPreviousBtn = false;
    /**
     * 是否顯示下一筆按鈕
     */
    boolean showNextBtn = false;
    /**
     * 是否顯示回首頁按鈕
     */
    boolean showBackListBtn = false;
    /**
     * 工作聯絡單 內容原件
     */
    @Getter
    @Setter
    private WorkReportDataComponent workReportDataComponent;
    public final WorkReportDataCallBack workReportDataCallBack = new WorkReportDataCallBack() {

        @Override
        public String getWorkReportData() { return workReportDataComponent.getContent(); }

        @Override
        public void updateWorkReportData(String content, String memo) {
            workReportDataComponent.setContent(content);
            workReportDataComponent.setRemark(memo);
        }

        @Override
        public String getContent() { return workReportDataComponent.getContent(); }

        @Override
        public String getMemo() { return workReportDataComponent.getRemark(); }
    };
    /**
     * 工作聯絡單 狀態原件
     */
    @Getter
    @Setter
    private WorkReportHeaderComponent workReportHeaderComponent;
    /**
     * 工作聯絡單 類別原件
     */
    @Getter
    private CategoryItemComponent categoryItemComponent;
    /**
     * 工作聯絡單 執行單位-執行說明原件
     */
    @Getter
    private ExecDescComponent execDescComponent;
    /**
     * 工作聯絡單 按鈕原件
     */
    @Getter
    @Setter
    private WorkReportBtnComponent workReportBtnComponent;
    /**
     * 工作聯絡單 是否編輯
     */
    @Getter
    @Setter
    private Boolean edit = false;
    /**
     * 操作的聯絡單sid
     */
    private String masterSid;
    /**
     * 操作的聯絡單No
     */
    private String masterNo;
    /**
     * 工作聯絡單 回覆物件
     */
    @Getter
    private WCTraceVO replyEditVO;
    /**
     * 工作聯絡單 再次回覆物件
     */
    @Getter
    private WCAgainTraceVO againReplyEditVO;
    /**
     * 工作聯絡單 單據狀態是否已作廢,結案
     */
    @Getter
    private boolean wcStatusDeleteOrInvail = false;
    /**
     * 工作聯絡單 是否顯示下方Tab
     */
    @Getter
    private boolean showBottomInfoTab = false;
    /**
     * 工作聯絡單 下方Tab,指定特定Tab
     */
    @Getter
    @Setter
    private int tabIndex = 0;
    /**
     * 開啟部門Dialog Type
     */
    private DepartmentType departmentType;
    /**
     * Org tree 挑選部門
     */
    private List<Org> selectOrgs;
    /**
     * 最後修改者資訊物件
     */
    @Getter
    private UserModifyVO userModifyVO;
    /**
     * 工作聯絡單 下方Tab,指定特定Tab
     */
    private TabType defaultTabType = null;
    /**
     * 工作聯絡單 從首頁跳轉至此維護頁,導致指定資料
     */
    private String selTraceSid = "";
    /**
     * 意見說明使用 - 確認當下是申請方還是核准方 意見說明
     */
    private ManagerSingInfoType managerSingInfoType;
    /**
     * 意見說明使用 - 確認當下簽核節點物件 意見說明
     */
    private List<SingleManagerSignInfo> singleManagerSignInfos;

    @Getter
    private boolean adminMode = false;
    /**
     * 不不寫閱讀記錄
     */
    private boolean passReadRecord = false;

    public WorkReportViewComponent(
            UserViewVO userViewVO,
            OrgViewVo depViewVo,
            OrgViewVo compViewVo,
            PreviousAndNextCallBack previousAndNextCallBack) {
        this.userViewVO = userViewVO;
        this.depViewVo = depViewVo;
        this.compViewVo = compViewVo;
        this.previousAndNextCallBack = previousAndNextCallBack;
        workReportDataComponent = new WorkReportDataComponent();
        workReportHeaderComponent = new WorkReportHeaderComponent();
        this.categoryItemComponent = new CategoryItemComponent();
        this.execDescComponent = new ExecDescComponent();
        this.execReplyComponent = new ExecReplyComponent(depViewVo.getSid(), userViewVO.getSid(), execReplyCallBack);
        // 建立Bpm 簽核資訊元件
        this.bpmComponent = new BpmComponent(userViewVO.getSid(), bpmtCallBack);
        workReportBtnComponent = new WorkReportBtnComponent(
                this.bpmComponent, this.workReportButtonCallBack, this.userViewVO.getSid());
        workReportTraceComponent = new WorkReportTraceComponent();
        workReportReplyComponent = new WorkReportReplyComponent();
        workReportFinishReplyComponent = new WorkReportFinishReplyComponent();
        workReportCorrectReplyComponent = new WorkReportCorrectReplyComponent();
        workReportDirectiveComponent = new WorkReportDirectiveComponent();
        wpAttachmentCompant = new WPAttachmentCompant(userViewVO.getSid(), depViewVo.getSid());
        replyEditVO = new WCTraceVO(0, "", null, "", "", "", null, "", "", false,
                userViewVO.getSid());
        againReplyEditVO = new WCAgainTraceVO("", "", null, "", "", "", false);
        workReportAttachMaintainCompant = new WorkReportAttachMaintainCompant(
                userViewVO.getSid(), workReportTabCallBack);
        workReportReadReceiptsMemberComponent = new WorkReportReadReceiptsMemberComponent(userViewVO.getSid());
        managerSignInfoMemberComponent = new ManagerSignInfoMemberComponent(
                userViewVO.getSid(), workReportTabCallBack,
                signInfoMemberCallBack);
        execManagerSignInfoMemberComponent = new ExecManagerSignInfoMemberComponent(
                userViewVO.getSid(), workReportTabCallBack,
                signInfoMemberCallBack);
        depTreeComponent = new DepTreeComponent();
        Org group = new Org(compViewVo.getSid());
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(
                Lists.newArrayList(),
                Lists.newArrayList(),
                group,
                selectOrgs,
                clearSelNodeWhenChaneDepTree,
                selectable,
                selectModeSingle);
        this.lockCountDownComponent = new LockCountDownComponent();
        this.wcExceDepComponent = new WCExceDepComponent(userViewVO.getSid(), workReportTabCallBack);
        this.viewPersonComponent = new ViewPersonComponent(userViewVO.getSid(), workReportTabCallBack);
        this.execSendPersonComponent = new ExecSendPersonComponent(
                compViewVo.getSid(),
                userViewVO.getSid(),
                execSendPersonReLoadCallBack);
        this.execTransPersonComponent = new ExecTransPersonComponent(
                compViewVo.getSid(),
                userViewVO.getSid(),
                execTransPersonReLoadCallBack);
        this.execFinishComponent = new ExecFinishComponent(userViewVO.getSid(), execFinishReLoadCallBack);
        commentsComponent = new CommentsComponent(commentsComponentCallBack);
    }

    /**
     * 讀取單據
     *
     * @param wcSid
     * @param wrcType
     * @param detailSID
     */
    public void loadMaster(String wcSid, String wrcType, String detailSID) {
        if (!Strings.isNullOrEmpty(wcSid)) {
            this.masterSid = wcSid;
        }
        if (WkStringUtils.notEmpty(wrcType)) {
            this.focusTab(wrcType, detailSID);
        }

        WCMaster wcMaster = WCMasterManager.getInstance().findBySid(wcSid);
        if (wcMaster == null) {
            return;
        }

        // ===================================
        // 為作廢時，預設顯示作廢原因 (追蹤)
        // ===================================
        if (WCStatus.INVALID.equals(wcMaster.getWc_status())) {
            this.reloadTabView(TabType.Trace);
            return;
        }

        // ===================================
        // WORKCOMMU-517
        // 【聯絡單檢視】調整『執行中』單據，預設顯示頁簽為『執行資訊』
        // ====================================
        this.showExecInfo(wcMaster);
    }

    private void showExecInfo(WCMaster wcMaster) {

        // ====================================
        // 執行方待簽人員, 顯示簽核頁
        // ====================================
        boolean isExecDepFlowCanSignOrSignedUser = SignFlowForExecDepLogic.getInstance().isExecDepFlowCanSignOrSignedUser(
                wcMaster.getSid(), SecurityFacade.getUserSid());
        if (isExecDepFlowCanSignOrSignedUser) {
            return;
        }

        // ====================================
        // 為核准中時，以下狀況顯示執行資訊頁簽
        // 1.沒有執行方簽核
        // 2.沒有任何一筆執行方簽核已經簽名了
        // ====================================
        if (WCStatus.APPROVED.equals(wcMaster.getWc_status())) {

            List<WCExecManagerSignInfo> execManagerSignInfo = WCExecManagerSignInfoManager.getInstance().findExecFlowSignInfos(
                    wcMaster.getSid());

            // 沒有執行方簽核
            if (WkStringUtils.isEmpty(execManagerSignInfo)) {
                this.reloadTabView(TabType.ExecInfo);
                return;
            }

            // 沒有任何一筆執行方簽核已經簽名了
            if (execManagerSignInfo.stream().allMatch(signInfo -> BpmStatus.APPROVING.equals(signInfo.getStatus()))) {
                this.reloadTabView(TabType.ExecInfo);
                return;
            }
        }

        // ====================================
        // 執行完成、結案，預設顯示執行資訊
        // ====================================
        Set<WCStatus> status = Sets.newHashSet(
                WCStatus.EXEC,
                WCStatus.EXEC_FINISH,
                WCStatus.CLOSE);

        if (status.contains(wcMaster.getWc_status())) {
            this.reloadTabView(TabType.ExecInfo);
            return;
        }

    }

    /**
     * 指定特定頁籤資料
     *
     * @param wrcType   focus 類型
     * @param detailSID 依據wrcType判斷是否需要使用detailSID
     */
    private void focusTab(String wrcType, String detailSID) {
        if (!Strings.isNullOrEmpty(wrcType)) {
            switch (wrcType) {
            case "news":
                if (!Strings.isNullOrEmpty(detailSID)) {
                    defaultTabType = this.focusTabByNews(detailSID);
                }
                break;
            case "BPMReq":
                defaultTabType = TabType.BpmFlow;
                break;
            default:
                break;
            }
        }
    }

    /**
     * @param sid
     * @param showPreviousBtn
     * @param showNextBtn
     * @param showBackListBtn
     */
    public void initialize(
            String sid, boolean showPreviousBtn, boolean showNextBtn, boolean showBackListBtn) {
        if (!Strings.isNullOrEmpty(sid)) {
            this.masterSid = sid;
        } else {
            log.warn("無取得聯絡單sid");
            DisplayController.getInstance().showPfWidgetVar("dlgIdleSession");
            return;
        }
        this.showPreviousBtn = showPreviousBtn;
        this.showNextBtn = showNextBtn;
        this.showBackListBtn = showBackListBtn;

        WCMaster master = WCMasterLogicComponents.getInstance().getWCMasterBySid(this.masterSid);
        if (master == null) {
            log.warn("無取得工作聯絡單主檔,故導致錯誤連結");
            this.reloadToIllegalPage(illegal_ulrPath);
            return;
        }
        boolean checkPermission = this.checkViewWorkReportPermission(master);
        if (!checkPermission) {
            this.reloadToIllegalPage(illegal_readPath);
            return;
        }
        // 寫閱讀記錄 (admin 閱讀模式時，不寫閱讀記錄)
        if (!this.passReadRecord) {
            WCReadLogicComponents.getInstance().readWCMaster(masterSid, userViewVO.getSid());
        }

        WCAlertManager.getInstance().readAlerts(masterSid, userViewVO.getSid());
        boolean reuslt = this.initLoadView();
        if (!reuslt) {
            return;
        }
        tabIndex = 0;
        workReportTraceComponent.loadData(masterSid, userViewVO.getSid());
        workReportReplyComponent.loadData(masterSid, userViewVO.getSid());
        workReportFinishReplyComponent.loadData(masterSid, userViewVO.getSid());
        workReportCorrectReplyComponent.loadData(masterSid, userViewVO.getSid());
        workReportDirectiveComponent.loadData(masterSid, userViewVO.getSid());
        execReplyComponent.loadData(masterSid);
        wcExceDepComponent.loadData(masterSid);
        AttachmentCondition ac = new AttachmentCondition(masterSid, masterNo, false);
        wpAttachmentCompant.loadAttachment(ac);
        workReportAttachMaintainCompant.loadData(masterSid, masterNo);
        workReportReadReceiptsMemberComponent.loadData(masterSid, master.getCreate_usr());
        managerSignInfoMemberComponent.loadWCID(masterSid, masterNo);
        execManagerSignInfoMemberComponent.loadWCID(masterSid, masterNo);
        viewPersonComponent.loadViewPersonData(masterSid);
        workReportTabCallBack.reloadTabView(defaultTabType);

    }

    private void reloadTabView(TabType tabType) {
        defaultTabType = tabType;
        workReportTabCallBack.reloadTabView(defaultTabType);
    }

    /**
     * 錯誤導向頁面
     *
     * @param path
     */
    private void reloadToIllegalPage(String path) {
        try {
            Faces.getExternalContext().redirect(path);
            Faces.getContext().responseComplete();
        } catch (Exception ex) {
            log.warn("導向讀取失敗頁面失敗..." + ex.getMessage());
        }
    }

    /**
     * 權限判斷
     *
     * @param master
     * @return
     */
    public boolean checkViewWorkReportPermission(WCMaster master) {

        this.adminMode = false;
        this.passReadRecord = false;

        CanViewType canViewType = ViewPermissionLogic.getInstance()
                .checkWorkConnectPermission(master);

        if (CanViewType.ALL.equals(canViewType)) {
            return true;
        } else if (CanViewType.ADMIN_SPECIAL_MASK_MODE.equals(canViewType)) {
            // 遮罩
            this.adminMode = true;
            // 不寫可閱
            this.passReadRecord = true;
            return true;
        } else if (CanViewType.ADMIN_SPECIAL_ALL_VIEW_MODE.equals(canViewType)) {
            // 不遮罩
            // 不寫可閱
            this.passReadRecord = true;
            return true;
        }

        log.warn(
                "工作聯絡單單號:["
                        + master.getWc_no()
                        + "]"
                        + "登入者部門:["
                        + depViewVo.getName()
                        + "]"
                        + "登入者:["
                        + userViewVO.getName()
                        + "] 無權限觀看此工作聯絡單");
        return false;
    }

    /**
     * 讀取單據
     */
    private boolean initLoadView() {
        try {
            workReportBtnComponent.setEditMode(false);
            this.loadData(masterSid);
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("initLoadView：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("initLoadView", e);
        }
        this.edit = false;
        DisplayController.getInstance().update("workReportBtnPanel");
        DisplayController.getInstance().update("workReportHeaderPanel");
        DisplayController.getInstance().update("legitimate_range_panel_id");
        DisplayController.getInstance().update("start_end_panel_id");
        DisplayController.getInstance().update("category_items_panel_id");
        DisplayController.getInstance().update("workReportDataPanel");
        DisplayController.getInstance().update("userModifyPanel");
        return true;
    }

    /**
     * 讀取單據
     *
     * @param sid
     * @return
     */
    private WorkReportComponent loadData(String sid) {
        WorkReportComponent result = WCMasterLogicComponents.getInstance()
                .getWorkReportComponentBySid(
                        sid,
                        this.userViewVO.getSid(),
                        this.compViewVo.getSid(),
                        workReportBtnComponent.getEditMode());
        if (result == null || result.getWcMaster() == null) {
            return null;
        }
        // 若是編輯模式,檢測參數及編輯問題
        if (workReportBtnComponent.getEditMode()) {
            settingEditMode(result.getWcMaster());
        }
        wcStatusDeleteOrInvail = result.getWcMaster().getWc_status().equals(WCStatus.INVALID)
                || result.getWcMaster().getWc_status().equals(WCStatus.CLOSE)
                || result.getWcMaster().getWc_status().equals(WCStatus.CLOSE_STOP);
        workReportDataComponent = result.getWorkReportDataComponent();
        workReportHeaderComponent = result.getWorkReportHeaderComponent();
        this.categoryItemComponent = result.getCategoryItemComponent();
        this.categoryItemComponent.setWorkReportDataCallBack(workReportDataCallBack);
        this.execDescComponent = result.getExecDescComponent();
        // 取得BPM簽核資料
        bpmComponent.loadData(result.getWcMaster());

        workReportBtnComponent.loadButtonAble(
                result.getWcMaster(), showPreviousBtn, showNextBtn, showBackListBtn);
        masterNo = result.getWcMaster().getWc_no();
        try {
            UserViewVO modifyUser = new UserViewVO(
                    (result.getWcMaster().getUpdate_usr() != null)
                            ? result.getWcMaster().getUpdate_usr()
                            : result.getWcMaster().getCreate_usr());
            String modifyTimeStr = ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                    result.getWcMaster().getUpdate_dt());
            userModifyVO = new UserModifyVO(modifyUser.getName(), modifyTimeStr);
        } catch (Exception e) {
            log.warn("loadData Error", e);
        }
        return result;
    }

    private void loadTabIndex(TabType tabType) {
        if (tabType == null) {
            return;
        }
        int bpmIndex = (bpmComponent.getBpmVo().isShowTab()) ? 0 : -1;
        int attachIndex = bpmIndex + (workReportAttachMaintainCompant.isShowAttTab() ? 1 : 0);
        int readReceiptIndex = attachIndex + (workReportReadReceiptsMemberComponent.isShowReadReceiptTab() ? 1 : 0);
        int execReplyIndex = readReceiptIndex + ((execReplyComponent.isShowTab()) ? 1 : 0);
        int traceIndex = execReplyIndex + ((workReportTraceComponent.isShowTraceTab()) ? 1 : 0);
        int execInfoIndex = traceIndex + ((workReportHeaderComponent.isShowExecStatus()) ? 1 : 0);
        int viewPersonIndex = execInfoIndex + ((viewPersonComponent.isShowViewPersonTab()) ? 1 : 0);

        switch (tabType) {
        case BpmFlow:
            tabIndex = bpmIndex;
            break;
        case Trace:
            tabIndex = traceIndex;
            break;
        case Attachment:
            tabIndex = attachIndex;
            break;
        case ReadReceipt:
            tabIndex = readReceiptIndex;
            break;
        case ExecReply:
            tabIndex = execReplyIndex;
            break;
        case ExecInfo:
            tabIndex = execInfoIndex;
            break;
        case ViewPerson:
            tabIndex = viewPersonIndex;
            break;
        default:
            tabIndex = 0;
            break;
        }
        if (tabIndex < 0) {
            tabIndex = 0;
        }
    }

    public void loadDepTree() {
        Org group = new Org(compViewVo.getSid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        if (DepartmentType.ExecManagerConunterSignInfo.equals(departmentType)) {
            try {
                List<Org> allOrg = WkOrgCache.getInstance().findAllDepByCompSid(group.getSid());
                depTreeComponent.init(
                        allOrg,
                        allOrg,
                        group,
                        selectOrgs,
                        clearSelNodeWhenChaneDepTree,
                        selectable,
                        selectModeSingle);
            } catch (Exception e) {
                log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
            }

        } else {
            List<Org> allOrg = WkOrgCache.getInstance().findAllDepByCompSid(group.getSid());
            depTreeComponent.init(
                    allOrg,
                    allOrg,
                    group,
                    selectOrgs,
                    clearSelNodeWhenChaneDepTree,
                    selectable,
                    selectModeSingle);
        }
        DisplayController.getInstance().update("dep");
        DisplayController.getInstance().showPfWidgetVar("deptUnitDlg");
    }

    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            if (DepartmentType.ReadReceipts.equals(departmentType)) {
                workReportReadReceiptsMemberComponent.loadSelectDepartment(selectOrgs);
                DisplayController.getInstance().update("readReceiptPanel");
                DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
            } else if (DepartmentType.ManagerConunterSignInfo.equals(departmentType)) {
                managerSignInfoMemberComponent.loadSelectDepartment(selectOrgs);
                DisplayController.getInstance().update("reqCounterSignPanel");
                DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
            } else if (DepartmentType.ExecManagerConunterSignInfo.equals(departmentType)) {
                execManagerSignInfoMemberComponent.loadSelectDepartment(selectOrgs);
                DisplayController.getInstance().update("execCounterSignPanel");
                DisplayController.getInstance().hidePfWidgetVar("deptUnitDlg");
                // ss
            }
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() { return depTreeComponent.getMultipleDepTreeManager(); }

    private void settingEditMode(WCMaster wcMaster) {
        if (wcMaster.getWc_status().equals(WCStatus.INVALID)) {
            Preconditions.checkState(false, "該工作聯絡單已作廢,不可編輯");
        }
        if (wcMaster.getWc_status().equals(WCStatus.CLOSE)) {
            Preconditions.checkState(false, "該工作聯絡單已結案,不可編輯");
        }
        if (wcMaster.getWc_status().equals(WCStatus.CLOSE_STOP)) {
            Preconditions.checkState(false, "該工作聯絡單已結案,不可編輯");
        }
        if (wcMaster.getLock_usr() != null && wcMaster.getLock_dt() != null) {
            if (!wcMaster.getLock_usr().equals(userViewVO.getSid())) {
                User lockUser = WkUserCache.getInstance().findBySid(wcMaster.getLock_usr());
                if (PermissionLogicForCanUse.getInstance().isCanUseReqSideEditAndUploadAttch(masterSid, lockUser.getSid())) {
                    int remainingTime = lockCountDownComponent.remainingTime(wcMaster.getLock_dt());
                    if (remainingTime > 0) {
                        Preconditions.checkState(
                                false,
                                "該工作聯絡單已被["
                                        + lockUser.getName()
                                        + "]鎖定,"
                                        + "將於"
                                        + ToolsDate.transDateToString(
                                                SimpleDateFormatEnum.SdfTime.getValue(), wcMaster.getLock_dt())
                                        + "解除鎖定");
                    }
                }
            }
        }
        lockCountDownComponent.loadData(
                wcMaster.getSid(), userViewVO.getSid(), userViewVO.getName(), new Date());
        lockCountDownComponent.findEffectiveTime();
        DisplayController.getInstance().update("userModifyPanel");
        DisplayController.getInstance()
                .execute(
                        "countDown('screen_fullissue_content_view_lockCount','"
                                + lockCountDownComponent.getCountDownTime()
                                + "','closeLock();');");
    }

    public void doReply() {
        try {
            WCMasterLogicComponents.getInstance().replyWCMaster(masterSid, userViewVO.getSid(), replyEditVO);
            loadData(masterSid);
            workReportTabCallBack.reloadTraceTab();
            workReportTabCallBack.reloadAttTab();
            execReplyComponent.loadData();
            workReportTabCallBack.reloadTabView(TabType.ExecReply);
            // boolean isFromAgainReply = false;
            // this.selectReplyTab(replyEditVO.getType(), isFromAgainReply);
            DisplayController.getInstance().execute("doReloadDataToUpdate('" + masterSid + "')");
            DisplayController.getInstance().hidePfWidgetVar("reply_dialog");

        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    /**
     * 依據類型選擇讀取的tab
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void selectReplyTab(WCTraceType type, boolean isFromAgainReply) {
        if (null != type) {
            switch (type) {
            case REPLY:
                workReportTabCallBack.reloadReplyTab();
                workReportTabCallBack.reloadTabView(TabType.Reply);
                if (!isFromAgainReply) {
                    if (replyEditVO == null || Strings.isNullOrEmpty(replyEditVO.getSid())) {
                        DisplayController.getInstance()
                                .execute("PF('wc_replytrace_tab_wv').select('" + 0 + "');");
                    } else {
                        int index = this.getIndexByReply(replyEditVO);
                        DisplayController.getInstance()
                                .execute("PF('wc_replytrace_tab_wv').select('" + index + "');");
                    }
                } else {
                    WCTraceVO traceVO = this.getTraceVOByAgainReply(
                            againReplyEditVO.getType(), againReplyEditVO.getTraceSid());
                    if (traceVO != null) {
                        DisplayController.getInstance()
                                .execute(
                                        "PF('wc_replytrace_tab_wv').select('"
                                                + this.getIndexByReply(traceVO)
                                                + "');");
                        if (Strings.isNullOrEmpty(againReplyEditVO.getSid())) {
                            DisplayController.getInstance()
                                    .execute(
                                            "PF('wc_replytrace_tab_wv_"
                                                    + againReplyEditVO.getTraceSid()
                                                    + "').select('"
                                                    + 0
                                                    + "');");
                        } else {
                            DisplayController.getInstance()
                                    .execute(
                                            "PF('wc_replytrace_tab_wv_"
                                                    + againReplyEditVO.getTraceSid()
                                                    + "').select('"
                                                    + traceVO.getAgainTrace().indexOf(againReplyEditVO)
                                                    + "');");
                        }
                    }
                }
                break;
            case FINISH_REPLY:
                workReportTabCallBack.reloadFinishReplyTab();
                // workReportTabCallBack.reloadTabView(TabType.FINISH_REPLY);
                if (!isFromAgainReply) {
                    if (replyEditVO == null || Strings.isNullOrEmpty(replyEditVO.getSid())) {
                        DisplayController.getInstance()
                                .execute("PF('wc_finish_reply_tab_wv').select('" + 0 + "');");
                    } else {
                        int index = this.getIndexByReply(replyEditVO);
                        DisplayController.getInstance()
                                .execute("PF('wc_finish_reply_tab_wv').select('" + index + "');");
                    }
                } else {
                    WCTraceVO traceVO = this.getTraceVOByAgainReply(
                            againReplyEditVO.getType(), againReplyEditVO.getTraceSid());
                    if (traceVO != null) {
                        DisplayController.getInstance()
                                .execute(
                                        "PF('wc_finish_reply_tab_wv').select('"
                                                + this.getIndexByReply(traceVO)
                                                + "');");
                        if (Strings.isNullOrEmpty(againReplyEditVO.getSid())) {
                            DisplayController.getInstance()
                                    .execute(
                                            "PF('wc_finish_reply_tab_wv_"
                                                    + againReplyEditVO.getTraceSid()
                                                    + "').select('"
                                                    + 0
                                                    + "');");
                        } else {
                            DisplayController.getInstance()
                                    .execute(
                                            "PF('wc_finish_reply_tab_wv_"
                                                    + againReplyEditVO.getTraceSid()
                                                    + "').select('"
                                                    + traceVO.getAgainTrace().indexOf(againReplyEditVO)
                                                    + "');");
                        }
                    }
                }
                break;
            case CORRECT_REPLY:
                workReportTabCallBack.reloadCorrectReplyTab();
                WorkReportComponent data = loadData(masterSid);
                workReportTraceComponent.loadData(masterSid, userViewVO.getSid());
                Preconditions.checkState(data != null, "載入工作聯絡單失敗！！");
                DisplayController.getInstance().update("workReportBtnPanel");
                DisplayController.getInstance().update("workReportHeaderPanel");
                DisplayController.getInstance().update("legitimate_range_panel_id");
                DisplayController.getInstance().update("start_end_panel_id");
                DisplayController.getInstance().update("category_items_panel_id");
                DisplayController.getInstance().update("workReportDataPanel");
                // workReportTabCallBack.reloadTabView(TabType.CORRECT_REPLY);
                if (!isFromAgainReply) {
                    if (replyEditVO == null || Strings.isNullOrEmpty(replyEditVO.getSid())) {
                        DisplayController.getInstance()
                                .execute("PF('wc_correct_reply_tab_wv').select('" + 0 + "');");
                    } else {
                        int index = this.getIndexByReply(replyEditVO);
                        DisplayController.getInstance()
                                .execute(
                                        "PF('wc_correct_reply_tab_wv').select('" + index + "');");
                    }
                }
                break;
            case DIRECTIVE:
                workReportTabCallBack.reloadDirectiveTab();
                // workReportTabCallBack.reloadTabView(TabType.DIRECTIVE);
                if (!isFromAgainReply) {
                    if (replyEditVO == null || Strings.isNullOrEmpty(replyEditVO.getSid())) {
                        DisplayController.getInstance()
                                .execute("PF('wc_directive_tab_wv').select('" + 0 + "');");
                    } else {
                        int index = this.getIndexByReply(replyEditVO);
                        DisplayController.getInstance()
                                .execute("PF('wc_directive_tab_wv').select('" + index + "');");
                    }
                }
                break;
            default:
                log.warn("無對應型態。type：" + type);
                break;
            }
        }
    }

    public void loadReply(String traceID, String widgetVar) {
        try {
            replyEditVO = WCTraceLogicComponents.getInstance()
                    .getWCTraceVOBySid(userViewVO.getSid(), traceID);
            // int index = this.getIndexByReply(replyEditVO);
            // DisplayController.getInstance().execute("PF('" + widgetVar + "').select('" +
            // String.valueOf(index) + "');");
            AttachmentCondition ac = new AttachmentCondition(masterSid, masterNo, false);
            wpAttachmentCompant.loadAttachment(ac);
            DisplayController.getInstance().showPfWidgetVar("reply_dialog");
            DisplayController.getInstance().update("reply_dialog");
            DisplayController.getInstance()
                    .execute("initReinstatedDlg('reply_dialog','reply_dialog_edit',150,170);");
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    /**
     * 取得索引值
     *
     * @param vo
     * @return
     */
    private int getIndexByReply(WCTraceVO vo) {
        if (null != vo && vo.getType() != null) {
            switch (vo.getType()) {
            case REPLY:
                return workReportReplyComponent.getWcTraceVO().indexOf(vo);
            case FINISH_REPLY:
                return workReportFinishReplyComponent.getWcTraceVO().indexOf(vo);
            case CORRECT_REPLY:
                return workReportCorrectReplyComponent.getWcTraceVO().indexOf(vo);
            case DIRECTIVE:
                return workReportDirectiveComponent.getWcTraceVO().indexOf(vo);
            default:
                log.warn("無對應型態。type：" + vo.getType());
            }
        }
        return 0;
    }

    public void doClose() {
        try {
            WCMaster wcMaster = WCMasterLogicComponents.getInstance().getWCMasterBySid(masterSid);

            if (!PermissionLogicForCanUse.getInstance().isCanUseReqSideClose(masterSid, SecurityFacade.getUserSid())) {
                MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
                return;
            }

            WCExceDepLogicComponent.getInstance()
                    .doClose(wcMaster.getSid(), wcMaster.getWc_no(), userViewVO.getSid());
            execFinishReLoadCallBack.onReload();
            DisplayController.getInstance().hidePfWidgetVar("close_dlg");
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("doClose ERROR：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("doClose ERROR", e);
        }
    }

    /**
     * 重新讀取
     */
    private void loadDataByBpm() {
        WorkReportComponent data = loadData(masterSid);
        workReportTraceComponent.loadData(masterSid, userViewVO.getSid());
        Preconditions.checkState(data != null, "載入工作聯絡單失敗！！");
        DisplayController.getInstance().update("workReportBtnPanel");
        DisplayController.getInstance().update("workReportHeaderPanel");
        DisplayController.getInstance().update("legitimate_range_panel_id");
        DisplayController.getInstance().update("start_end_panel_id");
        DisplayController.getInstance().update("category_items_panel_id");
        DisplayController.getInstance().update("workReportDataPanel");
        DisplayController.getInstance().update("userModifyPanel");
    }

    /**
     * 初始化 回覆視窗
     *
     * @param type
     */
    private void initReplyDialog(WCTraceType type) {

        String dateTime = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                new Date());
        String userInfo = depViewVo.getName() + "-" + userViewVO.getName();
        replyEditVO = new WCTraceVO(
                0, "", type, userInfo, dateTime, "", null, "", "", false, userViewVO.getSid());
        AttachmentCondition ac = new AttachmentCondition(masterSid, masterNo, false);
        wpAttachmentCompant.loadAttachment(ac);
        DisplayController.getInstance().showPfWidgetVar("reply_dialog");
        DisplayController.getInstance().update("reply_dialog");
        DisplayController.getInstance()
                .execute("initReinstatedDlg('reply_dialog','reply_dialog_edit',150,170);");
    }

    private final WorkReportButtonCallBack workReportButtonCallBack = new WorkReportButtonCallBack() {
        /** */
        private static final long serialVersionUID = 7630898252541945677L;

        @Override
        public void clickEdit() {
            try {
                User loginUser = WkUserCache.getInstance().findBySid(userViewVO.getSid());
                // 判斷是否有編輯權限
                if (!PermissionLogicForCanUse.getInstance().isCanUseReqSideEditAndUploadAttch(masterSid, loginUser.getSid())) {
                    Preconditions.checkState(false, "已無權限進行編輯！！");
                }
                // 輸入框改為編輯模式
                workReportBtnComponent.setEditMode(true);
                if (Strings.isNullOrEmpty(masterSid)) {
                    masterSid = Faces.getRequestParameterMap().get("wrcId");
                }
                WorkReportComponent data = loadData(masterSid);
                bpmComponent.getBpmVo().updateDisabledBtn(Boolean.TRUE);
                Preconditions.checkState(data != null, "載入工作聯絡單失敗！！");
                // else {
                // //將header變可編輯
                // edit = false;
                // }
                DisplayController.getInstance().update("workReportBtnPanel");
                DisplayController.getInstance().update("workReportHeaderPanel");
                DisplayController.getInstance().update("legitimate_range_panel_id");
                DisplayController.getInstance().update("start_end_panel_id");
                DisplayController.getInstance().update("category_items_panel_id");
                DisplayController.getInstance().update("workReportDataPanel");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickTrace() {
            viewPersonComponent.loadData(masterSid);
            DisplayController.getInstance().showPfWidgetVar("dlgViewUsers");
            DisplayController.getInstance().update("dlgTraceActionUsers_view");
        }

        @Override
        public void clickReply() {
            initReplyDialog(WCTraceType.REPLY);
        }

        @Override
        public void clickFinishReply() {
            execReplyComponent.loadExecReplyByNew();
        }

        @Override
        public void clickCorrectReply() {
            initReplyDialog(WCTraceType.CORRECT_REPLY);
        }

        @Override
        public void clickDirective() {
            initReplyDialog(WCTraceType.DIRECTIVE);
        }

        @Override
        public void clickTag() {
            try {
                execFinishComponent.loadData(masterSid);
                DisplayController.getInstance().update("execFinishPanel");
                DisplayController.getInstance().showPfWidgetVar("execFinishDlg");
                DisplayController.getInstance().execute(
                        "initReinstatedDlg('execFinishDlg','execFinishDlg_edit',150,170);");
            } catch (UserMessageException e) {
                MessagesUtils.show(e);
                return;
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickSummit() {
            try {
                execSendPersonComponent.loadData(masterSid);
                DisplayController.getInstance().update("dlg_sendExecPerson_view");
                DisplayController.getInstance().clearFilter("dataTableWidgetVar");
                DisplayController.getInstance().showPfWidgetVar("dlg_sendExecPerson");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        /**
         * 轉單
         */
        @Override
        public void clickDoSummit() {
            try {
                execTransPersonComponent.loadData(masterSid);
                DisplayController.getInstance().update("dlg_execPerson_view");
                DisplayController.getInstance().clearFilter("dataTableWidgetVar");
                DisplayController.getInstance().showPfWidgetVar("dlg_execPerson");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickSend() {
            try {
                User loginUser = WkUserCache.getInstance().findBySid(userViewVO.getSid());
                // 判斷是否有編輯權限
                if (!PermissionLogicForCanUse.getInstance().isCanUseReqSideEditAndUploadAttch(masterSid, loginUser.getSid())) {
                    Preconditions.checkState(false, "已無權限進行編輯！！");
                }
                WCMasterLogicComponents.getInstance()
                        .updateWCMaster(
                                masterSid,
                                workReportDataComponent,
                                wpAttachmentCompant.getAttachmentVOs(),
                                categoryItemComponent,
                                userViewVO.getSid());
                lockCountDownComponent.cancelLock(masterSid, userViewVO.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
                workReportTabCallBack.reloadTraceTab();
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance()
                        .execute("doEditContentToUpdate('" + masterSid + "')");

            } catch (UserMessageException e) {
                MessagesUtils.show(e);

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickCancel() {
            try {
                lockCountDownComponent.cancelLock(masterSid, userViewVO.getSid());
                boolean reuslt = initLoadView();
                if (!reuslt) {
                    return;
                }
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickAtt() {
            AttachmentCondition ac = new AttachmentCondition(masterSid, masterNo, true);
            ac.setUploadAttCallBack(uploadAttCallBack);
            wpAttachmentCompant.loadAttachment(ac);
            DisplayController.getInstance().showPfWidgetVar("dlgUploadWv");
            DisplayController.getInstance().execute("updateAthUploadMsg()");
        }

        @Override
        public void clickPrevious() {
            previousAndNextCallBack.doPrevious();
        }

        @Override
        public void clickNext() {
            previousAndNextCallBack.doNext();
        }

        @Override
        public void clickBackList() {
            throw new UnsupportedOperationException(
                    "Not supported yet."); // To change body of generated methods, choose Tools |
            // Templates.
        }

        /**
         * 需求單位-作廢
         */
        @Override
        public void clickInvalid() {
            bpmComponent.doAction(
                    ActionType.REQ_FLOW_INVAILD.name(),
                    workReportBtnComponent.getInvalidReson(),
                    "");
        }

        /**
         * 執行方-簽核者退回
         */
        @Override
        public void clickExecRollBack() {
            bpmComponent.doAction(
                    ActionType.EXEC_FLOW_SIGNER_ROLLBACK.name(),
                    workReportBtnComponent.getRollbackReason(),
                    bpmComponent.getBpmVo().getRollbackSelectTaskSid());
        }

        /**
         * 執行方-簽核者不執行
         */
        @Override
        public void clickExecStop() {
            bpmComponent.doAction(
                    ActionType.EXEC_FLOW_SIGNER_STOP.name(),
                    workReportBtnComponent.getRollbackReason(),
                    bpmComponent.getBpmVo().getRollbackSelectTaskSid());
        }

        /**
         * 執行方-單位成員退回
         */
        @Override
        public void clickExecDepRollBack() {
            bpmComponent.doAction(
                    ActionType.EXEC_FLOW_MEMBER_ROLLBACK.name(),
                    workReportBtnComponent.getRollbackReason(),
                    bpmComponent.getBpmVo().getRollbackSelectTaskSid());

        }

        /**
         * 執行方-單位成員不執行
         */
        @Override
        public void clickExecDepStop() {
            bpmComponent.doAction(
                    ActionType.EXEC_FLOW_MEMBER_STOP.name(),
                    workReportBtnComponent.getRollbackReason(),
                    bpmComponent.getBpmVo().getRollbackSelectTaskSid());
        }

        @Override
        public void clickReqCounterSign() {
            signInfoMemberCallBack.openReqSignInfo();
        }

        @Override
        public void clickExecCounterSign() {
            signInfoMemberCallBack.openCheckSignInfo();
        }

        @Override
        public void clickFavorite() {
            try {
                if (workReportBtnComponent.isFavrite()) {
                    WCFavoriteLogicComponents.getInstance()
                            .removeFavorite(masterSid, masterNo, userViewVO.getSid());
                } else {
                    WCFavoriteLogicComponents.getInstance()
                            .addFavorite(masterSid, masterNo, userViewVO.getSid());
                }
                workReportTabCallBack.reloadWCMasterData();
                DisplayController.getInstance()
                        .execute("doFavoriteToUpdate('" + masterSid + "')");
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickCustomerExceDep() {
            wcExceDepComponent.openCustomDepDialog();
        }

        @Override
        public void clickClose() {
            try {
                if (!PermissionLogicForCanUse.getInstance().isCanUseReqSideClose(masterSid, SecurityFacade.getUserSid())) {
                    MessagesUtils.showWarn(WkMessage.NEED_RELOAD);
                    return;
                }

                DisplayController.getInstance().showPfWidgetVar("close_dlg");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickComments() {
            try {
                User user = WkUserCache.getInstance().findBySid(userViewVO.getSid());
                List<SingleManagerSignInfo> reqFlowsingleManagerSignInfos = WCManagerSignInfoLogicComponent.getInstance()
                        .getSingleManagerSignInfo(masterSid, user, SignType.SIGN);
                Preconditions.checkState(
                        reqFlowsingleManagerSignInfos != null
                                && !reqFlowsingleManagerSignInfos.isEmpty(),
                        "該簽核流程已不在該人員節點,無法進行意見說明！！");
                managerSingInfoType = ManagerSingInfoType.MANAGERSIGNINFO;
                singleManagerSignInfos = reqFlowsingleManagerSignInfos;
                commentsComponent.init(
                        WCOpinionDescripeSettingLogicComponent.getInstance()
                                .getOpinionDescripe(
                                        managerSingInfoType,
                                        reqFlowsingleManagerSignInfos.get(0).getSignInfoSid(),
                                        userViewVO.getSid()));
                DisplayController.getInstance()
                        .update("dlg_comments_component_view_textarea_id");
                DisplayController.getInstance().showPfWidgetVar("dlg_comments_component_wv");
                LogHelper.getInstance()
                        .addLogInfo(masterSid, userViewVO.getSid(), "進入意見說明(需求流程)Dialog");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickCommentsContinueSign() {
            try {
                User user = WkUserCache.getInstance().findBySid(userViewVO.getSid());
                List<SingleManagerSignInfo> reqFlowsingleManagerSignInfos = WCManagerSignInfoLogicComponent.getInstance()
                        .getSingleManagerSignInfo(masterSid, user, SignType.COUNTERSIGN);
                Preconditions.checkState(
                        reqFlowsingleManagerSignInfos != null
                                && !reqFlowsingleManagerSignInfos.isEmpty(),
                        "該簽核流程已不在該人員節點,無法進行意見說明！！");
                managerSingInfoType = ManagerSingInfoType.MANAGERSIGNINFO;
                singleManagerSignInfos = reqFlowsingleManagerSignInfos;
                commentsComponent.init(
                        WCOpinionDescripeSettingLogicComponent.getInstance()
                                .getOpinionDescripe(
                                        managerSingInfoType,
                                        reqFlowsingleManagerSignInfos.get(0).getSignInfoSid(),
                                        userViewVO.getSid()));
                DisplayController.getInstance()
                        .update("dlg_comments_component_view_textarea_id");
                DisplayController.getInstance().showPfWidgetVar("dlg_comments_component_wv");
                LogHelper.getInstance()
                        .addLogInfo(masterSid, userViewVO.getSid(), "進入意見說明(需求會簽)Dialog");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickCommentsCheckSign() {
            try {
                User user = WkUserCache.getInstance().findBySid(userViewVO.getSid());
                List<SingleManagerSignInfo> execFlowsingleManagerSignInfos = WCExecManagerSignInfoLogicComponent.getInstance()
//                        .getSingleManagerSignInfo(masterSid, user, null);
                        .getSingleManagerSignInfo(masterSid, user, SignType.SIGN);
                Preconditions.checkState(
                        execFlowsingleManagerSignInfos != null
                                && !execFlowsingleManagerSignInfos.isEmpty(),
                        "該簽核流程已不在該人員節點,無法進行意見說明！！");
                managerSingInfoType = ManagerSingInfoType.CHECKMANAGERINFO;
                singleManagerSignInfos = execFlowsingleManagerSignInfos;
                commentsComponent.init(
                        WCOpinionDescripeSettingLogicComponent.getInstance()
                                .getOpinionDescripe(
                                        managerSingInfoType,
                                        execFlowsingleManagerSignInfos.get(0).getSignInfoSid(),
                                        userViewVO.getSid()));
                DisplayController.getInstance()
                        .update("dlg_comments_component_view_textarea_id");
                DisplayController.getInstance().showPfWidgetVar("dlg_comments_component_wv");
                LogHelper.getInstance()
                        .addLogInfo(masterSid, userViewVO.getSid(), "進入意見說明(執行簽核流程)Dialog");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void clickCommentsCheckContinueSign() {
            try {
                User user = WkUserCache.getInstance().findBySid(userViewVO.getSid());
                List<SingleManagerSignInfo> execFlowsingleManagerSignInfos = WCExecManagerSignInfoLogicComponent.getInstance()
                        .getSingleManagerSignInfo(masterSid, user, SignType.COUNTERSIGN);
                Preconditions.checkState(
                        execFlowsingleManagerSignInfos != null
                                && !execFlowsingleManagerSignInfos.isEmpty(),
                        "該簽核流程已不在該人員節點,無法進行意見說明！！");
                managerSingInfoType = ManagerSingInfoType.CHECKMANAGERINFO;
                singleManagerSignInfos = execFlowsingleManagerSignInfos;
                commentsComponent.init(
                        WCOpinionDescripeSettingLogicComponent.getInstance()
                                .getOpinionDescripe(
                                        managerSingInfoType,
                                        execFlowsingleManagerSignInfos.get(0).getSignInfoSid(),
                                        userViewVO.getSid()));
                DisplayController.getInstance()
                        .update("dlg_comments_component_view_textarea_id");
                DisplayController.getInstance().showPfWidgetVar("dlg_comments_component_wv");
                LogHelper.getInstance()
                        .addLogInfo(masterSid, userViewVO.getSid(), "進入意見說明(執行簽核流程)Dialog");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }
    };

    public void openAgainReply(String traceSid) {
        try {
            WCMaster wcMaster = WCMasterLogicComponents.getInstance().getWCMasterBySid(masterSid);
            Preconditions.checkState(
                    !wcMaster.getWc_status().equals(WCStatus.CLOSE)
                            && !wcMaster.getWc_status().equals(WCStatus.CLOSE_STOP)
                            && !wcMaster.getWc_status().equals(WCStatus.INVALID),
                    "不可進行執行完成回覆！！");
            String dateTime = ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date());
            String userInfo = depViewVo.getName() + "-" + userViewVO.getName();
            againReplyEditVO = new WCAgainTraceVO("", traceSid, WCTraceType.EXEC_FINISH, userInfo, dateTime, "",
                    false);
            DisplayController.getInstance().showPfWidgetVar("again_reply_dialog");
            DisplayController.getInstance().update("again_reply_dialog");
            DisplayController.getInstance()
                    .execute(
                            "initReinstatedDlg('again_reply_dialog','again_reply_dialog_edit',150,170);");

        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    /**
     * 更新 再次回覆資料
     */
    public void doAgainReply() {
        try {
            WCMaster wcMaster = WCMasterLogicComponents.getInstance().getWCMasterBySid(masterSid);
            Preconditions.checkState(
                    !wcMaster.getWc_status().equals(WCStatus.CLOSE)
                            && !wcMaster.getWc_status().equals(WCStatus.CLOSE_STOP)
                            && !wcMaster.getWc_status().equals(WCStatus.INVALID),
                    "不可進行執行完成回覆！！");
            @SuppressWarnings("unused")
            WCMaster wc = WCMasterLogicComponents.getInstance()
                    .againReplyWCMaster(masterSid, userViewVO.getSid(), againReplyEditVO);
            workReportTabCallBack.reloadTraceTab();
            workReportTabCallBack.reloadAttTab();
            workReportTabCallBack.reloadTabView(TabType.Trace);
            DisplayController.getInstance().hidePfWidgetVar("again_reply_dialog");
            DisplayController.getInstance()
                    .execute(
                            "PF('wc_trace_tab_wv').select('"
                                    + workReportTraceComponent
                                            .getWcTraceVO()
                                            .indexOf(new WCTraceVO(againReplyEditVO.getTraceSid()))
                                    + "');");

        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    /**
     * 轉換再次回覆型態
     *
     * @param type
     * @return
     */
    @SuppressWarnings("unused")
    private WCTraceType transReply(WCTraceType type) {
        if (null != type) {
            switch (type) {
            case FINISH_REPLY_AND_REPLY:
                return WCTraceType.FINISH_REPLY;
            default:
            }
        }
        return null;
    }

    /**
     * 取得索引值
     *
     * @param type
     * @param traceSid
     * @return
     */
    private WCTraceVO getTraceVOByAgainReply(WCTraceType type, String traceSid) {
        if (type != null) {
            switch (type) {
            case FINISH_REPLY_AND_REPLY:
                return workReportFinishReplyComponent.getWcTraceVO().stream()
                        .filter(each -> traceSid.equals(each.getSid()))
                        .findFirst()
                        .get();
            default:
                log.warn("無對應型態。type：" + type);
            }
        }
        return null;
    }

    @Getter
    public final UploadAttCallBack uploadAttCallBack = new UploadAttCallBack() {
        /** */
        private static final long serialVersionUID = 3431884011779269613L;

        @Override
        public void doUploadAtt(AttachmentVO att) {
            workReportTabCallBack.reloadAttTab();
            workReportTabCallBack.reloadTabView(TabType.Attachment);
        }
    };

    /**
     * 指定特定頁籤資料 By 最新消息
     *
     * @param alertSid
     * @return
     */
    public TabType focusTabByNews(String alertSid) {
        WCAlert alert = WCAlertManager.getInstance().findOne(alertSid);
        if (alert == null || alert.getType() == null) {
            log.warn("找不到Alert無法指定特定頁籤。sid：" + alertSid);
            return null;
        }
        switch (alert.getType()) {
        case EXEC_REPLY:
            return TabType.ExecReply;
        case EXEC_FINISH:
            selTraceSid = alert.getWcTraceSid();
            return TabType.Trace;
        case EXEC_FINISH_REPLY:
            selTraceSid = alert.getWcTraceSid();
            return TabType.Trace;
        }
        return null;
    }

    public void onTabChange(@SuppressWarnings("rawtypes") TabChangeEvent event) {
        try {
            if ("回覆".equals(event.getTab().getTitle())) {
                log.info(userViewVO.getName() + "執行讀取回覆" + masterSid);
                DisplayController.getInstance().execute("doReadReply();");
            }
        } catch (Exception e) {
            log.warn("onTabChange Error", e);
        }
    }

    public void readRelpy() {
        try {
            WCReadLogicComponents.getInstance()
                    .readReplyReadWCMaster(masterSid, userViewVO.getSid());
            DisplayController.getInstance().execute("doEditContentToUpdate('" + masterSid + "')");
        } catch (Exception e) {
            log.warn("readRelpy Error", e);
        }
    }

    public final TabCallBack workReportTabCallBack = new TabCallBack() {

        /** */
        private static final long serialVersionUID = 7518503665777023179L;

        @Override
        public void reloadBpmTab() {
            // bpmComponent.loadData(WCMasterLogicComponents.getInstance().getWRMasterBySid(wrcId));
            loadDataByBpm();
            workReportTabCallBack.reloadTabView(TabType.BpmFlow);
        }

        @Override
        public void reloadExecBpmTab() {
            loadDataByBpm();
            workReportTabCallBack.reloadTabView(TabType.BpmFlow);
        }

        @Override
        public void reloadTraceTab() {
            workReportTraceComponent.loadData();
        }

        @Override
        public void reloadReplyTab() {
            workReportReplyComponent.loadData();
        }

        @Override
        public void reloadFinishReplyTab() {
            workReportFinishReplyComponent.loadData();
        }

        @Override
        public void reloadCorrectReplyTab() {
            workReportCorrectReplyComponent.loadData();
        }

        @Override
        public void reloadDirectiveTab() {
            workReportDirectiveComponent.loadData();
        }

        @Override
        public void reloadAttTab() {
            workReportAttachMaintainCompant.loadData();
        }

        @Override
        public void reloadRelevanceTab() {
        }

        @Override
        public void reloadTabView(TabType tabType) {
            loadTabIndex(tabType);
            showBottomInfoTab = (workReportTraceComponent.isShowTraceTab() || workReportReplyComponent.isShowReplyTab() || workReportAttachMaintainCompant.isShowAttTab()
                    || workReportReadReceiptsMemberComponent.isShowReadReceiptTab() || bpmComponent.isShowExecManagerTab() || bpmComponent.getBpmVo().isShowTab()
                    || workReportFinishReplyComponent.isShowTab() || workReportCorrectReplyComponent.isShowTab() || workReportDirectiveComponent.isShowTab() || execReplyComponent.isShowTab()
                    || viewPersonComponent.isShowViewPersonTab());

            DisplayController.getInstance().update("viewPanelBottomInfoTabId");
            DisplayController.getInstance().execute("mergeSignCell();");
            if (!Strings.isNullOrEmpty(selTraceSid)) {
                try {
                    int ind = workReportTraceComponent.getWcTraceVO().indexOf(new WCTraceVO(selTraceSid));
                    if (ind >= 0) {
                        DisplayController.getInstance().execute("PF('wc_trace_tab_wv').select('" + ind + "');");
                    }
                } catch (Exception e) {
                    log.warn("reloadTabView ERROR", e);
                } finally {
                    selTraceSid = "";
                }
            }
        }

        @Override
        public void reloadWCMasterData() {
            boolean reuslt = initLoadView();
            if (!reuslt) {
                return;
            }
        }

        @Override
        public void reloadViewPersonTab() {
            try {
                viewPersonComponent.loadViewPersonData(masterSid);
            } catch (Exception e) {
                log.warn("reloadViewPersonTab ERROR", e);
            }
        }
    };

    private final BpmtCallBack bpmtCallBack = new BpmtCallBack() {

        /** */
        private static final long serialVersionUID = -8939721804380448411L;

        @Override
        public void showMessage(String message) {
            MessagesUtils.showWarn(message);
        }

        @Override
        public void reloadBPMTab() {
            loadDataByBpm();
            workReportTabCallBack.reloadTraceTab();
            workReportTabCallBack.reloadTabView(TabType.BpmFlow);
        }

        @Override
        public void reloadExecBPMTab() {
            loadDataByBpm();
            workReportTabCallBack.reloadTraceTab();
            workReportTabCallBack.reloadTabView(TabType.BpmFlow);
        }
    };

    /**
     * 執行分派後,CallBack
     */
    private final ReLoadCallBack execSendPersonReLoadCallBack = new ReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 449249219650186838L;

        @Override
        public void onReload() {
            workReportTabCallBack.reloadWCMasterData();
            workReportTabCallBack.reloadTraceTab();
            execReplyComponent.loadData();
            workReportTabCallBack.reloadTabView(TabType.Trace);
            DisplayController.getInstance().execute("doTraceToUpdate('" + masterSid + "')");
        }
    };

    /**
     * 執行轉單後,CallBack
     */
    private final ReLoadCallBack execTransPersonReLoadCallBack = new ReLoadCallBack() {
        /** */
        private static final long serialVersionUID = 3762115709365618234L;

        @Override
        public void onReload() {
            workReportTabCallBack.reloadWCMasterData();
            workReportTabCallBack.reloadTraceTab();
            execReplyComponent.loadData();
            workReportTabCallBack.reloadTabView(TabType.Trace);
            DisplayController.getInstance().execute("doTraceToUpdate('" + masterSid + "')");
        }
    };

    /**
     * 執行執行完成後,CallBack
     */
    private final ReLoadCallBack execFinishReLoadCallBack = new ReLoadCallBack() {
        /** */
        private static final long serialVersionUID = -4899887142381764071L;

        @Override
        public void onReload() {
            workReportTabCallBack.reloadWCMasterData();
            workReportTabCallBack.reloadTraceTab();
            execReplyComponent.loadData();
            workReportTabCallBack.reloadFinishReplyTab();
            workReportTabCallBack.reloadTabView(TabType.Trace);
            DisplayController.getInstance().execute("doTraceToUpdate('" + masterSid + "')");
        }
    };

    private final ExecReplyCallBack execReplyCallBack = new ExecReplyCallBack() {
        /** */
        private static final long serialVersionUID = 7357838172494946048L;

        @Override
        public void showMessage(String message) {
            MessagesUtils.showWarn(message);
        }

        @Override
        public void onReload() {
            execReplyComponent.loadData();
            workReportTabCallBack.reloadTabView(TabType.ExecReply);
        }
    };

    /**
     * 會簽元件 CallBack
     */
    @Getter
    public final SignInfoMemberCallBack signInfoMemberCallBack = new SignInfoMemberCallBack() {
        /** */
        private static final long serialVersionUID = -24417242583957244L;

        @Override
        public void openReqSignInfo() {
            try {
                departmentType = DepartmentType.ManagerConunterSignInfo;
                selectOrgs = Lists.newArrayList();
                managerSignInfoMemberComponent.loadUserPicker(masterSid);
                DisplayController.getInstance().update("reqCounterSignPanel");
                DisplayController.getInstance().showPfWidgetVar("reqCounterSignDlg");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void openCheckSignInfo() {
            try {
                departmentType = DepartmentType.ExecManagerConunterSignInfo;
                selectOrgs = Lists.newArrayList();
                execManagerSignInfoMemberComponent.loadUserPicker(masterSid);
                DisplayController.getInstance().update("execCounterSignPanel");
                DisplayController.getInstance().showPfWidgetVar("execCounterSignDlg");
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void saveCheckSignInfoMember(List<UserViewVO> selUserViews) {
            try {

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        /** 需求方儲存會簽人員 */
        @Override
        public void saveReqSignInfoMember(List<UserViewVO> selUserViews) {
            try {
                WCMaster wcMaster = WCMasterLogicComponents.getInstance().getWCMasterBySid(masterSid);

                // ====================================
                // 檢核是否為【需求方可異動會簽人員】的狀態
                // ====================================
                // 表列可加會簽狀態
                List<WCStatus> passStatus = Lists.newArrayList(WCStatus.NEW_INSTANCE, WCStatus.APPROVING, WCStatus.WAITAPPROVE, WCStatus.RECONSIDERATION);

                // 檢核狀態
                Preconditions.checkState(passStatus.contains(wcMaster.getWc_status()),
                        "該工作聯絡單狀態為" + (wcMaster.getWc_status() == null ? "空" : "【" + wcMaster.getWc_status().getVal() + "】") + "，不可異動會簽人員!");

                // ====================================
                // 整理新增或移除的會簽人員名單
                // ====================================
                SignInfoVO reqSignInfoVo = WCManagerSignInfoLogicComponent.getInstance().getReqSignInfo(
                        masterSid,
                        selUserViews,
                        userViewVO.getSid(),
                        depViewVo.getSid());

                // ====================================
                // 異動會簽人員名單 (資料+流程)
                // ====================================
                WCManagerSignInfoManager.getInstance().saveCounterSignInfo(wcMaster.getSid(), wcMaster.getWc_no(), reqSignInfoVo.getRemoveSingleManagerSignInfos(), reqSignInfoVo.getAddUserFlowSid(),
                        userViewVO.getSid());

                // ====================================
                // 新增追蹤記錄
                // ====================================
                WCMasterLogicComponents.getInstance().createCounterSignTrace(masterSid, masterNo, userViewVO.getSid(), reqSignInfoVo.getRemoveSingleManagerSignInfos(),
                        reqSignInfoVo.getAddUserFlowSid());

                // ====================================
                // 重新整理畫面
                // ====================================
                workReportTabCallBack.reloadWCMasterData();
                workReportTraceComponent.loadData(masterSid, userViewVO.getSid());
                workReportTabCallBack.reloadTabView(TabType.Trace);
                DisplayController.getInstance().hidePfWidgetVar("reqCounterSignDlg");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }
    };

    /**
     * 意見說明元件 CallBack
     */
    public final CommentsComponentCallBack commentsComponentCallBack = new CommentsComponentCallBack() {
        /** */
        private static final long serialVersionUID = 944679367967512241L;

        @Override
        public void showMessage(String message) {
            MessagesUtils.showWarn(message);
        }

        @Override
        public void submit() {
            try {
                WCOpinionDescripeSettingLogicComponent.getInstance().saveOpinionDescripe(managerSingInfoType, singleManagerSignInfos, userViewVO.getSid(), commentsComponent.getCommentsText(),
                        masterSid);
                workReportTabCallBack.reloadBpmTab();
                DisplayController.getInstance().hidePfWidgetVar("dlg_comments_component_wv");

            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
                MessagesUtils.showError(errorMessage);
                log.error(errorMessage, e);
            }
        }

        @Override
        public void cancel() {
            DisplayController.getInstance().hidePfWidgetVar("dlg_comments_component_wv");
        }
    };
}
