/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.web.logic.components.WCCategoryTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCExecDepSettingLogicComponent;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.cy.work.connect.web.logic.components.WCTagLogicComponents;
import com.cy.work.connect.web.view.vo.TagVO;
import com.google.common.collect.Lists;
import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * 挑選類別 元件
 *
 * @author jimmy_chou
 */
public class MultipleALLTagTreeManager extends MultipleTagTreeManager {

    /**
     *
     */
    private static final long serialVersionUID = -8464178803840074706L;

    public MultipleALLTagTreeManager() {
        rootNode = new DefaultTreeNode();
    }

    /**
     * 建立Tag樹
     *
     * @param loginCompSid 公司Sid
     */
    @Override
    protected void buildTagTree(Integer loginCompSid) {
        buildCategoryTagTree(loginCompSid, false, false);
    }

    /**
     * 建立Tag樹
     *
     * @param loginCompSid 公司Sid
     * @param mode         是否顯示執行單位
     * @param status       是否僅建立啟用節點
     */
    public void buildTagTree(Integer loginCompSid, boolean mode, boolean status) {
        buildCategoryTagTree(loginCompSid, mode, status);
    }

    private void buildCategoryTagTree(Integer loginCompSid, boolean mode, boolean status) {
        TreeNode categroyNode = null;
        List<WCCategoryTag> categoryTagList =
            WCCategoryTagLogicComponent.getInstance().findAll(null, loginCompSid);
        for (WCCategoryTag categoryTag : categoryTagList) {
            if (status == false || Activation.ACTIVE.equals(categoryTag.getStatus())) {
                categroyNode =
                    new DefaultTreeNode(
                        "CategoryTag",
                        new TagVO(
                            categoryTag.getSid(),
                            categoryTag.getCategoryName(),
                            "",
                            "",
                            categoryTag.getSeq(),
                            categoryTag.getStatus()),
                        rootNode);
                buildMenuTagTree(categoryTag, categroyNode, mode, status);
            }
        }
    }

    private void buildMenuTagTree(
        WCCategoryTag categoryTag, TreeNode parentNode, boolean mode, boolean status) {
        TreeNode menuNode = null;
        List<WCMenuTag> menuTagList =
            WCMenuTagLogicComponent.getInstance()
                .getWCMenuTagByCategorySid(categoryTag.getSid(), null);
        for (WCMenuTag menuTag : menuTagList) {
            if (status == false || Activation.ACTIVE.equals(menuTag.getStatus())) {
                menuNode =
                    new DefaultTreeNode(
                        "MenuTag",
                        new TagVO(
                            menuTag.getSid(),
                            "",
                            menuTag.getMenuName(),
                            "",
                            menuTag.getSeq(),
                            menuTag.getStatus()),
                        parentNode);
                buildSubTagTree(menuTag, menuNode, mode, status);
            }
        }
    }

    private void buildSubTagTree(
        WCMenuTag menuTag, TreeNode parentNode, boolean mode, boolean status) {
        TreeNode tagNode = null;
        List<WCTag> tagList =
            WCTagLogicComponents.getInstance().getWCTagByMenuSid(menuTag.getSid(), null);
        for (WCTag tag : tagList) {
            if (status == false || Activation.ACTIVE.equals(tag.getStatus())) {
                tagNode =
                    new DefaultTreeNode(
                        "Tag",
                        new TagVO(tag.getSid(), "", "", tag.getTagName(), tag.getSeq(),
                            tag.getStatus()),
                        parentNode);
                if (mode) {
                    buildExecDepTree(tag, tagNode);
                }
            }
        }
    }

    private void buildExecDepTree(WCTag tag, TreeNode parentNode) {
        List<WCExecDepSetting> execDepList =
            WCExecDepSettingLogicComponent.getInstance()
                .getWCExecDepSettingByWcTagSid(tag.getSid(), null);
        for (WCExecDepSetting execDep : execDepList) {
            Org dep = WkOrgCache.getInstance().findBySid(execDep.getExec_dep_sid());
            if (Activation.ACTIVE.equals(execDep.getStatus())) {
                new DefaultTreeNode(
                    "ExecDep",
                    new TagVO(
                        execDep.getSid(),
                        "",
                        "",
//                        ((dep != null) ? dep.getName() : ""),
                        ((dep != null) ? dep.getOrgNameWithParentIfDuplicate() : ""),
                        ((dep != null) ? dep.getSeqNumber() : 0),
                        ((dep != null) ? dep.getStatus() : Activation.INACTIVE)),
                    parentNode);
            }
        }
    }

    public void selectNode(TreeNode node) {
        List<TreeNode> tempSelNode = Lists.newArrayList();
        this.selNode.forEach(
            each -> {
                tempSelNode.add(each);
            });
        if (node.isSelected()) {
            tempSelNode.add(node);
        } else {
            tempSelNode.remove(node);
        }
        this.setSelNode(tempSelNode.stream().toArray(size -> new TreeNode[size]));
    }
}
