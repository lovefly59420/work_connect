/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.config;

import com.cy.security.BasedSecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * @author brain0925_liao
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends BasedSecurityConfig {

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        super.configure(webSecurity);
        webSecurity.ignoring().antMatchers("/monitor/**");
        webSecurity.ignoring().antMatchers("/clear/**");
    }
}
