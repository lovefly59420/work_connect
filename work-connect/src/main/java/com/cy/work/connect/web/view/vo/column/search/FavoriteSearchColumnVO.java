/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.column.search;

import com.cy.work.connect.web.view.vo.column.ColumnDetailVO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class FavoriteSearchColumnVO {

    /**
     * 序
     */
    @Setter
    @Getter
    private ColumnDetailVO index;

    @Setter
    @Getter
    private ColumnDetailVO favoriteDate;

    @Setter
    @Getter
    private ColumnDetailVO theme;
    @Setter
    @Getter
    private ColumnDetailVO wcNo;

    @Setter
    @Getter
    private String pageCount = "50";
}
