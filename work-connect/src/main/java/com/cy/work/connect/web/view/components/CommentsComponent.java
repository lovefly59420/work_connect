/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import com.cy.work.connect.web.callable.CommentsComponentCallBack;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 意見說明元件
 *
 * @author brain0925_liao
 */
@Slf4j
public class CommentsComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2370692732235700900L;

    @Getter
    private final CommentsComponentCallBack commentsComponentCallBack;

    /**
     * 意見說明
     */
    @Getter
    @Setter
    private String commentsText;

    public CommentsComponent(CommentsComponentCallBack commentsComponentCallBack) {
        this.commentsComponentCallBack = commentsComponentCallBack;
    }

    /**
     * 初始化
     */
    public void init(String commentsText) {
        this.commentsText = commentsText;
    }

    /**
     * 執行 確定 按鈕
     */
    public void btnSubmit() {
        try {
            commentsComponentCallBack.submit();
        } catch (Exception e) {
            log.warn("執行 確定 按鈕 發生錯誤!!", e);
            commentsComponentCallBack.showMessage("執行 確定 按鈕 發生錯誤!!");
        }
    }

    /**
     * 執行 取消 按鈕
     */
    public void btnCancel() {
        try {
            commentsComponentCallBack.cancel();
        } catch (Exception e) {
            log.warn("執行 取消 按鈕 發生錯誤!!", e);
            commentsComponentCallBack.showMessage("執行 取消 按鈕 發生錯誤!!");
        }
    }
}
