/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

import com.cy.work.connect.web.view.enumtype.TabType;
import java.io.Serializable;

/**
 * 工作聯絡單頁簽CallBack
 *
 * @author brain0925_liao
 */
public class TabCallBack implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6488880463268971414L;

    public void reloadBpmTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadExecBpmTab() {
    }

    public void reloadTraceTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadReplyTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadFinishReplyTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadCorrectReplyTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadDirectiveTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadAttTab() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadTabView(TabType tabType) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void reloadRelevanceTab() {
    }

    public void reloadWCMasterData() {
    }

    public void reloadViewPersonTab() {
    }
}
