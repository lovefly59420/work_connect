/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.model.DualListModel;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.web.listener.SignInfoMemberCallBack;
import com.cy.work.connect.web.listener.TabCallBack;
import com.cy.work.connect.web.logic.components.WCOrgLogic;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單位加簽物件
 *
 * @author brain0925_liao
 */
@Slf4j
public class ManagerSignInfoMemberComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7547945374020798327L;
    private final Integer loginUserSid;
    @Getter
    private final boolean showReadReceiptTab = false;
    @Getter
    private final SignInfoMemberCallBack signInfoMemberCallBack;
    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;
    private LinkedHashSet<UserViewVO> leftViewVO;
    private String wc_Id;
    @Getter
    @Setter
    private String filterNameValue;
    private Integer compSid;
    private LinkedHashSet<UserViewVO> allCompUser;

    /**
     * @param loginUserSid
     * @param messageCallBack        顯示訊息CallBack
     * @param workReportTabCallBack  工作聯絡單頁簽CallBack
     * @param signInfoMemberCallBack
     */
    public ManagerSignInfoMemberComponent(
        Integer loginUserSid,
        TabCallBack workReportTabCallBack,
        SignInfoMemberCallBack signInfoMemberCallBack) {

        this.loginUserSid = loginUserSid;
        this.signInfoMemberCallBack = signInfoMemberCallBack;
        pickUsers = new DualListModel<>();
    }

    public void loadWCID(String wc_Id, String wc_no) {
        this.wc_Id = wc_Id;
    }

    /**
     * 準備未選取清單和已選取清單資料 by 已選擇 user
     *
     * @param selUserViews
     */
    public void loadUserPicker(List<UserViewVO> selUserViews) {
        this.filterNameValue = "";
        User user = WkUserCache.getInstance().findBySid(loginUserSid);

        // ====================================
        // 取得全公司的 user (非停用)
        // ====================================
        // 公司SID
        this.compSid = user.getPrimaryOrg().getCompanySid();
        // 全公司 user
        this.allCompUser = Sets.newLinkedHashSet();
        try {
            WkOrgCache.getInstance().findAllDepByCompSid(this.compSid).stream()
                // 過濾非停用部門
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList())
                .forEach(
                    item -> {
                        // 依據單位別, 取得 user
                        allCompUser.addAll(
                            WCUserLogic.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }

        // ====================================
        // 準備未選取清單和已選取清單資料
        // ====================================
        // 左邊視窗顯示 (未選取)
        leftViewVO = Sets.newLinkedHashSet();
        // 取得基本部門(至少為【部】)
        Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        // 取得基本部門以下的所有子部門
        List<Org> allChildOrg =
            WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());
        // 加入基本部門
        leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(baseOrg.getSid()));
        // 加入所有子部門
        allChildOrg.forEach(
            item -> {
                leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        // 未挑選 (全部-已挑選)
        pickUsers.setSource(removeTargetUserView(Lists.newArrayList(leftViewVO), selUserViews));
        // 已挑選
        pickUsers.setTarget(selUserViews);
    }

    public void loadUserPicker(String wc_Id) {
        this.filterNameValue = "";
        this.wc_Id = wc_Id;
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        this.compSid = user.getPrimaryOrg().getCompanySid();
        allCompUser = Sets.newLinkedHashSet();
        try {
            WkOrgCache.getInstance().findAllDepByCompSid(this.compSid).stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList())
                .forEach(
                    item -> {
                        allCompUser.addAll(
                            WCUserLogic.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }

        leftViewVO = Sets.newLinkedHashSet();
        Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        List<Org> allChildOrg =
            WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());
        leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(
            item -> {
                leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        List<UserViewVO> rightViewVO = Lists.newArrayList();
        List<SingleManagerSignInfo> counterSignInfos =
            WCManagerSignInfoManager.getInstance().getCounterSignInfos(wc_Id);
        counterSignInfos.forEach(
            item -> {
                User tempUser = WkUserCache.getInstance()
                    .findBySid(Integer.valueOf(item.getUserSid()));
                Org dep = WkOrgCache.getInstance().findBySid(tempUser.getPrimaryOrg().getSid());
                UserViewVO uv =
                    new UserViewVO(
                        tempUser.getSid(),
                        tempUser.getName(),
                        WkOrgUtils.prepareBreadcrumbsByDepName(
                            dep.getSid(), OrgLevel.DIVISION_LEVEL, true, "-"));
                if (!item.getStatus().equals(BpmStatus.NEW_INSTANCE)
                    && !item.getStatus().equals(BpmStatus.WAIT_CREATE)) {
                    uv.setMoveAble(false);
                }
                rightViewVO.add(uv);
            });
        pickUsers.setSource(removeTargetUserView(Lists.newArrayList(leftViewVO), rightViewVO));
        pickUsers.setTarget(rightViewVO);
    }

    public void loadSelectDepartment(List<Org> departments) {
        settingTargetSigned();
        if (departments == null || departments.size() == 0) {
            Preconditions.checkState(false, "請挑選部門!!");
            return;
        }
        // leftViewVO.clear();

        LinkedHashSet<UserViewVO> selectDepUserView = Sets.newLinkedHashSet();
        departments.forEach(
            item -> {
                selectDepUserView.addAll(
                    WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        pickUsers.setSource(
            removeTargetUserView(Lists.newArrayList(selectDepUserView), pickUsers.getTarget()));
    }

    public void filterName() {
        settingTargetSigned();
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(
                removeTargetUserView(Lists.newArrayList(allCompUser), pickUsers.getTarget()));
        } else {
            pickUsers.setSource(
                removeTargetUserView(
                    allCompUser.stream()
                        .filter(
                            each -> each.getName().toLowerCase()
                                .contains(filterNameValue.toLowerCase()))
                        .collect(Collectors.toList()),
                    pickUsers.getTarget()));
        }

        DisplayController.getInstance().update("reqCounterSignPanel");
    }

    private void settingTargetSigned() {
        List<SingleManagerSignInfo> counterSignInfos = Lists.newArrayList();
        if (!Strings.isNullOrEmpty(wc_Id)) {
            counterSignInfos.addAll(
                WCManagerSignInfoManager.getInstance().getCounterSignInfos(wc_Id));
        }
        // 過濾出已簽核人員
        List<SingleManagerSignInfo> counterSignInfosSigned =
            counterSignInfos.stream()
                .filter(
                    each ->
                        !each.getStatus().equals(BpmStatus.NEW_INSTANCE)
                            && !each.getStatus().equals(BpmStatus.WAIT_CREATE))
                .collect(Collectors.toList());
        if (pickUsers.getTarget() != null && pickUsers.getTarget().size() > 0) {
            pickUsers
                .getTarget()
                .forEach(
                    item -> {
                        List<SingleManagerSignInfo> sameSingleManagerSignInfoTo =
                            counterSignInfosSigned.stream()
                                .filter(each -> each.getUserSid().equals(item.getSid()))
                                .collect(Collectors.toList());
                        if (sameSingleManagerSignInfoTo != null
                            && sameSingleManagerSignInfoTo.size() > 0) {
                            item.setMoveAble(false);
                        }
                    });
        }
    }

    public void saveReqCounterSignUser() throws UserMessageException {
        try {
            signInfoMemberCallBack.saveReqSignInfoMember(pickUsers.getTarget());
        } catch (Exception e) {
            log.error("處理會簽失敗!" + e.getMessage() , e);
            throw new UserMessageException("處理失敗!" + e.getMessage());
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
        List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
            item -> {
                // 未挑選不可以包含已挑選的人員及自己本身
                if (!target.contains(item) && !loginUserSid.equals(item.getSid())) {
                    filterSource.add(item);
                }
            });
        return filterSource;
    }
}
