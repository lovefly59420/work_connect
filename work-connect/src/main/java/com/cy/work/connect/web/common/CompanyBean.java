/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 目的公司Item Bean
 *
 * @author jimmy_chou
 */
@Controller
@Scope("view")
public class CompanyBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6251311624674623448L;

    /**
     * 取得來源公司SelectItems
     */
    public List<SelectItem> getCompanyItems() {
        return  WkUserCache.getInstance().findSecAuth()
                .stream()
                // 轉為 Org 資料
                .map(secAuth -> WkOrgCache.getInstance().findById(secAuth.getCompId()))
                .filter(comp -> comp != null)
                // 排序
                .sorted(Comparator.comparing(Org::getSeqNumber))
                // 轉為 SelectItem
                .map(comp -> new SelectItem(comp.getSid(), comp.getName()))
                .collect(Collectors.toList());
    }
}
