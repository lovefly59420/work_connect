/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.ExecDepLogic;
import com.cy.work.connect.logic.helper.PermissionLogicForExecDep;
import com.cy.work.connect.logic.helper.WaitAssignLogic;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.ExecDepCreateType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCExceDepLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5780368875869807675L;

    private static WCExceDepLogicComponent instance;

    @Override
    public void afterPropertiesSet() throws Exception {
        WCExceDepLogicComponent.instance = this;
    }

    private final transient Comparator<WCTag> wcTagComparator = new Comparator<WCTag>() {
        @Override
        public int compare(WCTag obj1, WCTag obj2) {
            final Integer seq1 = obj1.getSeq();
            final Integer seq2 = obj2.getSeq();
            return seq1.compareTo(seq2);
        }
    };
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WkOrgCache wkOrgCache;
    @Autowired
    private WCExecDepSettingLogicComponent execDepSettingLogicComponent;
    @Autowired
    private WCTagLogicComponents wcTagLogicComponents;
    @Autowired
    private WkUserCache wkUserCache;
    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;
    @Autowired
    private WCTagManager wCTagManager;

    public static WCExceDepLogicComponent getInstance() { return instance; }

    public void doClose(String wcSid, String wcNo, Integer loginUserSid) {
        wcExecDepManager.doClose(wcSid, wcNo, loginUserSid);
    }

    /**
     * 取得正在執行中的執行部門
     *
     * @param wc_ID 工作聯絡單Sid
     * @return
     */
    public List<Org> getProceedExecDeps(String wc_ID) {
        List<Org> orgs = Lists.newArrayList();
        try {
            List<WCExceDep> wcExceDep = wcExecDepManager.findActiveByWcSid(wc_ID);
            wcExceDep.forEach(
                    item -> {
                        if (item.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)) {
                            orgs.add(wkOrgCache.findBySid(item.getDep_sid()));
                        }
                    });
        } catch (Exception e) {
            log.warn("getProceedExecDeps ERROR", e);
        }
        return orgs;
    }

    /**
     * 取得所有的執行單位
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<WCExceDep> getAllExecDeps(String wcSid) {

        // ====================================
        // 依據單據查詢所有執行單位
        // ====================================
        List<WCExceDep> wcExceDep = this.wcExecDepManager.findActiveByWcSid(wcSid);
        if (WkStringUtils.isEmpty(wcExceDep)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 排序
        // ====================================
        // 先以狀態排序
        Comparator<WCExceDep> comparator = Comparator.comparing(
                currWcExceDep -> {
                    if (currWcExceDep == null || currWcExceDep.getExecDepStatus() == null) {
                        return Integer.MAX_VALUE;
                    }
                    return currWcExceDep.getExecDepStatus().getShowSeq();
                });

        // 狀態相同時，以單位順序排序
        comparator = comparator.thenComparing(
                Comparator.comparing(
                        currWcExceDep -> wkOrgCache.findOrgOrderSeqBySid(currWcExceDep.getDep_sid())));

        return wcExceDep.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    /**
     * 執行確認完成
     * 
     * @param wcSid        聯絡單 sid
     * @param loginUserSid 登入者
     * @param content      確認完成訊息
     * @throws UserMessageException 錯誤時拋出
     */
    public void saveExecFinish(
            String wcSid, Integer loginUserSid, String content) throws UserMessageException {
        try {
            this.wcExecDepManager.saveExecFinish(wcSid, loginUserSid, content);
        } catch (Exception e) {
            String message = WkMessage.PROCESS_FAILED + "(" + e.getMessage() + ")";
            log.error(message, e);
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message, SecurityFacade.getCompanyId());
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }
    }

    /**
     * 檢測轉單
     *
     * @param item       執行單位物件
     * @param selTagSids
     * @param sb
     */
    private void checkTransPersonExecDep(WCExceDep item, List<String> selTagSids,
            StringBuilder sb) {
        if (Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
            if (!item.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                    && !item.getExecDepStatus().equals(WCExceDepStatus.FINISH)) {
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("\n");
                }
                sb.append("狀態為[" + item.getExecDepStatus().getValue() + "]不可進行轉單");
            }
        } else {
            WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(item.getExecDepSettingSid());
            if (selTagSids.contains(execDepSetting.getWc_tag_sid())) {
                if (!item.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                        && !item.getExecDepStatus().equals(WCExceDepStatus.FINISH)) {
                    if (!Strings.isNullOrEmpty(sb.toString())) {
                        sb.append("\n");
                    }
                    sb.append(
                            "類別["
                                    + wcTagLogicComponents.getWCTagBySid(execDepSetting.getWc_tag_sid())
                                            .getTagName()
                                    + "]狀態為["
                                    + item.getExecDepStatus().getValue()
                                    + "]不可進行轉單");
                }
            }
        }
    }

    /**
     * 執行轉單
     *
     * @param wcSid        工作聯絡單Sid
     * @param tagMaps
     * @param selUserSid   欲轉單至該人員
     * @param loginUserSid 執行者Sid
     */
    public void saveTransPersonExecDep(
            String wcSid, Map<Integer, List<WCTag>> tagMaps, Integer selUserSid, Integer loginUserSid) {
        List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wcSid);
        StringBuilder sb = new StringBuilder();
        tagMaps
                .keySet()
                .forEach(
                        selExecDepSid -> {
                            List<WCExceDep> waitSendDeps = wcExceDeps.stream()
                                    .filter(
                                            each -> each.getStatus().equals(Activation.ACTIVE)
                                                    && each.getDep_sid().equals(selExecDepSid))
                                    .collect(Collectors.toList());
                            List<String> selTagSids = Lists.newArrayList();
                            tagMaps
                                    .get(selExecDepSid)
                                    .forEach(
                                            item -> {
                                                selTagSids.add(item.getSid());
                                            });

                            waitSendDeps.forEach(
                                    item -> {
                                        this.checkTransPersonExecDep(item, selTagSids, sb);
                                    });
                        });
        Preconditions.checkState(Strings.isNullOrEmpty(sb.toString()), sb.toString());
        wcExecDepManager.saveTransPersonExecDep(wcSid, tagMaps, selUserSid, loginUserSid);
    }

    /**
     * 檢測分派
     *
     * @param item       執行單位物件
     * @param selTagSids 可確認完成執行項目
     * @param sb
     */
    private void checkSendPersonExecDep(WCExceDep item, List<String> selTagSids, StringBuilder sb) {
        if (Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
            if (item.getExecUserSid() != null) {
                User user = this.wkUserCache.findBySid(item.getExecUserSid());
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("\n");
                }
                sb.append("已分派給[" + user.getName() + "]");
            }
        } else {
            WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(item.getExecDepSettingSid());
            if (selTagSids.contains(execDepSetting.getWc_tag_sid())) {
                if (item.getExecUserSid() != null) {
                    User user = this.wkUserCache.findBySid(item.getExecUserSid());
                    if (!Strings.isNullOrEmpty(sb.toString())) {
                        sb.append("\n");
                    }
                    sb.append(
                            "類別["
                                    + wcTagLogicComponents.getWCTagBySid(execDepSetting.getWc_tag_sid())
                                            .getTagName()
                                    + "]已分派給["
                                    + user.getName()
                                    + "]");
                }
            }
        }
    }

    /**
     * 執行分派
     *
     * @param wcSid        工作聯絡單Sid
     * @param tagMaps
     * @param selUserSid   欲分派至該人員
     * @param loginUserSid
     */
    public void saveSendPersonExecDep(
            String wcSid, Map<Integer, List<WCTag>> tagMaps, Integer selUserSid, Integer loginUserSid) {
        List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wcSid);
        StringBuilder sb = new StringBuilder();
        tagMaps.keySet()
                .forEach(
                        selExecDepSid -> {
                            List<WCExceDep> waitSendDeps = wcExceDeps.stream()
                                    .filter(
                                            each -> each.getStatus().equals(Activation.ACTIVE)
                                                    && each.getDep_sid().equals(selExecDepSid))
                                    .collect(Collectors.toList());
                            List<String> selTagSids = Lists.newArrayList();
                            tagMaps
                                    .get(selExecDepSid)
                                    .forEach(
                                            item -> {
                                                selTagSids.add(item.getSid());
                                            });
                            waitSendDeps.forEach(
                                    item -> {
                                        this.checkSendPersonExecDep(item, selTagSids, sb);
                                    });
                        });
        Preconditions.checkState(Strings.isNullOrEmpty(sb.toString()), sb.toString());
        wcExecDepManager.saveSendPersonExecDep(wcSid, tagMaps, selUserSid, loginUserSid);
    }

    /**
     * 取得該登入者可進行執行完成的執行部門及類別Map
     *
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public Map<Org, List<WCTag>> getExecFinishDeps(String wcSid, Integer loginUserSid) {

        // ====================================
        // 取得可進行『執行完成』的執行單位檔
        // ====================================
        List<WCExceDep> canExecFinishExecDeps = PermissionLogicForExecDep.getInstance().findCanExecFinish(
                wcSid, loginUserSid);

        if (WkStringUtils.isEmpty(canExecFinishExecDeps)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 資料整理
        // ====================================
        Map<Org, List<WCTag>> execDepMap = Maps.newConcurrentMap();
        for (WCExceDep item : canExecFinishExecDeps) {
            Org dep = wkOrgCache.findBySid(item.getDep_sid());
            List<WCTag> tags = execDepMap.get(dep);
            if (tags == null) {
                tags = Lists.newArrayList();
            }
            if (!Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
                WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(
                        item.getExecDepSettingSid());
                tags.add(wcTagLogicComponents.getWCTagBySid(execDepSetting.getWc_tag_sid()));
            }
            Collections.sort(tags, wcTagComparator);
            execDepMap.put(dep, tags);
        }

        return execDepMap;
    }

    public String prepareExecFinishDepsContent(String wcSid, Integer loginUserSid) {

        // ====================================
        // 取得可進行『執行完成』的執行單位檔
        // ====================================
        List<WCExceDep> canExecFinishExecDeps = PermissionLogicForExecDep.getInstance().findCanExecFinish(
                wcSid, loginUserSid);

        if (WkStringUtils.isEmpty(canExecFinishExecDeps)) {
            return "";
        }

        // ====================================
        // 取得所有執行單位的 tag sid
        // ====================================
        Set<String> tagSids = Sets.newHashSet();
        for (WCExceDep wcExceDep : canExecFinishExecDeps) {

            // 無執行單位設定檔跳過(加派單位)
            if (WkStringUtils.isEmpty(wcExceDep.getExecDepSettingSid())) {
                continue;
            }

            // 取得設定檔
            WCExecDepSetting execDepSetting = this.wcExecDepSettingManager.findBySidFromCache(wcExceDep.getExecDepSettingSid());
            if (execDepSetting == null
                    || WkStringUtils.isEmpty(execDepSetting.getWc_tag_sid()) // 防呆
            ) {
                continue;
            }

            tagSids.add(execDepSetting.getWc_tag_sid());

        }

        // ====================================
        // 取得所有 tag
        // ====================================
        String tagNames = "";
        if (WkStringUtils.notEmpty(tagSids)) {
            Comparator<WCTag> comparator = Comparator.comparing(WCTag::getTagType, Comparator.nullsLast(Comparator.naturalOrder()));
            comparator = comparator.thenComparing(Comparator.comparing(WCTag::getSeq, Comparator.nullsLast(Comparator.naturalOrder())));
            tagNames = WkCommonUtils.safeStream(this.wCTagManager.findBySidIn(tagSids))
                    .sorted(comparator)
                    .map(tag -> {
                        String tagName = "";
                        if (WkStringUtils.notEmpty(tag.getTagType())) {
                            tagName += tag.getTagType() + "&nbsp;";
                        }
                        return tagName + tag.getTagName();
                    })
                    .collect(Collectors.joining("<br/>"));
        }

        return "∎以下項目已執行完成<br/><br/>" + tagNames;
    }

    /**
     * 取得該登入者可進行轉單的執行部門及類別Map
     * 
     * @param wcSid        主單 sid
     * @param loginUserSid 登入者 sid
     * @param loginDepSid  登入部門
     * @return
     */
    public Map<Org, List<WCTag>> getTransPersonExecDeps(
            String wcSid,
            Integer loginUserSid,
            Integer loginDepSid) {

        // ====================================
        // 可執行轉單的執行單位
        // ====================================
        List<WCExceDep> wcExceDeps = ExecDepLogic.getInstance().findExecDepForCanTransPerson(
                wcSid, loginUserSid, loginDepSid);

        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 整理資料
        // ====================================
        Map<Org, List<WCTag>> execDepMap = Maps.newHashMap();

        for (WCExceDep wcExceDep : wcExceDeps) {

            Org dep = wkOrgCache.findBySid(wcExceDep.getDep_sid());
            List<WCTag> tags = execDepMap.get(dep);
            if (tags == null) {
                tags = Lists.newArrayList();
            }

            if (!Strings.isNullOrEmpty(wcExceDep.getExecDepSettingSid())) {
                WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(
                        wcExceDep.getExecDepSettingSid());
                tags.add(wcTagLogicComponents.getWCTagBySid(execDepSetting.getWc_tag_sid()));
            }
            Collections.sort(tags, wcTagComparator);
            execDepMap.put(dep, tags);
        }
        return execDepMap;
    }

    /**
     * 取得該登入者可進行分派的執行部門及類別Map
     *
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public Map<Org, List<WCTag>> getSendPersonExecDeps(String wcSid, Integer loginUserSid) {

        // ====================================
        // 查詢所有可分派的執行單位
        // ====================================
        Set<String> canAssignWcExecDepSids = WaitAssignLogic.getInstance().findUserCanAssignWcExecDepSids(
                loginUserSid,
                wcSid);
        if (WkStringUtils.isEmpty(canAssignWcExecDepSids)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 查詢執行單位檔
        // ====================================
        List<WCExceDep> wcExceDeps = this.wcExecDepManager.findBySids(canAssignWcExecDepSids);

        // ====================================
        //
        // ====================================
        Map<Org, List<WCTag>> execDepMap = Maps.newHashMap();

        try {
            for (WCExceDep currExceDep : wcExceDeps) {

                Org dep = wkOrgCache.findBySid(currExceDep.getDep_sid());
                List<WCTag> tags = execDepMap.get(dep);
                if (tags == null) {
                    tags = Lists.newArrayList();
                }
                if (!Strings.isNullOrEmpty(currExceDep.getExecDepSettingSid())) {
                    WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(
                            currExceDep.getExecDepSettingSid());
                    tags.add(
                            wcTagLogicComponents.getWCTagBySid(execDepSetting.getWc_tag_sid()));
                }
                Collections.sort(tags, wcTagComparator);
                execDepMap.put(dep, tags);

            }
        } catch (Exception e) {
            log.warn("getTransPersonExecDeps ERROR", e);
        }
        return execDepMap;
    }

    public void checkExecStatus(String wc_ID, List<Integer> correctDeps) {
        // List<Org> orgs = Lists.newArrayList();
        List<WCExceDep> wcExceDeps = wcExecDepManager.findActiveByWcSid(wc_ID);
        StringBuilder sb = new StringBuilder();
        for (Integer item : correctDeps) {
            List<WCExceDep> selExceDep = wcExceDeps.stream()
                    .filter(each -> each.getDep_sid().equals(item))
                    .collect(Collectors.toList());
            if (selExceDep == null || selExceDep.isEmpty()) {
                Org movedOrg = wkOrgCache.findBySid(item);
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("\n");
                }
                sb.append("[" + movedOrg.getName() + "]已非執行單位");
            } else if (selExceDep.get(0).getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE)) {
                Org org = wkOrgCache.findBySid(item);
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("\n");
                }
                sb.append("[" + org.getName() + "]目前尚未簽核完畢,無法進行符合需求動作");
            } else if (selExceDep.get(0).getExecDepStatus().equals(WCExceDepStatus.CLOSE)) {
                Org org = wkOrgCache.findBySid(item);
                if (!Strings.isNullOrEmpty(sb.toString())) {
                    sb.append("\n");
                }
                sb.append("[" + org.getName() + "]已符合需求");
            }
        }
        if (!Strings.isNullOrEmpty(sb.toString())) {
            Preconditions.checkState(false, sb.toString());
        }
    }

    public List<Org> getSystemExceDeps(String wc_ID) {
        List<Org> orgs = Lists.newArrayList();
        List<WCExceDep> wcExceDep = wcExecDepManager.findActiveByWcSid(wc_ID);
        wcExceDep.forEach(
                item -> {
                    if (item.getExecDepCreateType().equals(ExecDepCreateType.SYSTEM)) {
                        orgs.add(wkOrgCache.findBySid(item.getDep_sid()));
                    }
                });
        return orgs;
    }

    public List<Org> getCustomerAddExceDeps(String wc_ID) {
        List<Org> orgs = Lists.newArrayList();
        List<WCExceDep> wcExceDep = wcExecDepManager.findActiveByWcSid(wc_ID);
        wcExceDep.forEach(
                item -> {
                    if (item.getExecDepCreateType().equals(ExecDepCreateType.CUSTOMER)) {
                        orgs.add(wkOrgCache.findBySid(item.getDep_sid()));
                    }
                });
        return orgs;
    }

}
