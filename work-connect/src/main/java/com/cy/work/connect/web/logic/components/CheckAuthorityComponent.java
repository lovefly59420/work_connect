package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.FlowType;
import com.cy.work.connect.web.view.vo.*;
import com.cy.work.connect.web.view.vo.search.query.CheckAuthoritySearchQuery;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author marlow_chen
 */
@Component
public class CheckAuthorityComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8192963959630043755L;

    private static CheckAuthorityComponent instance;

    @Autowired
    private WCCategoryTagLogicComponent wCCategoryTagLogicComponent;

    @Autowired
    private WCMenuTagLogicComponent wCMenuTagLogicComponent;

    @Autowired
    private WCTagLogicComponents wCTagLogicComponents;

    @Autowired
    private WCExecDepSettingLogicComponent execDepSettingLogicComponent;

    @Autowired
    private WkOrgCache wkOrgCache;

    @Autowired
    private WkUserCache wkUserCache;

    public static CheckAuthorityComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        CheckAuthorityComponent.instance = this;
    }

    /**
     * 依據公司取得
     *
     * @param loginCompSid 登入公司 SID
     * @param loginDepSid  登入部門 SID
     * @param loginUserSid 登入User SID
     * @return
     */
    public List<CheckAuthorityVo> getAllCheckAuthority(
            Integer loginCompSid,
            Integer loginDepSid,
            Integer loginUserSid) {

        List<WCCategoryTag> categoryTags = wCCategoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid);

        List<CheckAuthorityVo> checkAuthoritys = Lists.newArrayList();

        for (WCCategoryTag categoryTag : categoryTags) {
            List<MenuTagSearchVO> menuTags = wCMenuTagLogicComponent.getMenuTagSearchVO(categoryTag.getSid(), Activation.ACTIVE);

            for (MenuTagSearchVO menuTag : menuTags) {
                List<WCTag> wcTags = wCTagLogicComponents.getWCTagActiveForMaster(
                        menuTag.getSid(),
                        null,
                        loginUserSid,
                        loginCompSid);

                MenuTagEditVO menuTagEditVO = wCMenuTagLogicComponent.getMenuTagEditVO(
                        menuTag.getSid());
                for (WCTag wcTag : wcTags) {

                    List<ExecDepSettingSearchVO> execDeps = execDepSettingLogicComponent.getExecDepSettingSearchVO(wcTag.getSid());
                    for (ExecDepSettingSearchVO execDep : execDeps) {
                        WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(
                                execDep.getWc_exec_dep_setting_sid());

                        CheckAuthorityVo checkAuthorityVo = new CheckAuthorityVo();

                        checkAuthorityVo.setTagId(categoryTag.getSid());
                        checkAuthorityVo.setTagName(categoryTag.getCategoryName());

                        if (wcTag.getReqFlowType() != null) {
                            checkAuthorityVo.setFlowType(wcTag.getReqFlowType());
                            checkAuthorityVo.setReqSignInfo(
                                    this.getFlowTypeToString(wcTag.getReqFlowType()));
                        } else {
                            // 此為前端merge使用,空字串並不會merge
                            checkAuthorityVo.setReqSignInfo("　");
                        }
                        checkAuthorityVo.setMenuTagId(menuTag.getSid());
                        checkAuthorityVo.setMenuTagName(menuTag.getName());

                        checkAuthorityVo.setTagType(wcTag.getTagType());
                        checkAuthorityVo.setSubTagId(wcTag.getSid());
                        checkAuthorityVo.setSubTagName(wcTag.getTagName());

                        // 加入可使用單位
                        this.addWhoCanUse(checkAuthorityVo, menuTag, menuTagEditVO, wcTag);
                        // 加入可使用人員
                        this.addWhoUserCanUse(checkAuthorityVo, menuTagEditVO);

                        checkAuthorityVo.setLegitimateRange(
                                this.converterLegitimate(
                                        wcTag.getLegitimateRangeTitle(),
                                        wcTag.getLegitimateRangeDays()));
                        checkAuthorityVo.setStartEnd(
                                this.converterStartEnd(
                                        wcTag.getStartEndTitle(),
                                        wcTag.getStartEndDays()));
                        // 此為前端merge使用,空字串並不會merge
                        checkAuthorityVo.setParentManager(
                                menuTagEditVO.isToParentManager() ? "✔" : "　");
                        checkAuthorityVo.setCyWindow(execDep.getCyWindow());

                        // 如果有設定可閱部門，超過20個部門用...代替
                        if (wcTag.getUseDep() != null) {
                            this.addWhoCanRead(checkAuthorityVo, wcTag);
                        } else {
                            // 此為前端merge使用,空字串並不會merge
                            checkAuthorityVo.setWhoCanRead("　");
                        }
                        // 如果有設定可閱人員,超過1個人員用...代替
                        if (wcTag.getUseUser() != null) {
                            addWhoUserCanRead(checkAuthorityVo, wcTag);
                        } else {
                            // 此為前端merge使用,空字串並不會merge
                            checkAuthorityVo.setViewUserNames("　");
                        }
                        // 執行(可領單)單位
                        List<Integer> execDepSids = Lists.newArrayList();
                        if (execDepSetting != null && execDepSetting.getExec_dep_sid() != null) {
                            execDepSids.add(execDepSetting.getExec_dep_sid());
                            Org execDepOrg = wkOrgCache.findBySid(execDepSetting.getExec_dep_sid());
                            if (execDepOrg != null) {
                                execDepSids.addAll(
                                        wkOrgCache.findAllChild(execDepOrg.getSid()).stream()
                                                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                                .map(Org::getSid)
                                                .collect(Collectors.toList()));
                            }
                        }
                        checkAuthorityVo.setExecDepSid(execDepSids);

                        checkAuthorityVo.setExecDep(execDep.getDepName());
                        checkAuthorityVo.setExecDepUsers(execDep.getUserListName());
                        checkAuthorityVo.setReceviceManager(execDep.getReceviceManager());
                        checkAuthorityVo.setReceviceUsers(execDep.getReceviceUserListName());

                        // 走領單或者派工
                        if (execDep.getIsNeedToAssign().equals("否") && execDep.getIsReadSetting()
                                .equals("否")) {
                            checkAuthorityVo.setExecMethod("領單");
                        } else {
                            checkAuthorityVo.setExecMethod("派工");
                            checkAuthorityVo.setAssignUsers(execDep.getAssignUserListName());
                        }
                        checkAuthorityVo.setTransdep(execDep.getTransdep());
                        checkAuthorityVo.setTransdepSid(execDep.getTransdepSids());

                        if (execDepSetting.getReceviceUser() != null) {
                            checkAuthorityVo.setReceviceUserList(execDepSetting.getReceviceUser());
                        }

                        checkAuthoritys.add(checkAuthorityVo);
                    }
                }
            }
        }

        return checkAuthoritys;
    }

    /**
     * 根據登入公司與單位與部門判斷登入部門是否有檢視權限
     *
     * @param loginCompSid 登入公司Sid
     * @param loginDepSid  登入部門Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public boolean isCheckAuthoritySearchPermission(
            Integer loginCompSid, Integer loginDepSid, Integer loginUserSid) {
        // 檢核是否為
        return this.loginUserIsWorkManagerPermission();
        // List<CheckAuthorityVo> checkAuthoritys = this.getAllCheckAuthority(loginCompSid);
        // return this.isPermissionCheck(checkAuthoritys, loginDepSid, loginUserSid);
    }

    /**
     * 權限判斷
     *
     * @param checkAuthoritys 經初始的核決權限list物件
     * @param loginUserSid    登入者Sid
     * @param loginDepSid     登入者部門Sid
     * @return
     */
    public List<CheckAuthorityVo> permissionCheckAuthority(
            List<CheckAuthorityVo> checkAuthoritys, Integer loginUserSid, Integer loginDepSid) {
        if (checkAuthoritys == null || checkAuthoritys.isEmpty()) {
            checkAuthoritys = Lists.newArrayList();
        }
        checkAuthoritys = this.permissionCheck(checkAuthoritys, loginDepSid, loginUserSid);
        return checkAuthoritys;
    }

    /**
     * 執行查詢
     *
     * @param checkAuthoritys 經初始的核決權限list物件
     * @param query           核決權限查詢物件
     * @return
     */
    public List<CheckAuthorityVo> queryCheckAuthority(
            List<CheckAuthorityVo> checkAuthoritys, CheckAuthoritySearchQuery query) {

        List<CheckAuthorityVo> queryList = Lists.newArrayList();
        queryList.addAll(checkAuthoritys);

        queryList = filterTags(queryList, query);
        queryList = filterQueryOrg(queryList, query);
        queryList = filterQueryUser(queryList, query);
        queryList = filterQueryModeAndFlowType(queryList, query);

        queryList = this.settingShowTagName(queryList);
        return queryList;
    }

    @SuppressWarnings("unused")
    private List<CheckAuthorityVo> settingShowTagName(List<CheckAuthorityVo> checkAuthoritys) {

        List<CheckAuthorityVo> checkAuthoritylist = Lists.newArrayList();
        if (checkAuthoritys.size() > 0) {
            checkAuthoritylist.add(checkAuthoritys.get(0));
        }

        int tagNo = 1;
        int menuTagNo = 1;
        int subTagNo = 1;

        for (CheckAuthorityVo each : checkAuthoritys) {

            CheckAuthorityVo last = checkAuthoritylist.get(checkAuthoritylist.size() - 1);
            if (!each.getTagName().equals(last.getTagName())) {
                tagNo++;
            }

            if (!each.getMenuTagName().equals(last.getMenuTagName())
                    && !each.getTagName().equals(last.getTagName())) {
                menuTagNo = 1;
            } else if (!each.getMenuTagName().equals(last.getMenuTagName())) {
                menuTagNo++;
            }

            if (!each.getMenuTagName().equals(last.getMenuTagName())) {
                subTagNo = 1;
            } else if (!each.getSubTagName().equals(last.getSubTagName())) {
                subTagNo++;
            }

            // each.setShowTagName(tagNo + "." + each.getTagName());
            // each.setShowMenuTagName(menuTagNo + "." + each.getMenuTagName());
            // each.setShowSubTagName(subTagNo + "." + each.getSubTagName());
            each.setShowTagName(each.getTagName());
            each.setShowMenuTagName(each.getMenuTagName());
            each.setShowSubTagName(each.getSubTagName());
            checkAuthoritylist.add(each);
        }
        if (checkAuthoritys.size() > 0) {
            checkAuthoritylist.remove(0);
        }

        return checkAuthoritylist;
    }

    /**
     * 權限判斷實作
     *
     * @param checkAuthoritys 經初始的核決權限list物件
     * @param loginUserSid    登入者Sid
     * @param loginDepSid     登入者部門Sid
     * @return
     */
    private List<CheckAuthorityVo> permissionCheck(
            List<CheckAuthorityVo> checkAuthoritys, Integer loginDepSid, Integer loginUserSid) {
        List<CheckAuthorityVo> checkAuthoritList = Lists.newArrayList();
        for (CheckAuthorityVo each : checkAuthoritys) {
            // 可使用單位的權限
            boolean isCanUse = this.loginDepCanUsePermission(each, loginDepSid);
            // 可領單單位的權限
            boolean isCanExec = this.loginDepCanExecPermission(each, loginDepSid);
            // 可閱部門的權限
            boolean isCanRead = this.loginDepCanReadPermission(each, loginDepSid);
            // 執行單位簽核人員的權限
            boolean isExecUser = this.loginUserCanExecPermission(each, loginUserSid);
            // 派工人員的權限
            boolean isAssignUser = this.loginUserIsAssignUserPermission(each, loginUserSid);
            // 領單人員的權限
            boolean isReceviceUser = this.loginUserIsReceviceUserPermission(each, loginUserSid);
            // 登入者是否為工作聯絡單管理者
            boolean isWorkManager = this.loginUserIsWorkManagerPermission();

            if (isCanUse
                    || isCanExec
                    || isCanRead
                    || isExecUser
                    || isAssignUser
                    || isReceviceUser
                    || isWorkManager) {
                checkAuthoritList.add(each);
            }
        }
        return checkAuthoritList;
    }

    /**
     * 權限判斷是否有權限
     *
     * @param checkAuthoritys 經初始的核決權限list物件
     * @param loginUserSid    登入者Sid
     * @param loginDepSid     登入者部門Sid
     * @return
     */
    @SuppressWarnings("unused")
    private boolean isPermissionCheck(
            List<CheckAuthorityVo> checkAuthoritys, Integer loginDepSid, Integer loginUserSid) {

        // //可使用單位的權限
        // boolean isCanUse = checkAuthoritys.stream().anyMatch(each ->
        // this.loginDepCanUsePermission(each, loginDepSid));
        // //可領單單位的權限
        // boolean isCanExec = checkAuthoritys.stream().anyMatch(each ->
        // this.loginDepCanExecPermission(each, loginDepSid));
        // //可閱部門的權限
        // boolean isCanRead = checkAuthoritys.stream().anyMatch(each ->
        // this.loginDepCanReadPermission(each, loginDepSid));
        // //執行單位簽核人員的權限
        // boolean isExecUser = checkAuthoritys.stream().anyMatch(each ->
        // this.loginUserCanExecPermission(each, loginDepSid));
        // //派工人員的權限
        // boolean isAssignUser = checkAuthoritys.stream().anyMatch(each ->
        // this.loginUserIsAssignUserPermission(each, loginDepSid));
        // //領單人員的權限
        // boolean isReceviceUser = checkAuthoritys.stream().anyMatch(each ->
        // this.loginUserIsReceviceUserPermission(each, loginDepSid));
        // 登入者是否為工作聯絡單管理者
        boolean isWorkManager = this.loginUserIsWorkManagerPermission();

        // if (isCanUse || isCanExec || isCanRead || isExecUser || isAssignUser ||
        // isReceviceUser || isWorkManager) {
        // return true;
        // }
        return isWorkManager;
    }

    // 可使用單位的權限
    private boolean loginDepCanUsePermission(CheckAuthorityVo checkAuthorityVo,
                                             Integer loginDepSid) {
        boolean result = false;
        if (!StringUtils.isEmpty(checkAuthorityVo.getWhoCanUseSid())) {
            result = checkAuthorityVo.getWhoCanUseSid().stream()
                    .anyMatch(each -> each.equals(loginDepSid));
        }
        if (checkAuthorityVo.getWhoCanUse().equals("全單位")) {
            result = true;
        }
        return result;
    }

    // 領單單位的權限
    private boolean loginDepCanExecPermission(
            CheckAuthorityVo checkAuthorityVo, Integer loginDepSid) {
        boolean result = false;
        if (!StringUtils.isEmpty(checkAuthorityVo.getExecDepSid())) {
            result = checkAuthorityVo.getExecDepSid().stream()
                    .anyMatch(each -> each.equals(loginDepSid));
        }
        return result;
    }

    // 可閱部門的權限
    private boolean loginDepCanReadPermission(
            CheckAuthorityVo checkAuthorityVo, Integer loginDepSid) {
        boolean result = false;
        if (!StringUtils.isEmpty(checkAuthorityVo.getWhoCanReadSid())) {
            result = checkAuthorityVo.getWhoCanReadSid().stream()
                    .anyMatch(each -> each.equals(loginDepSid));
        }
        return result;
    }

    // 執行單位簽核人員的權限
    private boolean loginUserCanExecPermission(
            CheckAuthorityVo checkAuthorityVo, Integer loginUserSid) {
        User loginUser = wkUserCache.findBySid(loginUserSid);
        boolean result = false;
        if (!StringUtils.isEmpty(checkAuthorityVo.getExecDepUsers())) {
            result = checkAuthorityVo.getExecDepUsers().contains(loginUser.getName());
        }
        return result;
    }

    // 派工人員的權限
    private boolean loginUserIsAssignUserPermission(
            CheckAuthorityVo checkAuthorityVo, Integer loginUserSid) {
        User loginUser = wkUserCache.findBySid(loginUserSid);
        boolean result = false;
        if (!StringUtils.isEmpty(checkAuthorityVo.getAssignUsers())) {
            result = checkAuthorityVo.getAssignUsers().contains(loginUser.getName());
        }
        return result;
    }

    /**
     * 管理領單人員的權限
     *
     * @param checkAuthorityVo
     * @param loginUserSid
     * @return
     */
    private boolean loginUserIsReceviceUserPermission(
            CheckAuthorityVo checkAuthorityVo, Integer loginUserSid) {
        User loginUser = wkUserCache.findBySid(loginUserSid);
        boolean result = false;
        if (!StringUtils.isEmpty(checkAuthorityVo.getReceviceUsers())) {
            result = checkAuthorityVo.getReceviceUserList().getUserTos().stream()
                    .anyMatch(each -> (each.getSid() + "").equals(loginUser.getSid() + ""));
        }
        return result;
    }

    /**
     * 登入者是否為工作聯絡單 設定人員
     *
     * @return
     */
    private boolean loginUserIsWorkManagerPermission() {
        return WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(), SecurityFacade.getCompanyId(), "工作聯絡單系統管理員:*")
                || WkUserUtils.isUserHasRole(
                SecurityFacade.getUserSid(), SecurityFacade.getCompanyId(), "聯絡單設定系統管理員");
    }

    /**
     * 加入可使用人員資訊
     *
     * @param checkAuthorityVo 核決權限list物件其中一筆
     * @param menuTagEditVO    單據名稱物件
     */
    private void addWhoUserCanUse(CheckAuthorityVo checkAuthorityVo, MenuTagEditVO menuTagEditVO) {
        List<Integer> useUserSids = Lists.newArrayList();
        // 此為前端merge使用,空字串並不會merge
        checkAuthorityVo.setUseUserNames("　");
        checkAuthorityVo.setShowUseUsers(menuTagEditVO.getUsers().size() > 1);
        StringBuilder sb = new StringBuilder();

        for (User user : menuTagEditVO.getUsers()) {
            if (!Strings.isNullOrEmpty(sb.toString())) {
                sb.append("\n");
            }
            if (user != null) {
                sb.append(user.getName());
                if (user.getStatus().equals(Activation.INACTIVE)) {
                    sb.append("<span style='color:red;'>(停用)</span>");
                }
            }
            useUserSids.add(user.getSid());
        }
        if (!StringUtils.isEmpty(sb.toString())) {
            checkAuthorityVo.setUseUserNames(sb.toString());
        }
        checkAuthorityVo.setUseUserSids(useUserSids);
    }

    /**
     * 內部方法，加入是否可以使用的部門，將單據名稱資訊寫入
     *
     * @param checkAuthorityVo 核決權限list物件其中一筆
     * @param menuTagEditVO    單據名稱物件
     * @param wcTag            執行項目物件
     * @return
     */
    private void addWhoCanUse(
            CheckAuthorityVo checkAuthorityVo,
            MenuTagSearchVO menuTag,
            MenuTagEditVO menuTagEditVO,
            WCTag wcTag) {
        List<Integer> oreSids = Lists.newArrayList();
        WCConfigTempletVo templet = WCConfigTempletManager.getInstance().findBySid(wcTag.getCanUserDepTempletSid());
        if (templet != null && ConfigTempletType.SUB_TAG_CAN_USE.equals(templet.getTempletType())) {
            List<TagSearchVO> tagSearchVOs = wCTagLogicComponents.getTagSearch(wcTag.getSid(), wcTag.getWcMenuTagSid(), false);
            checkAuthorityVo.setShowWhoCanUseTemplet(true);
            if (!tagSearchVOs.isEmpty()) {
                checkAuthorityVo.setWhoCanUse(
                        StringUtils.isEmpty(tagSearchVOs.get(0).getCanUserDepsShow())
                                ? "全單位"
                                : tagSearchVOs.get(0).getCanUserDepsShow());
                checkAuthorityVo.setWhoCanUseTooltip(
                        tagSearchVOs.get(0).getCanUserDepsShowToolTip());
            }
            oreSids = WCConfigTempletManager.getInstance()
                    .getTempletDeps(templet.getSid(), templet.getCompSid());
        } else if (menuTag.getUesDepsShow().contains("模版")) {
            checkAuthorityVo.setShowWhoCanUseTemplet(true);
            checkAuthorityVo.setWhoCanUse(
                    StringUtils.isEmpty(menuTag.getUesDepsShow()) ? "全單位" : menuTag.getUesDepsShow());
            checkAuthorityVo.setWhoCanUseTooltip(menuTag.getUesDepsShowToolTip());
            templet = WCConfigTempletManager.getInstance().findBySid(menuTag.getUseDepTempletSid());
            if (templet != null && ConfigTempletType.MENU_TAG_CAN_USE.equals(
                    templet.getTempletType())) {
                oreSids = WCConfigTempletManager.getInstance()
                        .getTempletDeps(templet.getSid(), templet.getCompSid());
            }
        } else {
            List<Org> orgs = Lists.newArrayList();
            StringBuilder whoCanUseListSb = new StringBuilder();
            boolean isUserContainFollowing = false;
            if (wcTag.getCanUserDep() != null
                    && wcTag.getCanUserDep().getUserDepTos() != null
                    && !wcTag.getCanUserDep().getUserDepTos().isEmpty()) {
                orgs = wcTag.getCanUserDep().getUserDepTos().stream()
                        .map(each -> wkOrgCache.findBySid(Integer.valueOf(each.getDepSid())))
                        .collect(Collectors.toList());
                isUserContainFollowing = wcTag.isUserContainFollowing();
            } else {
                orgs = menuTagEditVO.getOrgs();
                isUserContainFollowing = menuTagEditVO.isUserContainFollowing();
            }
            for (Org org : orgs) {
                if (org != null) {
//                    String orgName = org.getName();
                    String orgName = org.getOrgNameWithParentIfDuplicate();
                    if (org.getStatus().equals(Activation.INACTIVE)) {
                        orgName = "<span style='color:red;'>" + orgName + "</span>";
                    }
                    if (!Strings.isNullOrEmpty(whoCanUseListSb.toString())) {
                        whoCanUseListSb.append("\n");
                    }
                    whoCanUseListSb.append(orgName);
                    oreSids.add(org.getSid());
                    if (isUserContainFollowing) {
                        whoCanUseListSb.append("【<span class='WS1-1-3'>含以下</span>】");
                        oreSids.addAll(
                                wkOrgCache.findAllChild(org.getSid()).stream()
                                        .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                        .map(Org::getSid)
                                        .collect(Collectors.toList()));
                    }
                }
            }
            checkAuthorityVo.setWhoCanUse(
                    StringUtils.isEmpty(whoCanUseListSb.toString()) ? "全單位"
                            : whoCanUseListSb.toString());
        }
        checkAuthorityVo.setWhoCanUseSid(oreSids);
    }

    /**
     * 內部方法，加入是否可以閱讀的部門，將執行項目資訊寫入
     *
     * @param checkAuthorityVo 核決權限list物件其中一筆
     * @param wcTag            執行項目物件
     * @return
     */
    private void addWhoCanRead(CheckAuthorityVo checkAuthorityVo, WCTag wcTag) {
        List<Integer> oreSids = Lists.newArrayList();
        WCConfigTempletVo templet = WCConfigTempletManager.getInstance().findBySid(wcTag.getUseDepTempletSid());
        if (templet != null && ConfigTempletType.SUB_TAG_CAN_READ.equals(
                templet.getTempletType())) {
            List<TagSearchVO> tagSearchVOs = wCTagLogicComponents.getTagSearch(wcTag.getSid(), wcTag.getWcMenuTagSid(), false);
            checkAuthorityVo.setShowWhoCanReadTemplet(true);
            if (!tagSearchVOs.isEmpty()) {
                checkAuthorityVo.setWhoCanRead(tagSearchVOs.get(0).getUseDepShow());
                checkAuthorityVo.setWhoCanReadTooltip(tagSearchVOs.get(0).getUseDepShowToolTip());
            }
            oreSids = WCConfigTempletManager.getInstance()
                    .getTempletDeps(templet.getSid(), templet.getCompSid());
        } else {
            StringBuilder whoCanReadListSb = new StringBuilder();
            for (UserDepTo userDep : wcTag.getUseDep().getUserDepTos()) {
                if (userDep != null) {
                    Org org = wkOrgCache.findBySid(Integer.valueOf(userDep.getDepSid()));
                    if (org != null) {
//                        String orgName = org.getName();
                        String orgName = org.getOrgNameWithParentIfDuplicate();
                        if (org.getStatus().equals(Activation.INACTIVE)) {
                            orgName = "<span style='color:red;'>" + orgName + "</span>";
                        }
                        if (!Strings.isNullOrEmpty(whoCanReadListSb.toString())) {
                            whoCanReadListSb.append("\n");
                        }
                        whoCanReadListSb.append(orgName);
                        oreSids.add(org.getSid());
                        if (wcTag.isViewContainFollowing()) {
                            whoCanReadListSb.append("【<span class='WS1-1-3'>含以下</span>】");
                            oreSids.addAll(
                                    wkOrgCache.findAllChild(org.getSid()).stream()
                                            .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                            .map(Org::getSid)
                                            .collect(Collectors.toList()));
                        }
                    }
                }
            }
            // 此為前端merge使用,空字串並不會merge
            checkAuthorityVo.setWhoCanRead(
                    StringUtils.isEmpty(whoCanReadListSb.toString()) ? "　"
                            : whoCanReadListSb.toString());
        }
        checkAuthorityVo.setWhoCanReadSid(oreSids);
    }

    /**
     * 新增 可閱人員 名單
     *
     * @param checkAuthorityVo
     * @param wcTag
     */
    private void addWhoUserCanRead(CheckAuthorityVo checkAuthorityVo, WCTag wcTag) {
        List<Integer> viewUserSids = Lists.newArrayList();
        // 此為前端merge使用,空字串並不會merge
        checkAuthorityVo.setViewUserNames("　");
        checkAuthorityVo.setShowViewUsers(wcTag.getUseUser().getUserTos().size() > 1);
        StringBuilder sb = new StringBuilder();

        for (UserTo userTo : wcTag.getUseUser().getUserTos()) {
            User loginUser = wkUserCache.findBySid(userTo.getSid());
            if (!Strings.isNullOrEmpty(sb.toString())) {
                sb.append("\n");
            }
            if (loginUser != null) {
                sb.append(loginUser.getName());
                if (loginUser.getStatus().equals(Activation.INACTIVE)) {
                    sb.append("<span style='color:red;'>(停用)</span>");
                }
            }
            viewUserSids.add(userTo.getSid());
        }
        if (!StringUtils.isEmpty(sb.toString())) {
            checkAuthorityVo.setViewUserNames(sb.toString());
        }
        checkAuthorityVo.setViewUserSids(viewUserSids);
    }

    /**
     * 搜尋使用者選取的Tags，包含CategoryTag(類別)、MenuTag(單據名稱)、SubTag(執行項目)
     *
     * @param checkAuthoritys 核決權限list物件
     * @param query           核決權限查詢物件
     * @return
     */
    private List<CheckAuthorityVo> filterTags(
            List<CheckAuthorityVo> checkAuthoritys, CheckAuthoritySearchQuery query) {
        List<CheckAuthorityVo> checkAuthoritList = Lists.newArrayList();

        // query 選取物件前置處理(將父節點納入選取範圍)
        List<TreeNode> parentIncludeSelNode = Lists.newArrayList();
        for (TreeNode node : query.getSelNode()) {
            parentIncludeSelNode.add(node);
            while (node.getParent() != null) {
                parentIncludeSelNode.add(node.getParent());
                node = node.getParent();
            }
        }

        for (CheckAuthorityVo each : checkAuthoritys) {
            List<String> multiCategoryTagList = parentIncludeSelNode.stream()
                    .filter(item -> "CategoryTag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<String> multiMenuTagList = parentIncludeSelNode.stream()
                    .filter(item -> "MenuTag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());
            List<String> multiTagList = parentIncludeSelNode.stream()
                    .filter(item -> "Tag".equals(item.getType()))
                    .map(item -> ((TagVO) item.getData()).getSid())
                    .collect(Collectors.toList());

            boolean isCategoryTag = multiCategoryTagList.isEmpty() || multiCategoryTagList.contains(each.getTagId());
            boolean isMenuTag = multiMenuTagList.isEmpty() || multiMenuTagList.contains(each.getMenuTagId());
            boolean isTag = multiTagList.isEmpty() || multiTagList.contains(each.getSubTagId());

            boolean isHaveTag = each.getTagId().contains(query.getCategoryTagId());
            boolean isHaveMenuTag = each.getMenuTagId().contains(query.getMenuTagId());
            boolean isHaveSubTag = each.getSubTagId().contains(query.getSubTagId());
            if (isHaveTag && isHaveMenuTag && isHaveSubTag && isCategoryTag && isMenuTag && isTag) {
                checkAuthoritList.add(each);
            }
        }

        return checkAuthoritList;
    }

    /**
     * 搜尋使用者選取的可使用單位與執行單位(領單單位)
     *
     * @param checkAuthoritys 核決權限list物件
     * @param query           核決權限查詢物件
     * @return
     */
    private List<CheckAuthorityVo> filterQueryOrg(
            List<CheckAuthorityVo> checkAuthoritys, CheckAuthoritySearchQuery query) {

        List<CheckAuthorityVo> checkAuthoritList = Lists.newArrayList();
        for (CheckAuthorityVo each : checkAuthoritys) {

            boolean isHaveUse = query.getCanUseOrg().stream()
                    .anyMatch(
                            item -> each.getWhoCanUseSid() == null
                                    || each.getWhoCanUseSid().isEmpty()
                                    || each.getWhoCanUseSid().contains(item.getSid()));
            boolean isHaveExec = query.getExecOrg().stream()
                    .anyMatch(
                            item -> each.getExecDep() != null && each.getExecDepSid()
                                    .contains(item.getSid()));
            boolean isHaveViewDep = query.getViewOrgs().stream()
                    .anyMatch(
                            item -> each.getWhoCanReadSid() != null
                                    && !each.getWhoCanReadSid().isEmpty()
                                    && each.getWhoCanReadSid().contains(item.getSid()));
            boolean isHaveTransDep = query.getTransOrgs().stream()
                    .anyMatch(
                            item -> each.getTransdep() != null && each.getTransdepSid()
                                    .contains(item.getSid()));
            boolean isRelateDep = query.getRelatedOrg().stream()
                    .anyMatch(
                            item -> each.getWhoCanUseSid() != null
                                    && !each.getWhoCanUseSid().isEmpty()
                                    && each.getWhoCanUseSid().contains(item.getSid()))
                    || query.getRelatedOrg().stream()
                    .anyMatch(
                            item -> each.getExecDep() != null && each.getExecDep().contains(item.getName()))
                    || query.getRelatedOrg().stream()
                    .anyMatch(
                            item -> each.getWhoCanReadSid() != null
                                    && !each.getWhoCanReadSid().isEmpty()
                                    && each.getWhoCanReadSid().contains(item.getSid()))
                    || query.getRelatedOrg().stream()
                    .anyMatch(
                            item -> each.getTransdep() != null
                                    && each.getTransdep().contains(item.getName()));
            if (query.getCanUseOrg().isEmpty()) {
                isHaveUse = true;
            }
            if (query.getTransOrgs().isEmpty()) {
                isHaveTransDep = true;
            }
            if (query.getViewOrgs().isEmpty()) {
                isHaveViewDep = true;
            }
            if (query.getExecOrg().isEmpty()) {
                isHaveExec = true;
            }
            if (query.getRelatedOrg().isEmpty()) {
                isRelateDep = true;
            }
            if (isHaveUse && isHaveExec && isHaveViewDep && isHaveTransDep && isRelateDep) {
                checkAuthoritList.add(each);
            }
        }

        return checkAuthoritList;
    }

    /**
     * 搜尋執行單位簽核人員與派工人員
     *
     * @param checkAuthoritys 核決權限list物件
     * @param query           核決權限查詢物件
     * @return
     */
    private List<CheckAuthorityVo> filterQueryUser(
            List<CheckAuthorityVo> checkAuthoritys, CheckAuthoritySearchQuery query) {
        List<CheckAuthorityVo> checkAuthoritList = Lists.newArrayList();
        for (CheckAuthorityVo each : checkAuthoritys) {
            boolean isHaveUseUser = query.getUseUsers().stream()
                    .anyMatch(
                            item -> each.getUseUserSids() != null
                                    && each.getUseUserSids().contains(item.getSid()));
            boolean isHaveViewUser = query.getViewUsers().stream()
                    .anyMatch(
                            item -> each.getViewUserSids() != null
                                    && each.getViewUserSids().contains(item.getSid()));
            boolean isHaveExecUser = query.getExecSignUser().stream()
                    .anyMatch(
                            item -> each.getExecDepUsers() != null
                                    && each.getExecDepUsers().contains(item.getName()));
            boolean isHaveAssignUser = query.getAssignUser().stream()
                    .anyMatch(
                            item -> each.getAssignUsers() != null
                                    && each.getAssignUsers().contains(item.getName()));
            if (query.getUseUsers().isEmpty()) {
                isHaveUseUser = true;
            }
            if (query.getViewUsers().isEmpty()) {
                isHaveViewUser = true;
            }
            if (query.getExecSignUser().isEmpty()) {
                isHaveExecUser = true;
            }
            if (query.getAssignUser().isEmpty()) {
                isHaveAssignUser = true;
            }

            if (isHaveExecUser && isHaveAssignUser && isHaveUseUser && isHaveViewUser) {
                checkAuthoritList.add(each);
            }
        }

        return checkAuthoritList;
    }

    /**
     * 搜尋領單類型與簽核層級
     *
     * @param checkAuthoritys 核決權限list物件
     * @param query           核決權限查詢物件
     * @return
     */
    private List<CheckAuthorityVo> filterQueryModeAndFlowType(
            List<CheckAuthorityVo> checkAuthoritys, CheckAuthoritySearchQuery query) {
        List<CheckAuthorityVo> checkAuthoritList = Lists.newArrayList();
        for (CheckAuthorityVo each : checkAuthoritys) {

            boolean isHaveModeType = each.getExecMethod().contains(query.getExecMode());

            boolean isHaveFlowType = (each.getFlowType() != null && each.getFlowType().equals(query.getFlowType()));
            if (query.getFlowType() == null) {
                isHaveFlowType = true;
            }
            if (isHaveModeType && isHaveFlowType) {
                checkAuthoritList.add(each);
            }
        }

        return checkAuthoritList;
    }

    private String converterLegitimate(String title, String days) {
        StringBuffer sb = new StringBuffer();
        if (!StringUtils.isEmpty(days)) {
            sb.append("合法區間判斷天數: " + days + "</br>");
        }
        if (!StringUtils.isEmpty(title)) {
            sb.append("合法區間名稱定義: " + title + "</br>");
        }
        if (Strings.isNullOrEmpty(sb.toString())) {
            // 此為前端merge使用,空字串並不會merge
            sb.append("　");
        }
        return sb.toString();
    }

    private String converterStartEnd(String title, String days) {
        StringBuffer sb = new StringBuffer();
        if (!StringUtils.isEmpty(days)) {
            sb.append("起迄日合法天數: " + days + "</br>");
        }
        if (!StringUtils.isEmpty(title)) {
            sb.append("起訖日名稱定義: " + title + "</br>");
        }
        if (Strings.isNullOrEmpty(sb.toString())) {
            // 此為前端merge使用,空字串並不會merge
            sb.append("　");
        }
        return sb.toString();
    }

    private String getFlowTypeToString(FlowType flowType) {
        if (flowType != null) {
            return flowType.getDesc();
        }
        return "";
    }

    public List<SelectItem> initCategoryTagList(Integer loginDepSid, Integer loginCompSid) {
        return wCCategoryTagLogicComponent.findAll(Activation.ACTIVE, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getCategoryName()))
                .collect(Collectors.toList());
    }

    public List<SelectItem> initMenuTagList(String categorySid) {
        return wCMenuTagLogicComponent.getMenuTagSearchVO(categorySid, Activation.ACTIVE).stream()
                .map(each -> new SelectItem(each.getSid(), each.getName()))
                .collect(Collectors.toList());
    }

    public List<SelectItem> initSubTagList(String menuTagSid, Integer loginCompSid, Integer loginUserSid) {
        return wCTagLogicComponents.getWCTagActiveForMaster(menuTagSid, null, loginUserSid, loginCompSid).stream()
                .map(each -> new SelectItem(each.getSid(), each.getTagName()))
                .collect(Collectors.toList());
    }

    public List<SelectItem> initFlowTypeList() {
        List<SelectItem> flowTypes = Lists.newArrayList();
        for (FlowType flowType : FlowType.values()) {
            flowTypes.add(new SelectItem(flowType, flowType.getDesc()));
        }
        return flowTypes;
    }
}
