/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.work.common.utils.WkOrgUtils;

/**
 * 部門邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WCOrgLogic implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1535121902536908974L;

    private static WCOrgLogic instance;

    public static WCOrgLogic getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCOrgLogic.instance = this;
    }

    /**
     * 如CompanyCategory為部門（DEPARTMENT）且DepartmentLevel為組層級（THE_PANEL），則向上取到非組層級
     *
     * @param orgSid
     * @return basicOrg（至少為部級單位）
     */
    public Org getBaseOrg(Integer orgSid) {
        return WkOrgUtils.prepareBasicDep(orgSid, OrgLevel.MINISTERIAL);
    }
}
