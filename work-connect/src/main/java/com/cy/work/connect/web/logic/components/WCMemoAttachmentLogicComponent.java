/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.work.common.vo.basic.BasicAttachmentService;
import com.cy.work.connect.logic.manager.WCMemoAttachmentManager;
import com.cy.work.connect.logic.utils.AttachmentUtils;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.vo.WCMemoAttachment;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 備忘錄附件邏輯元件
 *
 * @author brain0925_liao
 */
@Component
public class WCMemoAttachmentLogicComponent
    implements BasicAttachmentService<AttachmentVO, WCMemoAttachment, String>,
    InitializingBean,
    Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8302527134260579903L;

    private static WCMemoAttachmentLogicComponent instance;
    @Autowired
    private WCMemoAttachmentManager wcMemoAttachmentManager;

    public static WCMemoAttachmentLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMemoAttachmentLogicComponent.instance = this;
    }

    @Override
    public File getServerSideFile(AttachmentVO attachment) {
        return AttachmentUtils.getServerSideFile(attachment, "wcMemoMaster");
    }

    /**
     * 取得附件物件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public WCMemoAttachment getWCMemoAttachmentBySid(String att_sid) {
        return wcMemoAttachmentManager.findBySid(att_sid);
    }

    /**
     * 取得附件介面物件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public AttachmentVO getAttachmentVOBySid(String att_sid) {
        WCMemoAttachment wa = wcMemoAttachmentManager.findBySid(att_sid);
        return wcMemoAttachmentManager.transWCMemoAttachmentToAttachmentVO(wa);
    }

    /**
     * 取得附件界面物件By 備忘錄Sid
     *
     * @param wcMemoSid 備忘錄Sid
     * @return
     */
    public List<AttachmentVO> getAttachmentsByWCMemoSid(String wcMemoSid) {
        return wcMemoAttachmentManager.getAttachmentsByWCMemoSid(wcMemoSid);
    }

    /**
     * 建立附件
     *
     * @param wcMemoSid     備忘錄Sid
     * @param wcMemoNo      備忘錄單號
     * @param fileName      附件名稱
     * @param create_uerSid 建立者Sid
     * @param departmetSid  建立者部門Sid
     * @return
     */
    public AttachmentVO createAttachment(
        String wcMemoSid,
        String wcMemoNo,
        String fileName,
        Integer create_uerSid,
        Integer departmetSid) {
        WCMemoAttachment create = new WCMemoAttachment();
        create.setStatus(Activation.ACTIVE);
        create.setFile_name(fileName);
        create.setWcMemoSid(wcMemoSid);
        create.setWcMemoNo(wcMemoNo);
        create.setDescription("");
        create.setDepartment_sid(departmetSid);
        WCMemoAttachment wc = wcMemoAttachmentManager.create(create, create_uerSid);
        if (wc == null) {
            return null;
        }
        return new AttachmentVO(
            wc.getSid(),
            wc.getFile_name(),
            wc.getDescription(),
            wc.getFile_name(),
            true,
            String.valueOf(wc.getCreate_usr_sid()));
    }

    /**
     * 更新附件資訊
     *
     * @param wcMemoSid      備忘錄Sid
     * @param wcMemoNo       備忘錄單號
     * @param description    描述
     * @param userSid        更新者Sid
     * @param attachment_sid 附件Sid
     */
    public void updateWCMemoMappingInfo(
        String wcMemoSid,
        String wcMemoNo,
        String description,
        Integer userSid,
        String attachment_sid) {
        wcMemoAttachmentManager.updateWCMemoMappingInfo(
            wcMemoSid, wcMemoNo, description, userSid, attachment_sid);
    }

    /**
     * 更新附件描述
     *
     * @param att_sid        附件Sid
     * @param desc           描述
     * @param update_userSid 更新者Sid
     * @return
     */
    public AttachmentVO updateAttachment(String att_sid, String desc, Integer update_userSid) {
        WCMemoAttachment wa = wcMemoAttachmentManager.findBySid(att_sid);
        if (wa == null) {
            return null;
        }
        wa.setDescription(desc);
        WCMemoAttachment wc = wcMemoAttachmentManager.update(wa, update_userSid);
        if (wc == null) {
            return null;
        }
        return new AttachmentVO(
            wc.getSid(),
            wc.getFile_name(),
            wc.getDescription(),
            wc.getFile_name(),
            true,
            String.valueOf(wc.getCreate_usr_sid()));
    }

    public void deleteAttachment(AttachmentVO attachmentVO, Integer loginUserSid, String memoSid) {
        wcMemoAttachmentManager.deleteAttachment(attachmentVO, loginUserSid, memoSid);
    }

    @Override
    public AttachmentVO findBySid(String att_sid) {
        WCMemoAttachment wa = wcMemoAttachmentManager.findBySid(att_sid);
        return wcMemoAttachmentManager.transWCMemoAttachmentToAttachmentVO(wa);
    }

    @Override
    public List<AttachmentVO> findAttachsByLazy(WCMemoAttachment e) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AttachmentVO> findAttachs(WCMemoAttachment e) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDirectoryName() {
        return "WCAttachment";
    }

    @Override
    public String getAttachPath() {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AttachmentVO createEmptyAttachment(String fileName, Integer executor, Integer dep) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AttachmentVO updateAttach(AttachmentVO attach) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAttach(AttachmentVO attach) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAttach(List<AttachmentVO> attachs) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String depAndUserName(AttachmentVO attachment) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void linkRelation(List<AttachmentVO> attachments, WCMemoAttachment entity,
        Integer login) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void linkRelation(AttachmentVO ra, WCMemoAttachment entity, Integer login) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeStatusToInActive(
        List<AttachmentVO> attachments,
        AttachmentVO attachment,
        WCMemoAttachment entity,
        Integer login) {
        throw new UnsupportedOperationException(
            "Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }
}
