package com.cy.work.connect.web.view.components.qstree;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.web.listener.OrgUserTreeCallBack;
import com.cy.work.connect.web.util.pf.PomPropertiesLoader;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.model.TreeNode;

/**
 * @author allen
 */
@Slf4j
public abstract class AbstractQucikSelectTreeComponentMBean {

    @Getter
    private final String memoMessage =
        "※&nbsp;<span style='font-weight: bold;text-decoration:underline;'>雙擊&nbsp;</span>清單項目選取 (雙擊項目後移至右邊或左邊)";
    /**
     * 訊息
     */
    @Getter
    public String message = "";
    /**
     * 組織人員選擇樹元件 CallBack
     */
    protected OrgUserTreeCallBack callback;
    /**
     * 樹節點結構資料
     */
    @Getter
    protected TreeNode rootTreeNode;
    /**
     * 選擇的節點
     */
    @Getter
    @Setter
    protected TreeNode selectedNode;
    /**
     *
     */
    @Getter
    protected List<?> selectedList;
    /**
     * 是否顯示停用
     */
    @Getter
    @Setter
    protected Boolean queryShowInactiveNode = false;
    /**
     *
     */
    protected Map<String, AbstractTreeComponentNodeData> allNodeDataRowKeyMap;
    protected Map<String, AbstractTreeComponentNodeData> allNodeDataSidMap;
    /**
     *
     */
    @Getter
    protected String nodeDataJsonString = "";
    /**
     *
     */
    @Getter
    @Setter
    protected String selectedNodeDataJsonString = "";
    /**
     *
     */
    @Getter
    @Setter
    protected boolean selectedListMode = false;
    /**
     * 取得已經選取的資料 : 由外部實做此方法
     */
    protected Function<Map<String, Object>, List<?>> getSelectedDataList;
    /**
     * 組件呼叫 getSelectedDataList 時, 帶入的值
     */
    protected Map<String, Object> conditionForGetSelectedDataListFunction;
    /**
     * 是否已經初始化
     */
    @Getter
    @Setter
    private String initFlag = "N";

    /**
     * 切換顯示模式(正常/停用)
     */
    public abstract void switchShowActiveOrinactiveMode();

    public abstract List<?> getSelectedDataList();

    /**
     * 在初始化前執行
     */
    protected void brforeInitialize(
        Function<Map<String, Object>, List<?>> getSelectedDataList,
        Map<String, Object> conditionForGetSelectedDataListFunction) {

        // 記錄傳入參數
        this.getSelectedDataList = getSelectedDataList;
        this.conditionForGetSelectedDataListFunction = conditionForGetSelectedDataListFunction;

        // 預設不【顯示停用】
        this.queryShowInactiveNode = false;
        //
        this.nodeDataJsonString = "{}";
        this.selectedNodeDataJsonString = "[]";
        //
        this.setInitFlag("N");
    }

    /**
     * 在初始化前執行
     */
    protected void brforeInitialize(
        Function<Map<String, Object>, List<?>> getSelectedDataList,
        Map<String, Object> conditionForGetSelectedDataListFunction,
        OrgUserTreeCallBack callback) {

        // 記錄傳入參數
        this.getSelectedDataList = getSelectedDataList;
        this.conditionForGetSelectedDataListFunction = conditionForGetSelectedDataListFunction;
        this.callback = callback;

        // 預設不【顯示停用】
        this.queryShowInactiveNode = false;
        //
        this.nodeDataJsonString = "{}";
        this.selectedNodeDataJsonString = "[]";
        //
        this.setInitFlag("N");
    }

    /**
     * 初始化後執行
     */
    protected void afterInitialize() {
        // ====================================
        // 註記初始化已完成
        // ====================================
        this.setInitFlag("Y");
    }

    protected void afterbuildTreeData() {
        // ====================================
        // 整理資料
        // ====================================
        // 將TreeNode 轉為 list
        List<TreeNode> allNodelist = Lists.newArrayList();
        this.treeNodeToList(allNodelist, this.rootTreeNode);

        // 轉為 map 形式
        this.allNodeDataRowKeyMap = Maps.newHashMap();
        this.allNodeDataSidMap = Maps.newHashMap();

        for (TreeNode eachNode : allNodelist) {
            if (eachNode.getData() == null) {
                continue;
            }
            AbstractTreeComponentNodeData nodeData = (AbstractTreeComponentNodeData) eachNode.getData();
            nodeData.setRowKey(eachNode.getRowKey());
            if (this.allNodeDataRowKeyMap.containsKey(eachNode.getRowKey())) {
                log.info(
                    "節點重複["
                        + eachNode.getRowKey()
                        + ""
                        + "原節點["
                        + this.allNodeDataRowKeyMap.get(eachNode.getRowKey()).getOuName()
                        + "]"
                        + "新節點["
                        + nodeData.getOuName()
                        + "]");
            }

            this.allNodeDataRowKeyMap.put(eachNode.getRowKey(), nodeData);

            this.allNodeDataSidMap.put(this.getAllNodeDataSidMapKey(nodeData), nodeData);
        }
        // 轉JSON以供 client 端調用
        this.nodeDataJsonString = new Gson().toJson(this.allNodeDataRowKeyMap);
    }

    public String getAllNodeDataSidMapKey(AbstractTreeComponentNodeData nodeData) {
        return nodeData.isDataNode() + nodeData.getSid();
    }

    /**
     * @param nodeData
     * @return
     */
    public String getNodeDisplayContext(AbstractTreeComponentNodeData nodeData) {
        if (nodeData == null) {
            return "";
        }

        // 取得顯示名稱字串
        String displayContext = this.getNodeName(nodeData);
        // 為user 節點時, 前方加入 icon
        if (nodeData.isDataNode()) {
            displayContext =
                ""
                    + "<span class=\"quick-select-tree-data-node-highlight\">"
                    + "<i class=\"fa\">"
                    + "</i>&nbsp;&nbsp;"
                    + displayContext
                    + "</span>";
        }

        return displayContext;
    }

    /**
     * 取得節點名稱
     *
     * @param nodeData
     * @return
     */
    public String getNodeName(AbstractTreeComponentNodeData nodeData) {

        String displayName = "<span class='ui-treenode-name'></span>";

        // 取得 node data
        if (nodeData == null) {
            return displayName;
        }

        // 取得節點名稱
        displayName = WkStringUtils.removeCtrlChr(WkStringUtils.safeTrim(nodeData.getName()));

        // 加上 class 標示 (client script 用)
        return "<span class='ui-treenode-name'>" + displayName + "</span>";
    }

    /**
     * for 清除 client 端 js 和 css 的快取用
     *
     * @return
     */
    public String getJsAndCssVersion() {
        // return new Date().getTime() + "";
        try {
            return PomPropertiesLoader.getProperty("version");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new Date().getTime() + "";
        }
    }

    /**
     * 將所有樹狀節點加入list中
     *
     * @param list
     * @param root
     */
    protected void treeNodeToList(List<TreeNode> list, TreeNode root) {
        if (root == null || root.getChildren() == null) {
            return;
        }
        root.getChildren()
            .forEach(
                each -> {
                    list.add(each);
                    this.treeNodeToList(list, each);
                });
    }

    @Data
    public abstract class AbstractTreeComponentNodeData implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -4879553686669942585L;
        /**
         * 名稱
         */
        protected String name;
        /**
         * SID
         */
        protected String sid;
        /**
         * 是否為 user 節點
         */
        protected boolean dataNode = false;
        /**
         * tree node key
         */
        protected String rowKey;
        /**
         * 是否啟用
         */
        protected boolean active = false;
        /**
         * 公司名稱 + user 名稱 (顯示用)
         */
        protected String ouName;
    }
}
