/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.util;

/**
 * @author saul_chen
 */
public class StringsUtil {

    /**
     * 取代特殊符號
     *
     * @param value
     * @return
     */
    public static String replaceSpecialSymbol(String value) {
        return value.replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\\\\", "\"");
    }
}
