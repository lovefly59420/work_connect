/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.config;

import java.util.List;

import javax.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.converter.SplitListStrConverter;

/**
 * @author brain0925_liao
 */
@Slf4j
@Data
@Component
public class WCProperties {

    @Value("${work-connect.db.driverClassName}")
    private String className;

    @Value("${work-connect.db.url}")
    private String dbUrl;

    @Value("${work-connect.username}")
    private String dbUserName;

    @Value("${work-connect.db.password}")
    private String dbPassword;

    @Value("${work-connect.db.dialect.hibernate}")
    private String dialect;

    @Value("${work-connect.db.hibernate.show_sql}")
    private boolean show_sql;

    @Value("${work-connect.db.hibernate.format_sql}")
    private boolean format_sql;

    @Value("${work-connect.db.maxPoolSize}")
    private int maxPoolSize;

    @Value("${work-connect.db.minimumIdle}")
    private int minimumIdle;

    @Value("${work-connect.db.maxStatements}")
    private int maxStatements;

    @Value("${work-connect.db.idleTimeout}")
    private int idleTimeout;

    @Value("${work-connect.db.maxLifetime}")
    private int maxLifetime;

    @Value("${work-connect.db.cachePrepStmts}")
    private String cachePrepStmts;

    @Value("${work-connect.db.useServerPrepStmts}")
    private String useServerPrepStmts;

    @Value("${work-connect.db.prepStmtCacheSize}")
    private String prepStmtCacheSize;

    @Value("${work-connect.db.prepStmtCacheSqlLimit}")
    private String prepStmtCacheSqlLimit;

    @Value("${work-connect.superAdmin.UserIDs}")
    private String superAdminUserIDs;

    public boolean isSuperAdmin(String loginUserID) {
        if (WkStringUtils.isEmpty(this.superAdminUserIDs)) {
            return false;
        }
        List<String> exportUserIDs = new SplitListStrConverter().convertToEntityAttribute(
                WkStringUtils.safeTrim(this.superAdminUserIDs));
        return exportUserIDs.contains(SecurityFacade.getUserId());
    }

    @PostConstruct
    void init() {
        log.info("WCProperties startup , display default value ..");
        log.info("className = " + className);
        log.info("dbUrl = " + dbUrl);
        //log.info("dbUserName = " + dbUserName.substring(0, 2) + ".....");
        //log.info("dbPassword = " + dbPassword.substring(0, 2) + ".....");
        log.info("dialect = " + dialect);
        log.info("show_sql = " + show_sql);
    }
}
