/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.MenuTagHelper;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserDepTo;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.enums.FlowType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.web.view.vo.SubTagEditVO;
import com.cy.work.connect.web.view.vo.TagSearchVO;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 小類(執行項目)邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WCTagLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2024966074563100543L;

    private static WCTagLogicComponents instance;
    @Autowired
    private WCTagManager wCTagManager;
    @Autowired
    private WCConfigTempletManager wcConfigTempletManager;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private MenuTagHelper menuTagHelper;

    public static WCTagLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTagLogicComponents.instance = this;
    }

    /**
     * 取得小類(執行項目) By 中類(單據名稱)Sid 及 狀態
     *
     * @param wcMenuTagSid 中類(單據名稱)Sid
     * @param status       狀態
     * @return
     */
    public List<WCTag> getWCTagByMenuSid(String wcMenuTagSid, Activation status) {
        List<WCTag> results = Lists.newArrayList();
        if (wcMenuTagSid == null) {
            return results;
        }
        try {
            results = wCTagManager.getWCTagForManager(wcMenuTagSid, status);
        } catch (Exception ex) {
            log.warn("getWCTagByMenuSid ERROR", ex);
        }
        return results;
    }

    /**
     * 取得執行項目List For 主檔編輯使用
     *
     * @param wcMenuTagSid     中類Sid
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginUserSid     登入者 sid
     * @param loginCompSid     登入公司
     * @return
     */
    public List<WCTag> getWCTagActiveForMaster(
            String wcMenuTagSid,
            final Set<Integer> loginManagedDeps,
            Integer loginUserSid,
            Integer loginCompSid) {
        return wCTagManager.getWCTagActiveForMaster(wcMenuTagSid, loginManagedDeps, loginUserSid, loginCompSid);
    }

    /**
     * 取得小類(執行項目) by 小類(執行項目)Sid清單
     *
     * @param sids 小類(執行項目)Sid清單
     * @return
     */
    public List<WCTag> findBySid(List<String> sids) {
        return sids.stream().map(each -> this.getWCTagBySid(each)).collect(Collectors.toList());
    }

    public WCTag getWCTagBySid(String sid) {
        return wCTagManager.getWCTagBySid(sid);
    }

    /**
     * 取得小類(執行項目)清單
     *
     * @return
     */
    public List<WCTag> getWCTags() {
        try {
            List<WCTag> result = Lists.newArrayList();
            return result;
        } catch (Exception e) {
            log.warn("getWCTags Error : " + e, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得小類(執行項目)編輯物件 By 小類(執行項目)Sid
     *
     * @param sid 小類(執行項目)Sid
     * @return
     */
    public SubTagEditVO getSubTagEditVO(String sid) {
        WCTag wcTag = wCTagManager.getWCTagBySid(sid);
        SubTagEditVO subTagEditVO = new SubTagEditVO();
        subTagEditVO.setSid(wcTag.getSid());
        subTagEditVO.setName(wcTag.getTagName());
        subTagEditVO.setStatus(wcTag.getStatus().name());
        subTagEditVO.setMemo(wcTag.getMemo());
        subTagEditVO.setSeq(String.valueOf(wcTag.getSeq()));
        subTagEditVO.setDefaultContentCss(wcTag.getDefaultContentCss());
        subTagEditVO.setDefaultMemoCss(wcTag.getDefaultMemoCss());
        subTagEditVO.setReqFlowType(
                (wcTag.getReqFlowType() != null) ? wcTag.getReqFlowType().name() : "");
        subTagEditVO.setLegitimateRangeCheck(wcTag.isLegitimateRangeCheck());
        subTagEditVO.setLegitimateRangeDays(wcTag.getLegitimateRangeDays());
        subTagEditVO.setLegitimateRangeTitle(wcTag.getLegitimateRangeTitle());
        subTagEditVO.setStartEndCheck(wcTag.isStartEndCheck());
        subTagEditVO.setStartEndDays(wcTag.getStartEndDays());
        subTagEditVO.setStartEndTitle(wcTag.getStartEndTitle());
        subTagEditVO.setEndRuleDays(wcTag.getEndRuleDays());
        subTagEditVO.setViewContainFollowing(wcTag.isViewContainFollowing());
        subTagEditVO.setUserContainFollowing(wcTag.isUserContainFollowing());
        subTagEditVO.setOnlyForUseDepManager(wcTag.isOnlyForUseDepManager());
        subTagEditVO.setUploadFile(wcTag.getUploadFile());

        // 可使用單位
        if (wcTag.getCanUserDep() != null && wcTag.getCanUserDep().getUserDepTos() != null) {
            wcTag
                    .getCanUserDep()
                    .getUserDepTos()
                    .forEach(
                            item -> {
                                try {
                                    subTagEditVO
                                            .getCanUserDeps()
                                            .add(WkOrgCache.getInstance()
                                                    .findBySid(Integer.valueOf(item.getDepSid())));
                                } catch (Exception e) {
                                    log.warn("getSubTagEditVO ERROR", e);
                                }
                            });
        }
        // 可使用單位
        subTagEditVO.setCanUserDepTempletSid(wcTag.getCanUserDepTempletSid());

        // 預設可閱部門：套用模版 SID
        if (wcTag.getUseDep() != null && wcTag.getUseDep().getUserDepTos() != null) {
            wcTag
                    .getUseDep()
                    .getUserDepTos()
                    .forEach(
                            item -> {
                                try {
                                    subTagEditVO
                                            .getOrgs()
                                            .add(WkOrgCache.getInstance()
                                                    .findBySid(Integer.valueOf(item.getDepSid())));
                                } catch (Exception e) {
                                    log.warn("getSubTagEditVO ERROR", e);
                                }
                            });
        }
        // 預設可閱部門：套用模版 SID
        subTagEditVO.setUseDepTempletSid(wcTag.getUseDepTempletSid());

        // 預設可閱人員
        if (wcTag.getUseUser() != null && wcTag.getUseUser().getUserTos() != null) {
            wcTag
                    .getUseUser()
                    .getUserTos()
                    .forEach(
                            item -> {
                                try {
                                    subTagEditVO.getUsers()
                                            .add(WkUserCache.getInstance().findBySid(item.getSid()));
                                } catch (Exception e) {
                                    log.warn("getSubTagEditVO ERROR", e);
                                }
                            });
        }

        // 顯示分類
        subTagEditVO.setTagType(wcTag.getTagType());

        // Tag 備註說明
        subTagEditVO.setTagDesc(wcTag.getTagDesc());

        return subTagEditVO;
    }

    /**
     * 取得小類(執行項目)搜尋物件 By 小類(執行項目)Sid 及 中類(單據名稱)Sid
     *
     * @param tagSid          小類(執行項目)Sid
     * @param menuSid         中類(單據名稱)Sid
     * @param hideInactiveTag 不顯示停用小類
     * @return
     */
    /**
     * @param tagSid
     * @param menuSid
     * @return
     */
    public List<TagSearchVO> getTagSearch(String tagSid, String menuSid, boolean hideInactiveTag) {
        List<TagSearchVO> tagSearchs = Lists.newArrayList();

        // 不顯示停用小類
        Activation status = null;
        if (WkStringUtils.isEmpty(tagSid)
                && hideInactiveTag) {
            status = Activation.ACTIVE;
        }

        // 依據中分類, 取得其下所有的執行項目
        List<WCTag> wcTags = wCTagManager.getWCTagForManager(menuSid, status);

        if (WkStringUtils.isEmpty(wcTags)) {
            return tagSearchs;
        }

        for (WCTag wcTag : wcTags) {

            if (!Strings.isNullOrEmpty(tagSid) && !wcTag.getSid().equals(tagSid)) {
                continue;
            }
            String statusStr = (Activation.ACTIVE.equals(wcTag.getStatus())) ? "正常" : "停用";

            // 準備執行單位名稱
            String[] execDepsName = this.execDepSettingManager.getExecDepSettingDepsName(
                    SecurityFacade.getCompanyId(), wcTag.getSid());

            TagSearchVO tagSearchVO = new TagSearchVO(
                    wcTag.getSid(),
                    ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wcTag.getCreateDate()),
                    WkUserUtils.findNameBySid(wcTag.getCreateUser()),
                    wcTag.getTagName(),
                    wcTag.getReqFlowType(),
                    statusStr,
                    ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wcTag.getUpdateDate()),
                    WkUserUtils.findNameBySid(wcTag.getUpdateUser()),
                    String.valueOf(0),
                    wcTag.getCanUserDep(),
                    wcTag.getCanUserDepTempletSid(),
                    wcTag.isUserContainFollowing(),
                    wcTag.getUseDep(),
                    wcTag.getUseDepTempletSid(),
                    wcTag.isViewContainFollowing(),
                    execDepsName[0],
                    execDepsName[1],
                    wcTag.getUseUser(),
                    wcTag.getTagType());

            tagSearchVO.setStatusObject(wcTag.getStatus());
            tagSearchs.add(tagSearchVO);
        }

        return tagSearchs;
    }

    /**
     * 取得小類(執行項目)搜尋物件 By 小類(執行項目)Sid
     *
     * @param tagSid 小類(執行項目)Sid
     * @return
     */
    public TagSearchVO getTagSearchByTagSid(String tagSid) {
        TagSearchVO tagSearch = null;

        // 依據中分類, 取得其下所有的執行項目
        WCTag wcTag = wCTagManager.getWCTagBySid(tagSid);

        if (WkStringUtils.isEmpty(wcTag)) {
            return tagSearch;
        }

        String statusStr = (Activation.ACTIVE.equals(wcTag.getStatus())) ? "顯示" : "不顯示";

        // 準備執行單位名稱
        String[] execDepsName = this.execDepSettingManager.getExecDepSettingDepsName(
                SecurityFacade.getCompanyId(), wcTag.getSid());

        tagSearch = new TagSearchVO(
                wcTag.getSid(),
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wcTag.getCreateDate()),
                WkUserUtils.findNameBySid(wcTag.getCreateUser()),
                wcTag.getTagName(),
                wcTag.getReqFlowType(),
                statusStr,
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wcTag.getUpdateDate()),
                WkUserUtils.findNameBySid(wcTag.getUpdateUser()),
                String.valueOf(0),
                wcTag.getCanUserDep(),
                wcTag.getCanUserDepTempletSid(),
                wcTag.isUserContainFollowing(),
                wcTag.getUseDep(),
                wcTag.getUseDepTempletSid(),
                wcTag.isViewContainFollowing(),
                execDepsName[0],
                execDepsName[1],
                wcTag.getUseUser(),
                wcTag.getTagType());

        tagSearch.setStatusObject(wcTag.getStatus());

        return tagSearch;
    }

    public Integer getMaxTagSeq() {
        Integer maxSeq = 0;
        // for (WCTag wCTag : wCTagManager.getWCTags()) {
        //// if (wCTag.getSeq() > maxSeq) {
        //// maxSeq = wCTag.getSeq();
        //// }
        // }
        return maxSeq;
    }

    /**
     * 儲存小類(執行項目)
     *
     * @param sid                    小類(執行項目)Sid
     * @param name                   名稱
     * @param status                 狀態
     * @param memo                   備註
     * @param loginUserSid           登入者Sid
     * @param menuSid                中類(單據名稱)Sid
     * @param reqFlowType            簽核層級
     * @param defaultContentCss      預設勾選項目的內容(css格式)
     * @param defaultMemoCss         預設勾選項目的備註(CSS格式)
     * @param orgs                   預設可閱部門
     * @param users                  預設可閱人員
     * @param legitimateRangeCheck   其他判斷1-是否有合法區間判斷
     * @param legitimateRangeDays    其他判斷1-合法區間判斷天數
     * @param legitimateRangeTitle   其他判斷1-合法區間名稱定義
     * @param startEndCheck          其他判斷2-是否有起迄日判斷
     * @param startEndDays           其他判斷2-起迄日合法天數
     * @param startEndTitle          其他判斷2-起迄日名稱定義
     * @param endRuleDays            其他判斷2-結束日規範判斷(天)
     * @param canUserOrgs            可使用單位
     * @param isViewContainFollowing 含以下(預設可閱部門)
     * @param isUserContainFollowing 含以下(可使用單位)
     * @param useDepTempletSid       預設可閱部門模版Sid
     * @param canUserDepTempletSid   可使用單位模版Sid
     * @param tagType                顯示分類
     * @param tagDesc                小類(執行項目)備註說明
     */
    public void saveTag(
            String sid,
            String name,
            String status,
            String memo,
            Integer loginUserSid,
            String menuSid,
            String reqFlowType,
            String defaultContentCss,
            String defaultMemoCss,
            List<Org> orgs,
            List<User> users,
            boolean legitimateRangeCheck,
            String legitimateRangeDays,
            String legitimateRangeTitle,
            boolean startEndCheck,
            String startEndDays,
            String startEndTitle,
            String endRuleDays,
            List<Org> canUserOrgs,
            boolean isViewContainFollowing,
            boolean isUserContainFollowing,
            boolean isOnlyForUseDepManager,
            Long useDepTempletSid,
            Long canUserDepTempletSid,
            String tagType,
            String tagDesc,
            boolean isUploadFile) {
        if (Strings.isNullOrEmpty(name)) {
            Preconditions.checkState(false, "執行項目為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(status)) {
            Preconditions.checkState(false, "狀態為必要輸入欄位，請確認！！");
        }
        if (Strings.isNullOrEmpty(reqFlowType)) {
            Preconditions.checkState(false, "需求流程為必要輸入欄位，請確認！！");
        }

        if (legitimateRangeCheck) {
            if (Strings.isNullOrEmpty(legitimateRangeDays)) {
                Preconditions.checkState(false, "合法區間判斷天數為必要輸入欄位，請確認！！");
            }
            if (Strings.isNullOrEmpty(legitimateRangeTitle)) {
                Preconditions.checkState(false, "合法區間名稱定義為必要輸入欄位，請確認！！");
            }

            try {
                Integer.valueOf(legitimateRangeDays);
            } catch (Exception e) {
                log.warn("saveCategoryTag ERROR", e);
                Preconditions.checkState(false, "合法區間判斷天數必須為數字，請確認！！");
            }
        }

        if (startEndCheck) {
            if (Strings.isNullOrEmpty(startEndDays)) {
                Preconditions.checkState(false, "起迄日合法天數為必要輸入欄位，請確認！！");
            }
            if (Strings.isNullOrEmpty(startEndTitle)) {
                Preconditions.checkState(false, "起訖日名稱定義為必要輸入欄位，請確認！！");
            }

            try {
                Integer.valueOf(startEndDays);
            } catch (Exception e) {
                log.warn("saveCategoryTag ERROR", e);
                Preconditions.checkState(false, "起迄日合法天數必須為數字，請確認！！");
            }

            try {
                Integer.valueOf(endRuleDays);
            } catch (Exception e) {
                log.warn("saveCategoryTag ERROR", e);
                Preconditions.checkState(false, "結束日規範判斷(天)必須為數字，請確認！！");
            }
        }

        for (WCTag item : wCTagManager.getWCTagForManager(menuSid, Activation.ACTIVE)) {
            if (!item.getSid().equals(sid)) {
                if (item.getTagName().equals(name)) {
                    Preconditions.checkState(false, "執行項目有重覆，請重新輸入！！");
                }
            }
        }

        WCTag selWCTag = null;
        if (Strings.isNullOrEmpty(sid)) {
            selWCTag = new WCTag();
            selWCTag.setWcMenuTagSid(menuSid);
        } else {
            selWCTag = wCTagManager.getWCTagBySid(sid);
        }
        // 使用狀態
        selWCTag.setStatus(Activation.valueOf(status));

        // 執行項目名稱
        selWCTag.setTagName(name);

        // 備註
        selWCTag.setMemo(memo);
        // 申請流程類型
        selWCTag.setReqFlowType(FlowType.valueOf(reqFlowType));

        // 顯示分類
        if (tagType != null) {
            // 前後去空白
            tagType = tagType.trim();
            if (tagType.isEmpty()) {
                tagType = null;
            }
        }
        selWCTag.setTagType(tagType);

        // Tag 備註說明
        selWCTag.setTagDesc(tagDesc);

        // 存檔判斷附加檔案
        selWCTag.setUploadFile(isUploadFile);

        // 預設內容
        selWCTag.setDefaultContentCss(defaultContentCss);
        selWCTag.setDefaultContent(defaultContentCss == null ? null : Jsoup.clean(defaultContentCss, Safelist.basicWithImages()));

        // 預設備註
        selWCTag.setDefaultMemoCss(defaultMemoCss);

        selWCTag.setLegitimateRangeCheck(legitimateRangeCheck);
        selWCTag.setLegitimateRangeDays(legitimateRangeDays);
        selWCTag.setLegitimateRangeTitle(legitimateRangeTitle);

        // 其他判斷2
        // 起訖日
        selWCTag.setStartEndCheck(startEndCheck);
        // 起迄日合法天數
        selWCTag.setStartEndDays(startEndDays);
        // 起訖日名稱定義
        selWCTag.setStartEndTitle(startEndTitle);
        // 結束日規範判斷(天)
        selWCTag.setEndRuleDays(endRuleDays);

        // ====================================
        // 可使用單位
        // ====================================
        UserDep canUseDeps = new UserDep();

        if (WkStringUtils.notEmpty(canUserOrgs)) {

            // 轉為sid
            List<Integer> depSids = canUserOrgs.stream()
                    .map(each -> {
                        if (each != null && each.getSid() != null) {
                            return each.getSid();
                        }
                        return Integer.MIN_VALUE;
                    })
                    .collect(Collectors.toList());

            // 資料精簡化, 如果有勾（含以下）則只儲存最頂端的單位
            if (isUserContainFollowing) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }

            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            depSids.forEach(
                    depSid -> {
                        UserDepTo udt = new UserDepTo();
                        udt.setDepSid(String.valueOf(depSid));
                        canUseDeps.getUserDepTos().add(udt);
                    });
        }

        selWCTag.setCanUserDep(canUseDeps);

        // 可使用單位 - 含以下
        selWCTag.setUserContainFollowing(isUserContainFollowing);
        // 可使用單位-是否僅主管可使用
        selWCTag.setOnlyForUseDepManager(isOnlyForUseDepManager);

        // 可使用單位 : 模版ID
        selWCTag.setCanUserDepTempletSid(canUserDepTempletSid);
        // 模版不為空時，清空選項
        if (canUserDepTempletSid != null) {
            selWCTag.setUserContainFollowing(false);
            selWCTag.setOnlyForUseDepManager(false);
        }

        // ====================================
        // 預設可閱部門
        // ====================================
        UserDep ud = new UserDep();
        if (WkStringUtils.notEmpty(orgs)) {

            // 轉為sid
            List<Integer> depSids = orgs.stream()
                    .map(each -> {
                        if (each != null && each.getSid() != null) {
                            return each.getSid();
                        }
                        return Integer.MIN_VALUE;
                    })
                    .collect(Collectors.toList());

            // 資料精簡化, 如果有勾（含以下）則只儲存最頂端的單位
            if (isViewContainFollowing) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }
            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            depSids.forEach(
                    depSid -> {
                        UserDepTo udt = new UserDepTo();
                        udt.setDepSid(String.valueOf(depSid));
                        ud.getUserDepTos().add(udt);
                    });
        }

        selWCTag.setUseDep(ud);
        // 預設可閱部門 - 含以下
        selWCTag.setViewContainFollowing(isViewContainFollowing);
        // 預設可閱部門 - 模版SID
        selWCTag.setUseDepTempletSid(useDepTempletSid);

        // ====================================
        // 預設可閱人員
        // ====================================
        UsersTo useUser = new UsersTo();
        List<UserTo> useUserTos = Lists.newArrayList();
        if (users != null) {
            users.forEach(
                    item -> {
                        UserTo udt = new UserTo();
                        udt.setSid(item.getSid());
                        useUserTos.add(udt);
                    });
        }
        useUser.setUserTos(useUserTos);

        selWCTag.setUseUser(useUser);

        // ====================================
        // Insert or Update
        // ====================================
        if (Strings.isNullOrEmpty(sid)) {
            wCTagManager.createWCTag(selWCTag, loginUserSid);
        } else {
            wCTagManager.updateWCTag(selWCTag, loginUserSid);
        }
    }

    /**
     * 取得可閱人員 By 小類(執行項目)Sid
     *
     * @param tagSid 小類(執行項目)Sid
     * @return
     */
    public List<UserViewVO> getViewUsersByTagSid(String tagSid) {

        WCTag wcTag = wCTagManager.getWCTagBySid(tagSid);
        if (wcTag == null) {
            return Lists.newArrayList();
        }
        return wcTag.getUseUserSids().stream()
                .map(sid -> new UserViewVO(sid))
                .collect(Collectors.toList());
    }

    /**
     * 取得部門設定-模版項目
     *
     * @param templetType 模版類別
     * @return
     */
    public List<WCConfigTempletVo> getDepsTempletItems(ConfigTempletType templetType) {
        // 依據模版類型查詢
        return this.wcConfigTempletManager.queryByCondition(
                SecurityFacade.getUserSid(),
                templetType,
                null,
                Optional.ofNullable(WkOrgCache.getInstance().findById(SecurityFacade.getCompanyId()))
                        .map(Org::getSid)
                        .orElseGet(() -> null));
    }

    /**
     * 依據傳入順序排序
     *
     * @param vos 小類(執行項目)搜尋物件清單
     */
    public void sort(List<TagSearchVO> vos) {
        if (WkStringUtils.isEmpty(vos)) {
            return;
        }

        List<String> sids = vos.stream().map(TagSearchVO::getSid).collect(Collectors.toList());

        this.wCTagManager.updateSortSeqByListIndex(sids);
    }

    /**
     * 顯示分類清單Item - 依中類(單據名稱)Sid查詢
     *
     * @param menuTagSid 中類(單據名稱)Sid
     * @return
     */
    public List<SelectItem> getTagTypeItems(String menuTagSid) {
        List<SelectItem> result = this.wCTagManager.findDistinctTagTypeByMenuTagSid(menuTagSid).stream()
                .map(each -> new SelectItem(each, each))
                .collect(Collectors.toList());

        if (result == null) {
            return Lists.newArrayList();
        }

        return result;
    }

    /**
     * 回傳可使用的小類(執行項目)Sid清單
     *
     * @param tags             小類(執行項目)Sid清單
     * @param loginManagedDeps 登入單位Sid & 管理部門Sid
     * @param loginUserSid     登入者Sid
     * @param loginCompSid     登入公司Sid
     * @return
     */
    public List<String> checkCanUseTag(
            List<String> tags,
            Set<Integer> loginManagedDeps,
            Integer loginUserSid,
            Integer loginCompSid) {

        // ====================================
        // 取得所有部門設定模版
        // ====================================
        List<WCConfigTemplet> wcConfigTemplets = wcConfigTempletManager.findAll();

        // ====================================
        // 收集可用的執行項目
        // ====================================
        List<Org> allCompActiveDeps = WkOrgCache.getInstance().findAllActiveDepByCompSid(loginCompSid);
        List<String> canUseTags = wCTagManager.getWCTagsBySidList(tags)
                .stream().filter(x -> Activation.ACTIVE.equals(x.getStatus()))
                .map(WCTag::getSid).collect(Collectors.toList());

        List<String> results = canUseTags.stream()
                .map(each -> wCTagManager.getWCTagBySid(each))
                .filter(each -> menuTagHelper.checkCanShowTagItem(
                        each, wcConfigTemplets, loginManagedDeps, loginUserSid, loginCompSid, allCompActiveDeps))
                .map(each -> each.getSid())
                .collect(Collectors.toList());

        return results;
    }
}
