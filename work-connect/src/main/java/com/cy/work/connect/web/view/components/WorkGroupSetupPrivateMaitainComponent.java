/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.model.DualListModel;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.connect.web.common.DepTreeComponent;
import com.cy.work.connect.web.common.MultipleDepTreeManager;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.logic.components.WCOrgLogic;
import com.cy.work.connect.web.logic.components.WCUserLogic;
import com.cy.work.connect.web.logic.components.WorkGroupSetupPrivateLogicComponents;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.cy.work.connect.web.view.vo.UserViewVO;
import com.cy.work.connect.web.view.vo.WorkGroupVO;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
public class WorkGroupSetupPrivateMaitainComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8782819873695014825L;
    private final Integer loginUserSid;
    private final ReLoadCallBack reLoadCallBack;
    private final DepTreeComponent depTreeComponent;
    @Getter
    private final String workGroup_dep;
    @Getter
    private final String workGroup_deptUnitDlg;
    @Getter
    private final String groupMaintainComponent_pickPanel;
    @Getter
    private final String group_setup_private_setting_dlg_wv;
    @Getter
    @Setter
    private String statusID;
    @Getter
    @Setter
    private String groupName;
    @Getter
    @Setter
    private String note;
    @Getter
    @Setter
    private String filterNameValue;
    private String groupID;
    @Getter
    @Setter
    private DualListModel<UserViewVO> pickUsers;
    private LinkedHashSet<UserViewVO> leftViewVO;
    private LinkedHashSet<UserViewVO> allCompUser;
    private List<Org> selectOrgs;

    public WorkGroupSetupPrivateMaitainComponent(
        String group_setup_private_setting_dlg_wv,
        String groupMaintainComponent_pickPanel,
        String workGroup_deptUnitDlg,
        String workGroup_dep,
        Integer loginUserSid,
        ReLoadCallBack reLoadCallBack) {
        this.loginUserSid = loginUserSid;
        this.reLoadCallBack = reLoadCallBack;
        this.workGroup_dep = workGroup_dep;
        this.groupMaintainComponent_pickPanel = groupMaintainComponent_pickPanel;
        this.group_setup_private_setting_dlg_wv = group_setup_private_setting_dlg_wv;
        this.workGroup_deptUnitDlg = workGroup_deptUnitDlg;
        depTreeComponent = new DepTreeComponent();
        initOrgTree();
        initPickUsers();
    }

    private void initPickUsers() {
        pickUsers = new DualListModel<>();
    }

    public void clear() {
        initPickUsers();
        initOrgTree();
        if (leftViewVO != null) {
            leftViewVO.clear();
            leftViewVO = null;
        }
        allCompUser = null;
        DisplayController.getInstance().update(workGroup_dep);
    }

    private void initOrgTree() {
        Org group = new Org(1);
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        selectOrgs = Lists.newArrayList();
        depTreeComponent.init(
            Lists.newArrayList(),
            Lists.newArrayList(),
            group,
            selectOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);
    }

    /**
     * view 取得組織樹Manager
     *
     * @return MultipleDepTreeManager 組織樹Manager
     */
    public MultipleDepTreeManager getDepTreeManager() {
        return depTreeComponent.getMultipleDepTreeManager();
    }

    public void loadDepTree() {
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        Org group = new Org(user.getPrimaryOrg().getCompanySid());
        // 修改,故切換顯示停用部門時,需清除以挑選部門
        boolean clearSelNodeWhenChaneDepTree = true;
        boolean selectable = true;
        boolean selectModeSingle = false;
        List<Org> allOrg = WkOrgCache.getInstance().findAllDepByCompSid(group.getSid());
        depTreeComponent.init(
            allOrg,
            allOrg,
            group,
            selectOrgs,
            clearSelNodeWhenChaneDepTree,
            selectable,
            selectModeSingle);
    }

    public void doSelectOrg() {
        try {
            selectOrgs = depTreeComponent.getSelectOrg();
            if (selectOrgs == null || selectOrgs.isEmpty()) {
                Preconditions.checkState(false, "請挑選部門!!");
                return;
            }
            LinkedHashSet<UserViewVO> selectDepUserView = Sets.newLinkedHashSet();
            selectOrgs.forEach(
                item -> {
                    selectDepUserView.addAll(
                        WCUserLogic.getInstance().findByDepSid(item.getSid()));
                });
            pickUsers.setSource(
                removeTargetUserView(Lists.newArrayList(selectDepUserView), pickUsers.getTarget()));
            DisplayController.getInstance().update(groupMaintainComponent_pickPanel);
            DisplayController.getInstance().hidePfWidgetVar(workGroup_deptUnitDlg);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void save() {
        try {
            WorkGroupVO workGroupVO =
                new WorkGroupVO(groupID, groupName, pickUsers.getTarget(), statusID, note);
            WorkGroupSetupPrivateLogicComponents.getInstance()
                .saveWorkGroupVO(workGroupVO, loginUserSid);
            clear();
            reLoadCallBack.onReload();
            DisplayController.getInstance().hidePfWidgetVar(group_setup_private_setting_dlg_wv);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void loadData(WorkGroupVO workGroupVO) {
        this.filterNameValue = "";
        allCompUser = Sets.newLinkedHashSet();
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        Integer compSid = user.getPrimaryOrg().getCompanySid();
        try {
            WkOrgCache.getInstance().findAllDepByCompSid(compSid).stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList())
                .forEach(
                    item -> {
                        allCompUser.addAll(
                            WCUserLogic.getInstance().findByDepSid(item.getSid()));
                    });
        } catch (Exception e) {
            log.warn(" OrgLogicComponents.getInstance().findOrgsByCompanySid", e);
        }
        leftViewVO = Sets.newLinkedHashSet();
        selectOrgs = Lists.newArrayList();
        Org baseOrg = WCOrgLogic.getInstance().getBaseOrg(user.getPrimaryOrg().getSid());
        selectOrgs.add(baseOrg);
        List<Org> allChildOrg =
            WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                .collect(Collectors.toList());
        selectOrgs.addAll(allChildOrg);
        leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(baseOrg.getSid()));
        allChildOrg.forEach(
            item -> {
                leftViewVO.addAll(WCUserLogic.getInstance().findByDepSid(item.getSid()));
            });
        List<UserViewVO> right = Lists.newArrayList();
        filterNameValue = "";
        if (workGroupVO == null) {
            statusID = "";
            groupName = "";
            note = "";
            groupID = "";
        } else {
            statusID = workGroupVO.getStatusID();
            groupName = workGroupVO.getGroup_name();
            note = workGroupVO.getNote();
            groupID = workGroupVO.getGroup_sid();
            right = workGroupVO.getUserViewVO();
        }
        pickUsers.setSource(removeTargetUserView(Lists.newArrayList(leftViewVO), right));
        pickUsers.setTarget(right);
    }

    public void filterName() {
        if (Strings.isNullOrEmpty(filterNameValue)) {
            pickUsers.setSource(
                removeTargetUserView(Lists.newArrayList(allCompUser), pickUsers.getTarget()));
        } else {
            pickUsers.setSource(
                removeTargetUserView(
                    allCompUser.stream()
                        .filter(
                            each -> each.getName().toLowerCase()
                                .contains(filterNameValue.toLowerCase()))
                        .collect(Collectors.toList()),
                    pickUsers.getTarget()));
        }
    }

    private List<UserViewVO> removeTargetUserView(List<UserViewVO> source,
        List<UserViewVO> target) {
        List<UserViewVO> filterSource = Lists.newArrayList();
        source.forEach(
            item -> {
                if (!target.contains(item)) {
                    filterSource.add(item);
                }
            });
        return filterSource;
    }
}
