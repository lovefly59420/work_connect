/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common.attachment;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.vo.basic.BasicAttachmentService;
import com.cy.work.common.vo.basic.BasicAttachmentServlet;
import com.cy.work.connect.web.logic.components.WCMemoAttachmentLogicComponent;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;

/**
 * @author brain0925_liao
 */
@WebServlet("/WCMemoAttachServlet")
public class WCMemoAttachServlet extends HttpServlet implements BasicAttachmentServlet {

    // 要覆寫
    public static final String SERVLET_NAME = WCMemoAttachServlet.class.getSimpleName();
    /**
     *
     */
    private static final long serialVersionUID = 5232824433630244313L;

    private static final String illegal_readPath = "/error/illegal_read_memo.xhtml";


    @SuppressWarnings("rawtypes")
    @Override
    public BasicAttachmentService getAttachService(HttpServletRequest request) {
        return WCMemoAttachmentLogicComponent.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (SecurityFacade.getUserSid() == null || SecurityFacade.getUserSid() <= 0) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            return;
        }

        if(!WCMemoCategoryLogicComponent.getInstance().isMemoViewPermission()) {
            response.sendRedirect(request.getContextPath() + illegal_readPath);
            return;
        }
        BasicAttachmentServlet.super.doGet(request, response, super.getServletContext());
    }

    @Override
    public Object findAttachSid(HttpServletRequest request) {
        return String.valueOf(this.getAttachmentSidParameter(request));
    }
}
