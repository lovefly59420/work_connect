package com.cy.work.connect.web.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import com.cy.commons.util.SpringContextHolder;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 案號產生服務
 * 
 * @author allen1214_wu
 */
@Slf4j
public class CaseNoGenericService {

    /**
     * 日期格式
     */
    private static SimpleDateFormat datePatten = new SimpleDateFormat("yyyyMMdd");

    /**
     * 流水號長度
     */
    public static final int NO_LENGTH = 3;

    /**
     * 產生聯絡單單號
     * 
     * @param compID 公司別 ID
     * @return 聯絡單單號
     */
    public static synchronized String generateWCNum(String compID) {
        // EX : TGWC20211224020
        String preFix = compID + "WC";
        String caseNo = generateNum(preFix, "wc_master", "wc_no");
        return caseNo;
    }

    /**
     * 產生備忘錄單號
     * 
     * @param compID 公司別 ID
     * @return 備忘錄單號
     */
    public static synchronized String generateMemoNum(String compID) {
        // EX : XS20200415023
        String preFix = compID;
        String caseNo = generateNum(preFix, "wc_memo_master", "wc_memo_no");
        return caseNo;
    }

    /**
     * 產生序號
     * 
     * @param sysName      系統名稱
     * @param tableName    系統主表
     * @param noColumnName 欄位名稱
     * @return
     */
    private static String generateNum(String sysName, String tableName, String noColumnName) {
        // 今日序號前綴
        String todayNoPrefix = sysName + datePatten.format(new Date());

        // 查詢序號最大值
        String maxNo = queryMaxNum(tableName, noColumnName, todayNoPrefix);
        maxNo = WkStringUtils.safeTrim(maxNo);

        // 從未產生序號, 或最大值非今日序號時 , 回傳 001
        if (WkStringUtils.isEmpty(maxNo) || !maxNo.startsWith(todayNoPrefix)) {
            return generateCaseNum(todayNoPrefix, naverDuplicate(todayNoPrefix, 0));
        }

        // 解析已存在的最大序號序號
        String dbMaxSNoStr = "0";
        if (maxNo.length() > todayNoPrefix.length()) {
            dbMaxSNoStr = maxNo.substring(todayNoPrefix.length());
        }

        // 非數字時, 回傳初始序號 (應該不可能)
        if (!WkStringUtils.isNumber(dbMaxSNoStr)) {
            return generateCaseNum(todayNoPrefix, naverDuplicate(todayNoPrefix, 0));
        }

        // 本次流水號為最大流水號 + 1
        int serialNo = naverDuplicate(todayNoPrefix, Integer.parseInt(dbMaxSNoStr));

        String no = generateCaseNum(todayNoPrefix, serialNo);
        log.info("產生單號:[{}]", no);
        return no;
    }

    /**
     * 依據 pattern 產生案號
     * 
     * @param todayNoPrefix
     * @param serialNo
     * @return
     */
    private static String generateCaseNum(String todayNoPrefix, Integer serialNo) {
        return todayNoPrefix + WkStringUtils.padding(serialNo + "", '0', NO_LENGTH, true);
    }

    /**
     * 查詢欄位最大值
     * 
     * @param tableName
     * @param noColumnName
     * @return
     */
    private static String queryMaxNum(String tableName, String noColumnName, String todayNoPrefix) {

        StringBuffer varname1 = new StringBuffer();
        varname1.append("SELECT Max(" + noColumnName + ") ");
        varname1.append("FROM   " + tableName + " ");
        varname1.append("WHERE  " + noColumnName + " like '" + todayNoPrefix + "%' ");

        JdbcTemplate jdbcTemplate = SpringContextHolder.getBean(ConnectConstants.CONNECT_JDBC_TEMPLATE);
        if (jdbcTemplate == null) {
            throw new SystemDevelopException("JdbcTemplate 尚未初始化");
        }

        return jdbcTemplate.queryForObject(varname1.toString(), new Object[] {}, String.class);
    }

    /**
     * 記錄已使用過的序號, 避免重複
     */
    private static Map<String, Integer> usedMapByTodayNoPrefix = Maps.newHashMap();

    /**
     * 1.將傳入序號 + 1 2.將新序號與已使用序號比對, 避免重複
     * 
     * @param todayNoPrefix
     * @param dbMaxSNo
     * @return
     */
    private static Integer naverDuplicate(String todayNoPrefix, int dbMaxSNo) {
        // 新序號 = 資料庫中最大的序號 + 1
        int newSNo = dbMaxSNo + 1;

        // 判斷產生的新序號還未用過
        // 1.本組序號今日還未產生
        // 2.產生的新序號大於已用過的序號
        if (!usedMapByTodayNoPrefix.containsKey(todayNoPrefix)
                || newSNo > usedMapByTodayNoPrefix.get(todayNoPrefix)) {

            // 將新序號放入已使用區
            usedMapByTodayNoPrefix.put(todayNoPrefix, newSNo);
            // 回傳新序號
            return newSNo;
        }

        // 等 1 秒
        try {
            CaseNoGenericService.class.wait(1000);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        // 遞迴重取新序號
        return naverDuplicate(todayNoPrefix, newSNo);
    }
}
