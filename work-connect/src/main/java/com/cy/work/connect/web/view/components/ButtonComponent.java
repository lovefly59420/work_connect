/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * 按鈕介面物件
 *
 * @author brain0925_liao
 */
public class ButtonComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7708005513770500860L;
    /**
     * 是否顯示刪除按鈕
     */
    @Getter
    @Setter
    private boolean showDeleteBtn = false;
    /**
     * 是否可編輯
     */
    @Getter
    @Setter
    private boolean editAble = false;
    /**
     * 是否可編輯可閱名單
     */
    @Getter
    @Setter
    private boolean viewPersonAble = false;

    @Getter
    @Setter
    private boolean deleteAble = false;
    /**
     * 是否可補充說明
     */
    @Getter
    @Setter
    private boolean replyAble = false;
    /**
     * 是否可執行回覆
     */
    @Getter
    @Setter
    private boolean execReplyAble = false;
    /**
     * 是否可轉單
     */
    @Getter
    @Setter
    private boolean execTransAble = false;
    /**
     * 是否可派工
     */
    @Getter
    @Setter
    private boolean transPersonAble = false;
    /**
     * 是否可執行完成
     */
    @Getter
    @Setter
    private boolean execFinishAble = false;
    /**
     * 是否可附加檔案
     */
    @Getter
    @Setter
    private boolean attachAble = false;
    /**
     * 是否可確定存檔
     */
    @Getter
    @Setter
    private boolean sendAble = false;
    /**
     * 是否可取消存檔
     */
    @Getter
    @Setter
    private boolean cancalAble = false;
    /**
     * 是否可上一筆
     */
    @Getter
    @Setter
    private boolean previousAble = false;
    /**
     * 是否可下一筆
     */
    @Getter
    @Setter
    private boolean nextAble = false;
    /**
     * 是否可回報表
     */
    @Getter
    @Setter
    private boolean backList = false;
    /**
     * 是否可結案
     */
    @Getter
    @Setter
    private boolean showCloseAble = false;
    /**
     * 是否可加派
     */
    @Getter
    @Setter
    private boolean addExecDepAble = false;

    /**
     * 是否可顯示意見說明(需求流程)
     */
    @Getter
    @Setter
    private boolean showReqCommontBtn = false;
    /**
     * 是否可顯示意見說明(需求會簽)
     */
    @Getter
    @Setter
    private boolean showReqContinueCommontBtn = false;
    /**
     * 是否可顯示意見說明(核准流程)
     */
    @Getter
    @Setter
    private boolean showExecCommontBtn = false;
    /**
     * 是否可顯示意見說明(執行會簽)
     */
    @Getter
    @Setter
    private boolean showExecContinueCommontBtn = false;
    /**
     * 是否可顯示需求會簽按鈕
     */
    @Getter
    @Setter
    private boolean showAddReqSignBtn = false;
    /**
     * 是否可顯示執行會簽按鈕
     */
    @Getter
    @Setter
    private boolean showAddExecSignBtn = false;
    /**
     * 是否可收藏
     */
    @Getter
    @Setter
    private boolean favoriteAble = false;
}
