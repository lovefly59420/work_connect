/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import org.primefaces.model.TreeNode;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.web.listener.ReLoadCallBack;
import com.cy.work.connect.web.logic.components.WCExceDepLogicComponent;
import com.cy.work.connect.web.util.pf.DisplayController;
import com.cy.work.connect.web.util.pf.MessagesUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉單套件
 *
 * @author brain0925_liao
 */
@Slf4j
public class ExecSendPersonComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 954131203340858634L;
    /**
     * 登入者Sid
     */
    private final Integer loginUserSid;
    /**
     * 存檔更新CallBacks
     */
    private final ReLoadCallBack reLoadCallBack;
    /**
     * 執行單位選單
     */
    @Getter
    private final List<SelectItem> execSelectItems;
    /**
     * 小類選單
     */
    @Getter
    private final List<SelectItem> tagSelectItems;
    /**
     * 執行單位及執行單位可挑選人員 Map
     */
    private final Map<Integer, LinkedHashSet<User>> depUsers;
    /**
     * 執行單位及執行單位可挑選小類 Map
     */
    private final Map<Integer, List<WCTag>> tagMaps;
    @Getter
    private final OrgTreePersonComponent orgTreePersonComponent;
    /**
     * 工作聯絡單Sid
     */
    private String wcSid;
    /**
     * 執行人員
     */
    @Getter
    private List<SelectItem> userItems;
    /**
     * 挑選的執行單位
     */
    @Getter
    @Setter
    private String selExecDepSid;
    /**
     * 挑選的小類
     */
    @Getter
    @Setter
    private List<String> selTagSids;
    /**
     * 挑選的執行人員
     */
    @Getter
    @Setter
    private String selUserSid;

    public ExecSendPersonComponent(
            Integer loginCompSid,
            Integer loginUserSid,
            ReLoadCallBack reLoadCallBack) {
        this.orgTreePersonComponent = new OrgTreePersonComponent(loginCompSid);
        this.loginUserSid = loginUserSid;
        this.reLoadCallBack = reLoadCallBack;
        this.execSelectItems = Lists.newArrayList();
        this.tagSelectItems = Lists.newArrayList();
        this.userItems = Lists.newArrayList();
        this.depUsers = Maps.newConcurrentMap();
        this.tagMaps = Maps.newConcurrentMap();
    }

    public void loadData(String wcSid) {
        this.wcSid = wcSid;
        this.selExecDepSid = "";
        this.selTagSids = Lists.newArrayList();
        this.selUserSid = "";
        depUsers.clear();
        execSelectItems.clear();
        tagSelectItems.clear();
        userItems.clear();
        depUsers.clear();
        tagMaps.clear();
        Map<Org, List<WCTag>> execDepMap = WCExceDepLogicComponent.getInstance().getSendPersonExecDeps(wcSid, loginUserSid);
        Preconditions.checkState(execDepMap.keySet().size() > 0, "已無執行單位可進行分派");
        orgTreePersonComponent.initItems(execDepMap);
        TreeNode nodes = orgTreePersonComponent.expandAllChildNode(
                orgTreePersonComponent.getDepTreeManager().getDepRoot());
        orgTreePersonComponent.getDepTreeManager().setDepRoot(nodes);
        execDepMap
                .keySet()
                .forEach(
                        item -> {
                            tagMaps.put(item.getSid(), execDepMap.get(item));
                        });
    }

    /**
     * 初始化選項
     *
     * @param execDepMap
     */
    @SuppressWarnings("unused")
    private void initItems(Map<Org, List<WCTag>> execDepMap) {
        Map<Integer, SelectItem> items = Maps.newHashMap();
        execDepMap
                .keySet()
                .forEach(
                        item -> {
                            List<Org> childOrgs = Lists.newArrayList(item);
                            childOrgs.addAll(
                                    WkOrgCache.getInstance().findAllChild(item.getSid()).stream()
                                            .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                            .collect(Collectors.toList()));
                            LinkedHashSet<User> users = Sets.newLinkedHashSet();
                            childOrgs.forEach(
                                    oItem -> {
                                        users.addAll(
                                                WkUserCache.getInstance()
                                                        .findUserWithManagerByOrgSids(Lists.newArrayList(oItem.getSid()), Activation.ACTIVE)
                                                        .stream()
                                                        .distinct()
                                                        .collect(Collectors.toList()));
                                    });
                            depUsers.put(item.getSid(), users);
                            tagMaps.put(item.getSid(), execDepMap.get(item));
                            if (execSelectItems.isEmpty()) {
                                execDepMap
                                        .get(item)
                                        .forEach(
                                                subItem -> {
                                                    tagSelectItems.add(
                                                            new SelectItem(subItem.getSid(), subItem.getTagName()));
                                                    selTagSids.add(subItem.getSid());
                                                });
                                depUsers
                                        .get(item.getSid())
                                        .forEach(
                                                iItem -> {
                                                    items.put(
                                                            iItem.getSid(),
                                                            new SelectItem(String.valueOf(iItem.getSid()),
                                                                    iItem.getName()));
                                                });
                            }
                            execSelectItems.add(
                                    new SelectItem(String.valueOf(item.getSid()), item.getName()));
                        });
        userItems = items.values().stream()
                .sorted((u1, u2) -> u1.getLabel().compareTo(u2.getLabel()))
                .collect(Collectors.toList());
    }

    public void changeExecDep() {

        try {
            this.selTagSids = Lists.newArrayList();
            this.selUserSid = "";
            tagSelectItems.clear();
            userItems = depUsers.get(Integer.valueOf(selExecDepSid)).stream()
                    .map(each -> new SelectItem(String.valueOf(each.getSid()), each.getName()))
                    .sorted((u1, u2) -> u1.getLabel().compareTo(u2.getLabel()))
                    .collect(Collectors.toList());
            tagMaps
                    .get(Integer.valueOf(selExecDepSid))
                    .forEach(
                            subItem -> {
                                tagSelectItems.add(new SelectItem(subItem.getSid(), subItem.getTagName()));
                                selTagSids.add(subItem.getSid());
                            });
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }

    public void saveSendPerson() {
        try {
            // Preconditions.checkState(!Strings.isNullOrEmpty(selExecDepSid), "請挑選執行單位");
            // Preconditions.checkState(selTagSids != null && !selTagSids.isEmpty(), "請挑選類別");
            Preconditions.checkState(orgTreePersonComponent.getSelUser() != null, "請挑選執行人員");
            WCExceDepLogicComponent.getInstance()
                    .saveSendPersonExecDep(
                            wcSid, tagMaps, orgTreePersonComponent.getSelUser().getSid(), loginUserSid);
            reLoadCallBack.onReload();
            DisplayController.getInstance().hidePfWidgetVar("dlg_sendExecPerson");
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "[" + e.getMessage() + "]";
            MessagesUtils.showError(errorMessage);
            log.error(errorMessage, e);
        }
    }
}
