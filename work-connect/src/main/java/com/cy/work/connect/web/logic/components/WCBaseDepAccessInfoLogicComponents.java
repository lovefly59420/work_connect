/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.logic.manager.WCBaseDepAccessInfoManager;
import com.cy.work.connect.vo.WCBaseDepAccessInfo;
import com.cy.work.connect.vo.converter.to.DepTo;
import com.cy.work.connect.vo.converter.to.LimitBaseAccessViewDep;
import com.cy.work.connect.vo.converter.to.OtherBaseAccessViewDep;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位可閱權限設定邏輯原件
 *
 * @author brain0925_liao
 */
@Component
public class WCBaseDepAccessInfoLogicComponents implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8737384574959095007L;
    /**
     * WCBaseDepAccessInfoLogicComponents
     */
    private static WCBaseDepAccessInfoLogicComponents instance;
    /**
     * WCBaseDepAccessInfoManager
     */
    @Autowired
    private WCBaseDepAccessInfoManager wcBaseDepAccessInfoManager;
    /**
     * OrgLogicComponents
     */
    @Autowired
    private WCOrgLogic orgLogicComponents;
    /**
     * WCBasePersonAccessInfoLogicComponents
     */
    @Autowired
    private WCBasePersonAccessInfoLogicComponents wcBasePersonAccessInfoLogicComponents;

    public static WCBaseDepAccessInfoLogicComponents getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCBaseDepAccessInfoLogicComponents.instance = this;
    }

    /**
     * 取得基礎單位外增加檢閱的單位(人員及部門聯集)
     *
     * @param wcBaseDepAccessInfoDepSid 登入者部門Sid
     * @param loginUserSid              登入者Sid
     * @return
     */
    public List<Org> getOtherBaseDepAccessInfoDeps(
            Integer wcBaseDepAccessInfoDepSid, Integer loginUserSid) {
        WCBaseDepAccessInfo wcBaseDepAccessInfo = wcBaseDepAccessInfoManager.getByloginDepSid(wcBaseDepAccessInfoDepSid);
        List<Org> results = Lists.newArrayList();
        if (wcBaseDepAccessInfo != null
                && wcBaseDepAccessInfo.getOtherBaseAccessViewDep() != null
                && wcBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos() != null) {
            wcBaseDepAccessInfo
                    .getOtherBaseAccessViewDep()
                    .getDepTos()
                    .forEach(
                            item -> {
                                results.add(WkOrgCache.getInstance().findBySid(item.getSid()));
                            });
        }
        List<User> otherUsers = wcBasePersonAccessInfoLogicComponents.getOtherBaseAccessViewUser(loginUserSid);
        otherUsers.forEach(
                item -> {
                    Org dep = WkOrgCache.getInstance().findBySid(item.getPrimaryOrg().getSid());
                    if (!results.contains(dep)) {
                        results.add(dep);
                    }
                });
        return results;
    }

    /**
     * 取得基礎單位內限制的單位(人員及部門交集)
     *
     * @param wcBaseDepAccessInfoDepSid 登入者部門Sid
     * @param loginUserSid              登入者Sid
     * @return
     */
    public List<Org> getBaseDepAccessInfoDeps(
            Integer wcBaseDepAccessInfoDepSid, Integer loginUserSid) {
        WCBaseDepAccessInfo wcBaseDepAccessInfo = wcBaseDepAccessInfoManager.getByloginDepSid(wcBaseDepAccessInfoDepSid);
        List<Org> results = Lists.newArrayList();
        // 優先檢測是否有限制基礎部門特定人員,若有代表基礎部門下僅能顯示該些人員
        List<User> limitUsers = wcBasePersonAccessInfoLogicComponents.getLimitBaseAccessViewUser(loginUserSid);
        List<Org> limitOrgs = getLimitBaseAccessViewDep(wcBaseDepAccessInfoDepSid);
        if (limitUsers != null) {
            limitUsers.forEach(
                    item -> {
                        Org dep = WkOrgCache.getInstance().findBySid(item.getPrimaryOrg().getSid());
                        if (limitOrgs != null && !limitOrgs.contains(dep)) {
                            return;
                        }

                        if (!results.contains(dep)) {
                            results.add(dep);
                        }
                    });
            return results;
        }
        // 若無限制基礎部門設定,預設帶入基礎部門都可搜尋
        if (wcBaseDepAccessInfo == null
                || wcBaseDepAccessInfo.getLimitBaseAccessViewDep() == null
                || wcBaseDepAccessInfo.getLimitBaseAccessViewDep().getDepTos() == null) {
            Org baseOrg = orgLogicComponents.getBaseOrg(wcBaseDepAccessInfoDepSid);
            if (baseOrg != null) {
                results.add(baseOrg);
                results.addAll(
                        WkOrgCache.getInstance().findAllChild(baseOrg.getSid()).stream()
                                .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                                .collect(Collectors.toList()));
            }
        } else {
            wcBaseDepAccessInfo
                    .getLimitBaseAccessViewDep()
                    .getDepTos()
                    .forEach(
                            item -> {
                                results.add(WkOrgCache.getInstance().findBySid(item.getSid()));
                            });
        }
        return results;
    }

    /**
     * 取得限制基本單位 By 部門Sid
     *
     * @param wcBaseDepAccessInfoDepSid 部門Sid
     * @return
     */
    public List<Org> getLimitBaseAccessViewDep(Integer wcBaseDepAccessInfoDepSid) {
        return wcBaseDepAccessInfoManager.getLimitBaseAccessViewDep(wcBaseDepAccessInfoDepSid);
    }

    /**
     * 取得增加基本單位 By 部門Sid
     *
     * @param wcBaseDepAccessInfoDepSid 部門Sid
     * @return
     */
    public List<Org> getOtherBaseAccessViewDep(Integer wcBaseDepAccessInfoDepSid) {
        return wcBaseDepAccessInfoManager.getOtherBaseAccessViewDep(wcBaseDepAccessInfoDepSid);
    }

    /**
     * 取得單位可閱權限增加(聯集)已設定單位
     *
     * @return
     */
    public List<Integer> getSettingOtherBaseOrgSids() { return wcBaseDepAccessInfoManager.getSettingOtherBaseOrgSids(); }

    /**
     * 儲存限制基本單位
     *
     * @param wcBaseDepAccessInfoDepSid 部門Sid
     * @param loginUserSid              登入者Sid
     * @param accessDeps                可閱讀的限制基本單位部門
     */
    public void saveLimitBaseAccessViewDep(
            Integer wcBaseDepAccessInfoDepSid, Integer loginUserSid, List<Org> accessDeps) {
        WCBaseDepAccessInfo wcBaseDepAccessInfo = wcBaseDepAccessInfoManager.getByloginDepSid(wcBaseDepAccessInfoDepSid);
        if (wcBaseDepAccessInfo == null) {
            wcBaseDepAccessInfo = new WCBaseDepAccessInfo();
            wcBaseDepAccessInfo.setLoginUserDepSid(wcBaseDepAccessInfoDepSid);
        }
        LimitBaseAccessViewDep limitBaseAccessViewDep = new LimitBaseAccessViewDep();
        accessDeps.forEach(
                item -> {
                    DepTo dt = new DepTo();
                    dt.setSid(item.getSid());
                    limitBaseAccessViewDep.getDepTos().add(dt);
                });
        wcBaseDepAccessInfo.setLimitBaseAccessViewDep(limitBaseAccessViewDep);
        if (!Strings.isNullOrEmpty(wcBaseDepAccessInfo.getSid())) {
            wcBaseDepAccessInfoManager.updateWCBaseDepAccessInfo(wcBaseDepAccessInfo, loginUserSid);
        } else {
            wcBaseDepAccessInfoManager.createWCBaseDepAccessInfo(wcBaseDepAccessInfo, loginUserSid);
        }
    }

    /**
     * 儲存增加基本部門
     *
     * @param wcBaseDepAccessInfoDepSid 部門Sid
     * @param loginUserSid              登入者Sid
     * @param accessDeps                增加的部門
     */
    public void saveOtherBaseAccessViewDep(
            Integer wcBaseDepAccessInfoDepSid, Integer loginUserSid, List<Org> accessDeps) {
        WCBaseDepAccessInfo wcBaseDepAccessInfo = wcBaseDepAccessInfoManager.getByloginDepSid(wcBaseDepAccessInfoDepSid);
        if (wcBaseDepAccessInfo == null) {
            wcBaseDepAccessInfo = new WCBaseDepAccessInfo();
            wcBaseDepAccessInfo.setLoginUserDepSid(wcBaseDepAccessInfoDepSid);
        }
        OtherBaseAccessViewDep otherBaseAccessViewDep = new OtherBaseAccessViewDep();
        accessDeps.forEach(
                item -> {
                    DepTo dt = new DepTo();
                    dt.setSid(item.getSid());
                    otherBaseAccessViewDep.getDepTos().add(dt);
                });
        wcBaseDepAccessInfo.setOtherBaseAccessViewDep(otherBaseAccessViewDep);
        if (!Strings.isNullOrEmpty(wcBaseDepAccessInfo.getSid())) {
            wcBaseDepAccessInfoManager.updateWCBaseDepAccessInfo(wcBaseDepAccessInfo, loginUserSid);
        } else {
            wcBaseDepAccessInfoManager.createWCBaseDepAccessInfo(wcBaseDepAccessInfo, loginUserSid);
        }
    }
}
