/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.cy.work.connect.vo.WCTag;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Marlow_Chen
 */
public class LegitimateRangeComponent {

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private Date selectDate;

    @Getter
    @Setter
    private String limitDate;

    @Getter
    @Setter
    private String sid;

    @Getter
    @Setter
    private boolean canEdit;

    public LegitimateRangeComponent(
            String legitimateRangeTitle,
            String legitimateRangeDays,
            String sid) {

        this.title = legitimateRangeTitle;
        this.limitDate = legitimateRangeDays;
        this.sid = sid;
    }

    public LegitimateRangeComponent(
            WCTag wcTag,
            String dataJsonStr) {
        this.title = wcTag.getLegitimateRangeTitle();
        this.limitDate = wcTag.getLegitimateRangeDays();
        this.sid = wcTag.getSid();
    }

    @Override
    public String toString() {
        return "{\"sid\" : \"" + sid + "\", \"date\" : \"" + covertDate(selectDate) + "\"}";
    }

    public String covertDate(Date selectDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String dateString = sdf.format(selectDate);

        return dateString;
    }
}
