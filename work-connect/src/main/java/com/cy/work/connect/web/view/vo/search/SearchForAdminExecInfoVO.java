/**
 * 
 */
package com.cy.work.connect.web.view.vo.search;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class SearchForAdminExecInfoVO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1899702983378175537L;

    /**
     * SID
     */
    @Getter
    @Setter
    private String wcSid;
    /**
     *tagSid
     */
    @Getter
    @Setter
    private String tagSid;
    /**
     * 執行部門
     */
    @Getter
    @Setter
    private Integer execDepSid;
    /**
     * 執行狀態
     */
    @Getter
    @Setter
    private String execStatus;
    /**
     * 執行人員
     */
    @Getter
    @Setter
    private Integer execUserSid;


}
