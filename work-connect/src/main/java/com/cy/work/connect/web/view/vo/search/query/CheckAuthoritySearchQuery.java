/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.vo.search.query;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.connect.vo.enums.FlowType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.primefaces.model.TreeNode;

/**
 * 核決權限表查詢
 *
 * @author marlow_chen
 */
@Data
@NoArgsConstructor
public class CheckAuthoritySearchQuery implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3804729396964449247L;
    /**
     * Tag多選
     */
    List<TreeNode> selNode;
    /**
     * 類別
     */
    private String categoryTagId;
    /**
     * 單據名稱
     */
    private String menuTagId;
    /**
     * 執行項目
     */
    private String subTagId;
    /**
     * 可使用單位
     */
    private List<Org> canUseOrg;
    /**
     * 可閱部門
     */
    private List<Org> viewOrgs;
    /**
     * 指派單位
     */
    private List<Org> transOrgs;
    /**
     * 執行單位
     */
    private List<Org> execOrg;
    /**
     * 相關單位
     */
    private List<Org> relatedOrg;
    /**
     * 執行模式
     */
    private String execMode;
    /**
     * 可使用人員
     */
    private List<User> useUsers;
    /**
     * 可閱人員
     */
    private List<User> viewUsers;
    /**
     * 指派人員
     */
    private List<User> assignUser;
    /**
     * 執行單位簽核人員
     */
    private List<User> execSignUser;
    /**
     * 簽核層級
     */
    private FlowType flowType;

    public void init() {
        clear();
    }

    public void clear() {
        this.categoryTagId = "";
        this.menuTagId = "";
        this.subTagId = "";
        this.canUseOrg = Lists.newArrayList();
        this.viewOrgs = Lists.newArrayList();
        this.transOrgs = Lists.newArrayList();
        this.execOrg = Lists.newArrayList();
        this.relatedOrg = Lists.newArrayList();
        this.execMode = "";
        this.useUsers = Lists.newArrayList();
        this.viewUsers = Lists.newArrayList();
        this.execSignUser = Lists.newArrayList();
        this.assignUser = Lists.newArrayList();
        this.flowType = null;
        this.selNode = Lists.newArrayList();
    }
}
