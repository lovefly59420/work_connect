/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.logic.components;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.vo.ExecReplyTo;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.cy.work.connect.web.view.vo.to.EditExecReplyTo;
import com.cy.work.connect.web.view.vo.to.ExecTagTo;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 執行單位(回覆)邏輯元件
 *
 * @author kasim
 */
@Component
@Slf4j
public class WCExecReplyLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2778502718096256117L;

    private static WCExecReplyLogicComponent instance;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCExecDepSettingLogicComponent execDepSettingLogicComponent;
    @Autowired
    private WCTagLogicComponents wcTagLogicComponents;

    public static WCExecReplyLogicComponent getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCExecReplyLogicComponent.instance = this;
    }

    /**
     * 取得該登入者可進行執行回覆的執行部門及類別Map
     *
     * @param wcSid
     * @param loginUserSid
     * @return
     */
    public List<String> getExecReplyExecDeps(String wcSid, Integer loginUserSid) {
        Map<Org, List<ExecTagTo>> execDepMap = Maps.newConcurrentMap();
        try {
            wcExecDepManager.findActiveByWcSid(wcSid).stream()
                    .filter(
                            each -> each.getStatus().equals(Activation.ACTIVE)
                                    && each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD))
                    .collect(Collectors.toList())
                    .forEach(
                            item -> {
                                if (Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
                                    if (item.getExecUserSid().equals(loginUserSid)) {
                                        Org org = orgManager.findBySid(item.getDep_sid());
                                        List<ExecTagTo> tags = execDepMap.get(org);
                                        if (tags == null) {
                                            tags = Lists.newArrayList();
                                        }
                                        tags.add(new ExecTagTo(item.getSid(), ""));
                                        execDepMap.put(org, tags);
                                    }
                                } else {
                                    WCExecDepSetting execDepSetting = execDepSettingLogicComponent.getWCExecDepSettingBySid(
                                            item.getExecDepSettingSid());
                                    if (item.getExecUserSid().equals(loginUserSid)) {
                                        Org org = orgManager.findBySid(item.getDep_sid());
                                        List<ExecTagTo> tags = execDepMap.get(org);
                                        if (tags == null) {
                                            tags = Lists.newArrayList();
                                        }
                                        WCTag tag = wcTagLogicComponents.getWCTagBySid(
                                                execDepSetting.getWc_tag_sid());
                                        tags.add(new ExecTagTo(item.getSid(), tag.getTagName()));
                                        execDepMap.put(org, tags);
                                    }
                                }
                            });
        } catch (Exception e) {
            log.warn("getExecReplyExecDeps ERROR", e);
        }
        List<String> result = Lists.newArrayList();
        execDepMap
                .entrySet()
                .forEach(
                        each -> {
                            each.getValue()
                                    .forEach(
                                            value -> {
                                                result.add(value.getExceDepSid());
                                            });
                        });
        return result;
    }

    /**
     * 確認新增
     *
     * @param loginUserSid 登入者
     * @param wcSid
     * @param contentCss
     * @param execDepSid   [wc_exec_dep].[wc_exec_dep_sid]
     */
    public void checkCreate(
            Integer loginUserSid, String wcSid, String contentCss, List<String> execDepSid) {
        Preconditions.checkState(execDepSid != null && !execDepSid.isEmpty(), "請選擇類別選項，請確認！！");
        Preconditions.checkState(
                wcExecDepManager.findActiveByWcSid(wcSid).stream()
                        .filter(each -> execDepSid.contains(each.getSid()))
                        .allMatch(
                                each -> {
                                    return Activation.ACTIVE.equals(each.getStatus())
                                            && WCExceDepStatus.PROCEDD.equals(each.getExecDepStatus())
                                            && each.getExecUserSid() != null
                                            && loginUserSid.equals(each.getExecUserSid());
                                }),
                "資料已被異動，請確認！！");
        Preconditions.checkState(!Strings.isNullOrEmpty(contentCss), "請輸入回覆內容，請確認！！");
    }

    /**
     * 取得聯絡單所有執行回覆紀錄
     *
     * @param userSid
     * @param wcSid
     * @return
     */
    public List<ExecReplyTo> findExecReplyToByWcSid(Integer userSid, String wcSid) {
        return WCTraceLogicComponents.getInstance()
                .getWCTraceVOsByWcSIDAndWCTraceType(userSid, wcSid, WCTraceType.REPLY)
                .stream()
                .map(
                        each -> {
                            return new ExecReplyTo(
                                    each.getSid(),
                                    each.isShowReplyEditBtn(),
                                    each.getCreateUserSid(),
                                    each.getUserInfo(),
                                    each.getTime(),
                                    each.getContent());
                        })
                .collect(Collectors.toList());
    }

    /**
     * 建立自訂物件(暫時)
     *
     * @param depSid
     * @param Integer
     * @return
     */
    public EditExecReplyTo createTo(Integer depSid, Integer Integer) {
        EditExecReplyTo result = new EditExecReplyTo();
        result.setLoginInfo(
                WkOrgUtils.findNameBySid(depSid) + " - " + WkUserUtils.findNameBySid(Integer));
        result.setUpdateDate(new DateTime(new Date()).toString("yyyy/MM/dd HH:mm:ss"));
        return result;
    }

}
