/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components.memo;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;

/**
 * 備忘錄 鎖定資料
 *
 * @author kasim
 */
public class MemoLockComponent implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3899573716276956028L;

    @Getter
    /** 是否顯示鎖定 */
    private Boolean showLock = false;

    @Getter
    /** 鎖定人員 */
    private String lockUserName;

    @Getter
    /** 鎖定人員 */
    private Integer lockUserSid;

    @Getter
    /** 鎖定時間 */
    private Date lockTime;

    @Getter
    /** 最後編輯人員 */
    private String updateUserName;

    @Getter
    /** 最後編輯時間 */
    private String updateTime;

    public MemoLockComponent() {
    }

    /**
     * 初始化資料
     *
     * @param lockUserSid
     * @param lockUserName
     * @param lockTime
     * @param updateUserName
     * @param updateTime
     */
    public void init(
        Integer lockUserSid,
        String lockUserName,
        Date lockTime,
        String updateUserName,
        String updateTime) {
        this.showLock = false;
        this.lockUserSid = lockUserSid;
        this.lockUserName = lockUserName;
        this.lockTime = lockTime;
        this.updateUserName = updateUserName;
        this.updateTime = updateTime;
    }

    /**
     * 異動鎖定欄位資料
     *
     * @param lockUserSid
     * @param lockUserName
     * @param lockTime
     */
    public void lock(Integer lockUserSid, String lockUserName, Date lockTime) {
        this.showLock = true;
        this.lockUserSid = lockUserSid;
        this.lockUserName = lockUserName;
        this.lockTime = lockTime;
    }

    /**
     * 清除鎖定欄位資料
     */
    public void closeLock() {
        this.showLock = false;
        this.lockUserName = null;
        this.lockTime = null;
    }
}
