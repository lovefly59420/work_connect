/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.common;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.NodeUnselectEvent;
import org.primefaces.model.TreeNode;
import org.springframework.util.CollectionUtils;

/**
 * 多選部門元件 Marlow_chen
 */
public class MultipleDepTreePickerManager extends BaseDepTreeManager {

    /**
     *
     */
    private static final long serialVersionUID = 1197413942365871113L;

    @Getter
    @Setter
    /** 是否全選 */
    private boolean checkAll = false;

    /**
     * 未選取清單
     */
    @Getter
    @Setter
    private List<User> allUnselected = Lists.newArrayList();

    @Getter
    @Setter
    private List<User> selUsersOfAllUnselected;

    /**
     * 已選取清單
     */
    @Getter
    @Setter
    private List<User> allSelected = Lists.newArrayList();

    @Getter
    @Setter
    private List<User> selUsersOfAllSelected;

    // //選擇的部門
    // @Getter
    // @Setter
    // private TreeNode[] selNodes;

    public MultipleDepTreePickerManager() {
    }

    public TreeNode[] getSelNode() {
        if (selNode.isEmpty()) {
            return null;
        } else {
            return selNode.stream().toArray(size -> new TreeNode[size]);
        }
    }

    public void setSelNode(TreeNode[] selNode) {
        if (selNode == null) {
            return;
        }
        this.selNode = Arrays.asList(selNode);
    }

    /**
     * 進行全選
     */
    public void selectAllTree() {
        if (!checkAll) {
            this.selectUnAllTree();
            return;
        }
        selNode = Lists.newArrayList();
        List<TreeNode> list = Lists.newArrayList();
        storeAllSubNodes(list, this.getDepRoot());
        list.forEach(
                each -> {
                    if (each.isSelectable()) {
                        this.selNode.add(each);
                        each.setSelected(true);
                    }
                });
        this.setUserPickTableValue();
        if (noShowDepHistory) {
            expandAll(depRootByActive, true, false);
        } else {
            expandAll(depRootByAll, true, false);
        }
    }

    /**
     * 進行反選
     */
    private void selectUnAllTree() {
        selNode = Lists.newArrayList();
        List<TreeNode> list = Lists.newArrayList();
        this.storeAllSubNodes(list, this.getDepRoot());
        list.forEach(
                each -> {
                    if (each.isSelectable()) {
                        each.setSelected(false);
                    }
                });
        this.setUserPickTableValue();
        if (noShowDepHistory) {
            expandAll(depRootByActive, false, false);
        } else {
            expandAll(depRootByAll, false, false);
        }
    }

    public void selectNode(NodeSelectEvent event) {
        this.setUserPickTableValue();
    }

    public void unselectNode(NodeUnselectEvent event) {
        this.setUserPickTableValue();
    }

    /**
     * 多選
     */
    public void setUserPickTableValue() {
        if (WkStringUtils.isEmpty(this.selNode)) {
            allUnselected = Lists.newArrayList();
            return;
        }

        // 取得所有選擇部門
        Set<Integer> selectOrgSids = this.selNode.stream()
                .filter(node -> node.getData() != null)
                .map(node -> ((Org) node.getData()).getSid())
                .collect(Collectors.toSet());

        // 取得所有選擇部門的 user
        allUnselected = WkUserCache.getInstance().findUserWithManagerByOrgSids(
                Lists.newArrayList(selectOrgSids),
                Activation.ACTIVE);

        // 扣掉已選擇
        allUnselected.removeAll(allSelected);
    }

    /**
     * 挑選所有到已選取清單
     */
    public void pickAllToHadSelectedTable() {
        if (CollectionUtils.isEmpty(allUnselected)) {
            return;
        }
        selUsersOfAllSelected = allUnselected;
        allSelected.addAll(allUnselected);

        allUnselected = Lists.newArrayList();
        selUsersOfAllUnselected = Lists.newArrayList();
    }

    /**
     * 挑選到已選取清單
     */
    public void pickToHadSelectedTable() {
        if (CollectionUtils.isEmpty(allUnselected)) {
            return;
        }
        if (CollectionUtils.isEmpty(selUsersOfAllUnselected)) {
            selUsersOfAllUnselected = Lists.newArrayList();
            selUsersOfAllUnselected.add(allUnselected.get(0));
        }
        selUsersOfAllSelected = selUsersOfAllUnselected;
        allSelected.addAll(selUsersOfAllSelected);

        allUnselected.removeAll(selUsersOfAllUnselected);
        selUsersOfAllUnselected = Lists.newArrayList();
    }

    /**
     * 挑選到未選取清單
     */
    public void pickToUnselectedTable() {
        if (CollectionUtils.isEmpty(allSelected)) {
            return;
        }
        if (CollectionUtils.isEmpty(selUsersOfAllSelected)) {
            selUsersOfAllSelected = Lists.newArrayList();
            selUsersOfAllSelected.add(allSelected.get(0));
        }
        selUsersOfAllUnselected = selUsersOfAllSelected;
        allUnselected.addAll(selUsersOfAllUnselected);

        allSelected.removeAll(selUsersOfAllUnselected);
        selUsersOfAllSelected = Lists.newArrayList();
    }

    /**
     * 挑選所有到未選取清單
     */
    public void pickAllToUnselectedTable() {
        if (CollectionUtils.isEmpty(allSelected)) {
            return;
        }
        selUsersOfAllUnselected = allSelected;
        allUnselected.addAll(allSelected);
        allSelected = Lists.newArrayList();
        selUsersOfAllSelected = Lists.newArrayList();
    }

    public String getName(Integer orgSid) {
        if (orgSid == null) {
            return "";
        }
        return WkOrgUtils.findNameBySid(orgSid);
    }
}
