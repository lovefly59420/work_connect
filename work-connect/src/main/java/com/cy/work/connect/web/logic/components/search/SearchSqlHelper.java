/**
 * 
 */
package com.cy.work.connect.web.logic.components.search;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.utils.WkSqlUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.web.logic.components.WCMenuTagLogicComponent;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

/**
 * @author allen1214_wu
 */
@Service
public class SearchSqlHelper implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4772391993072469709L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SearchSqlHelper instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static SearchSqlHelper getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(SearchSqlHelper instance) { SearchSqlHelper.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WCMenuTagLogicComponent wcMenuTagLogicComponent;
    @Autowired
    private WCMasterHelper wcMasterHelper;

    // ========================================================================
    // 方法
    // ========================================================================

    public String prepareQuerySqlForExecDep(
            Integer loginUserSid,
            Set<WCExceDepStatus> exceDepStatus,
            String categorySid,
            Date startDate,
            Date endDate,
            String targetWcSid,
            String searchKeyword) {

        // ====================================
        // 模糊查詢條件是否為單號
        // ====================================
        String forceWcNo = "";
        searchKeyword = WkStringUtils.safeTrim(searchKeyword);
        if (wcMasterHelper.isWcNo(SecurityFacade.getCompanyId(), searchKeyword)) {
            forceWcNo = searchKeyword;
        }

        // ====================================
        // 指定查詢
        // ====================================
        // 當有指定 wcSid 或單號時
        boolean isForceSearch = false;
        if (WkStringUtils.notEmpty(targetWcSid)
                || WkStringUtils.notEmpty(forceWcNo)) {
            isForceSearch = true;
        }

        // ====================================
        // 取得登入者可閱部門 (執行方可閱權限)
        // ====================================

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT execDep.wc_exec_dep_sid, ");
        sql.append("       wc.wc_sid, ");
        sql.append("       wc.wc_no, ");
        sql.append("       wc.create_dt, ");
        sql.append("       wc.menutag_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       execDep.exec_user_sid, ");
        sql.append("       execDep.wc_exec_dep_setting_sid, ");
        sql.append("       execDep.dep_sid, ");
        sql.append("       execDep.exec_dep_status, ");
        sql.append("       wc.wc_status, ");
        sql.append("       wc.content, ");
        sql.append("       wc.dep_sid as reqDepSid, ");
        sql.append("       wc.read_record, ");
        sql.append("       wc.reply_not_read ");
        sql.append("FROM   wc_master wc ");

        sql.append("       INNER JOIN (SELECT execDep.wc_exec_dep_sid, ");
        sql.append("                          execDep.wc_exec_dep_setting_sid, ");
        sql.append("                          execDep.wc_sid, ");
        sql.append("                          execDep.exec_user_sid, ");
        sql.append("                          execDep.dep_sid, ");
        sql.append("                          execDep.exec_dep_status, ");
        sql.append("                          execDep.receviceuser ");
        sql.append("                   FROM   wc_exec_dep execDep ");
        sql.append("                   WHERE  execDep.status = 0 ");

        // ----------------------------
        // 限定執行狀態
        // ----------------------------
        if (WkStringUtils.notEmpty(exceDepStatus)) {
            String selProcessStatusStr = exceDepStatus
                    .stream()
                    .map(WCExceDepStatus::name)
                    .collect(Collectors.joining("', '", "'", "'"));

            if (exceDepStatus.size() == 1) {
                sql.append("                     AND  execDep.exec_dep_status = " + selProcessStatusStr + " ");
            } else {
                sql.append("                     AND  execDep.exec_dep_status IN (" + selProcessStatusStr + ") ");
            }
        }

        // ----------------------------
        // 限定可閱條件
        // ----------------------------
        sql.append("                     AND " + RelationDepHelper.prepareConditionSqlForExecCanView(loginUserSid) + " ");
        sql.append("               ) execDep ");

        sql.append("               ON wc.wc_sid = execDep.wc_sid ");
        sql.append("WHERE  1 = 1 ");

        // ----------------------------
        // 指定查詢
        // ----------------------------
        if (isForceSearch) {
            // 限定 WC_SID
            if (WkStringUtils.notEmpty(targetWcSid)) {
                sql.append("  AND wc.wc_sid = '" + targetWcSid + "' ");
            }
            // 限定 單號
            if (WkStringUtils.notEmpty(forceWcNo)) {
                sql.append("  AND wc.wc_no = '" + forceWcNo + "' ");
            }
        }

        // ----------------------------
        // 類別
        // ----------------------------
        if (!isForceSearch &&
                !Strings.isNullOrEmpty(categorySid)) {
            sql.append(
                    " AND wc.menuTag_sid in ("
                            + this.wcMenuTagLogicComponent
                                    .getMenuTagSearchVO("", categorySid, null)
                                    .stream()
                                    .map(each -> each.getSid())
                                    .collect(Collectors.joining("','", "'", "'"))
                            + ") ");
        }

        // ----------------------------
        // 建立區間
        // ----------------------------
        if (!isForceSearch && startDate != null) {
            sql.append(" AND wc.create_dt >= '")
                    .append(new DateTime(startDate).toString("yyyy-MM-dd"))
                    .append(" 00:00:00'  ");
        }

        if (!isForceSearch && endDate != null) {
            sql.append(" AND wc.create_dt <= '")
                    .append(new DateTime(endDate).toString("yyyy-MM-dd"))
                    .append("  23:59:59'  ");
        }
        
        // ----------------------------
        // 模糊查詢
        // ----------------------------
        if (!isForceSearch && WkStringUtils.notEmpty(searchKeyword)) {
            String searchText = WkStringUtils.safeTrim(searchKeyword).toLowerCase();
            sql.append(" AND  (  ");
            // 主題
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.theme)", searchText));
            // 內容
            sql.append("         OR  ");
            sql.append(WkSqlUtils.prepareConditionSQLByMutiKeyword("LOWER(wc.content)", searchText));
            // 追蹤內容
            sql.append("         OR  ");
            Map<String, String> bindColumns = Maps.newHashMap();
            bindColumns.put("wc_sid", "wc.wc_sid");
            sql.append(WkSqlUtils.prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
                    "wc_trace", bindColumns, "wc_trace_content", searchText));

            sql.append("      ) ");
        }

        // ----------------------------
        // 以建立時間排序
        // ----------------------------
        sql.append("ORDER  BY wc.create_dt ASC");

        return sql.toString();

    }

}
