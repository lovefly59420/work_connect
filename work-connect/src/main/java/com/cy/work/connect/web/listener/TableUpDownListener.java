/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.listener;

/**
 * 父介面監聽子介面動作Interface
 *
 * @author brain0925_liao
 */
public interface TableUpDownListener {

    /**
     * 監聽子介面點選上筆
     */
    void openerByBtnUp();

    /**
     * 監聽子介面點選下筆
     */
    void openerByBtnDown();

    /**
     * 監聽子介面點選作廢
     */
    void openerByInvalid();

    /**
     * 監聽子介面編輯資訊
     */
    void openerByEditContent();

    /**
     * 監聽子介面執行追蹤
     */
    void openerByTrace();

    /**
     * 監聽子介面執行收藏變更
     */
    void openerByFavorite();
}
