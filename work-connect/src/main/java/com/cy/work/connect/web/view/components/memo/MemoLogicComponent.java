/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.web.view.components.memo;

import com.cy.security.utils.SecurityFacade;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.WCMemoTraceHelper;
import com.cy.work.connect.logic.manager.WCMemoMasterManager;
import com.cy.work.connect.logic.manager.WCMemoTraceManager;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.vo.WCMemoMaster;
import com.cy.work.connect.vo.WCMemoTrace;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.converter.to.ReadRecord;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCMemoStatus;
import com.cy.work.connect.vo.enums.WCReadStatus;
import com.cy.work.connect.web.common.CaseNoGenericService;
import com.cy.work.connect.web.logic.components.WCMemoCategoryLogicComponent;
import com.cy.work.connect.web.view.vo.to.MemoMasterTo;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 備忘錄 邏輯
 *
 * @author kasim
 */
@Component
@Slf4j
public class MemoLogicComponent implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4089198447233806897L;

    private static MemoLogicComponent instance;
    @Autowired
    private WCMemoMasterManager memoMasterManager;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WCMemoCategoryLogicComponent wcMemoCategoryLogicComponent;
    @Autowired
    private WCMemoTraceHelper wcMemoTraceHelper;
    @Autowired
    private WCMemoTraceManager wcMemoTraceManager;

    public static MemoLogicComponent getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MemoLogicComponent.instance = this;
    }

    public boolean isTraned(String memoSid) {
        WCMemoMaster obj = memoMasterManager.findBySid(memoSid);
        Preconditions.checkState(obj != null, "取得工作備忘錄失敗！！");
        return obj.getTransWcSid() != null
            && obj.getTransWcSid().getWcSidTos() != null
            && !obj.getTransWcSid().getWcSidTos().isEmpty();
    }

    /**
     * 將該使用者在該備忘錄更新成已閱讀
     *
     * @param memoSid      備忘錄Sid
     * @param loginUserSid 登入者Sid
     */
    public void readWCMemoMaster(String memoSid, Integer loginUserSid) {
        try {
            WCMemoMaster wc = memoMasterManager.findBySid(memoSid);
            wc.setReadRecord(settingReadInfo(wc, loginUserSid));
            memoMasterManager.updateOnlyWCMemoMaster(wc);
        } catch (Exception e) {
            log.warn("readWCMemoMaster Error", e);
        }
    }

    private ReadRecord settingReadInfo(WCMemoMaster wc, Integer loginUserSid) {
        // 閱讀紀錄
        ReadRecord readRecord = new ReadRecord();
        if (wc.getReadRecord() != null && wc.getReadRecord().getRRecord() != null) {
            wc.getReadRecord()
                .getRRecord()
                .forEach(
                    item -> {
                        if (item.getReader().equals(String.valueOf(loginUserSid))
                            && !item.getRead().equals(WCReadStatus.NEEDREAD.name())) {
                            item.setReadDay(
                                ToolsDate.transDateToString(
                                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
                            item.setRead(WCReadStatus.HASREAD.name());
                        }
                        readRecord.getRRecord().add(item);
                    });
        }
        List<RRcordTo> loginReadInfo =
            readRecord.getRRecord().stream()
                .filter(each -> each.getReader().equals(String.valueOf(loginUserSid)))
                .collect(Collectors.toList());
        if (loginReadInfo == null || loginReadInfo.isEmpty()) {
            readRecord
                .getRRecord()
                .add(
                    new RRcordTo(
                        String.valueOf(loginUserSid),
                        WCReadStatus.HASREAD.name(),
                        ToolsDate.transDateToString(
                            SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())));
        }
        return readRecord;
    }

    /**
     * 新增備忘錄
     *
     * @param theme
     * @param content
     * @param memo
     * @param atts
     * @param compId
     * @param comp
     * @param dep
     * @param user
     * @return
     * @throws InterruptedException
     * @throws UserMessageException 
     */
    public String create(
        String theme,
        String content,
        String memo,
        List<AttachmentVO> atts)
        throws UserMessageException {
        // 清除特殊字元
        theme = WkStringUtils.removeCtrlChr(theme);
        content = WkStringUtils.removeCtrlChr(content);
        memo = WkStringUtils.removeCtrlChr(memo);

        this.checkSave(theme, content, memo);
        WCMemoMaster wcMemoMaster = new WCMemoMaster();
        wcMemoMaster.setWcMemoNo(CaseNoGenericService.generateMemoNum(SecurityFacade.getCompanyId()));
        wcMemoMaster.setWcMemoCategorySid(wcMemoCategoryLogicComponent.getMemoCategorySidByLoginUser());
        wcMemoMaster.setComp_sid(SecurityFacade.getCompanySid());
        wcMemoMaster.setDep_sid(SecurityFacade.getPrimaryOrgSid());
        wcMemoMaster.setCreate_usr(SecurityFacade.getUserSid());
        wcMemoMaster.setCreate_dt(new Date());
        wcMemoMaster.setUpdate_usr(SecurityFacade.getUserSid());
        wcMemoMaster.setUpdate_dt(wcMemoMaster.getCreate_dt());
        wcMemoMaster.setWcMomeStatus(WCMemoStatus.NEW_INSTANCE);
        wcMemoMaster.setTheme(theme);
        wcMemoMaster.setContent_css(content);
        wcMemoMaster.setContent(jsoupUtils.clearCssTag(wcMemoMaster.getContent_css()));
        wcMemoMaster.setMemo_css(memo);
        wcMemoMaster.setMemo(jsoupUtils.clearCssTag(wcMemoMaster.getMemo_css()));
        wcMemoMaster = WCMemoMasterManager.create(SecurityFacade.getCompanyId(), wcMemoMaster, atts);
        Preconditions.checkState(wcMemoMaster != null, "新增工作備忘錄失敗！！");
        return wcMemoMaster.getSid();
    }

    /**
     * 更新備忘錄
     *
     * @param memoSid
     * @param theme
     * @param content
     * @param memo
     * @param user
     */
    public void update(String memoSid, String theme, String content, String memo, Integer user) {
        // 清除特殊字元
        theme = WkStringUtils.removeCtrlChr(theme);
        content = WkStringUtils.removeCtrlChr(content);
        memo = WkStringUtils.removeCtrlChr(memo);

        this.checkSave(theme, content, memo);
        WCMemoMaster obj = memoMasterManager.findBySid(memoSid);
        List<WCMemoTrace> wcMemoTraces = Lists.newArrayList();
        // 檢測標題是否有變更,若有變更需加入追蹤紀錄
        if (!obj.getTheme().equals(theme)) {
            wcMemoTraces.add(
                wcMemoTraceHelper.getWCMemoTrace_MODIFY_THEME(
                    obj.getTheme(), theme, user, obj.getSid(), obj.getWcMemoNo()));
        }
        // 檢測內容是否有變更,若有變更需加入追蹤紀錄
        if (!obj.getContent_css().equals(content)) {
            wcMemoTraces.add(
                wcMemoTraceHelper.getWCMemoTrace_MODIFY_CONTNET(
                    obj.getContent(), content, user, obj.getSid(), obj.getWcMemoNo()));
        }
        // 檢測備註是否有變更,若有變更需加入追蹤紀錄
        if (!obj.getMemo_css().equals(memo)) {
            wcMemoTraces.add(
                wcMemoTraceHelper.getWCMemoTrace_MODIFY_MEMO(
                    obj.getMemo(), memo, user, obj.getSid(), obj.getWcMemoNo()));
        }
        obj.setUpdate_usr(user);
        obj.setUpdate_dt(new Date());
        obj.setTheme(theme);
        obj.setContent_css(content);
        obj.setContent(jsoupUtils.clearCssTag(obj.getContent_css()));
        obj.setMemo_css(memo);
        obj.setMemo(jsoupUtils.clearCssTag(obj.getMemo_css()));
        obj.setLock_usr(null);
        obj.setLock_dt(null);
        memoMasterManager.save(obj);
        if (wcMemoTraces.size() > 0) {
            this.updateWaitReadWCMaster(memoSid, user);
        }
        wcMemoTraces.forEach(
            item -> {
                wcMemoTraceManager.create(item, user);
            });
    }

    /**
     * 將該工作聯絡單閱讀狀態都更改為待閱讀(除了登入者以外)
     *
     * @param memoSid      工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     */
    private void updateWaitReadWCMaster(String memoSid, Integer loginUserSid) {
        WCMemoMaster obj = memoMasterManager.findBySid(memoSid);
        List<RRcordTo> rRcordTos = Lists.newArrayList();
        if (obj.getReadRecord() != null && obj.getReadRecord().getRRecord() != null) {
            obj.getReadRecord()
                .getRRecord()
                .forEach(
                    item -> {
                        if (!item.getReader().equals(String.valueOf(loginUserSid))) {
                            item.setRead(WCReadStatus.WAIT_READ.name());
                        } else {
                            item.setRead(WCReadStatus.HASREAD.name());
                            item.setReadDay(
                                ToolsDate.transDateToString(
                                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
                        }
                        rRcordTos.add(item);
                    });
        }

        List<RRcordTo> loginReaderList =
            rRcordTos.stream()
                .filter(each -> each.getReader().equals(String.valueOf(loginUserSid)))
                .collect(Collectors.toList());
        if (loginReaderList == null || loginReaderList.isEmpty()) {
            RRcordTo rRcordTo = new RRcordTo();
            rRcordTo.setRead(WCReadStatus.HASREAD.name());
            rRcordTo.setReadDay(
                ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
            rRcordTo.setReader(String.valueOf(loginUserSid));
            rRcordTos.add(rRcordTo);
        }
        ReadRecord readRecord = new ReadRecord();
        readRecord.setRRecord(rRcordTos);
        obj.setReadRecord(readRecord);
        memoMasterManager.updateOnlyWCMemoMaster(obj);
    }

    /**
     * 備忘錄 存檔前檢核
     *
     * @param theme
     * @param content
     * @param memo
     */
    public void checkSave(String theme, String content, String memo) {
        Preconditions.checkState(
            !StringUtils.isBlank(jsoupUtils.clearCssTag(theme)), "[主題]為必要輸入資訊，請確認！！");
        Preconditions.checkState(theme.length() <= 255, "主題字數超過255個字的限制，請重新輸入！！");
        Preconditions.checkState(
            !StringUtils.isBlank(jsoupUtils.clearCssTag(content)), "[內容]為必要輸入資訊，請確認！！");
    }
    /**
     * 取得資料
     *
     * @param sid
     * @return
     */
    public MemoMasterTo findToBySid(String sid) {
        WCMemoMaster obj = memoMasterManager.findBySid(sid);
        if (obj == null) {
            return null;
        }
        MemoMasterTo result = new MemoMasterTo();
        result.setSid(obj.getSid());
        result.setMemoNo(obj.getWcMemoNo());
        result.setCreateDep(obj.getDep_sid());
        result.setCreateUser(obj.getCreate_usr());
        result.setCreateDate(obj.getCreate_dt());
        result.setTheme(obj.getTheme());
        result.setContentCss(obj.getContent_css());
        result.setMemoCss(obj.getMemo_css());
        result.setUpdateUser(obj.getUpdate_usr());
        result.setUpdateDate(obj.getUpdate_dt());
        result.setLockUser(obj.getLock_usr());
        result.setLockDate(obj.getLock_dt());
        result.setWcMemoCategorySid(obj.getWcMemoCategorySid());
        return result;
    }

    /**
     * 鎖定備忘錄
     *
     * @param memoSid
     * @param user
     * @param lockDate
     */
    public void lock(String memoSid, Integer user, Date lockDate) {
        memoMasterManager.lock(memoSid, user, lockDate);
    }

    /**
     * 解除鎖定備忘錄
     *
     * @param memoSid
     * @param user
     */
    public void unLock(String memoSid, Integer user) {
        memoMasterManager.unLock(memoSid, user);
    }
}
