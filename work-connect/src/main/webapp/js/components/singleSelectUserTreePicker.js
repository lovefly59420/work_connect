/**
 * 
 */
function SingleSelectUserTreePicker(componentID, doubleClickFunction) {

	var self = this;

	// 前次擊點節點
	var prevClickRowkey = "";
	// 前次擊點時間
	var prevClickTime = 0;

	this.nodeDoubleClick = function(node) {
		//防呆
		if (commonUtil.isNull(node)) {
			return;
		}

		//排除非可選 node
		var treenodeLabels = $(node).find(".ui-treenode-label");
		if (treenodeLabels.length == 0) {
			self.expandOrCollapseNode(node);
			return;
		}
		var userNode = $(treenodeLabels[0]).find(".ssTree-dataNode-highlight");
		if (userNode.length == 0) {
			self.expandOrCollapseNode(node);
			return;
		}
		
		var isImplDoubleClickCallBack = "true" == $("#sstreeStyle_" + componentID + "_hasCallback").val();

		//有實做雙擊人員事件時，才觸發判斷
		if (isImplDoubleClickCallBack && common_isFunc(doubleClickFunction)) {
			// 以下判斷為雙擊
			// 取得目前時間
			var nowTime = Date.now();
			//取得選擇節點
			var selectRowKey = $(node).attr('data-rowkey');

			// 判斷須為上次擊點的 node , 且間隔時間不大於定值
			var intervalTime = nowTime - prevClickTime;
			var result = (selectRowKey == prevClickRowkey && intervalTime <= 1000);

			// 更新本次擊點狀態
			prevClickRowkey = selectRowKey;
			prevClickTime = nowTime;

			//呼叫雙擊事件
			if (result) {
				doubleClickFunction();
			}

		}
	};

	this.expandOrCollapseNode = function(targetNode) {
		
		var treeWidget = PF("itemTree_wv_" + componentID);
		
		if (treeWidget == null) {
			return;
		}
		var treeToggler = self.getTreeNodeToggler(targetNode);
		if (treeToggler == null) {
			return;
		}

		var isInExpand = treeToggler.hasClass("ui-icon-triangle-1-s");
		if (isInExpand) {
			treeWidget.collapseNode(targetNode);
		} else {
			treeWidget.expandNode(targetNode);
		}
	};

	this.getTreeNodeToggler = function(targetNode) {
		if (targetNode == null) {
			return null;
		}
		var togglerElm = targetNode.children(".ui-treenode-content").children(".ui-tree-toggler");
		if (togglerElm.length != 0) {
			return togglerElm;
		}
		return null;
	}
}


SingleSelectUserTreePicker.prototype = {
	constructor: SingleSelectUserTreePicker
}