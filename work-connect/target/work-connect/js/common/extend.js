/**
 * startsWith (for safari)
 */
if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        value: function (search, pos) {
            pos = !pos || pos < 0 ? 0 : +pos;
            return this.substring(pos, pos + search.length) === search;
        }
    });
}

/**
 * includes (for safari)
 */
if (!String.prototype.includes) {
    Object.defineProperty(String.prototype, 'includes', {
        value: function (search, start) {
            if (typeof start !== 'number') {
                start = 0
            }
            search = search + "";
            if (start + search.length > this.length) {
                return false
            } else {
                return this.indexOf(search, start) !== -1
            }
        }
    })
}

//String 擴充方法 ,判斷是否為空
if (typeof String.prototype.isEmpty != 'function') {
    String.prototype.isEmpty = function () {
        return (this == null || this.length === 0 || !this.trim());
    };
}
