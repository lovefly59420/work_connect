/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.config;

import javax.annotation.PostConstruct;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author kasim
 */
@Slf4j
@Data
@Component
public class WCProperties {

    @Value("${work-connect.db.driverClassName}")
    private String className;

    @Value("${work-connect.db.url}")
    private String dbUrl;

    @Value("${work-connect.username}")
    private String dbUserName;

    @Value("${work-connect.db.password}")
    private String dbPassword;

    @Value("${work-connect.db.dialect.hibernate}")
    private String dialect;

    @Value("${work-connect.db.hibernate.show_sql:false}")
    private boolean show_sql;

    @Value("${work-connect.db.maxPoolSize:50}")
    private int maxPoolSize;

    @Value("${work-connect.db.minPoolSize:10}")
    private int minimumIdle;

    @Value("${work-connect.db.maxStatements:60}")
    private int maxStatements;

    @Value("${work-connect.db.idleTimeout}")
    private int idleTimeout;

    @Value("${work-connect.db.maxLifetime}")
    private int maxLifetime;

    @Value("${work-connect.db.cachePrepStmts}")
    private String cachePrepStmts;

    @Value("${work-connect.db.useServerPrepStmts}")
    private String useServerPrepStmts;

    @Value("${work-connect.db.prepStmtCacheSize}")
    private String prepStmtCacheSize;

    @Value("${work-connect.db.prepStmtCacheSqlLimit}")
    private String prepStmtCacheSqlLimit;

    @PostConstruct
    void init() {
        log.info("WCProperties startup , display default value ..");
        log.info("className = " + className);
        log.info("dbUrl = " + dbUrl);
        //log.info("dbUserName = " + dbUserName.substring(0, 2) + ".....");
        //log.info("dbPassword = " + dbPassword.substring(0, 2) + ".....");
        log.info("dialect = " + dialect);
        log.info("show_sql = " + show_sql);
    }
}
