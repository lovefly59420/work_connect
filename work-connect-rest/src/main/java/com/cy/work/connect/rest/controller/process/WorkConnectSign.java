/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.controller.process;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cy.bpm.rest.to.HistoryTaskTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.commons.vo.User;
import com.cy.formsigning.enums.FormSigningType;
import com.cy.formsigning.enums.FormType;
import com.cy.formsigning.enums.LanguageType;
import com.cy.formsigning.enums.QueryType;
import com.cy.formsigning.exceptions.FormSigningException;
import com.cy.formsigning.rest.impl.AbstractFormSignRestService;
import com.cy.formsigning.vo.BatchSignedResult;
import com.cy.formsigning.vo.ButtonInfo;
import com.cy.formsigning.vo.ButtonInfoWithOneSelectMenuAndOneTextarea;
import com.cy.formsigning.vo.ButtonInfoWithOneTextarea;
import com.cy.formsigning.vo.FormInfo;
import com.cy.formsigning.vo.TodoSummary;
import com.cy.formsigning.vo.TodoSummaryQueryParam;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.signflow.ExecFlowActionHelper;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.rest.logic.helper.WCManagerSignInfoRestCheckHelper;
import com.cy.work.connect.rest.logic.service.BpmService;
import com.cy.work.connect.rest.logic.service.SignService;
import com.cy.work.connect.rest.util.WCRestUtil;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * Portal 待簽
 *
 * @author brain0925_liao
 */
@RestController
@RequestMapping("/process/wc")
@Slf4j
public class WorkConnectSign extends AbstractFormSignRestService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5020700224766152010L;

    @Autowired
    private SignService signService;
    @Autowired
    private WCRestUtil wcRestUtil;

    // @Autowired
    // private ReqFlowActionHelper reqFlowActionHelper;

    @Autowired
    private ExecFlowActionHelper execFlowActionHelper;
    @Autowired
    private WCMasterManager wcMasterManager;

    @Autowired
    private WCManagerSignInfoRestCheckHelper managerSignInfoCheckHelper;

    public WorkConnectSign() {
        super(WorkConnectSign.class);
    }

    public WorkConnectSign(Class<? extends AbstractFormSignRestService> implClass) {
        super(implClass);
    }

    @Override
    public FormSigningType getFormSigningTypeImpl(
            FormType formType,
            String formId,
            String instanceId,
            String executorUuid) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("formType", formType);
        paramMap.put("formId", formId);
        paramMap.put("instanceId", instanceId);
        paramMap.put("executorUuid", executorUuid);

        // ====================================
        // 執行
        // ====================================
        FormSigningType formSigningType = FormSigningType.NOT_SIGNER;
        try {
            formSigningType = signService.getFormSigningType(formId, executorUuid);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return formSigningType;
    }

    @Override
    public FormInfo getFormImpl(
            FormType formType, String formId, String instanceId, String executorUuid,
            LanguageType lang) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("formType", formType);
        paramMap.put("formId", formId);
        paramMap.put("instanceId", instanceId);
        paramMap.put("executorUuid", executorUuid);
        paramMap.put("lang", lang);

        // ====================================
        // 執行
        // ====================================
        FormInfo result = new FormInfo();
        try {
            result = this.signService.getFormInfo(formId, executorUuid);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return result;
    }

    @Override
    public List<FormInfo> getFormsOfUnsingingImpl(
            Map<FormType, List<ProcessTask>> tasks,
            String executorUuid,
            Integer filteredCompanySid,
            QueryType queryType,
            LanguageType lang) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("tasks", tasks);
        paramMap.put("executorUuid", executorUuid);
        paramMap.put("filteredCompanySid", filteredCompanySid);
        paramMap.put("queryType", queryType);
        paramMap.put("lang", lang);

        // ====================================
        // 執行
        // ====================================
        List<FormInfo> results = Lists.newArrayList();
        try {

            results = signService.getUnsignFormInfo(
                    executorUuid,
                    filteredCompanySid);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return results;
    }

    @Override
    public List<FormInfo> getFormsOfSignedImpl(
            Map<FormType, List<HistoryTaskTo>> historyTasks,
            String executorUuid,
            Integer filteredCompanySid,
            Date from,
            Date to,
            LanguageType lang,
            QueryType queryType) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("historyTasks", historyTasks);
        paramMap.put("executorUuid", executorUuid);
        paramMap.put("filteredCompanySid", filteredCompanySid);
        paramMap.put("from", from);
        paramMap.put("to", to);
        paramMap.put("lang", lang);
        paramMap.put("queryType", queryType);

        // ====================================
        // 執行
        // ====================================
        List<FormInfo> results = Lists.newArrayList();
        try {
            results = signService.getSignedFormInfo(executorUuid, filteredCompanySid, from, to);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return results;

    }

    @Override
    public List<ButtonInfo> getButtonsImpl(
            FormType formType, String formId, String instanceId, String executorUuid,
            LanguageType lang) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("formType", formType);
        paramMap.put("formId", formId);
        paramMap.put("instanceId", instanceId);
        paramMap.put("executorUuid", executorUuid);
        paramMap.put("lang", lang);

        // ====================================
        // 執行
        // ====================================
        List<ButtonInfo> results = Lists.newArrayList();
        try {
            results = signService.createFormBtn(formId, executorUuid);
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return results;

    }

    @Override
    public List<BatchSignedResult> batchSigningImpl(
            Map<FormType, List<FormInfo>> formInfos, String executorUuid, LanguageType lang) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("formInfos", formInfos);
        paramMap.put("executorUuid", executorUuid);
        paramMap.put("lang", lang);

        // ====================================
        // 執行
        // ====================================
        List<BatchSignedResult> results = Lists.newArrayList();
        try {

            List<String> formIds = Lists.newArrayList();
            formInfos
                    .get(FormType.WORK_CONNENT_SUMMARY)
                    .forEach(
                            item -> {
                                formIds.add(item.getFormId());
                            });

            results.add(signService.doBatchSign(formIds, executorUuid));
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
            results = Lists.newArrayList(new BatchSignedResult(0, FormType.WORK_CONNENT_SUMMARY));
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return results;

    }

    @Override
    public FormInfo doActionImpl(
            FormType formType,
            String action,
            Map<String, Object> inputData,
            String executorUuid,
            LanguageType lang,
            Map<String, String> custParams) throws FormSigningException {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("formType", formType);
        paramMap.put("action", action);
        paramMap.put("inputData", inputData);
        paramMap.put("executorUuid", executorUuid);
        paramMap.put("lang", lang);
        paramMap.put("custParams", custParams);

        // ====================================
        // 執行
        // ====================================
        FormInfo result = new FormInfo();
        try {
            result = this.signService.doActionImpl(action, inputData, executorUuid);
        } catch (UserMessageException e) {
            // form sign 不允許 HTML 內容 , 故轉為 text
            String userMessage = WkJsoupUtils.getInstance().htmlToText(e.getMessage());
            // 轉為 FormSigningException , 交由公共方法處理
            throw new FormSigningException(e.getExceptionMessage(), userMessage);
        } catch (Exception e) {
            // 轉為 FormSigningException , 交由公共方法處理
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
            throw new FormSigningException(e.getMessage(), WkMessage.PROCESS_FAILED, e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return result;

    }

    @Override
    public Map<String, Map<QueryType, List<TodoSummary>>> getTodoSummaryImpl(
            TodoSummaryQueryParam param) {

        // ====================================
        // 準備參數
        // ====================================
        long startTime = System.currentTimeMillis();

        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("param", param);

        // ====================================
        // 執行
        // ====================================
        Map<String, Map<QueryType, List<TodoSummary>>> result = Maps.newConcurrentMap();
        try {
            User user = WkUserCache.getInstance().findByUUID(param.getExecutorUuid());
            Preconditions.checkArgument(
                    user != null, "傳送的成員uuid，無法訪查出有效成員資訊 uuid:" + param.getExecutorUuid());
            result = signService.getUnSignCount(user.getId());
        } catch (Exception e) {
            log.error(this.wcRestUtil.prepareExceptionLogMessage(e, paramMap), e);
        }

        // ====================================
        // log
        // ====================================
        this.wcRestUtil.showCostTimeLog(startTime, paramMap);;

        return result;
    }

    /**
     * 取得需求單位簽核水管圖
     * 
     * @param wcNo 單號
     * @return
     */
    @ApiOperation(value = "取得需求單位簽核水管圖")
    @RequestMapping(value = "/testFindReqUnitSignFlowTasks", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<ProcessTaskBase>> testReqUnitSignFlowTasks(
            @RequestParam(value = "wcNo") String wcNo) {
        try {

            List<WCMaster> wcMaster = wcMasterManager.findByNo(wcNo);
            if (WkStringUtils.isEmpty(wcMaster)) {
                throw new Exception("找不到單號:[" + wcNo + "]");
            }

            WCManagerSignInfo signInfo = managerSignInfoCheckHelper.getReqFlow(wcMaster.get(0).getSid());
            List<ProcessTaskBase> tasks = BpmService.getInstance().findSimulationChart(
                    "admin", signInfo.getBpmId());

            return new ResponseEntity<List<ProcessTaskBase>>(tasks, HttpStatus.OK);
        } catch (Exception e) {
            log.error(WkMessage.EXECTION + e.getMessage(), e);
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 需求單位簽核 for API 測試
     *
     * @param depSid  部門 SID
     * @param userSid 使用者SId
     * @return WCTodoTo list
     */
    @ApiOperation(value = "測試需求單位簽核")
    @RequestMapping(value = "/testReqSign", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<HttpStatus> findAllToDo(
            @RequestParam(value = "wcNo") String wcNo,
            @RequestParam(value = "userId") String userId) {
        try {
            User signUser = WkUserCache.getInstance().findById(userId);
            if (signUser == null) {
                return WCRestUtil.createErrorResponse("不存在的User! userId:[" + userId + "]");
            }

            List<WCMaster> wcMasters = wcMasterManager.findByNo(wcNo);
            if (wcMasters == null || wcMasters.size() > 1) {
                return WCRestUtil.createErrorResponse("不存在的單號，或單號重複! wcNo:[" + wcNo + "]");
            }

            this.signService.doReqflowSign(wcMasters.get(0).getSid(), signUser);

            return new ResponseEntity<HttpStatus>(HttpStatus.SEE_OTHER);
        } catch (Exception e) {
            log.error(WkMessage.EXECTION + e.getMessage(), e);
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }

    /**
     * 執行單位退件 for API 測試
     *
     * @param depSid  部門 SID
     * @param userSid 使用者SId
     * @return WCTodoTo list
     */
    @ApiOperation(value = "測試『執行方-簽核者退件』")
    @RequestMapping(value = "/testExecRollback", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<HttpStatus> testExecRollback(
            @RequestParam(value = "userId") String userId,
            @RequestParam(value = "wcNo") String wcNo,
            @RequestParam(value = "taskID") String taskID) {
        try {
            User signUser = WkUserCache.getInstance().findById(userId);
            if (signUser == null) {
                return WCRestUtil.createErrorResponse("不存在的User! userId:[" + userId + "]");
            }

            List<WCMaster> wcMasters = wcMasterManager.findByNo(wcNo);
            if (wcMasters == null || wcMasters.size() > 1) {
                return WCRestUtil.createErrorResponse("不存在的單號，或單號重複! wcNo:[" + wcNo + "]");
            }

            Map<String, Object> inputData = Maps.newHashMap();
            inputData.put(ButtonInfoWithOneTextarea.INPUT_TEXTAREA, "退回原因-" + WkDateUtils.formatDate(new Date(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss2));
            inputData.put(ButtonInfoWithOneSelectMenuAndOneTextarea.SELECT_ITEMS, taskID);

            this.execFlowActionHelper.doExecFlowRollBack(
                    wcMasters.get(0).getSid(),
                    signUser.getSid(),
                    taskID,
                    "退回原因-" + WkDateUtils.formatDate(new Date(), WkDateUtils.YYYY_MM_DD_HH24_mm_ss2));
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } catch (Exception e) {
            log.error(WkMessage.EXECTION + e.getMessage(), e);
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }

}
