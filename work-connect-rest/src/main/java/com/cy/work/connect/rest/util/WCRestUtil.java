package com.cy.work.connect.rest.util;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.cy.commons.util.Base64Utils;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WorkParamManager;

import lombok.extern.slf4j.Slf4j;

/**
 * Rest 工具
 *
 * @author jimmy_chou
 */
@Slf4j
@Component
public class WCRestUtil {

    @Autowired
    private WorkParamManager workParamManager;

    /**
     * 建立錯誤訊息
     *
     * @param errMsg
     * @return
     */
    public static <T> ResponseEntity<T> createErrorResponse(String errMsg) {
        log.warn(errMsg);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("error", Base64Utils.encode(errMsg));
        return new ResponseEntity<T>(responseHeaders, HttpStatus.SEE_OTHER);
    }

    /**
     * 兜組 exception log 訊息
     * 
     * @param e        exception
     * @param paramMap 方法執行參數
     * @return exception log 訊息
     */
    public String prepareExceptionLogMessage(Exception e, Map<String, Object> paramMap) {
        return WkMessage.EXECTION + e.getMessage()
                + "\r\n【"
                + WkJsonUtils.getInstance().toPettyJson(paramMap)
                + "\r\n】";
    }

    /**
     * 顯示執行時間 log
     * 
     * @param startTime 執行起始時間
     * @param paramMap  方法執行參數
     */
    public void showCostTimeLog(long startTime, Map<String, Object> paramMap) {

        // ====================================
        // 取得設定時間
        // ====================================
        Double responsetimeBound = workParamManager.findRestResponseTimeBound();
        // 未設定
        if (responsetimeBound == null) {
            return;
        }
        String responseTime = this.calcSec(responsetimeBound);
        if (WkStringUtils.isEmpty(responseTime)) {
            return;
        }

        // ====================================
        // 計算消耗時間
        // ====================================
        String execueTime = WkCommonUtils.calcCostSec(startTime);
        if (WkStringUtils.isEmpty(execueTime)) {
            return;
        }

        // ====================================
        // 判斷是否需要列印 log
        // ====================================
        // 執行時間，小於設定時間，不顯示
        if ((new BigDecimal(responseTime)).compareTo(new BigDecimal(execueTime)) > 0) {
            return;
        }

        // ====================================
        // 取得 caller 資訊
        // ====================================
        StackTraceElement[] stacks = new Exception().getStackTrace();
        if (stacks == null || stacks.length < 1) {
            log.warn("取不到 Exception().getStackTrace() ?");
            return;
        }
        StackTraceElement callerStack = stacks[stacks.length - 1];
        // 呼叫 class
        String className = callerStack.getClassName();
        // 呼叫方法
        String methodName = callerStack.getMethodName();

        // ====================================
        // log
        // ====================================
        String messageTemplet = "執行時間大於設定值!"
                + "\r\n 方法：[{}]->[{}]"
                + "\r\n 執行參數：【{}\r\n】";

        log.info(messageTemplet,
                className,
                methodName,
                WkJsonUtils.getInstance().toPettyJson(paramMap));
    }

    /**
     * 計算回應秒數
     *
     * @param responseTime
     * @return
     */
    public String calcSec(Double responseTime) {
        if (responseTime == null) {
            return "";
        }
        responseTime = responseTime / 1000;
        return responseTime + "";
    }
}
