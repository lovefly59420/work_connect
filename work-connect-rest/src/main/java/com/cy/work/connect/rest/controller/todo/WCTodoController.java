/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.controller.todo;

import com.cy.work.connect.rest.logic.service.WCTodoService;
import com.cy.work.connect.rest.logic.to.WCTodoTo;
import com.cy.work.connect.rest.util.WCRestUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 待辦事項
 *
 * @author kasim
 */
@Api(tags = "WCTodoController", description = "Portal 待辦事項")
@RestController
@RequestMapping("/todo")
public class WCTodoController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3951386132449233599L;

    @Autowired
    private WCTodoService todoService;

    /**
     * 取回待辦事項
     *
     * @param depSid  部門 SID
     * @param userSid 使用者SId
     * @return WCTodoTo list
     */
    @ApiOperation(value = "取回待辦事項")
    @RequestMapping(
        value = "/findAllToDo",
        method = RequestMethod.GET,
        produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WCTodoTo>> findAllToDo(
        @RequestParam(value = "depSid") Integer depSid,  //dep sid 已無需使用. 但 client 未改, 故先保留
        @RequestParam(value = "userSid") Integer userSid) {
        try {
            return new ResponseEntity<List<WCTodoTo>>(
                todoService.syncAllToDo(userSid), HttpStatus.OK);
        } catch (Exception e) {
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }
    
    @ApiOperation(value = "取回待辦事項(測試用)")
    @RequestMapping(
        value = "/findAllToDoForTest",
        method = RequestMethod.GET,
        produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<WCTodoTo>> findAllToDoForTest(
        @RequestParam(value = "userSid") Integer userSid) {
        try {
            return new ResponseEntity<List<WCTodoTo>>(
                todoService.syncAllToDo(userSid), HttpStatus.OK);
        } catch (Exception e) {
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }
}
