package com.cy.work.connect.rest.controller.process;

import com.cy.system.rest.client.util.SystemProjectCacheUtils;
import com.cy.work.connect.rest.util.WCRestUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 清除快取
 *
 * @author jimmy_chou
 */
@Api(tags = "WCCacheClearController", description = "清除快取")
@Slf4j
@RestController
@RequestMapping("/cache")
public class WCCacheClearController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2870101187020454902L;

    @ApiOperation(value = "清除REST快取")
    @RequestMapping(value = "/clear", method = RequestMethod.GET)
    public ResponseEntity<HttpStatus> clearAllCache() {
        try {
            log.info("receive clear cache signal.");
            SystemProjectCacheUtils.getInstance().doClearSystemProjectCache();
            log.info("receive clear cache signal done.");
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } catch (Exception e) {
            return WCRestUtil.createErrorResponse(e.getMessage());
        }
    }
}
