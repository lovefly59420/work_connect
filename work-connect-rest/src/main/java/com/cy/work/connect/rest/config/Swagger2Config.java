package com.cy.work.connect.rest.config;

import com.cy.commons.util.WebVersion;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * API Doc 設定
 *
 * @author jimmy_chou
 */
@Configuration
@EnableSwagger2
@Component
public class Swagger2Config {

    /**
     * 檢查測試 API
     *
     * @return
     */
    @Bean
    public Docket createmonitorDocket() {
        String groupName = "檢查測試";
        return this.createApiDocket(
            "0." + groupName, this.apiInfo(groupName), "com.cy.work.connect.rest.controller.check");
    }

    /**
     * 待辦事項 API
     *
     * @return
     */
    @Bean
    public Docket createPortalTaskDocket() {
        String groupName = "Portal 代辦事項";
        return this.createApiDocket(
            "1." + groupName, this.apiInfo(groupName), "com.cy.work.connect.rest.controller.todo");
    }

    /**
     * 最新消息 API
     *
     * @return
     */
    @Bean
    public Docket createPortalAlertDocket() {
        String groupName = "Portal 最新消息";
        return this.createApiDocket(
            "2." + groupName, this.apiInfo(groupName), "com.cy.work.connect.rest.controller.alert");
    }

    /**
     * 待簽 & 快取清除 & 回應測試 API
     *
     * @return
     */
    @Bean
    public Docket createProcessDocket() {
        String groupName = "Portal 待簽";
        return this.createApiDocket(
            "3." + groupName, this.apiInfo(groupName),
            "com.cy.work.connect.rest.controller.process");
    }

    /**
     * 創建API應用. apiInfo() 增加API相關信息 通過select()函數返回一個ApiSelectorBuilder實例,用來控制哪些接口暴露給Swagger來展現，
     * 本例採用指定掃描的包路徑來定義指定要建立API的目錄。
     *
     * @param groupName   分類名稱
     * @param apiInfo     API相關信息
     * @param basePackage 掃描的Package paths
     * @return Docket
     * @author Jimmy
     */
    private Docket createApiDocket(
        final String groupName, final ApiInfo apiInfo, final String basePackage) {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName(groupName)
            .apiInfo(apiInfo)
            .select()
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
            .apis(RequestHandlerSelectors.basePackage(basePackage))
            .paths(PathSelectors.any())
            .build();
    }

    /**
     * 建立API資訊.
     *
     * @param title - title for the API
     * @return API資訊
     * @author Jimmy
     */
    private ApiInfo apiInfo(final String title) {
        return new ApiInfoBuilder()
            .title(title)
            .description("相關說明")
            .contact(new Contact("jimmy", null, null))
            .version(WebVersion.getInstance().getWebVersion())
            .build();
    }
}
