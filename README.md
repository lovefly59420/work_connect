- RELEASE NOTE

## 1.64.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/16354)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.64.1)
- <span style="color:red;">security【1.26.0】</span>
- <span style="color:red;">work-common【2.38.0】</span>
- <span style="color:red;">work-connect-client【1.7.0】</span>

> by Jimmy

## 1.64.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/16287)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.64.0)
- <span style="color:red;">work-common【2.37.0】</span>

> by Jimmy

## 1.63.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/16139)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.63.0)
- <span style="color:red;">primefaces【8.0】</span>
- <span style="color:red;">poi【3.15】</span>
- <span style="color:red;">poi-ooxml【3.15】</span>
- <span style="color:red;">poi-scratchpad【3.15】</span>
- <span style="color:red;">commons-util【0.0.12】</span>
- <span style="color:red;">security【1.24.0】</span>
- <span style="color:red;">work-common【2.35.0】</span>

> by Jimmy

## 1.62.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/16195)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.62.1)

> by Jimmy

## 1.62.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/16116)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.62.0)

> by Jimmy

## 1.61.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/16071)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.61.0)
- <span style="color:red;">work-common【2.32.2】</span>

> by Jimmy

## 1.60.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15960)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.60.0)
- <span style="color:red;">spring-aop【4.2.5.RELEASE】</span>
- <span style="color:red;">guava【20.0】</span>
- <span style="color:red;">springfox-swagger2【2.9.2】</span>
- <span style="color:red;">springfox-swagger-ui【2.9.2】</span>

> by Jimmy

## 1.59.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15908)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.59.0)

> by Jimmy

## 1.58.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15925)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.58.1)

> by Jimmy

## 1.58.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15707)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.58.0)

> by Jimmy

## 1.57.2

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15808)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.57.2)

> by Jimmy

## 1.57.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15718)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.57.1)

> by Jimmy

## 1.57.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15693)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.57.0)

> by Jimmy

## 1.56.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15660)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.56.0)
- <span style="color:red;">work-common【2.30.0】</span>

> by Jimmy

## 1.55.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15461)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.55.0)
- <span style="color:red;">security【1.16.0】</span>
- <span style="color:red;">commons-util【0.0.11】</span>
- <span style="color:red;">work-common【2.27.0】</span>
- <span style="color:red;">work-logic【1.3.0】</span>

> by Jimmy

## 1.54.2

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15465)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.54.2)
- <span style="color:red;">bpm-rest-client【1.28.0】</span>

> by Jimmy

## 1.54.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15426)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.54.1)

> by Jimmy

## 1.54.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15390)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.54.0)

> by Jimmy

## 1.53.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15349)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.53.0)

> by Jimmy

## 1.52.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15328)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.52.1)
- <span style="color:red;">work-common【2.20.0】</span>
- <span style="color:red;">security【1.14.4】</span>

> by Jimmy

## 1.52.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15281)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.52.0)
- <span style="color:red;">合併REST</span>
- <span style="color:red;">work-common【2.17.0】</span>

> by Jimmy

## 1.51.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15242)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.51.0)
- <span style="color:red;">work-common【2.16.0】</span>
- <span style="color:red;">security【1.11.8】</span>

> by Jimmy

## 1.50.1

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15230)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.50.1)

> by Jimmy

## 1.50.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15208)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.50.0)
- <span style="color:red;">work-common【2.14.0】</span>
- <span style="color:red;">security【1.11.7】</span>

> by Jimmy

## 1.48.1

- 修正 orderList所有單據查詢 SQL

> by Allen

## 1.48.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15132)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.48.0)
- <span style="color:red;">work-common【2.12.0】</span>
- <span style="color:red;">commons-util【0.0.7】</span>
- <span style="color:red;">security【1.11.6.3】</span>
- <span style="color:red;">system-rest-client【3.3.1-SNAPSHOT】</span> (隨 work-common 升版 , 不另加在 pom)

> by Allen

## 1.47.1

- search5 組織樹 SQL 的 Like 換成 RLike

> by Jimmy

## 1.47.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/15109)
- [環境確認清單](http://confluence.iwerp.net:8090/display/AE/work-connect+version+1.47.0)

> by Jimmy

## 1.46.0

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/14807)
- [環境確認清單](http://confluence.iwerp.test:8090/display/AE/work-connect+version+1.46.0)

> by Jimmy

## 1.45.0

- WORKCOMMU-363 【所有報表】列表顯示欄位-增加控制字元過濾器

## 1.44.0 2019/10/28

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/14411)
- [環境確認清單](http://confluence.bb.com:8090/display/AE/work-connect+version+1.44.0)
- <span style="color:red;">work-common【2.10.0】</span> (2.9.0 會回傳已停用員工，所以再升版 )
- <span style="color:red;">commons-util【0.0.7-SNAPSHOT】</span>
- <span style="color:red;">security【1.11.4-SNAPSHOT】</span>
- <span style="color:red;">system-rest-client【3.1.0-SNAPSHOT】</span>

> by Thomas

## 1.43.0 2019/07/11

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/14508)
- [環境確認清單](http://confluence.bb.com:8090/display/AE/work-connect+version+1.43.1)

> by Allen

## 1.43.0 2019/06/27

- WORKCOMMU-337 簽到群級的單據, 若由群級的 user 簽核, 會直接掉執行方

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/14410)
- [環境確認清單](http://confluence.bb.com:8090/display/AE/work-connect+version+1.43.0)
- <span style="color:red;">work-common【2.7.8-SNAPSHOT】</span>

> by Allen

## 1.42.0 2019/03/07

- Improvement WORKCOMMU-331 升級 security 模組 1.11.3-SNAPSHOT
- Improvement WORKCOMMU-332 modify db connection to hikari
- Improvement WORKCOMMU-333 優化 logback 輸出資訊, 加入 user ID 和 session 識別資訊
- Bug WORKCOMMU-334 新增時點選項目, LOG 出現 : 建立類別選項 ERROR java.lang.NullPointerException: null

- <span style="color:red;">wsecurity【1.10.0-SNAPSHOT】->【1.11.3-SNAPSHOT】</span>
- <span style="color:red;">work-common【2.5.0】->【2.7.6】</span>
- <span style="color:red;">org.glassfish【2.2.10】->【2.2.15】</span>

- [明細清單](http://werp-gateway.chungyo.net:8080/projects/WORKCOMMU/versions/14280)

## 1.41.0

- WORKCOMMU-327 [BPM] 水管圖不撈取工作聯絡單的 TABLE, 改打 BPM REST

## 1.40.0

- WORKCOMMU-324 工作聯絡單的可使用單位/可閱部門/指派單位的部門多選顯示邏輯調整
- WORKCOMMU-325 工作聯絡單的可使用單位/可閱部門/指派單位的部門多選時增加勾選含以下功能
- WORKCOMMU-326 工作聯絡單的項目增加欄位：可使用單位，空白代表全公司皆可使用

## 1.39.1

- WORKCOMMU-323 工作聯絡單的加簽人員無法正常加入及顯示

## 1.39.0

- WORKCOMMU-317 提供查詢核決權限與執行單位的清單(新增三個欄位)
- SCM-61 [WORK-CONNECT] LOG 調整
- WORKCOMMU-322 工作聯絡單新增單據名稱/執行項目名稱時，可使用人員/可閱人員調整為非必填值

## 1.37.1

- WORKCOMMU-318 工作聯絡單的可使用單位、對象空值邏輯調整

## 1.37.0

- WORKCOMMU-313 工作聯絡單可使用單位及可閱單位增加可挑選多個同仁，派工人員可挑選多個單位的功能
- WORKCOMMU-312  [內部優化]工作聯絡單轉單自動展開左邊的組織樹
- WORKCOMMU-270 將工作聯絡單中主檔與簽核紀錄檔中的作廢字串更正為一致
- WORKCOMMU-274 提供查詢核決權限與執行單位的清單

## 1.36.0

- WORKCOMMU-144 會簽/加簽欄框中搜尋無結果不會提示user
- WORKCOMMU-168  [可閱單據查詢]點擊不顯示停用部門,會取消已選部門
- WORKCOMMU-243  [內部優化]工作聯絡單轉單希望可以提供人名搜尋
- WORKCOMMU-260 調整SQL-優化
- WORKCOMMU-282 人事主管設定的指定領單人員畫面,停用帳號需註記紅色(停用)
- WORKCOMMU-308 同一位主管可以審核多單位的單據

## 1.35.1

- WORKCOMMU-287 【備忘錄】主題欄位輸入空白字元時，單據可以成立
- WORKCOMMU-288 【備忘錄】主題欄位輸入超過255字時的系統提示有錯別字
- WORKCOMMU-290 【備忘錄】上傳50MB內的附加檔案，系統訊息"Invalid file size....etc"顯示
- WORKCOMMU-291 【備忘錄】上傳超過50MB的附加檔案，系統訊息"Invalid file size....etc"顯示，關閉視窗後再點選附加檔案，系統提示的錯誤訊息仍存在.Failed
  Rate:100%
- WORKCOMMU-295 【工作聯絡單查詢】模糊搜尋欄位，輸入"主題"括號內的內容都會找不到 Failed Rate:100%
- WORKCOMMU-298 【工作聯絡單加派】加派搜尋，搜尋結果與勾選"搜尋結果包含上層部門"的提示字不符 Failed Rate:100%
- WORKCOMMU-299 【工作聯絡單加簽】使用者新增一單據，追蹤紀錄出現"加簽人員異動"訊息 Failed Rate:100%

## 1.35.0

- WORKCOMMU-307 重構回覆功能筆數顯示邏輯

## 1.34.0

- WORKCOMMU-283 工作聯絡單追蹤頁籤顯示加簽資訊
- WORKCOMMU-284 加簽者可以取消加簽

## 1.33.0

- WORKCOMMU-266 工作聯絡單新增時增加日期判斷

## 1.32.0

- WORKCOMMU-257 可以自定義不同的勾選項目帶出該則勾選項目的預設內容

## 1.31.0

- WORKCOMMU-275 [執行項目維護]簽核層級下拉選項調整

## 1.19.0

- WORKCOMMU-253 調整工作聯絡單重新建立的cache排程
- WORKCOMMU-251 WERP MONITOR STATUS CHECK - WEB
- WORKCOMMU-252 工作聯絡單於多頁籤簽核狀態異動檢核
- WORKCOMMU-253 調整工作聯絡單重新建立的cache排程

## 1.17.1

- WORKCOMMU-237 收藏模糊搜尋搜不出來內容為html語言的單據.Failed Rate:100%
- WORKCOMMU-238 可閱單據查詢模糊搜尋搜不出來內容為html語言的單據.Failed Rate:100%
- WORKCOMMU-236 所有單據查詢模糊搜尋搜不出來內容為html語言的單據.Failed Rate:100%
- WORKCOMMU-234 GM 凱莉~play的ipad顯示有問題
- WORKCOMMU-241 工作聯絡單所輸入的內容與實際顯示不合

## 1.17.0.1

- 回覆WORKCOMMU-200做的修改

## 1.17.0

- 1.15.0更新修正為1.17.0

## 1.16.0

- WORKCOMMU-231 分點公司-工作聯絡單轉檔程式
- WORKCOMMU-234 GM 凱莉~play的ipad顯示有問題

## 1.15.0

- WORKCOMMU-141 作廢單據時，作廢原因的字數超過945字無法顯示確定取消按鈕
- WORKCOMMU-200 追蹤主題(不完整html語言)異動會顯示為空
- WORKCOMMU-207 模糊搜尋搜不出來內容為html語言的單據
- WORKCOMMU-208 個人待處理清單/部門待處理清單/可閱單據查詢的模糊搜尋搜不出來回覆中含有SQL語句的單據
- WORKCOMMU-213 工作聯絡表單 - 點入後 預設應為查詢，若需新增才點選新增，同需求單與案件單邏輯
- WORKCOMMU-212 工作聯絡單 - 點擊新增單據之後 - 『主題』 自動抓取 『立單時間』並帶入主題內,同忘簽到退單據

## 1.12.0

- WORKCOMMU-172 工作聯絡表單 - 請多一支報表 可以看到 全部所有的工作聯絡表單，包含 申請清單 / 可閱單據 / 執行清單。
- WORKCOMMU-197 回覆的editer元件判斷tag異常。
- WORKCOMMU-198 新增工作聯絡單時，清空默認內容後可以保存成功，不會提示‘[內容]為必要輸入資訊。

## 1.11.0

- WORKCOMMU-215 未領單/未派工 不判斷設定檔的status

## 1.10.0

- WORKCOMMU-185 模糊搜尋可以涵蓋回覆內容
- WORKCOMMU-192 工作聯絡單-執行清單查詢顯示未閱讀已閱讀
- WORKCOMMU-193 執行單據清單增加：單據名稱下拉選單的過濾條件

## 1.9.0

- WORKCOMMU-190 附加檔案按鈕是否開放執行單位可以點擊
- WORKCOMMU-201 當執行狀態＝已完成，執行單位進行加派時，追蹤頁籤請不要再寫入工作聯絡單已完成的資訊

## 1.8.0

- WORKCOMMU-187 可閱名單按鈕不卡單據狀態
- WORKCOMMU-177 加派UI設計調整
- WORKCOMMU-171 可閱單據查詢獨立為一張查詢清單
- WORKCOMMU-183 備忘錄查詢-新增『是否閱讀』功能

## 1.7.0

- WORKCOMMU-161 可閱單據查詢-請移除狀態＝作廢
- WORKCOMMU-167 【新增備忘錄】在新增備忘錄頁面，附加檔案的備註存儲不成功。
- WORKCOMMU-163 當內容超過66個字時，簽名或執行完成前後顯示格式不一致
- WORKCOMMU-169 【可閱單據查詢】勾掉建立單位的已選部門，確定後點擊‘清除’，不會恢復默認勾選狀態
- WORKCOMMU-162 上一筆下一筆按鈕請固定位置
- WORKCOMMU-164 所有的查詢均要能夠提供單號搜尋
- WORKCOMMU-166 關鍵字中間有空格找不到資料
- WORKCOMMU-154 執行方退回的資訊也希望能夠一併顯示在追蹤頁籤中
- WORKCOMMU-182 在有Portal欄的頁面點擊‘編輯’，鎖定倒數時間會停止在0:01
- WORKCOMMU-146 新增工作聯絡單頁面的會簽欄位與其他欄位沒有對齊

## 1.6.0

- WORKCOMMU-135 【追蹤-異動】主題/內容/備註異動資料不會記錄第二筆及之後的異動資料
- WORKCOMMU-134 鎖定倒數還沒有結束，就彈出頁面逾時提示
- REQ-825 報表清單順序,請依照wc_fun_item.seq欄位記錄的資訊進行排序
- WORKCOMMU-138 個人處理清單 rename 為 個人待處理清單
- WORKCOMMU-139 待處理清單 rename 為 部門待處理清單
- WORKCOMMU-140 執行單據清單 功能清單上面顯示的筆數請拿掉
- WORKCOMMU-137 【可閱名單】在沒有增加可閱人員時，簽核人員增加可閱人員中含自己，但可閱人員列表中自己的狀態還是待閱讀
- WORKCOMMU-148 新增頁面的菜單欄的收起按鈕無效
- WORKCOMMU-149 處理清單時執行下一筆的執行完成不會清除上一筆的執行完成訊息
- WORKCOMMU-150 search5可閱單據查詢頁面，按清除鍵會清除單據狀態默認值

## 1.5.1

- WORKCOMMU-135 【追蹤-異動】主題/內容/備註異動資料不會記錄第二筆及之後的異動資料

## 1.5.0

- WORKCOMMU-132 備忘錄查詢調整
- WORKCOMMU-130 所有的工作聯絡單報表提供自定義欄寬的功能
- WORKCOMMU-133 增加可閱名單的頁籤

## 1.4.0

- WORKCOMMU-127 點執行完成所輸入的dialog為必要輸入欄入
- WORKCOMMU-128 待辦事項新增 - 待結案筆數
- WORKCOMMU-129 報表增加需求單位欄位
- WORKCOMMU-131 所有工作聯絡單的報表欄寬調整

## 1.3.0

- WORKCOMMU-122 待辦事項新增未閱讀筆數
- WORKCOMMU-123 網頁上方字串,可根據挑選介面變更
- WORKCOMMU-124 執行回覆功能按鈕,結案後不顯示（其他的狀態都要顯示）
- WORKCOMMU-125 執行回覆按鈕名稱調整為"回覆",頁籤名稱調整為"回覆",追蹤頁籤裡面顯示的類別name調整為"回覆"
- WORKCOMMU-126 點執行完成時,彈輸入的dialog
- WORKCOMMU-104 收藏

## 1.2.0

- WORKCOMMU-92 執行回覆權限調整
- WORKCOMMU-95 轉單權限調整
- WORKCOMMU-96 分派權限調整
- WORKCOMMU-97 領單權限調整
- WORKCOMMU-99 加派權限調整
- WORKCOMMU-100 加簽權限調整
- WORKCOMMU-88 確認完成清單調整
- WORKCOMMU-84 可以設定哪些人進行特定類別的領單
- WORKCOMMU-87 個人處理清單
- WORKCOMMU-90 待派工清單調整
- WORKCOMMU-91 待領單據清單調整
- WORKCOMMU-101 可閱名單權限調整
- WORKCOMMU-108 執行狀況一覽顯示調整
- WORKCOMMU-102 待領單據清單調整
- WORKCOMMU-117 單據查詢rename為申請單據清單
- WORKCOMMU-118 執行狀況一覽表rename為執行單據清單
- WORKCOMMU-115 執行單位加簽按鈕名稱rename為加簽
- WORKCOMMU-116 需求單位加簽按鈕名稱rename為加簽
- WORKCOMMU-110 工作聯絡單主檔狀態增加「已完成」
- WORKCOMMU-112 工作聯絡單主單加上使用者所挑選的單據名稱
- WORKCOMMU-120 待處理清單(GM提出)
- WORKCOMMU-94 加派操作UI請調整
- WORKCOMMU-114 單據主單針對會簽資訊進行調整

## 1.12.0

- WORKCOMMU-172 工作聯絡表單 - 請多一支報表 可以看到 全部所有的工作聯絡表單，包含 申請清單 / 可閱單據 / 執行清單。
- WORKCOMMU-197 回覆的editer元件判斷tag異常。
- WORKCOMMU-198 新增工作聯絡單時，清空默認內容後可以保存成功，不會提示‘[內容]為必要輸入資訊。

## 1.11.0

- WORKCOMMU-215 未領單/未派工 不判斷設定檔的status

## 1.10.0

- WORKCOMMU-185 模糊搜尋可以涵蓋回覆內容
- WORKCOMMU-192 工作聯絡單-執行清單查詢顯示未閱讀已閱讀
- WORKCOMMU-193 執行單據清單增加：單據名稱下拉選單的過濾條件

## 1.9.0

- WORKCOMMU-190 附加檔案按鈕是否開放執行單位可以點擊
- WORKCOMMU-201 當執行狀態＝已完成，執行單位進行加派時，追蹤頁籤請不要再寫入工作聯絡單已完成的資訊

## 1.8.0

- WORKCOMMU-187 可閱名單按鈕不卡單據狀態
- WORKCOMMU-177 加派UI設計調整
- WORKCOMMU-171 可閱單據查詢獨立為一張查詢清單
- WORKCOMMU-183 備忘錄查詢-新增『是否閱讀』功能

## 1.7.0

- WORKCOMMU-161 可閱單據查詢-請移除狀態＝作廢
- WORKCOMMU-167 【新增備忘錄】在新增備忘錄頁面，附加檔案的備註存儲不成功。
- WORKCOMMU-163 當內容超過66個字時，簽名或執行完成前後顯示格式不一致
- WORKCOMMU-169 【可閱單據查詢】勾掉建立單位的已選部門，確定後點擊‘清除’，不會恢復默認勾選狀態
- WORKCOMMU-162 上一筆下一筆按鈕請固定位置
- WORKCOMMU-164 所有的查詢均要能夠提供單號搜尋
- WORKCOMMU-166 關鍵字中間有空格找不到資料
- WORKCOMMU-154 執行方退回的資訊也希望能夠一併顯示在追蹤頁籤中
- WORKCOMMU-182 在有Portal欄的頁面點擊‘編輯’，鎖定倒數時間會停止在0:01
- WORKCOMMU-146 新增工作聯絡單頁面的會簽欄位與其他欄位沒有對齊

## 1.6.0

- WORKCOMMU-135 【追蹤-異動】主題/內容/備註異動資料不會記錄第二筆及之後的異動資料
- WORKCOMMU-134 鎖定倒數還沒有結束，就彈出頁面逾時提示
- REQ-825 報表清單順序,請依照wc_fun_item.seq欄位記錄的資訊進行排序
- WORKCOMMU-138 個人處理清單 rename 為 個人待處理清單
- WORKCOMMU-139 待處理清單 rename 為 部門待處理清單
- WORKCOMMU-140 執行單據清單 功能清單上面顯示的筆數請拿掉
- WORKCOMMU-137 【可閱名單】在沒有增加可閱人員時，簽核人員增加可閱人員中含自己，但可閱人員列表中自己的狀態還是待閱讀
- WORKCOMMU-148 新增頁面的菜單欄的收起按鈕無效
- WORKCOMMU-149 處理清單時執行下一筆的執行完成不會清除上一筆的執行完成訊息
- WORKCOMMU-150 search5可閱單據查詢頁面，按清除鍵會清除單據狀態默認值

## 1.5.1

- WORKCOMMU-135 【追蹤-異動】主題/內容/備註異動資料不會記錄第二筆及之後的異動資料

## 1.5.0

- WORKCOMMU-132 備忘錄查詢調整
- WORKCOMMU-130 所有的工作聯絡單報表提供自定義欄寬的功能
- WORKCOMMU-133 增加可閱名單的頁籤

## 1.4.0

- WORKCOMMU-127 點執行完成所輸入的dialog為必要輸入欄入
- WORKCOMMU-128 待辦事項新增 - 待結案筆數
- WORKCOMMU-129 報表增加需求單位欄位
- WORKCOMMU-131 所有工作聯絡單的報表欄寬調整

## 1.3.0

- WORKCOMMU-122 待辦事項新增未閱讀筆數
- WORKCOMMU-123 網頁上方字串,可根據挑選介面變更
- WORKCOMMU-124 執行回覆功能按鈕,結案後不顯示（其他的狀態都要顯示）
- WORKCOMMU-125 執行回覆按鈕名稱調整為"回覆",頁籤名稱調整為"回覆",追蹤頁籤裡面顯示的類別name調整為"回覆"
- WORKCOMMU-126 點執行完成時,彈輸入的dialog
- WORKCOMMU-104 收藏

## 1.2.0

- WORKCOMMU-92 執行回覆權限調整
- WORKCOMMU-95 轉單權限調整
- WORKCOMMU-96 分派權限調整
- WORKCOMMU-97 領單權限調整
- WORKCOMMU-99 加派權限調整
- WORKCOMMU-100 加簽權限調整
- WORKCOMMU-88 確認完成清單調整
- WORKCOMMU-84 可以設定哪些人進行特定類別的領單
- WORKCOMMU-87 個人處理清單
- WORKCOMMU-90 待派工清單調整
- WORKCOMMU-91 待領單據清單調整
- WORKCOMMU-101 可閱名單權限調整
- WORKCOMMU-108 執行狀況一覽顯示調整
- WORKCOMMU-102 待領單據清單調整
- WORKCOMMU-117 單據查詢rename為申請單據清單
- WORKCOMMU-118 執行狀況一覽表rename為執行單據清單
- WORKCOMMU-115 執行單位加簽按鈕名稱rename為加簽
- WORKCOMMU-116 需求單位加簽按鈕名稱rename為加簽
- WORKCOMMU-110 工作聯絡單主檔狀態增加「已完成」
- WORKCOMMU-112 工作聯絡單主單加上使用者所挑選的單據名稱
- WORKCOMMU-120 待處理清單(GM提出)
- WORKCOMMU-94 加派操作UI請調整
- WORKCOMMU-114 單據主單針對會簽資訊進行調整

## 1.1.0

- WORKCOMMU-86 新增預設可閱的設定
- WORKCOMMU-85 加簽邏輯調整
- WORKCOMMU-83 處理清單調整
- WORKCOMMU-82 確認完成清單調整

## 0.0.5-SNAPSHOT

- WORKCOMMU-70 需求單位加簽功能
- WORKCOMMU-71 執行單位加簽功能
- WORKCOMMU-80 備忘錄填寫日期顯示調整
- WORKCOMMU-74 執行回覆功能權限調整
- WORKCOMMU-79 備忘錄查詢-搜尋區增加建立部門搜尋元件 / 日期快捷鍵新增今日按鈕
- WORKCOMMU-77 確認完成清單-邏輯調整
- WORKCOMMU-73 執行資訊頁籤調整
- WORKCOMMU-78 備忘錄追蹤頁籤調整
- WORKCOMMU-72 類別status=1新增時不顯示
- WORKCOMMU-76 執行回覆頁籤調整

## 0.0.4-SNAPSHOT

- WORKCOMMU-66 確認完成清單實作
- WORKCOMMU-65 加派功能

## 0.0.3-SNAPSHOT

- WORKCOMMU-58 功能清單-點文字就能夠開啟子項目（箭頭很難瞄準）
- WORKCOMMU-60 功能清單-待領單據清單旁邊請顯示筆數
- WORKCOMMU-61 功能清單-待派工清單旁邊請顯示筆數
- WORKCOMMU-62 功能清單-處理清單旁邊請顯示筆數
- WORKCOMMU-63 待領單據清單-單據名稱filter增加全部選項
- WORKCOMMU-64 待派工清單-單據名稱filter增加全部選項

## 0.0.1-SNAPSHOT 2016/10/20
