/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.AllTimeAccessPerson;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class AllTimeAccessPersonConverter
    implements AttributeConverter<AllTimeAccessPerson, String> {

    @Override
    public String convertToDatabaseColumn(AllTimeAccessPerson attribute) {
        if (attribute == null) {
            return "";
        }
        String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getUserTos());
        return json;
    }

    @Override
    public AllTimeAccessPerson convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            AllTimeAccessPerson to = new AllTimeAccessPerson();
            List<UserTo> resultList = WkJsonUtils.getInstance()
                .fromJsonToList(dbData, UserTo.class);
            to.setUserTos(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to AllTimeAccessPersonConverter fail!!" + ex.getMessage(), ex);
            return null;
        }
    }
}
