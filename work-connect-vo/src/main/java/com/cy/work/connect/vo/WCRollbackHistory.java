/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 退回記錄
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_rollback_history")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCRollbackHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5812846928925591002L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_rollback_history_sid", length = 36)
    private String sid;

    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;

    @Column(name = "user_sid", nullable = false)
    private Integer userSid;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createDate;
}
