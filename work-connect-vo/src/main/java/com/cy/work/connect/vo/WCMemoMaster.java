/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import com.cy.work.connect.vo.converter.ReadRecordConverter;
import com.cy.work.connect.vo.converter.WCMemoStatusConverter;
import com.cy.work.connect.vo.converter.WCSidsConverter;
import com.cy.work.connect.vo.converter.to.ReadRecord;
import com.cy.work.connect.vo.converter.to.WCSids;
import com.cy.work.connect.vo.enums.WCMemoStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_memo_master")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class)
public class WCMemoMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1936437380538449363L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_memo_sid", length = 36)
    private String sid;

    /**
     * 備忘錄單號
     */
    @Column(name = "wc_memo_no", nullable = false, length = 255, unique = true)
    private String wcMemoNo;

    /**
     * 建單者公司Sid
     */
    @Column(name = "comp_sid", nullable = false)
    private Integer comp_sid;
    /**
     * 建單者部門Sid
     */
    @Column(name = "dep_sid", nullable = false)
    private Integer dep_sid;

    @Column(name = "wc_memo_category_sid", nullable = false, length = 36)
    private String wcMemoCategorySid;
    /**
     * 建單者Sid
     */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /**
     * 建單時間
     */
    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;
    /**
     * 更新者Sid
     */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /**
     * 更新時間
     */
    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    /**
     * 單據狀態
     */
    @Convert(converter = WCMemoStatusConverter.class)
    @Column(name = "wc_mome_status", nullable = false)
    private WCMemoStatus wcMomeStatus;

    /**
     * 單據狀態
     */
    @Convert(converter = WCSidsConverter.class)
    @Column(name = "trans_wc_sid", nullable = false)
    private WCSids transWcSid;

    /**
     * 主題
     */
    @Column(name = "theme", nullable = true, length = 255)
    private String theme;
    /**
     * 內容
     */
    @Column(name = "content", nullable = false, length = 255)
    private String content;
    /**
     * 內容
     */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "content_css", nullable = false)
    private String content_css;
    /**
     * 備註
     */
    @Column(name = "memo", nullable = true, length = 255)
    private String memo;
    /**
     * 備註
     */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "memo_css", nullable = true)
    private String memo_css;

    @Column(name = "lock_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lock_dt;

    @Column(name = "lock_usr", nullable = true)
    private Integer lock_usr;

    /**
     * 閱讀記錄
     */
    @Column(name = "read_record")
    @Convert(converter = ReadRecordConverter.class)
    private ReadRecord readRecord;
}
