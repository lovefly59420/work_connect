/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

import lombok.Getter;

/**
 * 流程類型
 * 註: 新增類型需至 BrBpmLevelSettingHelper 新增對應層級
 *
 * @author brain0925_liao
 */
public enum FlowType {
    /**
     * 僅需自己簽核
     */
    SELF("僅自己簽核", "自己", true),
    /**
     * 簽核至組級
     */
    GROUP("簽核至組級", "組", true),
    /**
     * 簽核至部級
     */
    DEPARTMENT("簽核至部級", "部", true),
    /**
     * 簽核至處級
     */
    OFFICE("簽核至處級", "處", true),
    /**
     * 簽核至群級
     */
    BUSSINESSGROUP("簽核至群級", "群", true),
    /**
     * 執行長
     */
    MANAGER("簽核至最高主管", "組織樹最高主管", true),
    /**
     * [壐識]簽核至總經理
     */
    @Deprecated // WORKCOMMU-520
    XS_MANAGER("[壐識]簽至總經理(總經理簽核前通過款項覆核者)", "XS-總經理", false),

    // 以下不再使用
    /**
     * 會員服務處特殊流程-簽核至部級
     */
    @Deprecated
    SPECIAL_DEPARTMENT("會員服務處特殊流程-簽核至部級", "會員-部", false),
    /**
     * 會員服務處特殊流程-簽核至處級
     */
    @Deprecated
    SPECIAL_OFFICE("會員服務處特殊流程-簽核至處級", "會員-處", false),
    /**
     * 會員服務處特殊流程-簽核至群級
     */
    @Deprecated
    SPECIAL_BUSSINESSGROUP("會員服務處特殊流程-簽核至群級", "會員-群", false),
    /**
     * 會員服務處特殊流程-執行長
     */
    @Deprecated
    SPECIAL_MANAGER("會員服務處特殊流程-執行長", "會員-執行長", false),
    /**
     * 會員服務處特殊流程-簽核至部級_XS
     */
    @Deprecated
    SPECIAL_DEPARTMENT_XS("會員服務處特殊流程-簽核至部級_XS", "XS會員-部", false),
    /**
     * 會員服務處特殊流程-簽核至處級_XS
     */
    @Deprecated
    SPECIAL_OFFICE_XS("會員服務處特殊流程-簽核至處級_XS", "XS會員-處", false),
    /**
     * 會員服務處特殊流程-簽核至群級_XS
     */
    @Deprecated
    SPECIAL_BUSSINESSGROUP_XS("會員服務處特殊流程-簽核至群級_XS", "XS會員-群", false),
    /**
     * 會員服務處特殊流程-執行長_XS
     */
    @Deprecated
    SPECIAL_MANAGER_XS("會員服務處特殊流程-執行長_XS", "XS會員-執行長", false);

    @Getter
    private final String desc;

    @Getter
    private final String shortDesc;
    /**
     * 可使用
     */
    @Getter
    private final boolean enable;


    FlowType(
            String desc,
            String shortDesc,
            boolean enable) {
        this.desc = desc;
        this.shortDesc = shortDesc;
        this.enable = enable;
    }
}
