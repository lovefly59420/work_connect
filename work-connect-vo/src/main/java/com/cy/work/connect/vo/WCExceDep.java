/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.converter.ExceDepCreateTypeConverter;
import com.cy.work.connect.vo.converter.ExceDepTypeConverter;
import com.cy.work.connect.vo.converter.UsersAllowNullConverter;
import com.cy.work.connect.vo.converter.WCExceDepStatusConverter;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.ExecDepCreateType;
import com.cy.work.connect.vo.enums.ExecDepType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.utils.WorkConnectVOUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 單據執行單位
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_exec_dep")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Data
public class WCExceDep implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7927648768438116573L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_exec_dep_sid", length = 36)
    private String sid;

    /**
     * 簽核GroupSeq
     */
    @Column(name = "wc_exec_manager_sign_info_group_seq", nullable = true)
    private Integer execManagerSignInfoGroupSeq;

    /**
     * 簽核Sid
     */
    @Column(name = "wc_exec_manager_sign_info_sid", nullable = true, length = 36)
    private String execManagerSignInfoSid;

    /**
     * 工作聯絡單Sid
     */
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;

    /**
     * 執行部門Sid
     */
    @Column(name = "dep_sid", nullable = true)
    private Integer dep_sid;

    /**
     * 是否需簽核類型
     */
    @Convert(converter = ExceDepTypeConverter.class)
    @Column(name = "exec_dep_type", nullable = false)
    private ExecDepType execDepType;

    /**
     * 執行單位建立類型
     */
    @Convert(converter = ExceDepCreateTypeConverter.class)
    @Column(name = "exec_dep_create_type", nullable = false)
    private ExecDepCreateType execDepCreateType = ExecDepCreateType.SYSTEM;

    /**
     * 執行狀態
     */
    @Convert(converter = WCExceDepStatusConverter.class)
    @Column(name = "exec_dep_status", nullable = false)
    private WCExceDepStatus execDepStatus;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = true)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    /**
     * 執行人員
     */
    @Column(name = "exec_user_sid", nullable = true)
    private Integer execUserSid;

    /**
     * 執行設定
     */
    @Column(name = "wc_exec_dep_setting_sid", nullable = true)
    private String execDepSettingSid;

    /**
     * 僅領單人員可閱名單
     */
    // @Convert(converter = UsersAllowNullConverter.class)
    // private UsersTo receviceUser;
    // 改為 lazy 方式, 讀取或寫入時，才做 JSON 轉換, 提升 entity 轉換速度
    @Column(name = "receviceUser", nullable = true)
    private String receviceUserJsonStr;

    public UsersTo getReceviceUser() { return new UsersAllowNullConverter().convertToEntityAttribute(this.receviceUserJsonStr); }

    /**
     * 取得僅領單人員可閱名單 (為空時，代表非僅領單人員可閱)
     * 
     * @return
     */
    public Set<Integer> getReceviceUserSids() { return UsersTo.parserUserSids(this.getReceviceUser()); }

    /**
     * 取得可閱的領單人員名單
     * 
     * @return
     */
    public Set<Integer> getCanViewReceviceUserSids() { return WorkConnectVOUtils.prepareUsersToSids(this.getReceviceUser()); }

    public void setReceviceUser(UsersTo usersTo) { this.receviceUserJsonStr = new UsersAllowNullConverter().convertToDatabaseColumn(usersTo); }
}
