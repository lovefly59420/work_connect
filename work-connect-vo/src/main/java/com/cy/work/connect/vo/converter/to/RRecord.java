/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter.to;

import com.cy.work.connect.vo.enums.WCReadStatus;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author brain0925_liao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RRecord implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 793161421558828341L;

    private String reader;

    private String readDay;

    private WCReadStatus read;
}
