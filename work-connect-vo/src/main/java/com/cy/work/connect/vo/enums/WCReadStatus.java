/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 主檔已未讀狀態
 *
 * @author brain0925_liao
 */
public enum WCReadStatus {
    /**
     * 未閱讀
     */
    UNREAD("未閱讀"),
    /**
     * 已閱讀
     */
    HASREAD("已閱讀"),
    /**
     * 待閱讀
     */
    WAIT_READ("待閱讀"),
    /**
     * 需閱讀
     */
    NEEDREAD("需閱讀(未&待)");

    private final String val;

    WCReadStatus(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
