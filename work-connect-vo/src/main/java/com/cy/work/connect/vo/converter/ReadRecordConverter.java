/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.converter.to.ReadRecord;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作聯絡單已讀狀況Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ReadRecordConverter implements AttributeConverter<ReadRecord, String> {

    @Override
    public String convertToDatabaseColumn(ReadRecord attribute) {
        if (attribute == null || attribute.getRRecord() == null) {
            return "";
        }

        String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getRRecord());
        return json;
    }

    @Override
    public ReadRecord convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ReadRecord();
        }
        try {
            ReadRecord to = new ReadRecord();
            List<RRcordTo> resultList = WkJsonUtils.getInstance()
                .fromJsonToList(dbData, RRcordTo.class);
            //            resultList.forEach(item -> {
            //                RRecord r = new RRecord(item.getReader(), item.getReadDay(),
            // WRReadStatus.valueOf(item.getRead()));
            //                to.getRRecord().add(r);
            //            });
            to.setRRecord(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to ReadRecord fail!!" + ex.getMessage(), ex);
            return new ReadRecord();
        }
    }
}
