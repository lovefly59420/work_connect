package com.cy.work.connect.vo.enums;

import lombok.Getter;

/**
 * 模版類別
 *
 * @author allen
 */
public enum ConfigTempletType {
    MENU_TAG_CAN_USE("【單據名稱 (中)】- 可使用單位"),
    SUB_TAG_CAN_USE("【執行項目 (小)】- 可使用單位"),
    SUB_TAG_CAN_READ("【執行項目 (小)】- 預設可閱部門");
    // 暫未實做
    // EXEC_TRANS_DEP("執行單位-指派單位");

    /**
     * 說明
     */
    @Getter
    private final String descr;

    /**
     * @param descr
     * @param dep
     * @param user
     */
    ConfigTempletType(String descr) {
        this.descr = descr;
    }
}
