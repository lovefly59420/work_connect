/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * @author brain0925_liao
 */
public enum WCMemoStatus {
    /**
     * 發佈提交
     */
    NEW_INSTANCE("新建檔");

    private final String val;

    WCMemoStatus(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
