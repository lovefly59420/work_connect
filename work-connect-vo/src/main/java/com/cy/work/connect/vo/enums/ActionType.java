/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

import lombok.Getter;

public enum ActionType {

    REQ_FLOW_SIGN("需求單位-簽名", "簽名", true, false),
    REQ_FLOW_RECOVERY("需求單位-復原", "復原", true, false),
    REQ_FLOW_ROLLBACK("需求單位-退回", "退回", true, false),
    REQ_FLOW_INVAILD("需求單位-作廢", "作廢", true, false),
    REQ_FLOW_COUNTER_SIGN("需求單位會簽-簽名", "簽名", true, false),
    REQ_FLOW_COUNTER_RECOVERY("需求單位會簽-復原", "復原", true, false),

    EXEC_FLOW_SIGN("執行方-簽名", "簽名", false, true),
    EXEC_FLOW_RECOVERY("執行方-復原", "復原", false, true),
    EXEC_FLOW_SIGNER_ROLLBACK("執行方-簽核者退回", "退回", false, true),
    EXEC_FLOW_MEMBER_ROLLBACK("執行方-單位成員退回", "退回", false, true),
    EXEC_FLOW_SIGNER_STOP("執行方-簽核者不執行", "不執行", false, true),
    EXEC_FLOW_MEMBER_STOP("執行方-單位成員不執行", "不執行", false, true),
    ;

    @Getter
    private String descr;

    @Getter
    private String formSignButtonName;

    @Getter
    private boolean reloadBPMTab;

    @Getter
    private boolean reloadExecBPMTab;

    ActionType(String descr,
            String formSignButtonName,
            boolean reloadBPMTab,
            boolean reloadExecBPMTab) {
        this.descr = descr;
        this.formSignButtonName = formSignButtonName;
        this.reloadBPMTab = reloadBPMTab;
        this.reloadExecBPMTab = reloadExecBPMTab;
    }
}
