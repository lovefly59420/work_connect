package com.cy.work.connect.vo.enums;

import lombok.Getter;

/**
 * 轉換類型
 *
 * @author jimmy_chou
 */
public enum TransType {
    TRNS_EXEC_DEP("執行單位轉換"),
    TRNS_TRANS_DEP("指派單位轉換");

    @Getter
    private final String descr;

    TransType(String descr) {
        this.descr = descr;
    }
}
