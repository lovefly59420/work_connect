/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter.to;

import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author kasim
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExecDepTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4311633180699250660L;

    /**
     * [wc_exec_dep].[wc_exec_dep_sid]
     */
    @JsonProperty(value = "execDepSid")
    private String execDepSid;

    /**
     * 執行狀態
     */
    @JsonProperty(value = "status")
    private WCExceDepStatus status;

    /**
     * 執行狀態異動時間
     */
    @JsonProperty(value = "updateDate")
    private Date updateDate;

    /**
     * 執行狀態異動者
     */
    @JsonProperty(value = "updateUser")
    private Integer updateUser;

    public ExecDepTo(String execDepSid, WCExceDepStatus status) {
        this.execDepSid = execDepSid;
        this.status = status;
    }
}
