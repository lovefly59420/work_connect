/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 執行單位目前的執行狀況
 *
 * @author brain0925_liao
 */
public enum WCExceDepStatus {
    /**
     * 簽核中
     */
    APPROVING("簽核中", false, 1, false, false),
    /**
     * 待派工
     */
    WAITSEND("待派工", false, 3, false, false),
    /**
     * 待領單
     */
    WAITRECEIVCE("待領單", false, 2, false, false),
    /**
     * 執行中
     */
    PROCEDD("執行中", true, 4, false, false),
    /**
     * 終止
     */
    STOP("終止", true, Integer.MAX_VALUE, true, true),
    /**
     * 執行完成
     */
    FINISH("確認完成", true, 5, true, true),
    /**
     * 符合需求
     */
    CLOSE("符合需求", true, 6, false, true);

    @Getter
    private final String value;

    @Getter
    private final boolean isAlreadyDo;

    /**
     * 待結案
     */
    @Getter
    private final boolean isWaitClose;
    
    /**
     * 已完成
     */
    @Getter
    private final boolean isFinish;

    /**
     * 依據狀態排序的順序
     */
    @Getter
    private final int showSeq;

    WCExceDepStatus(String value, 
            boolean isAlreadyDo, 
            int showSeq, 
            boolean isWaitClose,
            boolean isFinish
            ) {
        this.value = value;
        this.isAlreadyDo = isAlreadyDo;
        this.showSeq = showSeq;
        this.isWaitClose = isWaitClose;
        this.isFinish = isFinish;
    }

    /**
     * 將字串轉為
     * 
     * @param str
     * @return
     */
    public static WCExceDepStatus safeTransType(String str) {
        if (WkStringUtils.isEmpty(str)) {
            return null;
        }
        str = WkStringUtils.safeTrim(str);

        for (WCExceDepStatus type : WCExceDepStatus.values()) {
            if (type.name().equals(str)) {
                return type;
            }
        }
        return null;
    }

}
