/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.ReplyRecord;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ReplyRecordConverter implements AttributeConverter<ReplyRecord, String> {

    @Override
    public String convertToDatabaseColumn(ReplyRecord attribute) {
        if (attribute == null || attribute.getRRecord() == null) {
            return "";
        }

        String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getRRecord());
        return json;
    }

    @Override
    public ReplyRecord convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ReplyRecord();
        }
        try {
            ReplyRecord to = new ReplyRecord();
            List<ReplyRecordTo> resultList =
                WkJsonUtils.getInstance().fromJsonToList(dbData, ReplyRecordTo.class);
            to.setRRecord(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to ReadRecord fail!!" + ex.getMessage(), ex);
            return new ReplyRecord();
        }
    }
}
