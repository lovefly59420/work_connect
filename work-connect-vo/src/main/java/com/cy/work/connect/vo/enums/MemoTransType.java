/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 備忘錄轉工作聯絡單類型
 *
 * @author brain0925_liao
 */
public enum MemoTransType {
    /**
     * 否
     */
    UNTRANSED("否"),
    /**
     * 是
     */
    TRANSED("是");

    private final String value;

    MemoTransType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
