/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums.column.search;

import com.cy.work.connect.vo.enums.Column;

/**
 * 申請單位查詢 欄位參數
 *
 * @author kasim
 */
public enum WaitCloseListColumn implements Column {
    INDEX("序", true, false, false, "30"),
    CREATE_DATE("建立日期", true, true, false, "75"),
    CATEGORY_NAME("類別", true, true, false, "120"),
    MENUTAG_NAME("單據名稱", true, true, false, "120"),
    APPLICATION_USER_NAME("申請人", true, true, false, "90"),
    THEME("主題", true, false, false, ""),
    STATUS_NAME("單據狀態", true, true, false, "100"),
    WC_NO("單號", true, true, false, "130");

    /**
     * 欄位名稱
     */
    private final String name;
    /**
     * 預設是否顯示
     */
    private final boolean defaultShow;
    /**
     * 是否可修改欄位寬度
     */
    private final boolean canModifyWidth;
    /**
     * 是否可修改是否顯示
     */
    private final boolean canSelectItem;
    /**
     * 預設欄位寬度
     */
    private final String defaultWidth;

    WaitCloseListColumn(
        String name,
        boolean defaultShow,
        boolean canModifyWidth,
        boolean canSelectItem,
        String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
