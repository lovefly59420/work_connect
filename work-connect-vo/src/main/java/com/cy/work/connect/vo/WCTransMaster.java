/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.connect.vo.converter.TransTypeConverter;
import com.cy.work.connect.vo.enums.TransType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 轉換程式主檔
 *
 * @author jimmy_chou
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "wc_trans_master")
public class WCTransMaster implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2927652190943459804L;

    /**
     * 轉換程式主檔 SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trans_sid")
    private Long sid;

    /**
     * 公司
     */
    @Column(name = "trans_comp", nullable = false)
    private Integer comp;

    /**
     * 轉換單號
     */
    @Column(name = "trans_no", length = 36, nullable = false)
    private String transNo;

    /**
     * 轉換類型
     */
    @Convert(converter = TransTypeConverter.class)
    @Column(name = "trans_type", length = 45, nullable = false)
    private TransType type;

    /**
     * 轉換主題
     */
    @Column(name = "trans_theme", length = 191, nullable = false)
    private String theme;

    /**
     * 轉換人員單位
     */
    @Column(name = "trans_dep")
    private Integer transDep;

    /**
     * 轉換人員
     */
    @Column(name = "trans_usr", length = 11, nullable = false)
    private Integer transUser;

    /**
     * 轉換日期
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trans_dt", nullable = false)
    private Date transDate;
}
