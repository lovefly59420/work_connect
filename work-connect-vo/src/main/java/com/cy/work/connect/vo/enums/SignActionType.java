/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 簽名類型
 *
 * @author brain0925_liao
 */
public enum SignActionType {
    /**
     * 簽名類型-復原動作
     */
    RECOVERY,
    /**
     * 簽名類型-退回動作
     */
    ROLLBACK,
    /**
     * 簽名類型-除復原動作
     */
    NORMAL
}
