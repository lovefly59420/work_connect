/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 工作聯絡單選單大項
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_fun_item_group")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCFunItemGroup implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4059525137803107069L;

    @Id
    @Column(name = "sid", nullable = false)
    private Integer sid;

    @Column(name = "category_model", nullable = false, length = 45)
    private String category_model;

    @Column(name = "group_base_sid", nullable = false, length = 36)
    private String group_base_sid;

    @Column(name = "name", nullable = false, length = 200)
    private String name;

    @Column(name = "seq", nullable = false)
    private Integer seq;
}
