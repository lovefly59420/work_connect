/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 主檔單據狀態類型
 */
public enum WCStatus {

    /**
     * 新建檔: 需求方申請人員開立需求單據尚未按【簽名】前，確認狀態為「新建檔」
     */
    NEW_INSTANCE("新建檔", 1,
            "申請方編輯和附件", true, " 執行方附件      ", false,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", true, " 意見說明(會簽者)", false, " 意見說明(執行方)", false,
            "申請方加會簽    ", true, " 執行方加會簽    ", false),

    /**
     * 待簽核: 申請人員簽立單據後，上級簽核人員尚未按【簽名】前，確認狀態為「待簽核」
     */
    WAITAPPROVE("待簽核", 2,
            "申請方編輯和附件", true, " 執行方附件      ", false,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", true, " 意見說明(會簽者)", true, " 意見說明(執行方)", false,
            "申請方加會簽    ", true, " 執行方加會簽    ", false),

    /**
     * 簽核中: 需求方簽核人員按【簽名】後，確認狀態為「簽核中」
     */
    APPROVING("簽核中", 3,
            "申請方編輯和附件", true, " 執行方附件      ", false,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", true, " 意見說明(會簽者)", true, " 意見說明(執行方)", false,
            "申請方加會簽    ", true, " 執行方加會簽    ", false),

    /**
     * 核准:
     * 1.申請方已經全部簽核完成，
     * 2.執行方待簽
     */
    APPROVED("核准", 4,
            "申請方編輯和附件", false, "執行方附件      ", true,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", false, "派工            ", true, " 加派", false,
            "執行完成        ", false, "確認完成        ", true,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", true,
            "申請方加會簽    ", false, "執行方加會簽    ", true),

    /**
     * 待執行 X
     */
    WAIT_EXEC("待執行", 6,
            "申請方編輯和附件", false, "執行方附件      ", false,
            "可閱人員:       ", false, "回覆            ", false,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", false,
            "申請方加會簽    ", false, "執行方加會簽    ", false),
    /**
     * 執行中:分為簽核人員簽核後分派/領單及無簽核分派/領單。<br/>
     * 簽核後派工: 確認執行方簽核人員按【簽名】並分派後，狀態為「執行中」。<br/>
     * 簽核後領單: 確認執行方簽核人員按【簽名】並有人領單後，狀態為「執行中」。<br/>
     * 無簽核派工: 確認執行方分派人員分派人員後，狀態為「執行中」。<br/>
     * 無簽核領單: 確認執行方領單人員按領單後，狀態為「執行中」。<br/>
     */
    EXEC("執行中", 7,
            "申請方編輯和附件", false, "執行方附件      ", true,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", true, " 派工            ", true, " 加派", true,
            "執行完成        ", true, " 確認完成        ", false,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", true,
            "申請方加會簽    ", false, "執行方加會簽    ", true),

    /**
     * 已完成: 所有執行人員按【執行完成】後，狀態為「已完成」。
     */
    EXEC_FINISH("已完成", 8,
            "申請方編輯和附件", false, "執行方附件      ", true,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", true,
            "執行完成        ", false, "確認完成        ", true,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", false,
            "申請方加會簽    ", false, "執行方加會簽    ", true),

    /**
     * 結案: 申請單位確認單據已完成按【確認完成】後，狀態為「結案」。
     */
    CLOSE("結案", 9,
            "申請方編輯和附件", false, "執行方附件      ", false,
            "可閱人員:       ", true, " 回覆            ", false,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", true,
            "申請方加會簽    ", false, "執行方加會簽    ", false),
    /**
     * 結案(終止)
     */
    CLOSE_STOP("結案(終止)", 97,
            "申請方編輯和附件", false, "執行方附件      ", false,
            "可閱人員:       ", true, " 回覆            ", false,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", false,
            "申請方加會簽    ", false, "執行方加會簽    ", false),

    /**
     * 再議
     */
    RECONSIDERATION("再議", 98,
            "申請方編輯和附件", true, " 執行方附件      ", false,
            "可閱人員:       ", true, " 回覆            ", true,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", true, " 意見說明(會簽者)", false, "意見說明(執行方)", false,
            "申請方加會簽    ", true, " 執行方加會簽    ", false),

    /**
     * 作廢 X
     */
    INVALID("作廢", 99,
            "申請方編輯和附件", false, "執行方附件      ", false,
            "可閱人員:       ", false, "回覆            ", false,
            "收藏            ", true, " 轉單            ", false, "派工            ", false, "加派", false,
            "執行完成        ", false, "確認完成        ", false,
            "意見說明(申請方)", false, "意見說明(會簽者)", false, "意見說明(執行方)", false,
            "申請方加會簽    ", false, "執行方加會簽    ", false),

    ;

    @Getter
    private final String val;
    @Getter
    private final int seq;

    /**
     * 1.可使用編輯、上傳附件 (申請單位)
     */
    @Getter
    private final boolean canUseReqSideEditAndUploadAttch;

    /**
     * 2.可上傳附件 (執行單位)
     */
    @Getter
    private final boolean canExecSideUploadAttch;

    /**
     * 3.可編輯可閱人員名單
     */
    @Getter
    private final boolean canEditSpecViewPerson;
    /**
     * 4.可使用回覆
     */
    @Getter
    private final boolean canUseReply;
    /**
     * 5.可使用收藏
     */
    @Getter
    private final boolean canUseFavorite;
    /**
     * 6.可使用執行方轉單
     */
    @Getter
    private final boolean canUseExecSideDepTransExecutor;
    /**
     * 7.可使用執行方派工給執行人員
     */
    @Getter
    private final boolean canUseExecSideAssignExecutor;
    /**
     * 8.可使用執行方加派執行單位
     */
    @Getter
    private final boolean canUseExecSideDepAddExecDep;
    /**
     * 9.可使用執行方『執行完成』
     */
    @Getter
    private final boolean canUseExecSideComplete;
    /**
     * 10.可使用需求方『確認完成』(結案)
     */
    @Getter
    private final boolean canUseReqSideClose;
    /**
     * 11.可使用意見說明(申請單位)
     */
    @Getter
    private final boolean canUseReqSideComments;
    /**
     * 12.可使用意見說明(申請方會簽者)
     */
    @Getter
    private final boolean canUseReqSideCountersignComments;
    /**
     * 13.可使用意見說明(執行方簽核、會簽者)
     */
    @Getter
    private final boolean canUseExecSideCmments;
    /**
     * 14.可使用申請方加會簽
     */
    @Getter
    private final boolean canUseReqSideAddCountersign;
    /**
     * 15.可使用執行方加會簽
     */
    @Getter
    private final boolean canUseExecSideAddCountersign;

    WCStatus(
            String val,
            int seq,
            String canEditAndUploadAttchDescr, boolean canEditAndUploadAttch,
            String canExecSideUploadAttchDescr, boolean canExecSideUploadAttch,
            String canEditSpecViewPersonDescr, boolean canEditSpecViewPerson,
            String canUseReplyDescr, boolean canUseReply,
            String canUseFavoriteDescr, boolean canUseFavorite,
            String canUseExecSideDepTransExecutorDescr, boolean canUseExecSideDepTransExecutor,
            String canUseExecSideAssignExecutorDescr, boolean canUseExecSideAssignExecutor,
            String canUseExecSideDepAddExecDepDescr, boolean canUseExecSideDepAddExecDep,
            String canUseExecSideCompleteDescr, boolean canUseExecSideComplete,
            String canUseReqSideCloseDescr, boolean canUseReqSideClose,
            String canUseReqSideCommentsDescr, boolean canUseReqSideComments,
            String canUseReqSideCountersignCommentsDescr, boolean canUseReqSideCountersignComments,
            String canUseExecSideCmmentsDescr, boolean canUseExecSideCmments,
            String canUseReqSideAddCountersignDescr, boolean canUseReqSideAddCountersign,
            String canUseExecSideAddCountersignDescr, boolean canUseExecSideAddCountersign) {
        this.val = val;
        this.seq = seq;
        // 1.可使用編輯、上傳附件 (申請單位)
        this.canUseReqSideEditAndUploadAttch = canEditAndUploadAttch;
        // 2.可上傳附件 (執行單位)
        this.canExecSideUploadAttch = canExecSideUploadAttch;
        // 3.可編輯可閱人員名單
        this.canEditSpecViewPerson = canEditSpecViewPerson;
        // 4.可使用回覆
        this.canUseReply = canUseReply;
        // 5.可使用收藏
        this.canUseFavorite = canUseFavorite;
        // 6.可使用執行方轉單
        this.canUseExecSideDepTransExecutor = canUseExecSideDepTransExecutor;
        // 7.可使用執行方派工給執行人員
        this.canUseExecSideAssignExecutor = canUseExecSideAssignExecutor;
        // 8.可使用執行方加派執行單位
        this.canUseExecSideDepAddExecDep = canUseExecSideDepAddExecDep;
        // 9.可使用執行方『執行完成』
        this.canUseExecSideComplete = canUseExecSideComplete;
        // 10.可使用需求方『確認完成』(結案)
        this.canUseReqSideClose = canUseReqSideClose;
        // 11.可使用意見說明(申請單位)
        this.canUseReqSideComments = canUseReqSideComments;
        // 12.意見說明(會簽者)
        this.canUseReqSideCountersignComments = canUseReqSideCountersignComments;
        // 13.意見說明(執行方簽核者)
        this.canUseExecSideCmments = canUseExecSideCmments;
        // 14.可使用申請方加會簽
        this.canUseReqSideAddCountersign = canUseReqSideAddCountersign;
        // 15.可使用執行方加會簽
        this.canUseExecSideAddCountersign = canUseExecSideAddCountersign;
    }

    /**
     * 將字串轉為
     * 
     * @param str
     * @return
     */
    public static WCStatus safeTransType(String str) {
        if (WkStringUtils.isEmpty(str)) {
            return null;
        }
        str = WkStringUtils.safeTrim(str);

        for (WCStatus type : WCStatus.values()) {
            if (type.name().equals(str)) {
                return type;
            }
        }
        return null;
    }

    /**
     * 將字串狀態轉為說明
     * 
     * @param str
     * @return
     */
    public static String safeTransDescr(String str) {
        if (WkStringUtils.isEmpty(str)) {
            return "";
        }
        str = WkStringUtils.safeTrim(str);
        for (WCStatus type : WCStatus.values()) {
            if (type.name().equals(str)) {
                return type.getVal();
            }
        }
        return str;
    }
}
