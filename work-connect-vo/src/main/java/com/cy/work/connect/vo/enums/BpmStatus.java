/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * BPM 狀態類型
 *
 * @author kasim
 */
public enum BpmStatus {

    /**
     * 新建檔
     */
    NEW_INSTANCE("新建檔"),
    /**
     * 待簽核
     */
    WAITING_FOR_APPROVE("待簽核"),
    /**
     * 待建立
     */
    WAIT_CREATE("待建立"),
    /**
     * 簽核中
     */
    APPROVING("簽核中"),
    /**
     * 核准
     */
    APPROVED("核准"),
    /**
     * 作廢 - 真的刪除
     */
    DELETE("作廢"),
    /**
     * 作廢 - 保留流程
     */
    INVALID("作廢"),
    /**
     * 再議
     */
    RECONSIDERATION("再議"),
    /**
     * 結案
     */
    CLOSED("結案");

    private final String val;

    BpmStatus(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
