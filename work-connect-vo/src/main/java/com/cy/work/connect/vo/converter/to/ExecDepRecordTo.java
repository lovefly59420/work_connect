/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter.to;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author kasim
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExecDepRecordTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9206338421423687731L;

    private List<ExecDepTo> record = Lists.newArrayList();
}
