/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.common.vo.converter.StringBlobConverter;
import com.cy.work.connect.vo.converter.WCTraceTypeConverter;
import com.cy.work.connect.vo.enums.WCTraceType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作聯絡單再次追蹤
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_again_trace")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCAgainTrace implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5214941849105756296L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_again_trace_sid", length = 36)
    private String sid;

    /*追蹤主單 Sid*/
    @Column(name = "wc_trace_sid", nullable = false, length = 36)
    private String traceSid;

    /*工作聯絡單 Sid*/
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;

    /*工作聯絡單 單號*/
    @Column(name = "wc_no", nullable = false, length = 21)
    private String wcNo;

    /*追蹤類型*/
    @Convert(converter = WCTraceTypeConverter.class)
    @Column(name = "wc_trace_type", nullable = false, length = 45)
    private WCTraceType type;

    /**
     * 回覆內容
     */
    @Column(name = "wc_trace_content", nullable = true, length = 255)
    private String content;

    /**
     * 回覆內容 CSS
     */
    @Convert(converter = StringBlobConverter.class)
    @Column(name = "wc_trace_content_css", nullable = true)
    private String contentCss;

    /**
     * 資料狀態
     */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = false)
    private Integer createUser;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "update_usr", nullable = true)
    private Integer updateUser;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;

    public WCAgainTrace(String traceSid, String wcSid, String wcNo, WCTraceType type) {
        this.traceSid = traceSid;
        this.wcSid = wcSid;
        this.wcNo = wcNo;
        this.type = type;
    }
}
