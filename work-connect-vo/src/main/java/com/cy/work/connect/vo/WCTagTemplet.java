package com.cy.work.connect.vo;

import com.cy.work.common.eo.AbstractEo;
import com.cy.work.connect.vo.converter.CategoryTagToConverter;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author jimmy_chou
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(
    of = {"sid"},
    callSuper = false)
@Entity
@Table(name = "wc_tag_templet")
public class WCTagTemplet extends AbstractEo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4536240890986767282L;

    /**
     * SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wc_tag_templet_sid")
    private Long sid;

    /**
     * 公司sid
     */
    @Column(name = "comp_sid")
    private Integer compSid;

    /**
     * 單據名稱sid
     */
    @Column(name = "wc_menu_tag_sid", nullable = false)
    private String menuTagSid;

    /**
     * tag模版使用名稱sid
     */
    @Column(name = "usename", nullable = false)
    private String usename;

    /**
     * 建議勾選項目設定（JSON String）
     */
    @Column(name = "default_value", nullable = true)
    @Convert(converter = CategoryTagToConverter.class)
    private CategoryTagTo defaultValue;

    /**
     * 備註
     */
    @Column(name = "memo")
    private String memo;

    /**
     * 排序序號
     */
    @Column(name = "sort_seq")
    private Integer sortSeq = Integer.MAX_VALUE;
}
