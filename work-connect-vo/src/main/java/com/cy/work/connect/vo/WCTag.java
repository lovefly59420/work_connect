/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.eo.AbstractEo;
import com.cy.work.connect.vo.converter.FlowTypeConverter;
import com.cy.work.connect.vo.converter.UserDepConverter;
import com.cy.work.connect.vo.converter.UsersConverter;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.FlowType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * 工作聯絡單 - 執行項目
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_tag")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = { "sid" }, callSuper = false)
@Data
public class WCTag extends AbstractEo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1852540551018146507L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_tag_sid", length = 36)
    private String sid;

    @Column(name = "wc_menu_tag_sid", nullable = false, length = 36)
    private String wcMenuTagSid;

    @Column(name = "tag_name", nullable = false, length = 255)
    private String tagName;

    @Column(name = "memo", nullable = true, length = 255)
    private String memo;

    @Column(name = "seq", nullable = false)
    private Integer seq = 0;

    @Convert(converter = FlowTypeConverter.class)
    @Column(name = "reqFlowType", nullable = false)
    private FlowType reqFlowType;

    @Column(name = "default_content", nullable = true)
    private String defaultContent;

    @Column(name = "default_content_css", nullable = true)
    private String defaultContentCss;

    @Column(name = "default_memo_css", nullable = true)
    private String defaultMemoCss;

    @Column(name = "is_upload_file", nullable = true)
    private Boolean uploadFile;

    @Column(name = "legitimate_range_check", nullable = true)
    private boolean legitimateRangeCheck;

    @Column(name = "legitimate_range_days", nullable = true)
    private String legitimateRangeDays;

    @Column(name = "legitimate_range_title", nullable = true)
    private String legitimateRangeTitle;

    @Column(name = "start_end_check", nullable = true)
    private boolean startEndCheck;

    @Column(name = "start_end_days", nullable = true)
    private String startEndDays;

    @Column(name = "start_end_title", nullable = true)
    private String startEndTitle;

    @Column(name = "end_rule_days", nullable = true)
    private String endRuleDays;
    /**
     * 可使用單位
     */
    // @Column(name = "can_user_dep", nullable = true)
    // @Convert(converter = UserDepConverter.class)
    // private UserDep canUserDep;
    // 改為 lazy 方式, 讀取或寫入時，才做 JSON 轉換, 提升 entity 轉換速度
    @Column(name = "can_user_dep", nullable = true)
    private String canUserDepJsonStr;
    /**
     * 可使用單位：套用模版 ref:wc_config_templet
     */
    @Column(name = "can_user_dep_templet_sid", nullable = true)
    private Long canUserDepTempletSid;
    /**
     * 可使用單位：包含以下
     */
    @Column(name = "isUserContainFollowing", nullable = true)
    private boolean isUserContainFollowing;
    /**
     * 預設可閱部門
     */
    // @Convert(converter = UserDepConverter.class)
    // private UserDep useDep;
    // 改為 lazy 方式, 讀取或寫入時，才做 JSON 轉換, 提升 entity 轉換速度
    @Column(name = "use_dep", nullable = true)
    private String useDepJsonStr;
    /**
     * 預設可閱部門：是否包含以下
     */
    @Column(name = "isViewContainFollowing", nullable = true)
    private boolean isViewContainFollowing;
    /**
     * 預設可閱部門：套用模版 ref:wc_config_templet
     */
    @Column(name = "use_dep_templet_sid", nullable = true)
    private Long useDepTempletSid;

    /**
     * 預設可閱人員
     */
    // @Column(name = "use_user", nullable = true)
    // @Convert(converter = UsersConverter.class)
    // private UsersTo useUser;
    // 改為 lazy 方式, 讀取或寫入時，才做 JSON 轉換, 提升 entity 轉換速度
    @Column(name = "use_user", nullable = true)
    private String useUserJsonStr;

    /**
     * 是否忽略「需求方最終簽核人員」設定
     */
    @Column(name = "is_req_final_apply_ignored")
    private boolean isReqFinalApplyIgnored;
    /**
     * 可使用單位-是否僅主管可使用
     */
    @Column(name = "is_only_for_use_dep_manager", nullable = false)
    private boolean onlyForUseDepManager;
    /**
     * 顯示分類
     */
    @Column(name = "tagType", nullable = true)
    private String tagType;
    /**
     * Tag 備註說明
     */
    @Column(name = "tagDesc", nullable = true)
    private String tagDesc;

    public UsersTo getUseUser() { return new UsersConverter().convertToEntityAttribute(this.useUserJsonStr); }
    public Set<Integer> getUseUserSids() {
        return UsersTo.parserUserSids(this.getUseUser());
    }

    public void setUseUser(UsersTo usersTo) { this.useUserJsonStr = new UsersConverter().convertToDatabaseColumn(usersTo); }

    public UserDep getCanUserDep() { return new UserDepConverter().convertToEntityAttribute(this.canUserDepJsonStr); }

    public void setCanUserDep(UserDep userDep) { this.canUserDepJsonStr = new UserDepConverter().convertToDatabaseColumn(userDep); }

    public UserDep getUseDep() { return new UserDepConverter().convertToEntityAttribute(this.useDepJsonStr); }

    public void setUseDep(UserDep userDep) { this.useDepJsonStr = new UserDepConverter().convertToDatabaseColumn(userDep); }
}
