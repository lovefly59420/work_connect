/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.ExecDepCreateType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 執行單位建立類型 Converter
 *
 * @author kasim
 */
@Slf4j
@Converter
public class ExceDepCreateTypeConverter implements AttributeConverter<ExecDepCreateType, String> {

    @Override
    public String convertToDatabaseColumn(ExecDepCreateType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ExecDepCreateType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ExecDepCreateType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ExecDepCreateType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
