/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.ExecDepRecordTo;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 執行單位自定物件 Converter
 *
 * @author kasim
 */
@Converter
@Slf4j
public class ExecDepRecordToConverter implements AttributeConverter<ExecDepRecordTo, String> {

    @Override
    public String convertToDatabaseColumn(ExecDepRecordTo attribute) {
        if (attribute == null) {
            return "";
        }
        return WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute);
    }

    @Override
    public ExecDepRecordTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ExecDepRecordTo();
        }
        try {
            return WkJsonUtils.getInstance().fromJson(dbData, ExecDepRecordTo.class);
        } catch (IOException ex) {
            log.error("parser json to ExecDepRecordTo fail!!" + ex.getMessage(), ex);
            return new ExecDepRecordTo();
        }
    }
}
