/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.SignActionType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * BPM狀態 Converter
 *
 * @author kasim
 */
@Slf4j
@Converter
public class SignActionTypeConverter implements AttributeConverter<SignActionType, String> {

    @Override
    public String convertToDatabaseColumn(SignActionType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public SignActionType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return SignActionType.valueOf(dbData);
        } catch (Exception e) {
            log.error("SignActionType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
