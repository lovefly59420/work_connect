/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * @author brain0925_liao
 */
public enum SignType {
    /**
     * 建單時產生需求流程類型
     */
    SIGN,
    /**
     * 加簽
     */
    COUNTERSIGN
}
