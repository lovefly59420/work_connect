/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.connect.vo.converter.BpmStatusConverter;
import com.cy.work.connect.vo.converter.SignTypeConverter;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 執行方簽核資訊
 *
 * @author kasim
 */
@Entity
@Table(name = "wc_exec_manager_sign_info")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCExecManagerSignInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3425952059143408489L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_exec_manager_sign_info_sid", length = 36)
    private String sid;

    /**
     * 工作聯絡單Sid
     */
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;
    /**
     * 工作聯絡單單號
     */
    @Column(name = "wc_no", nullable = false, length = 21)
    private String wcNo;

    /**
     * 簽核狀態
     */
    @Convert(converter = BpmStatusConverter.class)
    @Column(name = "status_code", nullable = true)
    private BpmStatus status;

    /**
     * 簽核類型-加簽或非加簽
     */
    @Convert(converter = SignTypeConverter.class)
    @Column(name = "signType", nullable = false)
    private SignType signType;

    /**
     * BPM ID
     */
    @Column(name = "bpm_instance_id", nullable = false, length = 45)
    private String bpmId;

    /**
     * 預設簽核人名
     */
    @Column(name = "bpm_default_signed_name", nullable = true, length = 45)
    private String bpmDefaultSignedName;

    /**
     * 若為指定人員才有值(加簽)
     */
    @Column(name = "user_sid", nullable = true)
    private Integer user_sid;
    /**
     * 若為指定人員才有值(加簽)
     */
    @Column(name = "dep_sid", nullable = true)
    private Integer dep_sid;

    /**
     * GroupSeq
     */
    @Column(name = "groupSeq", nullable = true)
    private Integer groupSeq;

    /**
     * 序號
     */
    @Column(name = "seq", nullable = true)
    private Integer seq;
    /**
     * 建立者
     */
    @Column(name = "create_user_sid", nullable = true)
    private Integer create_user_sid;

    @Column(name = "create_dt", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
}
