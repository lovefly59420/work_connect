/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 執行單位建立類型
 *
 * @author brain0925_liao
 */
public enum ExecDepCreateType {
    /**
     * 系統增加
     */
    SYSTEM,
    /**
     * 手動新增
     */
    CUSTOMER
}
