/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 工作記錄特定參數
 *
 * @author shaun
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(
    of = {"sid"},
    callSuper = false)
@Entity
@Table(name = "wc_backend_param")
public class WorkParam implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -452070751592567272L;

    @Id
    @Basic(optional = false)
    @Column(name = "sid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sid;

    /**
     * 特定參數查詢關鍵字
     */
    @Column(name = "keyword", nullable = false, length = 255, unique = true)
    private String keyword;

    /**
     * 對應內容
     */
    @Column(name = "content")
    private String content;

    /**
     * 參數描述
     */
    @Column(name = "description")
    private String description;

    /**
     * 排序編號
     */
    @Column(name = "seq")
    private Integer seq;
}
