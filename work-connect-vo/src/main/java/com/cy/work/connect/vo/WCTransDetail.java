package com.cy.work.connect.vo;

import com.cy.work.connect.vo.converter.TransTypeConverter;
import com.cy.work.connect.vo.enums.TransType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 轉換程式明細檔
 *
 * @author jimmy_chou
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"sid"})
@Entity
@Table(name = "wc_trans_detail")
public class WCTransDetail implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1806112052277485983L;

    /**
     * 轉換程式主檔 SID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trans_detail_sid")
    private Long sid;

    /**
     * 轉換主檔sid
     */
    @Column(name = "trans_sid", nullable = false)
    private Long transSid;

    /**
     * 轉換單號
     */
    @Column(name = "trans_no", length = 45, nullable = false)
    private String transNo;

    /**
     * 轉換的工作聯絡單號
     */
    @Column(name = "trane_wc_no", length = 45, nullable = false)
    private String wcNo;

    /**
     * 轉換類型
     */
    @Convert(converter = TransTypeConverter.class)
    @Column(name = "trans_type", length = 45, nullable = false)
    private TransType type;

    /**
     * 轉換前單位
     */
    @Column(name = "b_trans_dep", length = 11, nullable = false)
    private Integer beforeOrg;

    /**
     * 轉換後單位
     */
    @Column(name = "a_trans_dep", length = 11, nullable = false)
    private Integer afterOrg;

    /**
     * 轉換日期
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trans_dt")
    private Date transDate;
}
