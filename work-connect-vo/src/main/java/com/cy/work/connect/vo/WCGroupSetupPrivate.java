/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.converter.GroupInfoConverter;
import com.cy.work.connect.vo.converter.to.GroupInfo;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 工作聯絡單 - 個人群組設定
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_group_setup_private")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCGroupSetupPrivate implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1637875142853524122L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "group_sid", length = 36)
    private String sid;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "type", nullable = false, length = 50)
    private String type;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;

    @Column(name = "group_name", nullable = false, length = 20)
    private String group_name;

    @Convert(converter = GroupInfoConverter.class)
    @Column(name = "group_info", nullable = false)
    private GroupInfo group_info;

    @Column(name = "note", nullable = true, length = 50)
    private String note;
}
