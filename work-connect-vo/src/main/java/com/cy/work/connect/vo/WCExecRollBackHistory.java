/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.connect.vo.converter.UserDepConverter;
import com.cy.work.connect.vo.converter.to.UserDep;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_exec_rollBack_history")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCExecRollBackHistory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -704288887180794511L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_exec_rollBack_history_sid", length = 36)
    private String sid;

    /**
     * [wc_master].[wc_sid]
     */
    @Column(name = "wc_sid", nullable = false, length = 36)
    private String wcSid;

    /**
     * 退回時間
     */
    @Column(name = "rollBack_dt", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date rollBackDate;

    /**
     * 退回者
     */
    @Column(name = "user_sid", nullable = false)
    private Integer rollBackSid;

    @Column(name = "exec_dep", nullable = true)
    @Convert(converter = UserDepConverter.class)
    private UserDep rollBackDep;
}
