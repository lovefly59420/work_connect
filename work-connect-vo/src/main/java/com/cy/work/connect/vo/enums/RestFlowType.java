/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * @author jimmy_chou
 */
public enum RestFlowType {
    /**
     * 需求流程
     */
    REQ,
    /**
     * 執行流程
     */
    EXEC
}
