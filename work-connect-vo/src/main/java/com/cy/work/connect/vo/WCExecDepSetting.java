/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.converter.UserDepConverter;
import com.cy.work.connect.vo.converter.UsersConverter;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.utils.WorkConnectVOUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 執行單位設定值
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_exec_dep_setting")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = { "sid" })
@Data
public class WCExecDepSetting implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5534910276287510995L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_exec_dep_setting_sid", length = 36)
    private String sid;

    /* 工作聯絡單類別 Sid */
    @Column(name = "wc_tag_sid", nullable = false, length = 36)
    private String wc_tag_sid;

    /* 執行單位Sid */
    @Column(name = "exec_dep_sid", nullable = false)
    private Integer exec_dep_sid;

    /**
     * 該執行人員是否需被指派人員指派
     */
    @Column(name = "isNeedAssigned", nullable = false)
    private boolean isNeedAssigned;

    /**
     * 是否讀取分派人員設定
     */
    @Column(name = "isReadAssignSetting", nullable = false)
    private boolean isReadAssignSetting;

    /**
     * 分派人員原始資料 (JSON)
     */
    @Column(name = "transUser", nullable = true)
    private String transUserJsonStr;
    /**
     * 指派單位List
     */
    // @Convert(converter = UserDepConverter.class)
    // private UserDep transdep;
    @Column(name = "transdep", nullable = true)
    private String transdepJsonStr;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;

    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
    /**
     * 簽核人員List
     */
    // @Convert(converter = UsersConverter.class)
    // private UsersTo execSignMember;
    @Column(name = "execSignMember", nullable = true)
    private String execSignMemberJsonStr;
    /**
     * 管理領單人員List
     */
    // @Convert(converter = UsersConverter.class)
    // private UsersTo managerRecevice;
    @Column(name = "manager_recevice", nullable = true)
    private String managerReceviceJsonStr;
    /**
     * 領單人員List
     */
    // @Convert(converter = UsersConverter.class)
    // private UsersTo receviceUser;
    @Column(name = "receviceUser", nullable = true)
    private String receviceUserJsonStr;
    /**
     * 該執行人員是否讀取領單人員設定
     */
    @Column(name = "readReceviceSetting", nullable = false)
    private boolean readReceviceSetting;
    /**
     * 是否僅領單人員可閱
     */
    @Column(name = "isOnlyReceviceUser", nullable = false)
    private boolean isOnlyReceviceUser;
    /**
     * 中佑對應窗口
     */
    @Column(name = "contact", nullable = false)
    private String contact;
    /**
     * 指派單位含以下單位
     */
    @Column(name = "isTransDepContainFollowing", nullable = false)
    private boolean isTransDepContainFollowing;
    /**
     * 執行說明
     */
    @Column(name = "exec_desc", nullable = true)
    private String execDesc;

    /**
     * 取得分派人員
     *
     * @return
     */
    public UsersTo getTransUser() {
        // 由對應欄位取得 JSON 資料，並用 UsersConverter 轉為對應物件
        return WorkConnectVOUtils.usersConverterFromJson(this.transUserJsonStr);
    }

    /**
     * 取得分派人員 sid
     * 
     * @return
     */
    public Set<Integer> getTransUserSids() {
        //
        return WorkConnectVOUtils.prepareUsersToSids(this.getTransUser());
    }

    /**
     * 設定分派人員
     *
     * @return
     */
    public void setTransUser(UsersTo usersTo) {
        // UsersTo，並用 UsersConverter 轉為JSON 字串，並存回對應欄位
        this.transUserJsonStr = new UsersConverter().convertToDatabaseColumn(usersTo);
    }

    public UserDep getTransdep() { return new UserDepConverter().convertToEntityAttribute(this.transdepJsonStr); }

    /**
     * 取得分派部門 sid
     * 
     * @return
     */
    public Set<Integer> getTransdepSids() {
        //
        return WorkConnectVOUtils.prepareUserDepSids(getTransdep());
    }

    public void setTransdep(UserDep userDep) { this.transdepJsonStr = new UserDepConverter().convertToDatabaseColumn(userDep); }

    public UsersTo getExecSignMember() { return new UsersConverter().convertToEntityAttribute(this.execSignMemberJsonStr); }

    /**
     * 取得簽核人員
     * 
     * @return
     */
    public Set<Integer> getExecSignMemberUserSids() { return WorkConnectVOUtils.prepareUsersToSids(this.getExecSignMember()); }

    public void setExecSignMember(UsersTo usersTo) { this.execSignMemberJsonStr = new UsersConverter().convertToDatabaseColumn(usersTo); }

    public UsersTo getManagerRecevice() { return new UsersConverter().convertToEntityAttribute(this.managerReceviceJsonStr); }

    public void setManagerRecevice(UsersTo usersTo) { this.managerReceviceJsonStr = new UsersConverter().convertToDatabaseColumn(usersTo); }

    /**
     * 取得可領單人員
     * 
     * @return
     */
    public UsersTo getReceviceUser() { return new UsersConverter().convertToEntityAttribute(this.receviceUserJsonStr); }

    /**
     * 取得可領單人員
     * 
     * @return
     */
    public Set<Integer> getReceviceUserSid() {
        // 轉為 sid
        return WorkConnectVOUtils.prepareUsersToSids(this.getReceviceUser());
    }

    public void setReceviceUser(UsersTo usersTo) { this.receviceUserJsonStr = new UsersConverter().convertToDatabaseColumn(usersTo); }

    /**
     * 狀態是否為【啟用】
     *
     * @return 是/否
     */
    public boolean isActive() { return Activation.ACTIVE.equals(this.status); }

}
