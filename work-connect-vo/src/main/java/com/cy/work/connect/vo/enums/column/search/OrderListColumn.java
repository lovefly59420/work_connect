package com.cy.work.connect.vo.enums.column.search;

import com.cy.work.connect.vo.enums.Column;

public enum OrderListColumn implements Column {
    INDEX("序", true, false, false, "30"),
    CREATE_DATE("建立日期", true, true, false, "80"),
    CATEGORY_NAME("類別", true, true, false, "120"),
    MENUTAG_NAME("單據名稱", true, true, false, "120"),
    APPLICATION_USER_NAME("申請人", true, true, false, "80"),
    THEME("主題", true, true, false, ""),
    STATUS_NAME("單據狀態", true, true, false, "70"),
    WC_NO("單號", true, true, false, "130");

    /**
     * 欄位名稱
     */
    private final String name;
    /**
     * 預設是否顯示
     */
    private final boolean defaultShow;
    /**
     * 是否可修改欄位寬度
     */
    private final boolean canModifyWidth;
    /**
     * 是否可修改是否顯示
     */
    private final boolean canSelectItem;
    /**
     * 預設欄位寬度
     */
    private final String defaultWidth;

    OrderListColumn(
        String name,
        boolean defaultShow,
        boolean canModifyWidth,
        boolean canSelectItem,
        String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
