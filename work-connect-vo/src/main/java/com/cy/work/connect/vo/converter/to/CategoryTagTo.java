/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter.to;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自訂物件 - 類別Sid( 小類)
 *
 * @author kasim
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryTagTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8390960053182632750L;

    private List<String> tagSids = Lists.newArrayList();
}
