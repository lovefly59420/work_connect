/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums.column.search;

import com.cy.work.connect.vo.enums.Column;

/**
 * 備忘錄查詢 欄位參數
 *
 * @author kasim
 */
public enum MemoColumn implements Column {
    INDEX("序", true, false, false, "30"),
    CREATE_DATE("建立日期", true, true, false, "75"),
    CREATE_DEP("建立部門", true, true, false, "150"),
    THEME("主題", true, false, false, ""),
    TRANSWC_STATUS("是否已轉聯絡單", true, true, false, "130"),
    READ_STATUS("閱讀狀態", true, true, false, "100"),
    MEMO_NO("單號", true, true, false, "130");

    /**
     * 欄位名稱
     */
    private final String name;
    /**
     * 預設是否顯示
     */
    private final boolean defaultShow;
    /**
     * 是否可修改欄位寬度
     */
    private final boolean canModifyWidth;
    /**
     * 是否可修改是否顯示
     */
    private final boolean canSelectItem;
    /**
     * 預設欄位寬度
     */
    private final String defaultWidth;

    MemoColumn(
        String name,
        boolean defaultShow,
        boolean canModifyWidth,
        boolean canSelectItem,
        String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
