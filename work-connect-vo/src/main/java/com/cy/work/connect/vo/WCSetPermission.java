/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.common.eo.AbstractEo;
import com.cy.work.connect.vo.converter.Json2LongListConverter;
import com.cy.work.connect.vo.converter.TargetTypeConverter;
import com.cy.work.connect.vo.enums.TargetType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 工作聯絡單 設定權限
 *
 * @author jimmy_chou
 */
@Entity
@Table(name = "wc_set_permission")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(
    of = {"sid"},
    callSuper = false)
@Data
public class WCSetPermission extends AbstractEo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 451404222405313160L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wc_set_permission_sid")
    private Long sid;

    /**
     * 公司sid
     */
    @Column(name = "comp_sid")
    private Integer compSid;

    /**
     * 設定目標SID
     */
    @Column(name = "target_sid", nullable = false, length = 36)
    private String targetSid;

    /**
     * 設定來源(大類、中類、小類、執行單位、模版)
     */
    @Column(name = "target_type", nullable = false)
    @Convert(converter = TargetTypeConverter.class)
    private TargetType targetType;

    /**
     * 授權角色清單（JSON String） Json2IntListConverter
     */
    @Convert(converter = Json2LongListConverter.class)
    @Column(name = "permission_group")
    private List<Long> permissionGroups;
}
