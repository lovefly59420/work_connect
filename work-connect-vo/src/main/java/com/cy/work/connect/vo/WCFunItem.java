/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 工作聯絡單選單細項
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_fun_item")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCFunItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4559239833854058963L;

    @Id
    @Column(name = "sid", nullable = false)
    private Integer sid;

    @Column(name = "category_model", nullable = false, length = 45)
    private String category_model;

    @Column(name = "fun_item_group_sid", nullable = false)
    private Integer fun_item_group_sid;

    @Column(name = "function_title", nullable = false, length = 45)
    private String function_title;

    @Column(name = "component_id", nullable = false, length = 45)
    private String component_id;

    @Column(name = "url", nullable = false, length = 45)
    private String url;

    @Column(name = "seq", nullable = false)
    private Integer seq;
}
