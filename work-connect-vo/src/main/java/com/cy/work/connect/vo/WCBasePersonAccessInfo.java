/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.work.connect.vo.converter.LimitBaseAccessViewPersonConverter;
import com.cy.work.connect.vo.converter.OtherBaseAccessViewPersonConverter;
import com.cy.work.connect.vo.converter.to.LimitBaseAccessViewPerson;
import com.cy.work.connect.vo.converter.to.OtherBaseAccessViewPerson;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_base_person_access_info")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCBasePersonAccessInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4529972100699157310L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "base_person_access_sid", length = 36)
    private String sid;

    @Column(name = "login_user", nullable = false)
    private Integer loginUserSid;

    @Column(name = "limit_base_access_view_person")
    @Convert(converter = LimitBaseAccessViewPersonConverter.class)
    private LimitBaseAccessViewPerson limitBaseAccessViewPerson;

    @Column(name = "other_base_access_view_person")
    @Convert(converter = OtherBaseAccessViewPersonConverter.class)
    private OtherBaseAccessViewPerson otherBaseAccessViewPerson;

    @Column(name = "create_usr", nullable = false)
    private Integer create_usr_sid;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;

    @Column(name = "update_usr", nullable = true)
    private Integer update_usr_sid;

    @Column(name = "update_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
}
