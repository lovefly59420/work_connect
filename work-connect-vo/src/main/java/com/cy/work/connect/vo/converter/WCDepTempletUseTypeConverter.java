package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 部門模版設定使用別 Converter
 *
 * @author allen
 */
@Slf4j
@Converter
public class WCDepTempletUseTypeConverter implements AttributeConverter<ConfigTempletType, String> {

    @Override
    public String convertToDatabaseColumn(ConfigTempletType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ConfigTempletType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ConfigTempletType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ConfigTempletType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
