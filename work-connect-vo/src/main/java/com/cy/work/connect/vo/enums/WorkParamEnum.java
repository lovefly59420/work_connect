/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums;

/**
 * 特殊參數
 *
 * @author brain0925_liao
 */
public enum WorkParamEnum {
    BIG_BOSS_SIDS("BIG BOSS 名單"),
    REST_RESPONSE_TIME_BOUND("REST回應毫秒界限"),
    SHOW_SEARCH_SQL("報表查詢時，在LOG顯示查詢SQL"),
    SHOW_DEBUG_FOR_WAIT_ASSIGN("SHOW_DEBUG_FOR_WAIT_ASSIGN"),
    ;

    private final String val;

    WorkParamEnum(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
