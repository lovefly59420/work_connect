/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.enums.column.search;

import com.cy.work.connect.vo.enums.Column;

/**
 * 待領單據清單 欄位參數
 *
 * @author kasim
 */
public enum WaitWorkReceiveListColumn implements Column {
    INDEX("序", true, false, false, "30"),
    REQUIRE_ESTABLISH_DATE("需求成立日", true, true, false, "120"),
    REQUIRE_DEP_NAME("需求單位", true, true, false, "90"),
    REQUIRE_USER_NAME("需求人員", true, true, false, "90"),
    MENUTAG_NAME("單據名稱", true, true, false, "120"),
    THEME("主題", true, false, false, ""),
    WC_NO("單號", true, true, false, "130");

    /**
     * 欄位名稱
     */
    private final String name;
    /**
     * 預設是否顯示
     */
    private final boolean defaultShow;
    /**
     * 是否可修改欄位寬度
     */
    private final boolean canModifyWidth;
    /**
     * 是否可修改是否顯示
     */
    private final boolean canSelectItem;
    /**
     * 預設欄位寬度
     */
    private final String defaultWidth;

    WaitWorkReceiveListColumn(
        String name,
        boolean defaultShow,
        boolean canModifyWidth,
        boolean canSelectItem,
        String defaultWidth) {
        this.name = name;
        this.defaultShow = defaultShow;
        this.canModifyWidth = canModifyWidth;
        this.defaultWidth = defaultWidth;
        this.canSelectItem = canSelectItem;
    }

    @Override
    public boolean getCanSelectItem() {
        return canSelectItem;
    }

    @Override
    public boolean getDefaultShow() {
        return defaultShow;
    }

    @Override
    public boolean getCanModifyWidth() {
        return canModifyWidth;
    }

    @Override
    public String getDefaultWidth() {
        return defaultWidth;
    }

    @Override
    public String getVal() {
        return name;
    }
}
