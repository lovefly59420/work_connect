/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.vo.converter.to.GroupInfo;
import com.cy.work.connect.vo.converter.to.GroupInfoTo;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 群組資訊Converter
 *
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class GroupInfoConverter implements AttributeConverter<GroupInfo, String> {

    @Override
    public String convertToDatabaseColumn(GroupInfo attribute) {
        if (attribute == null) {
            return "";
        }
        String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(attribute.getGroupInfoTos());
        return json;
    }

    @Override
    public GroupInfo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new GroupInfo();
        }
        try {
            GroupInfo to = new GroupInfo();
            List<GroupInfoTo> resultList =
                WkJsonUtils.getInstance().fromJsonToList(dbData, GroupInfoTo.class);
            to.setGroupInfoTos(resultList);
            return to;
        } catch (IOException ex) {
            log.error("parser json to GroupInfoController fail!!" + ex.getMessage(), ex);
            return new GroupInfo();
        }
    }
}
