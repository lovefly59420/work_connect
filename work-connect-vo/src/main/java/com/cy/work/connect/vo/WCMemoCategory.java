/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.converter.UserDepConverter;
import com.cy.work.connect.vo.converter.to.UserDep;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_memo_category")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
public class WCMemoCategory implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3413167293017787083L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_memo_category_sid", length = 36)
    private String sid;

    /**
     * 類別名稱
     */
    @Column(name = "wc_memo_category_name", nullable = true, length = 255)
    private String memoCategoryName;

    /**
     * 可使用的單位
     */
    @Column(name = "use_dep", nullable = true)
    @Convert(converter = UserDepConverter.class)
    private UserDep useDep;

    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /**
     * 可編輯的單位
     */
    @Column(name = "edit_dep", nullable = true)
    @Convert(converter = UserDepConverter.class)
    private UserDep editDep;

    /**
     * 建單者Sid
     */
    @Column(name = "create_usr", nullable = false)
    private Integer create_usr;
    /**
     * 建單時間
     */
    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date create_dt;
    /**
     * 更新者Sid
     */
    @Column(name = "update_usr", nullable = true)
    private Integer update_usr;
    /**
     * 更新時間
     */
    @Column(name = "update_dt", nullable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date update_dt;
}
