/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.common.vo.listener.RemoveContorlCharListener;
import com.cy.work.connect.vo.converter.ManagerSingInfoTypeController;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

/**
 * 意見說明
 *
 * @author brain0925_liao
 */
@Entity
@Table(name = "wc_opinion_descripe_setting")
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = {"sid"})
@Data
@EntityListeners(RemoveContorlCharListener.class) // 控制字元移除器
public class WCOpinionDescripeSetting implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5078523519582337231L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "wc_opinion_descripe_setting_sid", length = 36)
    private String sid;

    @Column(name = "wcSid", nullable = false, length = 36)
    private String wcSid;

    /*簽核類型Type*/
    @Convert(converter = ManagerSingInfoTypeController.class)
    @Column(name = "sourceType", nullable = false, length = 36)
    private ManagerSingInfoType sourceType;

    /*簽核類型Sid*/
    @Column(name = "sourceSid", nullable = false, length = 36)
    private String sourceSid;

    /*意見說明發表人員*/
    @Column(name = "opinion_usr", nullable = false)
    private Integer opinion_usr;

    /*意見說明*/
    @Column(name = "opinion_descripe", nullable = true)
    private String opinion_descripe;

    @Column(name = "rollBackReason", nullable = true)
    private String rollBackReason;

    @Column(name = "invaildReason", nullable = true)
    private String invaildReason;
    /*狀態*/
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    @Column(name = "create_usr", nullable = false)
    private Integer createUser;

    @Column(name = "create_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "update_usr", nullable = true)
    private Integer updateUser;

    @Column(name = "update_dt")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;
}
