/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Converter
@Slf4j
public class ManagerSingInfoTypeController
    implements AttributeConverter<ManagerSingInfoType, String> {

    @Override
    public String convertToDatabaseColumn(ManagerSingInfoType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.name();
    }

    @Override
    public ManagerSingInfoType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ManagerSingInfoType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ManagerSingInfoType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
