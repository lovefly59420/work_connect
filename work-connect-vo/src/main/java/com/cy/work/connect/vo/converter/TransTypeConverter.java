package com.cy.work.connect.vo.converter;

import com.cy.work.connect.vo.enums.TransType;
import com.google.common.base.Strings;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 轉單類型 Converter
 *
 * @author jimmy_chou
 */
@Slf4j
@Converter
public class TransTypeConverter implements AttributeConverter<TransType, String> {

    @Override
    public String convertToDatabaseColumn(TransType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public TransType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return TransType.valueOf(dbData);
        } catch (Exception e) {
            log.error("TransType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }
}
