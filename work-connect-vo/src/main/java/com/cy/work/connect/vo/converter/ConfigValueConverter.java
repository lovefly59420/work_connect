package com.cy.work.connect.vo.converter;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.slf4j.Slf4j;

/**
 * 模版設定值 Converter
 *
 * @author allen
 */
@Slf4j
@Converter
public class ConfigValueConverter implements AttributeConverter<ConfigValueTo, String> {

    /*
     * (non-Javadoc)
     *
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.
     * Object)
     */
    @Override
    public String convertToDatabaseColumn(ConfigValueTo configValueTo) {
        if (configValueTo == null) {
            return "{}";
        }
        String jsonStr = new Gson().toJson(configValueTo);
        if (WkStringUtils.isEmpty(jsonStr)) {
            return "{}";
        }

        return jsonStr;
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.
     * Object)
     */
    @Override
    public ConfigValueTo convertToEntityAttribute(String jsonStrFromDB) {
        if (Strings.isNullOrEmpty(jsonStrFromDB)) {
            return new ConfigValueTo();
        }
        try {

            ConfigValueTo configValueTo =
                new Gson().fromJson(jsonStrFromDB, new TypeToken<ConfigValueTo>() {
                }.getType());

            if (configValueTo != null) {
                return configValueTo;
            }
            return new ConfigValueTo();

        } catch (Exception e) {
            log.error("ConfigValueConverter 轉型失敗。 jsonStrFromDB ：【" + jsonStrFromDB + "】", e);
            return new ConfigValueTo();
        }
    }
}
