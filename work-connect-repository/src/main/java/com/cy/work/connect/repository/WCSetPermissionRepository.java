package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.enums.TargetType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作聯絡單 設定權限 Repository
 *
 * @author jimmy_chou
 */
public interface WCSetPermissionRepository
    extends JpaRepository<WCSetPermission, Long>, Serializable {

    List<WCSetPermission> findByCompSid(Integer compSid);

    List<WCSetPermission> findByCompSidAndTargetTypeIn(
        Integer compSid,
        List<TargetType> targetTypes);

    List<WCSetPermission> findByTargetType(TargetType targetType);

    WCSetPermission findBySid(Long sid);

    /**
     * 以公司SID、設定目標SID、設定來源 查詢
     *
     * @param compSid
     * @param targetSid
     * @param targetType
     * @return
     */
    @Query(
        "SELECT o "
            + " FROM #{#entityName} o "
            + " WHERE o.compSid = :compSid"
            + " AND o.targetSid=:targetSid"
            + " AND o.targetType = :targetType")
    WCSetPermission findPermissionByCompSidAndTargetSidAndTargetType(
        @Param("compSid") Integer compSid,
        @Param("targetSid") String targetSid,
        @Param("targetType") TargetType targetType);

    /**
     * 以公司SID、群組SID 查詢
     *
     * @param compSid
     * @param groupSid
     * @return
     */
    @Query(
        value =
            "SELECT o.* "
                + " FROM wc_set_permission o "
                + " WHERE o.comp_sid = :compSid "
                + " AND JSON_CONTAINS(o.permission_group, :groupSid) = 1 ",
        nativeQuery = true)
    List<WCSetPermission> findPermissionByCompSidAndGroupSid(
        @Param("compSid") Integer compSid,
        @Param("groupSid") Long groupSid);
}
