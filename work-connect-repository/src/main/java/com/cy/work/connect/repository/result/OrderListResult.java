package com.cy.work.connect.repository.result;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(of = {"wcSid"})
@Entity
public class OrderListResult implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7039729390466242616L;

    /**
     * UUID
     */
    @Getter
    @Setter
    private String wcSid;

    /**
     * 建立日期
     */
    @Getter
    @Setter
    private Date createDate;

    /**
     * 類別
     */
    @Getter
    @Setter
    private String categoryName;

    /**
     * 單據名稱
     */
    @Getter
    @Setter
    private String menuTagName;

    /**
     * 申請人Sid
     */
    @Getter
    @Setter
    private Integer userSid;

    /**
     * 申請人
     */
    @Getter
    @Setter
    private String applicant;

    /**
     * 主題
     */
    @Getter
    @Setter
    private String theme;

    /**
     * 內容
     */
    @Getter
    @Setter
    private String content;

    /**
     * 單據狀態
     */
    @Getter
    @Setter
    private String status;
    
    /**
     * 單據狀態
     */
    @Getter
    @Setter
    private String statusName;

    /**
     * 單號
     */
    @Getter
    @Setter
    private String wcNo;

    /**
     * 小類
     */
    @Getter
    @Setter
    private String wcTagSids;

    @Getter
    @Setter
    private String replyNotRead;
    @Getter
    @Setter
    private String unReadCount;
    @Getter
    @Setter
    private boolean hasUnReadCount = false;

    @Getter
    @Setter
    private Integer resultIndex;

    public OrderListResult() {
    }

    public OrderListResult(String wcSid) {
        this.wcSid = wcSid;
    }

    public OrderListResult(
        String wcSid,
        Date createDate,
        String categoryName,
        String menuTagName,
        Integer userSid,
        String theme,
        String status,
        String wcNo,
        String wcTagSids,
        String replyNotRead,
        String content) {
        super();
        this.wcSid = wcSid;
        this.createDate = createDate;
        this.categoryName = categoryName;
        this.menuTagName = menuTagName;
        this.userSid = userSid;
        this.theme = theme;
        this.status = status;
        this.wcNo = wcNo;
        this.wcTagSids = wcTagSids;
        this.replyNotRead = replyNotRead;
        this.content = content;
    }

    public void replaceValue(OrderListResult updateDeail) {
        this.theme = updateDeail.getTheme();
        this.status = updateDeail.getStatus();
    }
}
