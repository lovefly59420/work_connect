package com.cy.work.connect.repository;

import com.cy.work.connect.repository.vo.Setting11BaseDtVO;
import com.cy.work.connect.vo.WCTransMaster;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 轉換程式主檔 repository
 *
 * @author jimmy_chou
 */
public interface WCTransMasterRepository
    extends JpaRepository<WCTransMaster, String>, Serializable {

    @Query("SELECT count(t.sid) FROM #{#entityName} t " + "WHERE t.transNo like :transNo ")
    Integer countNoByHead(@Param("transNo") String transNo);

    /**
     * 依據執行單位取得組織異動轉換管理資料
     *
     * @param beforOrgSid
     * @return
     */
    @Query(nativeQuery = true, name = "queryForExecDep")
    List<Setting11BaseDtVO> queryForExecDep(@Param("beforOrgSid") Integer beforOrgSid);

    /**
     * 依據指派單位取得組織異動轉換管理資料
     *
     * @param beforOrgSid
     * @return
     */
    @Query(nativeQuery = true, name = "queryForTransDep")
    List<Setting11BaseDtVO> queryForTransDep(@Param("beforOrgSid") Integer beforOrgSid);
}
