/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.commons.vo.Role;
import com.cy.work.connect.vo.SpecificRoleForwardDep;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 轉寄部門
 *
 * @author brain0925_liao
 */
public interface SpecificRoleForwardDepRepository
    extends JpaRepository<SpecificRoleForwardDep, Long>, Serializable {

    List<SpecificRoleForwardDep> findByRoleIn(List<Role> roles);
}
