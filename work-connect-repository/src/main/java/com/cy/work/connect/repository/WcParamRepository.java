/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WorkParam;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * 工作記錄參數查詢
 *
 * @author shaun
 */
public interface WcParamRepository extends JpaRepository<WorkParam, Long>, Serializable {

    /**
     * @param keyword 關鍵字
     * @return WorkBackendParam
     */
    WorkParam findByKeyword(WorkParam keyword);

    /**
     * @return max Seq
     */
    @Query("SELECT max(o.seq) FROM #{#entityName} o")
    Integer maxSeq();
}
