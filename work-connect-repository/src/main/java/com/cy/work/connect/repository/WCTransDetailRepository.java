package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCTransDetail;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 轉換程式明細檔 repository
 *
 * @author jimmy_chou
 */
public interface WCTransDetailRepository
    extends JpaRepository<WCTransDetail, String>, Serializable {

}
