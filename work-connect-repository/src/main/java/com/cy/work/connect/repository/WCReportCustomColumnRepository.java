/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCReportCustomColumn;
import com.cy.work.connect.vo.enums.WCReportCustomColumnUrlType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作聯絡單自訂欄位
 *
 * @author brain0925_liao
 */
public interface WCReportCustomColumnRepository
    extends JpaRepository<WCReportCustomColumn, String>, Serializable {

    /**
     * 搜尋自訂欄位資廖 By 使用者及URL
     */
    @Query("SELECT o FROM #{#entityName} o WHERE o.url= :url and o.usersid = :usersid  ")
    List<WCReportCustomColumn> getWkReportCustomColumnByUrlAndUserSid(
        @Param("url") WCReportCustomColumnUrlType url,
        @Param("usersid") Integer usersid);
}
