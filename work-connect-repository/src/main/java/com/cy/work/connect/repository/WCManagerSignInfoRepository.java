/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 需求方簽核資訊 DAO
 *
 * @author kasim
 */
public interface WCManagerSignInfoRepository
        extends JpaRepository<WCManagerSignInfo, String>, Serializable {

    /**
     * find by Sid
     * 
     * @param sid sid
     * @return WCManagerSignInfo
     */
    public WCManagerSignInfo findBySid(String sid);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid 工作聯絡單號
     * @return
     */
    List<WCManagerSignInfo> findByWcSid(String wcSid);

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.wcSid = :wcSid")
    List<WCManagerSignInfo> findOneByWCSid(@Param("wcSid") String wcSid);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid  主單 SID
     * @param status 狀態
     * @param signType 簽核類型
     * @return WCManagerSignInfo list
     */
    List<WCManagerSignInfo> findByWcSidAndStatusAndSignType(
            String wcSid, 
            BpmStatus status,
            SignType signType
            );

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.wcSid = :wcSid " + "AND w.seq = 0 ")
    WCManagerSignInfo findOneByWCSidAndSeq(@Param("wcSid") String wcSid);
}
