package com.cy.work.connect.repository.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author jimmy_chou
 */
@NoArgsConstructor
public class Setting11BaseDtVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8235846133439639478L;
    /**
     * 狀態
     */
    @Getter
    @Setter
    String status;
    /**
     * 執行單位設定 Sid
     */
    @Getter
    @Setter
    private String execDepSettingSid;
    /**
     * (小)執行項目 Sid
     */
    @Getter
    @Setter
    private String tagSid;
    /**
     * (小)執行項目 名稱
     */
    @Getter
    @Setter
    private String tagName;
    /**
     * (中)單據名稱 名稱
     */
    @Getter
    @Setter
    private String menuName;
    /**
     * (大)類別 名稱
     */
    @Getter
    @Setter
    private String categoryName;

    public Setting11BaseDtVO(
        String categoryName,
        String menuName,
        String tagName,
        String tagSid,
        String execDepSettingSid) {
        this.categoryName = categoryName;
        this.menuName = menuName;
        this.tagName = tagName;
        this.tagSid = tagSid;
        this.execDepSettingSid = execDepSettingSid;
    }
}
