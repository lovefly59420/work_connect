/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCTagTemplet;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 執行項目建議勾選模版 Repository
 *
 * @author jimmy_chou
 */
public interface WCTagTempletRepository extends JpaRepository<WCTagTemplet, Long>, Serializable {

    /**
     * 更新建議勾選項目設定
     *
     * @param sid
     * @param defaultValue
     * @param updatedUser
     * @param updatedDate
     * @return
     */
    @Modifying
    @Transactional(rollbackFor = Exception.class)
    @Query(
        "UPDATE #{#entityName} t "
            + "SET t.defaultValue = :defaultValue "
            + ", t.updateUser = :updatedUser "
            + ", t.updateDate = :updatedDate "
            + "WHERE t.sid = :sid ")
    int updateConditionData(
        @Param("sid") Long sid,
        @Param("defaultValue") String defaultValue,
        @Param("updatedUser") Integer updatedUser,
        @Param("updatedDate") Date updatedDate);

    /**
     * 以單據名稱sid AND 公司sid查詢
     *
     * @param menuTagSid
     * @param compSid
     * @return
     */
    List<WCTagTemplet> findByMenuTagSidAndCompSidOrderBySortSeq(
        String menuTagSid,
        Integer compSid);

    /**
     * 以公司sid查詢
     *
     * @param compSid
     * @return
     */
    List<WCTagTemplet> findByCompSid(Integer compSid);

    /**
     * 使用名稱清單 - 依公司Sid查詢
     *
     * @param compSid
     * @return
     */
    @Query(
        value =
            "SELECT distinct usename "
                + "FROM wc_tag_templet "
                + "WHERE comp_sid = :compSid "
                + "AND usename is not null "
                + "AND usename <> '' "
                + "ORDER BY usename ",
        nativeQuery = true)
    List<String> findDistinctUseNameByCompSid(@Param("compSid") Integer compSid);

    /**
     * 判斷使用名稱是否已存在
     *
     * @param usename
     * @param menuTagSid
     * @param compSid
     * @return
     */
    @Query(
        "SELECT CASE WHEN (COUNT(e) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} e "
            + "WHERE e.usename = :usename "
            + "AND e.menuTagSid = :menuTagSid "
            + "AND e.compSid = :compSid ")
    Boolean isExistUsenameByUsenameAndMenuTagSidAndCompSid(
        @Param("usename") String usename,
        @Param("menuTagSid") String menuTagSid,
        @Param("compSid") Integer compSid);
}
