/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.WCAgainTrace;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作聯絡單再次追蹤 DAO
 *
 * @author kasim
 */
public interface WCAgainTraceRepository extends JpaRepository<WCAgainTrace, String>, Serializable {

    @Query(
        "SELECT o FROM #{#entityName} o "
            + "WHERE o.status = :status "
            + "AND o.wcSid = :wcSid "
            + "ORDER BY o.updateDate DESC ")
    List<WCAgainTrace> findByStatusAndWcSid(
        @Param("status") Activation status,
        @Param("wcSid") String wcSid);

    @Query(
        "SELECT o FROM #{#entityName} o "
            + "WHERE o.status = :status "
            + "AND o.traceSid = :traceSid "
            + "ORDER BY o.updateDate DESC ")
    List<WCAgainTrace> findByStatusAndTraceSid(
        @Param("status") Activation status,
        @Param("traceSid") String traceSid);
}
