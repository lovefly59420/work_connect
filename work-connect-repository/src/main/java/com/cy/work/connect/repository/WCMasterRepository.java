/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.converter.to.ReadRecord;
import com.cy.work.connect.vo.converter.to.ReplyRecord;
import com.cy.work.connect.vo.enums.WCStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作聯絡單主檔
 *
 * @author brain0925_liao
 */
public interface WCMasterRepository extends JpaRepository<WCMaster, String>, Serializable {

    /**
     * 以 SID 查詢
     * @param sid sid
     * @return 聯絡單主檔
     */
    public WCMaster findBySid(String sid);
    
    @Query(
        value =
            "SELECT COUNT(*) "
                + "FROM wc_master r "
                + "WHERE r.menuTag_sid in (SELECT m.wc_menu_tag_sid "
                + "FROM wc_menu_tag m "
                + "WHERE m.wc_category_tag_sid = :CategoryTagSid) ",
        nativeQuery = true)
    Integer findByCategoryTag(@Param("CategoryTagSid") String categoryTagSid);

    @Query(
        value = "SELECT COUNT(*) " + "FROM wc_master r " + "WHERE r.menuTag_sid = :menuTagSid ",
        nativeQuery = true)
    Integer findByMenuTagSid(@Param("menuTagSid") String menuTagSid);

    @Query(
        value =
            "SELECT COUNT(*) "
                + "FROM wc_master r "
                + "WHERE json_contains(r.category_tag,:tagSid,'$.tagSids') = 1 ",
        nativeQuery = true)
    Integer findByTag(@Param("tagSid") String tagSid);

    @Query("SELECT r FROM #{#entityName} r WHERE r.wc_no = :wc_no")
    List<WCMaster> findByNo(@Param("wc_no") String wc_no);

    @Query("SELECT r FROM #{#entityName} r WHERE r.create_usr = :create_usr")
    List<WCMaster> findByCreateUserSid(@Param("create_usr") Integer createUsrSid);

    /**
     * 搜尋[提交或作廢]工作聯絡單號 By 建立日期區間
     */
    @Query(
        "SELECT wc_no FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end AND r.comp_sid = :companySid order by r.create_dt DESC")
    List<String> findTodayLastWCMaster(
        @Param("start") Date start,
        @Param("end") Date end,
        @Param("companySid") Integer companySid);

    /**
     * 搜尋工作聯絡單號筆數[提交或作廢] By 建立日期區間
     */
    @Query(
        "SELECT COUNT(r) AS size FROM #{#entityName} r WHERE r.create_dt BETWEEN :start AND :end AND r.comp_sid = :companySid")
    Integer findTodayWCMasterTotalSize(
        @Param("start") Date start,
        @Param("end") Date end,
        @Param("companySid") Integer companySid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.replyRead  = :reply_not_read " + " WHERE r.sid = :wc_sid")
    int updateReplyNotRead(
        @Param("reply_not_read") ReplyRecord reply_not_read,
        @Param("wc_sid") String wc_sid);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} r SET r.read_record  = :read_record " + " WHERE r.sid = :wc_sid")
    int updateReadRecord(
        @Param("read_record") ReadRecord read_record,
        @Param("wc_sid") String wc_sid);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.lock_dt  = :lock_dt,r.lock_usr = :lock_usr "
            + " WHERE r.sid = :wc_sid")
    int updateLockUserAndLockDate(
        @Param("lock_dt") Date lock_dt,
        @Param("lock_usr") Integer lock_usr,
        @Param("wc_sid") String wc_sid);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.wc_status  = :wc_status, "
            + " r.update_usr = :update_usr, "
            + " r.update_dt = :update_dt "
            + " WHERE r.sid = :wc_sid")
    int updateWCStatus(
        @Param("wc_status") WCStatus wc_status,
        @Param("wc_sid") String wc_sid,
        @Param("update_usr") Integer update_usr,
        @Param("update_dt") Date update_dt);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.wc_status  = :wc_status, "
            + " r.update_usr = :update_usr, "
            + " r.update_dt = :update_dt, "
            + " r.require_establish_dt = :update_dt "
            + " WHERE r.sid = :wc_sid")
    int updateWCStatusAndRequireEstablishDate(
        @Param("wc_status") WCStatus wc_status,
        @Param("wc_sid") String wc_sid,
        @Param("update_usr") Integer update_usr,
        @Param("update_dt") Date update_dt);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} r SET r.lock_dt  = :lock_dt,r.lock_usr = :lock_usr "
            + " WHERE r.sid = :wc_sid and r.lock_usr  = :closeUserSid")
    int closeLockUserAndLockDate(
        @Param("lock_dt") Date lock_dt,
        @Param("lock_usr") Integer lock_usr,
        @Param("wc_sid") String wc_sid,
        @Param("closeUserSid") Integer closeUserSid);
}
