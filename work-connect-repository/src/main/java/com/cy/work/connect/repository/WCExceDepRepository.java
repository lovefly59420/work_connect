/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.enums.WCExceDepStatus;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author brain0925_liao
 */
public interface WCExceDepRepository extends JpaRepository<WCExceDep, String>, Serializable {

    /**
     * 以 sid 查詢
     * 
     * @param sids sid list
     * @return
     */
    List<WCExceDep> findBySidIn(Set<String> sids);

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.wcSid = :wcSid")
    List<WCExceDep> findByWcSid(@Param("wcSid") String wcSid);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid  主單 sid
     * @param status 資料狀態
     * @return WCExceDep list
     */
    List<WCExceDep> findByWcSidAndStatus(String wcSid, Activation status);
    
    /**
     * 以下列條件查詢
     * @param wcSids 主單 sid list
     * @param status 資料狀態
     * @return WCExceDep list
     */
    List<WCExceDep> findByWcSidInAndStatus(List<String> wcSids, Activation status);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid         主單 sid
     * @param execDepStatus 執行部門狀態
     * @param status        資料狀態
     * @return WCExceDep list
     */
    List<WCExceDep> findByWcSidAndExecDepStatusAndStatus(
            String wcSid,
            WCExceDepStatus execDepStatus,
            Activation status);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid                       主單 sid
     * @param execManagerSignInfoGroupSeq 群組序號
     * @param status                      資料狀態
     * @return WCExceDep list
     */
    List<WCExceDep> findByWcSidAndExecManagerSignInfoGroupSeqAndStatus(
            String wcSid,
            Integer execManagerSignInfoGroupSeq,
            Activation status);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid       主單 sid
     * @param execUserSid 執行人員 SID
     * @param status      資料狀態
     * @return WCExceDep list
     */
    List<WCExceDep> findByWcSidAndExecUserSidAndStatus(
            String wcSid,
            Integer execUserSid,
            Activation status);

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.execDepSettingSid = :execDepSettingSid")
    List<WCExceDep> findOneByExecDepSettingSid(
            @Param("execDepSettingSid") String execDepSettingSid);

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.dep_sid = :dep_sid")
    List<WCExceDep> findByDepSid(@Param("dep_sid") Integer dep_sid);

    @Query("SELECT w FROM #{#entityName} w " + "WHERE w.execUserSid = :execUserSid")
    List<WCExceDep> findByExceUserSid(@Param("execUserSid") Integer execUserSid);

    @Query("SELECT w FROM #{#entityName} w "
            + "WHERE w.execManagerSignInfoGroupSeq = :execManagerSignInfoGroupSeq And w.wcSid = :wcSid And w.status = 0 ")
    List<WCExceDep> findOneByGroupSeqAndWcSId(
            @Param("execManagerSignInfoGroupSeq") Integer execManagerSignInfoGroupSeq,
            @Param("wcSid") String wcSid);

    @Query(value = "SELECT distinct m.wc_no "
            + "FROM wc_exec_dep w "
            + "JOIN wc_master m ON m.wc_sid = w.wc_sid "
            + "LEFT JOIN wc_exec_dep_setting ex ON ex.wc_exec_dep_setting_sid = w.wc_exec_dep_setting_sid "
            + "WHERE w.exec_dep_status NOT IN ('CLOSE', 'STOP') "
            + "AND ( w.wc_exec_dep_setting_sid in ( :execDepSettingSids ) "
            + " OR ( ex.wc_exec_dep_setting_sid IS NULL "
            + " AND w.dep_sid = :beforOrgSid )) ", nativeQuery = true)
    List<String> findWCNOsByExecDepSettingSids(
            @Param("execDepSettingSids") List<String> execDepSettingSids,
            @Param("beforOrgSid") Integer beforOrgSid);
}
