/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCAlert;
import com.cy.work.connect.vo.enums.WCAlertType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作聯絡單 最新消息
 *
 * @author kasim
 */
public interface WCAlertRepository extends JpaRepository<WCAlert, String>, Serializable {

    @Query(
        value =
            "SELECT o.* FROM wc_alert o "
                + "WHERE o.read_dt IS NULL "
                + "AND o.recipient = :recipient "
                + "order by o.create_dt DESC "
                + "limit :limit ",
        nativeQuery = true)
    List<WCAlert> findByRecipientAndLimit(
        @Param("recipient") Integer recipient,
        @Param("limit") Integer limit);

    @Query(
        "SELECT o FROM #{#entityName} o "
            + "WHERE o.readDate IS NULL "
            + "AND o.recipient = :recipient "
            + "order by o.createDate DESC ")
    List<WCAlert> findByRecipient(@Param("recipient") Integer recipient);

    @Query(
        "SELECT o FROM #{#entityName} o "
            + "WHERE o.readDate IS NOT NULL "
            + "AND o.recipient = :recipient "
            + "AND o.createDate Between :startDate AND :endDate "
            + "order by o.createDate DESC ")
    List<WCAlert> findHistoryByRecipientAndBetweenCreateDate(
        @Param("recipient") Integer recipient,
        @Param("startDate") Date startDate,
        @Param("endDate") Date endDate);

    @Modifying
    @Query(
        "UPDATE #{#entityName} o "
            + "SET o.autoUpdate = 'N' "
            + ", o.readDate = :readDate "
            + "WHERE o.readDate IS NULL "
            + "AND o.sid = :sid ")
    int readAlert(@Param("sid") String sid,
        @Param("readDate") Date readDate);

    @Query(
        "SELECT CASE WHEN (COUNT(e) > 0) THEN 'TRUE' ELSE 'FALSE' END FROM #{#entityName} e "
            + "WHERE e.readDate IS NULL "
            + "AND e.wcSid = :wcSid "
            + "AND e.type = :type "
            + "AND e.recipient = :recipient ")
    Boolean isAnyNotReadByWcSidAndTypeAndRecipient(
        @Param("wcSid") String wcSid,
        @Param("type") WCAlertType type,
        @Param("recipient") Integer recipient);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} o "
            + "SET o.autoUpdate = 'Y' "
            + ", o.readDate = :readDate "
            + "WHERE o.readDate IS NULL "
            + "AND o.wcSid = :wcSid ")
    int readAlertsByClose(@Param("wcSid") String wcSid,
        @Param("readDate") Date readDate);

    @Modifying
    @Transactional
    @Query(
        "UPDATE #{#entityName} o "
            + "SET o.autoUpdate = 'N' "
            + ", o.readDate = :readDate "
            + "WHERE o.readDate IS NULL "
            + "AND o.wcSid = :wcSid "
            + "AND o.recipient = :recipient ")
    int readAlerts(
        @Param("wcSid") String wcSid,
        @Param("recipient") Integer recipient,
        @Param("readDate") Date readDate);
}
