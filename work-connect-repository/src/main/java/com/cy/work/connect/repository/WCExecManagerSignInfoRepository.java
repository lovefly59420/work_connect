/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCExecManagerSignInfo;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author brain0925_liao
 */
public interface WCExecManagerSignInfoRepository
        extends JpaRepository<WCExecManagerSignInfo, String>, Serializable {


    /**
     * 以下列條件查詢
     * 
     * @param sid sid
     * @return WCExecManagerSignInfo
     */
    public WCExecManagerSignInfo findBySid(String sid);

    /**
     * 以下列條件查詢
     * 
     * @param wcSid 主單單號
     * @return WCExecManagerSignInfo list
     */
    public List<WCExecManagerSignInfo> findByWcSid(String wcSid);

    @Query("SELECT MAX(w.groupSeq) FROM #{#entityName} w WHERE w.wcSid = :wcSid")
    public Integer findMaxGroupSeqByWcSid(@Param("wcSid") String wcSid);

    @Query("SELECT MAX(w.groupSeq) FROM #{#entityName} w WHERE w.wcSid = :wcSid AND w.groupSeq = :groupSeq")
    public Integer findMaxSeqByWcSidAndGroupSeq(
            @Param("wcSid") String wcSid,
            @Param("groupSeq") Integer groupSeq);
}
