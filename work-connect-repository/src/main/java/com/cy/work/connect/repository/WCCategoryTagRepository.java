/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.work.connect.vo.WCCategoryTag;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 類別大類 - Repository
 *
 * @author brain0925_liao
 */
public interface WCCategoryTagRepository
        extends JpaRepository<WCCategoryTag, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param compSid 公司SID
     * @return WCCategoryTag list
     */
    public List<WCCategoryTag> findByCompSid(Integer compSid);

    @Query("SELECT DISTINCT r.compSid FROM #{#entityName} r WHERE r.status = com.cy.commons.enums.Activation.ACTIVE")
    List<Integer> findDistinctCompSidByActiveStatus();

    @Query(value = "SELECT ct.* "
            + "FROM wc_category_tag ct "
            + "LEFT JOIN wc_menu_tag mt ON mt.wc_category_tag_sid = ct.wc_category_tag_sid "
            + "LEFT JOIN wc_tag tt ON tt.wc_menu_tag_sid = mt.wc_menu_tag_sid "
            + "WHERE ct.compSid = :companySid "
            + "AND (ifnull(:categoryName, null) is null OR ct.category_name = :categoryName) "
            + "AND (ifnull(:menuName, null) is null OR mt.menu_name = :menuName) "
            + "AND (ifnull(:tagName, null) is null OR tt.tag_name = :tagName) ", nativeQuery = true)
    List<WCCategoryTag> findByTagNameAndCompanySid(
            @Param("companySid") Integer companySid,
            @Param("categoryName") String categoryName,
            @Param("menuName") String menuName,
            @Param("tagName") String tagName);
}
