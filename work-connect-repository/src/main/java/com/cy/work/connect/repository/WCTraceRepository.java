/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.repository;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.WCTraceType;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 工作聯絡單追蹤
 *
 * @author brain0925_liao
 */
public interface WCTraceRepository extends JpaRepository<WCTrace, String>, Serializable {

    /**
     * 以下列條件查詢
     * 
     * @param sid sid
     * @return WCTrace
     */
    public WCTrace findBySid(String sid);

    /**
     * 搜尋工作聯絡單追蹤 BY 工作聯絡單Sid
     */
    @Query("SELECT o FROM #{#entityName} o WHERE o.status=0 and o.wc_sid = :wc_sid ORDER BY o.update_dt DESC ")
    List<WCTrace> getWCTraceByWcSid(@Param("wc_sid") String wc_sid);

    @Query("SELECT o FROM #{#entityName} o "
            + "WHERE o.status = :status  "
            + "AND o.wc_sid in :sids "
            + "AND o.wc_trace_type in :types ")
    List<WCTrace> findByStatusAndSidInAndTypeIn(
            @Param("status") Activation status,
            @Param("sids") List<String> sids,
            @Param("types") List<WCTraceType> types);

    /**
     * 工作聯絡單模糊搜尋 BY 工作聯絡單Sid
     */
    @Query("SELECT o.wc_sid FROM #{#entityName} o "
            + "WHERE o.wc_sid in :wcSid "
            + "AND o.wc_trace_type in :types "
            + "and o.wc_trace_content like :lazyContent ")
    List<String> getWcSidByLikeContentAndWcSidAndType(
            @Param("wcSid") List<String> wcSid,
            @Param("types") List<WCTraceType> types,
            @Param("lazyContent") String lazyContent);
}
