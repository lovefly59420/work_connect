/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.common;

import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * JDBC SQL工具
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class SQLUtil implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1823681867438142427L;

    /**
     * 轉換成字串
     *
     * @param obj
     * @return
     */
    public String transToString(Object obj) {
        if (obj == null) {
            return "";
        }
        try {
            return String.valueOf(obj);
        } catch (Exception e) {
            log.warn("transToString ERROR", e);
        }
        return "";
    }

    public Integer transToInteger(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return Integer.valueOf(transToString(obj));
        } catch (Exception e) {
            log.warn("transToInteger ERROR", e);
        }
        return null;
    }

    /**
     * 轉換成日期
     *
     * @param obj
     * @return
     */
    public Date transToDate(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return (Date) obj;
        } catch (Exception e) {
            log.warn("transToDate ERROR", e);
        }
        return null;
    }
}
