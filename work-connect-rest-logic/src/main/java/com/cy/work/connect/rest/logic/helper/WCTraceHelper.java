/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.rest.logic.common.SimpleDateFormatEnum;
import com.cy.work.connect.rest.logic.common.ToolsDate;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.WCTraceType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
public class WCTraceHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6682094301302315166L;

    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WkUserCache userManager;

    public WCTrace getTraceReqRollback(
        String wcSid,
        String wcNo,
        String rollBackReason,
        Integer doUserSid,
        Integer rollBackToUserSid) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        wrt.setWc_trace_type(WCTraceType.REQROLLBACK);
        StringBuilder sb = new StringBuilder();
        sb.append("退回人員 : ");
        sb.append(WkUserUtils.findNameBySid(doUserSid));
        sb.append(" \n");
        sb.append("退回原因 : ");
        sb.append(rollBackReason);
        sb.append(" \n");
        sb.append("退回至[" + WkUserUtils.findNameBySid(rollBackToUserSid) + "]");
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(sb.toString());
        wrt.setWc_trace_content_css(sb.toString());
        return wrt;
    }

    public WCTrace getTraceExecRollbackToReq(
        String wcSid,
        String wcNo,
        List<Integer> execDepSids,
        String rollBackReason,
        Integer rollBackToUserSid) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        wrt.setWc_trace_type(WCTraceType.EXECROLLBACK);
        StringBuilder sb = new StringBuilder();
        sb.append("執行單位 : ");
        execDepSids.forEach(
            item -> {
                sb.append("[" + WkOrgUtils.findNameBySid(item) + "]");
            });
        sb.append(" \n");
        sb.append("退回原因 : ");
        sb.append(rollBackReason);
        sb.append(" \n");
        sb.append("退回至申請單位[" + WkUserUtils.findNameBySid(rollBackToUserSid) + "]");
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(sb.toString());
        wrt.setWc_trace_content_css(sb.toString());
        return wrt;
    }

    public WCTrace getTraceExecRollbackToStop(
        String wcSid, String wcNo, List<Integer> execDepSids, String rollBackReason) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wcSid);
        wrt.setWc_no(wcNo);
        wrt.setWc_trace_type(WCTraceType.EXECROLLBACK);
        StringBuilder sb = new StringBuilder();
        sb.append("執行單位 : ");
        execDepSids.forEach(
            item -> {
                sb.append("[" + WkOrgUtils.findNameBySid(item) + "]");
            });
        sb.append(" \n");
        sb.append("退回原因 : ");
        sb.append(rollBackReason);
        sb.append(" \n");
        sb.append("退回單位終止");
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(sb.toString());
        wrt.setWc_trace_content_css(sb.toString());
        return wrt;
    }

    public WCTrace getEXECDEP_STATUS_CHANGE(
        Integer userSid, String wr_sid, String wr_no, Integer depSid) {
        User user = userManager.findBySid(userSid);
        Org dep = orgManager.findBySid(depSid);
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wr_sid);
        wrt.setWc_no(wr_no);
        wrt.setWc_trace_type(WCTraceType.EXECDEP_STATUS_CHANGE);
        String content =
            "["
                + user.getName()
                + "] 於 ["
                + ToolsDate.transDateToString(
                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date())
                + "]"
                + "進行加簽，故目前["
                + dep.getName()
                + "]的狀態變更為 : 待執行"
                + "";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_CLOSE(Integer userSid, String wr_sid, String wr_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wr_sid);
        wrt.setWc_no(wr_no);
        wrt.setWc_trace_type(WCTraceType.CLOSE);
        String content = "工作聯絡單已結案";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_EXEC_FINISH_STATUS(Integer userSid, String wr_sid, String wr_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wr_sid);
        wrt.setWc_no(wr_no);
        wrt.setWc_trace_type(WCTraceType.EXEC_FINISH_STATUS);
        String content = "工作聯絡單已執行完成";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    public WCTrace getWCTrace_CLOSE_STOP(Integer userSid, String wr_sid, String wr_no) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wr_sid);
        wrt.setWc_no(wr_no);
        wrt.setWc_trace_type(WCTraceType.CLOSE_STOP);
        String content = "工作聯絡單已結案(終止)";
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 建立作廢工作聯絡單追蹤物件
     *
     * @param wr_sid 工作聯絡單Sid
     * @param wr_no  工作聯絡單單號
     * @param reason 作廢理由
     * @return
     */
    public WCTrace getWCTrace_INVAILD(String wr_sid, String wr_no, String reason) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wr_sid);
        wrt.setWc_no(wr_no);
        wrt.setWc_trace_type(WCTraceType.INVAILD);
        wrt.setStatus(Activation.ACTIVE);
        String content = "作廢原因 :" + reason;
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }

    /**
     * 取得提交追蹤物件
     *
     * @param userSid 提交者Sid
     * @param wr_sid  工作聯絡單Sid
     * @param wr_no   工作聯絡單單號
     * @return
     */
    public WCTrace getWCTrace_MODIFY_APPROVED(
        Integer userSid, String wr_sid, String wr_no, boolean isFromReqSign) {
        WCTrace wrt = new WCTrace();
        wrt.setWc_sid(wr_sid);
        wrt.setWc_no(wr_no);
        String content = "";
        if (isFromReqSign) {
            wrt.setWc_trace_type(WCTraceType.MODIFY_APPRVOED);
            content = "需求成立";
        }
        wrt.setStatus(Activation.ACTIVE);
        wrt.setWc_trace_content(content);
        wrt.setWc_trace_content_css(content);
        return wrt;
    }
}
