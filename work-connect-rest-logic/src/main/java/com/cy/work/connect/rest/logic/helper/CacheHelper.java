/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper;

import com.cy.commons.util.FusionUrlServiceUtils;
import java.net.URL;
import java.net.URLConnection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class CacheHelper {

    // private String apUrl =
    // FusionUrlServiceUtils.getUrlByPropKey("work-connect.service.ap.rest.url");

    public void clearProcessCache(String processSid) {
        URL url = null;
        try {
            url = new URL(
                    FusionUrlServiceUtils.getUrlByPropKey("work-connect.service.ap.rest.url")
                            + "/monitor/clear_process_cache.xhtml?processSid="
                            + processSid);
            URLConnection conn = url.openConnection();
            conn.connect();
            conn.getInputStream().close();
        } catch (Exception e) {
            if(url!=null) {
                log.debug("url:[{}]", url.toString());
            }
            log.warn("清 web 快取失敗![{}]", e.getMessage(), e);
        }
    }
}
