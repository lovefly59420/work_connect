/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import com.cy.work.connect.repository.WCRollbackHistoryRepository;
import com.cy.work.connect.vo.WCRollbackHistory;
import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCRollbackHistorySservice implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -508030508828886210L;

    @Autowired
    private WCRollbackHistoryRepository wcRollbackHistoryRepository;

    public Integer getCountByWcSidAndUserSid(Integer userSid, String wcSid) {
        try {
            return wcRollbackHistoryRepository.findByWcSidAndUserSid(wcSid, userSid).size();
        } catch (Exception e) {
            log.warn("getByWcSidAndUserSid (userSid:{}, wcSid:{})", userSid, wcSid);
            log.warn("getByWcSidAndUserSid ERROR", e);
        }
        return 0;
    }

    public void insertRollBackHistory(Integer userSid, String wcSid) {
        try {
            WCRollbackHistory wcRollbackHistory = new WCRollbackHistory();
            wcRollbackHistory.setCreateDate(new Date());
            wcRollbackHistory.setUserSid(userSid);
            wcRollbackHistory.setWcSid(wcSid);
            wcRollbackHistoryRepository.save(wcRollbackHistory);
        } catch (Exception e) {
            log.warn("insertRollBackHistory ERROR", e);
        }
    }
}
