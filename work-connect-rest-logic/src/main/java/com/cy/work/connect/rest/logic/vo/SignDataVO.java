/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.vo;

import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.RestFlowType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class SignDataVO {

    @Getter
    private final String wc_sid;
    @Getter
    private final String wc_no;
    @Getter
    private final String bpm_instance_id;
    @Getter
    private final SignType signType;
    @Getter
    private final Date create_dt;
    @Getter
    private final Date update_dt;
    @Getter
    private final RestFlowType flowType;
    @Getter
    private final Date signDate;
    @Getter
    private final Integer wc_create_usr;
    @Getter
    private final Date wc_create_dt;
    @Getter
    private final WCStatus wcStatus;
    //    @Getter
    //    private List<ProcessTaskBase> pks;
    @Getter
    private final List<String> canSignSids;
    @Getter
    private final Integer compSid;
    @Getter
    private final BpmStatus bpmStatus;
    private final List<String> tgSids = Lists.newArrayList();
    @Getter
    private final List<Date> signDates = Lists.newArrayList();
    @Getter
    private String signTypeName;
    @Getter
    @Setter
    private String theme;
    /**
     * 類別資訊(第三層)
     */
    @Getter
    private String tagInfo = "";

    public SignDataVO(
        String wc_sid,
        String wc_no,
        String bpm_instance_id,
        SignType signType,
        Date create_dt,
        Date update_dt,
        String theme,
        RestFlowType flowType,
        Date signDate,
        Integer wc_create_usr,
        Date wc_create_dt,
        WCStatus wcStatus,
        List<String> canSignSids,
        Integer compSid,
        BpmStatus bpmStatus) {
        this.wc_sid = wc_sid;
        this.wc_no = wc_no;
        this.bpm_instance_id = bpm_instance_id;
        this.signType = signType;
        if (signType == null) {
            this.signTypeName = "(簽核類型比對異常)";
        } else if (SignType.SIGN.equals(signType)) {
            this.signTypeName = "";
        } else if (SignType.COUNTERSIGN.equals(signType)) {
            this.signTypeName = "加簽";
        }
        this.create_dt = create_dt;
        this.update_dt = update_dt;
        this.theme = theme;
        this.flowType = flowType;
        this.signDate = signDate;
        this.wc_create_usr = wc_create_usr;
        this.wc_create_dt = wc_create_dt;
        this.wcStatus = wcStatus;
        this.canSignSids = canSignSids;
        // this.pks = pks;
        this.compSid = compSid;
        this.bpmStatus = bpmStatus;
    }

    public void bulidExecTag(String tagSid, String name) {
        if (tgSids == null) {
            return;
        }
        if (tgSids.contains(tagSid)) {
            return;
        }
        tgSids.add(tagSid);
        if (!Strings.isNullOrEmpty(tagInfo)) {
            tagInfo += "、";
        }
        tagInfo += name;
    }
}
