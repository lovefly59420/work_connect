/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to.enums;

import lombok.Getter;

/**
 * 快取資料類型
 *
 * @author jimmy_chou
 */
public enum WkCommonCacheType {
    WORK_CONNECT_REST_ALL_TODO("REST-查詢所有待辦事項");

    @Getter
    private final String value;

    WkCommonCacheType(String value) {
        this.value = value;
    }
}
