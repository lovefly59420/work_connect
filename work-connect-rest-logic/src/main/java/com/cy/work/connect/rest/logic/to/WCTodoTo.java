/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to;

import com.cy.work.connect.rest.logic.to.enums.WCToDoType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * rest / clent 使用 - 待辦事項
 *
 * @author kasim
 */
@Data
@ToString
@EqualsAndHashCode(of = {"type"})
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class WCTodoTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7283282519838886952L;

    @JsonProperty("type")
    /** 類別 名稱 */
    private WCToDoType type;

    @JsonProperty("count")
    /** 數量 */
    private Integer count;
    //    web / rest /client 必須同步

    public WCTodoTo(WCToDoType type, Integer count) {
        this.type = type;
        this.count = count;
    }
}
