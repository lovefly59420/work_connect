/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.helper.todo;

import java.io.Serializable;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cy.work.connect.rest.logic.common.Constants;
import com.cy.work.connect.rest.logic.to.WCTodoTo;
import com.cy.work.connect.rest.logic.to.enums.WCToDoType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 待領單據清單 查詢邏輯
 *
 * @author kasim
 */
@Component
@Slf4j
public class WaitReadWorkListHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7264515369415315722L;

    @Autowired
    @Qualifier(Constants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    /**
     * 查詢 待閱讀 數量
     *
     * @param loginUserSid
     * @return
     */
    public WCTodoTo findByWaitReadList(Integer loginUserSid) {
        try {
            String sql = this.bulidSqlByWaitReadList(loginUserSid);
            int count = jdbc.queryForObject(sql,
                    Integer.class);
            if (count > 0) {
                return new WCTodoTo(WCToDoType.WAIT_READ_LIST, count);
            }
        } catch (Exception e) {
            log.warn("查詢 待閱讀 數量 ERROR!!", e);
        }
        return null;
    }

    /**
     * 建立主要 Sql
     *
     * @param loginUserSid
     * @return
     */
    private String bulidSqlByWaitReadList(Integer loginUserSid) {

        // JSON_CONTAINS(wc.read_record,'{"reader":"3383","read":"HASREAD"}')
        //String readRecordCondutionFormat = " JSON_CONTAINS(wc.read_record,'{\"reader\":\"%s\",\"read\":\"%s\"}') = 1 ";

        String statusContition = Lists.newArrayList(WCStatus.INVALID, WCStatus.NEW_INSTANCE, WCStatus.RECONSIDERATION).stream()
                .map(WCStatus::name)
                .collect(Collectors.joining("', '", "'", "'"));

        StringBuffer sql = new StringBuffer();
        sql.append("SELECT Count(wc.wc_sid) ");
        sql.append("FROM   wc_master wc ");
        sql.append("WHERE  wc.wc_status NOT IN ( " + statusContition + " ) ");
        sql.append("       AND ( EXISTS (SELECT 1 ");
        sql.append("                     FROM   wc_read_receipts rr ");
        sql.append("                     WHERE  rr.wc_sid = wc.wc_sid ");
        sql.append("                           and rr.reader = " + loginUserSid + " ");
        sql.append("                            AND rr.readreceipt = 'UNREAD') ");
        
        // 先不開啟查詢閱讀記錄
        // sql.append(" OR " + String.format(readRecordCondutionFormat, loginUserSid, WCReadStatus.UNREAD.name()) + " ");
        // sql.append(" OR " + String.format(readRecordCondutionFormat, loginUserSid, WCReadStatus.WAIT_READ.name()) + " ");
        sql.append("              )");

        return sql.toString();
    }
}
