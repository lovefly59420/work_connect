/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.vo;

import com.cy.bpm.rest.vo.client.ICreate;
import com.cy.bpm.rest.vo.common.ProcessType;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

/**
 * 建立 BPM 所需自定物件
 *
 * @author kasim
 */
public class BpmCreateTo implements ICreate {

    /**
     *
     */
    private static final long serialVersionUID = 2466072978129824089L;

    @Getter
    private final ProcessType type;
    @Getter
    private final String definition;
    @Getter
    private final String display;
    @Getter
    private final String description;
    @Getter
    private final String docId;
    @Getter
    private final String executorId;
    @Getter
    private final String executorAgentId;
    @Getter
    private final String executorRoleId;
    @Getter
    private final Map<String, Object> parameter;

    public BpmCreateTo(String docId, String executorId, String exeRoleId, WcBpmFlowType flowType) {
        this.definition = flowType.getDefinition();
        this.display = flowType.getDisplayDesc();
        this.description = flowType.getDisplayDesc();
        this.parameter = new HashMap<>();
        this.docId = docId;
        this.executorId = executorId;
        this.executorAgentId = executorId;
        this.executorRoleId = exeRoleId;
        this.type = ProcessType.SIGNFLOW;
    }
}
