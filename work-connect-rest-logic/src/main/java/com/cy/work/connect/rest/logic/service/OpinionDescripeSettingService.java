/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.repository.WCOpinionDescripeSettingRepository;
import com.cy.work.connect.rest.logic.common.SimpleDateFormatEnum;
import com.cy.work.connect.rest.logic.common.ToolsDate;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class OpinionDescripeSettingService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 154385189636802566L;

    @Autowired
    private WCOpinionDescripeSettingRepository opinionDescripeSettingRepository;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private BpmOrganizationClient organizationClient;

    public void create(
        WCOpinionDescripeSetting opinionDescripeSetting, Integer loginUserSid, String wcSid) {
        opinionDescripeSetting.setCreateUser(loginUserSid);
        opinionDescripeSetting.setCreateDate(new Date());
        opinionDescripeSetting.setWcSid(wcSid);
        opinionDescripeSetting = opinionDescripeSettingRepository.save(opinionDescripeSetting);
    }

    public void update(WCOpinionDescripeSetting opinionDescripeSetting, Integer loginUserSid) {
        opinionDescripeSetting.setUpdateUser(loginUserSid);
        opinionDescripeSetting.setUpdateDate(new Date());
        opinionDescripeSetting = opinionDescripeSettingRepository.save(opinionDescripeSetting);
    }

    public WCOpinionDescripeSetting getOpinionDescripeSetting(
        ManagerSingInfoType managerSingInfoType, String sourceSid, Integer opinion_usr) {
        List<WCOpinionDescripeSetting> results =
            opinionDescripeSettingRepository.getOpinionDescripeSetting(
                managerSingInfoType, sourceSid, opinion_usr);
        if (results == null || results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    public void saveRollbackReason(
        ManagerSingInfoType managerSingInfoType,
        String sourceSid,
        Integer opinion_usr,
        String rollbackReason,
        Integer rollbackUserSid,
        String wcSid) {
        try {
            WCOpinionDescripeSetting opinionDescripeSetting =
                getOpinionDescripeSetting(managerSingInfoType, sourceSid, opinion_usr);
            if (opinionDescripeSetting == null) {
                opinionDescripeSetting = new WCOpinionDescripeSetting();
                opinionDescripeSetting.setOpinion_usr(opinion_usr);
                opinionDescripeSetting.setSourceType(managerSingInfoType);
                opinionDescripeSetting.setSourceSid(sourceSid);
                opinionDescripeSetting.setStatus(Activation.ACTIVE);
            }
            User rollbackUser = userManager.findBySid(rollbackUserSid);
            StringBuilder sb = new StringBuilder();
            sb.append("退回者:");
            try {
                sb.append(organizationClient.findUserMajorRoleName(rollbackUser.getId()));
                sb.append("-");
            } catch (Exception ex) {
                log.warn("findUserMajorRoleName (rollbackUserSid:{})", rollbackUserSid);
                log.warn("findUserMajorRoleName ERROR", ex);
            }
            sb.append(rollbackUser.getName());
            sb.append("<br/>");
            sb.append("退回原因:" + rollbackReason);
            sb.append("<br/>");
            sb.append(
                "退回時間:"
                    + ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date()));
            opinionDescripeSetting.setRollBackReason(sb.toString());
            if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
                this.create(opinionDescripeSetting, opinion_usr, wcSid);
            } else {
                this.update(opinionDescripeSetting, opinion_usr);
            }

        } catch (Exception e) {
            log.warn(
                "saveInvailReason (managerSingInfoType:{}, sourceSid:{}, opinion_usr:{}, rollbackReason:{}, rollbackUserSid:{}, wcSid:{})",
                managerSingInfoType,
                sourceSid,
                opinion_usr,
                rollbackReason,
                rollbackUserSid,
                wcSid);
            log.warn("saveInvailReason ERROR", e);
        }
    }

    public void saveInvailReason(
        ManagerSingInfoType managerSingInfoType,
        String sourceSid,
        Integer opinion_usr,
        String invailReason,
        String wcSid) {
        try {
            WCOpinionDescripeSetting opinionDescripeSetting =
                getOpinionDescripeSetting(managerSingInfoType, sourceSid, opinion_usr);
            if (opinionDescripeSetting == null) {
                opinionDescripeSetting = new WCOpinionDescripeSetting();
                opinionDescripeSetting.setOpinion_usr(opinion_usr);
                opinionDescripeSetting.setSourceType(managerSingInfoType);
                opinionDescripeSetting.setSourceSid(sourceSid);
                opinionDescripeSetting.setStatus(Activation.ACTIVE);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("作廢原因:" + invailReason);
            sb.append("<br/>");
            sb.append(
                "作廢時間:"
                    + ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date()));
            opinionDescripeSetting.setInvaildReason(sb.toString());
            if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
                this.create(opinionDescripeSetting, opinion_usr, wcSid);
            } else {
                this.update(opinionDescripeSetting, opinion_usr);
            }
        } catch (Exception e) {
            log.warn(
                "saveInvailReason (managerSingInfoType:{}, sourceSid:{}, opinion_usr:{}, invailReason:{}, wcSid:{})",
                managerSingInfoType,
                sourceSid,
                opinion_usr,
                invailReason,
                wcSid);
            log.warn("saveInvailReason ERROR", e);
        }
    }

    /**
     * 更新終止原因
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @param invailReason        作廢原因
     * @param wcSid               主檔Sid
     */
    public void saveStopReason(
        ManagerSingInfoType managerSingInfoType,
        String sourceSid,
        Integer opinion_usr,
        String invailReason,
        String wcSid) {
        try {
            WCOpinionDescripeSetting opinionDescripeSetting =
                getOpinionDescripeSetting(managerSingInfoType, sourceSid, opinion_usr);
            if (opinionDescripeSetting == null) {
                opinionDescripeSetting = new WCOpinionDescripeSetting();
                opinionDescripeSetting.setOpinion_usr(opinion_usr);
                opinionDescripeSetting.setSourceType(managerSingInfoType);
                opinionDescripeSetting.setSourceSid(sourceSid);
                opinionDescripeSetting.setStatus(Activation.ACTIVE);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("終止原因:" + invailReason);
            sb.append("<br/>");
            sb.append(
                "終止時間:"
                    + ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date()));
            opinionDescripeSetting.setInvaildReason(sb.toString());
            if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
                this.create(opinionDescripeSetting, opinion_usr, wcSid);
            } else {
                this.update(opinionDescripeSetting, opinion_usr);
            }
        } catch (Exception e) {
            log.warn(
                "saveInvailReason (managerSingInfoType:{}, sourceSid:{}, opinion_usr:{}, invailReason:{}, wcSid:{})",
                managerSingInfoType,
                sourceSid,
                opinion_usr,
                invailReason,
                wcSid);
            log.warn("saveInvailReason ERROR", e);
        }
    }
}
