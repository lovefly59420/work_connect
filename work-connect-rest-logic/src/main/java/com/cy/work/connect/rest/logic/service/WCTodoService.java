/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import com.antkorwin.xsync.XSync;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.WaitAssignLogic;
import com.cy.work.connect.logic.helper.WaitReceiveLogic;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.rest.logic.helper.todo.ProcessListHelper;
import com.cy.work.connect.rest.logic.helper.todo.WaitCloseListHelper;
import com.cy.work.connect.rest.logic.helper.todo.WaitReadWorkListHelper;
import com.cy.work.connect.rest.logic.to.WCTodoTo;
import com.cy.work.connect.rest.logic.to.enums.WCToDoType;
import com.cy.work.connect.rest.logic.to.enums.WkCommonCacheType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 待辦事項
 *
 * @author kasim
 */
@Component
@Slf4j
public class WCTodoService implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6406864660716483591L;
    /**
     * 快取逾期時間 : 30秒
     */
    private final long overdueMillisecond = 30 * 1000;
    @Autowired
    private WaitReceiveLogic waitReceiveLogic;
    @Autowired
    private ProcessListHelper processListHelper;
    @Autowired
    private WaitReadWorkListHelper waitReadWorkListHelper;
    @Autowired
    private WaitCloseListHelper waitCloseListHelper;
    @Autowired
    private WorkParamManager workParamManager;
    @Autowired
    private transient WkCommonCache wkCommonCache;
    @Autowired
    private XSync<Integer> xSync;

    /**
     * 取回待辦事項 (synchronized by userSid)
     *
     * @param depSid
     * @param userSid
     * @return
     */
    public List<WCTodoTo> syncAllToDo(Integer userSid) {
        xSync.execute(
                userSid,
                () -> {
                    this.findAllToDo(userSid);
                });

        return this.wkCommonCache.getCache(
                WkCommonCacheType.WORK_CONNECT_REST_ALL_TODO.name(),
                Lists.newArrayList(userSid + ""),
                overdueMillisecond);
    }

    /**
     * 取回待辦事項
     *
     * @param depSid
     * @param userSid
     * @return
     */
    public List<WCTodoTo> findAllToDo(Integer userSid) {
        List<WCTodoTo> results = Lists.newArrayList();
        List<String> queueLog = Lists.newArrayList();

        Long startTime = System.currentTimeMillis();
        Long subStartTime = System.currentTimeMillis();

        // ====================================
        // 由快取中取回
        // ====================================
        List<String> keys = Lists.newArrayList(userSid + "");
        List<WCTodoTo> cacheData = this.wkCommonCache.getCache(
                WkCommonCacheType.WORK_CONNECT_REST_ALL_TODO.name(), keys, overdueMillisecond);
        if (cacheData != null) {
            // log.debug("findAllToDo[" + userSid + "] return by cache!");
            return cacheData;
        }

        // ====================================
        // 查詢 待派工 數量
        // ====================================
        subStartTime = System.currentTimeMillis();
        String procName = "";
        try {
            procName = "待派工 數量";
            long count = WaitAssignLogic.getInstance().queryCanAssignCount(userSid);

            if (count > 0) {
                results.add(new WCTodoTo(WCToDoType.WAIT_WORK_LIST, Integer.parseInt(count + "")));
            }
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        queueLog.add(WkCommonUtils.prepareCostMessage(subStartTime, procName));

        // ====================================
        // 查詢 待領單 數量
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "待領單 數量";
            Integer waitReceiveCount = this.waitReceiveLogic.queryWaitReceiveCount(userSid);
            if (waitReceiveCount > 0) {
                results.add(new WCTodoTo(WCToDoType.WAIT_RECEIVE_WORK_LIST, waitReceiveCount));
            }
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        queueLog.add(WkCommonUtils.prepareCostMessage(subStartTime, procName));

        // ====================================
        // 查詢 待處理 數量
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "待處理 數量";
            WCTodoTo to3 = processListHelper.findByProcessList(userSid);
            if (to3 != null) {
                results.add(to3);
            }
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        queueLog.add(WkCommonUtils.prepareCostMessage(subStartTime, procName));

        // ====================================
        // 查詢 待閱讀 數量
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "待閱讀 數量";
            WCTodoTo to4 = waitReadWorkListHelper.findByWaitReadList(userSid);
            if (to4 != null) {
                results.add(to4);
            }
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        queueLog.add(WkCommonUtils.prepareCostMessage(subStartTime, procName));

        // ====================================
        // 查詢 待結案 數量
        // ====================================
        subStartTime = System.currentTimeMillis();
        try {
            procName = "待結案 數量";
            WCTodoTo to5 = waitCloseListHelper.findByWaitCloseList(userSid);
            if (to5 != null) {
                results.add(to5);
            }
        } catch (Exception e) {
            log.error("查詢 [待辦事項-" + procName + "] 失敗!", e);
        }

        queueLog.add(WkCommonUtils.prepareCostMessage(subStartTime, procName));

        // ====================================
        // 記錄快取
        // ====================================
        this.wkCommonCache.putCache(WkCommonCacheType.WORK_CONNECT_REST_ALL_TODO.name(), keys, results);

        // ====================================
        // 全部處理時間 (大於等於後臺設定responsetimeBound的耗時才輸出LOG紀錄)
        // ====================================
        String execueTime = WkCommonUtils.calcCostSec(startTime);
        Double responsetimeBound = workParamManager.findRestResponseTimeBound();
        String responseTime = this.calcSec(responsetimeBound);
        if (WkStringUtils.notEmpty(execueTime) && WkStringUtils.notEmpty(responseTime)) {
            if ((new BigDecimal(responseTime)).compareTo(new BigDecimal(execueTime)) <= 0) {
                log.warn("登入者[{}] "
                        + WkCommonUtils.prepareCostMessage(startTime, "findAllToDo[" + userSid + "]"),
                        WkUserUtils.findNameBySid(userSid));
                for (String each : queueLog) {
                    log.warn("登入者[{}] UserSid[{}] " + each, WkUserUtils.findNameBySid(userSid), userSid);
                }
            }
        }

        return results;
    }

    /**
     * 計算回應秒數
     *
     * @param responseTime
     * @return
     */
    public String calcSec(Double responseTime) {
        responseTime = responseTime / 1000;
        return responseTime + "";
    }
}
