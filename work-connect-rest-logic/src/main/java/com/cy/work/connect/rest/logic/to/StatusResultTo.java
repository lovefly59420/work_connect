/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.to;

import java.io.Serializable;
import lombok.Getter;

/**
 * @author kasim
 */
public class StatusResultTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8434913206457900666L;

    @Getter
    private final Boolean result;

    @Getter
    private final String message;

    public StatusResultTo(String message) {
        this(Boolean.TRUE, message);
    }

    public StatusResultTo(Boolean result, String message) {
        this.result = result;
        this.message = message;
    }
}
