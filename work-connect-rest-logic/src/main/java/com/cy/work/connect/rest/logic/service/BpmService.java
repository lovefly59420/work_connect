/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.rest.logic.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.client.ProcessClient;
import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.common.ProcessType;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.rest.logic.vo.BpmCreateTo;
import com.cy.work.connect.rest.logic.vo.BpmSignTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class BpmService implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7258129511037161591L;

    private static BpmService instance;
    @Autowired
    private BpmOrganizationClient organizationClient;
    @Autowired
    private ProcessClient processClient;
    @Autowired
    private TaskClient taskClient;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;

    public static BpmService getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        BpmService.instance = this;
    }

    /**
     * 建立流程
     *
     * @param masterNo
     * @param userSid
     * @param companySid
     * @param flowType
     * @return
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public String createBpmFlow(
            String masterNo, Integer userSid, Integer companySid, WcBpmFlowType flowType)
            throws ProcessRestException {
        return this.createBpmFlow(
                masterNo,
                userManager.findBySid(userSid).getId(),
                orgManager.findBySid(companySid).getId(),
                flowType);
    }

    /**
     * 建立流程
     *
     * @param masterNo
     * @param userId
     * @param companyId
     * @param flowType
     * @return
     * @throws ProcessRestException
     */
    private String createBpmFlow(
            String masterNo, String userId, String companyId, WcBpmFlowType flowType)
            throws ProcessRestException {
        return processClient.create(
                new BpmCreateTo(masterNo, userId, this.findUserRoleByCompany(userId, companyId),
                        flowType));
    }

    /**
     * 尋找使用者在指定公司角色
     *
     * @param userId
     * @param companyId
     * @return
     * @throws ProcessRestException
     */
    private String findUserRoleByCompany(String userId, String companyId)
            throws ProcessRestException {
        return organizationClient.findUserRoleByCompany(userId, companyId);
    }

    /**
     * 尋找流程實例主要執行任務<br>
     * 可能會回傳NULL值
     *
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public ProcessTask findMajorTask(String bpmId) throws ProcessRestException {
        return taskClient.findMajorTask(bpmId);
    }

    /**
     * 尋找成員全部待簽核任務 - by 流程定義
     *
     * @param userId     成員ID
     * @param definition 流程定義名稱 (例:Journal_New)
     * @return
     * @throws ProcessRestException
     */
    public List<ProcessTask> findTaskByUserIdAndDefinition(String userId, String definition)
            throws ProcessRestException {
        List<ProcessTask> result = Lists.newArrayList();
        result.addAll(taskClient.findTaskByUserAndDefinition(userId, definition));
        return result;
    }

    /**
     * 尋找成員全部待簽核任務 - by 流程定義清單
     *
     * @param userId      成員ID
     * @param definitions 流程定義清單
     * @return
     * @throws ProcessRestException
     */
    public List<ProcessTask> findTaskByUserIdAndDefinitions(String userId, List<String> definitions)
            throws ProcessRestException {
        List<ProcessTask> result = Lists.newArrayList();
        result.addAll(taskClient.findTaskByUserAndDefinitions(userId, definitions));
        return result;
    }

    /**
     * 執行簽核
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void sign(String userId, String bpmId) throws ProcessRestException {
        processClient.sign(new BpmSignTo(userId, bpmId));
    }

    /**
     * 執行復原
     *
     * @param userId
     * @param recoveryTask
     * @throws ProcessRestException
     */
    public void recovery(String userId, ProcessTaskHistory recoveryTask)
            throws ProcessRestException {
        processClient.recovery(userId, ProcessType.SIGNFLOW, recoveryTask);
    }

    /**
     * 執行退回
     *
     * @param userId
     * @param rollBackTask
     * @param comment
     * @throws ProcessRestException
     */
    public void rollBack(String userId, ProcessTaskHistory rollBackTask, String comment)
            throws ProcessRestException {
        processClient.rollback(userId, ProcessType.SIGNFLOW, rollBackTask, comment);
    }

    /**
     * 執行作廢
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void invalid(String userId, String bpmId) throws ProcessRestException {
        processClient.terminate(bpmId, userId);
    }

    /**
     * 建立模擬圖
     *
     * @param userId 查詢人員
     * @param bpmId
     * @return
     */
    public List<ProcessTaskBase> findSimulationChart(String userId, String bpmId) {
        try {
            if (Strings.isNullOrEmpty(bpmId)) {
                return Lists.newArrayList();
            }
            return taskClient.findSimulationChart(bpmId, userId);
        } catch (Exception e) {
            log.warn("findSimulationChart (userId:{}, bpmId:{})", userId, bpmId);
            log.warn("findSimulationChart ERROR", e);
        }
        return Lists.newArrayList();
    }
}
