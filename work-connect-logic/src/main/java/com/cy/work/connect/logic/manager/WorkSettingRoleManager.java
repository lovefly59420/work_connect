/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.vo.Role;
import com.cy.commons.vo.special.UserWithRoles;
import com.cy.work.common.cache.WkRoleCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao Role物件管理器
 */
@Component
public class WorkSettingRoleManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8414598919927598547L;
    /**
     * Role物件Cache管理器
     */
    @Autowired
    private WkRoleCache wkRoleCache;

    @Autowired
    private WkUserWithRolesCache wkUserWithRolesCache;

    public List<Role> findRolesByUserSid(Integer userSid) {
        UserWithRoles userWithRole = wkUserWithRolesCache.findBySid(userSid);
        if (userWithRole == null || userWithRole.getRoles() == null) {
            return Lists.newArrayList();
        }
        List<Role> roles = Lists.newArrayList();
        userWithRole
            .getRoles()
            .forEach(
                item -> {
                    roles.add(wkRoleCache.findBySid(item.getSid()));
                });
        return roles;
    }
}
