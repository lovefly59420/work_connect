package com.cy.work.connect.logic.manager;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.converter.ReplyRecordConverter;
import com.cy.work.connect.vo.converter.to.ReplyRecord;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 計算未閱讀的回覆邏輯
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class CountUnreadReplyLogic implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6643292108598367015L;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbcTemplate;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private transient WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private transient WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private transient WCExecDepManager wcExecDepManager;
    @Autowired
    private transient WCMasterManager wcMasterManager;
    @Autowired
    private transient BpmManager bpmManager;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 未閱讀數量 + 1
     * 
     * @param wcSid
     * @param loginUserID
     */
    public void processPlusOne(String wcSid, Integer loginUserSid) {

        // ====================================
        // 取得登入者資料
        // ====================================
        User loginUser = WkUserCache.getInstance().findBySid(loginUserSid);
        if (loginUser == null) {
            log.warn("傳入使用者找不到!userSid[{}]", loginUserSid);
        }

        // ====================================
        // 計算相關人員 (需要+1筆的)
        // ====================================
        Set<Integer> relationUserSids = this.prepareRelationUserSids(wcSid, loginUser.getId());

        // ====================================
        // 撈回目前 DB 的未讀數量資料
        // ====================================
        List<ReplyRecordTo> replyRecordTos = this.findUnReadRecord(wcSid);

        // ====================================
        // 更新資料
        // ====================================
        Set<Integer> exsitsUserSids = Sets.newHashSet();

        // 1.現在有有未讀數量的都+1
        for (ReplyRecordTo replyRecordTo : replyRecordTos) {

            // 防呆
            if (!WkStringUtils.isNumber(replyRecordTo.getUser())
                    || !WkStringUtils.isNumber(replyRecordTo.getUnReadCount())) {
                try {
                    log.error("資料錯誤:\r\nReplyRecordTo:【{}】", WkJsonUtils.getInstance().toJson(replyRecordTo));
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
                continue;
            }

            // 未讀筆數+1
            Integer newUnReadCount = Integer.parseInt(replyRecordTo.getUnReadCount()) + 1;
            replyRecordTo.setUnReadCount(newUnReadCount + "");

            // 收集已存在的user Sid
            if (WkStringUtils.isNumber(replyRecordTo.getUser())) {
                exsitsUserSids.add(Integer.parseInt(replyRecordTo.getUser()));
            }
        }

        // 2.不存在列表中的相關人員，新增一筆
        for (Integer relationUserSid : relationUserSids) {
            if (!exsitsUserSids.contains(relationUserSid)) {
                ReplyRecordTo currUserTo = new ReplyRecordTo();
                currUserTo.setUser(relationUserSid + "");
                currUserTo.setUnReadCount("1");

                replyRecordTos.add(currUserTo);
            }
        }

        // ====================================
        // 移除登入者本身 的待閱讀數量
        // 1.操作者視為已全部閱讀
        // 2.不一定存在
        // ====================================
        replyRecordTos = replyRecordTos.stream()
                .filter(to -> !WkCommonUtils.compareByStr(to.getUser(), loginUserSid))
                .collect(Collectors.toList());

        // ====================================
        // UPADTE
        // ====================================
        this.updateWCReplyNotRead(wcSid, replyRecordTos);
    }

    /**
     * 清空使用者未閱讀筆數
     * @param wcSid
     * @param loginUserSid
     */
    public void processClearUnreadCount(String wcSid, Integer loginUserSid) {

        // ====================================
        // 查詢已存在資料
        // ====================================
        // 兜組 SQL
        StringBuffer querySql = new StringBuffer();
        querySql.append("SELECT reply_not_read ");
        querySql.append("FROM   wc_master ");
        querySql.append("WHERE  wc_sid = ? ");

        // 查詢
        String replyNotReadJsonStr = this.jdbcTemplate.queryForObject(querySql.toString(), String.class, wcSid);

        // 資料為空，無需處理
        if (WkStringUtils.isEmpty(replyNotReadJsonStr)) {
            return;
        }

        // ====================================
        // 轉為資料物件
        // ====================================
        // 轉換
        ReplyRecord replyRecord = new ReplyRecordConverter().convertToEntityAttribute(replyNotReadJsonStr);
        // 無資料時不用處理
        if (replyRecord == null || WkStringUtils.isEmpty(replyRecord.getRRecord())) {
            return;
        }

        // ====================================
        // 清除登入者未閱讀筆數
        // ====================================
        // 僅保留除登入者以外的資料
        List<ReplyRecordTo> rRecord = replyRecord.getRRecord().stream()
                .filter(vo -> !WkCommonUtils.compareByStr(vo.getUser(), loginUserSid))
                .collect(Collectors.toList());

        replyRecord.setRRecord(rRecord);

        // ====================================
        // update
        // ====================================
        this.updateWCReplyNotRead(wcSid, rRecord);

    }

    /**
     * 變更使用者未閱讀的回覆筆數
     *
     * @param sid            工作聯絡單Sid
     * @param replyRecordTos 使用者未閱讀的回覆筆數
     */
    public void updateWCReplyNotRead(String wcSid, List<ReplyRecordTo> replyRecordTos) {

        // 將資料轉為 JSON
        ReplyRecord replyRecord = new ReplyRecord();
        replyRecord.setRRecord(replyRecordTos);
        String replyNotReadJsonStr = new ReplyRecordConverter().convertToDatabaseColumn(
                replyRecord);

        // 兜組 SQL
        StringBuffer sql = new StringBuffer();
        sql.append("UPDATE wc_master ");
        sql.append("SET    reply_not_read = ? ");
        sql.append("WHERE  wc_sid = ? ");

        // 準備參數
        List<Object> argsList = Lists.newArrayList();
        argsList.add(replyNotReadJsonStr);
        argsList.add(wcSid);

        // 執行
        this.jdbcTemplate.update(sql.toString(), argsList.toArray());
    }

    /**
     * @param wcSid
     * @return
     */
    private List<ReplyRecordTo> findUnReadRecord(String wcSid) {

        // 兜組 SQL
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT reply_not_read ");
        sql.append("FROM   wc_master ");
        sql.append("WHERE  wc_sid = ? ");

        // 查詢
        String replyNotReadJsonDataStr = (String) jdbcTemplate.queryForObject(
                sql.toString(), String.class, wcSid);

        // 轉回物件
        ReplyRecord replyRecord = new ReplyRecordConverter().convertToEntityAttribute(
                replyNotReadJsonDataStr);

        if (replyRecord == null || WkStringUtils.isEmpty(replyRecord)) {
            return Lists.newArrayList();
        }

        return replyRecord.getRRecord();
    }

    /**
     * @param wcSid
     * @param loginUserID
     * @return
     */
    private Set<Integer> prepareRelationUserSids(String wcSid, String loginUserID) {

        Set<Integer> relationUserSids = Sets.newHashSet();

        // ====================================
        // 查詢主檔狀態
        // ====================================
        WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
        if (wcMaster == null) {
            log.error("主檔找不到!wcSid:[{}]" + wcSid);
            return Sets.newHashSet();
        }

        // ====================================
        // 申請單位簽核節點人員
        // 1.簽完才收的到
        // ====================================
        // 查詢簽核流程
        List<SingleManagerSignInfo> applyUnitSignInfos = this.wcManagerSignInfoManager.getFlowSignInfos(wcSid);
        // 提取簽核人員
        Set<Integer> applyUnitSignUserSids = this.getSignedUserSids(applyUnitSignInfos, loginUserID);
        // 加入總表
        if (WkStringUtils.notEmpty(applyUnitSignUserSids)) {
            relationUserSids.addAll(applyUnitSignUserSids);
        }

        // ====================================
        // 申請單位會簽人員
        // 1.簽完才收的到
        // ====================================
        // 查詢流程
        List<SingleManagerSignInfo> applyUnitCounterSignInfos = wcManagerSignInfoManager.getCounterSignInfos(wcSid);
        // 提取簽核人員
        Set<Integer> applyUnitCounterSignUserSids = this.getSignedUserSids(applyUnitCounterSignInfos, loginUserID);
        // 加入總表
        if (WkStringUtils.notEmpty(applyUnitCounterSignUserSids)) {
            relationUserSids.addAll(applyUnitCounterSignUserSids);
        }

        // ====================================
        // 執行單位簽核人員
        // 1.簽完才收的到
        // 2.案件為執行中才收的到
        // ====================================
        if (this.isExecSideNeedReciveByWcStatus(wcMaster.getWc_status())) {
            // 查詢執行單位簽核流程
            List<SingleManagerSignInfo> execDepSignInfos = this.wcExecManagerSignInfoManager.getFlowSignInfos(wcSid);
            // 提取簽核人員
            Set<Integer> execDepSignUserSids = this.getSignedUserSids(execDepSignInfos, loginUserID);
            // 加入總表
            if (WkStringUtils.notEmpty(execDepSignUserSids)) {
                relationUserSids.addAll(execDepSignUserSids);
            }
        }

        // ====================================
        // 執行單位會簽人員
        // 1.簽完才收的到
        // 2.結案後收不到
        // ====================================
        if (this.isExecSideNeedReciveByWcStatus(wcMaster.getWc_status())) {
            // 查詢執行單位簽核流程
            List<SingleManagerSignInfo> execDepCounterSignInfos = this.wcExecManagerSignInfoManager.getCounterSignInfos(wcSid);
            // 提取簽核人員
            Set<Integer> execDepCounterSignUserSids = this.getSignedUserSids(execDepCounterSignInfos, loginUserID);
            // 加入總表
            if (WkStringUtils.notEmpty(execDepCounterSignUserSids)) {
                relationUserSids.addAll(execDepCounterSignUserSids);
            }
        }

        // ====================================
        // 執行單位執行人員
        // 1.人員產生後才收的到
        // 2.結案後收不到
        // ====================================
        if (this.isExecSideNeedReciveByWcStatus(wcMaster.getWc_status())) {
            // 查詢單據所有執行單位檔
            List<WCExceDep> wcExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);

            for (WCExceDep wcExceDep : wcExceDeps) {
                // 無效資料
                if (!Activation.ACTIVE.equals(wcExceDep.getStatus())) {
                    continue;
                }

                // 跳過
                // 1.未產生執行人員
                // 2.執行人員已停用
                if (wcExceDep.getExecUserSid() == null
                        || !WkUserUtils.isActive(wcExceDep.getExecUserSid())) {
                    continue;
                }

                // 加入執行人員
                relationUserSids.add(wcExceDep.getExecUserSid());
            }
        }

        return relationUserSids;
    }

    /**
     * 提取已簽核人員
     * 
     * @param signInfos   簽核檔
     * @param loginUserID 登入者 ID
     * @return 流程簽核人員 list
     */
    private Set<Integer> getSignedUserSids(List<SingleManagerSignInfo> signInfos, String loginUserID) {
        Set<Integer> signUserSids = Sets.newHashSet();

        for (SingleManagerSignInfo signInfo : signInfos) {

            try {
                List<ProcessTaskBase> processTasks = this.bpmManager.findSimulationChart(
                        loginUserID,
                        signInfo.getInstanceID());

                for (ProcessTaskBase processTask : processTasks) {

                    // 未簽核時跳過
                    if (!this.isSinged(processTask)) {
                        continue;
                    }
                    // 取得簽核人員
                    User currUser = WkUserCache.getInstance().findById(processTask.getUserID());
                    if (currUser == null) {
                        log.warn("UserID對應不到WERP資料 USER_ID:[{}]", processTask.getUserID());
                        continue;
                    }
                    signUserSids.add(currUser.getSid());

                }
            } catch (Exception e) {
                log.error("查詢流程簽核人員失敗!" + e.getMessage(), e);
            }
        }

        return signUserSids;

    }

    /**
     * 節點是否已簽核
     * 
     * @param processTask ProcessTaskBase 流程基礎物件
     * @return 是/否
     */
    private boolean isSinged(ProcessTaskBase processTask) {
        return processTask instanceof ProcessTaskHistory;
    }

    /**
     * 依據主單狀態，判斷執行方相關人員是否該收到新增回覆訊息
     * 
     * @return
     */
    private boolean isExecSideNeedReciveByWcStatus(WCStatus wcStatus) {
        return WCStatus.APPROVED.equals(wcStatus)// 申請方簽完，進入執行單位流程，但還沒有任一單位進入執行狀態 (待簽核、分派、領單)
                || WCStatus.EXEC.equals(wcStatus)// 任一單位已開始執行
                || WCStatus.EXEC_FINISH.equals(wcStatus); // 全部單位都已經直完成
    }

}
