/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.connect.logic.cache.WCBaseDepAccessInfoCache;
import com.cy.work.connect.repository.WCBaseDepAccessInfoRepository;
import com.cy.work.connect.vo.WCBaseDepAccessInfo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位可閱權限設定
 *
 * @author brain0925_liao
 */
@Component
public class WCBaseDepAccessInfoManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 270600144486752792L;
    /**
     * 基礎單位可閱權限設定Cache
     */
    @Autowired
    private WCBaseDepAccessInfoCache wcBaseDepAccessInfoCache;
    /**
     * WCBaseDepAccessInfoRepository
     */
    @Autowired
    private WCBaseDepAccessInfoRepository wcBaseDepAccessInfoRepository;
    /**
     * WorkSettingOrgManager
     */
    @Autowired
    private WkOrgCache orgManager;

    /**
     * 取得基礎單位可閱權限設定
     *
     * @param depSid 部門Sid
     * @return
     */
    public WCBaseDepAccessInfo getByloginDepSid(Integer depSid) {
        return wcBaseDepAccessInfoCache.getByDepSid(depSid);
    }

    /**
     * 取得基礎單位限制部門
     *
     * @param wcBaseDepAccessInfoDepSid 部門Sid
     * @return
     */
    public List<Org> getLimitBaseAccessViewDep(Integer wcBaseDepAccessInfoDepSid) {
        WCBaseDepAccessInfo wcBaseDepAccessInfo = getByloginDepSid(wcBaseDepAccessInfoDepSid);
        if (wcBaseDepAccessInfo == null
            || wcBaseDepAccessInfo.getLimitBaseAccessViewDep() == null
            || wcBaseDepAccessInfo.getLimitBaseAccessViewDep().getDepTos() == null) {
            return null;
        }
        List<Org> limitBaseAccessViewDeps = Lists.newArrayList();
        wcBaseDepAccessInfo
            .getLimitBaseAccessViewDep()
            .getDepTos()
            .forEach(
                item -> {
                    limitBaseAccessViewDeps.add(orgManager.findBySid(item.getSid()));
                });
        return limitBaseAccessViewDeps;
    }

    /**
     * 取得基礎單位增加部門
     *
     * @param wcBaseDepAccessInfoDepSid 部門Sid
     * @return
     */
    public List<Org> getOtherBaseAccessViewDep(Integer wcBaseDepAccessInfoDepSid) {
        WCBaseDepAccessInfo wcBaseDepAccessInfo = getByloginDepSid(wcBaseDepAccessInfoDepSid);
        if (wcBaseDepAccessInfo == null
            || wcBaseDepAccessInfo.getOtherBaseAccessViewDep() == null
            || wcBaseDepAccessInfo.getOtherBaseAccessViewDep().getDepTos() == null) {
            return Lists.newArrayList();
        }
        List<Org> otherBaseAccessViewDeps = Lists.newArrayList();
        wcBaseDepAccessInfo
            .getOtherBaseAccessViewDep()
            .getDepTos()
            .forEach(
                item -> {
                    otherBaseAccessViewDeps.add(orgManager.findBySid(item.getSid()));
                });
        return otherBaseAccessViewDeps;
    }

    /**
     * 建立基礎單位可閱權限設定
     *
     * @param wcBaseDepAccessInfo 基礎單位可閱權限設定物件
     * @param loginUserSid        建立者Sid
     */
    public void createWCBaseDepAccessInfo(
        WCBaseDepAccessInfo wcBaseDepAccessInfo, Integer loginUserSid) {
        wcBaseDepAccessInfo.setCreate_dt(new Date());
        wcBaseDepAccessInfo.setCreate_usr_sid(loginUserSid);
        wcBaseDepAccessInfo = wcBaseDepAccessInfoRepository.save(wcBaseDepAccessInfo);
        wcBaseDepAccessInfoCache.updateCacheBySid(wcBaseDepAccessInfo);
    }

    /**
     * 更新基礎單位可閱權限設定
     *
     * @param wcBaseDepAccessInfo 基礎單位可閱權限設定物件
     * @param loginUserSid        更新者Sid
     */
    public void updateWCBaseDepAccessInfo(
        WCBaseDepAccessInfo wcBaseDepAccessInfo, Integer loginUserSid) {
        wcBaseDepAccessInfo.setUpdate_dt(new Date());
        wcBaseDepAccessInfo.setUpdate_usr_sid(loginUserSid);
        wcBaseDepAccessInfo = wcBaseDepAccessInfoRepository.save(wcBaseDepAccessInfo);
        wcBaseDepAccessInfoCache.updateCacheBySid(wcBaseDepAccessInfo);
    }

    /**
     * 取得單位可閱權限增加(聯集)已設定單位
     *
     * @return
     */
    public List<Integer> getSettingOtherBaseOrgSids() {
        return wcBaseDepAccessInfoCache.getSettingOtherBaseOrgSids();
    }
}
