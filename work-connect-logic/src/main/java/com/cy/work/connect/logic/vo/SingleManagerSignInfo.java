/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;
import java.io.Serializable;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class SingleManagerSignInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 671689309259054076L;

    @Getter
    private final String signInfoSid;
    @Getter
    private final String instanceID;
    @Getter
    private final Integer cereateUserSid;
    @Getter
    private final Integer depSid;
    @Getter
    private final Integer userSid;
    @Getter
    private final SignType signType;
    @Getter
    private final BpmStatus status;
    @Getter
    private final String defaultSignName;
    @Getter
    private final Integer groupSeq;

    public SingleManagerSignInfo(
            String signInfoSid,
            String instanceID,
            Integer userSid,
            Integer depSid,
            SignType signType,
            BpmStatus status,
            String defaultSignName,
            Integer cereateUserSid,
            Integer groupSeq) {
        this.signInfoSid = signInfoSid;
        this.instanceID = instanceID;
        this.userSid = userSid;
        this.depSid = depSid;
        this.signType = signType;
        this.status = status;
        this.defaultSignName = defaultSignName;
        this.cereateUserSid = cereateUserSid;
        this.groupSeq = groupSeq;
    }

    public SingleManagerSignInfo(WCManagerSignInfo wcManagerSignInfo) {
        this.signInfoSid = wcManagerSignInfo.getSid();
        this.instanceID = wcManagerSignInfo.getBpmId();
        this.userSid = wcManagerSignInfo.getUser_sid();
        this.depSid = wcManagerSignInfo.getDep_sid();
        this.signType = wcManagerSignInfo.getSignType();
        this.status = wcManagerSignInfo.getStatus();
        this.defaultSignName = wcManagerSignInfo.getBpmDefaultSignedName();
        this.cereateUserSid = null;
        this.groupSeq = wcManagerSignInfo.getGroupSeq();
    }

    public SingleManagerSignInfo(WCExecManagerSignInfo wcExecManagerSignInfo) {
        this.signInfoSid = wcExecManagerSignInfo.getSid();
        this.instanceID = wcExecManagerSignInfo.getBpmId();
        this.userSid = wcExecManagerSignInfo.getUser_sid();
        this.depSid = wcExecManagerSignInfo.getDep_sid();
        this.signType = wcExecManagerSignInfo.getSignType();
        this.status = wcExecManagerSignInfo.getStatus();
        this.defaultSignName = wcExecManagerSignInfo.getBpmDefaultSignedName();
        this.cereateUserSid = wcExecManagerSignInfo.getCreate_user_sid();
        this.groupSeq = wcExecManagerSignInfo.getGroupSeq();
    }

}
