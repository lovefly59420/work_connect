/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.cache.WCOpinionDescripeSettingCache;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.repository.WCOpinionDescripeSettingRepository;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.google.common.base.Strings;

import lombok.extern.slf4j.Slf4j;

/**
 * 意見說明Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCOpinionDescripeSettingManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2706565411049184992L;
    /**
     * 意見說明Cache
     */
    @Autowired
    private WCOpinionDescripeSettingCache opinionDescripeSettingCache;
    /**
     * 意見說明Repository
     */
    @Autowired
    private WCOpinionDescripeSettingRepository opinionDescripeSettingRepository;
    /**
     * 使用者Manager
     */
    @Autowired
    private WkUserCache userManager;

    @Autowired
    private BpmOrganizationClient organizationClient;

    /**
     * 建立意見說明
     *
     * @param opinionDescripeSetting 意見說明物件
     * @param loginUserSid           建立者Sid
     * @param peSid                  簽呈Sid
     */
    public WCOpinionDescripeSetting create(
            WCOpinionDescripeSetting opinionDescripeSetting, Integer loginUserSid, String wcSid) {
        opinionDescripeSetting.setCreateUser(loginUserSid);
        opinionDescripeSetting.setCreateDate(new Date());
        opinionDescripeSetting.setWcSid(wcSid);
        opinionDescripeSetting = opinionDescripeSettingRepository.save(opinionDescripeSetting);
        opinionDescripeSettingCache.updateCache(opinionDescripeSetting);
        return opinionDescripeSetting;
    }

    /**
     * 更新意見說明
     *
     * @param opinionDescripeSetting 意見說明物件
     * @param loginUserSid           更新者Sid
     */
    public WCOpinionDescripeSetting update(WCOpinionDescripeSetting opinionDescripeSetting, Integer loginUserSid) {
        opinionDescripeSetting.setUpdateUser(loginUserSid);
        opinionDescripeSetting.setUpdateDate(new Date());
        opinionDescripeSetting = opinionDescripeSettingRepository.save(opinionDescripeSetting);
        opinionDescripeSettingCache.updateCache(opinionDescripeSetting);
        return opinionDescripeSetting;
    }

    /**
     * 取得意見說明苗數
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @return
     */
    public WCOpinionDescripeSetting getOpinionDescripeSetting(
            ManagerSingInfoType managerSingInfoType, String sourceSid, Integer opinion_usr) {
        return opinionDescripeSettingCache.getOpinionDescripeSetting(
                managerSingInfoType, sourceSid, opinion_usr);
    }

    /**
     * 更新退回原因
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @param rollbackReason      退回理由
     * @param rollbackUserSid     被退回的人員Sid
     * @param wcSid               聯絡單Sid
     */
    public WCOpinionDescripeSetting saveRollbackReason(
            ManagerSingInfoType managerSingInfoType,
            ActionType actionType,
            String sourceSid,
            Integer opinion_usr,
            String rollbackReason,
            Integer rollbackUserSid,
            String wcSid) {

        // ====================================
        // 組退回原因內容
        // ====================================
        User rollbackUser = userManager.findBySid(rollbackUserSid);
        StringBuilder sb = new StringBuilder();

        if (ActionType.EXEC_FLOW_SIGNER_ROLLBACK.equals(actionType)
                || ActionType.EXEC_FLOW_MEMBER_ROLLBACK.equals(actionType)) {
            sb.append(actionType.getDescr());
            sb.append("<br/>");
        }

        sb.append("退回者:");
        try {
            sb.append(organizationClient.findUserMajorRoleName(rollbackUser.getId()));
            sb.append("-");
        } catch (Exception ex) {
            log.error("findUserMajorRoleName ERROR", ex);
        }
        sb.append(rollbackUser.getName());
        sb.append("<br/>");
        sb.append("退回原因:" + rollbackReason);
        sb.append("<br/>");
        sb.append(
                "退回時間:"
                        + ToolsDate.transDateToString(
                                SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date()));

        // ====================================
        // 準備資料 (insert or update)
        // ====================================
        WCOpinionDescripeSetting opinionDescripeSetting = getOpinionDescripeSetting(
                managerSingInfoType, sourceSid, opinion_usr);

        // 無舊資料時新增
        if (opinionDescripeSetting == null) {
            opinionDescripeSetting = new WCOpinionDescripeSetting();
            opinionDescripeSetting.setOpinion_usr(opinion_usr);
            opinionDescripeSetting.setSourceType(managerSingInfoType);
            opinionDescripeSetting.setSourceSid(sourceSid);
            opinionDescripeSetting.setStatus(Activation.ACTIVE);
        }

        // 退回原因
        opinionDescripeSetting.setRollBackReason(sb.toString());

        if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
            opinionDescripeSetting = this.create(opinionDescripeSetting, opinion_usr, wcSid);
        } else {
            opinionDescripeSetting = this.update(opinionDescripeSetting, opinion_usr);
        }

        return opinionDescripeSetting;
    }

    /**
     * 更新作廢原因
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @param invailReason        作廢原因
     * @param peSid               簽呈Sid
     */
    public void saveInvailReason(
            ManagerSingInfoType managerSingInfoType,
            String sourceSid,
            Integer opinion_usr,
            String invailReason,
            String peSid) {
        try {
            WCOpinionDescripeSetting opinionDescripeSetting = getOpinionDescripeSetting(managerSingInfoType, sourceSid, opinion_usr);
            if (opinionDescripeSetting == null) {
                opinionDescripeSetting = new WCOpinionDescripeSetting();
                opinionDescripeSetting.setOpinion_usr(opinion_usr);
                opinionDescripeSetting.setSourceType(managerSingInfoType);
                opinionDescripeSetting.setSourceSid(sourceSid);
                opinionDescripeSetting.setStatus(Activation.ACTIVE);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("作廢原因:" + invailReason);
            sb.append("<br/>");
            sb.append(
                    "作廢時間:"
                            + ToolsDate.transDateToString(
                                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date()));
            opinionDescripeSetting.setInvaildReason(sb.toString());
            if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
                this.create(opinionDescripeSetting, opinion_usr, peSid);
            } else {
                this.update(opinionDescripeSetting, opinion_usr);
            }
        } catch (Exception e) {
            log.warn("saveInvailReason ERROR", e);
        }
    }

    /**
     * 更新中止原因
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @param invailReason        作廢原因
     * @param peSid               簽呈Sid
     */
    public void saveStopReason(
            ManagerSingInfoType managerSingInfoType,
            String sourceSid,
            Integer opinion_usr,
            String invailReason,
            String peSid) {
        try {
            WCOpinionDescripeSetting opinionDescripeSetting = getOpinionDescripeSetting(managerSingInfoType, sourceSid, opinion_usr);
            if (opinionDescripeSetting == null) {
                opinionDescripeSetting = new WCOpinionDescripeSetting();
                opinionDescripeSetting.setOpinion_usr(opinion_usr);
                opinionDescripeSetting.setSourceType(managerSingInfoType);
                opinionDescripeSetting.setSourceSid(sourceSid);
                opinionDescripeSetting.setStatus(Activation.ACTIVE);
            }

            String stopTime = ToolsDate.transDateToString(
                    SimpleDateFormatEnum.SdfDateTimestampss.getValue(), new Date());
            String style = "text-align:left; border:hidden !important;";
            StringBuilder sb = new StringBuilder();
            sb.append("<table style='border:hidden;'>");
            // sb.append("<tr><td style='text-align: left' colspan='2'>【執行方不執行】</td></tr>");
            sb.append("<tr style='border:hidden;'>");
            sb.append("<td style='" + style + " width:60px;'>說明:</td>");
            sb.append("<td style='" + style + "'>執行方不執行</td>");
            sb.append("</tr>");
            sb.append("<tr style='border:hidden;'>");
            sb.append("<td style='" + style + "'>原因:</td>");
            sb.append("<td style='" + style + "'>" + invailReason + "</td>");
            sb.append("</tr>");
            sb.append("<tr style='border:hidden;'>");
            sb.append("<td style='" + style + "'>終止時間:</td>");
            sb.append("<td style='" + style + "'>" + stopTime + "</td>");
            sb.append("</tr>");
            sb.append("</table>");

            opinionDescripeSetting.setInvaildReason(sb.toString());
            if (Strings.isNullOrEmpty(opinionDescripeSetting.getSid())) {
                this.create(opinionDescripeSetting, opinion_usr, peSid);
            } else {
                this.update(opinionDescripeSetting, opinion_usr);
            }
        } catch (Exception e) {
            log.warn("saveInvailReason ERROR", e);
        }
    }
}
