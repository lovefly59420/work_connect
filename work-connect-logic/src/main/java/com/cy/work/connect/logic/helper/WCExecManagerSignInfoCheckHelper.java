/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.vo.User;
import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.repository.WCExecManagerSignInfoRepository;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * @author brain0925_liao
 */
@Component
public class WCExecManagerSignInfoCheckHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7092698500541496399L;

    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;
    @Autowired
    private WCExecManagerSignInfoRepository wcExecManagerSignInfoRepository;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;


    /**
     * 取得可退回簽核節點物件List
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者
     * @return
     */
    public List<WCExecManagerSignInfo> getRollBackExecSignInfo(String wcSid, User loginUser) {
        List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
        List<WCExecManagerSignInfo> rollBackExecManagerSignInfos = Lists.newArrayList();
        for (GroupManagerSignInfo gi : groupManagerSignInfos) {
            for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                if (si.getStatus().equals(BpmStatus.INVALID)) {
                    continue;
                }
                if (wcExecManagerSignInfoCache
                        .findTaskCanSignUserIds(si.getInstanceID())
                        .contains(loginUser.getId())) {
                    WCExecManagerSignInfo rollBackExecManagerSignInfo = wcExecManagerSignInfoRepository.findOne(si.getSignInfoSid());
                    Preconditions.checkState(rollBackExecManagerSignInfo != null, "取得簽核物件異常，請確認!!");
                    rollBackExecManagerSignInfos.add(rollBackExecManagerSignInfo);
                }
            }
        }
        Preconditions.checkState(rollBackExecManagerSignInfos.size() > 0, "執行BPM簽名，比對簽核人員不符，請確認！！");
        return rollBackExecManagerSignInfos;
    }

    /**
     * 取得可簽名節點物件List
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 執行者
     * @return
     */
    public List<WCExecManagerSignInfo> getSignExecSignInfo(String wcSid, User loginUser) {
        List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
        List<WCExecManagerSignInfo> wcExecManagerSignInfos = Lists.newArrayList();
        for (GroupManagerSignInfo gi : groupManagerSignInfos) {
            for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                if (si.getStatus().equals(BpmStatus.INVALID)) {
                    continue;
                }
                if (wcExecManagerSignInfoCache
                        .findTaskCanSignUserIds(si.getInstanceID())
                        .contains(loginUser.getId())) {
                    WCExecManagerSignInfo wcExecManagerSignInfo = wcExecManagerSignInfoRepository.findOne(si.getSignInfoSid());
                    wcExecManagerSignInfos.add(wcExecManagerSignInfo);
                }
            }
        }
        Preconditions.checkState(wcExecManagerSignInfos.size() > 0, "執行BPM簽名，比對簽核人員不符，請確認！！");
        return wcExecManagerSignInfos;
    }

    /**
     * 是否顯示復原按鈕
     *
     * @param userId
     * @param status
     * @param tasks
     * @return
     */
    public Boolean isShowRecovery(String userId, BpmStatus status, List<ProcessTaskBase> tasks) {
        // 前一個簽核者為當前執行者才可復原
        ProcessTaskBase rTask = tasks.stream().reduce((a, b) -> b).orElseGet(() -> null);
        if (rTask instanceof ProcessTaskHistory) {
            return userId.equals(((ProcessTaskHistory) rTask).getExecutorID());
        }
        return false;
    }

    /**
     * 是否可退回至需求流程
     *
     * @param wcSid 工作聯絡單Sid
     */
    public boolean isCanRollBackToReq(String wcSid) {
        // ====================================
        // 檢查任一筆主流程已簽
        // ====================================
        // 查詢所有執行方簽核流程 (主流程、會簽)
        List<WCExecManagerSignInfo> activeExecFlowSignInfos = 
                this.wcExecManagerSignInfoManager.findActiveExecFlowSignInfos(wcSid);

        for (WCExecManagerSignInfo execSignInfo : activeExecFlowSignInfos) {
            if (BpmStatus.APPROVED.equals(execSignInfo.getStatus())) {
                return false;
            }
        }

        // ====================================
        // 檢查 執行單位已經開始做了 (包含完成)
        // ====================================
        List<WCExceDep> doingExceDeps = this.wcExecDepManager.findActiveByWcSid(wcSid);

        for (WCExceDep wcExceDep : doingExceDeps) {
            if (wcExceDep.getExecDepStatus() != null
                    && wcExceDep.getExecDepStatus().isAlreadyDo()) {
                return false;
            }
        }
        
        
        //檢核通過, 可執行
        return true;
    }

}
