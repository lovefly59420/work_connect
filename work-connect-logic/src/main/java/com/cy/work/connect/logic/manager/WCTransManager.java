package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.system.rest.client.vo.OrgTransMappingTo;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkHtmlUtils;
import com.cy.work.common.utils.WkOrgTransMappingUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.cache.WCExecDepSettingCache;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.helper.WCExecDepSettingHelper;
import com.cy.work.connect.logic.vo.view.setting11.SettingOrgTransPageVO;
import com.cy.work.connect.logic.vo.view.setting11.SettingOrgTransMappingVO;
import com.cy.work.connect.logic.vo.view.setting11.subfunc.Setting11ExecDepVO;
import com.cy.work.connect.logic.vo.view.setting11.subfunc.Setting11TransDepVO;
import com.cy.work.connect.repository.WCExceDepRepository;
import com.cy.work.connect.repository.WCTransDetailRepository;
import com.cy.work.connect.repository.WCTransMasterRepository;
import com.cy.work.connect.repository.vo.Setting11BaseDtVO;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.WCTransDetail;
import com.cy.work.connect.vo.WCTransMaster;
import com.cy.work.connect.vo.enums.TransType;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 工作聯絡單轉換作業 Manager
 *
 * @author jimmy_chou
 */
@Slf4j
@Component
public class WCTransManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4645702566958858159L;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private WCTransMasterRepository masterRep;
    @Autowired
    private WCTransDetailRepository detailRep;
    @Autowired
    private WkStringUtils stringUtils;
    @Autowired
    private WCExecDepSettingCache wcExecDepSettingCache;
    @Autowired
    private WCTagCache wCTagCache;
    @Autowired
    private WCExceDepRepository wcExceDepRepository;
    @Autowired
    private WCExecDepSettingHelper execDepSettingHelper;

    /**
     * 建立明細物件
     *
     * @param wcNo
     * @param beforeOrg
     * @param afterOrg
     * @param type
     * @return
     */
    public WCTransDetail createTransDetail(
            String wcNo, Integer beforeOrg, Integer afterOrg, TransType type) {
        WCTransDetail result = new WCTransDetail();
        result.setWcNo(wcNo);
        result.setType(type);
        result.setBeforeOrg(beforeOrg);
        result.setAfterOrg(afterOrg);
        return result;
    }

    /**
     * 轉換程式主檔
     *
     * @param comp
     * @param transUser
     * @param type
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public WCTransMaster createTransMaster(Integer comp, Integer transUser, TransType type) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        WCTransMaster result = new WCTransMaster();
        result.setComp(comp);
        result.setTransNo(this.findTransNo());
        result.setType(type);
        result.setTransUser(transUser);
        result.setTransDate(new Date());
        result.setTheme("[" + sdf.format(result.getTransDate()) + "] - " + type.getDescr());
        return masterRep.save(result);
    }

    /**
     * 建立轉換程式主檔
     *
     * @param compSid
     * @param depSid
     * @param userSid
     * @param type
     * @param sysDate
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public WCTransMaster createTransMaster(
            Integer compSid, Integer depSid, Integer userSid, TransType type, Date sysDate) {

        // 準備 entity
        WCTransMaster result = new WCTransMaster();

        result.setComp(compSid);
        result.setTransNo(this.findTransNo());
        result.setType(type);
        String theme = "[" + WkDateUtils.formatDate(sysDate, WkDateUtils.YYYY_MM_DD) + "]";
        theme += " - " + type.getDescr();
        result.setTheme(theme);
        result.setTransUser(userSid);
        result.setTransDep(depSid);
        result.setTransDate(new Date());

        // insert
        return this.masterRep.save(result);
    }

    /**
     * 取得當天轉單編號
     *
     * @return
     */
    private synchronized String findTransNo() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String head = "TRANS" + sdf.format(new Date());
        Integer count = this.countNoByHead(head + "%");
        return head + stringUtils.fullLeftText(String.valueOf(count + 1), 3, "0");
    }

    /**
     * 取得當天轉單數量
     *
     * @param head
     * @return
     */
    @Transactional(readOnly = true)
    private Integer countNoByHead(String head) {
        return masterRep.countNoByHead(head + "%");
    }

    /**
     * 明細存檔
     *
     * @param master
     * @param transDetails
     */
    @Transactional(rollbackFor = Exception.class)
    private void saveTransDetail(WCTransMaster master, List<WCTransDetail> transDetails) {
        transDetails.forEach(
                each -> {
                    each.setTransSid(master.getSid());
                    each.setTransNo(master.getTransNo());
                    each.setTransDate(master.getTransDate());
                });
        detailRep.save(transDetails);
    }

    /**
     * @param pageVO
     */
    public void btnSearchWorkVerify(SettingOrgTransPageVO pageVO, String compId) {

        // ====================================
        // 初始化
        // ====================================
        // 清空舊的查詢資料
        pageVO.setVerifyDtVOList(Lists.newArrayList());

        // ====================================
        // 查詢組織異動設定檔
        // ====================================
        // 查詢起始時間
        Long startTime = System.currentTimeMillis();

        // 查詢
        List<OrgTransMappingTo> orgTransMappingTos = WkOrgTransMappingUtils.getInstance().findOrgTrnsMapping(
                compId,
                new Date(pageVO.getQryEffectiveDate() == null ? 0 : pageVO.getQryEffectiveDate()));

        if (WkStringUtils.isEmpty(orgTransMappingTos)) {
            log.debug("查詢組織異動設定檔：0筆");
            return;
        }

        log.debug(
                WkCommonUtils.prepareCostMessage(startTime,
                        "查詢組織異動設定檔：[" + orgTransMappingTos.size() + "]筆"));

        // ====================================
        // 排序器
        // ====================================
        // 取得所有單位排序序號
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();
        // 以轉換前單位排序
        Comparator<OrgTransMappingTo> comparator = Comparator.comparing(
                vo -> WkOrgUtils.prepareOrgSortSeq(sortSeqMapByOrgSid, vo.getBeforeOrgSid()));

        // ====================================
        // 排序並轉 VO
        // ====================================
        List<SettingOrgTransMappingVO> verifyDtVOList = orgTransMappingTos.stream()
                .sorted(comparator)
                .map(orgTransMapping -> new SettingOrgTransMappingVO(orgTransMapping))
                .collect(Collectors.toList());

        pageVO.setVerifyDtVOList(verifyDtVOList);

        log.debug("查詢組織異動設定檔：" + verifyDtVOList.size() + "筆");
    }

    // ===========================================================================
    // 轉換：執行單位
    // ===========================================================================

    /**
     * @param beforOrgSid
     * @return
     */
    public List<Setting11ExecDepVO> queryForExecDep(Integer beforOrgSid, Integer afterOrgSid) {
        List<Setting11ExecDepVO> results = Lists.newArrayList();

        // ====================================
        // 以轉換前單位查詢執行單位設定
        // ====================================
        List<Setting11BaseDtVO> baseList = this.masterRep.queryForExecDep(beforOrgSid);

        for (Setting11BaseDtVO baseVO : baseList) {

            // 轉為執行單位專用VO
            Setting11ExecDepVO currVO = new Setting11ExecDepVO(baseVO);

            // 從快取中取得執行單位設定
            WCExecDepSetting execDepSetting = wcExecDepSettingCache.getExecDepSettingBySid(baseVO.getExecDepSettingSid());

            // 執行模式
            String execMode = execDepSetting != null ? (execDepSetting.isNeedAssigned() ? "派工" : "領單") : "";
            currVO.setExecMode(execMode);

            // 狀態
            String status = execDepSetting != null
                    ? (Activation.ACTIVE.equals(execDepSetting.getStatus())
                            ? "正常"
                            : "<span style='color:red;'>刪除</span>")
                    : "";
            currVO.setStatus(status);

            // 從快取中取得執行項目設定
            WCTag tag = wCTagCache.findBySid(baseVO.getTagSid());

            // 需求方簽核
            String reqSignInfo = "";
            if (tag.getReqFlowType() != null) {
                reqSignInfo = tag.getReqFlowType().getDesc();
            }
            currVO.setReqSignInfo(reqSignInfo);

            // ====================================
            // 處理領單人員
            // ====================================
            // 領單人員
            List<User> receviceUsers = Lists.newArrayList();
            if (execDepSetting != null
                    && execDepSetting.getReceviceUser() != null
                    && execDepSetting.getReceviceUser().getUserTos() != null) {
                receviceUsers = execDepSetting.getReceviceUser().getUserTos().stream()
                        .map(each -> WkUserCache.getInstance().findBySid(each.getSid()))
                        .collect(Collectors.toList());
            }
            currVO.setReceviceUsers(receviceUsers);

            // 取得轉換後單位，含以下部門
            Set<Integer> targerOrgSids = Sets.newHashSet(afterOrgSid);
            Set<Integer> childOrgSids = WkOrgCache.getInstance().findAllChildSids(afterOrgSid);
            if (WkStringUtils.notEmpty(childOrgSids)) {
                targerOrgSids.addAll(childOrgSids);
            }

            List<String> infos = Lists.newArrayList();
            for (User receviceUser : receviceUsers) {
                String userName = WkUserUtils.prepareUserNameWithDep(
                        receviceUser.getSid(),
                        OrgLevel.DIVISION_LEVEL,
                        false,
                        ":");

                Set<Integer> recevicerDepSids = Sets.newHashSet();
                if (receviceUser.getPrimaryOrg() != null) {
                    recevicerDepSids.add(receviceUser.getPrimaryOrg().getSid());
                }

                Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(receviceUser.getSid());
                if (WkStringUtils.notEmpty(managerOrgSids)) {
                    recevicerDepSids.addAll(managerOrgSids);
                }

                List<String> userErrorStatus = Lists.newArrayList();

                if (Activation.INACTIVE.equals(receviceUser.getStatus())) {
                    userErrorStatus.add("停用");
                }

                //檢查非轉換後部門成員
                boolean isInAfterDep = false;
                for (Integer targerOrgSid : targerOrgSids) {
                    if (recevicerDepSids.contains(targerOrgSid)) {
                        isInAfterDep = true;
                        break;
                    }
                }

                if (!isInAfterDep) {
                    userErrorStatus.add("非轉換後部門成員");
                }

                // 有錯誤狀態時，加標註
                if (WkStringUtils.notEmpty(userErrorStatus)) {
                    String userErrorStatusInfo = String.join("、", userErrorStatus);
                    userName = WkHtmlUtils.addStrikethroughStyle(userName, userErrorStatusInfo);
                    // 註記此筆資料不可轉換 (有任一領單人員有狀況，就不可轉換)
                    currVO.setSelectAble(false);
                }

                infos.add(userName);
            }

            if (WkStringUtils.notEmpty(infos)) {
                currVO.setReceviceUsersShow(String.join("<br/>", infos));
            }

            results.add(currVO);

        }

        return results;
    }

    /**
     * @param selectedDtVOList
     * @param beforOrgSid
     * @param afterOrgSid
     * @param funcDescr
     * @param loginCompSid
     * @param loginDepSid
     * @param loginUserSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void trnsExecDep(
            List<Setting11ExecDepVO> selectedDtVOList,
            Integer beforOrgSid,
            Integer afterOrgSid,
            String funcDescr,
            Integer loginCompSid,
            Integer loginDepSid,
            Integer loginUserSid) {
        // ====================================
        // 檢核轉換資料(執行單位已存在 & 領單人員已不在執行部門)
        // ====================================
        Org afterOrg = WkOrgCache.getInstance().findBySid(afterOrgSid);
        List<Org> orgs = Lists.newArrayList(afterOrg);
        orgs.addAll(
                WkOrgCache.getInstance().findAllChild(loginDepSid).stream()
                        .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                        .collect(Collectors.toList()));
        LinkedHashSet<User> canReceviceUsers = Sets.newLinkedHashSet();
        orgs.forEach(
                each -> {
                    canReceviceUsers.addAll(
                            WkUserCache.getInstance().findUserWithManagerByOrgSids(
                                    Lists.newArrayList(each.getSid()), Activation.ACTIVE));
                });
        for (Setting11ExecDepVO currVO : selectedDtVOList) {
            List<Integer> receviceUserSids = currVO.getReceviceUsers().stream().map(User::getSid).collect(Collectors.toList());

            List<User> receverData = Lists.newArrayList();
            if (receviceUserSids != null && !receviceUserSids.isEmpty()) {
                receviceUserSids.forEach(
                        each -> {
                            receverData.add(WkUserCache.getInstance().findBySid(each));
                        });
            }
            List<User> ngUsers = Lists.newArrayList();
            receverData.forEach(
                    each -> {
                        if (!canReceviceUsers.contains(each)) {
                            ngUsers.add(each);
                        }
                    });
            if (!ngUsers.isEmpty()) {
                // 從快取中取得執行單位設定
                WCExecDepSetting execDepSetting = wcExecDepSettingCache.getExecDepSettingBySid(currVO.getExecDepSettingSid());
                Org execDep = WkOrgCache.getInstance().findBySid(execDepSetting.getExec_dep_sid());

                String ngUserString = ngUsers.stream().map(User::getName)
                        .collect(Collectors.joining(","));
                Preconditions.checkState(
                        false,
                        "("
                                + currVO.getCategoryName()
                                + "-"
                                + currVO.getMenuName()
                                + "-"
                                + currVO.getTagName()
                                + "-"
                                + execDep.getName()
                                + ")請移除下述領單人員，因已不在執行部門("
                                + afterOrg.getName()
                                + ") : "
                                + ngUserString);
            }
            execDepSettingHelper.checkOtherExecDepSetting(
                    currVO.getExecDepSettingSid(), afterOrg, Lists.newArrayList(), currVO.getTagSid());
        }

        // ====================================
        // 準備轉換資料
        // ====================================
        // 收集轉換記錄 -> wc_trans_detail
        List<WCTransDetail> transDetail = Lists.newArrayList();

        // 收集選擇資料
        List<String> execDepSettingList = selectedDtVOList.stream()
                .map(Setting11ExecDepVO::getExecDepSettingSid)
                .distinct()
                .collect(Collectors.toList());
        List<String> wcNOList = Lists.newArrayList();
        if (WkStringUtils.notEmpty(execDepSettingList)) {
            wcNOList = wcExceDepRepository.findWCNOsByExecDepSettingSids(execDepSettingList,
                    beforOrgSid);
        }

        for (String currVO : wcNOList) {
            if (WkStringUtils.isEmpty(currVO)) {
                continue;
            }

            // 建立轉換資料【轉換明細檔物件:wc_trans_detail】
            transDetail.add(
                    this.createTransDetail(currVO, beforOrgSid, afterOrgSid, TransType.TRNS_EXEC_DEP));
        }

        // ====================================
        // 轉換資料
        // =====================================
        this.updateWCExecDepSetting(afterOrgSid, execDepSettingList);
        this.updateWCExecDep(afterOrgSid, execDepSettingList);
        this.wcExecDepSettingCache.updateCache();

        // ====================================
        // 寫轉換記錄主檔
        // ====================================
        Date sysDate = new Date();

        // 建立轉單程式主檔
        WCTransMaster transMaster = this.createTransMaster(
                loginCompSid, loginDepSid, loginUserSid, TransType.TRNS_EXEC_DEP, sysDate);

        log.info("轉檔單號：" + transMaster.getTransNo());

        // 儲存轉換明細檔
        this.saveTransDetail(transMaster, transDetail);
    }

    /**
     * @param afterOrgSid
     * @param execDepSettingSids
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateWCExecDep(Integer afterOrgSid, List<String> execDepSettingSids) {

        // 組SQL
        StringBuffer updateSql = new StringBuffer();
        updateSql.append("UPDATE wc_exec_dep ");
        updateSql.append("SET    dep_sid = :afterOrgSid ");
        updateSql.append("WHERE  exec_dep_status NOT IN ('CLOSE', 'STOP') ");
        updateSql.append("AND wc_exec_dep_setting_sid IN ( :execDepSettingSids ) ");

        // 參數
        Query query = em.createNativeQuery(updateSql.toString());
        query.setParameter("afterOrgSid", afterOrgSid);
        query.setParameter("execDepSettingSids", execDepSettingSids);

        // 執行
        int executeCount = query.executeUpdate();
        if (executeCount != 0) {
            log.info("組織異動：執行單位轉換【背景同步資料wc_exec_dep】:" + executeCount);
        }
        em.flush();
    }

    /**
     * @param beforOrgSid
     * @param afterOrgSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateWCExecDepClosed(Integer beforOrgSid, Integer afterOrgSid) {

        // 組SQL
        StringBuffer updateSql = new StringBuffer();
        updateSql.append("UPDATE wc_exec_dep ");
        updateSql.append("SET    dep_sid = :afterOrgSid ");
        updateSql.append("WHERE  exec_dep_status IN ('CLOSE', 'STOP') ");
        updateSql.append("AND dep_sid = :beforOrgSid ");

        // 參數
        Query query = em.createNativeQuery(updateSql.toString());
        query.setParameter("afterOrgSid", afterOrgSid);
        query.setParameter("beforOrgSid", beforOrgSid);

        // 執行
        int executeCount = query.executeUpdate();
        if (executeCount != 0) {
            log.info("組織異動：執行單位轉換【背景同步資料wc_exec_dep】【結案轉檔】:" + executeCount);
        }
        em.flush();
    }

    /**
     * @param afterOrgSid
     * @param execDepSettingSids
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateWCExecDepSetting(Integer afterOrgSid, List<String> execDepSettingSids) {

        // 組SQL
        StringBuffer updateSql = new StringBuffer();
        updateSql.append("UPDATE    wc_exec_dep_setting ");
        updateSql.append("SET       exec_dep_sid = :afterOrgSid ");
        updateSql.append("WHERE     wc_exec_dep_setting_sid IN ( :execDepSettingSids ) ");

        // 參數
        Query query = em.createNativeQuery(updateSql.toString());
        query.setParameter("afterOrgSid", afterOrgSid);
        query.setParameter("execDepSettingSids", execDepSettingSids);

        // 執行
        int executeCount = query.executeUpdate();
        if (executeCount != 0) {
            log.info("組織異動：執行單位轉換【wc_exec_dep_setting】:" + executeCount);
        }
        em.flush();
    }

    // ===========================================================================
    // 轉換：指派單位
    // ===========================================================================

    /**
     * @param beforOrgSid
     * @return
     */
    public List<Setting11TransDepVO> queryForTransDep(Integer beforOrgSid) {
        List<Setting11TransDepVO> results = Lists.newArrayList();

        // ====================================
        // 以轉換前單位查詢指派單位設定
        // ====================================
        List<Setting11BaseDtVO> baseList = this.masterRep.queryForTransDep(beforOrgSid);

        for (Setting11BaseDtVO baseVO : baseList) {

            // 轉為指派單位專用VO
            Setting11TransDepVO currVO = new Setting11TransDepVO(baseVO);

            // 從快取中取得執行單位設定
            WCExecDepSetting execDepSetting = wcExecDepSettingCache.getExecDepSettingBySid(baseVO.getExecDepSettingSid());

            // 略過已刪除狀態
            if (Activation.INACTIVE.equals(execDepSetting.getStatus())) {
                continue;
            }

            // 指派單位
            List<Integer> transDeps = Lists.newArrayList();
            if (execDepSetting != null
                    && execDepSetting.getTransdep() != null
                    && execDepSetting.getTransdep().getUserDepTos() != null) {
                transDeps = execDepSetting.getTransdep().getUserDepTos().stream()
                        .map(each -> Integer.valueOf(each.getDepSid()))
                        .collect(Collectors.toList());
            }
            currVO.setTransDeps(transDeps);

            // 執行模式
            String execMode = execDepSetting != null ? (execDepSetting.isNeedAssigned() ? "派工" : "領單") : "";
            currVO.setExecMode(execMode);

            // 狀態
            String status = execDepSetting != null
                    ? (Activation.ACTIVE.equals(execDepSetting.getStatus())
                            ? "正常"
                            : "<span style='color:red;'>刪除</span>")
                    : "";
            currVO.setStatus(status);

            // 從快取中取得執行項目設定
            WCTag tag = wCTagCache.findBySid(baseVO.getTagSid());

            // 需求方簽核
            String reqSignInfo = "";
            if (tag.getReqFlowType() != null) {
                reqSignInfo = tag.getReqFlowType().getDesc();
            }
            currVO.setReqSignInfo(reqSignInfo);
            results.add(currVO);
        }

        return results;
    }

    /**
     * @param selectedDtVOList
     * @param beforOrgSid
     * @param afterOrgSid
     * @param funcDescr
     * @param loginCompSid
     * @param loginDepSid
     * @param loginUserSid
     */
    @Transactional(rollbackFor = Exception.class)
    public void trnsTransDep(
            List<Setting11TransDepVO> selectedDtVOList,
            Integer beforOrgSid,
            Integer afterOrgSid,
            String funcDescr,
            Integer loginCompSid,
            Integer loginDepSid,
            Integer loginUserSid) {
        // ====================================
        // 檢核轉換資料(指派單位已存在)
        // ====================================
        for (Setting11TransDepVO currVO : selectedDtVOList) {
            List<Integer> transDeps = currVO.getTransDeps();
            if (WkStringUtils.notEmpty(transDeps) && transDeps.contains(afterOrgSid)) {
                // 從快取中取得執行單位設定
                WCExecDepSetting execDepSetting = wcExecDepSettingCache.getExecDepSettingBySid(currVO.getExecDepSettingSid());
                Org execDep = WkOrgCache.getInstance().findBySid(execDepSetting.getExec_dep_sid());

                Preconditions.checkState(
                        false,
                        "("
                                + currVO.getCategoryName()
                                + "-"
                                + currVO.getMenuName()
                                + "-"
                                + currVO.getTagName()
                                + "-"
                                + execDep.getName()
                                + ")欲轉換指派單位已存在");
            }
        }

        // ====================================
        // 準備轉換資料
        // ====================================
        // 收集選擇資料
        List<String> execDepSettingList = selectedDtVOList.stream()
                .map(Setting11TransDepVO::getExecDepSettingSid)
                .distinct()
                .collect(Collectors.toList());

        // ====================================
        // 轉換資料
        // ====================================
        this.updateTransDep(beforOrgSid, afterOrgSid, execDepSettingList);
        this.wcExecDepSettingCache.updateCache();

        // ====================================
        // 寫轉換記錄主檔
        // ====================================
        Date sysDate = new Date();

        // 建立轉單程式主檔
        WCTransMaster transMaster = this.createTransMaster(
                loginCompSid, loginDepSid, loginUserSid, TransType.TRNS_EXEC_DEP, sysDate);

        log.info("轉檔單號：" + transMaster.getTransNo());
    }

    /**
     * @param beforOrgSid
     * @param afterOrgSid
     * @param execDepSettingSids
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateTransDep(
            Integer beforOrgSid, Integer afterOrgSid, List<String> execDepSettingSids) {

        // 組SQL
        StringBuffer updateSql = new StringBuffer();
        updateSql.append("UPDATE wc_exec_dep_setting ");
        updateSql.append(
                "SET    transdep = JSON_REPLACE(transdep, JSON_UNQUOTE(JSON_SEARCH(transdep,'one', :beforOrgSid ,null,'$[*].depSid')),convert(:afterOrgSid using utf8mb4)) ");
        updateSql.append("WHERE  wc_exec_dep_setting_sid IN ( :execDepSettingSids ) ");

        // 參數
        Query query = em.createNativeQuery(updateSql.toString());
        query.setParameter("beforOrgSid", beforOrgSid);
        query.setParameter("afterOrgSid", afterOrgSid);
        query.setParameter("execDepSettingSids", execDepSettingSids);

        // 執行
        int executeCount = query.executeUpdate();
        if (executeCount != 0) {
            log.info("組織異動：指派單位轉換【背景同步資料】:" + executeCount);
        }
        em.flush();
    }
}
