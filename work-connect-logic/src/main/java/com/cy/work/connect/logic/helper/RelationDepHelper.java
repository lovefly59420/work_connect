/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class RelationDepHelper {

    /**
     * 最大快取時間5秒
     */
    private static long CACHE_OVERDUE_MILLISECOND = 5;

    // ========================================================================
    // 主要方法
    // ========================================================================
    /**
     * 計算關係部門 (預設包含 user 的主要部門)
     * 
     * @param userSid
     * @param isIncludeUserChildOrg  是否包含使用者的下層所有部門 (含兼職)
     * @param isIncludeUserParentOrg 是否包含使用者的直系向上部門 (含兼職)
     * @return
     */
    public static Set<Integer> findRelationOrgSids(
            Integer userSid,
            boolean isIncludeUserChildOrg,
            boolean isIncludeUserParentOrg) {

        // ====================================
        // 快取
        // ====================================
        // cache name
        String uniqueCacheName = RelationDepHelper.class.getSimpleName() + "_findRelationOrgSids";
        // 快取 key (記得把全部參數加入)
        List<String> keys = Lists.newArrayList(
                userSid + "",
                isIncludeUserChildOrg + "",
                isIncludeUserParentOrg + "");
        // 取得
        Set<Integer> relationOrgSids = WkCommonCache.getInstance().getCache(uniqueCacheName, keys, CACHE_OVERDUE_MILLISECOND);
        if (relationOrgSids != null) {
            return relationOrgSids;
        }

        // 快取中無資料，繼續往下做
        relationOrgSids = Sets.newHashSet();

        // ====================================
        // 取得 user 主要部門
        // ====================================
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null
                || user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            return Sets.newHashSet();
        }

        if (user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            log.warn("User:[{}-{}]，沒有 PrimaryOrg 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }

        Org primaryOrg = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        if (primaryOrg == null) {
            log.warn("User:[{}-{}] 找不到 PrimaryOrg:[{}] 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }

        // ====================================
        // 加入自己的部門
        // ====================================
        // 主要部門
        Integer primaryOrgSid = primaryOrg.getSid();
        relationOrgSids.add(primaryOrgSid);
        // 管理部門 (兼職)
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(userSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            relationOrgSids.addAll(managerOrgSids);
        }

        // ====================================
        // 直系向上部門
        // ====================================
        if (isIncludeUserParentOrg) {
            // 主要部門的直系向上部門
            Set<Integer> parentOrgSids = WkOrgCache.getInstance().findAllParentSid(primaryOrgSid);
            if (WkStringUtils.notEmpty(parentOrgSids)) {
                relationOrgSids.addAll(parentOrgSids);
            }

            // 管理部門的直系向上部門
            if (WkStringUtils.notEmpty(managerOrgSids)) {
                for (Integer managerOrgSid : managerOrgSids) {
                    Set<Integer> managerOrgSparentOrgSids = WkOrgCache.getInstance().findAllParentSid(managerOrgSid);
                    if (WkStringUtils.notEmpty(managerOrgSparentOrgSids)) {
                        relationOrgSids.addAll(managerOrgSparentOrgSids);
                    }
                }
            }
        }

        // ====================================
        // 以下部門
        // ====================================
        if (isIncludeUserChildOrg) {
            Set<Integer> childOrgSids = WkOrgCache.getInstance().findAllChildSids(primaryOrgSid);
            if (WkStringUtils.notEmpty(childOrgSids)) {
                relationOrgSids.addAll(childOrgSids);
            }
            // 管理部門的下層部門
            if (WkStringUtils.notEmpty(managerOrgSids)) {
                Set<Integer> allManagerChildSid = WkOrgCache.getInstance().findAllChildSids(managerOrgSids);
                if (WkStringUtils.notEmpty(allManagerChildSid)) {
                    relationOrgSids.addAll(allManagerChildSid);
                }
            }
        }

        // 加入快取
        WkCommonCache.getInstance().putCache(uniqueCacheName, keys, relationOrgSids);

        return relationOrgSids;
    }

    // ========================================================================
    // 類別可使用權限
    // ========================================================================
    /**
     * 取得使用者對應單位，for 聯絡單單據類別可使用權限
     * 
     * @param loginUserSid
     * @return
     */
    public static Set<Integer> findMatchOrgSidsForWcForm(Integer loginUserSid) {

        Set<Integer> matchDepSids = Sets.newHashSet();

        // ====================================
        // 取得 user 主要部門
        // ====================================
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        if (user == null
                || user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            return Sets.newHashSet();
        }

        if (user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            log.warn("User:[{}-{}]，沒有 PrimaryOrg 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }

        Org primaryOrg = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        if (primaryOrg == null) {
            log.warn("User:[{}-{}] 找不到 PrimaryOrg:[{}] 資料! ", user.getSid(), user.getName(), user.getPrimaryOrg().getSid());
            return Sets.newHashSet();
        }
        // 加入主要部門
        matchDepSids.add(primaryOrg.getSid());

        // 加入管理的單位
        Set<Integer> managerDepSids = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid)
                .stream()
                //WORKCOMMU-592 【單據新增】使用者可使用項目，排除『已停用』管理單位權限
                .filter(depSid -> WkOrgUtils.isActive(depSid))
                .collect(Collectors.toSet());

        if (WkStringUtils.notEmpty(managerDepSids)) {
            matchDepSids.addAll(managerDepSids);
        }

        return matchDepSids;
    }

    // ========================================================================
    // 領單
    // ========================================================================
    /**
     * 計算關係部門：for 可領單部門
     * 
     * @return
     */
    public static Set<Integer> findRelationOrgSidsForExecCanReceive(Integer loginUserSid) {

        Set<Integer> canReceiveDepSids = findRelationOrgSids(
                loginUserSid,
                false, // 上層單位不可領單 (組級的單，部級成員不用領)
                true // 執行單位往下所有成員可領單
        );

        if (WorkParamManager.getInstance().isShowSearchSql()) {
            String canReceiveDepforShow = WkOrgUtils.prepareDepsNameByTreeStyle(canReceiveDepSids, 100);
            canReceiveDepforShow = WkJsoupUtils.getInstance().htmlToText(canReceiveDepforShow);
            log.debug("\r\n可領單位:" + canReceiveDepforShow);
        }

        return canReceiveDepSids;
    }

    // ========================================================================
    // 分派
    // ========================================================================
    /**
     * 計算關係部門：for 可分派的部門 (僅管理的單位可以分派. 不含以下)
     * 
     * @param loginUserSid
     * @return
     */
    public static Set<Integer> findRelationOrgSidsForExecCanAssign(Integer loginUserSid) {

        // ====================================
        // 快取
        // ====================================
        // cache name
        String uniqueCacheName = RelationDepHelper.class.getSimpleName() + "findRelationOrgSidsForExecCanAssign";
        // 快取 key (記得把全部參數加入)
        List<String> keys = Lists.newArrayList(loginUserSid + "");

        // 取得
        Set<Integer> relationDepSids = WkCommonCache.getInstance().getCache(uniqueCacheName, keys, CACHE_OVERDUE_MILLISECOND);
        if (relationDepSids != null) {
            return relationDepSids;
        }

        // 無快取資料時，往下處理

        // ====================================
        // 僅管理的單位可以分派
        // ====================================
        relationDepSids = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid)
                .stream()
                .collect(Collectors.toSet());

        // 加入快取
        WkCommonCache.getInstance().putCache(uniqueCacheName, keys, relationDepSids);

        return relationDepSids;
    }

    /**
     * 計算關係部門：當執行單位有設定『分派(指派)部門』時，user 對應的權限部門<br/>
     * 1.自己的主要單位 (不含以下)
     * 2.自己管理的單位 (不含以下)(for 兼職)
     * 
     * @param loginUserSid
     * @return
     */
    public static Set<Integer> findRelationOrgSidsForExecCanAssignSettingAssignDeps(Integer loginUserSid) {

        // ====================================
        // 快取
        // ====================================
        // cache name
        String uniqueCacheName = RelationDepHelper.class.getSimpleName() + "_findRelationOrgSidsForExecCanAssignSettingAssignDeps";
        // 快取 key (記得把全部參數加入)
        List<String> keys = Lists.newArrayList(loginUserSid + "");
        // 取得
        Set<Integer> relationDepSids = WkCommonCache.getInstance().getCache(uniqueCacheName, keys, CACHE_OVERDUE_MILLISECOND);
        if (relationDepSids != null) {
            return relationDepSids;
        }

        // 無快取資料時，往下處理
        relationDepSids = Sets.newHashSet();

        // ====================================
        // 取得 user 主要部門
        // ====================================
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        if (user == null
                || user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            return Sets.newHashSet();
        }

        if (user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            log.warn("User:[{}-{}]，沒有 PrimaryOrg 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }

        Org primaryOrg = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        if (primaryOrg == null) {
            log.warn("User:[{}-{}] 找不到 PrimaryOrg:[{}] 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }

        relationDepSids.add(primaryOrg.getSid());

        // ====================================
        // 管理的部門
        // ====================================
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            relationDepSids.addAll(managerOrgSids);
        }

        WkCommonCache.getInstance().putCache(uniqueCacheName, keys, relationDepSids);
        return relationDepSids;
    }

    // ========================================================================
    // user 有權限轉單的執行部門
    // ========================================================================
    /**
     * 計算關係部門：當為特別可轉單部門時，user 有權限轉單的執行單位<br/>
     * 說明：<br/>
     * 1.執行單位為下層單位時可轉單
     * 2.執行單位為上層單位時可轉單
     * 
     * @param loginUserSid
     * @return
     */
    public static Set<Integer> findRelationOrgSidsForSpecialPermissionExecTransPerson(Integer loginUserSid) {
        return findRelationOrgSids(
                loginUserSid,
                true, // 執行單位為下層單位時，可轉單
                true // 執行單位往下所有成員可轉單
        );
    }

    // ========================================================================
    // 執行方可閱
    // ========================================================================
    /**
     * 計算關係部門：for 可閱的執行方部門<br/>
     * 1.登入者部門(含兼職)以上單位(含登入部門)
     * 2.管理的部門含以下單位
     * 
     * @return
     */
    public static Set<Integer> findRelationOrgSidsForExecCanView(Integer loginUserSid) {

        // ====================================
        // 登入者部門(含兼職)以上單位(含主要部門)
        // ====================================
        Set<Integer> relationOrgSids = findRelationOrgSids(loginUserSid, false, true);

        // ====================================
        // 加入管理部門含以下單位
        // ====================================
        // 取得管理部門
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);

        if (WkStringUtils.notEmpty(managerOrgSids)) {
            // 加入管理部門
            relationOrgSids.addAll(managerOrgSids);
            // 加入管理部門的下層部門
            Set<Integer> allManagerChildSid = WkOrgCache.getInstance().findAllChildSids(managerOrgSids);
            if (WkStringUtils.notEmpty(allManagerChildSid)) {
                relationOrgSids.addAll(allManagerChildSid);
            }
        }
        return relationOrgSids;
    }

    /**
     * 兜組執行單位檔wc_exec_dep可閱過濾條件SQL
     * 
     * @param loginUserSid 登入者 sid
     * @return condition SQL
     */
    public static String prepareConditionSqlForExecCanView(Integer loginUserSid) {

        // 1.執行人員可閱 (execDep.exec_user_sid)
        // 3.若[非僅領單人員可閱(execDep.receviceuser is empty)]：比對『可閱的執行方部門』
        // 4.若[僅領單人員可閱]：比對登入者是否在名中 (in execDep.receviceuser)
        // 備註：不比對設定檔 wc_exec_dep_setting, 因當下設定可能和單據執行時設定不一致

        // ====================================
        // 可閱的執行方部門
        // ====================================
        // 取得
        Set<Integer> execCanViewOrgSids = RelationDepHelper.findRelationOrgSidsForExecCanView(loginUserSid);
        // 轉SQL string
        String execCanViewOrgSidsSQLStr = execCanViewOrgSids
                .stream()
                .map(orgSid -> orgSid + "")
                .collect(Collectors.joining("' ,'", "'", "'"));
        // 防呆
        if (WkStringUtils.isEmpty(execCanViewOrgSidsSQLStr)) {
            execCanViewOrgSidsSQLStr = "-1";
        }

        // ====================================
        // 管理部門 (for 僅領單人員可閱時，主管亦可閱)
        // ====================================
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerWithChildOrgSids(loginUserSid);
        // 轉SQL string
        String managerOrgSidsSQLStr = execCanViewOrgSids
                .stream()
                .map(orgSid -> orgSid + "")
                .collect(Collectors.joining("' ,'", "'", "'"));

        // ====================================
        // 可閱的執行方部門
        // ====================================
        // 限定執行單位需已進入執行狀態
        String alreadyDoExecDepStatus = Lists.newArrayList(WCExceDepStatus.values()).stream()
                .filter(status -> status.isAlreadyDo())
                .map(WCExceDepStatus::name)
                .collect(Collectors.joining("','", "'", "'"));

        // ====================================
        // 兜組 SQL
        // ====================================
        StringBuffer sql = new StringBuffer();
        sql.append("(");

        sql.append(" execDep.status = 0 ");
        // 限定執行單位狀態
        sql.append(" AND execDep.exec_dep_status IN (" + alreadyDoExecDepStatus + ") ");
        sql.append(" AND ( ");

        // 1.執行人員可閱
        sql.append("        execDep.exec_user_sid = " + loginUserSid + " ");

        // 2.直系主管可閱 (僅領單人員可閱時，主管亦可閱)
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            sql.append("    OR execDep.dep_sid IN ( " + managerOrgSidsSQLStr + " )  ");
        }

        // 2.非僅領單人員可閱比對『可閱的執行方部門』
        sql.append("        OR (  ");
        // execDep.receviceuser 沒有值
        sql.append("                (execDep.receviceuser IS NULL ");
        sql.append("                 OR execDep.receviceuser NOT LIKE '%sid%') ");
        sql.append("            AND execDep.dep_sid IN ( " + execCanViewOrgSidsSQLStr + " )  ");
        sql.append("            )");
        // 3.僅領單人員可閱，比對名單
        sql.append("        OR Json_contains(Json_extract(IF(execDep.receviceuser = '', '[]', execDep.receviceuser), \"$[*].sid\"), " + loginUserSid + ")");

        sql.append("    )");
        sql.append(")");

        return sql.toString();
    }

    // ========================================================================
    // 退件一覽表
    // ========================================================================
    /**
     * 退件一覽表，退件執行單位成員可閱
     * 
     * @param loginUserSid
     * @return
     */
    public static Set<Integer> findRelationOrgSidsForRejectExecCanView(Integer loginUserSid) {
        return findRelationOrgSids(
                loginUserSid,
                false, // 執行單位為下層單位時, 上層單位不可閱
                true // 執行單位往下所有成員可閱
        );
    }

    // ========================================================================
    // 備忘錄
    // ========================================================================
    /**
     * 取得 wc_memo_category 比對 use_dep, edit_dep 時，登入者對應部門
     * 設定部門以下人員都可使用
     * 主要部門+直系向上（含兼職）
     * 
     * @param loginUserSid 登入者 sid
     * @return
     */
    public static Set<Integer> findMemoMatchOrgSidsForCategoryDepSetting(Integer loginUserSid) {
        // wc_memo_category

        Set<Integer> relationDepSids = Sets.newHashSet();

        // ====================================
        // 取得 user 主要部門
        // ====================================
        User user = WkUserCache.getInstance().findBySid(loginUserSid);
        if (user == null
                || user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            return Sets.newHashSet();
        }

        if (user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            log.warn("User:[{}-{}]，沒有 PrimaryOrg 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }

        Org primaryOrg = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        if (primaryOrg == null) {
            log.warn("User:[{}-{}] 找不到 PrimaryOrg:[{}] 資料! ", user.getSid(), user.getName());
            return Sets.newHashSet();
        }
        // 加入主要部門
        relationDepSids.add(primaryOrg.getSid());

        // ====================================
        // 管理的部門
        // ====================================
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            relationDepSids.addAll(managerOrgSids);
        }

        // ====================================
        // 直系向上
        // ====================================
        Set<Integer> relationDepSidsWithParent = Sets.newHashSet();
        relationDepSidsWithParent.addAll(relationDepSids);
        // 逐筆查詢
        for (Integer depSid : relationDepSids) {
            Set<Integer> parentSids = WkOrgCache.getInstance().findAllParentSid(depSid);
            if (WkStringUtils.notEmpty(parentSids)) {
                relationDepSidsWithParent.addAll(parentSids);
            }
        }

        return relationDepSidsWithParent;
    }
}
