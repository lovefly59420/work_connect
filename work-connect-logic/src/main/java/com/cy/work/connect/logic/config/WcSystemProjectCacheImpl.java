package com.cy.work.connect.logic.config;

import com.cy.system.rest.client.util.SystemProjectCache;
import com.cy.work.connect.logic.helper.ClearCacheHelper;
import org.springframework.stereotype.Component;

/**
 * 公用清快取實做
 *
 * @author allen
 */
@Component
public class WcSystemProjectCacheImpl implements SystemProjectCache {

    @Override
    public void doClearProjectCache() {
        // 清除專案Cache
        ClearCacheHelper.process();
    }
}
