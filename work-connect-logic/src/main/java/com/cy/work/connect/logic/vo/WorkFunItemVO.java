/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class WorkFunItemVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8517398066980285817L;

    @Getter
    private final Integer sid;
    @Getter
    private final String function_title;
    @Getter
    private final String component_id;
    @Getter
    private final String url;
    @Getter
    private final Integer seq;
    @Setter
    @Getter
    private String cssStr = "";
    @Setter
    @Getter
    private String countInfo = "";

    public WorkFunItemVO(
        Integer sid, String function_title, String component_id, String url, Integer seq) {

        this.sid = sid;
        this.function_title = function_title;
        this.component_id = component_id;
        this.url = url;
        this.seq = seq;
    }
}
