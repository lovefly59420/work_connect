package com.cy.work.connect.logic.cache;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCMenuTagRepository;
import com.cy.work.connect.vo.WCMenuTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 單據名稱Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMenuTagCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 325629189622106963L;
    private final transient Comparator<WCMenuTag> wcMenuTagComparator = new Comparator<WCMenuTag>() {
        @Override
        public int compare(WCMenuTag obj1, WCMenuTag obj2) {
            final Integer seq1 = obj1.getSeq();
            final Integer seq2 = obj2.getSeq();
            return seq1.compareTo(seq2);
        }
    };
    @Autowired
    private WCMenuTagRepository wcMenuTagRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCMenuTag> wcMenuTagMap;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCMenuTagCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcMenuTagMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立中類(單據名稱)Cache");
        try {
            Map<String, WCMenuTag> tempWCMenuTagMap = Maps.newConcurrentMap();
            wcMenuTagRepository
                    .findAll()
                    .forEach(
                            item -> {
                                tempWCMenuTagMap.put(item.getSid(), item);
                            });
            wcMenuTagMap = tempWCMenuTagMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立單據名稱Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache
     *
     * @param wcMenuTag
     */
    public synchronized void updateCacheByWCMenuTag(WCMenuTag wcMenuTag) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立中類(單據名稱)Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立中類(單據名稱)Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wcMenuTagMap.put(wcMenuTag.getSid(), wcMenuTag);
    }

    public WCMenuTag getWCMenuTagBySid(String sid) {
        return wcMenuTagMap.get(sid);
    }

    /**
     * 取得單據名稱
     *
     * @param wcCategoryTagSid 類別Sid
     * @return
     */
    public List<WCMenuTag> getWCMenuTagByCategorySid(
            String wcCategoryTagSid,
            Activation activation) {
        List<WCMenuTag> result = Lists.newArrayList();
        try {
            wcMenuTagMap
                    .values()
                    .forEach(
                            item -> {
                                if (activation != null && !activation.equals(item.getStatus())) {
                                    return;
                                }
                                if (!Strings.isNullOrEmpty(wcCategoryTagSid)
                                        && !wcCategoryTagSid.equals(item.getWcCategoryTagSid())) {
                                    return;
                                }
                                result.add(item);
                            });
        } catch (Exception e) {
            log.warn("getWCMenuTagByCategorySid ERROR", e);
        }
        Collections.sort(result, wcMenuTagComparator);
        return result;
    }

    /**
     * @param wcCategoryTagSid
     * @return
     */
    public List<WCMenuTag> findByCategorySidAndOrderBySeq(String wcCategoryTagSid) {

        Comparator<WCMenuTag> comparator = Comparator.comparing(WCMenuTag::getSeq);

        return this.wcMenuTagMap.values().stream()
                .filter(tag -> WkCommonUtils.compareByStr(wcCategoryTagSid, tag.getWcCategoryTagSid()))
                // 以 SEQ 排序
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}
