/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import java.util.List;
import lombok.Getter;

/**
 * @author brain0925_liao
 */
public class GroupManagerSignInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4230362779522596130L;

    @Getter
    private final Integer groupSeq;
    @Getter
    private final List<SingleManagerSignInfo> singleManagerSignInfos;

    public GroupManagerSignInfo(
        Integer groupSeq, List<SingleManagerSignInfo> singleManagerSignInfos) {
        this.groupSeq = groupSeq;
        this.singleManagerSignInfos = singleManagerSignInfos;
    }
}
