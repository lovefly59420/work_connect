/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCFunItemRepository;
import com.cy.work.connect.vo.WCFunItem;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 選單細項
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCFunItemManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3899818754120258602L;

    @Autowired
    private WCFunItemRepository funItemRepository;

    /**
     * 取得選單細項
     */
    public List<WCFunItem> getFunItemByCategoryModel() {
        try {
            String category_model = "WC";
            return funItemRepository.getFunItemByCategoryModel(category_model);
        } catch (Exception e) {
            log.warn("getFunItemByCategoryModel Error : " + e, e);
            return Lists.newArrayList();
        }
    }
}
