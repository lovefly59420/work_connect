/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCMemoTraceRepository;
import com.cy.work.connect.vo.WCMemoTrace;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 備忘錄追蹤Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMemoTraceManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8026726107561240018L;

    private static WCMemoTraceManager instance;
    @Autowired
    private WCMemoTraceRepository wcMemoTraceRepository;

    public static WCMemoTraceManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMemoTraceManager.instance = this;
    }

    /**
     * 取得追蹤資料 By Sid
     *
     * @param sid
     * @return
     */
    public WCMemoTrace getWCMemoTraceBySid(String sid) {
        try {
            return wcMemoTraceRepository.findOne(sid);
        } catch (Exception e) {
            log.warn("getWCMemoTraceBySid Error : " + e, e);
        }
        return null;
    }

    /**
     * 建立追蹤資料
     *
     * @param wcMemoTrace   追蹤物件
     * @param createUserSid 建立者Sid
     * @return
     */
    public WCMemoTrace create(WCMemoTrace wcMemoTrace, Integer createUserSid) {
        try {
            wcMemoTrace.setCreate_usr(createUserSid);
            Date createDate = new Date();
            wcMemoTrace.setCreate_dt(createDate);
            wcMemoTrace.setUpdate_dt(createDate);
            return wcMemoTraceRepository.save(wcMemoTrace);
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 取得追蹤資料 By 備忘錄Sid
     *
     * @param memoSid 備忘錄Sid
     * @return
     */
    public List<WCMemoTrace> getWCMemoTracesByMemoSid(String memoSid) {
        try {
            List<WCMemoTrace> result = wcMemoTraceRepository.getWCTraceByWcMemoSid(memoSid);
            Collections.sort(
                result,
                new Comparator<WCMemoTrace>() {
                    @Override
                    public int compare(WCMemoTrace o1, WCMemoTrace o2) {
                        Date d1 =
                            (o1.getUpdate_dt() != null) ? o1.getUpdate_dt() : o1.getCreate_dt();
                        Date d2 =
                            (o2.getUpdate_dt() != null) ? o2.getUpdate_dt() : o2.getCreate_dt();
                        if (!d1.equals(d2)) {
                            return d2.compareTo(d1);
                        } else {
                            if (o1.getWcMemoTraceType().equals(WCTraceType.CLOSE)
                                || o1.getWcMemoTraceType().equals(WCTraceType.CLOSE_STOP)) {
                                return -1;
                            } else if (o2.getWcMemoTraceType().equals(WCTraceType.CLOSE)
                                || o2.getWcMemoTraceType().equals(WCTraceType.CLOSE_STOP)) {
                                return 1;
                            }
                        }
                        return 0;
                    }
                });
            return result;
        } catch (Exception e) {
            log.warn("getWCMemoTracesByMemoSid Error : ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 更新追蹤資料
     *
     * @param wcMemoTrace   追蹤資料
     * @param updateUserSid 更新者Sid
     * @return
     */
    public WCMemoTrace update(WCMemoTrace wcMemoTrace, Integer updateUserSid) {
        try {
            wcMemoTrace.setUpdate_usr(updateUserSid);
            wcMemoTrace.setUpdate_dt(new Date());
            return wcMemoTraceRepository.save(wcMemoTrace);
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }
}
