package com.cy.work.connect.logic.helper.signflow;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.CheckLegalDateHelper;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterCustomLogicManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCOpinionDescripeSettingManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.logic.manager.WCTraceCustomLogicManager;
import com.cy.work.connect.logic.manager.WCTraceManager;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 需求單位簽核流程
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class ReqFlowActionHelper {

    // ========================================================================
    // 服務
    // ========================================================================
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private BpmManager bpmManager;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private CheckLegalDateHelper checkLegalDateHelper;
    @Autowired
    private WCOpinionDescripeSettingManager wcOpinionDescripeSettingManager;

    // ========================================================================
    // 主方法 - 需求單位流程
    // ========================================================================
    /**
     * 需求流程-簽名
     *
     * @param wcSid       單據 sid
     * @param execUserSid 執行者 sid
     * @throws UserMessageException 錯誤時拋出
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doReqflowSign(String wcSid, Integer execUserSid) throws UserMessageException {

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ReqFlowActionData actionData = this.prepareReqSignActionData(wcSid, execUserSid, false);

        // ====================================
        // 比對是否登入者是否為『待簽人員』
        // ====================================
        try {
            boolean isCanSign = this.bpmManager.isSignUserWithException(
                    actionData.execUser.getId(), actionData.reqSignInfo.getBpmId());
            if (!isCanSign) {
                String errorMessage = WkMessage.NEED_RELOAD + "(比對非目前可簽核人員)";
                log.warn(errorMessage);
                throw new UserMessageException(errorMessage, InfomationLevel.WARN);
            }
        } catch (SystemOperationException e) {
            throw new UserMessageException(e.getMessage(), e.getExceptionMessage(), InfomationLevel.WARN);
        }

        // ====================================
        // 取得單據小類的設定資料
        // ====================================
        // 防呆 - 無小類資料，不可能發生
        if (actionData.wcMaster.getCategoryTagTo() == null
                || WkStringUtils.isEmpty(actionData.wcMaster.getCategoryTagTo().getTagSids())) {
            String errorMessage = WkMessage.EXECTION;
            log.error(errorMessage + "單據無執行項目(小類)資料 (wc_master.category_tag) wcsid:[{}]", actionData.wcMaster.getSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
        // 查詢
        List<WCTag> wcTags = this.wcTagManager.findBySidIn(actionData.wcMaster.getCategoryTagTo().getTagSids());

        // 防呆
        if (WkStringUtils.isEmpty(wcTags)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的小類資料)";
            log.error(errorMessage + "單據無執行項目(小類)資料 (wc_master.category_tag) wcsid:[{}]", actionData.wcMaster.getSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 檢查小類-其他判斷-日期合理性
        // ====================================
        // 送件日期為空時，代表當下為『申請人簽名』，故送件日期為今日
        Date sendDate = actionData.wcMaster.getSendDate();
        if (sendDate == null) {
            sendDate = new Date();
        }

        this.checkLegalDateHelper.checkLegitimateRangeAndSendEndDate(
                wcSid,
                sendDate,
                actionData.wcMaster.getLegitimateRangeDate(),
                actionData.wcMaster.getStartEndDate(),
                wcTags);

        // ====================================
        // 檢查包含需要上傳的小類時，未上傳附件
        // ====================================
        // 檢查是否為主要簽核階段 (會簽階段時不檢查) (簽核流程 > 1條)
        if (this.wcManagerSignInfoManager.findActiveReqFlowSignInfos(actionData.wcMaster.getSid()).size() == 1) {
            this.wcMasterHelper.checkNeedUpdateFile(actionData.wcMaster.getSid(), wcTags);
        }

        // ====================================
        // 簽名
        // ====================================
        try {
            this.wcManagerSignInfoManager.onlyDoBPMSign(actionData.reqSignInfo, actionData.execUser);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED;
            log.error(errorMessage + "wcsid:[{}], [{}]", actionData.wcMaster.getSid(), e.getMessage(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 流程推進
        // ====================================
        this.wcManagerSignInfoManager.changeWCMasterStatus(wcSid, execUserSid);

        // ====================================
        // 註記已送件邏輯 WORKCOMMU-503
        // ====================================
        // 送件時間
        WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
        if (wcMaster.getSendDate() == null) {
            this.wcMasterManager.updateSendDate(wcSid, sendDate);
        }
        // 判斷是否註記已送件
        this.wcMasterManager.processUpdateSended(wcSid, execUserSid);

    }

    /**
     * 需求流程-復原
     *
     * @param wcSid       工作聯絡單Sid
     * @param execUserSid 執行者 sid
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doReqFlowRecovery(String wcSid, Integer execUserSid) throws UserMessageException {

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ReqFlowActionData actionData = this.prepareReqSignActionData(wcSid, execUserSid, true);

        // 是否為已核准復原
        boolean isApprovedRecovery = BpmStatus.APPROVED.equals(actionData.reqSignInfo.getStatus());

        // ====================================
        // 執行權限檢核
        // ====================================
        boolean isCanRecovery = false;
        if (isApprovedRecovery) {
            // 已核准-復原
            isCanRecovery = this.wcManagerSignInfoManager.isShowApprovedRecovery(
                    actionData.execUser.getId(),
                    actionData.processTasks,
                    wcSid);
        } else {
            // 一般復原
            isCanRecovery = this.wcManagerSignInfoManager.isShowRecovery(
                    actionData.execUser.getId(),
                    actionData.reqSignInfo.getStatus(),
                    actionData.processTasks);
        }

        if (!isCanRecovery) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行，因按鈕權限不符)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // BPM 復原
        // ====================================
        // 註記此功能非會簽復原
        boolean isContinueRecovery = false;
        // BPM 復原
        try {
            actionData.reqSignInfo = this.wcManagerSignInfoManager.recoveryBpm(
                    actionData.execUser,
                    actionData.reqSignInfo,
                    isContinueRecovery);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(簽核復原失敗)";
            log.error(errorMessage + e.getMessage(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 移除送件時間 WORKCOMMU-503
        // ====================================
        // 還未註記送件時，清空送件日期
        if (!actionData.wcMaster.isSended()) {
            this.wcMasterManager.updateSendDate(wcSid, null);
        }

        // ====================================
        // 已核准復原時，將執行資料刪除
        // ====================================
        if (isApprovedRecovery) {
            // 清除已建立的會簽流程
            this.wcManagerSignInfoManager.clearContinueSignInfo(wcSid, actionData.execUser);
            // 清除執行方主流程
            this.wcExecManagerSignInfoManager.deleteExecFlowSignInfo(wcSid, actionData.execUser);
            // 清除執行單位檔
            this.wcExecDepManager.stopExecDep(wcSid, actionData.execUser);
        }
    }

    /**
     * 需求流程-退回
     *
     * @param wcSid           工作聯絡單Sid
     * @param user            執行者
     * @param rollBackTaskSid 欲退回至的節點物件 sid
     * @param rollBackReason  退回理由
     * @throws UserMessageException 錯誤時拋出
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doReqFlowRollBack(
            String wcSid,
            Integer execUserSid,
            String rollBackTaskSid,
            String rollBackReason) throws UserMessageException {

        // ====================================
        // 資料檢查
        // ====================================
        if (WkStringUtils.isEmpty(rollBackReason)) {
            throw new UserMessageException("請填寫退回原因！", InfomationLevel.WARN);
        }

        if (WkStringUtils.isEmpty(rollBackTaskSid)) {
            throw new UserMessageException("未指定退回人員！", InfomationLevel.WARN);
        }

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ReqFlowActionData actionData = this.prepareReqSignActionData(wcSid, execUserSid, false);

        // ====================================
        // 檢查可使用狀態
        // ====================================
        boolean isCanRollBack = this.wcManagerSignInfoManager.isShowRollBack(
                actionData.execUser.getId(),
                actionData.reqSignInfo.getBpmId(),
                actionData.processTasks);
        if (!isCanRollBack) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行，因按鈕權限不符)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得要退回的節點
        // ====================================
        ProcessTaskHistory rollBackTask = null;
        for (ProcessTaskBase processTaskBase : actionData.processTasks) {
            if (processTaskBase instanceof ProcessTaskHistory) {
                if (WkCommonUtils.compareByStr(rollBackTaskSid, processTaskBase.getSid())) {
                    rollBackTask = (ProcessTaskHistory) processTaskBase;
                }
            }
        }
        if (rollBackTask == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行，因流程已異動)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 退回
        // ====================================
        this.wcManagerSignInfoManager.doRollBack(
                actionData.execUser.getId(),
                actionData.reqSignInfo,
                rollBackTask,
                rollBackReason,
                ActionType.REQ_FLOW_ROLLBACK);

        // ====================================
        // 寫追蹤
        // ====================================
        User rollbackTargetUser = WkUserCache.getInstance().findById(rollBackTask.getExecutorID());

        // 建立追蹤檔資料
        WCTrace wcTrace = this.wcTraceCustomLogicManager.prepareForRollBackToReqFlow(
                wcSid,
                actionData.wcMaster.getWc_no(),
                rollBackReason,
                rollBackTask.getExecutorRoleName(),
                rollbackTargetUser.getSid(),
                ActionType.REQ_FLOW_ROLLBACK);

        this.wcTraceManager.create(wcTrace, actionData.execUser.getSid());

        // ====================================
        // 處理註記已送件
        // ====================================
        this.wcMasterManager.processUpdateSended(wcSid, execUserSid);
    }

    /**
     * 執行需求單位流程作廢
     *
     * @param wcSid        工作聯絡單Sid
     * @param execUserSid  執行者 sid
     * @param invalidReson 作廢原因
     * @throws UserMessageException 失敗時拋出
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doReqFlowInvalid(String wcSid, Integer execUserSid, String invalidReson) throws UserMessageException {

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ReqFlowActionData actionData = this.prepareReqSignActionData(wcSid, execUserSid, false);

        // ====================================
        // 檢查可使用狀態
        // ====================================
        boolean isCanInvalid = this.wcManagerSignInfoManager.isShowInvalid(
                actionData.execUser.getId(),
                actionData.reqSignInfo.getStatus(),
                actionData.reqSignInfo.getBpmId(),
                actionData.processTasks);
        if (!isCanInvalid) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行，因按鈕權限不符)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // BPM 流程作廢
        // ====================================
        try {
            this.bpmManager.invalid(actionData.execUser.getId(), actionData.reqSignInfo.getBpmId());
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(簽核流程作廢失敗)";
            log.error(errorMessage + " bpmID:[{}]", actionData.reqSignInfo.getBpmId());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新流程(sign info)BPM 狀態 (作廢)
        // ====================================
        this.wcManagerSignInfoManager.updateBpmStatus(
                actionData.execUser.getId(),
                actionData.reqSignInfo,
                BpmStatus.INVALID);

        // ====================================
        // 更新主檔狀態 （作廢）
        // ====================================
        // native SQL update
        this.wcMasterCustomLogicManager.updateWCStatus(
                actionData.wcMaster.getSid(),
                execUserSid,
                WCStatus.INVALID);

        // 更新entity 快取
        this.entityManager.refresh(actionData.wcMaster);

        // ====================================
        // 寫追蹤
        // ====================================
        WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_INVAILD(
                actionData.wcMaster.getSid(),
                actionData.wcMaster.getWc_no(),
                invalidReson);

        this.wcTraceManager.create(wcTrace, execUserSid);

        // ====================================
        // 寫作廢原因
        // ====================================
        this.wcOpinionDescripeSettingManager.saveInvailReason(
                ManagerSingInfoType.MANAGERSIGNINFO,
                actionData.reqSignInfo.getSid(),
                actionData.execUser.getSid(),
                invalidReson,
                actionData.reqSignInfo.getWcSid());
    }

    // ========================================================================
    // 主方法 - 需求單位會簽流程
    // ========================================================================
    /**
     * 需求會簽流程-簽名
     *
     * @param wcSid       單據 sid
     * @param execUserSid 執行者 sid
     * @throws UserMessageException 錯誤時拋出
     */
    // @Transactional(rollbackForClassName = { "Exception" }) 避免單筆失敗 roll back 已成功的 (已簽單據無法被復原)
    public void doReqflowCounterSign(String wcSid, Integer execUserSid) throws UserMessageException {

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ReqFlowActionData actionData = this.prepareReqSignActionData(wcSid, execUserSid, false);

        // ====================================
        // 查詢『待簽』的會簽流程 （主要會簽者、被會簽者代理人）
        // ====================================
        // 查詢所有會簽
        List<WCManagerSignInfo> counterSignInfos = this.wcManagerSignInfoManager.findReqFlowCounterSignInfos(wcSid);
        // 比對傳入使用者待簽的流程
        List<WCManagerSignInfo> canSignInfos = Lists.newArrayList();
        for (WCManagerSignInfo wcManagerSignInfo : counterSignInfos) {
            try {
                if (this.bpmManager.isSignUserWithException(actionData.execUser.getId(), wcManagerSignInfo.getBpmId())) {
                    canSignInfos.add(wcManagerSignInfo);
                }
            } catch (SystemOperationException e) {
                throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
            }
        }

        if (WkStringUtils.isEmpty(canSignInfos)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行，因按鈕權限不符)";
            log.error(errorMessage + " user 沒有待簽的會簽流程 userID:[{}]", actionData.execUser.getId());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 逐筆簽核 (一筆 exception 即中止)
        // ====================================
        for (WCManagerSignInfo wcManagerSignInfo : canSignInfos) {
            try {
                this.wcManagerSignInfoManager.onlyDoBPMSign(wcManagerSignInfo, actionData.execUser);
            } catch (Exception e) {
                String message = WkMessage.PROCESS_FAILED;
                log.error(message + "bpmID[{}],[{}],",
                        wcManagerSignInfo.getBpmId(),
                        e.getMessage(),
                        e);
                throw new UserMessageException(message, InfomationLevel.ERROR);
            }
        }

        // ====================================
        // 流程推進
        // ====================================
        this.wcManagerSignInfoManager.changeWCMasterStatus(wcSid, execUserSid);

        // ====================================
        // 處理註記已送件
        // ====================================
        this.wcMasterManager.processUpdateSended(wcSid, execUserSid);
    }

    /**
     * 需求會簽流程-復原
     *
     * @param wcSid       工作聯絡單Sid
     * @param execUserSid 執行者 sid
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doReqflowCounterSignRecovery(String wcSid, Integer execUserSid) throws UserMessageException {

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ReqFlowActionData actionData = this.prepareReqSignActionData(wcSid, execUserSid, true);

        // ====================================
        // 查詢使用者可簽的會簽流程
        // ====================================
        // 查詢所有active的會簽流程
        List<WCManagerSignInfo> allCounterSignInfos = this.wcManagerSignInfoManager.findReqFlowCounterSignInfos(wcSid);

        // 查詢執行者復原的流程
        List<WCManagerSignInfo> canRecoverySignInfos = Lists.newArrayList();
        for (WCManagerSignInfo wcManagerSignInfo : allCounterSignInfos) {
            // 查詢水管圖
            List<ProcessTaskBase> pbs = bpmManager.findSimulationChart(
                    actionData.execUser.getId(),
                    wcManagerSignInfo.getBpmId());
            // 判斷該流程是否可復原
            boolean isCanContinueRecovery = this.wcManagerSignInfoManager.isShowContinueRecovery(
                    actionData.execUser,
                    pbs,
                    wcSid);

            if (isCanContinueRecovery) {
                canRecoverySignInfos.add(wcManagerSignInfo);
            }
        }

        if (WkStringUtils.isEmpty(canRecoverySignInfos)) {
            String errorMessage = WkMessage.NEED_RELOAD + "([" + ActionType.REQ_FLOW_COUNTER_RECOVERY + "]無法執行，因按鈕權限不符)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // BPM復原
        // ====================================
        // 標註為會簽復原
        boolean isContinueRecovery = true;
        for (WCManagerSignInfo counterSignInfo : canRecoverySignInfos) {
            try {
                counterSignInfo = this.wcManagerSignInfoManager.recoveryBpm(
                        actionData.execUser,
                        counterSignInfo,
                        isContinueRecovery);
            } catch (Exception e) {
                log.error("復原會簽流程失敗(wc_manager_sign_info)! sid:[" + counterSignInfo.getSid() + "] [" + e.getMessage() + "]", e);
            }
        }

        // ====================================
        // 移除後續流程的資料
        // ====================================
        // 清除執行方主流程
        this.wcExecManagerSignInfoManager.deleteExecFlowSignInfo(wcSid, actionData.execUser);
        // 清除執行單位檔
        this.wcExecDepManager.stopExecDep(wcSid, actionData.execUser);
    }

    // ========================================================================
    // 內部方法
    // ========================================================================

    @AllArgsConstructor
    private class ReqFlowActionData {
        public User execUser;
        public WCMaster wcMaster;
        public WCManagerSignInfo reqSignInfo;
        public List<ProcessTaskBase> processTasks;
    }

    /**
     * 準備簽核用的資料
     * 
     * @param wcSid       主檔 sid
     * @param execUserSid 執行者 sid
     * @param isRecovery  為復原
     * @return
     * @throws UserMessageException 錯誤時拋出
     */
    private ReqFlowActionData prepareReqSignActionData(
            String wcSid,
            Integer execUserSid,
            boolean isRecovery) throws UserMessageException {

        // ====================================
        // 執行者
        // ====================================
        User execUser = WkUserCache.getInstance().findBySid(execUserSid);
        if (execUser == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(執行者資料不存在)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得主檔資料
        // ====================================
        WCMaster wcMaster = wcMasterManager.findBySid(wcSid);

        if (wcMaster == null || wcMaster.getWc_status() == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(主檔資料不存在 sid:[" + wcSid + "])";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 狀態檢查
        // ====================================
        this.wcMasterHelper.checkReqFlowSignWCMasterStatus(wcMaster, isRecovery);

        // ====================================
        // 取得需求單位主簽核流程資料
        // ====================================
        WCManagerSignInfo signInfo = null;
        try {
            Optional<WCManagerSignInfo> optional = this.wcManagerSignInfoManager.findReqFlowSignInfo(wcSid);
            if (optional.isPresent()) {
                signInfo = optional.get();
            }
        } catch (SystemOperationException e) {
            throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
        }

        if (signInfo == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(查無需求單位簽核資訊)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得水管圖
        // ====================================
        List<ProcessTaskBase> pbs = bpmManager.findSimulationChart(
                execUser.getId(),
                signInfo.getBpmId());

        if (WkStringUtils.isEmpty(pbs)) {
            String errorMessage = WkMessage.EXECTION + "(無法取得BPM簽核流程資料BPMID:[" + signInfo.getBpmId() + "])";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 組資料
        // ====================================
        return new ReqFlowActionData(execUser, wcMaster, signInfo, pbs);
    }
}
