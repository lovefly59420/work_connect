/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WorkParamManager;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 待分派單據邏輯
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class WaitAssignLogic implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8729909306771143661L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WaitAssignLogic instance;

    public static WaitAssignLogic getInstance() { return instance; }

    private static void setInstance(WaitAssignLogic instance) { WaitAssignLogic.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;

    // ========================================================================
    // 私有 model
    // ========================================================================
    @AllArgsConstructor
    @Getter
    @Setter
    private class WaitAssignResultModel {
        private String execDepSid;
        private String wcSid;
    }

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 查詢登入者所有可以分派的執行單位檔 sid (wc_exec_dep.wc_exec_dep_sid)
     * 
     * @param loginUserSid 登入者 sid
     * @return wc_exec_dep_sid list
     */
    public Set<String> findUserCanAssignWcExecDepSids(Integer loginUserSid) {
        return this.findCanAssignWcExecDepsByUser("", loginUserSid)
                .stream()
                .map(WaitAssignResultModel::getExecDepSid)
                .collect(Collectors.toSet());
    }

    /**
     * 查詢登入者所有可以分派的執行單位檔 sid (wc_exec_dep.wc_exec_dep_sid)
     * 
     * @param loginUserSid 登入者 sid
     * @param wcSid        主單 sid
     * @return wc_exec_dep_sid list
     */
    public Set<String> findUserCanAssignWcExecDepSids(Integer loginUserSid, String wcSid) {
        return this.findCanAssignWcExecDepsByUser(wcSid, loginUserSid)
                .stream()
                .map(WaitAssignResultModel::getExecDepSid)
                .collect(Collectors.toSet());
    }

    /**
     * 查詢可分派的單據數量
     * 
     * @param loginUserSid
     * @return
     */
    public long queryCanAssignCount(Integer loginUserSid) {
        return this.findCanAssignWcExecDepsByUser("", loginUserSid)
                .stream()
                // 查詢可分派的執行單位後，以主單做 distinct
                .map(WaitAssignResultModel::getWcSid)
                .distinct()
                .count();
    }

    // ========================================================================
    // 核心方法
    // ========================================================================
    /**
     * 查詢『可派工』的『待派工』執行單位
     * 
     * @param wcSid         主檔 sid
     * @param targetUserSid 比對 user sid
     * @return WaitAssignResultModel list
     */
    public List<WaitAssignResultModel> findCanAssignWcExecDepsByUser(
            String wcSid,
            Integer targetUserSid) {

        // ====================================
        // 取得可分派部門 (管理的單位)
        // ====================================
        // 僅管理的單位可以分派. 不含以下
        Set<Integer> loginUserManagerDepSids = RelationDepHelper.findRelationOrgSidsForExecCanAssign(targetUserSid);
        String canAssignDepsStr = loginUserManagerDepSids.stream()
                .map(canAssignDepsSid -> canAssignDepsSid + "")
                .collect(Collectors.joining(", "));

        // ====================================
        // 兜組SQL
        // ====================================
        // 條件1. 執行單位檔非停用(status=0) , 執行單位檔狀態為待分派 (exec_dep_status = 'WAITSEND')
        // 條件2. 有指定派工人員時，讀取派工人員名單
        // 條件3. 有指定指派單位時，讀取指派人員名單
        // 條件4. 未指定派工人員時，取取登入者可派工單位 (主副主管)
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT execDep.wc_exec_dep_sid, ");
        sql.append("       execDep.wc_sid, ");
        sql.append("       execDep.wc_exec_dep_setting_sid, ");
        sql.append("       wc.theme, ");
        sql.append("       execDep.exec_dep_create_type ");
        sql.append("FROM   wc_exec_dep execDep ");
        sql.append("       INNER JOIN wc_master wc ");
        sql.append("              ON wc.wc_sid = execDep.wc_sid ");
        sql.append("       LEFT  JOIN wc_exec_dep_setting exeDepSetting ");
        sql.append("              ON exeDepSetting.wc_exec_dep_setting_sid = execDep.wc_exec_dep_setting_sid ");

        sql.append("WHERE  execDep.status = 0 ");

        // 特定單據 (不指定時查詢全部)
        if (WkStringUtils.notEmpty(wcSid)) {
            sql.append("       AND execDep.wc_sid = '" + wcSid + "' ");
        }
        // 待派工
        sql.append("       AND execDep.exec_dep_status = 'WAITSEND' ");

        // 為派工模式 (不考慮加派，加派單位不會有派工)
        // sql.append(" AND exeDepSetting.isNeedAssigned = 1 ");

        sql.append("       AND (  ");
        // 需讀取派工設定時，讀取派工人員、部門名單(為JSON格式，撈出來之後再判斷)
        sql.append("               (exec_dep_create_type = 'SYSTEM' AND  exeDepSetting.isReadAssignSetting = 1) ");

        // 不需讀取派工設定時，直些比對登入者是否可分派該單位(管理單位不含以下)
        if (WkStringUtils.notEmpty(canAssignDepsStr)) {
            sql.append("            OR (  ");
            sql.append("                    (  ");
            // 為執行單位且無需讀取分派設定，或加派單位時，直接比對登入者有權限分派單位的
            sql.append("                        (exec_dep_create_type = 'SYSTEM' AND  exeDepSetting.isReadAssignSetting = 0) ");
            sql.append("                        OR exec_dep_create_type = 'CUSTOMER'  "); // for 加派單位 WORKCOMMU-559 【加派單位】加派單位執行流程調整，一律改為不簽核、需派工模式
            sql.append("                    )  ");
            sql.append("                    AND execDep.dep_sid IN (" + canAssignDepsStr + ")  ");
            sql.append("               )  ");
        }
        sql.append("       ) ");

        sql.append(" ORDER BY execDep.create_dt ");

        if (WorkParamManager.getInstance().isSHOW_DEBUG_FOR_WAIT_ASSIGN()
                || WorkParamManager.getInstance().isShowSearchSql()) {
            log.debug("查詢可分派執行單位"
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 查詢
        // ====================================
        List<Map<String, Object>> dbRowDataMapList = null;
        try {
            dbRowDataMapList = jdbc.queryForList(sql.toString());

            if (WkStringUtils.isEmpty(dbRowDataMapList)) {
                return Lists.newArrayList();
            }
        } catch (Exception e) {
            log.error("查詢可分派執行單位失敗!" + e.getMessage(), e);
            log.error(""
                    + "\r\nSQL:【\r\n" + new org.hibernate.engine.jdbc.internal.BasicFormatterImpl().format(sql.toString()) + "\r\n】"
                    + "\r\n");
            return Lists.newArrayList();
        }

        if (WorkParamManager.getInstance().isSHOW_DEBUG_FOR_WAIT_ASSIGN()) {
            log.debug("查詢可分派部門"
                    + "\r\nSQL:【\r\n" + WkJsonUtils.getInstance().toPettyJson(dbRowDataMapList) + "\r\n】"
                    + "\r\n");
        }

        // ====================================
        // 收集user可分派資料
        // ====================================
        List<WaitAssignResultModel> canAssignExecDeps = Lists.newArrayList();
        // 逐筆判斷
        // 需讀取分派設定人員或部門時，額外判斷，其餘一律加入(SQL已判斷)
        for (Map<String, Object> dbRowDataMap : dbRowDataMapList) {

            // ====================================
            // 判斷有指定派工人員的執行單位
            // ====================================
            // 執行單位設定檔ID
            String execDepSettingSid = dbRowDataMap.get("wc_exec_dep_setting_sid") + "";

            if (WkStringUtils.notEmpty(execDepSettingSid)) {
                // 取得對應執行單位設定檔
                WCExecDepSetting execDepSetting = this.wcExecDepSettingManager.findBySidFromCache(execDepSettingSid);

                // 有指定派工人員時才判斷
                if (execDepSetting != null
                        && execDepSetting.isReadAssignSetting()) {

                    // 判斷不是指定的指派人員, 跳過
                    if (!ExecDepLogic.getInstance().isDesignateAssignUser(execDepSetting, targetUserSid)) {
                        continue;
                    }
                }
            }

            // ====================================
            // 封裝
            // ====================================
            // 取得執行單位檔sid
            String currExecDepSid = dbRowDataMap.get("wc_exec_dep_sid") + "";
            // 取得主單 sid
            String currWcSid = dbRowDataMap.get("wc_sid") + "";

            // 加入結果 list
            canAssignExecDeps.add(
                    new WaitAssignResultModel(
                            currExecDepSid,
                            currWcSid));

        }

        if (WorkParamManager.getInstance().isSHOW_DEBUG_FOR_WAIT_ASSIGN()) {
            log.debug("查詢可分派部門"
                    + "\r\nSQL:【\r\n" + WkJsonUtils.getInstance().toPettyJson(canAssignExecDeps) + "\r\n】"
                    + "\r\n");
        }

        return canAssignExecDeps;
    }

}
