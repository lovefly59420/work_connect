/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignType;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SignFlowForExecDepLogic implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8588154534928518729L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SignFlowForExecDepLogic instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static SignFlowForExecDepLogic getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(SignFlowForExecDepLogic instance) { SignFlowForExecDepLogic.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient BpmManager bpmManager;
    @Autowired
    private transient WCExecManagerSignInfoManager wcExecManagerSignInfoManager;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 為執行單位[主流程或會簽]流程，待簽或或已簽人員
     * 
     * @param wcSid
     * @param targetUserSid
     * @return
     */
    public boolean isExecDepFlowCanSignOrSignedUser(String wcSid, Integer targetUserSid) {

        // ====================================
        // 取得 user 資料
        // ====================================
        User targetUser = WkUserCache.getInstance().findBySid(targetUserSid);
        if (targetUser == null) {
            return false;
        }

        // ====================================
        // 查詢所有執行方流程
        // ====================================
        //
        List<WCExecManagerSignInfo> execFlowSignInfos = this.wcExecManagerSignInfoManager.findActiveExecFlowSignInfos(wcSid);
        if (WkStringUtils.isEmpty(execFlowSignInfos)) {
            return false;
        }

        // ====================================
        // 檢查是否為需求方簽核流程待簽者
        // ====================================
        try {
            if (this.bpmManager.isExecFlowSignUser(targetUser.getId(), execFlowSignInfos)) {
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        // ====================================
        // 檢查為已簽名者
        // ====================================
        // 收集 instanceId
        List<String> instanceIds = execFlowSignInfos.stream()
                .map(WCExecManagerSignInfo::getBpmId)
                .collect(Collectors.toList());

        // 取得所有水管圖
        Map<String, List<ProcessTaskHistory>> processTasksMapByinstanceId = this.bpmManager.findSimulationCharts(
                instanceIds);

        // 比對經簽核者
        for (List<ProcessTaskHistory> processTaskHistorys : processTasksMapByinstanceId.values()) {
            for (ProcessTaskHistory processTask : processTaskHistorys) {
                if (WkCommonUtils.compareByStr(targetUser.getId(), processTask.getExecutorID())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 為執行單位簽核人員，且為已簽名
     * 
     * @param wcSid
     * @param targetUserSid
     * @return
     */
    public boolean isExecDepFlowMainSignedUser(String wcSid, Integer targetUserSid) {

        // ====================================
        // 取得 user 資料
        // ====================================
        User targetUser = WkUserCache.getInstance().findBySid(targetUserSid);
        if (targetUser == null) {
            return false;
        }

        // ====================================
        // 為簽核人員，且為已簽名者
        // ====================================
        // 查詢所有執行方簽核流程
        List<WCExecManagerSignInfo> execFlowSignInfos = this.wcExecManagerSignInfoManager.findExecFlowSignInfos(wcSid);
        // 過濾狀態需為核准
        execFlowSignInfos = execFlowSignInfos.stream()
                .filter(signInfo -> BpmStatus.APPROVED.equals(signInfo.getStatus()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(execFlowSignInfos)) {
            return false;
        }

        // ====================================
        // 檢查為已簽名者
        // ====================================
        // 收集 instanceId
        List<String> instanceIds = execFlowSignInfos.stream()
                .map(WCExecManagerSignInfo::getBpmId)
                .collect(Collectors.toList());

        // 取得所有水管圖
        Map<String, List<ProcessTaskHistory>> processTasksMapByinstanceId = this.bpmManager.findSimulationCharts(
                instanceIds);

        // 比對經簽核者
        for (List<ProcessTaskHistory> processTaskHistorys : processTasksMapByinstanceId.values()) {
            for (ProcessTaskHistory processTask : processTaskHistorys) {
                if (WkCommonUtils.compareByStr(targetUser.getId(), processTask.getExecutorID())) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isCanSignBtn(String wc_SID, Integer targetUserSid, SignType signType) {
        String targetUserID = WkUserUtils.findUserIDBySid(targetUserSid);
        if (WkStringUtils.isEmpty(targetUserID)) {
            return false;
        }

        try {
            List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoManager.getGroupManagerSignInfo(wc_SID);
            for (GroupManagerSignInfo gi : groupManagerSignInfos) {
                for (SingleManagerSignInfo si : gi.getSingleManagerSignInfos()) {
                    if (signType != null && !signType.equals(si.getSignType())) {
                        continue;
                    }
                    List<String> ids = BpmManager.getInstance().findTaskCanSignUserIds(si.getInstanceID());
                    if (ids.contains(targetUserID)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            log.warn("isCanSignBtn ERROR", e);
        }
        return false;
    }

}
