/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.query.WCMasterQueryService;
import com.cy.work.connect.repository.WCManagerSignInfoDetailRepository;
import com.cy.work.connect.vo.WCManagerSignInfoDetail;
import com.cy.work.connect.vo.enums.SignActionType;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 需求單位簽核過程Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCManagerSignInfoDetailManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3957457530879752130L;

    @Autowired
    private WCManagerSignInfoDetailRepository wcManagerSignInfoDetailRepository;

    @Autowired
    @Qualifier("wcMasterQueryService")
    private WCMasterQueryService wcMasterQueryService;

    /**
     * 檢測該工作聯絡單是否該使用者有簽名過
     *
     * @param loginUserSid 登入者Sid
     * @param wcSid        工作聯絡單主檔Sid
     * @return
     */
    public boolean isHistorySigned(Integer loginUserSid, String wcSid) {
        try {
            Map<String, Object> parameters = Maps.newHashMap();
            StringBuilder builder = new StringBuilder();
            builder.append(" SELECT COUNT(*) as count FROM wc_exec_manager_sign_info_detail pcd ");
            builder.append(" INNER JOIN wc_exec_manager_sign_info ps ON ps.wc_exec_manager_sign_info_sid = pcd.wc_exec_manager_sign_info_sid ");
            builder.append(" AND ps.wc_sid ='" + wcSid + "' ");
            builder.append(" WHERE pcd.user_sid ='" + loginUserSid + "'");
            @SuppressWarnings("rawtypes")
            List result = wcMasterQueryService.findWithQuery(builder.toString(), parameters, null);
            Integer wcCheckSignedCount = 0;
            for (int i = 0; i < result.size(); i++) {
                wcCheckSignedCount = ((BigInteger) result.get(i)).intValue();
            }
            if (wcCheckSignedCount > 0) {
                return true;
            }
            builder = new StringBuilder();
            builder.append(" SELECT COUNT(*) as count FROM wc_manager_sign_info_detail pcd ");
            builder.append(" INNER JOIN wc_manager_sign_info ps ON ps.wc_manager_sign_info_sid = pcd.wc_manager_sign_info_sid ");
            builder.append(" AND ps.wc_sid ='" + wcSid + "' ");
            builder.append(" WHERE pcd.user_sid ='" + loginUserSid + "'");
            result = wcMasterQueryService.findWithQuery(builder.toString(), parameters, null);
            Integer wcSignedCount = 0;
            for (int i = 0; i < result.size(); i++) {
                wcSignedCount = ((BigInteger) result.get(i)).intValue();
            }
            if (wcSignedCount > 0) {
                return true;
            }
        } catch (Exception e) {
            log.warn("isHistorySigned ERROR", e);
        }
        return false;
    }

    /**
     * 更新簽名記錄
     *
     * @param wcManagerSignInfoSid 需求單位簽核物件 sid
     * @param tasks                水管圖資訊
     * @param signActionType
     * @param signUserSid
     */
    public void updateSignData(
            String wcManagerSignInfoSid,
            List<ProcessTaskBase> tasks,
            SignActionType signActionType,
            Integer signUserSid) {

        try {
            // ====================================
            // 將舊資料修改成無效資料
            // ====================================
            this.wcManagerSignInfoDetailRepository.updateStatus(
                    Activation.INACTIVE, wcManagerSignInfoSid);

            // ====================================
            // 由於復原及退回在單據歷史紀錄是無法查詢的,所以必須更新狀態
            // ====================================
            Set<SignActionType> targetSignActionTypes = Sets.newHashSet(
                    SignActionType.RECOVERY,
                    SignActionType.ROLLBACK);

            if (signUserSid != null
                    && targetSignActionTypes.contains(signActionType)) {
                this.wcManagerSignInfoDetailRepository.updateSignActionType(
                        signActionType, wcManagerSignInfoSid, signUserSid);
            }

            if (WkStringUtils.notEmpty(tasks)) {
                for (ProcessTaskBase task : tasks) {
                    try {
                        if (task instanceof ProcessTaskHistory) {
                            ProcessTaskHistory ph = (ProcessTaskHistory) task;
                            // 找執行者資料
                            User user = WkUserCache.getInstance().findById(ph.getExecutorID());
                            if (user == null) {
                                log.error("BPM 流程簽核者，找不到對應的 USER! sysID:[{}],executorID:[{}]",
                                        ph.getSysID(),
                                        ph.getExecutorID());
                                continue;
                            }
                            // 寫入
                            WCManagerSignInfoDetail wd = new WCManagerSignInfoDetail();
                            wd.setSign_dt(ph.getTaskEndTime());
                            wd.setStatus(Activation.ACTIVE);
                            wd.setUser_sid(user.getSid());
                            wd.setWc_manager_sign_info_sid(wcManagerSignInfoSid);
                            this.wcManagerSignInfoDetailRepository.save(wd);
                        }
                    } catch (Exception e) {
                        log.error("updateSignData ERROR", e);
                    }
                }

            }
        } catch (Exception e) {
            log.error("updateSignData ERROR", e);
        }
    }
}
