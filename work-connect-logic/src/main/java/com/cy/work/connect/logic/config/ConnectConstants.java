/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.config;

/**
 * 系統常數
 *
 * @author kasim
 */
public class ConnectConstants {

    /**
     * 呼叫及宣告JDBC TEMPLATE 用
     */
    public static final String CONNECT_JDBC_TEMPLATE = "CONNECT_JDBC_TEMPLATE";

    /**
     * 排程執行間隔時間 (間隔 30 分鐘一次)
     */
    public static final long SCHEDULED_FIXED_DELAY_TIME = 30 * 60 * 1000;
}
