/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCTransSpecialPermissionCache;
import com.cy.work.connect.logic.helper.RelationDepHelper;
import com.google.common.collect.Sets;

/**
 * 轉單特殊權限擁有部門Manager
 *
 * @author brain0925_liao
 */
@Component
public class WCTransSpecialPermissionManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1211954590489190676L;

    @Autowired
    private WCTransSpecialPermissionCache wcTransSpecialPermissionCache;

    /**
     * @param loginUserSid
     * @param execDepSid
     * @return
     */
    public boolean isSpecialPermission(
            Integer loginUserSid,
            Integer execDepSid) {

        // ====================================
        // 計算使用者的單位
        // ====================================
        Integer primaryOrgSid = WkUserCache.getInstance().findPrimaryOrgSidByUserSid(loginUserSid);
        if (primaryOrgSid == null) {
            return false;
        }
        Set<Integer> userDepSids = Sets.newHashSet(primaryOrgSid);
        // 兼職部門
        Set<Integer> managerOrgSids = WkOrgCache.getInstance().findManagerOrgSids(loginUserSid);
        if (WkStringUtils.notEmpty(managerOrgSids)) {
            userDepSids.addAll(managerOrgSids);
        }

        // ====================================
        // 比對是否為特殊轉單權限部門
        // ====================================
        // 取得特殊轉單單位設定
        Set<Integer> specDepSids = wcTransSpecialPermissionCache.getAllWCTransSpecialPermission();

        // 比對
        boolean isMacthSpecDep = userDepSids.stream()
                .anyMatch(userDepSid -> specDepSids.contains(userDepSid));

        if (!isMacthSpecDep) {
            return false;
        }

        // ====================================
        // 比對執行單位，是否為使用者相關的單位
        // ====================================
        // 執行單位以下所有成員
        Set<Integer> relationOrgSids = RelationDepHelper.findRelationOrgSidsForSpecialPermissionExecTransPerson(loginUserSid);
        return relationOrgSids.contains(execDepSid);
    }
}
