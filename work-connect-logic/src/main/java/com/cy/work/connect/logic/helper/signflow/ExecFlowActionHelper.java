package com.cy.work.connect.logic.helper.signflow;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.PermissionLogicForExecDep;
import com.cy.work.connect.logic.helper.WCExecManagerSignInfoCheckHelper;
import com.cy.work.connect.logic.helper.WCMasterHelper;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCExecRollBackHistoryManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.manager.WCTraceCustomLogicManager;
import com.cy.work.connect.logic.manager.WCTraceManager;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.ActionType;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 執行單位流程邏輯
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class ExecFlowActionHelper {
    // ========================================================================
    // 服務
    // ========================================================================
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private BpmManager bpmManager;
    @Autowired
    private WCMasterManager wcMasterManager;
    @Autowired
    private WCMasterHelper wcMasterHelper;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCExecManagerSignInfoCheckHelper execManagerSignInfoCheckHelper;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private WCExecRollBackHistoryManager wcExecRollBackHistoryManager;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;

    // ========================================================================
    //
    // ========================================================================
    /**
     * 執行單位-簽名
     *
     * @param wcSid       工作聯絡單Sid
     * @param execUserSid 執行者SID
     * @throws UserMessageException 錯誤時拋出
     */
    // @Transactional (transaction 不綁這邊,避免造成單一筆失敗，其他流程狀態也被退回)
    public void doExecFlowSign(String wcSid, Integer execUserSid) throws UserMessageException {

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ExecFlowActionData actionData = this.prepareExecSignActionData(wcSid, execUserSid);

        // ====================================
        // 查詢『待簽』的會簽流程 （主要會簽者、被會簽者代理人）
        // ====================================
        // 查詢所有簽核流程
        List<WCExecManagerSignInfo> activeSignInfos = this.wcExecManagerSignInfoManager.findActiveExecFlowSignInfos(wcSid);
        // 比對傳入使用者待簽的流程
        List<WCExecManagerSignInfo> canSignInfos = Lists.newArrayList();
        for (WCExecManagerSignInfo execSignInfo : activeSignInfos) {
            try {
                if (this.bpmManager.isSignUserWithException(actionData.execUser.getId(), execSignInfo.getBpmId())) {
                    canSignInfos.add(execSignInfo);
                }
            } catch (SystemOperationException e) {
                throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
            }
        }

        if (WkStringUtils.isEmpty(canSignInfos)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行" + ActionType.EXEC_FLOW_SIGN.getDescr() + "，因按鈕權限不符)";
            log.error(errorMessage + " user 沒有待簽的執行方流程 userID:[{}]", actionData.execUser.getId());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // BPM 簽名 + 推進執行單位狀態
        // ====================================
        for (WCExecManagerSignInfo canSignInfo : canSignInfos) {
            try {
                this.wcExecManagerSignInfoManager.doExecFlowSign(canSignInfo, actionData.execUser);
            } catch (UserMessageException e) {
                throw e;
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "(執行單位簽核失敗)";
                log.error(errorMessage + "執行單位簽核失敗 [" + e.getMessage() + "] ,[" + canSignInfo.getBpmId() + "]", e);
                throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
            }
        }
    }

    /**
     * 執行單位-簽名復原
     * 
     * @param wcSid       工作聯絡單Sid
     * @param execUserSid 執行者SID
     * @throws UserMessageException 錯誤時拋出
     */
    public void doExecFlowRecovery(String wcSid, Integer execUserSid) throws UserMessageException {
        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ExecFlowActionData actionData = this.prepareExecSignActionData(wcSid, execUserSid);

        // ====================================
        // 查詢所有簽核流程
        // ====================================
        List<WCExecManagerSignInfo> activeSignInfos = this.wcExecManagerSignInfoManager.findActiveExecFlowSignInfos(wcSid);

        // ====================================
        // 逐筆執行復原
        // ====================================
        boolean hasAnySigned = false;
        // 逐筆處理
        for (WCExecManagerSignInfo execSignInfo : activeSignInfos) {
            try {
                // 執行復原 (如果檢核都有通過的話)
                // 丟到另外一個 service 處理, roll back 時僅會 roll back一筆
                boolean isSigned = this.wcExecManagerSignInfoManager.doExecFlowRecovery(execSignInfo, actionData.execUser);
                // 註記有完成任何一筆
                if (isSigned) {
                    hasAnySigned = true;
                }
            } catch (Exception e) {
                String errorMessage = WkMessage.PROCESS_FAILED + "(執行單位簽核復原失敗)";
                log.error(errorMessage + " [" + e.getMessage() + "] ,[" + execSignInfo.getBpmId() + "]", e);
                throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
            }
        }

        if (!hasAnySigned) {
            String errorMessage = WkMessage.NEED_RELOAD + "(無法執行" + ActionType.EXEC_FLOW_RECOVERY.getDescr() + "，因按鈕權限不符)";
            log.error(errorMessage + "沒有可以復原的流程 userID:[{}]", actionData.execUser.getId());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
    }

    /**
     * 執行單位-簽核者退回
     *
     * @param wcSid              工作聯絡單Sid
     * @param loginUser          執行者
     * @param rollBackReqTaskSid 欲退回至需求方的節點 sid
     * @param rollBackReason     退回原因
     * @throws UserMessageException 錯誤時拋出
     */
    public void doExecFlowRollBack(
            String wcSid,
            Integer execUserSid,
            String rollBackReqTaskSid,
            String rollBackReason) throws UserMessageException {

        // ====================================
        // 資料檢查
        // ====================================
        if (WkStringUtils.isEmpty(rollBackReason)) {
            throw new UserMessageException("請填寫退回原因！", InfomationLevel.WARN);
        }

        if (WkStringUtils.isEmpty(rollBackReqTaskSid)) {
            throw new UserMessageException("未指定退回人員！", InfomationLevel.WARN);
        }

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ExecFlowActionData actionData = this.prepareExecSignActionData(wcSid, execUserSid);

        // ====================================
        // 檢查需求方是否已簽完
        // ====================================
        Optional<WCManagerSignInfo> optional = null;
        try {
            optional = this.wcManagerSignInfoManager.findReqFlowSignInfo(wcSid);
            if (!optional.isPresent()
                    || !BpmStatus.APPROVED.equals(optional.get().getStatus())) {
                String errorMessage = WkMessage.NEED_RELOAD + "(需求方尚未簽核完成)";
                log.warn(errorMessage + "wcSid:[{}]", wcSid);
                throw new UserMessageException(errorMessage, InfomationLevel.WARN);
            }

        } catch (SystemOperationException e) {
            throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
        }

        WCManagerSignInfo reqSignInfo = optional.get();

        // ====================================
        // 檢核『執行方狀態』是否允許退回需求方
        // ====================================
        // 不能有其他執行方已簽完或已執行
        boolean isCanRollBackToReq = this.execManagerSignInfoCheckHelper.isCanRollBackToReq(wcSid);
        if (!isCanRollBackToReq) {
            String errorMessage = WkMessage.NEED_RELOAD + "(執行方已簽核，或單位已開始執行)";
            log.warn(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 檢查自己是否可退回 (為任一主流程待簽人員)
        // ====================================
        // 查詢所有執行方簽核流程 (主流程)
        List<WCExecManagerSignInfo> execFlowSignInfos = this.wcExecManagerSignInfoManager.findExecFlowSignInfos(wcSid);

        // 檢查有任一筆為執行者待簽
        boolean hasAnyWaitSign = this.bpmManager.isExecFlowSignUser(
                actionData.execUser.getId(),
                execFlowSignInfos);

        if (!hasAnyWaitSign) {
            String errorMessage = WkMessage.NEED_RELOAD + "(比對非目前可退回人員)";
            log.warn(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 取得需求方退回節點
        // ====================================
        List<ProcessTaskBase> processTaskBases = this.bpmManager.findSimulationChart("", reqSignInfo.getBpmId());
        ProcessTaskHistory rollBackTask = null;
        for (ProcessTaskBase processTaskBase : processTaskBases) {
            if (processTaskBase instanceof ProcessTaskHistory) {
                // 比對退回節點 task sid
                if (WkCommonUtils.compareByStr(processTaskBase.getSid(), rollBackReqTaskSid)) {
                    rollBackTask = (ProcessTaskHistory) processTaskBase;
                    break;
                }
            }
        }

        if (rollBackTask == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(流程已異動)";
            log.warn(errorMessage + " bpmsid:[{}], rollReqTaskSid:[{}]",
                    reqSignInfo.getBpmId(),
                    rollBackReqTaskSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 執行退回需求方流程
        // ====================================
        this.execRollBackToReq(
                ActionType.EXEC_FLOW_SIGNER_ROLLBACK,
                reqSignInfo,
                rollBackTask,
                rollBackReason,
                actionData.execUser);
    }

    /**
     * 執行單位-執行單位成員退回
     *
     * @param wcSid          工作聯絡單Sid
     * @param execUserSid    登入者 sid
     * @param rollBackReason 欲退回至的節點物件 sid
     * @param rollBackReason 退回理由
     * @throws UserMessageException 錯誤時拋出
     */
    public void doExecFlowMemberRollBack(
            String wcSid,
            Integer execUserSid,
            String rollBackReqTaskSid,
            String rollBackReason) throws UserMessageException {

        // ====================================
        // 資料檢查
        // ====================================
        if (WkStringUtils.isEmpty(rollBackReason)) {
            throw new UserMessageException("請填寫退回原因！", InfomationLevel.WARN);
        }

        if (WkStringUtils.isEmpty(rollBackReqTaskSid)) {
            throw new UserMessageException("未指定退回人員！", InfomationLevel.WARN);
        }

        // ====================================
        // 取得需要的資料
        // ====================================
        ExecFlowActionData actionData = this.prepareExecSignActionData(wcSid, execUserSid);

        // ====================================
        // 檢查需求方是否已簽完
        // ====================================
        Optional<WCManagerSignInfo> optional = null;
        try {
            optional = this.wcManagerSignInfoManager.findReqFlowSignInfo(wcSid);
            if (!optional.isPresent()
                    || !BpmStatus.APPROVED.equals(optional.get().getStatus())) {
                String errorMessage = WkMessage.NEED_RELOAD + "(需求方尚未簽核完成)";
                log.warn(errorMessage + "wcSid:[{}]", wcSid);
                throw new UserMessageException(errorMessage, InfomationLevel.WARN);
            }

        } catch (SystemOperationException e) {
            throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
        }

        WCManagerSignInfo reqSignInfo = optional.get();

        // ====================================
        // 檢核『執行方狀態』是否允許退回需求方
        // ====================================
        // 不能有其他執行方已簽完或已執行
        boolean isCanDo = this.wcExecDepManager.isShowExecDepRollBackPermisison(wcSid, actionData.execUser);
        if (!isCanDo) {
            String errorMessage = WkMessage.NEED_RELOAD + "(狀態已異動)";
            log.warn(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 取得需求方退回節點
        // ====================================
        List<ProcessTaskBase> processTaskBases = this.bpmManager.findSimulationChart("", reqSignInfo.getBpmId());
        ProcessTaskHistory rollBackTask = null;
        for (ProcessTaskBase processTaskBase : processTaskBases) {
            if (processTaskBase instanceof ProcessTaskHistory) {
                // 比對退回節點 task sid
                if (WkCommonUtils.compareByStr(processTaskBase.getSid(), rollBackReqTaskSid)) {
                    rollBackTask = (ProcessTaskHistory) processTaskBase;
                    break;
                }
            }
        }

        if (rollBackTask == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(流程已異動)";
            log.warn(errorMessage + " bpmsid:[{}], rollReqTaskSid:[{}]",
                    reqSignInfo.getBpmId(),
                    rollBackReqTaskSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 執行退回需求方流程
        // ====================================
        this.execRollBackToReq(
                ActionType.EXEC_FLOW_MEMBER_ROLLBACK,
                reqSignInfo,
                rollBackTask,
                rollBackReason,
                actionData.execUser);

    }

    /**
     * 執行單位-簽核者不執行
     *
     * @param wcSid      工作聯絡單Sid
     * @param loginUser  執行者
     * @param stopReason 不執行原因
     * @throws UserMessageException 錯誤時拋出
     */
    public void doExecFlowSignerStop(
            String wcSid,
            Integer execUserSid,
            String stopReason) throws UserMessageException {

        // ====================================
        // 資料檢查
        // ====================================
        if (WkStringUtils.isEmpty(stopReason)) {
            throw new UserMessageException("請填寫原因！", InfomationLevel.WARN);
        }

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ExecFlowActionData actionData = this.prepareExecSignActionData(wcSid, execUserSid);

        // ====================================
        // 查詢所有執行者可簽的執行方簽核流程 (主流程)
        // ====================================
        List<WCExecManagerSignInfo> canSignExecFlowSignInfos = this.wcExecManagerSignInfoManager.findCanSignExecFlowSignInfos(
                wcSid, actionData.execUser.getId());

        // ====================================
        // 檢查可執行狀態
        // ====================================
        boolean isCanExecFlowSignerStop = PermissionLogicForExecDep.getInstance().isCanExecFlowSignerStop(wcSid, actionData.execUser);

        if (!isCanExecFlowSignerStop) {
            String errorMessage = WkMessage.NEED_RELOAD + "(狀態未符合)";
            log.warn(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 逐筆執行
        // ====================================
        for (WCExecManagerSignInfo execManagerSignInfo : canSignExecFlowSignInfos) {
            // 單筆失敗不影響其他筆 (僅單筆狀態被 roll back)
            try {
                this.wcExecManagerSignInfoManager.doExecFlowSignerStop(
                        execManagerSignInfo, stopReason, actionData.execUser);
            } catch (UserMessageException e) {
                // 已準備好的 exception 直接throw
                throw e;
            } catch (Exception e) {
                // 意料之外的 exception 轉包為 UserMessageException
                String errorMessage = WkMessage.EXECTION;
                log.warn(errorMessage + e.getMessage() + " wc_exec_manager_sign_info_sid:[{}]", execManagerSignInfo.getSid());
                throw new UserMessageException(errorMessage, e.getMessage(), InfomationLevel.ERROR);
            }
        }

        // ====================================
        // 塞入部門退回記錄,For 退件一覽表
        // ====================================
        this.wcExecRollBackHistoryManager.create(
                wcSid,
                actionData.execUser.getSid(),
                null); // 簽核者退回，執行部門都還沒有領單，故不寫

        // ====================================
        // 流程推進 (主檔狀態)
        // ====================================
        this.wcExecDepManager.changeToExecFinish(wcSid, actionData.execUser.getSid());
    }

    /**
     * 執行單位-單位成員不執行
     *
     * @param wcSid      工作聯絡單Sid
     * @param loginUser  執行者
     * @param stopReason 不執行原因
     * @throws UserMessageException 錯誤時拋出
     */
    public void doExecFlowMemberStop(
            String wcSid, Integer execUserSid, String stopReason) throws UserMessageException {

        // ====================================
        // 資料檢查
        // ====================================
        if (WkStringUtils.isEmpty(stopReason)) {
            throw new UserMessageException("請填寫原因！", InfomationLevel.WARN);
        }

        // ====================================
        // 取得需求流程需要的資料
        // ====================================
        ExecFlowActionData actionData = this.prepareExecSignActionData(wcSid, execUserSid);

        // ====================================
        // 檢查可執行狀態
        // ====================================
        boolean isCanExecFlowMemberStop = this.wcExecDepManager.isShowExecDepStopPermisison(wcSid, actionData.execUser);

        if (!isCanExecFlowMemberStop) {
            String errorMessage = WkMessage.NEED_RELOAD + "(狀態未符合)";
            log.warn(errorMessage + "wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }

        // ====================================
        // 將執行者為自己的執行單位改為 STOP
        // ====================================
        List<WCExceDep> exceDepsByCurrUser = this.wcExecDepManager.getWCExceDepByWcIDAndUserSid(
                wcSid,
                execUserSid);

        // 將狀態改為中止
        for (WCExceDep wcExceDep : exceDepsByCurrUser) {
            wcExceDep.setExecDepStatus(WCExceDepStatus.STOP);
            wcExceDep.setUpdate_usr(execUserSid);
            wcExceDep.setUpdate_dt(new Date());
        }

        // update
        this.wcExecDepManager.save(exceDepsByCurrUser);

        // ====================================
        // 寫追蹤
        // ====================================
        // 收集退回的執行單位 (不重複)
        List<Integer> stopExecDepSids = exceDepsByCurrUser.stream()
                .map(WCExceDep::getDep_sid)
                .distinct()
                .collect(Collectors.toList());

        // 建立追蹤檔資料
        WCTrace wcTrace = this.wcTraceCustomLogicManager.prepareForExecFlowMemberStop(
                wcSid,
                actionData.wcMaster.getWc_no(),
                stopExecDepSids,
                stopReason);

        // insert
        this.wcTraceManager.create(wcTrace, actionData.execUser.getSid());

        // ====================================
        // 流程推進 (主檔狀態)
        // ====================================
        this.wcExecDepManager.changeToExecFinish(wcSid, actionData.execUser.getSid());
    }

    // ========================================================================
    // 內部方法
    // ========================================================================

    @AllArgsConstructor
    private class ExecFlowActionData {
        public User execUser;
        public WCMaster wcMaster;
    }

    /**
     * 準備簽核用的資料
     * 
     * @param wcSid       主檔 sid
     * @param execUserSid 執行者 sid
     * @param isRecovery  為復原
     * @return
     * @throws UserMessageException 錯誤時拋出
     */
    private ExecFlowActionData prepareExecSignActionData(
            String wcSid,
            Integer execUserSid) throws UserMessageException {

        // ====================================
        // 執行者
        // ====================================
        User execUser = WkUserCache.getInstance().findBySid(execUserSid);
        if (execUser == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(執行者資料不存在)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得主檔資料
        // ====================================
        WCMaster wcMaster = wcMasterManager.findBySid(wcSid);

        if (wcMaster == null || wcMaster.getWc_status() == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(主檔資料不存在 sid:[" + wcSid + "])";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 狀態檢查
        // ====================================
        this.wcMasterHelper.checkWCMasterStatus(wcMaster);

        return new ExecFlowActionData(execUser, wcMaster);
    }

    /**
     * 執行方退回需求方
     * 
     * @param actionType     動作類型
     * @param reqSignInfo    需求方主簽核資料
     * @param rollBackTask   退回節點
     * @param rollBackReason 退回原因
     * @param execUser       執行者
     * @throws UserMessageException 錯誤時拋出
     */
    private void execRollBackToReq(
            ActionType actionType,
            WCManagerSignInfo reqSignInfo,
            ProcessTaskHistory rollBackTask,
            String rollBackReason,
            User execUser) throws UserMessageException {

        String wcSid = reqSignInfo.getWcSid();

        // ====================================
        // 收集已領單單位 for 退件一覽表
        // ====================================
        // 有判斷狀態，需在執行單位刪除前執行
        Set<Integer> receiveExecDepSids = this.wcExecDepManager.prepareReceiveExecDepSidForExecRollBack(wcSid);

        // ====================================
        // 處理需求方資訊
        // ====================================
        // 需求單位流程退回
        this.wcManagerSignInfoManager.doRollBack(
                execUser.getId(),
                reqSignInfo,
                rollBackTask,
                rollBackReason,
                actionType);

        // 清除需求單位會簽
        this.wcManagerSignInfoManager.clearContinueSignInfo(wcSid, execUser);

        // ====================================
        // 處理執行方資訊
        // ====================================
        // 刪除執行單位流程簽核資訊
        this.wcExecManagerSignInfoManager.deleteExecFlowSignInfo(wcSid, execUser);

        // 清除執行單位會簽流程
        this.wcExecManagerSignInfoManager.clearContinueSignInfo(wcSid, execUser);

        // 刪除執行單位物件
        this.wcExecDepManager.stopExecDep(wcSid, execUser);

        // ====================================
        // 塞入部門退回記錄,For 退件一覽表
        // ====================================
        this.wcExecRollBackHistoryManager.create(
                wcSid,
                execUser.getSid(),
                Lists.newArrayList(receiveExecDepSids));

        // ====================================
        // 寫入追蹤
        // ====================================
        // 取得退回目標使用者資料
        User rollbackTargetUser = WkUserCache.getInstance().findById(rollBackTask.getExecutorID());
        // 建立追蹤檔資料
        WCTrace wcTrace = this.wcTraceCustomLogicManager.prepareForRollBackToReqFlow(
                wcSid,
                reqSignInfo.getWcNo(),
                rollBackReason,
                rollBackTask.getExecutorRoleName(),
                rollbackTargetUser.getSid(),
                actionType);
        // insert
        this.wcTraceManager.create(wcTrace, execUser.getSid());

    }

}
