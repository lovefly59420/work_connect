/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCConfigTempletManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCTagManager;
import com.cy.work.connect.vo.WCConfigTemplet;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.converter.to.UserTo;
import com.cy.work.connect.vo.enums.ConfigTempletType;
import com.cy.work.connect.vo.utils.WorkConnectVOUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 中類 helper
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class MenuTagHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 783964474171635455L;
    /**
     * 執行單位設定Manager
     */
    @Autowired
    private WCExecDepSettingManager wcExecDepSettingManager;

    @Autowired
    private WCTagManager wcTagManager;

    /**
     * 檢測是否顯示中類
     *
     * @param wcMenuTag            中類物件
     * @param wcConfigTemplets     所有設定模版
     * @param loginUserMatchDepSid 登入單位Sid & 管理部門Sid
     * @param loginUserSid         登入者Sid
     * @param loginCompSid         登入公司Sid
     * @return
     */
    public boolean checkCanShowMenuItem(
            WCMenuTag wcMenuTag,
            final List<WCConfigTemplet> wcConfigTemplets,
            final Set<Integer> loginUserMatchDepSid,
            Integer loginUserSid,
            Integer loginCompSid,
            final List<Org> allCompActiveDeps) {

        // ====================================
        // 檢測中類可使用"人員"
        // ====================================
        if (wcMenuTag.getUseUserSids().contains(loginUserSid)) {
            return true;
        }

        // ====================================
        // 檢測可使用"部門"
        // ====================================
        // 取得部門設定模版 (類別:單據名稱-可使用部門)
        // 不是模版時，會回傳 null
        WCConfigTemplet depTemplet = this.getTempletBySid(
                wcMenuTag.getUseDepTempletSid(),
                wcConfigTemplets,
                ConfigTempletType.MENU_TAG_CAN_USE);

        // 登入者部門是否可顯示
        boolean isDepUse = this.isCanUseDep(
                loginUserMatchDepSid,
                loginUserSid,
                loginCompSid,
                wcMenuTag.getUseDep(),
                wcMenuTag.isUserContainFollowing(),
                wcMenuTag.isOnlyForUseDepManager(),
                depTemplet,
                allCompActiveDeps);

        // 非中類可使用部門
        if (!isDepUse) {
            return false;
        }

        // ====================================
        // 檢查中類下的執行項目是否設定可顯示
        // ====================================
        // 查詢中類下的所有項目
        List<WCTag> tags = wcTagManager.getWCTagForManager(
                wcMenuTag.getSid(),
                Activation.ACTIVE);

        // 無執行項目的中類不可使用
        if (WkStringUtils.isEmpty(tags)) {
            return false;
        }

        for (WCTag tag : tags) {
            // 依次檢查可用的小類
            if (this.checkCanShowTagItem(
                    tag,
                    wcConfigTemplets,
                    loginUserMatchDepSid,
                    loginUserSid,
                    loginCompSid,
                    allCompActiveDeps)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 依據模版類型及Sid取得設定模版
     *
     * @param templetSid
     * @param wcConfigTemplets
     * @param templetType
     * @return
     */
    private WCConfigTemplet getTempletBySid(
            Long templetSid,
            List<WCConfigTemplet> wcConfigTemplets,
            ConfigTempletType templetType) {

        if (templetSid == null) {
            return null;
        }

        // 依據SID取得模版設定檔
        Optional<WCConfigTemplet> optional = wcConfigTemplets.stream()
                .filter(templet -> {
                    return templet != null
                            && templetType.equals(templet.getTempletType())
                            && templet.getSid() == templetSid;
                })
                .findFirst();

        if (optional.isPresent()) {
            return optional.get();
        }

        log.error("[" + templetType.getDescr() + "] 找不到對應的 WcConfigTemplet : [" + templetSid + "]");
        return null;
    }

    /**
     * 是否為可使用單位
     * 
     * @param loginUserMatchDepSids       登入單位Sid & 管理部門Sid
     * @param loginUserSid                登入者 SID
     * @param loginCompSid                登入公司Sid
     * @param canUseDep                   可使用單位(自行設定)
     * @param isCanUseDepContainFollowing 可使用單位(自行設定-含以下)
     * @param isOnlyUseDepManager         僅主管可用
     * @param depTemplet                  可使用單位(模版)
     * @param allCompActiveDeps           公司下所有非停用單位
     * @return
     */
    private boolean isCanUseDep(
            Set<Integer> loginUserMatchDepSids,
            Integer loginUserSid,
            Integer loginCompSid,
            UserDep canUseDep,
            Boolean isCanUseDepContainFollowing,
            Boolean isOnlyUseDepManager,
            WCConfigTemplet depTemplet,
            final List<Org> allCompActiveDeps) {

        // ====================================
        // 取得可使用部門
        // ====================================
        // 可使用部門
        List<Integer> canUseDepSids = Lists.newArrayList();

        // 有模版設定時, 以模版設定優先
        if (depTemplet != null) {
            // 吃模版設定
            canUseDepSids = WCConfigTempletManager.getInstance()
                    .getTempletDeps(depTemplet.getSid(), loginCompSid);

        } else {
            // 吃項目自行設定
            canUseDepSids = Lists.newArrayList(WorkConnectVOUtils.prepareUserDepSids(canUseDep));
            // 含以下時
            if (isCanUseDepContainFollowing) {
                // 取得含以下所有單位
                canUseDepSids = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(canUseDepSids, true));
            }
        }

        // ====================================
        // 部門為空時，代表全單位皆可使用
        // ====================================
        // 排除 『僅主管可用』
        if (WkStringUtils.isEmpty(canUseDepSids)
                && !isOnlyUseDepManager) {
            return true;
        }

        // ====================================
        // 核決權限表 全查使用
        // ====================================
        if (loginUserMatchDepSids == null) {
            return true;
        }

        // ====================================
        // 僅主管可使用邏輯
        // ====================================
        // 僅自行設定時生效, 模版不生效
        if (isOnlyUseDepManager && depTemplet == null) {

            List<Org> currAllDep = null;
            if (WkStringUtils.isEmpty(canUseDepSids)) {
                // 可使用單位未設定時，代表項目可使用單位為『全單位』
                // 需為『非停用』單位主管才可以使用
                currAllDep = allCompActiveDeps;
            } else {
                // 取得可使用單位
                currAllDep = WkOrgCache.getInstance().findBySids(canUseDepSids);
            }

            // 逐筆比對可使用單位
            for (Org dep : currAllDep) {
                if (WkOrgUtils.isDepManager(dep.getSid(), loginUserSid)) {
                    return true;
                }
            }

            // 都不符合時， return false
            return false;
        }

        // ====================================
        // 可使用單位成員判斷
        // ====================================
        // 判斷回傳『符合任一』
        for (Integer matchDepSid : loginUserMatchDepSids) {
            if (canUseDepSids.contains(matchDepSid)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 檢測是否顯示中類(管理人員)
     *
     * @param wcMenuTag    中類物件
     * @param loginUserSid 登入者Sid
     * @return
     */
    public boolean checkCanShowManagerMenuItem(WCMenuTag wcMenuTag, Integer loginUserSid) {
        List<WCTag> tags = wcTagManager.getWCTagForManager(wcMenuTag.getSid(), Activation.ACTIVE);
        if (tags == null || tags.isEmpty()) {
            return false;
        }
        for (WCTag tag : tags) {
            if (this.checkCanShowManagerTagItem(tag, loginUserSid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 檢測是否顯示執行項目(管理人員)
     *
     * @param tag          執行項目物件
     * @param loginUserSid 登入者Sid
     * @return
     */
    public boolean checkCanShowManagerTagItem(WCTag tag, Integer loginUserSid) {
        List<WCExecDepSetting> execDepSettings = wcExecDepSettingManager.getExecDepSettingFromCacheByWcTagSid(tag.getSid(), Activation.ACTIVE);
        for (WCExecDepSetting execDepSetting : execDepSettings) {
            if (checkCanShowManagerExecDepSetting(execDepSetting, loginUserSid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 檢測是否顯示管理執行單位
     *
     * @param execDepSetting 執行單位設定
     * @param loginUserSid   登入者Sid
     * @return
     */
    public boolean checkCanShowManagerExecDepSetting(
            WCExecDepSetting execDepSetting, Integer loginUserSid) {
        if (execDepSetting.getManagerRecevice() != null
                && execDepSetting.getManagerRecevice().getUserTos() != null) {
            List<UserTo> managerUsers = execDepSetting.getManagerRecevice().getUserTos().stream()
                    .filter(each -> each.getSid().equals(loginUserSid))
                    .collect(Collectors.toList());
            return managerUsers != null && !managerUsers.isEmpty();
        }
        return false;
    }

    /**
     * 檢測是否可顯式執行項目(需設定執行單位才可挑選)
     *
     * @param tag               執行項目物件
     * @param wcConfigTemplets  所有設定模版
     * @param loginManagedDeps  登入單位Sid & 管理部門Sid
     * @param loginUserSid      登入userSid
     * @param loginCompSid      登入公司Sid
     * @param allCompActiveDeps 所有非停用單位
     * @return
     */
    public boolean checkCanShowTagItem(
            WCTag tag,
            List<WCConfigTemplet> wcConfigTemplets,
            Set<Integer> loginManagedDeps,
            Integer loginUserSid,
            Integer loginCompSid,
            final List<Org> allCompActiveDeps) {

        // ====================================
        // 檢查是否有設定執行單位
        // ====================================
        List<WCExecDepSetting> execDepSettings = wcExecDepSettingManager.getExecDepSettingFromCacheByWcTagSid(
                tag.getSid(),
                Activation.ACTIVE);

        // 未設定執行單位的執行項目不可使用
        if (WkStringUtils.isEmpty(execDepSettings)) {
            return false;
        }

        // ====================================
        // 檢查是否有設定可使用單位
        // ====================================
        // 取得部門設定模版 (類別:單據名稱-可使用部門)
        WCConfigTemplet depTemplet = this.getTempletBySid(
                tag.getCanUserDepTempletSid(),
                wcConfigTemplets,
                ConfigTempletType.SUB_TAG_CAN_USE);

        // 登入者部門是否可顯示
        return this.isCanUseDep(
                loginManagedDeps,
                loginUserSid,
                loginCompSid,
                tag.getCanUserDep(),
                tag.isUserContainFollowing(),
                tag.isOnlyForUseDepManager(),
                depTemplet,
                allCompActiveDeps);
    }
}
