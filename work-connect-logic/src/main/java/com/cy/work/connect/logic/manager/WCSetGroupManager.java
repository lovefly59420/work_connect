/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCExecDepSettingCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.vo.WCConfigTempletVo;
import com.cy.work.connect.logic.vo.WCGroupVo;
import com.cy.work.connect.repository.WCSetGroupRepository;
import com.cy.work.connect.repository.WCSetPermissionRepository;
import com.cy.work.connect.vo.WCCategoryTag;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCMenuTag;
import com.cy.work.connect.vo.WCSetGroup;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import com.cy.work.connect.vo.converter.to.UserDep;
import com.cy.work.connect.vo.enums.TargetType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jimmy_chou
 */
@Slf4j
@Component
public class WCSetGroupManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3617612686029139836L;

    private static WCSetGroupManager instance;
    /**
     *
     */
    @Autowired
    private WorkSettingOrgManager workSettingOrgManager;

    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WkUserCache wkUserCache;
    /**
     * 設定群組
     */
    @Autowired
    private WCSetGroupRepository groupRepository;
    /**
     * 設定權限
     */
    @Autowired
    private WCSetPermissionRepository permissionRepository;
    /**
     * 模版
     */
    @Autowired
    private WCConfigTempletManager configTempletManager;
    /**
     * 工作聯絡單 - 單據相關
     */
    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;

    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCTagCache wcTagCache;
    @Autowired
    private WCExecDepSettingCache wcExecDepSettingCache;

    public static WCSetGroupManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCSetGroupManager.instance = this;
    }

    /**
     * 以SID ˊ查詢
     *
     * @param sid
     * @return
     */
    public WCGroupVo findBySid(Long sid) {

        // ====================================
        // 查詢
        // ====================================
        if (sid == null) {
            // log.warn("傳入 sid 為 null");
            return null;
        }

        WCSetGroup group = this.groupRepository.findOne(sid);
        if (group == null) {
            log.warn("WCGroup 找不到指定的資料檔");
            return null;
        }

        // ====================================
        // 轉VO 並回傳
        // ====================================
        return new WCGroupVo(group);
    }

    /**
     * 依據條件查詢
     *
     * @param deps
     * @param users
     * @param compSid
     * @return
     */
    public List<WCGroupVo> queryByCondition(
        List<Integer> deps, List<Integer> users, Integer compSid) {

        // ====================================
        // 查詢
        // ====================================
        if (compSid == null) {
            return Lists.newArrayList();
        }

        List<WCSetGroup> results = this.groupRepository.findByCompSid(compSid);

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        results =
            results.stream()
                .filter(
                    group -> {
                        if (WkStringUtils.isEmpty(deps) && WkStringUtils.isEmpty(users)) {
                            return true;
                        }
                        if (WkStringUtils.isEmpty(group.getConfigValue().getDeps())
                            && WkStringUtils.isEmpty(group.getConfigValue().getUsers())) {
                            return false;
                        }

                        // ====================================
                        // 過濾查詢條件：部門
                        // ====================================
                        List<Integer> settingDeps = this.getGroupDeps(group.getSid(), compSid);

                        // 判斷回傳『符合任一』
                        if (WkStringUtils.notEmpty(settingDeps)) {
                            return !Collections.disjoint(settingDeps, deps);
                        }

                        // ====================================
                        // 過濾查詢條件：人員
                        // ====================================
                        List<Integer> settingUsers = this.getGroupUsers(group.getSid(), compSid);

                        // 判斷回傳『符合任一』
                        if (WkStringUtils.notEmpty(settingUsers)) {
                            return !Collections.disjoint(settingUsers, users);
                        }

                        return false;
                    })
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(results)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 排序
        // ====================================
        // 1.類別
        Comparator<WCSetGroup> comparator = Comparator.comparing(WCSetGroup::getStatus);
        // 2.排序序號
        comparator = comparator.thenComparing(Comparator.comparing(WCSetGroup::getSortSeq));
        // 3.建立時間
        comparator = comparator.thenComparing(Comparator.comparing(WCSetGroup::getCreateDate));

        results = results.stream().sorted(comparator).collect(Collectors.toList());

        // ====================================
        // 轉成 VO 並回傳
        // ====================================
        List<WCGroupVo> voResult = Lists.newArrayList();
        for (WCSetGroup group : results) {
            voResult.add(new WCGroupVo(group));
        }
        return voResult;
    }

    /**
     * @param groupVO
     * @param loginUserSid
     * @param loginUserCompSid
     * @throws Exception
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void saveSetting(WCGroupVo groupVO, Integer loginUserSid, Integer loginUserCompSid)
        throws Exception {

        Date sysDate = new Date();

        // ====================================
        // 準備主檔 DB entities
        // ====================================
        WCSetGroup group;

        if (WkStringUtils.isEmpty(groupVO.getSid())) {
            // 為新增
            group = new WCSetGroup();
            // 公司別 （避免不同的公司混用）
            group.setCompSid(loginUserCompSid);
            // 建立者
            group.setCreateUser(loginUserSid);
            // 建立時間
            group.setCreateDate(sysDate);
            // 狀態
            group.setStatus(Activation.ACTIVE);
            // 排列序號 => 初始不給, 由DB給 default (最大值, 讓他排在最後面)
            // group.setSortSeq(Integer.MAX_VALUE);

        } else {
            // 查詢
            group = this.groupRepository.findOne(groupVO.getSid());
            // 檢核
            if (group == null) {
                throw new Exception("編輯項目已不存在, 請重新查詢");
            }

            // 更新者
            group.setUpdateUser(loginUserSid);
            // 更新時間
            group.setUpdateDate(sysDate);
        }

        // 名稱
        group.setGroupName(groupVO.getGroupName());
        // 設定值
        group.setConfigValue(this.prepareDeps(loginUserCompSid, groupVO.getConfigValue()));
        // 備註
        group.setMemo(groupVO.getMemo());

        // ====================================
        // save wc_config_group
        // ====================================
        this.groupRepository.save(group);
    }

    /**
     * 在『包含以下時』將單位列表縮減成最上層
     *
     * @param configValueVo
     * @return
     */
    private ConfigValueTo prepareDeps(Integer compSid, ConfigValueTo configValueVo) {
        if (configValueVo == null || configValueVo.getDeps() == null) {
            return configValueVo;
        }

        List<Integer> deps = configValueVo.getDeps();

        // 在『包含以下時』將單位列表縮減成最上層
        if (configValueVo.isDepContainFollowing()) {
            deps = Lists.newArrayList(WkOrgUtils.collectToTopmost(deps));
        }

        // 依據組織樹排序
        deps = WkOrgUtils.sortByOrgTree(deps);

        // 回傳
        configValueVo.setDeps(deps);
        return configValueVo;
    }

    /**
     * 取得群組部門
     *
     * @param groupSid
     * @param loginCompSid
     * @return
     */
    public List<Integer> getGroupDeps(Long groupSid, Integer loginCompSid) {

        // ====================================
        // 以sid 查詢
        // ====================================
        WCSetGroup group = this.groupRepository.findOne(groupSid);

        if (group == null) {
            log.error("getUseDeps 找不到對應資料 groupSid : [" + groupSid + "]");
            return Lists.newArrayList();
        }

        // ====================================
        // 取得設定部門
        // ====================================
        List<Integer> depSids = group.getConfigValue().getDeps();
        // 部門為空時
        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // 部門含以下
        if (group.getConfigValue().isDepContainFollowing()) {
            depSids = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(depSids, false));
        }

        // 被選擇部門除外
        if (group.getConfigValue().isDepExcept()) {
            depSids = this.workSettingOrgManager.findAllWhitOutDeps(depSids, loginCompSid);
        }

        // 排序部門
        depSids = WkOrgUtils.sortByOrgTree(depSids);

        return depSids;
    }

    /**
     * 取得群組 user
     *
     * @param groupSid
     * @param loginCompSid
     * @return
     */
    public List<Integer> getGroupUsers(Long groupSid, Integer loginCompSid) {

        // ====================================
        // 以sid 查詢
        // ====================================
        WCSetGroup group = this.groupRepository.findOne(groupSid);

        if (group == null) {
            log.error("getDefultCanReadUsers 找不到對應資料 groupSid : [" + groupSid + "]");
            return Lists.newArrayList();
        }

        // ====================================
        // 取得設定部門
        // ====================================
        List<Integer> depSids = group.getConfigValue().getDeps();

        // ====================================
        // 取得設定的人員 user
        // ====================================
        List<Integer> userSids = group.getConfigValue().getUsers();

        // 為空時代表無人可使用
        if (WkStringUtils.isEmpty(depSids) && WkStringUtils.isEmpty(userSids)) {
            return Lists.newArrayList();
        }

        // 部門含以下
        if (group.getConfigValue().isDepContainFollowing()) {
            depSids = Lists.newArrayList(WkOrgUtils.prepareAllDepsByTopmost(depSids, false));
        }

        // 被選擇部門除外
        if (group.getConfigValue().isDepExcept()) {
            depSids = this.workSettingOrgManager.findAllWhitOutDeps(depSids, loginCompSid);
        }

        // 排序部門
        depSids = WkOrgUtils.sortByOrgTree(depSids);

        // ====================================
        // 取得設定部門以下的 user
        // ====================================
        List<Integer> allUsers = Lists.newArrayList();
        for (Integer depSid : depSids) {
            // 查部門下所有的User
            List<User> users = this.findUserByDepSid(depSid);
            if (WkStringUtils.isEmpty(users)) {
                continue;
            }

            // 以名稱排序
            users =
                users.stream().sorted(Comparator.comparing(User::getName))
                    .collect(Collectors.toList());

            for (User usr : users) {
                // 為啟用時加入
                if (Activation.ACTIVE.equals(usr.getStatus()) && !allUsers.contains(usr.getSid())) {
                    allUsers.add(usr.getSid());
                }
            }
        }

        // ====================================
        // 加入設定的 user
        // ====================================
        if (WkStringUtils.notEmpty(group.getConfigValue().getUsers())) {
            // 被選擇除外
            if (group.getConfigValue().isDepExcept()) {
                if (WkStringUtils.isEmpty(allUsers)) {
                    allUsers =
                        this.findUserByDepSid(loginCompSid).stream()
                            .map(each -> each.getSid())
                            .collect(Collectors.toList());
                }
                allUsers.removeAll(group.getConfigValue().getUsers());
            } else {
                allUsers.addAll(group.getConfigValue().getUsers());
            }
        }

        return allUsers;
    }

    /**
     * 依據單位 sid , 由快取中取得該部門的 user
     *
     * @param depSid
     * @return
     */
    private List<User> findUserByDepSid(Integer depSid) {
        // 由快取中取得
        List<User> depUsers = this.wkUserCache.getUsersByPrimaryOrgSid(depSid);
        // 防呆
        if (depUsers == null) {
            return Lists.newArrayList();
        }
        return depUsers;
    }

    /**
     * 依據傳入 List 順序, 更新排序序號
     *
     * @param groupList
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void updateSortSeqByListIndex(List<WCGroupVo> groupList) {

        // 傳入列表為空
        if (WkStringUtils.isEmpty(groupList)) {
            return;
        }

        // 以公司別, 紀錄 index 序號
        Map<Integer, Integer> indexMap = Maps.newConcurrentMap();

        for (WCGroupVo groupVo : groupList) {

            // 取得群組資料檔
            WCSetGroup wcConfigGroup = this.groupRepository.findOne(groupVo.getSid());
            if (wcConfigGroup == null) {
                continue;
            }

            // 依據群組類別累加序號
            Integer compSid = wcConfigGroup.getCompSid();
            Integer index = indexMap.get(compSid);
            if (index == null) {
                index = 1;
                indexMap.put(compSid, index);
            }
            wcConfigGroup.setSortSeq(index);
            indexMap.put(compSid, index + 1);

            // 更新
            this.groupRepository.save(wcConfigGroup);
        }
    }

    /**
     * 準備部門設定要顯示的文字
     *
     * @param useDep             UserDep 容器 - for 自行設定資料
     * @param isContainFollowing 是否包含以下 - for 自行設定資料
     * @param groupSid           部門範本SID
     * @return
     */
    public String[] prepareDepSettingShowContent(
        UserDep useDep, boolean isContainFollowing, Long groupSid) {

        // 結果資料
        String depTag = "";
        String depTooltip = "";

        // ====================================
        // 查詢群組設定檔
        // ====================================
        WCGroupVo groupVo = null;
        if (groupSid != null) {
            groupVo = this.findBySid(groupSid);
        }

        // ====================================
        // 自行設定
        // ====================================
        if (groupVo == null) {

            // 轉 sid list
            List<Integer> depSids = this.workSettingOrgManager.convertUserDepToSids(useDep);

            // 含以下單位先最取得最上層處理
            if (isContainFollowing) {
                depSids = Lists.newArrayList(WkOrgUtils.collectToTopmost(depSids));
            }
            // 依組織樹排序
            depSids = WkOrgUtils.sortByOrgTree(depSids);

            // 產生部門列表顯示文字
            depTooltip =
                this.workSettingOrgManager.prepareDepsShowText(depSids, isContainFollowing, false);

            if (WkStringUtils.notEmpty(depSids)) {
                depTag = "自行設定";
                if (isContainFollowing) {
                    depTag += "&nbsp;&nbsp;(<span class='WS1-1-3'>含以下</span>)";
                }
            } else {
                depTag = "-";
            }

            depTag = WkStringUtils.notEmpty(depSids) ? "自行設定" : "";
        }

        // ====================================
        // 依群組設定
        // ====================================
        if (groupVo != null) {
            // 產生部門列表顯示文字
            depTooltip =
                workSettingOrgManager.prepareDepsShowText(
                    groupVo.getConfigValue().getDeps(),
                    groupVo.getConfigValue().isDepContainFollowing(),
                    groupVo.getConfigValue().isDepExcept());

            depTag = "群組:&nbsp";
            depTag +=
                this.getPettyGroupName(groupVo.getGroupName(), groupVo.getConfigValue(), true,
                    true);
        }

        return new String[]{depTag, depTooltip};
    }

    /**
     * 檢查設定群組是否被參考使用
     *
     * @param compSid
     * @param groupSid
     * @return
     */
    public String checkInUsed(Integer compSid, Long groupSid) {

        // ====================================
        // 依據不同的[設定來源]作檢核後的呈現
        // ====================================
        List<WCSetPermission> permissions =
            this.permissionRepository.findPermissionByCompSidAndGroupSid(compSid, groupSid);

        Map<TargetType, List<WCSetPermission>> permissionMap =
            permissions.stream().collect(Collectors.groupingBy(key -> key.getTargetType()));

        StringBuilder result = new StringBuilder();

        for (TargetType key : permissionMap.keySet()) {
            if (result.length() > 0) {
                result.append("<br/><br/>");
            }
            switch (key) {
                case CATEGORY_TAG:
                    result.append("《大類》：");
                    this.buildCategoryTagInUsed(result, permissionMap.get(key));
                    continue;
                case MENU_TAG:
                    result.append("《中類》：");
                    this.buildMenuTagInUsed(result, permissionMap.get(key));
                    continue;
                case TAG:
                    result.append("《小類》：");
                    this.buildTagInUsed(result, permissionMap.get(key));
                    continue;
                case EXEC_DEP:
                    result.append("《執行單位》：");
                    this.buildExecDepSettingInUsed(result, permissionMap.get(key));
                    continue;
                case CONFIG_TEMPLET:
                    result.append("《模版》：");
                    this.buildConfigTempletInUsed(result, permissionMap.get(key));
                    continue;
                default:
                    continue;
            }
        }
        return result.toString();
    }

    /**
     * 串組設定群組已使用項目(大類)
     *
     * @param result
     * @param permissions
     */
    private void buildCategoryTagInUsed(StringBuilder result, List<WCSetPermission> permissions) {
        for (WCSetPermission each : permissions) {
            if (result.length() > 0) {
                result.append("<br/>");
            }
            // 查詢大類
            WCCategoryTag categoryTag =
                this.wcCategoryTagCache.getWCCategoryTagBySid(each.getTargetSid());
            // 組訊息文字
            if (categoryTag != null) {
                result.append(categoryTag.getCategoryName());
            } else {
                result.append("[找不到大類]");
            }
        }
    }

    /**
     * 串組設定群組已使用項目(中類)
     *
     * @param result
     * @param permissions
     */
    private void buildMenuTagInUsed(StringBuilder result, List<WCSetPermission> permissions) {
        for (WCSetPermission each : permissions) {
            if (result.length() > 0) {
                result.append("<br/>");
            }
            // 查詢中類
            WCMenuTag menuTag = this.wcMenuTagCache.getWCMenuTagBySid(each.getTargetSid());
            if (menuTag != null) {
                // 查詢大類
                WCCategoryTag categoryTag =
                    this.wcCategoryTagCache.getWCCategoryTagBySid(menuTag.getWcCategoryTagSid());
                // 組訊息文字
                if (categoryTag != null) {
                    result.append(categoryTag.getCategoryName());
                } else {
                    result.append("[找不到大類]");
                }
                result.append("-" + menuTag.getItemName());
            } else {
                result.append("[找不到中類]");
            }
        }
    }

    /**
     * 串組設定群組已使用項目(小類)
     *
     * @param result
     * @param permissions
     */
    private void buildTagInUsed(StringBuilder result, List<WCSetPermission> permissions) {
        for (WCSetPermission each : permissions) {
            if (result.length() > 0) {
                result.append("<br/>");
            }

            // 查詢小類
            WCTag tag = this.wcTagCache.findBySid(each.getTargetSid());
            if (tag != null) {
                // 查詢中類
                WCMenuTag menuTag = this.wcMenuTagCache.getWCMenuTagBySid(tag.getWcMenuTagSid());
                if (menuTag != null) {
                    // 查詢大類
                    WCCategoryTag categoryTag =
                        this.wcCategoryTagCache.getWCCategoryTagBySid(
                            menuTag.getWcCategoryTagSid());
                    // 組訊息文字
                    if (categoryTag != null) {
                        result.append(categoryTag.getCategoryName());
                    } else {
                        result.append("[找不到大類]");
                    }
                    result.append("-" + menuTag.getItemName());
                } else {
                    result.append("[找不到中類]");
                }
                result.append("-" + tag.getTagName());
            } else {
                result.append("[找不到小類]");
            }
        }
    }

    /**
     * 串組設定群組已使用項目(執行單位)
     *
     * @param result
     * @param permissions
     */
    private void buildExecDepSettingInUsed(StringBuilder result,
        List<WCSetPermission> permissions) {
        for (WCSetPermission each : permissions) {
            if (result.length() > 0) {
                result.append("<br/>");
            }

            // 查詢執行單位
            WCExecDepSetting execDep =
                this.wcExecDepSettingCache.getExecDepSettingBySid(each.getTargetSid());
            if (execDep != null) {
                // 查詢小類
                WCTag tag = this.wcTagCache.findBySid(execDep.getWc_tag_sid());
                if (tag != null) {
                    // 查詢中類
                    WCMenuTag menuTag = this.wcMenuTagCache.getWCMenuTagBySid(
                        tag.getWcMenuTagSid());
                    if (menuTag != null) {
                        // 查詢大類
                        WCCategoryTag categoryTag =
                            this.wcCategoryTagCache.getWCCategoryTagBySid(
                                menuTag.getWcCategoryTagSid());
                        // 組訊息文字
                        if (categoryTag != null) {
                            result.append(categoryTag.getCategoryName());
                        } else {
                            result.append("[找不到大類]");
                        }
                        result.append("-" + menuTag.getItemName());
                    } else {
                        result.append("[找不到中類]");
                    }
                    result.append("-" + tag.getTagName());
                } else {
                    result.append("[找不到小類]");
                }
                Org dep = this.orgManager.findBySid(execDep.getExec_dep_sid());
                if (dep != null) {
                    result.append("-" + dep.getName());
                } else {
                    result.append("[找不到執行單位]");
                }
            } else {
                result.append("[找不到執行單位]");
            }
        }
    }

    /**
     * 串組設定群組已使用項目(模版)
     *
     * @param result
     * @param permissions
     */
    private void buildConfigTempletInUsed(StringBuilder result, List<WCSetPermission> permissions) {
        for (WCSetPermission each : permissions) {
            if (result.length() > 0) {
                result.append("<br/>");
            }

            // 查詢模版
            WCConfigTempletVo templet =
                this.configTempletManager.findBySid(Long.valueOf(each.getTargetSid()));
            // 組訊息文字
            if (templet != null) {
                result.append(templet.getTempletNameShowColorful());
            } else {
                result.append("[找不到模版]");
            }
        }
    }

    /**
     * 刪除群組
     *
     * @param groupSid
     */
    public void deleteGroup(Long groupSid) {
        this.groupRepository.delete(groupSid);
        log.info("刪除群組:" + groupSid);
    }

    /**
     * @param groupName
     * @param configValueTo
     * @param isColorful
     * @param isAddUnderline
     * @return
     */
    public String getPettyGroupName(
        String groupName, ConfigValueTo configValueTo, boolean isColorful, boolean isAddUnderline) {

        // 組群組名稱資訊
        String showText = "";
        if (isAddUnderline) {
            showText += "<span style='text-decoration:underline;'>";
        }
        showText = groupName;
        if (isAddUnderline) {
            showText += "</span>";
        }

        boolean isDepContainFollowing = configValueTo.isDepContainFollowing();
        boolean isExecpt = configValueTo.isDepExcept();

        if (isDepContainFollowing || isExecpt) {
            showText += "【";
            if (isExecpt) {
                if (isColorful) {
                    showText += "<span class='WS1-1-2'>";
                }
                showText += "除外";
                if (isColorful) {
                    showText += "</span>";
                }
            }
            if (isDepContainFollowing) {
                if (isExecpt) {
                    showText += "-";
                }
                if (isColorful) {
                    showText += "<span class='WS1-1-3'>";
                }
                showText += "含以下";
                if (isColorful) {
                    showText += "</span>";
                }
            }
            showText += "】";
        }
        return showText;
    }
}
