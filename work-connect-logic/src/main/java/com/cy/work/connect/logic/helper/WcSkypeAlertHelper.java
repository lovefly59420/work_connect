/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import com.cy.work.common.exception.alert.WorkConnectAlertException;

/**
 * 發送 skype alert
 * 
 * @author allen1214_wu
 */
@Service
public class WcSkypeAlertHelper implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2166518793965464646L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WcSkypeAlertHelper instance;

    public static WcSkypeAlertHelper getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WcSkypeAlertHelper.instance = this;
    }

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * @param message
     */
    public void sendSkypeAlert(String message) {
        // 僅需要 new 出來即發送
        new WorkConnectAlertException(message);
    }

    /**
     * @param message
     */
    public void sendSkypeAlert(String message, String compID) {
        new WorkConnectAlertException(message, compID);
    }

}
