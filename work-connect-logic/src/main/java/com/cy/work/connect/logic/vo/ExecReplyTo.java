/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 執行回覆自訂物件
 *
 * @author kasim
 */
@EqualsAndHashCode(of = {"sid"})
public class ExecReplyTo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7745173992993647127L;

    @Getter
    /** [wc_exec_reply].[exec_reply_sid] */
    private final String sid;

    @Getter
    /** 是否能編輯(限本人) */
    private final boolean showEdit;

    @Getter
    /** 執行人員 */
    private final Integer exceUser;

    @Getter
    /** 執行人員名稱 */
    private final String exceUserName;

    @Getter
    /** 回覆時間 */
    private final String updateDate;

    @Getter
    /** 回覆內容CSS */
    private final String content;

    public ExecReplyTo(
        String sid,
        boolean showEdit,
        Integer exceUser,
        String exceUserName,
        String updateDate,
        String content) {
        this.sid = sid;
        this.showEdit = showEdit;
        this.exceUser = exceUser;
        this.exceUserName = exceUserName;
        this.updateDate = updateDate;
        this.content = content;
    }
}
