/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import java.util.Objects;
import lombok.Getter;

/**
 * 意見說明Key 物件
 *
 * @author brain0925_liao
 */
public class OpinionDescripeSettingVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 72221137018357183L;
    /**
     * 簽核類型-需求或者核准
     */
    @Getter
    private final String sourceTypeName;
    /**
     * 簽核物件Sid
     */
    @Getter
    private final String sourceSid;
    /**
     * 意見說明的User Sid
     */
    @Getter
    private final Integer opinion_usr;

    public OpinionDescripeSettingVO(String sourceTypeName, String sourceSid, Integer opinion_usr) {
        this.sourceTypeName = sourceTypeName;
        this.sourceSid = sourceSid;
        this.opinion_usr = opinion_usr;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.sourceTypeName);
        hash = 59 * hash + Objects.hashCode(this.sourceSid);
        hash = 59 * hash + Objects.hashCode(this.opinion_usr);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OpinionDescripeSettingVO other = (OpinionDescripeSettingVO) obj;
        if (!Objects.equals(this.sourceTypeName, other.sourceTypeName)) {
            return false;
        }
        if (!Objects.equals(this.sourceSid, other.sourceSid)) {
            return false;
        }
        return Objects.equals(this.opinion_usr,
            other.opinion_usr);
    }
}
