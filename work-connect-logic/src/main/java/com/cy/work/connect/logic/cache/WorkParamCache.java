/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WcParamRepository;
import com.cy.work.connect.vo.WorkParam;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 特殊設定值Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WorkParamCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2427456075498020172L;
    /**
     * 提供一鍵清快取顯示執行狀況使用
     */
    private final List<FacesMessage> cacheResults = Lists.newArrayList();
    /**
     * WorkParamRepository
     */
    @Autowired
    private WcParamRepository workParamRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WorkParam> workParamMap;

    public WorkParamCache() {
        startController = Boolean.TRUE;
        workParamMap = Maps.newConcurrentMap();
    }

    public void initCacheResults() {
        cacheResults.clear();
    }

    public void addInfoCacheResults(String msg) {
        cacheResults.add(new FacesMessage(msg));
    }

    public void addErrorCacheResults(String msg) {
        cacheResults.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    }

    public List<FacesMessage> getCacheResults() { return cacheResults; }

    /**
     * 初始化Cache資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            this.updateCache();
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay = ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        log.debug("建立特殊參數Cache");
        try {
            Map<String, WorkParam> tempWorkParamMap = Maps.newConcurrentMap();
            workParamRepository
                    .findAll()
                    .forEach(
                            item -> {
                                tempWorkParamMap.put(item.getKeyword(), item);
                            });
            workParamMap = tempWorkParamMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立特殊參數Cache結束");
    }

    /**
     * 取得特殊資料 By keyword
     *
     * @param keyword
     * @return
     */
    public String findContentByKeyword(String keyword) {
        if (!workParamMap.containsKey(keyword)) {
            return "";
        }
        return workParamMap.get(keyword).getContent();
    }
}
