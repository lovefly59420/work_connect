/**
 * 
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCManagerSignInfoManager;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.SignType;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Service
@Slf4j
public class SignFlowForRequireLogic implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8588154534928518729L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SignFlowForRequireLogic instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static SignFlowForRequireLogic getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(SignFlowForRequireLogic instance) { SignFlowForRequireLogic.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient BpmManager bpmManager;
    @Autowired
    private transient WCManagerSignInfoManager wcManagerSignInfoManager;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 為需求單位[主流程或會簽]流程，待簽或或已簽人員
     * 
     * @param wcSid
     * @param targetUserSid
     * @return
     */
    public boolean isRequireFlowCanSignOrSignedUser(String wcSid, Integer targetUserSid) {

        // ====================================
        // 查詢 userID
        // ====================================
        String targetUserID = WkUserUtils.findUserIDBySid(targetUserSid);
        if (WkStringUtils.isEmpty(targetUserID)) {
            return false;
        }

        // ====================================
        // 檢查是否為需求方簽核流程待簽者
        // ====================================
        try {
            if (this.isReqFlowSignUser(wcSid, targetUserSid, null)) {
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        // ====================================
        // 檢查為已簽名者
        // ====================================
        List<WCManagerSignInfo> reqFlowSignInfos = this.wcManagerSignInfoManager.findActiveReqFlowSignInfos(wcSid);
        if (WkStringUtils.isEmpty(reqFlowSignInfos)) {
            return false;
        }

        // 收集 instanceId
        List<String> instanceIds = reqFlowSignInfos.stream()
                .map(WCManagerSignInfo::getBpmId)
                .collect(Collectors.toList());

        // 取得所有水管圖
        Map<String, List<ProcessTaskHistory>> processTasksMapByinstanceId = this.bpmManager.findSimulationCharts(
                instanceIds);

        // 比對已經簽名者
        for (List<ProcessTaskHistory> processTaskHistorys : processTasksMapByinstanceId.values()) {
            for (ProcessTaskHistory processTask : processTaskHistorys) {
                if (WkCommonUtils.compareByStr(targetUserID, processTask.getExecutorID())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 是否為需求方簽核流程的待簽者
     * 
     * @param userId           user ID
     * @param reqFlowSignInfos 需求方簽核流程資料
     * @return y/n
     * @throws UserMessageException 錯誤時拋出
     */
    public Boolean isReqFlowSignUser(String wcSid, Integer targetUserSid, SignType signType) {

        // ====================================
        // 查詢 userID
        // ====================================
        String userID = WkUserUtils.findUserIDBySid(targetUserSid);
        if (WkStringUtils.isEmpty(userID)) {
            return false;
        }

        // ====================================
        // 查詢所有需求方流程
        // ====================================
        List<WCManagerSignInfo> reqFlowSignInfos = this.wcManagerSignInfoManager.findActiveReqFlowSignInfos(wcSid);
        if (WkStringUtils.isEmpty(reqFlowSignInfos)) {
            return false;
        }

        // 防呆
        if (WkStringUtils.isEmpty(reqFlowSignInfos)) {
            return false;
        }

        // ====================================
        // 逐筆處理
        // ====================================
        for (WCManagerSignInfo signInfo : reqFlowSignInfos) {
            // 有指定 signType 時, 比對流程類型
            if (signType != null 
                    && !signType.equals(signInfo.getSignType())) {
                continue;
            }
            try {
                if (BpmManager.getInstance().isCanSignUser(userID, signInfo.getBpmId(), signInfo.getStatus())) {
                    return true;
                }
            } catch (UserMessageException e) {
                // 失敗時，不顯示訊息
                log.error(e.getMessage());
            }
        }
        return false;
    }
}
