package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.client.BpmOrganizationClient;
import com.cy.bpm.rest.client.ProcessClient;
import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.to.RoleTo;
import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.common.ProcessType;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.logic.vo.BpmCreateTo;
import com.cy.work.connect.logic.vo.BpmRoleTo;
import com.cy.work.connect.logic.vo.BpmSignTo;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * BPM 管理
 *
 * @author kasim
 */
@Component
@Slf4j
public class BpmManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -522983975318494919L;

    private static BpmManager instance;
    @Autowired
    private BpmOrganizationClient organizationClient;
    @Autowired
    private ProcessClient processClient;
    @Autowired
    private TaskClient taskClient;

    public static BpmManager getInstance() { return instance; }

    public String getRoleIdInCompany(String userId, String companyId) throws ProcessRestException {
        String roleId = organizationClient.findUserRoleByCompany(userId, companyId);
        return roleId;
    }

    public Integer getRoleLevelInCompany(String userId, String companyId)
            throws ProcessRestException {
        String roleId = organizationClient.findUserRoleByCompany(userId, companyId);
        return organizationClient.findRoleToById(roleId).getRoleLevel();
    }

    public String getUserMajorRoleName(String userId) {
        try {
            return (organizationClient.findUserMajorRoleName(userId));
        } catch (ProcessRestException ex) {
            log.warn("getUserMajorRoleName ERROR", ex);
        }
        return "";
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        BpmManager.instance = this;
    }

    /**
     * 建立流程
     * 
     * @param masterNo
     * @param userSid
     * @param companySid
     * @param flowType
     * @return
     * @throws SystemOperationException
     */
    @Transactional(rollbackFor = Exception.class)
    public String createBpmFlow(
            String masterNo, Integer userSid, Integer companySid, WcBpmFlowType flowType) throws SystemOperationException {

        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            throw new SystemOperationException("找不到使用者資料[" + userSid + "]", InfomationLevel.ERROR);
        }

        Org comp = WkOrgCache.getInstance().findBySid(companySid);
        if (comp == null) {
            throw new SystemOperationException("找不到傳入公司別資料[" + companySid + "]", InfomationLevel.ERROR);
        }

        try {
            return processClient.create(
                    new BpmCreateTo(
                            masterNo,
                            user.getId(),
                            this.findUserRoleByCompany(user.getId(), comp.getId()),
                            flowType));
        } catch (ProcessRestException e) {
            String message = "建立簽核流程失敗! [" + e.getMessage() + "]";
            log.error(message, e);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

    }

    /**
     * 建立流程
     *
     * @param masterNo
     * @param userSid
     * @param companySid
     * @param flowType
     * @return
     * @throws ProcessRestException
     */
    @Transactional(rollbackFor = Exception.class)
    public String createBpmFlow(
            String masterNo,
            Integer userSid,
            Integer companySid,
            WcBpmFlowType flowType,
            Map<String, Object> parameters)
            throws SystemOperationException {

        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            throw new SystemOperationException("找不到使用者資料[" + userSid + "]", InfomationLevel.ERROR);
        }

        Org comp = WkOrgCache.getInstance().findBySid(companySid);
        if (comp == null) {
            throw new SystemOperationException("找不到傳入公司別資料[" + companySid + "]", InfomationLevel.ERROR);
        }

        try {
            return processClient.create(
                    new BpmCreateTo(
                            masterNo,
                            user.getId(),
                            this.findUserRoleByCompany(user.getId(), comp.getId()),
                            flowType,
                            parameters));
        } catch (ProcessRestException e) {
            String message = "建立簽核流程失敗! [" + e.getMessage() + "]";
            log.error(message, e);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }
    }

    /**
     * 尋找使用者在指定公司角色
     *
     * @param userId
     * @param companyId
     * @return
     * @throws ProcessRestException
     */
    public String findUserRoleByCompany(String userId, String companyId)
            throws ProcessRestException {
        return organizationClient.findUserRoleByCompany(userId, companyId);
    }

    /**
     * 尋找使用者在指定公司角色資訊
     *
     * @param userId
     * @param companyId
     * @return
     * @throws ProcessRestException
     */
    public RoleTo findUserRoleInfoByCompany(String userId, String companyId)
            throws ProcessRestException {
        String roleId = this.findUserRoleByCompany(userId, companyId);
        return organizationClient.findRoleToById(roleId);
    }

    /**
     * 取回所有的使用者角色
     *
     * @param userId
     * @return
     * @throws ProcessRestException
     */
    public List<String> findRoleFromUser(String userId) throws ProcessRestException {
        return organizationClient.findRoleFromUser(userId);
    }

    /**
     * 比對 User 是否有某個角色
     *
     * @param userId
     * @param roleName
     * @return
     * @throws ProcessRestException
     */
    public boolean isRole(String userId, String roleName) throws ProcessRestException {
        // 查詢
        List<String> roleList = this.findRoleFromUser(userId);
        if (WkStringUtils.isEmpty(roleList)) {
            return false;
        }

        // 比對
        return roleList.contains(roleName);
    }

    //

    /**
     * 尋找流程實例當前可簽核人員
     *
     * @param bpmId
     * @return
     */
    public List<String> findTaskCanSignUserIds(String bpmId) {
        try {
            if (Strings.isNullOrEmpty(bpmId)) {
                return Lists.newArrayList();
            }
            return taskClient.findTaskCanSignUserIds(bpmId);
        } catch (Exception ex) {
            String message = "查詢BPM待簽者失敗! bpmId :[" + bpmId + "]," + ex.getMessage();
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
            log.error(message, ex);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得可簽人員 (通訊失敗時 ProcessRestException，會吐 exception)
     * 
     * @param bpmId BPM ID
     * @return 可簽核人員 ID (多筆)
     * @throws SystemOperationException 呼叫 BPM REST 失敗時拋出
     */
    public List<String> findTaskCanSignUserIdsWithException(String bpmId) throws SystemOperationException {

        try {
            return taskClient.findTaskCanSignUserIds(bpmId);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "(取得可簽核人員失敗)";
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(errorMessage + "bpmId[" + bpmId + "], " + e.getMessage());
            log.error(errorMessage + "bpmId[" + bpmId + "]", e);
            throw new SystemOperationException(errorMessage, e.getMessage(), InfomationLevel.ERROR);
        }

    }

    public Boolean isSignUserWithException(String userId, String bpmId) throws SystemOperationException {
        List<String> ids = this.findTaskCanSignUserIdsWithException(bpmId);
        return ids.contains(userId);
    }

    /**
     * 是否為執行方簽核流程的待簽者
     * 
     * @param userId            user ID
     * @param execFlowSignInfos 執行方簽核流程資料
     * @return y/n
     * @throws UserMessageException 錯誤時拋出
     */
    public Boolean isExecFlowSignUser(String userId, List<WCExecManagerSignInfo> execFlowSignInfos) throws UserMessageException {

        // 防呆
        if (WkStringUtils.isEmpty(execFlowSignInfos)) {
            return false;
        }
        // 逐筆檢查
        for (WCExecManagerSignInfo signInfo : execFlowSignInfos) {
            if (this.isCanSignUser(userId, signInfo.getBpmId(), signInfo.getStatus())) {
                return true;
            }
        }
        return false;

    }
    //
    // /**
    // * 是否為需求方簽核流程的待簽者
    // *
    // * @param userId user ID
    // * @param reqFlowSignInfos 需求方簽核流程資料
    // * @return y/n
    // * @throws UserMessageException 錯誤時拋出
    // */
    // public Boolean isReqFlowSignUser(String userId, List<WCManagerSignInfo> reqFlowSignInfos) throws UserMessageException
    // {
    // // 防呆
    // if (WkStringUtils.isEmpty(reqFlowSignInfos)) {
    // return false;
    // }
    // // 逐筆檢查
    // for (WCManagerSignInfo signInfo : reqFlowSignInfos) {
    // if (this.isCanSignUser(userId, signInfo.getBpmId(), signInfo.getStatus())) {
    // return true;
    // }
    // }
    // return false;
    // }
    //
    // /**
    // * 是否為需求方簽核流程的待簽者
    // *
    // * @param userId user ID
    // * @param reqFlowSignInfos 需求方簽核流程資料
    // * @return y/n
    // * @throws UserMessageException 錯誤時拋出
    // */
    // public Boolean isReqUnitFlowSignUser(String userId, List<WCManagerSignInfo> reqFlowSignInfos) throws
    // UserMessageException {
    // // 防呆
    // if (WkStringUtils.isEmpty(reqFlowSignInfos)) {
    // return false;
    // }
    // // 逐筆檢查
    // for (WCManagerSignInfo signInfo : reqFlowSignInfos) {
    // if (this.isCanSignUser(userId, signInfo.getBpmId(), signInfo.getStatus())) {
    // return true;
    // }
    // }
    // return false;
    // }
    //

    public boolean isCanSignUser(String userId, String bpmId, BpmStatus signInfoStatus) throws UserMessageException {

        // 此邏輯目前無法確認 BPM 的各種狀態，故此優化取消
        // Set<BpmStatus> bpmStatus = Sets.newHashSet(
        // BpmStatus.WAITING_FOR_APPROVE,
        // BpmStatus.APPROVING,
        // BpmStatus.NEW_INSTANCE);
        //
        // // 流程狀態需為待簽
        // if (!bpmStatus.contains(signInfoStatus)) {
        // return false;
        // }

        // 排除已完成或無效的狀態
        Set<BpmStatus> bpmStatus = Sets.newHashSet(
                BpmStatus.APPROVED,
                BpmStatus.DELETE,
                BpmStatus.CLOSED,
                BpmStatus.INVALID);

        if (bpmStatus.contains(signInfoStatus)) {
            return false;
        }

        // 檢查是否為待簽人員
        try {
            boolean isCanSignUser = this.isSignUserWithException(userId, bpmId);
            if (isCanSignUser) {
                return true;
            }
        } catch (SystemOperationException e) {
            throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
        }

        return false;
    }

    /**
     * 建立模擬圖
     *
     * @param userId 查詢人員
     * @param bpmId
     * @return
     */
    public List<ProcessTaskBase> findSimulationChart(String userId, String bpmId) {
        try {
            if (Strings.isNullOrEmpty(bpmId)) {
                return Lists.newArrayList();
            }
            return taskClient.findSimulationChart(bpmId, userId);
        } catch (Exception e) {
            WkCommonUtils.logWithStackTraceByFullClassNamePrefix(
                    InfomationLevel.ERROR, "查詢水管圖失敗" + e.getMessage(), "com.cy");
        }
        return Lists.newArrayList();
    }

    /**
     * 取得水管圖 (多筆)
     * 
     * @param instanceIds
     * @return
     */
    public Map<String, List<ProcessTaskHistory>> findSimulationCharts(List<String> instanceIds) {
        if (WkStringUtils.isEmpty(instanceIds)) {
            return Maps.newHashMap();
        }
        try {
            return this.taskClient.findSimulationCharts(instanceIds);
        } catch (Exception e) {
            // exception 時, 不 throw
            String message = "call BPM-REST 失敗! (findSimulationCharts) " + e.getMessage();
            log.error(message, e);
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
        }
        return Maps.newHashMap();
    }

    /**
     * 尋找流程實例主要執行任務<br>
     * 可能會回傳NULL值
     *
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public ProcessTask findMajorTask(String bpmId) throws ProcessRestException {
        return taskClient.findMajorTask(bpmId);
    }

    /**
     * 查詢當前簽核人員名稱
     *
     * @param lastTask
     * @return
     */
    public String findTaskDefaultUserName(ProcessTaskBase lastTask) {
        try {
            // 最後節點如果為歷史任務的話就無待簽人員..
            if (lastTask instanceof ProcessTaskHistory) {
                return "";
            }
            if (!Strings.isNullOrEmpty(lastTask.getUserName())) {
                return lastTask.getUserName();
            }
            List<String> userids = this.findUserFromRole(lastTask.getRoleID());
            if (userids.isEmpty()) {
                return "";
            }
            User user = WkUserCache.getInstance().findById(userids.get(0));
            return user.getName();
        } catch (ProcessRestException ex) {
            log.warn("查詢當前簽核人員名稱 ERROR", ex);
        }
        return "";
    }

    /**
     * 尋找角色全部使用者
     *
     * @param roleId
     * @return
     * @throws ProcessRestException
     */
    public List<String> findUserFromRole(String roleId) throws ProcessRestException {
        return organizationClient.findUserFromRole(roleId);
    }

    /**
     * 執行簽核
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void sign(String userId, String bpmId) throws ProcessRestException {
        log.debug("BPM sign start  [{}, {}]", userId, bpmId);
        this.processClient.sign(new BpmSignTo(userId, bpmId));
        log.debug("BPM sign finish [{}, {}]", userId, bpmId);
    }

    /**
     * 執行簽核 (串簽)
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void signContinue(String userId, String bpmId) throws ProcessRestException {
        log.debug("BPM signContinue start  [{}, {}]", userId, bpmId);
        this.processClient.signContinue(new BpmSignTo(userId, bpmId));
        log.debug("BPM signContinue finish [{}, {}]", userId, bpmId);
    }

    /**
     * 執行復原
     *
     * @param userId
     * @param recoveryTask
     * @throws ProcessRestException
     */
    public void recovery(String userId, ProcessTaskHistory recoveryTask)
            throws ProcessRestException {
        log.debug("BPM recovery start  [{}, {}]", userId, recoveryTask.getInstanceID());
        this.processClient.recovery(userId, ProcessType.SIGNFLOW, recoveryTask);
        log.debug("BPM recovery finish [{}, {}]", userId, recoveryTask.getInstanceID());
    }

    /**
     * 執行退回
     *
     * @param userId
     * @param rollBackTask
     * @param comment
     * @throws ProcessRestException
     */
    public void rollBack(String userId, ProcessTaskHistory rollBackTask, String comment)
            throws ProcessRestException {
        log.debug("BPM roll back to [{}] start  [{}, {}]", rollBackTask.getExecutorID(), userId, rollBackTask.getInstanceID());

        this.processClient.rollback(userId, ProcessType.SIGNFLOW, rollBackTask, comment);
        log.debug("BPM roll back finish [{}, {}]", userId, rollBackTask.getInstanceID());
    }

    /**
     * 執行作廢
     *
     * @param userId
     * @param bpmId
     * @throws ProcessRestException
     */
    public void invalid(String userId, String bpmId) throws ProcessRestException {
        log.debug("BPM invalid start  [{}, {}]", userId, bpmId);
        this.processClient.terminate(bpmId, userId);
        log.debug("BPM invalid finish [{}, {}]", userId, bpmId);
    }

    /**
     * 取得當前BPM狀態
     *
     * @param userId
     * @param bpmId
     * @return
     * @throws ProcessRestException
     */
    public BpmStatus getBpmStatus(
            ManagerSingInfoType singInfoType,
            String userId, String bpmId, WCOpinionDescripeSetting wcOpinionDescripeSetting)
            throws ProcessRestException {
        List<ProcessTaskBase> tasks = this.findSimulationChart(userId, bpmId);
        // 新建檔，第一節點未簽核時
        ProcessTaskBase firstInfo = tasks.get(0);
        if (firstInfo == null) {
            log.warn("流程ID:" + bpmId + " 取得任務節點為空值，請檢查相關流程資料...");
            return null; // 如果有問題暫時回傳無意義值
        }

        // 核准，狀態為結束且最後節點簽完
        ProcessTaskBase majorTask = this.findMajorTask(bpmId);
        boolean isFinish = (majorTask == null);

        // 如果為歷史的任務代表已簽核過
        boolean firstDoSign = (firstInfo instanceof ProcessTaskHistory);
        if (!firstDoSign && !isFinish) {
            if (firstInfo.getRollbackInfo().isEmpty()
                    && (wcOpinionDescripeSetting == null
                            || Strings.isNullOrEmpty(wcOpinionDescripeSetting.getRollBackReason()))) {

                if (ManagerSingInfoType.CHECKMANAGERINFO.equals(singInfoType)) {
                    // 執行方流程
                    return BpmStatus.APPROVING;
                } else {
                    // 需求方流程
                    return BpmStatus.NEW_INSTANCE;
                }

            } else {
                return BpmStatus.RECONSIDERATION;
            }
        }
        if (!isFinish) {
            // 待簽核，流程未結束且，第一節點簽了，但第二節點未簽核時
            ProcessTaskBase secondInfo = tasks.get(1);
            boolean secondDoSign = (secondInfo instanceof ProcessTaskHistory);
            if (firstDoSign && !secondDoSign) {
                return BpmStatus.WAITING_FOR_APPROVE;
            }
        }

        ProcessTaskBase lastInfo = tasks.get(tasks.size() - 1);
        boolean lastDoSign = (lastInfo instanceof ProcessTaskHistory);
        if (lastDoSign && isFinish) {
            return BpmStatus.APPROVED;
        }
        // 簽核中
        return BpmStatus.APPROVING;
    }

    /**
     * 查詢傳入使用者往上簽和流程
     * 
     * @param userSid user sid
     * @return BpmRoleTo list (由下往上)
     * @throws UserMessageException 錯誤時拋出
     */
    public List<BpmRoleTo> prepareUserSignFlow(Integer userSid) throws UserMessageException {

        // ====================================
        // 查詢 user 資料
        // ====================================
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            String message = WkMessage.EXECTION + "(找不到建單者資料:[" + userSid + "])";
            log.error(message);
            throw new UserMessageException(message);
        }

        if (user.getPrimaryOrg() == null) {
            String message = WkMessage.EXECTION + "建單者:" + user.getName() + "(" + user.getSid() + ") 沒有歸屬單位";
            log.error(message);
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        if (user.getPrimaryOrg().getCompanySid() == null) {
            String message = WkMessage.EXECTION + "建單者:" + user.getName() + "(" + user.getSid() + ") 單位 (" + user.getPrimaryOrg().getSid() + " )沒有歸屬公司!";
            log.error(message);
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢歸屬公司資料
        // ====================================
        Org comp = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getCompanySid());
        if (comp == null) {
            String message = WkMessage.EXECTION + "未查到歸屬公司資料!(orgSid=" + user.getPrimaryOrg().getCompanySid() + ")";
            log.error(message);
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢主要角色
        // ====================================
        String mainBpmRoleID = "";
        try {
            mainBpmRoleID = this.organizationClient.findUserRoleByCompany(user.getId(), comp.getId());
            if (WkStringUtils.isEmpty(mainBpmRoleID)) {
                String message = WkMessage.EXECTION + "找不到使用者主要角色!(" + user.getId() + ")";
                log.error(message);
                throw new UserMessageException(message, InfomationLevel.ERROR);
            }
        } catch (ProcessRestException e) {
            log.error("查詢使用者BPM主要角色失敗!" + e.getMessage(), e);
            throw new UserMessageException("查詢使用者BPM主要角色失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

        BpmRoleTo mainRoleTo = null;
        try {
            RoleTo currRoleTo = this.organizationClient.findRoleToById(mainBpmRoleID);
            if (currRoleTo == null) {
                String message = WkMessage.EXECTION + "找不到BPM角色資料!(" + user.getId() + ")";
                log.error(message);
                throw new UserMessageException(message, InfomationLevel.ERROR);
            }

            // RoleTo 轉 BpmRoleTo
            mainRoleTo = new BpmRoleTo(currRoleTo);
        } catch (ProcessRestException e) {
            log.error("查詢BPM主要角色失敗!" + e.getMessage(), e);
            throw new UserMessageException("查詢BPM主要角色失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

        // ====================================
        // 遞迴往上查詢角色
        // ====================================
        List<BpmRoleTo> roleTos = Lists.newArrayList();
        this.prepareSignFlow_recursion(mainRoleTo, roleTos);

        return roleTos;
    }

    /**
     * 遞迴往上查詢角色
     * 
     * @param roleTo
     * @param roleTos
     * @throws UserMessageException
     * @throws ProcessRestException
     */
    private void prepareSignFlow_recursion(BpmRoleTo roleTo, List<BpmRoleTo> roleTos) throws UserMessageException {

        try {
            List<String> userIdsByRole = this.findUserFromRole(roleTo.getId());
            if (WkStringUtils.notEmpty(userIdsByRole)) {
                roleTo.setRoleUserIds(userIdsByRole);
            }
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "(查詢BPM角色人員失敗)";
            log.error(errorMessage + " roleID:[{}]", roleTo.getId(), e);
            throw new UserMessageException("查詢BPM主要角色失敗!" + e.getMessage(), InfomationLevel.ERROR);
        }

        roleTos.add(roleTo);
        if (WkStringUtils.isEmpty(roleTo.getParentRoleId())) {
            return;
        }
        RoleTo parentRoleTo;
        try {
            parentRoleTo = this.organizationClient.findRoleToById(roleTo.getParentRoleId());
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.EXECTION + "(查詢BPM人員角色失敗)";
            log.error(errorMessage + " roleID:[{}]", roleTo.getId(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
        if (parentRoleTo == null) {
            log.warn("找不到BPM角色資訊!roleID:[{}]", roleTo.getParentRoleId());
            return;
        }
        this.prepareSignFlow_recursion(new BpmRoleTo(parentRoleTo), roleTos);
    }
}
