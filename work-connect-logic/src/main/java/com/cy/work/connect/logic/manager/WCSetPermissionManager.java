/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.repository.WCSetPermissionRepository;
import com.cy.work.connect.vo.WCSetPermission;
import com.cy.work.connect.vo.enums.TargetType;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 設定權限 Manager
 *
 * @author jimmy_chou
 */
@Component
public class WCSetPermissionManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1795044769810577130L;

    private static WCSetPermissionManager instance;
    /**
     * 設定權限 Repository
     */
    @Autowired
    private WCSetPermissionRepository permissionRepository;

    public static WCSetPermissionManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCSetPermissionManager.instance = this;
    }

    /**
     * 查詢所有 設定權限
     *
     * @return
     */
    public List<WCSetPermission> findAll() {
        return permissionRepository.findAll();
    }

    /**
     * 以公司SID 查詢 設定權限
     *
     * @return
     */
    public List<WCSetPermission> findByCompSid(Integer compSid) {
        if (compSid == null) {
            return Lists.newArrayList();
        }
        return permissionRepository.findByCompSid(compSid);
    }

    /**
     * 以公司SID、設定來源清單 查詢 設定權限
     *
     * @param compSid
     * @param targetTypes
     * @return
     */
    public List<WCSetPermission> findByCompSidAndTargetTypeIn(
        Integer compSid, List<TargetType> targetTypes) {
        if (compSid == null || targetTypes == null || targetTypes.isEmpty()) {
            return Lists.newArrayList();
        }
        return permissionRepository.findByCompSidAndTargetTypeIn(compSid, targetTypes);
    }

    /**
     * 以SID ˊ查詢
     *
     * @param sid
     * @return
     */
    public WCSetPermission findBySid(Long sid) {
        if (sid == null) {
            // log.warn("傳入 sid 為 null");
            return null;
        }
        return permissionRepository.findBySid(sid);
    }

    /**
     * 以公司SID、設定目標SID、設定來源 查詢
     *
     * @param compSid
     * @param targetSid
     * @param targetType
     * @return
     */
    public WCSetPermission findPermissionByCompSidAndTargetSidAndTargetType(
        Integer compSid, String targetSid, TargetType targetType) {
        if (compSid == null || WkStringUtils.isEmpty(compSid) || targetType == null) {
            return null;
        }
        return permissionRepository.findPermissionByCompSidAndTargetSidAndTargetType(
            compSid, targetSid, targetType);
    }

    /**
     * 以公司SID、群組SID 查詢
     *
     * @param compSid
     * @param groupSid
     * @return
     */
    public List<WCSetPermission> findPermissionByCompSidAndGroupSid(Integer compSid,
        Long groupSid) {
        if (compSid == null || groupSid == null) {
            return Lists.newArrayList();
        }
        return permissionRepository.findPermissionByCompSidAndGroupSid(compSid, groupSid);
    }

    /**
     * @param permission
     * @param loginUserSid
     * @param loginUserCompSid
     * @throws Exception
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void saveSetting(
        WCSetPermission permission, Integer loginUserSid, Integer loginUserCompSid)
        throws Exception {

        Date sysDate = new Date();

        // ====================================
        // 準備主檔 DB entities
        // ====================================

        if (WkStringUtils.isEmpty(permission.getSid())) {
            // 為新增

            // 公司別 （避免不同的公司混用）
            permission.setCompSid(loginUserCompSid);
            // 建立者
            permission.setCreateUser(loginUserSid);
            // 建立時間
            permission.setCreateDate(sysDate);
            // 狀態
            permission.setStatus(Activation.ACTIVE);

        } else {
            // 更新

            // 更新者
            permission.setUpdateUser(loginUserSid);
            // 更新時間
            permission.setUpdateDate(sysDate);
            // 狀態
            permission.setStatus(Activation.ACTIVE);
        }

        // ====================================
        // save wc_config_group
        // ====================================
        this.permissionRepository.save(permission);
    }

    /**
     * 設定群組 抓取擁有權限使用者
     *
     * @param loginCompSid
     * @param groupSids
     * @return
     */
    public List<Integer> groupPermissionUserSids(Integer loginCompSid, List<Long> groupSids) {
        List<Integer> permissionUserSids =
            groupSids.stream()
                .flatMap(
                    each -> WCSetGroupManager.getInstance().getGroupUsers(each, loginCompSid)
                        .stream())
                .collect(Collectors.toList());
        if (permissionUserSids == null) {
            return Lists.newArrayList();
        }

        return permissionUserSids;
    }
}
