/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Role;
import com.cy.work.connect.repository.SpecificRoleForwardDepRepository;
import com.cy.work.connect.vo.SpecificRoleForwardDep;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 轉寄部門
 *
 * @author brain0925_liao
 */
@Component
public class SpecificRoleForwardDepManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8360298327860815771L;

    @Autowired
    private SpecificRoleForwardDepRepository specificRoleForwardDepRepository;

    /**
     * 取得可轉寄部門 By 角色Sid
     *
     * @param roleSID 角色Sids
     * @return
     */
    public List<SpecificRoleForwardDep> getSpecificRoleForwardDepByRoleSid(List<Long> roleSID) {
        List<Role> roles = Lists.newArrayList();
        roleSID.forEach(
            item -> {
                roles.add(new Role(item));
            });
        List<SpecificRoleForwardDep> results = specificRoleForwardDepRepository.findByRoleIn(roles);
        List<SpecificRoleForwardDep> realResults =
            results.stream()
                .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                .collect(Collectors.toList());
        return realResults;
    }
}
