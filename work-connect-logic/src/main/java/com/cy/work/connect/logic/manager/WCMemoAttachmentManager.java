/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.helper.WCMemoTraceHelper;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.repository.WCMemoAttachmentRepository;
import com.cy.work.connect.vo.WCMemoAttachment;
import com.cy.work.connect.vo.WCMemoMaster;
import com.cy.work.connect.vo.WCMemoTrace;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 備忘錄附件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMemoAttachmentManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3583930990896649730L;

    @Autowired
    private WCMemoAttachmentRepository wcMemoAttachmentRepository;
    @Autowired
    private WCMemoMasterManager wcMemoMasterManager;
    @Autowired
    private WCMemoTraceHelper wcMemoTraceHelper;
    @Autowired
    private WCMemoTraceManager wcMemoTraceManager;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;

    /**
     * 取得附件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public WCMemoAttachment findBySid(String att_sid) {
        try {
            return wcMemoAttachmentRepository.findOne(att_sid);
        } catch (Exception e) {
            log.warn("findBySid Error : " + e, e);
            return null;
        }
    }

    /**
     * 取得資料庫所有附件資料
     *
     * @return
     */
    public List<WCMemoAttachment> findAll() {
        try {
            return wcMemoAttachmentRepository.findAll();
        } catch (Exception e) {
            log.warn("findAll Error : " + e, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得附件List By 備忘錄Sid
     *
     * @param wcMemoSid
     * @return
     */
    public List<WCMemoAttachment> getWCAttachmentByWcMemoSid(String wcMemoSid) {
        try {
            return wcMemoAttachmentRepository.getWCAttachmentByWCMemoSid(wcMemoSid);
        } catch (Exception e) {
            log.warn("getWCAttachmentByWcMemoSid Error : " + e, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 建立附件資料
     *
     * @param wcMemoAttachment 附件物件
     * @param userSid          建立者
     * @return
     */
    public WCMemoAttachment create(WCMemoAttachment wcMemoAttachment, Integer userSid) {
        try {
            wcMemoAttachment.setCreate_usr_sid(userSid);
            wcMemoAttachment.setCreate_dt(new Date());
            wcMemoAttachment = wcMemoAttachmentRepository.save(wcMemoAttachment);
            return wcMemoAttachment;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 更新備忘錄與附件關聯及附件資訊
     *
     * @param wcMemoSid      備忘錄Sid
     * @param wcMemoNo       備忘錄單號
     * @param description    附件描述
     * @param userSid        更新者
     * @param attachment_sid 附件Sid
     */
    public void updateWCMemoMappingInfo(
        String wcMemoSid,
        String wcMemoNo,
        String description,
        Integer userSid,
        String attachment_sid) {
        try {
            wcMemoAttachmentRepository.updateWCMemoMappingInfo(
                wcMemoSid, wcMemoNo, description, userSid, new Date(), attachment_sid);
        } catch (Exception e) {
            log.warn("updateWCMemoMappingInfo Error : " + e, e);
        }
    }

    /**
     * 更新附件
     *
     * @param wcMemoAttachment 附件物件
     * @param userSid          更新者Sid
     * @return
     */
    public WCMemoAttachment update(WCMemoAttachment wcMemoAttachment, Integer userSid) {
        try {
            wcMemoAttachment.setUpdate_dt(new Date());
            wcMemoAttachment.setUpdate_usr_sid(userSid);
            wcMemoAttachment = wcMemoAttachmentRepository.save(wcMemoAttachment);
            return wcMemoAttachment;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    @Transactional(rollbackForClassName = {"Exception"})
    public void deleteAttachment(AttachmentVO attachmentVO, Integer loginUserSid, String memoSid) {
        boolean result = this.deleteDBAttachment(attachmentVO.getAttSid(), loginUserSid);
        Preconditions.checkState(result, "刪除附件失敗");
        WCMemoMaster wc = wcMemoMasterManager.findBySid(memoSid);
        WCMemoTrace deleteWCTrace =
            wcMemoTraceHelper.getWCMemoTrace_DEL_ATTACHMENT(
                attachmentVO.getAttSid(),
                attachmentVO.getAttName(),
                Integer.valueOf(attachmentVO.getCreateUserSId()),
                loginUserSid,
                memoSid,
                wc.getWcMemoNo());
        wcMemoTraceManager.create(deleteWCTrace, loginUserSid);
    }

    /**
     * 刪除附件
     *
     * @param att_sid        附件Sid
     * @param delete_userSid 刪除者Sid
     * @return
     */
    private boolean deleteDBAttachment(String att_sid, Integer delete_userSid) {
        WCMemoAttachment wa = wcMemoAttachmentRepository.findOne(att_sid);
        if (wa == null) {
            return false;
        }
        wa.setStatus(Activation.INACTIVE);
        wa = this.update(wa, delete_userSid);
        return wa != null;
    }

    /**
     * 將附件物件轉換成附件界面物件
     *
     * @param wcMemoAttachment 附件物件
     * @return
     */
    public AttachmentVO transWCMemoAttachmentToAttachmentVO(WCMemoAttachment wcMemoAttachment) {
        AttachmentVO av =
            new AttachmentVO(
                wcMemoAttachment.getSid(),
                wcMemoAttachment.getFile_name(),
                wcMemoAttachment.getDescription(),
                wcMemoAttachment.getFile_name(),
                true,
                String.valueOf(wcMemoAttachment.getCreate_usr_sid()));
        av.setCreateTime(
            ToolsDate.transDateToString(
                SimpleDateFormatEnum.SdfDate.getValue(), wcMemoAttachment.getCreate_dt()));
        av.setParamCreateTime(
            ToolsDate.transDateToString(
                SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(),
                wcMemoAttachment.getCreate_dt()));
        av.setStatus(wcMemoAttachment.getStatus());
        try {
            User user = userManager.findBySid(wcMemoAttachment.getCreate_usr_sid());
            av.setCreateUserSId(String.valueOf(user.getSid()));
            Org dep = orgManager.findBySid(user.getPrimaryOrg().getSid());
            String userInfo =
                WkOrgUtils.prepareBreadcrumbsByDepName(dep.getSid(), OrgLevel.DIVISION_LEVEL, true,
                    "-")
                    + "-"
                    + user.getName();
            av.setUserInfo(userInfo);
        } catch (Exception e) {
            log.warn("transWCMemoAttachmentToAttachmentVO", e);
        }
        return av;
    }

    /**
     * 取得附件界面物件By 備忘錄Sid
     *
     * @param wcMemoSid 備忘錄Sid
     * @return
     */
    public List<AttachmentVO> getAttachmentsByWCMemoSid(String wcMemoSid) {
        List<WCMemoAttachment> as = this.getWCAttachmentByWcMemoSid(wcMemoSid);
        if (as == null) {
            return Lists.newArrayList();
        }
        List<AttachmentVO> avs = Lists.newArrayList();
        as.forEach(
            item -> {
                avs.add(transWCMemoAttachmentToAttachmentVO(item));
            });
        return avs;
    }
}
