package com.cy.work.connect.logic.cache;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCTransSpecialPermissionRepository;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 轉單特殊權限擁有部門 Cache
 */
@Slf4j
@Component
public class WCTransSpecialPermissionCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1554263508223393069L;

    @Autowired
    private WCTransSpecialPermissionRepository wcTransSpecialPermissionRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    // private Map<String, WCTransSpecialPermission> wcTransSpecialPermissionMap;

    private Set<Integer> transSpecialPermissionDepSids = Sets.newHashSet();

    public WCTransSpecialPermissionCache() {
        startController = Boolean.TRUE;
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            this.updateCache();
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay = ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        log.debug("建立轉單特殊權限擁有部門Cache");
        try {
            Set<Integer> tempDepSids = WkCommonUtils.safeStream(this.wcTransSpecialPermissionRepository.findAll())
                    .filter(each -> each != null && each.getDep_sid() != null)
                    .map(each -> each.getDep_sid())
                    .collect(Collectors.toSet());

            // 查詢所有子單位
            Set<Integer> childSids = WkOrgCache.getInstance().findAllChildSids(tempDepSids);
            if (WkStringUtils.notEmpty(tempDepSids)) {
                tempDepSids.addAll(childSids);
            }

            this.transSpecialPermissionDepSids = tempDepSids;

        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立轉單特殊權限擁有部門Cache結束");
    }

    public Set<Integer> getAllWCTransSpecialPermission() {
        // 複製一份，以免被外部修改
        return this.transSpecialPermissionDepSids.stream().collect(Collectors.toSet());
    }
}
