/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkJsoupUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCManagerSignInfoCache;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.helper.BrBpmLevelSettingHelper;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.repository.WCAgainTraceRepository;
import com.cy.work.connect.repository.WCAttachmentRepository;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.repository.WCTraceRepository;
import com.cy.work.connect.vo.WCAgainTrace;
import com.cy.work.connect.vo.WCAttachment;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.converter.to.CategoryTagTo;
import com.cy.work.connect.vo.converter.to.RRcordTo;
import com.cy.work.connect.vo.converter.to.ReplyRecordTo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.FlowType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.cy.work.connect.vo.enums.WCReadStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMasterCustomLogicManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5860698348106406980L;
    
    public static WCMasterCustomLogicManager instance;
    public static WCMasterCustomLogicManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCMasterCustomLogicManager.instance = this;
    }

    @Autowired
    private WCMasterRepository wcMasterRepository;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;
    @Autowired
    private WCTraceRepository wcTraceRepository;
    @Autowired
    private WCAgainTraceRepository wcAgainTraceRepository;
    @Autowired
    private WkJsoupUtils jsoupUtils;
    @Autowired
    private WCAttachmentRepository wcAttachmentRepository;
    @Autowired
    private WCManagerSignInfoManager wcManagerSignInfoManager;

    @Autowired
    private WCAlertManager alertManager;
    @Autowired
    private WCTraceManager wcTraceManager;

    @Autowired
    private CountUnreadReplyLogic countUnreadReplyLogic;

    @Autowired
    private WCManagerSignInfoCache wcManagerSignInfoCache;

    @Autowired
    private BpmManager bpmManager;

    @Autowired
    private WCTagManager wcTagManager;

    @Autowired
    @Qualifier(ConnectConstants.CONNECT_JDBC_TEMPLATE)
    private JdbcTemplate jdbc;

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private BrBpmLevelSettingHelper brBpmLevelSettingHelper;

    /**
     * 建立工作聯絡單
     *
     * @param master         聯絡單主檔資料
     * @param loginUserSid   登入者SID
     * @param attachmentVOs  附件
     * @param addUserFlowSid 加簽人員SID
     * @return
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCMaster createMaster(
            WCMaster master,
            Integer loginUserSid,
            List<AttachmentVO> attachmentVOs,
            List<Integer> addUserFlowSid) throws UserMessageException {

        // ====================================
        // insert wc_master
        // ====================================
        WCMaster wcMaster = this.createMaster(master, loginUserSid);

        // ====================================
        // 建立需求單位流程 （BPM）
        // ====================================
        String bpmId = this.wcManagerSignInfoManager.createBpmFlow(master, loginUserSid);

        // ====================================
        // 建立需求方簽核資訊 wc_manager_sign_info、wc_manager_sign_info_detail
        // ====================================
        this.wcManagerSignInfoManager.createSignInfo(master, bpmId);

        // ====================================
        // 更新單位會簽
        // ====================================
        try {
            this.wcManagerSignInfoManager.saveCounterSignInfo(
                    wcMaster.getSid(), wcMaster.getWc_no(), Lists.newArrayList(), addUserFlowSid,
                    loginUserSid);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(建立需求流程會簽資料失敗!)";
            log.error(errorMessage + e.getMessage(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新附件資訊
        // ====================================
        if (WkStringUtils.notEmpty(attachmentVOs)) {
            for (AttachmentVO attachmentVO : attachmentVOs) {
                this.wcAttachmentRepository.updateWCMasterMappingInfo(
                        wcMaster.getSid(),
                        wcMaster.getWc_no(),
                        attachmentVO.getAttDesc(),
                        loginUserSid,
                        new Date(),
                        attachmentVO.getAttSid());
            }
        }

        return wcMaster;
    }

    /**
     * 重新建立BPM 流程
     *
     * @param master       聯絡單主檔資料
     * @param loginUserSid 登入者SID
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void reCreateBPMSignFlow(
            String wcSid,
            Integer loginUserSid)
            throws UserMessageException {

        log.info("重新產生需求方簽核流程!wcSid:[{}]", wcSid);

        // ====================================
        // 查詢主檔
        // ====================================
        WCMaster wcMaster = this.wcMasterRepository.findBySid(wcSid);
        if (wcMaster == null) {
            String message = WkMessage.NEED_RELOAD + "(找不到聯絡單資料)";
            log.error(message + "wcSid:[{}]", wcSid);
            throw new UserMessageException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // 查詢需求單位簽核流程
        // ====================================
        // 查詢
        Optional<WCManagerSignInfo> optional;
        try {
            optional = this.wcManagerSignInfoManager.findReqUnitFlowSignInfo(wcSid);
        } catch (SystemOperationException e) {
            throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
        }
        // 判斷取不到資料
        if (!optional.isPresent()) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到需求單位簽核流程資料)";
            log.warn(errorMessage + " wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        WCManagerSignInfo wcManagerSignInfo = optional.get();

        // ====================================
        // 依據簽核別, 建立需求單位流程 （BPM）
        // ====================================
        String newBpmId = "";
        try {
            newBpmId = this.wcManagerSignInfoManager.createBpmFlow(wcMaster, loginUserSid);
        } catch (Exception e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(建立簽核流程失敗!)";
            log.error(errorMessage + " wcSid:[{}],[{}]", wcSid, e.getMessage(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 作廢原有BPM流程
        // ====================================
        // 取得 簽核 user
        User user = WkUserCache.getInstance().findBySid(wcManagerSignInfo.getUser_sid());
        try {
            this.bpmManager.invalid(user.getId(), wcManagerSignInfo.getBpmId());
        } catch (Exception e) {
            // 作廢失敗不影響流程，不拋 exception 中斷作業
            log.error("作廢舊有流程失敗,但不影響作業 BpmId:[{}],[{}]", wcManagerSignInfo.getBpmId(), e.getMessage(), e);
        }

        // ====================================
        // 更新 簽核檔資料 (wc_manager_sign_info)
        // ====================================
        wcManagerSignInfo.setStatus(BpmStatus.NEW_INSTANCE);
        wcManagerSignInfo.setSignType(SignType.SIGN);
        wcManagerSignInfo.setCreate_dt(new Date());
        wcManagerSignInfo.setUser_sid(wcMaster.getCreate_usr());
        wcManagerSignInfo.setDep_sid(wcMaster.getDep_sid());

        wcManagerSignInfo.setBpmId(newBpmId);

        // ====================================
        // 同步BPM 水管圖 後, 建立需求方簽核資訊檔
        // insert wc_manager_sign_info、wc_manager_sign_info_detail
        // ====================================
        try {
            wcManagerSignInfo = this.wcManagerSignInfoManager.doSyncBpmData(wcManagerSignInfo, null, null);
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(同步簽核資料失敗!)";
            log.error(errorMessage + " wcSid:[{}],[{}]", wcSid, e.getMessage(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 更新Cache By 需求單位簽核物件
        // ====================================
        this.wcManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(wcManagerSignInfo);
    }

    /**
     * 建立工作聯絡單
     *
     * @param master        工作聯絡單物件
     * @param createUserSid 建立者Sid
     * @return
     */
    private WCMaster createMaster(WCMaster master, Integer createUserSid) {
        master.setCreate_usr(createUserSid);
        Date createDate = new Date();
        master.setCreate_dt(createDate);
        master.setUpdate_dt(createDate);
        return wcMasterRepository.save(master);
    }

    /**
     * 更新主檔狀態
     *
     * @param sid     Sid
     * @param userSid 更新者Sid
     * @param status  狀態
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateWCStatus(String sid, Integer userSid, WCStatus status) {
        try {
            StringBuilder sql = new StringBuilder();
            List<Object> argsList = Lists.newArrayList();
            sql.append("UPDATE wc_master");
            sql.append(" SET wc_status = ? ");
            argsList.add(status.name());
            sql.append(" ,update_usr = ? ");
            argsList.add(userSid);
            sql.append(" ,update_dt = ? ");
            argsList.add(new Date());
            if (WCStatus.EXEC_FINISH.equals(status) || WCStatus.APPROVED.equals(status)) {
                sql.append(" ,require_establish_dt = ? ");
                argsList.add(new Date());
            }
            sql.append(" WHERE wc_sid = ? ");
            argsList.add(sid);
            this.jdbc.update(sql.toString(), argsList.toArray());

        } catch (Exception e) {
            log.warn("updateWCStatus ERROR", e);
        }
    }

    /**
     * 更新主檔狀態
     *
     * @param sid      Sid
     * @param userSid  使用者Sid
     * @param wcStatus 狀態
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateStatus(String sid, Integer userSid, WCStatus wcStatus) {
        // 查詢主檔
        WCMaster wc = this.wcMasterRepository.findBySid(sid);

        // 狀態一致時不更新
        if (WkCommonUtils.compareByStr(wc.getWc_status(), wcStatus)) {
            return;
        }

        WCStatus oldWCStatus = wc.getWc_status();

        // update
        this.updateWCStatus(sid, userSid, wcStatus);
        em.refresh(wc);

        log.info("單號[" + wc.getWc_no() + "] 更新狀態:[{}->{}] ({})",
                oldWCStatus, wcStatus, wcStatus.getVal());
    }

    /**
     * 更新工作聯絡單狀態為核准
     *
     * @param sid
     * @param userSid
     * @param isReqSignFinish
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToApproved(String sid, Integer userSid, boolean isReqSignFinish) {
        // ====================================
        // 更新工作聯絡單狀態為核准 wc_master
        // ====================================
        WCMaster wc = wcMasterRepository.findBySid(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為核准");
        this.updateWCStatus(sid, userSid, WCStatus.APPROVED);
        em.refresh(wc);

        // ====================================
        // 更新閱讀記錄
        // ====================================
        this.updateWaitReadWCMaster(sid, userSid);

        // ====================================
        // 建立追蹤資料 wc_trace
        // ====================================
        // 準備資料
        WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_MODIFY_APPROVED(
                userSid,
                wc.getSid(),
                wc.getWc_no(),
                true);
        // insert wc_trace
        this.wcTraceManager.create(wcTrace, userSid);

    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToExecFinish(String wcSid, Integer userSid) {

        // 判斷是否需要 update
        WCMaster wc = wcMasterRepository.findBySid(wcSid);
        if (WCStatus.EXEC_FINISH.equals(wc.getWc_status())) {
            return;
        }

        // 更新主檔
        this.updateWCStatus(wcSid, userSid, WCStatus.EXEC_FINISH);
        em.refresh(wc);
        log.debug("單號[" + wc.getWc_no() + "] 變為已執行完成");

        // 寫追蹤
        WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_EXEC_FINISH_STATUS(
                userSid, wc.getSid(), wc.getWc_no());
        this.wcTraceManager.create(wcTrace, userSid);

        this.updateWaitReadWCMaster(wcSid, userSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToExec(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為執行中");
        this.updateWCStatus(sid, userSid, WCStatus.EXEC);
        em.refresh(wc);
        this.updateWaitReadWCMaster(sid, userSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToClose(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為結案");
        this.updateWCStatus(sid, userSid, WCStatus.CLOSE);
        em.refresh(wc);
        this.updateWaitReadWCMaster(sid, userSid);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateToCloseStop(String sid, Integer userSid) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        log.debug("單號[" + wc.getWc_no() + "] 變為結案(終止)");
        this.updateWCStatus(sid, userSid, WCStatus.CLOSE_STOP);
        em.refresh(wc);
        this.updateWaitReadWCMaster(sid, userSid);
    }

    /**
     * 更新工作聯絡單(主題內容備註)
     *
     * @param sid                 工作聯絡單Sid
     * @param title
     * @param content
     * @param remark
     * @param selCategorySids
     * @param endStarJson
     * @param legitimateRangeJson
     * @param userSid             更新者Sid
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void updateContentWCMaster(
            String sid,
            String title,
            String content,
            String remark,
            List<String> selCategorySids,
            String endStarJson,
            String legitimateRangeJson,
            Integer userSid) throws UserMessageException {

        List<WCTrace> wcTraces = Lists.newArrayList();

        // ====================================
        // 查詢目前主檔資料
        // ====================================
        WCMaster wc = this.wcMasterRepository.findBySid(sid);
        if (wc == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "載入工作聯絡單失敗";
            log.error(errorMessage + "wcSid:[{}]", sid);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        Set<WCStatus> notEditStats = Sets.newHashSet(WCStatus.INVALID, WCStatus.CLOSE, WCStatus.CLOSE_STOP);
        if (notEditStats.contains(wc.getWc_status())) {
            String errorMessage = WkMessage.NEED_RELOAD + "單據已[" + wc.getWc_status().getVal() + "], 不可編輯！";
            log.error(errorMessage + "wcSid:[{}]", sid);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 判斷簽核層級是否有異動(若有須更新需求方簽核資訊)
        // ====================================
        // 舊的簽核層級
        List<WCTag> oldWcTags = this.wcTagManager.findBySidIn(wc.getCategoryTagTo().getTagSids());
        Org comp = WkOrgCache.getInstance().findBySid(wc.getComp_sid());
        Map<FlowType, Integer> bpmLevelMapByFlowType = Maps.newConcurrentMap();
        try {
            bpmLevelMapByFlowType = brBpmLevelSettingHelper.getFlowTypeBpmLevelMapByCompId(comp.getId());
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "(取得PPS6簽核層級失敗)";
            log.error(errorMessage + "[" + e.getMessage() + "]", e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
        FlowType oldTagFlowType = this.wcManagerSignInfoManager.findTopFlowType(oldWcTags, bpmLevelMapByFlowType);

        // 本次異動後的簽核層級
        List<WCTag> newWcTags = this.wcTagManager.findBySidIn(selCategorySids);
        FlowType tagFlowType = this.wcManagerSignInfoManager.findTopFlowType(newWcTags, bpmLevelMapByFlowType);

        // 檢測簽核層級是否有變更,若有變更需加入追蹤紀錄(更新需求方簽核資訊)
        if (!oldTagFlowType.equals(tagFlowType)) {

            log.info("簽核層級異動，重建簽核流程:[{}] -> [{}] , wcNo:[{}]",
                    oldTagFlowType,
                    tagFlowType,
                    wc.getWc_no());

            try {
                wc.setCategoryTagTo(new CategoryTagTo(selCategorySids));
                this.reCreateBPMSignFlow(wc.getSid(), userSid);
            } catch (Exception e) {
                String errorMessage = WkMessage.EXECTION + "(重建簽核流程失敗)";
                log.error(errorMessage + "[" + e.getMessage() + "]", e);
                throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
            }

            // 追蹤-簽核層級異動
            wcTraces.add(
                    wcTraceCustomLogicManager.getWCTrace_MODIFY_FLOWTYPE(
                            userSid, wc.getSid(), wc.getWc_no()));
        }

        // 檢測標題是否有變更,若有變更需加入追蹤紀錄
        if (!wc.getTheme().equals(title)) {
            wcTraces.add(
                    wcTraceCustomLogicManager.getWCTrace_MODIFY_THEME(
                            wc.getTheme(), title, userSid, wc.getSid(), wc.getWc_no()));
        }
        // 檢測內容是否有變更,若有變更需加入追蹤紀錄
        if (!wc.getContent_css().equals(content)) {
            wcTraces.add(
                    wcTraceCustomLogicManager.getWCTrace_MODIFY_CONTNET(
                            wc.getContent(), content, userSid, wc.getSid(), wc.getWc_no()));
        }
        // 檢測備註是否有變更,若有變更需加入追蹤紀錄
        if (!wc.getMemo_css().equals(remark)) {
            wcTraces.add(
                    wcTraceCustomLogicManager.getWCTrace_MODIFY_MEMO(
                            wc.getMemo(), remark, userSid, wc.getSid(), wc.getWc_no()));
        }
        wc.setTheme(title);
        wc.setContent_css(content);
        wc.setContent(jsoupUtils.clearCssTag(content));
        wc.setMemo_css(remark);
        wc.setMemo(jsoupUtils.clearCssTag(remark));
        wc.getCategoryTagTo().setTagSids(selCategorySids);
        wc.setLegitimateRangeDate(legitimateRangeJson);
        wc.setStartEndDate(endStarJson);

        wc = this.updateWCMaster(wc, userSid);
        em.flush();

        for (WCTrace wcTrace : wcTraces) {
            this.wcTraceManager.create(wcTrace, userSid);
        }

        if (!wcTraces.isEmpty()) {
            this.updateWaitReadWCMaster(wc.getSid(), userSid);
        }
        em.refresh(wc);
    }

    /**
     * 回復工作聯絡單
     *
     * @param sid       工作聯絡單Sid
     * @param userSid   回覆者Sid
     * @param replySid  回覆Sid
     * @param content   回覆內容
     * @param traceType 回覆類型
     * @return
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCMaster replyWCMaster(
            String sid, Integer userSid, String replySid, String content, WCTraceType traceType) {
        WCMaster wc = wcMasterRepository.findBySid(sid);
        Preconditions.checkState(wc != null, "載入工作聯絡單失敗！");
        Preconditions.checkState(!(wc.getWc_status().equals(WCStatus.CLOSE)), "單據已結案,不可回覆！");
        Preconditions.checkState(!(wc.getWc_status().equals(WCStatus.CLOSE_STOP)), "單據已結案,不可回覆！");
        Preconditions.checkState(!(wc.getWc_status().equals(WCStatus.INVALID)), "單據已作廢,不可回覆！");
        WCTrace wcTrace = null;
        if (Strings.isNullOrEmpty(replySid)) {
            wcTrace = wcTraceCustomLogicManager.getWCTrace(
                    content, userSid, wc.getSid(), wc.getWc_no(), traceType);
        } else {
            wcTrace = wcTraceRepository.findBySid(replySid);
            wcTrace.setWc_trace_content(jsoupUtils.clearCssTag(content));
            wcTrace.setWc_trace_content_css(content);
        }
        if (Strings.isNullOrEmpty(replySid)) {

            wcTrace = this.wcTraceManager.create(wcTrace, userSid);

            this.createAlert(wc, wcTrace, userSid);
        } else {
            this.updateWCTrace(wcTrace, userSid);
        }
        this.updateWaitReadWCMaster(wc.getSid(), userSid);

        // 相關者未閱讀數量 + 1
        this.countUnreadReplyLogic.processPlusOne(wc.getSid(), userSid);

        this.em.refresh(wc);

        return wc;
    }

    /**
     * 變更使用者未閱讀的回覆筆數
     *
     * @param sid           工作聯絡單Sid
     * @param updateRecords 使用者未閱讀的回覆筆數
     */
    public void updateWCReplyNotRead(String sid, List<ReplyRecordTo> updateRecords) {
        try {
            StringBuilder sql = new StringBuilder();
            List<Object> argsList = Lists.newArrayList();
            String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(updateRecords);
            sql.append("UPDATE wc_master");
            sql.append(" SET reply_not_read = ? ");
            argsList.add(json);
            sql.append(" WHERE wc_sid = ? ");
            argsList.add(sid);
            this.jdbc.update(sql.toString(), argsList.toArray());
        } catch (Exception e) {
            log.warn("updateWCReplyNotRead ERROR", e);
        }
    }

    /**
     * 建立訊息
     *
     * @param obj
     * @param trace
     */
    private void createAlert(WCMaster obj, WCTrace trace, Integer loginUserSid) {
        // List<Integer> recipients = Lists.newArrayList();
        if (WCTraceType.REPLY.equals(trace.getWc_trace_type())) {
            if (!loginUserSid.equals(obj.getCreate_usr())) {
                this.alertManager.createAlertsByExecReply(
                        obj.getSid(),
                        obj.getWc_no(),
                        Lists.newArrayList(obj.getCreate_usr()),
                        obj.getTheme(),
                        loginUserSid);
            }
        }
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void deleteAttachment(AttachmentVO attachmentVO, Integer loginUserSid, String wc_Sid) {
        boolean result = this.deleteDBAttachment(attachmentVO.getAttSid(), loginUserSid);
        Preconditions.checkState(result, "刪除附件失敗");
        WCMaster wc = wcMasterRepository.findOne(wc_Sid);
        WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_DEL_ATTACHMENT(
                attachmentVO.getAttSid(),
                attachmentVO.getAttName(),
                Integer.valueOf(attachmentVO.getCreateUserSId()),
                loginUserSid,
                wc_Sid,
                wc.getWc_no());

        this.wcTraceManager.create(wcTrace, loginUserSid);

    }

    public void saveViewPerson(
            WCMaster wc, Integer loginUserSid, List<User> addUser, List<User> removeUser) {
        // WCMaster wc = this.updateWCMaster(wcMaster, loginUserSid);
        if ((addUser != null && !addUser.isEmpty()) || (removeUser != null
                && !removeUser.isEmpty())) {

            WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_ADD_VIEW_PERSON(
                    wc.getSid(), wc.getWc_no(), addUser, removeUser, loginUserSid);

            this.wcTraceManager.create(wcTrace, loginUserSid);
        }
    }

    /**
     * 刪除附件
     *
     * @param att_sid        附件Sid
     * @param delete_userSid 刪除者Sid
     * @return
     */
    private boolean deleteDBAttachment(String att_sid, Integer delete_userSid) {
        WCAttachment wa = wcAttachmentRepository.findOne(att_sid);
        if (wa == null) {
            return false;
        }
        wa.setStatus(Activation.INACTIVE);
        WCAttachment wc = this.updateWCAttachment(wa, delete_userSid);
        return wc != null;
    }

    /**
     * 更新附件
     *
     * @param wcAttachment 附件物件
     * @param userSid      更新者Sid
     * @return
     */
    private WCAttachment updateWCAttachment(WCAttachment wcAttachment, Integer userSid) {
        wcAttachment.setUpdate_dt(new Date());
        wcAttachment.setUpdate_usr_sid(userSid);
        WCAttachment wc = wcAttachmentRepository.save(wcAttachment);
        return wc;
    }

    /**
     * 更新追蹤資料
     *
     * @param wcTrace       追蹤資料
     * @param updateUserSid 更新者Sid
     * @return
     */
    private WCTrace updateWCTrace(WCTrace wcTrace, Integer updateUserSid) {
        wcTrace.setUpdate_usr(updateUserSid);
        wcTrace.setUpdate_dt(new Date());
        WCTrace wc = wcTraceRepository.save(wcTrace);
        return wc;
    }

    /**
     * 更新工作聯絡單(更新修改時間及修改者)
     *
     * @param wcMaster      工作聯絡單物件
     * @param updateUserSid 更新者Sid
     * @return
     */
    private WCMaster updateWCMaster(WCMaster wcMaster, Integer updateUserSid) {
        wcMaster.setUpdate_usr(updateUserSid);
        wcMaster.setUpdate_dt(new Date());
        WCMaster wc = wcMasterRepository.save(wcMaster);
        return wc;
    }

    /**
     * 變更閱讀記錄
     *
     * @param sid       工作聯絡單Sid
     * @param rRcordTos 閱讀記錄
     */
    public void updateWCReadRecord(String sid, List<RRcordTo> rRcordTos) {
        try {
            StringBuilder sql = new StringBuilder();
            List<Object> argsList = Lists.newArrayList();
            String json = WkJsonUtils.getInstance().toJsonWithOutPettyJson(rRcordTos);
            sql.append("UPDATE wc_master");
            sql.append(" SET read_record = ? ");
            argsList.add(json);
            sql.append(" WHERE wc_sid = ? ");
            argsList.add(sid);
            this.jdbc.update(sql.toString(), argsList.toArray());
        } catch (Exception e) {
            log.warn("updateWCReadRecord ERROR", e);
        }
    }

    /**
     * 將該工作聯絡單閱讀狀態都更改為待閱讀(除了登入者以外)
     *
     * @param wc_sid       工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     */
    public void updateWaitReadWCMaster(String wc_sid, Integer loginUserSid) {
        WCMaster wc = wcMasterRepository.findBySid(wc_sid);

        List<RRcordTo> rRcordTos = Lists.newArrayList();

        // ====================================
        // 加入登入者已讀資料
        // ====================================
        RRcordTo rRcordTo = new RRcordTo();
        rRcordTo.setRead(WCReadStatus.HASREAD.name());
        rRcordTo.setReadDay(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), new Date()));
        rRcordTo.setReader(String.valueOf(loginUserSid));
        rRcordTos.add(rRcordTo);

        // ====================================
        // 將其他閱讀記錄異動為待閱讀
        // ====================================
        // 逐筆處理已存在的閱讀資料
        if (wc.getRead_record() != null && WkStringUtils.notEmpty(wc.getRead_record().getRRecord())) {

            for (RRcordTo to : wc.getRead_record().getRRecord()) {
                // 為登入者時 pass (上面已經加了)
                if ((loginUserSid + "").equals(to.getReader())) {
                    continue;
                }
                // 資料異動為『待閱讀』
                to.setRead(WCReadStatus.WAIT_READ.name());

                // 加入 list
                rRcordTos.add(to);
            }

        }

        // ====================================
        // 更新資料
        // ====================================
        this.updateWCReadRecord(wc_sid, rRcordTos);
        em.refresh(wc);
    }

    /**
     * 更新 再次回覆資料
     *
     * @param sid       聯絡單sid
     * @param replySid  再次回覆sid
     * @param traceSid  回覆sid
     * @param userSid   回覆使用者
     * @param traceType 型態
     * @param content   內容
     * @return
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCMaster againReplyWCMaster(
            String sid,
            String replySid,
            String traceSid,
            Integer userSid,
            WCTraceType traceType,
            String content) {
        WCMaster wc = wcMasterRepository.findOne(sid);
        Preconditions.checkState(wc != null, "載入工作聯絡單失敗！");
        Preconditions.checkState(!(wc.getWc_status().equals(WCStatus.CLOSE)), "單據已結案,不可回覆！");
        Preconditions.checkState(!(wc.getWc_status().equals(WCStatus.CLOSE_STOP)), "單據已結案,不可回覆！");
        WCTrace wcTrace = wcTraceRepository.findOne(traceSid);
        WCAgainTrace againTrace = null;
        if (Strings.isNullOrEmpty(replySid)) {
            againTrace = new WCAgainTrace(traceSid, wc.getSid(), wc.getWc_no(), traceType);
        } else {
            againTrace = wcAgainTraceRepository.findOne(replySid);
        }
        againTrace.setContent(jsoupUtils.clearCssTag(content));
        againTrace.setContentCss(content);
        boolean isCreateAlert = false;
        if (Strings.isNullOrEmpty(replySid)) {
            againTrace.setCreateUser(userSid);
            againTrace.setCreateDate(new Date());
            againTrace.setUpdateDate(againTrace.getCreateDate());
            isCreateAlert = true;
        } else {
            againTrace.setUpdateUser(userSid);
            againTrace.setUpdateDate(new Date());
        }
        againTrace = wcAgainTraceRepository.save(againTrace);
        if (isCreateAlert) {
            try {
                List<Integer> recipients = Lists.newArrayList(wcTrace.getCreate_usr());
                if (!recipients.contains(wc.getCreate_usr())) {
                    recipients.add(wc.getCreate_usr());
                }
                List<WCAgainTrace> aginTraces = wcTraceManager.findByStatusAndTraceSid(Activation.ACTIVE, traceSid);
                aginTraces.forEach(
                        item -> {
                            if (!recipients.contains(item.getCreateUser())) {
                                recipients.add(item.getCreateUser());
                            }
                        });
                recipients.remove(userSid);
                if (!recipients.isEmpty()) {
                    alertManager.createAlertsByExecFinishReply(
                            wc.getSid(), wc.getWc_no(), recipients, wc.getTheme(), userSid, traceSid);
                }
            } catch (IllegalStateException | IllegalArgumentException e) {
                log.warn("createAlertsByExecFinishReply ERROR：{}", e.getMessage());
            } catch (Exception e) {
                log.warn("createAlertsByExecFinishReply ERROR", e);
            }
        }
        this.updateWaitReadWCMaster(wc.getSid(), userSid);
        return wc;
    }
}
