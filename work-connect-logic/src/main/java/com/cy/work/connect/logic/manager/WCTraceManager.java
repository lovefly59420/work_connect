/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.repository.WCAgainTraceRepository;
import com.cy.work.connect.repository.WCTraceRepository;
import com.cy.work.connect.vo.WCAgainTrace;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.WCTraceType;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCTraceManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -423793703050024344L;

    private static WCTraceManager instance;
    @Autowired
    private WCTraceRepository wcTraceRepository;
    @Autowired
    private WCAgainTraceRepository wcAgainTraceRepository;

    public static WCTraceManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCTraceManager.instance = this;
    }

    /**
     * 取得追蹤資料 By Sid
     *
     * @param sid
     * @return
     */
    public WCTrace getWCTraceBySid(String sid) {
        try {
            return wcTraceRepository.findOne(sid);
        } catch (Exception e) {
            log.warn("getWCTraceBySid Error : " + e, e);
        }
        return null;
    }

    /**
     * 建立追蹤資料
     *
     * @param wcTrace       追蹤物件
     * @param createUserSid 建立者Sid
     * @return
     */
    public WCTrace create(WCTrace wcTrace, Integer createUserSid) {
        try {
            wcTrace.setCreate_usr(createUserSid);
            Date createDate = new Date();
            wcTrace.setCreate_dt(createDate);
            wcTrace.setUpdate_dt(createDate);
            WCTrace wc = wcTraceRepository.save(wcTrace);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e.getMessage(), e);
        }
        return null;
    }

    /**
     * 取得追蹤資料 By 工作聯絡單Sid
     *
     * @param wc_id 工作聯絡單Sid
     * @return
     */
    public List<WCTrace> getWCTracesByWCSid(String wc_id) {
        try {
            List<WCTrace> result = wcTraceRepository.getWCTraceByWcSid(wc_id);
            Collections.sort(
                result,
                new Comparator<WCTrace>() {
                    @Override
                    public int compare(WCTrace o1, WCTrace o2) {
                        Date d1 =
                            (o1.getUpdate_dt() != null) ? o1.getUpdate_dt() : o1.getCreate_dt();
                        Date d2 =
                            (o2.getUpdate_dt() != null) ? o2.getUpdate_dt() : o2.getCreate_dt();
                        if (!d1.equals(d2)) {
                            return d2.compareTo(d1);
                        } else if (o1.getWc_trace_type().equals(WCTraceType.CLOSE)
                            || o1.getWc_trace_type().equals(WCTraceType.CLOSE_STOP)) {
                            return -1;
                        } else if (o2.getWc_trace_type().equals(WCTraceType.CLOSE)
                            || o2.getWc_trace_type().equals(WCTraceType.CLOSE_STOP)) {
                            return 1;
                        }
                        return 0;
                    }
                });
            return result;
        } catch (Exception e) {
            log.warn("getWCTracesByWCSid Error : ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 更新追蹤資料
     *
     * @param wcTrace       追蹤資料
     * @param updateUserSid 更新者Sid
     * @return
     */
    public WCTrace update(WCTrace wcTrace, Integer updateUserSid) {
        try {
            wcTrace.setUpdate_usr(updateUserSid);
            wcTrace.setUpdate_dt(new Date());
            WCTrace wc = wcTraceRepository.save(wcTrace);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 取得再次追蹤資料 By 聯絡單Sid
     *
     * @param status
     * @param wcSid  聯絡單Sid
     * @return
     */
    public List<WCAgainTrace> findByStatusAndWcSid(Activation status, String wcSid) {
        try {
            return wcAgainTraceRepository.findByStatusAndWcSid(status, wcSid);
        } catch (Exception e) {
            log.warn("findByStatusAndWcSid Error : ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得再次追蹤資料 By 追蹤Sid
     *
     * @param status
     * @param traceSid 追蹤Sid
     * @return
     */
    public List<WCAgainTrace> findByStatusAndTraceSid(Activation status, String traceSid) {
        try {
            return wcAgainTraceRepository.findByStatusAndTraceSid(status, traceSid);
        } catch (Exception e) {
            log.warn("findByStatusAndTraceSid Error : ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得追蹤資料 By Sid
     *
     * @param sid
     * @return
     */
    public WCAgainTrace getWCAgainTraceBySid(String sid) {
        try {
            return wcAgainTraceRepository.findOne(sid);
        } catch (Exception e) {
            log.warn("getWCAgainTraceBySid Error : " + e, e);
        }
        return null;
    }

    /**
     * 取得有效追蹤資料
     *
     * @param sids
     * @param type
     * @return
     */
    public List<WCTrace> findActiveBySidInAndType(List<String> sids, WCTraceType type) {
        Preconditions.checkState(type != null, "追蹤類型不可為空!!");
        return this.findActiveBySidInAndType(sids, Lists.newArrayList(type));
    }

    /**
     * 取得有效追蹤資料
     *
     * @param sids
     * @param types
     * @return
     */
    public List<WCTrace> findActiveBySidInAndType(List<String> sids, List<WCTraceType> types) {
        Preconditions.checkState(sids != null && !sids.isEmpty(), "聯絡單主檔sid不可為空!!");
        Preconditions.checkState(types != null && !types.isEmpty(), "追蹤類型不可為空!!");
        try {
            return wcTraceRepository.findByStatusAndSidInAndTypeIn(Activation.ACTIVE, sids, types);
        } catch (Exception e) {
            log.warn("findActiveBySidInAndType Error : " + e, e);
        }
        return Lists.newArrayList();
    }

    /**
     * 工作聯絡單模糊搜尋 是否有存在回覆關鍵字
     *
     * @param wcSid
     * @param types
     * @param lazyContent
     * @return
     */
    public List<String> getWcSidByLikeContentAndWcSidAndType(
        List<String> wcSid, List<WCTraceType> types, String lazyContent) {
        try {
            return wcTraceRepository.getWcSidByLikeContentAndWcSidAndType(
                wcSid, types, "%" + lazyContent + "%");
        } catch (Exception e) {
            log.warn("getWcSidByLikeContentAndWcSidAndType get Error : " + e, e);
            return Lists.newArrayList();
        }
    }
}
