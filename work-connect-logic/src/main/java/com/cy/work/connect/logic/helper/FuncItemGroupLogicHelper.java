/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCFunItemGroupManager;
import com.cy.work.connect.logic.manager.WCFunItemManager;
import com.cy.work.connect.logic.vo.WorkFunItemVO;
import com.cy.work.connect.logic.vo.view.WorkFunItemGroupVO;
import com.cy.work.connect.vo.WCFunItem;
import com.cy.work.connect.vo.WCFunItemGroup;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 選單Group邏輯元件
 *
 * @author brain0925_liao
 */
@Component
@Slf4j
public class FuncItemGroupLogicHelper implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2739795753960081447L;

    private static FuncItemGroupLogicHelper instance;
    
    
    /**
     * 報表快選區的 sid
     */
    public final static Integer REPORT_FAVORITE_ITEM_GROUP_SID = 5;
    
    private final transient Comparator<WorkFunItemVO> workFunItemVOComparator =
        new Comparator<WorkFunItemVO>() {
            @Override
            public int compare(WorkFunItemVO obj1, WorkFunItemVO obj2) {
                final Integer seq1 = obj1.getSeq();
                final Integer seq2 = obj2.getSeq();
                return seq1.compareTo(seq2);
            }
        };
    @Autowired
    private WCFunItemGroupManager funItemGroupManager;
    @Autowired
    private WCFunItemManager funItemManager;

    public static FuncItemGroupLogicHelper getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        FuncItemGroupLogicHelper.instance = this;
    }

    /**
     * 取得選單Group
     */
    public List<WorkFunItemGroupVO> getWorkFunItemGroupVOs() {

        List<WorkFunItemGroupVO> workFunItemGroupVOs = null;
        // ====================================
        // 快取
        // ====================================
        // 兜組快取名稱
        String cacheKey = this.getClass().getSimpleName() + "_getWorkFunItemGroupVOs";
        // 無快取 key
        List<String> keys = Lists.newArrayList();
        // 快取時間為最大值
        long overdueMillisecond = WkCommonCache.getInstance().getTimeToLiveMilliseconds();

        // 由快取取得資料
        workFunItemGroupVOs = WkCommonCache.getInstance()
            .getCache(cacheKey, keys, overdueMillisecond);

        // 不為 null 時, 代表成功取得快取
        if (workFunItemGroupVOs != null) {
            return workFunItemGroupVOs;
        }

        // ====================================
        // 查詢選單大項 (wc_fun_item_group)
        // ====================================
        List<WCFunItemGroup> funItemGroups = funItemGroupManager.getFunItemGroupByCategoryModel();
        if (WkStringUtils.isEmpty(funItemGroups)) {
            log.error("找不到任何選單大項資料");
            return Lists.newArrayList();
        }

        // ====================================
        // 查詢選單細項 (wc_fun_item)
        // ====================================
        List<WCFunItem> funItems = funItemManager.getFunItemByCategoryModel();

        // ====================================
        // 兜組選單結構
        // ====================================
        workFunItemGroupVOs = Lists.newArrayList();

        for (WCFunItemGroup itemGroup : funItemGroups) {
            List<WCFunItem> groupFunItems =
                funItems.stream()
                    .filter(p -> p.getFun_item_group_sid() == itemGroup.getSid())
                    .collect(Collectors.toList());

            List<WorkFunItemVO> workFunItemVOs = Lists.newArrayList();
            groupFunItems.forEach(
                item -> {
                    if (Strings.isNullOrEmpty(item.getUrl())) {
                        return;
                    }
                    WorkFunItemVO wv =
                        new WorkFunItemVO(
                            item.getSid(),
                            item.getFunction_title(),
                            item.getComponent_id(),
                            item.getUrl(),
                            item.getSeq());
                    workFunItemVOs.add(wv);
                });
            // sid = report_favorite 代表是報表快選區,預設永遠會出現
            if (workFunItemVOs.isEmpty() && ! WkCommonUtils.compareByStr(REPORT_FAVORITE_ITEM_GROUP_SID, itemGroup.getSid())) {
                continue;
            }
            try {
                Collections.sort(workFunItemVOs, workFunItemVOComparator);
            } catch (Exception e) {
                log.warn("search ERROR", e);
            }
            WorkFunItemGroupVO wfg =
                new WorkFunItemGroupVO(
                    String.valueOf(itemGroup.getSid()),
                    itemGroup.getName(),
                    itemGroup.getSeq(),
                    itemGroup.getGroup_base_sid(),
                    workFunItemVOs);

            workFunItemGroupVOs.add(wfg);
        }

        // ====================================
        // 寫入快取
        // ====================================
        WkCommonCache.getInstance().putCache(cacheKey, keys, workFunItemGroupVOs);

        return workFunItemGroupVOs;
    }
}
