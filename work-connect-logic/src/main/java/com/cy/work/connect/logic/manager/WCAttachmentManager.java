/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.repository.WCAttachmentRepository;
import com.cy.work.connect.vo.WCAttachment;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 工作聯絡單附件
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCAttachmentManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2822091566758638661L;

    @Autowired
    private WCAttachmentRepository wcAttachmentRepository;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WkOrgCache orgManager;

    /**
     * 取得附件 By 附件Sid
     *
     * @param att_sid
     * @return
     */
    public WCAttachment findBySid(String att_sid) {
        try {
            return wcAttachmentRepository.findOne(att_sid);
        } catch (Exception e) {
            log.warn("findBySid Error : " + e, e);
            return null;
        }
    }

    /**
     * 取得資料庫所有附件資料
     *
     * @return
     */
    public List<WCAttachment> findAll() {
        try {
            return wcAttachmentRepository.findAll();
        } catch (Exception e) {
            log.warn("findAll Error : " + e, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得附件List By 工作聯絡單Sid
     *
     * @param wcSid
     * @return
     */
    public List<WCAttachment> getWCAttachmentByWCSid(String wcSid) {
        try {
            return wcAttachmentRepository.getWCAttachmentByWCSid(wcSid);
        } catch (Exception e) {
            log.warn("getWCAttachmentByWCSid Error : " + e, e);
            return Lists.newArrayList();
        }
    }

    /**
     * 取得附件界面物件By 工作聯絡單Sid
     *
     * @param wc_sid 工作聯絡單Sid
     * @return
     */
    public List<AttachmentVO> getAttachmentsByWCSid(String wc_sid) {
        List<WCAttachment> as = this.getWCAttachmentByWCSid(wc_sid);
        if (as == null) {
            return Lists.newArrayList();
        }
        List<AttachmentVO> avs = Lists.newArrayList();
        as.forEach(
                item -> {
                    avs.add(transWCAttachmentToAttachmentVO(item));
                });
        return avs;
    }

    /**
     * 將附件物件轉換成附件界面物件
     *
     * @param wcAttachment 附件物件
     * @return
     */
    public AttachmentVO transWCAttachmentToAttachmentVO(WCAttachment wcAttachment) {
        AttachmentVO attachmentVO = new AttachmentVO(
                wcAttachment.getSid(),
                wcAttachment.getFile_name(),
                wcAttachment.getDescription(),
                wcAttachment.getFile_name(),
                true,
                String.valueOf(wcAttachment.getCreate_usr_sid()));
        
        attachmentVO.setDepSid(wcAttachment.getDepartment_sid());
        
        attachmentVO.setCreateTime(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDate.getValue(), wcAttachment.getCreate_dt()));
        attachmentVO.setParamCreateTime(
                ToolsDate.transDateToString(
                        SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), wcAttachment.getCreate_dt()));
        attachmentVO.setStatus(wcAttachment.getStatus());
        try {
            User user = userManager.findBySid(wcAttachment.getCreate_usr_sid());
            attachmentVO.setCreateUserSId(String.valueOf(user.getSid()));
            attachmentVO.setUserName(user.getName());
            Org dep = orgManager.findBySid(user.getPrimaryOrg().getSid());
            String userInfo = WkOrgUtils.prepareBreadcrumbsByDepName(dep.getSid(), OrgLevel.DIVISION_LEVEL, true,
                    "-")
                    + "-"
                    + user.getName();
            attachmentVO.setUserInfo(userInfo);
        } catch (Exception e) {
            log.warn("transWCAttachmentToAttachmentVO", e);
        }
        return attachmentVO;
    }

    /**
     * 建立附件資料
     *
     * @param wcAttachment 附件物件
     * @param userSid      建立者
     * @return
     */
    public WCAttachment create(WCAttachment wcAttachment, Integer userSid) {
        try {
            wcAttachment.setCreate_usr_sid(userSid);
            wcAttachment.setCreate_dt(new Date());
            WCAttachment wc = wcAttachmentRepository.save(wcAttachment);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 更新工作聯絡單與附件關聯及附件資訊
     *
     * @param wc_sid         工作聯絡單Sid
     * @param wc_no          工作聯絡單號
     * @param description    附件描述
     * @param userSid        更新者
     * @param attachment_sid 附件Sid
     */
    public void updateWCMasterMappingInfo(
            String wc_sid, String wc_no, String description, Integer userSid, String attachment_sid) {
        try {
            wcAttachmentRepository.updateWCMasterMappingInfo(
                    wc_sid, wc_no, description, userSid, new Date(), attachment_sid);
        } catch (Exception e) {
            log.warn("updateWCMasterMappingInfo Error : " + e, e);
        }
    }

    /**
     * 更新附件
     *
     * @param wcAttachment 附件物件
     * @param userSid      更新者Sid
     * @return
     */
    public WCAttachment update(WCAttachment wcAttachment, Integer userSid) {
        try {
            wcAttachment.setUpdate_dt(new Date());
            wcAttachment.setUpdate_usr_sid(userSid);
            WCAttachment wc = wcAttachmentRepository.save(wcAttachment);
            return wc;
        } catch (Exception e) {
            log.warn("save Error : " + e, e);
        }
        return null;
    }

    /**
     * 計算單據的上傳檔案數量
     * @param wcSid 主檔 sid
     * @return 檔案數量
     */
    public Integer countByWcSid(String wcSid) {
        return this.wcAttachmentRepository.countByWcSid(wcSid);
    }
}
