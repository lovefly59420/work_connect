/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.logic.cache.WCMemoCategoryCache;
import com.cy.work.connect.vo.WCMemoCategory;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 備忘錄類別Manager
 *
 * @author brain0925_liao
 */
@Component
public class WCMemoCategoryManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 126585496851493189L;
    /**
     * 備忘錄類別Cache
     */
    @Autowired
    private WCMemoCategoryCache wcMemoCategoryCache;

    /**
     * 取得所有備忘錄類別
     */
    public List<WCMemoCategory> getAllWCMemoCategory() {
        return wcMemoCategoryCache.getAllWCMemoCategory();
    }

    /**
     * 取得備忘錄 By Sid
     *
     * @param sid 備忘錄Sid
     * @return
     */
    public WCMemoCategory getWCMemoCategory(String sid) {
        return wcMemoCategoryCache.getWCMemoCategoryBySid(sid);
    }
}
