/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.logic.query.WCMasterQueryService;
import com.cy.work.connect.logic.utils.ReqularPattenUtils;
import com.cy.work.connect.logic.utils.ToolsDate;
import com.cy.work.connect.logic.vo.WCFavoriteWorkReportVO;
import com.cy.work.connect.repository.WCFavoriteRepository;
import com.cy.work.connect.vo.WCFavorite;
import com.cy.work.connect.vo.enums.SimpleDateFormatEnum;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 工作聯絡單收藏
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCFavoriteManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2531841006474114892L;

    @Autowired
    private WCFavoriteRepository wcFavoriteRepository;
    @Autowired
    private ReqularPattenUtils reqularUtils;

    @Autowired
    @Qualifier("wcMasterQueryService")
    private WCMasterQueryService wcMasterQueryService;

    public List<WCFavorite> getByWCSid(String wcSid) {
        return wcFavoriteRepository.findByWCSid(wcSid);
    }

    /**
     * 取得收藏資料 By 工作聯絡單Sid及登入者Sid
     *
     * @param wc_Id
     * @param loginUserSid
     * @return
     */
    public WCFavorite getWCFavoriteByloginUserSidAndWCSid(String wc_Id, Integer loginUserSid) {
        List<WCFavorite> wcFavorites = wcFavoriteRepository.findByUserSidAndWCSid(wc_Id,
            loginUserSid);
        if (wcFavorites.size() > 0) {
            return wcFavorites.get(0);
        }
        return null;
    }

    /**
     * 取得收藏資料筆數 By 收藏時間區間及登入者Sid
     *
     * @param loginUserSid 登入者Sid
     * @param start        收藏起始時間
     * @param end          收藏結束時間
     * @return
     */
    public Integer findFavoriteCountByUserSidAndUpdateDt(Integer loginUserSid, Date start,
        Date end) {
        try {
            String startDateStr =
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(), start);
            startDateStr = startDateStr + " 00:00:00";
            Date searchStartDate =
                ToolsDate.transStringToDate(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), startDateStr);

            String endDateStr =
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDateDash.getValue(), end);
            endDateStr = endDateStr + " 23:59:59";
            Date searchEndDate =
                ToolsDate.transStringToDate(
                    SimpleDateFormatEnum.SdfDateDashTimeSS.getValue(), endDateStr);
            return wcFavoriteRepository.findFavoriteCountByUserSidAndUpdateDt(
                searchStartDate, searchEndDate, loginUserSid);
        } catch (Exception e) {
            log.warn("findFavoriteCountByUserSidAndUpdateDt", e);
        }
        return 0;
    }

    /**
     * 取得收藏工作聯絡單資料 By 登入者及收藏時間區間及主題字串及工作聯絡單Sid
     *
     * @param loginUserSid 登入者
     * @param start        收藏起始時間
     * @param end          收藏結束時間
     * @param text         主題字串
     * @param wc_sid       工作聯絡單Sid
     * @return
     */
    public List<WCFavoriteWorkReportVO> findFavoriteByUserSidAndUpdateDtAndText(
        Integer loginUserSid, Date start, Date end, String text, String wc_sid) {

        Map<String, Object> parameters = Maps.newHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ");
        builder.append(" wf.favorite_sid,wc.wc_sid,wc.theme,wf.update_dt,wc.wc_no");
        builder.append(" FROM wc_favorite wf INNER JOIN wc_master wc ON wc.wc_sid = wf.wc_sid");
        builder.append(" WHERE wf.status = 0 ");
        builder.append(" AND wf.create_usr = :create_usr ");
        parameters.put("create_usr", loginUserSid);
        if (start != null) {
            String startDateStr =
                ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(), start);
            builder.append(" AND wf.update_dt >= :start_dt ");
            parameters.put("start_dt", startDateStr + " 00:00:00");
        }

        if (end != null) {
            String endDateStr = ToolsDate.transDateToString(SimpleDateFormatEnum.SdfDate.getValue(),
                end);
            builder.append(" AND wf.update_dt <= :end_dt ");
            parameters.put("end_dt", endDateStr + " 23:59:59");
        }

        if (!Strings.isNullOrEmpty(text)) {
            String textNo = "%" + reqularUtils.replaceIllegalSqlLikeStr(text) + "%";
            builder.append(" AND wc.theme LIKE :theme");
            parameters.put("theme", textNo);
        }
        if (!Strings.isNullOrEmpty(wc_sid)) {
            builder.append(" AND wf.wc_sid = :wc_sid ");
            parameters.put("wc_sid", wc_sid);
        }
        builder.append(" ORDER BY  wf.update_dt DESC ");
        List<WCFavoriteWorkReportVO> wcFavoriteWorkReportVOs = Lists.newArrayList();
        @SuppressWarnings("rawtypes")
        List result = wcMasterQueryService.findWithQuery(builder.toString(), parameters, null);
        for (int i = 0; i < result.size(); i++) {
            try {
                Object[] record = (Object[]) result.get(i);
                String favorite_sid = (String) record[0];
                String wrc_sid = (String) record[1];
                String theme = (String) record[2];
                Date favoriteDate = null;
                if (record[3] != null) {
                    favoriteDate = (Date) record[3];
                }
                String wrc_no = (String) record[4];
                WCFavoriteWorkReportVO wv =
                    new WCFavoriteWorkReportVO(favorite_sid, wrc_sid, theme, favoriteDate, wrc_no);
                wcFavoriteWorkReportVOs.add(wv);
            } catch (Exception e) {
                log.warn("findFavoriteByUserSidAndUpdateDtAndText", e);
            }
        }
        return wcFavoriteWorkReportVOs;
    }

    /**
     * 建立收藏資料
     *
     * @param wcFavorite   收藏物件
     * @param loginUserSid 建立者
     */
    public WCFavorite createWCFavorite(WCFavorite wcFavorite, Integer loginUserSid) {
        wcFavorite.setStatus(Activation.ACTIVE);
        wcFavorite.setCreate_dt(new Date());
        wcFavorite.setCreate_usr(loginUserSid);
        wcFavorite.setUpdate_dt(new Date());
        return wcFavoriteRepository.save(wcFavorite);
    }

    /**
     * 更新收藏資料
     *
     * @param wcFavorite   收藏物件
     * @param loginUserSid 更新者
     */
    public WCFavorite updateWCFavorite(WCFavorite wcFavorite, Integer loginUserSid) {
        wcFavorite.setUpdate_dt(new Date());
        wcFavorite.setUpdate_usr(loginUserSid);
        return wcFavoriteRepository.save(wcFavorite);
    }
}
