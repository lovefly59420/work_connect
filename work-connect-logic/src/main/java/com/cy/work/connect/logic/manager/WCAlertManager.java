/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCAlertRepository;
import com.cy.work.connect.vo.WCAlert;
import com.cy.work.connect.vo.enums.WCAlertType;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 最新消息 管理
 *
 * @author kasim
 */
@Slf4j
@Component
public class WCAlertManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1254176433862873586L;

    private static WCAlertManager instance;
    @Autowired
    private WCAlertRepository alertRepository;

    public static WCAlertManager getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCAlertManager.instance = this;
    }

    /**
     * 抓取資料
     *
     * @param sid
     * @return
     */
    public WCAlert findOne(String sid) {
        return alertRepository.findOne(sid);
    }

    /**
     * 建立訊息 by 執行回覆
     *
     * @param wcSid
     * @param wcNo
     * @param recipients
     * @param exe
     * @param theme
     * @param createUser
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void createAlertsByExecReply(
        String wcSid, String wcNo, List<Integer> recipients, String theme, Integer createUser) {
        this.createAlerts(wcSid, wcNo, WCAlertType.EXEC_REPLY, recipients, null, theme, createUser,
            "");
    }

    /**
     * 建立訊息 by 執行完成
     *
     * @param wcSid
     * @param wcNo
     * @param recipients
     * @param execDepName
     * @param theme
     * @param createUser
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void createAlertsByExecFinish(
        String wcSid,
        String wcNo,
        List<Integer> recipients,
        String execDepName,
        String theme,
        Integer createUser,
        String traceSid) {
        this.createAlertsAlways(
            wcSid, wcNo, WCAlertType.EXEC_FINISH, recipients, execDepName, theme, createUser, "");
    }

    /**
     * 建立訊息 by 執行完成回覆
     *
     * @param wcSid
     * @param wcNo
     * @param recipients
     * @param execDepName
     * @param theme
     * @param createUser
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void createAlertsByExecFinishReply(
        String wcSid,
        String wcNo,
        List<Integer> recipients,
        String theme,
        Integer createUser,
        String traceSid) {
        this.createAlerts(
            wcSid, wcNo, WCAlertType.EXEC_FINISH_REPLY, recipients, null, theme, createUser,
            traceSid);
    }

    /**
     * 建立訊息(不管未讀是否已有相同類型最新消息都產生)
     *
     * @param wcSid       工作聯絡單 Sid
     * @param wcNo        工作聯絡單 單號
     * @param type        型態
     * @param recipients  收件人
     * @param execDepName 執行單位
     * @param theme
     * @param createUser
     */
    @Transactional(rollbackForClassName = {"Exception"})
    private void createAlertsAlways(
        String wcSid,
        String wcNo,
        WCAlertType type,
        List<Integer> recipients,
        String execDepName,
        String theme,
        Integer createUser,
        String traceSid) {
        this.check(wcSid, wcNo, type, recipients, execDepName, theme, createUser);
        try {
            alertRepository.save(
                recipients.stream()
                    .map(
                        each -> {
                            WCAlert obj = new WCAlert();
                            obj.setWcSid(wcSid);
                            obj.setWcNo(wcNo);
                            obj.setType(type);
                            obj.setTheme(this.bulidTheme(type, theme, execDepName));
                            obj.setRecipient(each);
                            obj.setCreateUser(createUser);
                            obj.setCreateDate(new Date());
                            obj.setWcTraceSid(traceSid);
                            return obj;
                        })
                    .collect(Collectors.toList()));
        } catch (Exception e) {
            log.warn("建立訊息 Error!!", e);
            Preconditions.checkState(false, "建立訊息 Error!!");
        }
    }

    /**
     * 建立訊息
     *
     * @param wcSid       工作聯絡單 Sid
     * @param wcNo        工作聯絡單 單號
     * @param type        型態
     * @param recipients  收件人
     * @param execDepName 執行單位
     * @param theme
     * @param createUser
     */
    @Transactional(rollbackForClassName = {"Exception"})
    private void createAlerts(
        String wcSid,
        String wcNo,
        WCAlertType type,
        List<Integer> recipients,
        String execDepName,
        String theme,
        Integer createUser,
        String traceSid) {
        this.check(wcSid, wcNo, type, recipients, execDepName, theme, createUser);
        try {
            alertRepository.save(
                recipients.stream()
                    .map(
                        each -> {
                            WCAlert obj = new WCAlert();
                            obj.setWcSid(wcSid);
                            obj.setWcNo(wcNo);
                            obj.setType(type);
                            obj.setTheme(this.bulidTheme(type, theme, execDepName));
                            obj.setRecipient(each);
                            obj.setCreateUser(createUser);
                            obj.setCreateDate(new Date());
                            obj.setWcTraceSid(traceSid);
                            return obj;
                        })
                    .filter(
                        each ->
                            !alertRepository.isAnyNotReadByWcSidAndTypeAndRecipient(
                                each.getWcSid(), each.getType(), each.getRecipient()))
                    .collect(Collectors.toList()));
        } catch (Exception e) {
            log.warn("建立訊息 Error!!", e);
            Preconditions.checkState(false, "建立訊息 Error!!");
        }
    }

    /**
     * 檢核建立訊息參數
     *
     * @param wcSid
     * @param wcNo
     * @param type
     * @param recipients
     * @param execDepName
     * @param theme
     * @param createUser
     */
    private void check(
        String wcSid,
        String wcNo,
        WCAlertType type,
        List<Integer> recipients,
        String execDepName,
        String theme,
        Integer createUser) {
        Preconditions.checkState(!Strings.isNullOrEmpty(wcSid), "工作聯絡單Sid 不允許為空!!");
        Preconditions.checkState(!Strings.isNullOrEmpty(wcNo), "工作聯絡單單號 不允許為空!!");
        Preconditions.checkState(type != null, "型態 不允許為空!!");
        if (WCAlertType.EXEC_FINISH.equals(type)) {
            Preconditions.checkState(!Strings.isNullOrEmpty(execDepName), "執行單位 不允許為空!!");
        }
        Preconditions.checkState(!Strings.isNullOrEmpty(theme), "主題 不允許為空!!");
        Preconditions.checkState(createUser != null, "建立者 不允許為空!!");
    }

    /**
     * 建立訊息主題
     *
     * @param type
     * @param theme
     * @param theme
     * @return
     */
    private String bulidTheme(WCAlertType type, String theme, String execDepName) {
        if (null != type) {
            switch (type) {
                case EXEC_REPLY:
                    return theme + "-已有執行回覆通知";
                case EXEC_FINISH:
                    return theme + "-" + execDepName + "-執行完成";
                case EXEC_FINISH_REPLY:
                    return theme + "-已有執行完成回覆通知";
                default:
                    break;
            }
        }
        return theme;
    }

    /**
     * 讀取訊息(當此工作聯絡單變更為結案時，尚未讀取的同仁，系統需自動更新為已閱讀)
     *
     * @param wcSid 工作聯絡單 Sid
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void readAlertsByClose(String wcSid) {
        try {
            alertRepository.readAlertsByClose(wcSid, new Date());
        } catch (Exception e) {
            log.warn("批次讀取訊息失敗！！wcSid：" + wcSid, e);
        }
    }

    /**
     * 讀取訊息(一般開啟單據)
     *
     * @param wcSid     工作聯絡單 Sid
     * @param recipient
     */
    @Transactional(rollbackForClassName = {"Exception"})
    public void readAlerts(String wcSid, Integer recipient) {
        try {
            alertRepository.readAlerts(wcSid, recipient, new Date());
        } catch (Exception e) {
            log.warn("批次讀取訊息失敗！！wcSid：" + wcSid + "，recipient：" + recipient, e);
        }
    }
}
