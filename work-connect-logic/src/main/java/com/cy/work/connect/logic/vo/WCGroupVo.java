/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCSetGroupManager;
import com.cy.work.connect.logic.manager.WorkSettingOrgManager;
import com.cy.work.connect.logic.manager.WorkSettingUserManager;
import com.cy.work.connect.vo.WCSetGroup;
import com.cy.work.connect.vo.converter.to.ConfigValueTo;
import java.io.Serializable;
import lombok.Getter;
import org.springframework.beans.BeanUtils;

/**
 * @author jimmy_chou
 */
public class WCGroupVo extends WCSetGroup implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 460998544767281620L;

    /**
     *
     */
    @Getter
    private String configValueDepsShow;

    /**
     *
     */
    @Getter
    private String configValueUsersShow;

    /**
     *
     */
    public WCGroupVo() {
        this.setConfigValue(new ConfigValueTo());
    }

    /**
     * 建構子
     *
     * @param group
     */
    public WCGroupVo(WCSetGroup group) {
        // 複製所有欄位屬性
        BeanUtils.copyProperties(group, this);

        // 初始化設定值物件
        if (this.getConfigValue() == null) {
            this.setConfigValue(new ConfigValueTo());
        }

        // 組部門顯示資訊
        this.configValueDepsShow =
            WorkSettingOrgManager.getInstance()
                .prepareDepsShowText(
                    this.getConfigValue().getDeps(),
                    this.getConfigValue().isDepContainFollowing(),
                    this.getConfigValue().isDepExcept());

        if (WkStringUtils.isEmpty(this.configValueDepsShow)) {
            this.configValueDepsShow = "";
        }

        // 組人員顯示資訊
        this.configValueUsersShow =
            WorkSettingUserManager.getInstance()
                .prepareUsersShowText(
                    this.getConfigValue().getUsers(), this.getConfigValue().isDepExcept());

        if (WkStringUtils.isEmpty(this.configValueUsersShow)) {
            this.configValueUsersShow = "";
        }
    }

    /**
     * 顯示用：設定群組名稱
     *
     * @return
     */
    public String getGroupNameShowColorful() {
        return WCSetGroupManager.getInstance()
            .getPettyGroupName(this.getGroupName(), this.getConfigValue(), true, false);
    }
}
