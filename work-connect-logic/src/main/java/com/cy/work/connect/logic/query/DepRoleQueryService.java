/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.query;

import com.cy.commons.vo.Org;
import com.cy.commons.vo.Role;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkRoleCache;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.connect.logic.vo.RoleSIDVO;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author brain0925_liao
 */
@Component("depRoleQuery")
@Slf4j
public class DepRoleQueryService implements QueryService<Role>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3054102235908817743L;

    @Autowired
    private WkRoleCache wkRoleCache;
    @PersistenceContext
    private transient EntityManager em;
    @Autowired
    private WkJsonUtils jsonUtils;
    @Autowired
    private WkOrgCache orgManager;

    public List<Org> findOrgWithQuery(String sql, Map<String, Object> parameters, User executor) {
        @SuppressWarnings("rawtypes")
        List result = findWithQuery(sql, parameters, executor);
        List<Org> orgs = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            try {
                Integer dep_sid = (Integer) result.get(i);
                Org o = orgManager.findBySid(dep_sid);
                orgs.add(o);
            } catch (Exception e) {
                log.warn("findOrgWithQuery : Error -" + e, e);
            }
        }
        return orgs;
    }

    public List<Role> findRoleWithQueryForDep(
        String sql, Map<String, Object> parameters, User executor) {
        @SuppressWarnings("rawtypes")
        List result = findWithQuery(sql, parameters, executor);
        List<Role> roles = Lists.newArrayList();
        for (int i = 0; i < result.size(); i++) {
            try {
                String roleIdsStr = (String) result.get(i);
                List<RoleSIDVO> roleSIDVOs = jsonUtils.fromJsonToList(roleIdsStr, RoleSIDVO.class);
                roleSIDVOs.forEach(
                    item -> {
                        Role r = wkRoleCache.findBySid(Long.valueOf(item.getRoleSID()));
                        roles.add(r);
                    });
            } catch (Exception e) {
                log.warn("findRoleWithQueryForDep : Error -" + e, e);
            }
        }
        return roles;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public List findWithQuery(String sql, Map<String, Object> parameters, User executor) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = em.createNativeQuery(sql);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        List result = query.getResultList();
        return result;
    }
}
