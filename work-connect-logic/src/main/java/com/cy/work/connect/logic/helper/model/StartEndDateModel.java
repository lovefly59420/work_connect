package com.cy.work.connect.logic.helper.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StartEndDateModel {
    String sid;
    String startdate;
    String enddate;
}