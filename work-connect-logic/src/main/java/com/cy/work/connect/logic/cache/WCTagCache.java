/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCTagRepository;
import com.cy.work.connect.vo.WCTag;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 類別細項Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCTagCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -331809660287393608L;
    private final transient Comparator<WCTag> wcTagComparator = new Comparator<WCTag>() {
        @Override
        public int compare(WCTag obj1, WCTag obj2) {
            final Integer seq1 = obj1.getSeq();
            final Integer seq2 = obj2.getSeq();
            return seq1.compareTo(seq2);
        }
    };
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private WCTagRepository wCTagRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCTag> wCTagMap;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCTagCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wCTagMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立小類(執行項目)Cache");
        try {
            em.clear();
            // long startTime = System.currentTimeMillis();
            List<WCTag> tags = this.wCTagRepository.findAll();
            // log.debug(com.cy.work.common.utils.WkCommonUtils.prepareCostMessage(startTime, "查詢所有小類"));

            Map<String, WCTag> tempWCTagMap = Maps.newConcurrentMap();
            for (WCTag wcTag : tags) {
                tempWCTagMap.put(wcTag.getSid(), wcTag);
            }

            wCTagMap = tempWCTagMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立類別細項Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache資料
     *
     * @param wCTag
     */
    public synchronized void updateCacheBySid(WCTag wCTag) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立小類(執行項目)Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立小類(執行項目)Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wCTagMap.put(wCTag.getSid(), wCTag);
    }

    /**
     * 取得執行項目 By Sid
     *
     * @param sid 小類Sid
     * @return
     */
    public WCTag findBySid(String sid) {
        return wCTagMap.get(sid);
    }

    /**
     * 取得有效類別細項List
     *
     * @param wcMenuTagSid
     * @param status
     * @return
     */
    public List<WCTag> getWCTag(String wcMenuTagSid, Activation status) {
        List<WCTag> wCTags = Lists.newArrayList();
        Lists.newArrayList(wCTagMap.values())
                .forEach(
                        item -> {
                            if (status != null && !status.equals(item.getStatus())) {
                                return;
                            }
                            if (!Strings.isNullOrEmpty(wcMenuTagSid)
                                    && !wcMenuTagSid.equals(item.getWcMenuTagSid())) {
                                return;
                            }
                            wCTags.add(item);
                        });
        Collections.sort(wCTags, wcTagComparator);
        return wCTags;
    }

    /**
     * 以中類 sid 查詢對應小類，並做排序
     * @param wcMenuTagSid
     * @return
     */
    public List<WCTag> findByMenuTagSidAndOrderBySeq(String wcMenuTagSid) {

        Comparator<WCTag> comparator = Comparator.comparing(WCTag::getSeq);

        return this.wCTagMap.values().stream()
                .filter(tag -> WkCommonUtils.compareByStr(wcMenuTagSid, tag.getWcMenuTagSid()))
                // 以 SEQ 排序
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}
