/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCReadReceiptsRepository;
import com.cy.work.connect.vo.WCReadReceipts;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 可閱名單 管理
 *
 * @author jimmy_chou
 */
@Component
public class WCReadReceiptsManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1567597890311884567L;

    private static WCReadReceiptsManager instance;
    @Autowired
    private WCReadReceiptsRepository readReceiptsRepository;

    public static WCReadReceiptsManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCReadReceiptsManager.instance = this;
    }


    @Transactional(rollbackForClassName = { "Exception" })
    public void deleteInBatch(List<WCReadReceipts> readReceiptsList) {
        readReceiptsRepository.deleteInBatch(readReceiptsList);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void save(List<WCReadReceipts> readReceiptsList) {
        readReceiptsRepository.save(readReceiptsList);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void save(WCReadReceipts readReceipts) {
        readReceiptsRepository.save(readReceipts);
    }

    @Transactional(rollbackForClassName = { "Exception" })
    public void updateReadTime(Integer reader) {
        readReceiptsRepository.updateReadTime(reader);
    }

    public List<WCReadReceipts> findByWcsid(String wcsid) {
        return readReceiptsRepository.findByWcsid(wcsid);
    }

    public WCReadReceipts findReadReceiptsByWCSidAndReader(String wcsid, Integer reader) {
        return readReceiptsRepository.findReadReceiptsByWCSidAndReader(wcsid, reader);
    }

    public List<WCReadReceipts> findReadReceiptsByWCSidAndReaderList(
            String wcsid, List<Integer> readerList) {
        return readReceiptsRepository.findReadReceiptsByWCSidAndReaderList(wcsid, readerList);
    }

    /**
     * 是否為可閱人員
     * 
     * @param wcSid   主單 sid
     * @param userSid user sid
     */
    public boolean isReader(
            String wcSid,
            Integer userSid) {
        return this.readReceiptsRepository.countByWcsidAndReader(wcSid, userSid) > 0;
    }
}
