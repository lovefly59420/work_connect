/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.repository.WCBasePersonAccessInfoRepository;
import com.cy.work.connect.vo.WCBasePersonAccessInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 可閱讀部門成員權限Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCBasePersonAccessInfoCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 897468109113203664L;
    /**
     * WCBaseDepAccessInfoRepository
     */
    @Autowired
    private WCBasePersonAccessInfoRepository wcBasePersonAccessInfoRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * Cache 物件
     */
    private Map<String, WCBasePersonAccessInfo> wcBasePersonAccessInfoMap;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    public WCBasePersonAccessInfoCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcBasePersonAccessInfoMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立可閱讀部門成員權限Cache");
        try {
            Map<String, WCBasePersonAccessInfo> tempWCBaseDepAccessInfoMap = Maps.newConcurrentMap();
            wcBasePersonAccessInfoRepository
                .findAll()
                .forEach(
                    item -> {
                        tempWCBaseDepAccessInfoMap.put(item.getSid(), item);
                    });
            wcBasePersonAccessInfoMap = tempWCBaseDepAccessInfoMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立可閱讀部門成員權限Cache結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新Cache資料
     */
    public synchronized void updateCacheBySid(WCBasePersonAccessInfo wcBasePersonAccessInfo) {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立可閱讀部門成員權限Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立可閱讀部門成員權限Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
        wcBasePersonAccessInfoMap.put(wcBasePersonAccessInfo.getSid(), wcBasePersonAccessInfo);
    }

    /**
     * 取得可閱讀部門成員權限 By Sid
     *
     * @param sid 可閱讀部門成員權限Sid
     * @return
     */
    public WCBasePersonAccessInfo findBySid(String sid) {
        return wcBasePersonAccessInfoMap.get(sid);
    }

    /**
     * 取得可閱讀部門成員權限 By 使用者Sid
     *
     * @param userSid 使用者Sid
     * @return
     */
    public WCBasePersonAccessInfo getByUserSid(Integer userSid) {
        for (WCBasePersonAccessInfo wi : Lists.newArrayList(wcBasePersonAccessInfoMap.values())) {
            if (wi.getLoginUserSid().equals(userSid)) {
                return wi;
            }
        }
        return null;
    }

    /**
     * 取得單位成員可閱權限增加(聯集)已設定成員
     *
     * @return
     */
    public List<Integer> getSettingOtherBasePerson() {
        List<Integer> result = Lists.newArrayList();
        for (WCBasePersonAccessInfo wi : Lists.newArrayList(wcBasePersonAccessInfoMap.values())) {
            if (WkStringUtils.notEmpty(wi.getOtherBaseAccessViewPerson())) {
                result.add(wi.getLoginUserSid());
            }
        }
        return result;
    }
}
