/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo.enums;

import lombok.Getter;

/**
 * BPM 流程型態
 *
 * @author kasim
 */
public enum WcBpmFlowType {
    WC_REQUEST("wc_request", "工作連絡單-需求單位簽核"),
    //WC_SPECIAL_REQUEST("wc_special_request", "工作連絡單-特殊會員服務處簽核"),
    //WC_SPECIAL_REQUEST_XS("wc_special_request_xs", "工作連絡單-特殊會員服務處簽核_XS"),
    WC_EXEC("wc_exec", "工作連絡單-執行單位簽核"),
    WC_CS_REQ_UNIT("wc_cs_req_unit", "工作連絡單-需求單位加簽"),
    WC_CS_EXE_UNIT("wc_cs_exe_unit", "工作連絡單-執行單位加簽"),
    // WC_SPECIAL_REQUEST_XS_MANAGER("wc_request_xs_manager", "工作聯絡單-需求單位簽核-XS專用-總經理");
    // 借用流程代號 wc_special_request, 避免改 portal
    //WC_SPECIAL_REQUEST_XS_MANAGER("wc_special_request", "工作聯絡單-需求單位簽核-XS專用");
    ;

    @Getter
    private final String definition;
    @Getter
    private final String displayDesc;

    WcBpmFlowType(String definition, String displayDesc) {
        this.definition = definition;
        this.displayDesc = displayDesc;
    }
}
