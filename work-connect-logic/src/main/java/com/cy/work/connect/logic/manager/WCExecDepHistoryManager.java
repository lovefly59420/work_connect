/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.repository.WCExecDepHistoryRepository;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepHistory;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 執行單位分派歷史紀錄
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecDepHistoryManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4890805926412058384L;

    @Autowired
    private WCExecDepHistoryRepository wcExecDepHistoryRepository;

    /**
     * 執行單位分派歷史紀錄建立
     *
     * @param wcSid        工作聯絡單Sid
     * @param wcExecDepSid 執行單位Sid
     * @param execUserSid  執行人員Sid
     * @param loginUserSid 建立者Sid
     */
    public void createWCExecDepHistory(
            String wcSid, String wcExecDepSid, Integer execUserSid, Integer loginUserSid) {
        WCExecDepHistory wh = new WCExecDepHistory();
        wh.setWcExecDepSid(wcExecDepSid);
        wh.setWcSid(wcSid);
        wh.setExec_usr(execUserSid);
        wh.setCreate_dt(new Date());
        wh.setCreate_usr(loginUserSid);
        wcExecDepHistoryRepository.save(wh);
    }

    public void createWCExecDepHistory(
            List<WCExceDep> wcExecDeps, Integer loginUserSid) {

        if (WkStringUtils.isEmpty(wcExecDeps)) {
            return;
        }

        List<WCExecDepHistory> entities = Lists.newArrayList();
        for (WCExceDep wcExceDep : wcExecDeps) {
            WCExecDepHistory wcExecDepHistory = new WCExecDepHistory();
            entities.add(wcExecDepHistory);

            wcExecDepHistory.setWcExecDepSid(wcExceDep.getSid());
            wcExecDepHistory.setWcSid(wcExceDep.getWcSid());
            wcExecDepHistory.setExec_usr(wcExceDep.getExecUserSid());
            wcExecDepHistory.setCreate_dt(new Date());
            wcExecDepHistory.setCreate_usr(loginUserSid);
        }

        wcExecDepHistoryRepository.save(entities);
    }

    /**
     * 該登入者是否之前有被指派過
     *
     * @param wcSid        工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     * @return
     */
    public boolean isExecDepHistory(String wcSid, Integer loginUserSid) {
        boolean result = false;
        try {
            List<WCExecDepHistory> whs = wcExecDepHistoryRepository.findByWCSidAndExecUser(wcSid, loginUserSid);
            if (whs != null && !whs.isEmpty()) {
                result = true;
            }
        } catch (Exception e) {
            log.warn("isExecDepHistory ERROR", e);
        }
        return result;
    }
}
