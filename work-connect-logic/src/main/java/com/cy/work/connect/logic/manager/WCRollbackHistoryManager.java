/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.work.connect.repository.WCRollbackHistoryRepository;
import com.cy.work.connect.vo.WCRollbackHistory;
import java.io.Serializable;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 退回記錄Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCRollbackHistoryManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7297885543200809134L;

    @Autowired
    private WCRollbackHistoryRepository wcRollbackHistoryRepository;

    public Integer getCountByWcSidAndUserSid(Integer userSid, String wcSid) {
        try {
            return wcRollbackHistoryRepository.findByWcSidAndUserSid(wcSid, userSid).size();
        } catch (Exception e) {
            log.warn("getByWcSidAndUserSid ERROR", e);
        }
        return 0;
    }
    
    /**
     * 是否有退回記錄
     * @param userSid
     * @param wcSid
     * @return
     */
    public boolean hasRollbackHistory(Integer userSid, String wcSid) {
        return this.getCountByWcSidAndUserSid(userSid, wcSid)>0;
    }
        

    public void insertRollBackHistory(Integer userSid, String wcSid) {
        try {
            WCRollbackHistory wcRollbackHistory = new WCRollbackHistory();
            wcRollbackHistory.setCreateDate(new Date());
            wcRollbackHistory.setUserSid(userSid);
            wcRollbackHistory.setWcSid(wcSid);
            this.wcRollbackHistoryRepository.save(wcRollbackHistory);
        } catch (Exception e) {
            log.warn("insertRollBackHistory ERROR", e);
        }
    }
}
