/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.vo.MenuTagVO;
import com.cy.work.connect.repository.WCMenuTagRepository;
import com.cy.work.connect.vo.WCMenuTag;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCMenuTagManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1038591814105744406L;

    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;
    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCMenuTagRepository wcMenuTagRepository;

    /**
     * 依公司及類別名稱尋找單據名稱
     *
     * @param companySid
     * @param categoryName
     * @param menuName
     * @param tagName
     * @return
     */
    public List<WCMenuTag> findByTagNameAndCompanySid(
            Integer companySid, String categoryName, String menuName, String tagName) {
        return wcMenuTagRepository.findByTagNameAndCompanySid(
                companySid, categoryName, menuName, tagName);
    }

    /**
     * 刪除單據名稱
     *
     * @param sid
     */
    public void deleteMenuTag(String sid) {
        this.wcMenuTagRepository.delete(sid);
        log.info("刪除類別選單:" + sid);
    }

    /**
     * 建立單據名稱
     *
     * @param wcMenuTag    單據名稱物件
     * @param loginUserSid 登入者Sid
     * @return
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCMenuTag createWCMenuTag(WCMenuTag wcMenuTag, Integer loginUserSid) {
        wcMenuTag.setCreateDate(new Date());
        wcMenuTag.setCreateUser(loginUserSid);
        // 排序序號
        // wcMenuTag.setSeq(Integer.MAX_VALUE);
        wcMenuTag.setSeq(
                wcMenuTagCache
                        .getWCMenuTagByCategorySid(wcMenuTag.getWcCategoryTagSid(), Activation.ACTIVE)
                        .stream()
                        .reduce((a, b) -> b)
                        .map(WCMenuTag::getSeq)
                        .orElseGet(() -> 0)
                        + 1);
        wcMenuTag = wcMenuTagRepository.save(wcMenuTag);
        wcMenuTagCache.updateCacheByWCMenuTag(wcMenuTag);
        return wcMenuTag;
    }

    /**
     * 更新單據名稱
     *
     * @param wcMenuTag    單據名稱物件
     * @param loginUserSid 登入者Sid
     * @return
     */
    public WCMenuTag updateWCMenuTag(WCMenuTag wcMenuTag, Integer loginUserSid) {
        wcMenuTag.setUpdateDate(new Date());
        wcMenuTag.setUpdateUser(loginUserSid);
        wcMenuTag = wcMenuTagRepository.save(wcMenuTag);
        wcMenuTagCache.updateCacheByWCMenuTag(wcMenuTag);
        return wcMenuTag;
    }

    /**
     * 取得單據名稱List
     *
     * @param wcCategoryTagSid 類別Sid
     * @param activation
     * @return
     */
    public List<WCMenuTag> getWCMenuTagByCategorySid(
            String wcCategoryTagSid,
            Activation activation) {
        return wcMenuTagCache.getWCMenuTagByCategorySid(wcCategoryTagSid, activation);
    }

    /**
     * 取得單據名稱List By Sid list
     *
     * @param sids
     * @return
     */
    public List<WCMenuTag> getWCMenuTagsBySidList(List<String> sids) {
        List<WCMenuTag> result = Lists.newArrayList();
        result = wcMenuTagCache.getWCMenuTagByCategorySid(null, null).stream()
                .filter(each -> sids.contains(each.getSid()))
                .collect(Collectors.toList());
        return result;
    }

    public WCMenuTag getWCMenuTagBySid(String sid) {
        return wcMenuTagCache.getWCMenuTagBySid(sid);
    }
    
    /**
     * 取得中類名稱
     * @param sid 中類 sid
     * @return
     */
    public String findMenuTagName(String wcMenuTagSid) {
        WCMenuTag WCMenuTag = this.getWCMenuTagBySid(wcMenuTagSid);
        if(WCMenuTag!=null) {
            return WCMenuTag.getMenuName();
        }
        return "";
    }

    /**
     * get menu tag next seq by category sid
     *
     * @param categorySid
     * @return
     */
    public Integer getMenuTagNextSeqByCategory(String categorySid) {
        return wcMenuTagRepository.getMenuTagNextSeqByCategory(categorySid);
    }

    public WCMenuTag findMenuTagByCategoryAndName(String categorySid, String menuTagName) {
        return wcMenuTagRepository.findMenuTagByCategoryAndName(categorySid, menuTagName);
    }

    public WCMenuTag findMenuTagBySid(String sid) {
        return wcMenuTagRepository.findOne(sid);
    }

    public List<WCMenuTag> findAll() {
        return wcMenuTagRepository.findAll();
    }

    // public List<WCMenuTag> findBy

    /**
     * 依據傳入 List 順序, 更新排序序號
     *
     * @param sids
     */
    @Transactional()
    public void updateSortSeqByListIndex(List<String> sids) {

        // 傳入列表為空
        if (WkStringUtils.isEmpty(sids)) {
            return;
        }

        Integer index = 1;
        for (String sid : sids) {

            // 取資料檔
            WCMenuTag wcMenuTag = this.wcMenuTagRepository.findOne(sid);

            if (wcMenuTag == null) {
                continue;
            }

            wcMenuTag.setSeq(index++);

            // 更新
            this.wcMenuTagRepository.save(wcMenuTag);
            this.wcMenuTagCache.updateCacheByWCMenuTag(wcMenuTag);
        }
    }

    /**
     * 以可使用單位模版SID查詢
     *
     * @param useDepTempletSid
     * @return
     */
    public List<WCMenuTag> findByUseDepTempletSid(Long useDepTempletSid) {
        // 查詢
        List<WCMenuTag> results = this.wcMenuTagRepository.findByUseDepTempletSid(useDepTempletSid);

        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    /**
     * 取得多執行項目的中類(單據名稱清單)
     *
     * @param loginCompSid 登入公司Sid
     * @return
     */
    public List<MenuTagVO> getMutiTagMenuTags(Integer loginCompSid) {
        List<MenuTagVO> result = Lists.newArrayList();
        try {
            this.wcMenuTagRepository
                    .findMutiTagByCompanySid(loginCompSid)
                    .forEach(
                            item -> {
                                // 借用 MenuTagVO 屬性itemName當作 大類名稱
                                result.add(
                                        new MenuTagVO(
                                                item.getSid(),
                                                item.getMenuName(),
                                                wcCategoryTagCache
                                                        .getWCCategoryTagBySid(item.getWcCategoryTagSid())
                                                        .getCategoryName(),
                                                ""));
                            });
        } catch (Exception e) {
            log.warn("getMutiTagMenuTags ERROR", e);
        }
        return result;
    }

    /**
     * 以下列條件查詢
     * 
     * @param wcCategoryTagSids 大類 sid
     * @return WCMenuTag list
     */
    public List<WCMenuTag> findByWcCategoryTagSidIn(List<String> wcCategoryTagSids) {
        return this.wcMenuTagRepository.findByWcCategoryTagSidIn(wcCategoryTagSids);
    }

}
