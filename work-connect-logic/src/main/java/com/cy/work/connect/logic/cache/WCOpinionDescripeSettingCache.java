/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.commons.enums.Activation;
import com.cy.work.connect.logic.config.ConnectConstants;
import com.cy.work.connect.logic.vo.OpinionDescripeSettingVO;
import com.cy.work.connect.repository.WCOpinionDescripeSettingRepository;
import com.cy.work.connect.vo.WCOpinionDescripeSetting;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 意見說明Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCOpinionDescripeSettingCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1568839929190995576L;
    /**
     * 意見說明Repository
     */
    @Autowired
    private WCOpinionDescripeSettingRepository opinionDescripeSettingRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;

    private Map<OpinionDescripeSettingVO, WCOpinionDescripeSetting> opinionDescripeSettingMap;

    public WCOpinionDescripeSettingCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        opinionDescripeSettingMap = Maps.newConcurrentMap();
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /** 每30分鐘進行更新Cache資料 */
    /**
     * 排程更新Cache資料
     */
    @Scheduled(fixedDelay =  ConnectConstants.SCHEDULED_FIXED_DELAY_TIME)
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立意見說明Cache");
        try {
            Map<OpinionDescripeSettingVO, WCOpinionDescripeSetting> tempOpinionDescripeSettingMap =
                Maps.newConcurrentMap();
            opinionDescripeSettingRepository
                .findAll()
                .forEach(
                    item -> {
                        if (item.getStatus().equals(Activation.ACTIVE)) {
                            OpinionDescripeSettingVO os =
                                new OpinionDescripeSettingVO(
                                    item.getSourceType().name(), item.getSourceSid(),
                                    item.getOpinion_usr());
                            tempOpinionDescripeSettingMap.put(os, item);
                        }
                    });
            opinionDescripeSettingMap = tempOpinionDescripeSettingMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立意見說明結束");
        produceTime = Boolean.FALSE;
    }

    /**
     * 更新該簽呈Cache
     *
     * @param peSid 簽呈Sid
     */
    public void updateCache(String wcSid) {
        try {
            this.processWait();
            opinionDescripeSettingRepository
                .findOneByWcSid(wcSid)
                .forEach(
                    item -> {
                        if (item.getStatus().equals(Activation.ACTIVE)) {
                            OpinionDescripeSettingVO os =
                                new OpinionDescripeSettingVO(
                                    item.getSourceType().name(), item.getSourceSid(),
                                    item.getOpinion_usr());
                            opinionDescripeSettingMap.put(os, item);
                        }
                    });
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
    }

    /**
     * 更新該筆意見說明Cache
     *
     * @param opinionDescripeSetting 意見說明物件
     */
    public void updateCache(WCOpinionDescripeSetting opinionDescripeSetting) {
        this.processWait();
        OpinionDescripeSettingVO os =
            new OpinionDescripeSettingVO(
                opinionDescripeSetting.getSourceType().name(),
                opinionDescripeSetting.getSourceSid(),
                opinionDescripeSetting.getOpinion_usr());
        opinionDescripeSettingMap.put(os, opinionDescripeSetting);
    }

    /**
     * 取得意見說明物件
     *
     * @param managerSingInfoType 簽核類型-需求或者核准
     * @param sourceSid           簽核物件Sid
     * @param opinion_usr         意見說明的User Sid
     * @return
     */
    public WCOpinionDescripeSetting getOpinionDescripeSetting(
        ManagerSingInfoType managerSingInfoType, String sourceSid, Integer opinion_usr) {
        OpinionDescripeSettingVO os =
            new OpinionDescripeSettingVO(managerSingInfoType.name(), sourceSid, opinion_usr);
        return opinionDescripeSettingMap.get(os);
    }

    /**
     * 等待 建立意見說明Cache結束
     */
    private synchronized void processWait() {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立意見說明Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立意見說明Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
    }
}
