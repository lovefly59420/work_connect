/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.helper.WCExecManagerSignInfoCheckHelper;
import com.cy.work.connect.logic.helper.WCExecManagerSignInfoHelper;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.repository.WCExecManagerSignInfoRepository;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ManagerSingInfoType;
import com.cy.work.connect.vo.enums.SignActionType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 執行方簽核資訊 Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecManagerSignInfoManager implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5824087807783566735L;

    private static WCExecManagerSignInfoManager instance;
    public static WCExecManagerSignInfoManager getInstance() { return instance; }
    @Override
    public void afterPropertiesSet() throws Exception {
        WCExecManagerSignInfoManager.instance = this;
    }

    @Autowired
    private WCMasterRepository wcMasterRepository;
    @Autowired
    private WCExecManagerSignInfoHelper wcExecManagerSignInfoHelper;
    @Autowired
    private WCExecManagerSignInfoCheckHelper execManagerSignInfoCheckHelper;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;
    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCExecManagerSignInfoRepository wcExecManagerSignInfoRepository;
    @Autowired
    private BpmManager bpmManager;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;

    @Autowired
    private WCRollbackHistoryManager wcRollbackHistoryManager;

    /**
     * 意見說明,退回理由,作廢理由 Manager
     */
    @Autowired
    private WCOpinionDescripeSettingManager opinionDescripeSettingManager;
    @Autowired
    private WCTraceManager wcTraceManager;



    /**
     * 查詢 by Sid
     *
     * @param sid
     * @return
     */
    public WCExecManagerSignInfo findBySid(String sid) {
        return wcExecManagerSignInfoRepository.findBySid(sid);
    }

    /**
     * 取得執行單位簽核在該工作聯絡單最大群組Seq
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public int getMaxGroupSeq(String wcSid) {
        Integer groupSeq = wcExecManagerSignInfoRepository.findMaxGroupSeqByWcSid(wcSid);
        int maxGroupSeq = ((groupSeq == null) ? 0 : groupSeq) + 1;
        return maxGroupSeq;
    }

    /**
     * 執行方流程簽名
     * @param signInfo
     * @param execUser
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doExecFlowSign(WCExecManagerSignInfo signInfo, User execUser) throws UserMessageException {
        // ====================================
        // BPM 簽名
        // ====================================
        try {
            // 簽名
            this.bpmManager.sign(execUser.getId(), signInfo.getBpmId());

            // 即時更新流程資訊
            signInfo = wcExecManagerSignInfoCache.doSyncBpmData(
                    signInfo, SignActionType.NORMAL, execUser.getSid());

            // 更新執行單位簽核檔狀態
            signInfo = this.updateBpmStatus(
                    execUser.getId(),
                    signInfo,
                    bpmManager.getBpmStatus(ManagerSingInfoType.CHECKMANAGERINFO,execUser.getId(), signInfo.getBpmId(), null));
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(執行方簽名失敗)";
            log.error(errorMessage + e.getMessage() + " bpmid:[{}]", signInfo.getBpmId(), e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // 更新快取
        this.wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);

        // ====================================
        // 推進執行單位狀態
        // ====================================
        this.changeExecDepStatus(signInfo.getWcSid(), signInfo.getSid());
    }

    /**
     * 異動執行單位狀態
     * 
     * @param wcSid      主檔 sid
     * @param signedInfo
     */
    private void changeExecDepStatus(String wcSid, String execSignInfoSid) {

        // ====================================
        // 查詢相關的執行單位資料(待核准)
        // ====================================
        // 查詢
        List<WCExceDep> approvingExecDeps = this.wcExecDepManager.getApprovingExecDeps(
                wcSid, execSignInfoSid);
        // empty
        if (WkStringUtils.isEmpty(approvingExecDeps)) {
            return;
        }

        // ====================================
        // 更新執行單位狀態
        // ====================================
        for (WCExceDep wcExceDep : approvingExecDeps) {

            // 查詢同群組的流程是否都簽核完畢了
            boolean isGroupAllApproved = this.isGroupExecSignInfoApproved(
                    wcSid,
                    wcExceDep.getExecManagerSignInfoGroupSeq());

            if (!isGroupAllApproved) {
                continue;
            }

            // 根據類型進行更新執行單位執行狀態
            this.wcExecDepManager.updateWaitWorkStatus(wcExceDep);
        }
    }

    /**
     * 刪除執行簽核(系統預設)
     *
     * @param wc_ID     工作聯絡單Sid
     * @param loginUser 登入者Sid
     */
    public void deleteExecFlowSignInfo(String wcSid, User loginUser) {

        // ====================================
        // 查詢主簽核流程
        // ====================================
        List<WCExecManagerSignInfo> execManagerSignInfos = this.findExecFlowSignInfos(wcSid);
        if (WkStringUtils.isEmpty(execManagerSignInfos)) {
            return;
        }

        // ====================================
        // 查詢主簽核流程
        // ====================================
        for (WCExecManagerSignInfo wcExecManagerSignInfo : execManagerSignInfos) {
            try {
                if (!Strings.isNullOrEmpty(wcExecManagerSignInfo.getBpmId())) {
                    this.bpmManager.invalid(loginUser.getId(), wcExecManagerSignInfo.getBpmId());
                }
                this.updateBpmStatus(loginUser.getId(), wcExecManagerSignInfo, BpmStatus.DELETE);
            } catch (Exception e) {
                log.error("清除執行方流程[wc_exec_manager_sign_info]失敗!sid:[" + wcExecManagerSignInfo.getSid() + "]", e);
            }
        }
    }

    /**
     * 清除執行單位會簽
     *
     * @param wc_ID     工作聯絡單Sid
     * @param loginUser 執行者Sid
     */
    public void clearContinueSignInfo(String wcSid, User loginUser) {
        // ====================================
        // 查詢
        // ====================================
        List<WCExecManagerSignInfo> execCounterSignInfos = this.findExecFlowCounterSignInfos(wcSid);
        if (WkStringUtils.isEmpty(execCounterSignInfos)) {
            return;
        }
        // ====================================
        // 清除
        // ====================================
        for (WCExecManagerSignInfo execCounterSignInfo : execCounterSignInfos) {
            // 清除會簽流程
            try {
                this.doClearBpmContinueSign(loginUser.getId(), execCounterSignInfo);
            } catch (Exception e) {
                log.error("清除會簽流程失敗 bpmid:[{}]" + e.getMessage(), execCounterSignInfo.getBpmId(), e);
            }
        }
    }

    /**
     * 此為清除會簽節點的BPM資訊, 會簽節點會根據需求流程節點簽名完畢,才會產生 若有人進行退回或復原 該BPM節點必須清除,但會簽人員資訊不需要再重新挑選
     *
     * @param userId   清除節點User ID
     * @param signInfo 簽核物件
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void doClearBpmContinueSign(String userId, WCExecManagerSignInfo signInfo)
            throws ProcessRestException {
        bpmManager.invalid(userId, signInfo.getBpmId());
        signInfo.setBpmId("");
        signInfo.setStatus(BpmStatus.WAIT_CREATE);
        signInfo.setUpdate_dt(new Date());
        signInfo.setBpmDefaultSignedName("");
        signInfo = wcExecManagerSignInfoRepository.save(signInfo);
        wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
    }


    /**
     * 將執行單位會簽簽核節點刪除
     *
     * @param wcExecManagerSignInfoSid 執行單位簽核物件Sid
     * @param loginUser                執行者
     * @throws Exception
     */
    public void deleteWCExecManagerSignInfo(String wcExecManagerSignInfoSid, User loginUser)
            throws Exception {
        
        WCExecManagerSignInfo wcExecManagerSignInfo = wcExecManagerSignInfoRepository.findBySid(
                wcExecManagerSignInfoSid);
        if (!Strings.isNullOrEmpty(wcExecManagerSignInfo.getBpmId())) {
            bpmManager.invalid(loginUser.getId(), wcExecManagerSignInfo.getBpmId());
        }
        this.updateBpmStatus(loginUser.getId(), wcExecManagerSignInfo, BpmStatus.DELETE);
    }

    /**
     * 更新BpmStatus
     *
     * @param userId
     * @param signInfo
     * @param status
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCExecManagerSignInfo updateBpmStatus(String userId, WCExecManagerSignInfo signInfo, BpmStatus status) {
        Preconditions.checkState(status != null, "BpmStatus 為NULL 無法更新簽核資訊，請確認！！");
        signInfo.setStatus(status);
        signInfo.setUpdate_dt(new Date());
        signInfo = wcExecManagerSignInfoRepository.save(signInfo);
        this.wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
        return signInfo;
    }

    /**
     * 執行執行單位會簽變更
     *
     * @param wcSid                        工作聯絡單Sid
     * @param wcNo                         工作聯絡單單號
     * @param removeSingleManagerSignInfos 欲移除執行單位簽核物件List
     * @param addUserFlowSid               欲新增簽核人員Sid List
     * @param loginUserSid                 執行者Sid
     * @throws ProcessRestException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void saveCounterSignInfo(
            String wcSid,
            String wcNo,
            List<SingleManagerSignInfo> removeSingleManagerSignInfos,
            List<Integer> addUserFlowSid,
            Integer loginUserSid)
            throws ProcessRestException {
        removeSingleManagerSignInfos.forEach(
                item -> {
                    User user = userManager.findBySid(item.getUserSid());
                    try {
                        log.debug(
                                "執行單位加簽移除 工作聯絡單單號-"
                                        + wcNo
                                        + "==移除加簽人員-userId"
                                        + user.getId()
                                        + "==instanceID:"
                                        + item.getInstanceID());
                        this.deleteWCExecManagerSignInfo(item.getSignInfoSid(), user);
                    } catch (IllegalStateException | IllegalArgumentException e) {
                        log.warn("bpmManager.invalid ERROR：{}", e.getMessage());
                        Preconditions.checkState(false, "刪除流程失敗！！");
                    } catch (Exception ex) {
                        log.warn("bpmManager.invalid ERROR", ex);
                        Preconditions.checkState(false, "刪除流程失敗！！");
                    }
                });

        addUserFlowSid.forEach(
                item -> {
                    try {
                        this.createContinueExecManagerSignInfo(item, wcSid, wcNo, loginUserSid);
                    } catch (Exception ex) {
                        log.warn("bpmManager.create ERROR", ex);
                    }
                });
        WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_ADD_SIGN_CHANGE(
                wcSid, wcNo, loginUserSid, removeSingleManagerSignInfos, addUserFlowSid);
        this.wcTraceManager.create(wcTrace, loginUserSid);
    }

    /**
     * 建立執行單位會簽
     *
     * @param userSid      欲新增簽核人員
     * @param wcSid        工作聯絡單Sid
     * @param wcNo         工作聯絡單單號
     * @param loginUserSid 執行人員Sid
     * @throws Exception
     */
    private void createContinueExecManagerSignInfo(
            Integer userSid, String wcSid, String wcNo, Integer loginUserSid) throws Exception {
        User user = userManager.findBySid(userSid);
        String bpmId = bpmManager.createBpmFlow(
                wcNo,
                user.getSid(),
                user.getPrimaryOrg().getCompanySid(),
                WcBpmFlowType.WC_CS_EXE_UNIT);
        log.debug("執行單位加簽新增 工作聯絡單單號-" + wcNo + "==新增加簽人員-userId" + user.getId());
        Preconditions.checkState(!Strings.isNullOrEmpty(bpmId), "BPM 流程建立失敗！！");
        WCExecManagerSignInfo signInfo = new WCExecManagerSignInfo();
        signInfo.setWcSid(wcSid);
        signInfo.setWcNo(wcNo);
        signInfo.setStatus(BpmStatus.NEW_INSTANCE);
        signInfo.setSignType(SignType.COUNTERSIGN);
        signInfo.setCreate_dt(new Date());
        signInfo.setCreate_user_sid(loginUserSid);
        signInfo.setUser_sid(userSid);
        signInfo.setDep_sid(user.getPrimaryOrg().getSid());
        signInfo.setGroupSeq(wcExecManagerSignInfoCache.getMaxGroupSeq(wcSid));
        signInfo.setSeq(wcExecManagerSignInfoCache.getMaxSeq(signInfo.getGroupSeq(), wcSid));
        signInfo.setBpmId(bpmId);
        signInfo = wcExecManagerSignInfoCache.doSyncBpmData(signInfo, null, null);
        wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
    }

    /**
     * 取得會簽簽核物件List
     *
     * @param wcSid 工作聯絡單單號
     * @return
     */
    public List<SingleManagerSignInfo> getCounterSignInfos(String wcSid) {
        List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
        if (groupManagerSignInfos == null || groupManagerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<SingleManagerSignInfo> counterSignInfos = Lists.newArrayList();
        groupManagerSignInfos.forEach(
                item -> {
                    List<SingleManagerSignInfo> subCounterSignInfos = item.getSingleManagerSignInfos().stream()
                            .filter(each -> each.getSignType().equals(SignType.COUNTERSIGN))
                            .collect(Collectors.toList());
                    if (subCounterSignInfos != null) {
                        counterSignInfos.addAll(subCounterSignInfos);
                    }
                });

        return counterSignInfos;
    }

    /**
     * 取得執行單位流程簽核List
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public List<SingleManagerSignInfo> getFlowSignInfos(String wcSid) {
        List<GroupManagerSignInfo> groupManagerSignInfos = wcExecManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
        if (groupManagerSignInfos == null || groupManagerSignInfos.isEmpty()) {
            return Lists.newArrayList();
        }
        List<SingleManagerSignInfo> counterSignInfos = Lists.newArrayList();
        groupManagerSignInfos.forEach(
                item -> {
                    List<SingleManagerSignInfo> subCounterSignInfos = item.getSingleManagerSignInfos().stream()
                            .filter(each -> each.getSignType().equals(SignType.SIGN))
                            .collect(Collectors.toList());
                    if (subCounterSignInfos != null) {
                        counterSignInfos.addAll(subCounterSignInfos);
                    }
                });

        return counterSignInfos;
    }

    public List<GroupManagerSignInfo> getGroupManagerSignInfo(String wcSid) {
        try {
            return wcExecManagerSignInfoCache.getGroupManagerSignInfo(wcSid);
        } catch (Exception e) {
            log.warn("getGroupManagerSignInfo ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 建立系統預設執行單位簽核流程
     *
     * @param wcSid        工作聯絡單Sid
     * @param loginUserSid 執行人員Sid
     */
    public void createDefaultExecFlow(String wcSid, Integer loginUserSid) {
        try {
            WCMaster wcMaster = this.wcMasterRepository.findBySid(wcSid);

            // 依據勾選的執行項目, 分別建立執行單位簽核流程
            // 逐筆處理 小類(執行項目)
            for (String item : wcMaster.getCategoryTagTo().getTagSids()) {
                this.wcExecManagerSignInfoHelper.createExecInfos(wcMaster, item, loginUserSid);
            }
        } catch (IllegalStateException | IllegalArgumentException e) {
            log.warn("createDefaultExecFlow ERROR：{}", e.getMessage());
        } catch (Exception e) {
            log.warn("createDefaultExecFlow ERROR", e);
        }
    }

    /**
     * 檢測是否有(任何一個)再會簽簽核
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckAnyContinue(String wcSid) {
        return this.findExecFlowCounterSignInfos(wcSid).size() > 0;
    }

    /**
     * 檢測是否(任何一個)核准者簽核節點已有簽名
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckAnyFlowSign(String wcSid) {
        Set<BpmStatus> targetBpmStatus = Sets.newHashSet(
                BpmStatus.APPROVED);
        for (WCExecManagerSignInfo signInfo : this.findExecFlowCounterSignInfos(wcSid)) {
            if (targetBpmStatus.contains(signInfo.getStatus())) {
                return true;
            }
        }

        return false;

        // 無法確認為何要判斷 BpmStatus.INVALID by allen

        // for (WCExecManagerSignInfo si : this.getFlowSignInfos(wcSid)) {
        // if (si.getStatus().equals(BpmStatus.APPROVED) || si.getStatus()
        // .equals(BpmStatus.INVALID)) {
        // return true;
        // }
        // }

    }

    /**
     * 檢測是否(任何一個)核准者簽核節點已有簽名
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isCheckAnyContinueSign(String wcSid) {
        for (SingleManagerSignInfo si : this.getCounterSignInfos(wcSid)) {
            if (si.getStatus().equals(BpmStatus.APPROVED) || si.getStatus()
                    .equals(BpmStatus.INVALID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查詢作用中的需求單位流程 (主流程 + 會簽)
     * 
     * @param wcSid
     * @return
     */
    public List<WCExecManagerSignInfo> findActiveExecFlowSignInfos(String wcSid) {
        // ====================================
        // 查詢
        // ====================================
        List<WCExecManagerSignInfo> execManagerSignInfos = this.wcExecManagerSignInfoRepository.findByWcSid(wcSid);

        if (WkStringUtils.isEmpty(execManagerSignInfos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾狀態
        // ====================================
        // 無效的流程狀態
        Set<BpmStatus> inActiveBpmStatus = Sets.newHashSet(BpmStatus.DELETE, BpmStatus.INVALID, BpmStatus.WAIT_CREATE);

        // 過濾
        return execManagerSignInfos.stream()
                // 過濾無效狀態
                .filter(signInfo -> !inActiveBpmStatus.contains(signInfo.getStatus()))
                .collect(Collectors.toList());
    }
    
    /**
     * 查詢簽核流程 for 畫面顯示
     * 
     * @param WcSid
     * @return
     */
    public List<WCExecManagerSignInfo> findAllExecFlowSignInfosForViewSignInfo(String WcSid) {
        // ====================================
        // 查詢
        // ====================================
        List<WCExecManagerSignInfo> managerSignInfos = this.wcExecManagerSignInfoRepository.findByWcSid(WcSid);

        if (WkStringUtils.isEmpty(managerSignInfos)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 過濾
        // ====================================
        // 簽核資訊不顯示刪除
        List<BpmStatus> statusfilters = Lists.newArrayList(
                BpmStatus.DELETE);

        return managerSignInfos.stream()
                .filter(signInfo -> !statusfilters.contains(signInfo.getStatus()))
                .collect(Collectors.toList());
    }

    /**
     * 查詢執行單位主簽核流程
     * 
     * @param wcSid 主單 sid
     * @return
     */
    public List<WCExecManagerSignInfo> findExecFlowSignInfos(String wcSid) {
        return this.findActiveExecFlowSignInfos(wcSid).stream()
                .filter(signInfo -> SignType.SIGN.equals(signInfo.getSignType()))
                .collect(Collectors.toList());
    }

    /**
     * 查詢可簽名的執行單位主流程
     * 
     * @param wcSid  主單 sid
     * @param userID user ID
     * @return 傳入 USER 可簽的主流程
     * @throws UserMessageException 錯誤時拋出
     */
    public List<WCExecManagerSignInfo> findCanSignExecFlowSignInfos(String wcSid, String userID) throws UserMessageException {

        // ====================================
        // 查詢所有 active 的主簽核流程
        // ====================================
        List<WCExecManagerSignInfo> execFlowSignInfos = this.findExecFlowSignInfos(wcSid);

        // ====================================
        // 收集可簽的流程
        // ====================================
        List<WCExecManagerSignInfo> canSignExecFlowSignInfos = Lists.newArrayList();
        for (WCExecManagerSignInfo execFlowSignInfo : execFlowSignInfos) {
            // 狀態需為待簽
            if (!BpmStatus.APPROVING.equals(execFlowSignInfo.getStatus())) {
                continue;
            }
            // 檢查是否為待簽人員
            try {
                if (this.bpmManager.isSignUserWithException(userID, execFlowSignInfo.getBpmId())) {
                    canSignExecFlowSignInfos.add(execFlowSignInfo);;
                }
            } catch (SystemOperationException e) {
                throw new UserMessageException(e.getMessage(), InfomationLevel.ERROR);
            }
        }
        return canSignExecFlowSignInfos;
    }

    /**
     * 查詢執行單位會簽流程
     * 
     * @param wcSid 主單 sid
     * @return
     */
    public List<WCExecManagerSignInfo> findExecFlowCounterSignInfos(String wcSid) {
        return this.findActiveExecFlowSignInfos(wcSid).stream()
                .filter(signInfo -> SignType.COUNTERSIGN.equals(signInfo.getSignType()))
                .collect(Collectors.toList());
    }

    /**
     * 是否相同群組的執行單位簽核物件皆已簽核完畢
     *
     * @param wcSid    工作聯絡單Sid
     * @param groupSeq 群組序號
     * @return true:全部狀態為APPROVED
     */
    public boolean isGroupExecSignInfoApproved(String wcSid, Integer groupSeq) {
        // ====================================
        // 查詢所有可用流程
        // ====================================
        List<WCExecManagerSignInfo> activeExecFlowSignInfos = this.findActiveExecFlowSignInfos(wcSid);

        // ====================================
        // 檢查
        // ====================================
        Optional<WCExecManagerSignInfo> Optional = activeExecFlowSignInfos.stream()
                // 限定同群組
                .filter(execFlowSignInfo -> WkCommonUtils.compareByStr(groupSeq, execFlowSignInfo.getGroupSeq()))
                // 尋找未核准的
                .filter(execFlowSignInfo -> !BpmStatus.APPROVED.equals(execFlowSignInfo.getStatus()))
                .findAny();

        // 回傳 (true:全部狀態為APPROVED )
        return !Optional.isPresent();
    }

    /**
     * 主要簽核流程作廢
     * 
     * @param execManagerSignInfo 簽核流程
     * @param stopReason          不執行原因
     * @param execUser            執行者
     * @return
     * @throws UserMessageException 錯誤時拋出
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public WCExecManagerSignInfo doExecFlowSignerStop(
            WCExecManagerSignInfo execManagerSignInfo,
            String stopReason,
            User execUser) throws UserMessageException {

        // ====================================
        // 防呆
        // ====================================
        if (!SignType.SIGN.equals(execManagerSignInfo.getSignType())) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(流程類型錯誤)";
            log.error(errorMessage + " sid:[{}]", execManagerSignInfo.getSid());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 寫不執行原因
        // ====================================
        this.opinionDescripeSettingManager.saveStopReason(
                ManagerSingInfoType.CHECKMANAGERINFO,
                execManagerSignInfo.getSid(),
                execUser.getSid(),
                stopReason,
                execManagerSignInfo.getWcSid());

        // ====================================
        // 執行單位改為終止
        // ====================================
        // 查詢此簽核流程下，相關的執行單位
        List<WCExceDep> exceDepsByCurrGroup = this.wcExecDepManager.getWCExceDepByWcIDAndGroupSeq(
                execManagerSignInfo.getWcSid(),
                execManagerSignInfo.getGroupSeq());

        // 將狀態改為終止
        for (WCExceDep wcExceDep : exceDepsByCurrGroup) {
            wcExceDep.setExecDepStatus(WCExceDepStatus.STOP);
            wcExceDep.setUpdate_usr(execUser.getSid());
            wcExceDep.setUpdate_dt(new Date());
        }

        // update
        this.wcExecDepManager.save(exceDepsByCurrGroup);

        // ====================================
        // 寫追蹤
        // ====================================
        // 收集退回的執行單位 (不重複)
        List<Integer> stopExecDepSids = exceDepsByCurrGroup.stream()
                .map(WCExceDep::getDep_sid)
                .distinct()
                .collect(Collectors.toList());

        // 建立追蹤檔資料
        WCTrace wcTrace = this.wcTraceCustomLogicManager.prepareForExecFlowSignerStop(
                execManagerSignInfo.getWcSid(),
                execManagerSignInfo.getWcNo(),
                stopExecDepSids,
                stopReason);

        // insert
        this.wcTraceManager.create(wcTrace, execUser.getSid());

        // ====================================
        // 寫追蹤
        // ====================================
        this.wcRollbackHistoryManager.insertRollBackHistory(execUser.getSid(), execManagerSignInfo.getWcSid());

        // ====================================
        // BPM 流程作廢
        // ====================================
        // 放在最後， 打 BPM 失敗時，前面全部 rollBack
        try {
            this.bpmManager.invalid(execUser.getId(), execManagerSignInfo.getBpmId());
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(簽核流程作廢失敗)";
            log.error(errorMessage + " bpmID:[{}]", execManagerSignInfo.getBpmId());
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 異動流程狀態
        // ====================================
        return this.updateBpmStatus(
                stopReason,
                execManagerSignInfo,
                BpmStatus.INVALID);
    }
    
    
    /**
     * 執行執行單位流程復原
     * 
     * @param execSignInfo 執行單位簽核資訊
     * @param execUser     執行者
     * @return true 有執行/false:無執行
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public boolean doExecFlowRecovery(WCExecManagerSignInfo execSignInfo, User execUser) throws UserMessageException {

        // ====================================
        // 取得水管圖
        // ====================================
        if (WkStringUtils.isEmpty(execSignInfo.getBpmId())) {
            log.warn("wc_exec_manager_sign_info.bpm_instance_id  不應為空! sid:[{}]", execSignInfo.getSid());
            return false;
        }
        // 水管圖
        List<ProcessTaskBase> processTaskBases = this.bpmManager.findSimulationChart(
                execUser.getId(), execSignInfo.getBpmId());
        if (WkStringUtils.isEmpty(processTaskBases)) {
            log.warn("水管圖為空! wc_exec_manager_sign_info.bpm_instance_id:[{}]", execSignInfo.getBpmId());
            return false;
        }

        // ====================================
        // 檢查不可簽的狀況
        // ====================================
        // 前一個流程的簽核者需為自己
        boolean isShowRecovery = this.execManagerSignInfoCheckHelper.isShowRecovery(
                execUser.getId(), execSignInfo.getStatus(), processTaskBases);
        if (!isShowRecovery) {
            return false;
        }

        // 有已經在執行的單位不可復原
        boolean isExecWorking = this.wcExecDepManager.isExecWorking(execSignInfo.getWcSid(), execSignInfo);
        if (isExecWorking) {
            return false;
        }

        // ====================================
        // BPM 復原
        // ====================================
        int recoverySub = BpmStatus.APPROVED.equals(execSignInfo.getStatus()) ? 1 : 2;
        try {
            this.bpmManager.recovery(
                    execUser.getId(),
                    (ProcessTaskHistory) processTaskBases.get(processTaskBases.size() - recoverySub));
        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(BPM復原失敗!)";
            log.error(errorMessage + "[" + e.getMessage() + "]", e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 同步簽核資訊
        // ====================================
        // 同步簽核資訊
        try {
            execSignInfo = this.wcExecManagerSignInfoCache.doSyncBpmData(
                    execSignInfo,
                    SignActionType.RECOVERY,
                    execUser.getSid());

            // 更新簽核狀態
            BpmStatus bpmStatus = bpmManager.getBpmStatus(
                    ManagerSingInfoType.CHECKMANAGERINFO,
                    execUser.getId(), execSignInfo.getBpmId(), null);
            execSignInfo = this.updateBpmStatus(
                    execUser.getId(),
                    execSignInfo,
                    bpmStatus);

        } catch (ProcessRestException e) {
            String errorMessage = WkMessage.PROCESS_FAILED + "(復原成功，但同步簽核流程資料失敗)";
            log.error(errorMessage + "[" + e.getMessage() + "]", e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // 更新快取
        this.wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(execSignInfo);

        // ====================================
        // 復原執行單位核准狀態
        // ====================================
        this.wcExecDepManager.updateExecWorkingApproving(execSignInfo.getWcSid(), execSignInfo);

        return true;
    }

}
