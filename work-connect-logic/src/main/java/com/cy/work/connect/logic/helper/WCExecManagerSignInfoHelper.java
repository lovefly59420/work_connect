/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCExecDepManager;
import com.cy.work.connect.logic.manager.WCExecDepSettingManager;
import com.cy.work.connect.logic.manager.WCExecManagerSignInfoManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.logic.vo.enums.WcBpmFlowType;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.ExecDepType;
import com.cy.work.connect.vo.enums.SignType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 執行單位Helper
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecManagerSignInfoHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2650006576359631730L;

    @Autowired
    private WkUserCache userManager;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;
    @Autowired
    private BpmManager bpmManager;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCExecDepManager wcExecDepManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;

    /**
     * 建立執行單位BPM流程
     *
     * @param wcMaster 工作聯絡單物件
     * @param signUser 欲簽核人員
     * @return
     * @throws Exception
     */
    public WCExecManagerSignInfo createExecFlow(WCMaster wcMaster, User signUser, int groupSeq)
            throws Exception {

        // ====================================
        // 建立執行方簽核流程 (BPM)
        // ====================================
        String bpmId = bpmManager.createBpmFlow(
                wcMaster.getWc_no(),
                signUser.getSid(),
                signUser.getPrimaryOrg().getCompanySid(),
                WcBpmFlowType.WC_EXEC);

        // ====================================
        // 建立執行方簽核流程資料
        // ====================================
        WCExecManagerSignInfo wcExecManagerSignInfo = new WCExecManagerSignInfo();
        wcExecManagerSignInfo.setWcSid(wcMaster.getSid());
        wcExecManagerSignInfo.setWcNo(wcMaster.getWc_no());

        wcExecManagerSignInfo.setStatus(BpmStatus.APPROVING);
        wcExecManagerSignInfo.setSignType(SignType.SIGN);
        wcExecManagerSignInfo.setBpmId(bpmId);
        wcExecManagerSignInfo.setUser_sid(signUser.getSid());
        wcExecManagerSignInfo.setDep_sid(signUser.getPrimaryOrg().getSid());
        wcExecManagerSignInfo.setGroupSeq(groupSeq);

        // 沒有用? by allen
        wcExecManagerSignInfo.setSeq(
                wcExecManagerSignInfoCache.getMaxSeq(
                        wcExecManagerSignInfo.getGroupSeq(), wcMaster.getSid()));

        wcExecManagerSignInfo.setCreate_user_sid(null);
        wcExecManagerSignInfo.setCreate_dt(new Date());
        wcExecManagerSignInfo = wcExecManagerSignInfoCache.doSyncBpmData(wcExecManagerSignInfo, null, null);
        wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(wcExecManagerSignInfo);
        return wcExecManagerSignInfo;
    }

    /**
     * 根據執行單位設定檔建置執行單位級流程
     *
     * @param wcMaster         工作聯絡單物件
     * @param wcExecDepSetting 執行單位設定檔物件
     * @throws Exception
     */
    private void createExecInfo(WCMaster wcMaster, WCExecDepSetting wcExecDepSetting) throws Exception {

        // ====================================
        // 取得執行單位簽核在該工作聯絡單最大群組序號
        // ====================================
        int groupSeq = this.wcExecManagerSignInfoManager.getMaxGroupSeq(wcMaster.getSid());

        // ====================================
        // 建立執行單位簽核流程
        // ====================================
        List<WCExecManagerSignInfo> wcExecManagerSignInfos = Lists.newArrayList();

        // 執行單位簽核人員
        Set<Integer> execSignMemberUserSids = wcExecDepSetting.getExecSignMemberUserSids();

        // 檢測設定檔,是否有設定審核人員,若有必須建置簽核節點
        if (WkStringUtils.notEmpty(execSignMemberUserSids)) {

            for (Integer execSignMemberUserSid : execSignMemberUserSids) {
                // 簽核人員資料
                User execSignMemberUser = WkUserCache.getInstance().findBySid(execSignMemberUserSid);
                // 建立執行方簽核流程
                WCExecManagerSignInfo wcExecManagerSignInfo = this.createExecFlow(
                        wcMaster,
                        execSignMemberUser,
                        groupSeq);

                wcExecManagerSignInfos.add(wcExecManagerSignInfo);
            }
        }
        // 簽核群組Seq,當有需求必須兩個已上的人簽核完畢才可執行時,便利用群組Seq將其綁定
        Integer execManagerSignInfoGroupSeq = (wcExecManagerSignInfos != null && !wcExecManagerSignInfos.isEmpty())
                ? groupSeq
                : null;

        // 簽核流程sid
        String execManagerSignInfoSid = (wcExecManagerSignInfos != null && !wcExecManagerSignInfos.isEmpty())
                ? wcExecManagerSignInfos.get(0).getSid()
                : null;

        // 是否需要簽核
        ExecDepType execDepType = (wcExecManagerSignInfos != null && !wcExecManagerSignInfos.isEmpty())
                ? ExecDepType.NEED_SIGN
                : ExecDepType.NO_SIGN;

        // 執行單位狀態
        WCExceDepStatus wcExceDepStatus = null;

        if (WkStringUtils.notEmpty(wcExecManagerSignInfos)) {
            // 有簽核人員待簽時，為簽核中
            wcExceDepStatus = WCExceDepStatus.APPROVING;

        } else if (wcExecDepSetting.isNeedAssigned()) {
            // 無需簽核，為派工模式
            wcExceDepStatus = WCExceDepStatus.WAITSEND;

        } else {
            // 無需簽核，為領單模式
            wcExceDepStatus = WCExceDepStatus.WAITRECEIVCE;
        }

        // ====================================
        // 建立執行單位檔 wc_exec_dep
        // ====================================
        wcExecDepManager.createExceDep(
                wcMaster.getSid(),
                execManagerSignInfoGroupSeq,
                execManagerSignInfoSid,
                wcExecDepSetting.getSid(),
                wcExecDepSetting.getExec_dep_sid(),
                execDepType,
                wcExceDepStatus,
                wcExecDepSetting.isOnlyReceviceUser() ? wcExecDepSetting.getReceviceUser() : null);
    }

    /**
     * 建立執行單位資料及BPM流程
     *
     * @param wcMaster     工作聯絡單主檔
     * @param tagSid       小類(執行項目)Sid
     * @param loginUserSid 登入者Sid
     * @throws ProcessRestException
     */
    public void createExecInfos(WCMaster wcMaster, String tagSid, Integer loginUserSid)
            throws Exception {

        // 取得小類(執行項目)的執行單位 (直接撈 DB)
        List<WCExecDepSetting> execDepSettings = this.execDepSettingManager.findByWcTagSidAndStatus(
                tagSid,
                Activation.ACTIVE,
                false);

        for (WCExecDepSetting execDepSetting : execDepSettings) {
            // 根據執行單位設定檔建置執行單位級流程
            this.createExecInfo(wcMaster, execDepSetting);
        }

        // 若有執行單位加簽,將一併建置BPM流程
        this.reBackContinueFlow(wcMaster.getSid(), wcMaster.getWc_no());
    }

    /**
     * 恢復執行單位加簽BPM流程
     *
     * @param wcSid 工作聯絡單Sid
     * @param wcNo  工作聯絡單單號
     */
    private void reBackContinueFlow(String wcSid, String wcNo) {
        wcExecManagerSignInfoManager
                .getCounterSignInfos(wcSid)
                .forEach(
                        item -> {
                            try {
                                User user = userManager.findBySid(item.getUserSid());
                                String bpmId = bpmManager.createBpmFlow(
                                        wcNo,
                                        user.getSid(),
                                        user.getPrimaryOrg().getCompanySid(),
                                        WcBpmFlowType.WC_CS_EXE_UNIT);
                                Preconditions.checkState(!Strings.isNullOrEmpty(bpmId), "BPM 流程建立失敗！！");
                                WCExecManagerSignInfo signInfo = wcExecManagerSignInfoManager.findBySid(item.getSignInfoSid());
                                signInfo.setStatus(BpmStatus.NEW_INSTANCE);
                                signInfo.setBpmId(bpmId);
                                signInfo = wcExecManagerSignInfoCache.doSyncBpmData(signInfo, null, null);
                                wcExecManagerSignInfoCache.updateCacheByWcIDAndGroupSeq(signInfo);
                            } catch (IllegalStateException | IllegalArgumentException e) {
                                log.warn("bpmManager.create ERROR：{}", e.getMessage());
                                Preconditions.checkState(false, e.getMessage());
                            } catch (Exception ex) {
                                log.warn("bpmManager.create ERROR", ex);
                                Preconditions.checkState(false, "新增流程失敗！！");
                            }
                        });
    }

    /**
     * 將執行單位簽核DB物件 轉換成 自訂物件
     *
     * @param wcExecManagerSignInfos 執行單位簽核物件List
     * @return
     */
    public List<SingleManagerSignInfo> settingSingleManagerSignInfo(
            List<WCExecManagerSignInfo> wcExecManagerSignInfos) {
        if (wcExecManagerSignInfos == null) {
            return Lists.newArrayList();
        }
        Collections.sort(
                wcExecManagerSignInfos,
                new Comparator<WCExecManagerSignInfo>() {
                    @Override
                    public int compare(WCExecManagerSignInfo o1, WCExecManagerSignInfo o2) {
                        return o1.getSeq().compareTo(o2.getSeq());
                    }
                });
        List<SingleManagerSignInfo> results = Lists.newArrayList();
        wcExecManagerSignInfos.forEach(
                item -> {
                    if (item.getStatus() == null || BpmStatus.DELETE.equals(item.getStatus())) {
                        return;
                    }
                    SingleManagerSignInfo singleManagerSignInfo = new SingleManagerSignInfo(
                            item.getSid(),
                            item.getBpmId(),
                            item.getUser_sid(),
                            item.getDep_sid(),
                            item.getSignType(),
                            item.getStatus(),
                            item.getBpmDefaultSignedName(),
                            item.getCreate_user_sid(),
                            item.getGroupSeq());
                    results.add(singleManagerSignInfo);
                });
        return results;
    }

    /**
     * 取得水管圖 By 執行單位簽核物件
     *
     * @param wcExecManagerSignInfo 執行單位簽核物件
     * @return
     */
    public List<ProcessTaskBase> settingProcessTaskBase(
            WCExecManagerSignInfo wcExecManagerSignInfo) {
        return bpmManager.findSimulationChart("", wcExecManagerSignInfo.getBpmId());
    }

    /**
     * 轉換水管圖 By 執行單位簽核物件
     *
     * @param wcExecManagerSignInfos 執行單位簽核物件List
     * @return
     */
    public List<ProcessTaskBase> settingProcessTaskBase(
            List<WCExecManagerSignInfo> wcExecManagerSignInfos) {
        if (wcExecManagerSignInfos == null) {
            return Lists.newArrayList();
        }
        List<ProcessTaskBase> results = Lists.newArrayList();

        Collections.sort(
                wcExecManagerSignInfos,
                new Comparator<WCExecManagerSignInfo>() {
                    @Override
                    public int compare(WCExecManagerSignInfo o1, WCExecManagerSignInfo o2) {
                        return o1.getSeq().compareTo(o2.getSeq());
                    }
                });
        wcExecManagerSignInfos.forEach(
                item -> {
                    results.addAll(settingProcessTaskBase(item));
                });
        return results;
    }
}
