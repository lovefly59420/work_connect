package com.cy.work.connect.logic.helper;

import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.cache.WCBaseDepAccessInfoCache;
import com.cy.work.connect.logic.cache.WCBasePersonAccessInfoCache;
import com.cy.work.connect.logic.cache.WCCategoryTagCache;
import com.cy.work.connect.logic.cache.WCExecDepSettingCache;
import com.cy.work.connect.logic.cache.WCExecManagerSignInfoCache;
import com.cy.work.connect.logic.cache.WCHomepageFavoriteCache;
import com.cy.work.connect.logic.cache.WCManagerSignInfoCache;
import com.cy.work.connect.logic.cache.WCMemoCategoryCache;
import com.cy.work.connect.logic.cache.WCMenuTagCache;
import com.cy.work.connect.logic.cache.WCOpinionDescripeSettingCache;
import com.cy.work.connect.logic.cache.WCTagCache;
import com.cy.work.connect.logic.cache.WCTransSpecialPermissionCache;
import com.cy.work.connect.logic.cache.WorkParamCache;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 清除專案所有快取 ()
 *
 * @author Allen
 */
@Component
@Slf4j
public class ClearCacheHelper implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3001593011470983880L;
    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static ClearCacheHelper instance;
    // ========================================================================
    // Log 控制
    // ========================================================================
    private static List<String> latestProcessLogs;
    private static int latestProcessIndex = 0;
    private static long latestProcessStartTime = 0;
    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private WkCommonCache wkCommonCache;
    @Autowired
    private WCCategoryTagCache wcCategoryTagCache;
    @Autowired
    private WCExecDepSettingCache execDepSettingCache;
    @Autowired
    private WCMenuTagCache wcMenuTagCache;
    @Autowired
    private WCOpinionDescripeSettingCache wcOpinionDescripeSettingCache;
    @Autowired
    private WCTagCache wcTagCache;
    @Autowired
    private WCHomepageFavoriteCache wcHomepageFavoriteCache;
    @Autowired
    private WorkParamCache workParamCache;
    @Autowired
    private WCBaseDepAccessInfoCache wcBaseDepAccessInfoCache;
    @Autowired
    private WCBasePersonAccessInfoCache wcBasePersonAccessInfoCache;
    @Autowired
    private WCManagerSignInfoCache wcManagerSignInfoCache;
    @Autowired
    private WCExecManagerSignInfoCache wcExecManagerSignInfoCache;
    @Autowired
    private WCMemoCategoryCache wcMemoCategoryCache;
    @Autowired
    private WCTransSpecialPermissionCache wcTransSpecialPermissionCache;

    public static ClearCacheHelper getInstance() {
        return instance;
    }

    private static void setInstance(ClearCacheHelper instance) {
        ClearCacheHelper.instance = instance;
    }

    /**
     * 初始化 log 變數
     */
    private static void initLatestProcessLog() {
        ClearCacheHelper.latestProcessLogs = Lists.newArrayList();
        ClearCacheHelper.latestProcessIndex = 0;

        // latest log message
        List<String> logMessages = Lists.newArrayList();
        logMessages.add("====================================");
        logMessages.add(" 開始重建所有系統快取");
        logMessages.add("====================================");
        ClearCacheHelper.latestProcessLogs.addAll(logMessages);

        // 自己的 log
        for (String logMessage : logMessages) {
            log.info(logMessage);
        }
    }

    private static void addStartLog(String cacheName) {
        // index + 1
        latestProcessIndex++;

        // latest log message
        List<String> logMessages = Lists.newArrayList();
        logMessages.add("-------------------------");
        logMessages.add(" " + latestProcessIndex + ".【" + cacheName + "】");
        logMessages.add("-------------------------");
        logMessages.add("start");
        ClearCacheHelper.latestProcessLogs.addAll(logMessages);

        // 自己的 log
        for (String logMessage : logMessages) {
            log.info(logMessage);
        }

        // 初始化執行時間 (當下)
        ClearCacheHelper.latestProcessStartTime = System.currentTimeMillis();
    }

    private static void addSeccussLog() {
        String logMessage =
            "finish ! cost:[" + WkCommonUtils.calcCostSec(latestProcessStartTime) + " sec]";
        ClearCacheHelper.latestProcessLogs.add(logMessage);
        log.info(logMessage);
    }

    private static void addExceptionLog(Exception e) {
        String logMessage = "執行失敗 ! 【" + e.getMessage() + "】";
        ClearCacheHelper.latestProcessLogs.add(logMessage);
        log.error(logMessage, e);
    }

    private static void addFinishLog() {
        String logMessage = "★★★★所有快取清除已完成★★★★";
        ClearCacheHelper.latestProcessLogs.add(logMessage);
        log.info(logMessage);
    }

    public static String getLatestProcessLog(boolean isHtmlLineSeparatorStyle) {

        if (WkStringUtils.isEmpty(ClearCacheHelper.latestProcessLogs)) {
            return "尚未執行快取清除！";
        }

        String lineSeparator = System.lineSeparator();
        if (isHtmlLineSeparatorStyle) {
            lineSeparator = "<br/>";
        }

        return ClearCacheHelper.latestProcessLogs.stream()
            .collect(Collectors.joining(lineSeparator));
    }

    /**
     * 主執行方法
     */
    public static synchronized void process() {

        if (ClearCacheHelper.getInstance() == null
            || ClearCacheHelper.getInstance().workParamCache == null) {
            throw new SystemDevelopException("元件尚未初始化，需延後呼叫順序!");
        }

        // 初始化 log 變數
        ClearCacheHelper.initLatestProcessLog();

        String cacheName = "";

        // ====================================
        // WKCommonCache
        // ====================================
        cacheName = "WKCommonCache";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wkCommonCache.clearAllCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 系統參數 (work_backend_param)
        // ====================================
        cacheName = "系統參數 (work_backend_param)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().workParamCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 備忘錄類別 (wc_memo_category)
        // ====================================
        cacheName = "備忘錄類別 (wc_memo_category)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcMemoCategoryCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據類別設定：大類 (wc_category_tag)
        // ====================================
        cacheName = "單據類別設定：大類 (wc_category_tag)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcCategoryTagCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據類別設定：中類 (wc_menu_tag)
        // ====================================
        cacheName = "單據類別設定：中類 (wc_menu_tag)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcMenuTagCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據類別設定：小類 (wc_tag)
        // ====================================
        cacheName = "單據類別設定：小類 (wc_tag)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcTagCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據類別設定：執行單位設定 (wc_exec_dep_setting)
        // ====================================
        cacheName = "單據類別設定：執行單位設定 (wc_exec_dep_setting)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().execDepSettingCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 特殊權限：轉單 (wc_trans_special_permission)
        // ====================================
        cacheName = "特殊權限：轉單 (wc_trans_special_permission)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcTransSpecialPermissionCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 特殊權限：可閱讀部門 (wc_base_dep_access_info)
        // ====================================
        cacheName = "特殊權限：可閱讀部門權限 (wc_base_dep_access_info)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcBaseDepAccessInfoCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 特殊權限：可閱讀成員 (wc_base_person_access_info)
        // ====================================
        cacheName = "特殊權限：可閱讀成員權限 (wc_base_person_access_info)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcBasePersonAccessInfoCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據：需求方簽核資訊 (wc_manager_sign_info)
        // ====================================
        cacheName = "單據：需求方簽核資訊 (wc_manager_sign_info) (僅同步DB資料, 未同步BPM)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcManagerSignInfoCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據：執行方簽核資訊 (wc_exec_manager_sign_info)
        // ====================================
        cacheName = "單據：執行方簽核資訊 (wc_exec_manager_sign_info) (僅同步DB資料, 未同步BPM)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcExecManagerSignInfoCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 單據：意見說明 (wc_opinion_descripe_setting)
        // ====================================
        cacheName = "單據：意見說明 (wc_opinion_descripe_setting)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcOpinionDescripeSettingCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // ====================================
        // 個人：首頁收藏資訊 (wc_homepage_favorite)
        // ====================================
        cacheName = "個人：首頁收藏資訊 (wc_homepage_favorite)";
        ClearCacheHelper.addStartLog(cacheName);
        try {
            ClearCacheHelper.getInstance().wcHomepageFavoriteCache.updateCache();
            ClearCacheHelper.addSeccussLog();
        } catch (Exception e) {
            ClearCacheHelper.addExceptionLog(e);
        }

        // 完成log
        ClearCacheHelper.addFinishLog();
    }

    // ========================================================================
    // 主方法
    // ========================================================================

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }
}
