package com.cy.work.connect.logic.helper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.model.LegitimateRangeDateModel;
import com.cy.work.connect.logic.helper.model.StartEndDateModel;
import com.cy.work.connect.vo.WCTag;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 小類-其他判斷-日期檢查邏輯
 * 
 * @author allen1214_wu
 */
@Service
@Slf4j
public class CheckLegalDateHelper {

    // ========================================================================
    //
    // ========================================================================
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    // ========================================================================
    //
    // ========================================================================
    /**
     * 檢查小類設定『其他判斷1』、『其他判斷2』
     * 
     * @param wcSid                      聯絡單主檔 sid
     * @param sendDate                   送件日
     * @param legitimateRangeDateJsonStr 判斷1輸入資料
     * @param StartEndDateJsonStr        判斷2輸入資料
     * @param wcTags                     該聯絡單所有的小類設定資料
     * @throws UserMessageException 檢核錯誤時拋出
     */
    public void checkLegitimateRangeAndSendEndDate(
            String wcSid,
            Date sendDate,
            String legitimateRangeDateJsonStr,
            String StartEndDateJsonStr,
            List<WCTag> wcTags) throws UserMessageException {

        // ====================================
        // 防呆
        // ====================================
        // 無送件日期資料，到此階段應該不可能發生
        if (sendDate == null) {
            String errorMessage = WkMessage.EXECTION + "單據無送件日期資料!";
            log.error(errorMessage + "(wc_master.send_dt) wcSid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // 防呆
        if (WkStringUtils.isEmpty(wcTags)) {
            String errorMessage = WkMessage.NEED_RELOAD + "(找不到對應的小類資料)";
            log.error(errorMessage + "單據無執行項目(小類)資料 (wc_master.category_tag) wcsid:[{}]", wcSid);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 取得日期欄位資料
        // ====================================
        // 解析 legitimateRangeDate 資料
        Map<String, LegitimateRangeDateModel> legitimateRangeDateMapByTagSid = this.parseLegitimateRangeDate(
                wcSid, legitimateRangeDateJsonStr);

        // 取得 StartEndDate 資料
        Map<String, StartEndDateModel> startEndDateMapByTagSid = this.parseStartEndDate(
                wcSid, StartEndDateJsonStr);

        // ====================================
        // 逐筆比對
        // ====================================
        for (WCTag wcTag : wcTags) {

            // -----------------
            // LegitimateRangeDate
            // -----------------
            if (wcTag.isLegitimateRangeCheck()) {
                // 取得小類對應資料
                LegitimateRangeDateModel dateModel = legitimateRangeDateMapByTagSid.get(wcTag.getSid());
                // 無資料
                if (dateModel == null || WkStringUtils.isEmpty(dateModel.getDate())) {
                    String message = String.format(
                            "欄位：[%s] 不可為空！",
                            wcTag.getLegitimateRangeTitle());
                    throw new UserMessageException(message);
                }
                // 日期格式轉換
                Date inputDate = null;
                try {
                    if (WkStringUtils.notEmpty(dateModel.getDate())) {
                        inputDate = dateFormat.parse(dateModel.getDate());
                    }
                } catch (ParseException e) {
                    String message = String.format("欄位：[%s] 輸入格式有誤！(%s)", wcTag.getLegitimateRangeTitle(), dateFormat.toPattern());
                    throw new UserMessageException(message);
                }
                // 檢查
                this.checkLegitimateRangeDate(wcTag, sendDate, inputDate);
            }

            // -----------------
            // StartEndSate
            // -----------------
            if (wcTag.isStartEndCheck()) {
                // 取得小類對應資料
                StartEndDateModel dateModel = startEndDateMapByTagSid.get(wcTag.getSid());
                // 無資料
                if (dateModel == null
                        || WkStringUtils.isEmpty(dateModel.getStartdate())
                        || WkStringUtils.isEmpty(dateModel.getEnddate())) {
                    String message = String.format(
                            "欄位：[%s] 不可為空！",
                            wcTag.getLegitimateRangeTitle());
                    throw new UserMessageException(message);
                }
                // 日期格轉換
                Date inputStartDate = null;
                try {
                    if (WkStringUtils.notEmpty(dateModel.getStartdate())) {
                        inputStartDate = dateFormat.parse(dateModel.getStartdate());
                    }
                } catch (ParseException e) {
                    String message = String.format("欄位：[%s] 起日輸入格式有誤！(%s)", wcTag.getStartEndTitle(), dateFormat.toPattern());
                    throw new UserMessageException(message);
                }
                Date inputEndDate = null;
                try {
                    if (WkStringUtils.notEmpty(dateModel.getEnddate())) {
                        inputEndDate = dateFormat.parse(dateModel.getEnddate());
                    }
                } catch (ParseException e) {
                    String message = String.format("欄位：[%s] 迄日輸入格式有誤！(%s)", wcTag.getStartEndTitle(), dateFormat.toPattern());
                    throw new UserMessageException(message);
                }

                // 檢查
                this.checkStartEndDate(wcTag, sendDate, inputStartDate, inputEndDate);
            }
        }
    }

    /**
     * 合法區間判斷規則1
     * 
     * @param wcTag     wcTag 設定資料檔
     * @param inputDate 輸入時間
     * @param sendDate  送件時間
     * @throws UserMessageException
     */
    public void checkLegitimateRangeDate(
            WCTag wcTag,
            Date sendDate,
            Date inputDate) throws UserMessageException {

        // ====================================
        // 無需檢查
        // ====================================
        if (!wcTag.isLegitimateRangeCheck()) {
            return;
        }

        // ====================================
        // 解析日期區間限制
        // ====================================
        int legitimateRangeDays = 0;
        try {
            legitimateRangeDays = Integer.parseInt(wcTag.getLegitimateRangeDays());
            if (legitimateRangeDays < 1) {
                throw new Exception();
            }
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "合法區間判斷天數設定有誤!";
            log.error(errorMessage + "wcTag:[{}], legitimateRangeDaysStr:[{}]",
                    wcTag.getSid(),
                    wcTag.getLegitimateRangeDays(),
                    e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 檢核輸入資料
        // ====================================
        if (inputDate == null) {
            String message = String.format("請輸入【%s】！", wcTag.getLegitimateRangeTitle());
            throw new UserMessageException(message);
        }

        // ====================================
        // 日期回歸0點
        // ====================================
        sendDate = WkDateUtils.toStartDay(sendDate);
        inputDate = WkDateUtils.toStartDay(inputDate);
        Date systemDate = WkDateUtils.toStartDay(new Date());

        // ====================================
        // 檢查 是否超過系統日
        // ====================================
        if (WkDateUtils.isAfter(inputDate, systemDate, false)) {
            String message = String.format(
                    "【%s】日期不可大於今日！",
                    wcTag.getLegitimateRangeTitle());
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 檢查 是否超過限制天數
        // ====================================
        // 【最後期限】 (畫面輸入日期 + 限制天數)
        Date limitDate = WkDateUtils.plusDays(inputDate, legitimateRangeDays - 1);

        if (WkDateUtils.isAfter(sendDate, limitDate, false)) {
            String message = String.format(
                    "【送件時間:%s】超過申請時間%s天！<br/>【%s:%s】的最晚送件日為【%s】",
                    WkDateUtils.formatDate(sendDate, WkDateUtils.YYYY_MM_DD),
                    legitimateRangeDays,
                    wcTag.getLegitimateRangeTitle(),
                    WkDateUtils.formatDate(inputDate, WkDateUtils.YYYY_MM_DD),
                    WkDateUtils.formatDate(limitDate, WkDateUtils.YYYY_MM_DD));
            log.debug("\r\n" + message);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

    }

    /**
     * 合法區間判斷規則2
     * 
     * @param wcTag          wcTag 設定資料檔
     * @param sendDate       送件時間
     * @param inputStartDate 輸入起日
     * @param inputEndDate   輸入迄日
     * @throws UserMessageException 錯誤時拋出
     */
    public void checkStartEndDate(
            WCTag wcTag,
            Date sendDate,
            Date inputStartDate,
            Date inputEndDate) throws UserMessageException {

        // ====================================
        // 無需檢查
        // ====================================
        if (!wcTag.isStartEndCheck()) {
            return;
        }

        // ====================================
        // 解析日期區間限制
        // ====================================
        int startEndDays = 0;
        try {
            startEndDays = Integer.parseInt(wcTag.getStartEndDays());
            if (startEndDays < 1) {
                throw new Exception();
            }
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "起迄合法天數設定有誤!";
            log.error(errorMessage + "wcTag:[{}], startEndDays:[{}]",
                    wcTag.getSid(),
                    wcTag.getStartEndDays(),
                    e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        int endRuleDays = 0;
        try {
            endRuleDays = Integer.parseInt(wcTag.getEndRuleDays());
            if (endRuleDays < 1) {
                throw new Exception();
            }
        } catch (Exception e) {
            String errorMessage = WkMessage.EXECTION + "結束日規範判斷(天)設定有誤!";
            log.error(errorMessage + "wcTag:[{}], endRuleDays:[{}]",
                    wcTag.getSid(),
                    wcTag.getStartEndDays(),
                    e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 檢核輸入資料
        // ====================================
        if (inputStartDate == null) {
            String message = String.format("請輸入【%s】- 起日！", wcTag.getStartEndTitle());
            throw new UserMessageException(message, InfomationLevel.WARN);
        }
        if (inputEndDate == null) {
            String message = String.format("請輸入【%s】- 迄日！", wcTag.getStartEndTitle());
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 日期回歸0點
        // ====================================
        sendDate = WkDateUtils.toStartDay(sendDate);
        inputStartDate = WkDateUtils.toStartDay(inputStartDate);
        inputEndDate = WkDateUtils.toStartDay(inputEndDate);

        // ====================================
        // 檢查 起日大於迄日
        // ====================================
        if (WkDateUtils.isAfter(inputStartDate, inputEndDate, false)) {
            String message = String.format("【%s】輸入錯誤，起日大於迄日！", wcTag.getStartEndTitle());
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 檢查 起迄日合法天數
        // ====================================
        // 說明 01~03 這樣算三天，合法，startEndDays 故-1天
        Date limitDate = WkDateUtils.plusDays(inputStartDate, startEndDays - 1);

        if (WkDateUtils.isAfter(limitDate, inputEndDate, false)) {
            String message = String.format(
                    "【%s】輸入錯誤，起迄日期間需大於%s天(含)以上！",
                    wcTag.getStartEndTitle(),
                    startEndDays);
            throw new UserMessageException(message, InfomationLevel.WARN);
        }

        // ====================================
        // 檢查 結束日規範判斷(天)
        // ====================================
        // 【最後期限】 (迄日 + 限制天數)
        limitDate = WkDateUtils.plusDays(inputEndDate, endRuleDays - 1);
        // 送件日期>最後期限
        if (WkDateUtils.isAfter(sendDate, limitDate, false)) {
            String message = String.format(
                    "【送件時間:%s】超過申請時間%s天！<br/>【%s-迄日:%s】的最晚送件日為【%s】",
                    WkDateUtils.formatDate(sendDate, WkDateUtils.YYYY_MM_DD),
                    endRuleDays,
                    wcTag.getStartEndTitle(),
                    WkDateUtils.formatDate(inputEndDate, WkDateUtils.YYYY_MM_DD),
                    WkDateUtils.formatDate(limitDate, WkDateUtils.YYYY_MM_DD));

            throw new UserMessageException(message, InfomationLevel.WARN);
        }
    }

    /**
     * 解析資料
     * 
     * @param wcSid                      聯絡單 sid
     * @param legitimateRangeDateJsonStr 資料 jsonStr
     * @return
     * @throws UserMessageException 錯誤時拋出
     */
    private Map<String, LegitimateRangeDateModel> parseLegitimateRangeDate(
            String wcSid,
            String legitimateRangeDateJsonStr) throws UserMessageException {

        // ====================================
        // 為空時無需執行
        // ====================================
        if (WkStringUtils.isEmpty(legitimateRangeDateJsonStr)) {
            return Maps.newHashMap();
        }

        // ====================================
        // JSON 轉 model list
        // ====================================
        List<LegitimateRangeDateModel> models = Lists.newArrayList();
        try {
            models = WkJsonUtils.getInstance().fromJsonToList(
                    legitimateRangeDateJsonStr,
                    LegitimateRangeDateModel.class);
        } catch (IOException e) {
            String errorMessage = WkMessage.EXECTION + " (申請日期資料錯誤)";
            log.error(errorMessage + "\r\n(wc_master.send_dt.legitimate_range_date) wcSid:【{}】,legitimateRangeDate:【{}】\r\n",
                    wcSid,
                    legitimateRangeDateJsonStr,
                    e);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        if (WkStringUtils.isEmpty(models)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 轉 map
        // ====================================
        return models.stream()
                .collect(Collectors.toMap(
                        LegitimateRangeDateModel::getSid,
                        model -> model));

    }

    /**
     * 解析資料
     * 
     * @param wcSid               聯絡單 sid
     * @param startEndDateJsonStr 資料 jsonStr
     * @return
     * @throws UserMessageException 錯誤時拋出
     */
    private Map<String, StartEndDateModel> parseStartEndDate(
            String wcSid,
            String startEndDateJsonStr) throws UserMessageException {

        // ====================================
        // 為空時無需執行
        // ====================================
        if (WkStringUtils.isEmpty(startEndDateJsonStr)) {
            return Maps.newHashMap();
        }

        // ====================================
        // JSON 轉 model list
        // ====================================
        List<StartEndDateModel> models = Lists.newArrayList();
        try {
            models = WkJsonUtils.getInstance().fromJsonToList(
                    startEndDateJsonStr,
                    StartEndDateModel.class);
        } catch (IOException e) {
            String errorMessage = WkMessage.EXECTION + " (申請日期資料錯誤)";
            log.error(errorMessage + "(wc_master.send_dt.legitimate_range_date) wcSid:【{}】,startEndDate:【{}】",
                    wcSid,
                    startEndDateJsonStr);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }

        if (WkStringUtils.isEmpty(models)) {
            return Maps.newHashMap();
        }

        // ====================================
        // 轉 map
        // ====================================
        return models.stream()
                .collect(Collectors.toMap(
                        StartEndDateModel::getSid,
                        model -> model));

    }
}
