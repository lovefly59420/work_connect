/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.cache;

import com.cy.bpm.rest.client.TaskClient;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.bpm.rest.vo.exception.ProcessRestException;
import com.cy.commons.enums.Activation;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.helper.WCExecManagerSignInfoHelper;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.manager.WCMasterManager;
import com.cy.work.connect.logic.vo.GroupManagerSignInfo;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.repository.WCExecManagerSignInfoDetailRepository;
import com.cy.work.connect.repository.WCExecManagerSignInfoRepository;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCExecManagerSignInfoDetail;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.cy.work.connect.vo.enums.SignActionType;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 執行單位簽核資訊Cache
 *
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WCExecManagerSignInfoCache implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7667179374678313972L;

    @Autowired
    private WCExecManagerSignInfoRepository wcExecManagerSignInfoRepository;
    /**
     * 控制啟動FLAG
     */
    private Boolean startController;
    /**
     * 執行任務FLAG
     */
    private Boolean produceTime;
    /**
     * Cache 物件
     */
    private Map<String, Map<Integer, List<WCExecManagerSignInfo>>> wcExecManagerSignInfoMap;
    /**
     * Cache 物件
     */
    private Map<String, SingleManagerSignInfo> singleManagerSignInfoMap;
    /**
     * TaskClient
     */
    @Autowired
    private TaskClient taskClient;
    /**
     * BpmManager
     */
    @Autowired
    private BpmManager bpmManager;
    /**
     * WorkSettingUserManager
     */
    @Autowired
    private WkUserCache userManager;
    /**
     * WCExecManagerSignInfoHelper
     */
    @Autowired
    private WCExecManagerSignInfoHelper wcExecManagerSignInfoHelper;
    /**
     * WCExecManagerSignInfoDetailRepository
     */
    @Autowired
    private WCExecManagerSignInfoDetailRepository wcExecManagerSignInfoDetailRepository;

    private boolean isRunning = false;
    @Autowired
    private WCMasterManager wcMasterManager;

    public WCExecManagerSignInfoCache() {
        startController = Boolean.TRUE;
        produceTime = Boolean.FALSE;
        wcExecManagerSignInfoMap = Maps.newConcurrentMap();
        singleManagerSignInfoMap = Maps.newConcurrentMap();
    }

    /**
     * 更新執行簽核資訊 By 工作聯絡單Sid
     *
     * @param wcSid 工作聯絡單Sid
     */
    public void updateCacheByWcSid(String wcSid) {
        try {
            this.processWait();
            List<WCExecManagerSignInfo> wcExecManagerSignInfos = wcExecManagerSignInfoRepository.findByWcSid(wcSid);
            Map<Integer, List<WCExecManagerSignInfo>> wcManagerSignInfos = Maps.newConcurrentMap();
            wcExecManagerSignInfos.forEach(
                    item -> {
                        singleManagerSignInfoMap.put(
                                item.getBpmId(),
                                new SingleManagerSignInfo(
                                        item.getSid(),
                                        item.getBpmId(),
                                        item.getUser_sid(),
                                        item.getDep_sid(),
                                        item.getSignType(),
                                        item.getStatus(),
                                        item.getBpmDefaultSignedName(),
                                        item.getCreate_user_sid(),
                                        item.getGroupSeq()));
                        List<WCExecManagerSignInfo> groupWcManagerSignInfos = wcManagerSignInfos.get(item.getGroupSeq());
                        if (groupWcManagerSignInfos == null) {
                            groupWcManagerSignInfos = Lists.newArrayList();
                        }
                        groupWcManagerSignInfos.add(item);
                        wcManagerSignInfos.put(item.getGroupSeq(), groupWcManagerSignInfos);
                    });
            wcExecManagerSignInfoMap.put(wcSid, wcManagerSignInfos);
        } catch (Exception e) {
            log.warn("updateCacheByWcSid ERROR", e);
        }
    }

    /**
     * 初始化資料
     */
    public void start() {
        if (startController) {
            startController = Boolean.FALSE;
            if (!produceTime) {
                this.updateCache();
            }
        }
    }

    /**
     * 取得可簽核人員ID By BmpID(InstanceID)
     *
     * @param bpmID BmpID(InstanceID)
     * @return
     */
    public List<String> findTaskCanSignUserIds(String bpmID) {
        try {
            if (Strings.isNullOrEmpty(bpmID)) {
                return Lists.newArrayList();
            }

            return taskClient.findTaskCanSignUserIds(bpmID);
        } catch (Exception e) {
            log.warn("findTaskCanSignUserIds ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得執行單位群組簽核資訊 By 工作聯絡單Sid
     *
     * @param wc_ID 工作聯絡單Sid
     * @return
     */
    public List<GroupManagerSignInfo> getGroupManagerSignInfo(String wc_ID) {
        Map<Integer, List<WCExecManagerSignInfo>> selWsis = wcExecManagerSignInfoMap.get(wc_ID);
        if (selWsis == null || selWsis.values().isEmpty()) {
            return Lists.newArrayList();
        }
        List<GroupManagerSignInfo> groupManagerSignInfos = Lists.newArrayList();
        selWsis
                .values()
                .forEach(
                        item -> {
                            if (item == null || item.isEmpty()) {
                                return;
                            }

                            GroupManagerSignInfo gsi = new GroupManagerSignInfo(
                                    item.get(0).getGroupSeq(),
                                    wcExecManagerSignInfoHelper.settingSingleManagerSignInfo(item));
                            groupManagerSignInfos.add(gsi);
                        });
        return groupManagerSignInfos;
    }

    /**
     * 更新Cache By 執行單位簽核物件
     *
     * @param wcExecManagerSignInfo 執行單位簽核物件
     */
    public void updateCacheByWcIDAndGroupSeq(WCExecManagerSignInfo wcExecManagerSignInfo) {
        this.processWait();
        Map<Integer, List<WCExecManagerSignInfo>> wcExecManagerSignInfos = wcExecManagerSignInfoMap.get(wcExecManagerSignInfo.getWcSid());
        if (wcExecManagerSignInfos == null) {
            wcExecManagerSignInfos = Maps.newConcurrentMap();
        }
        List<WCExecManagerSignInfo> groupWcManagerSignInfos = wcExecManagerSignInfos.get(wcExecManagerSignInfo.getGroupSeq());
        if (groupWcManagerSignInfos == null) {
            groupWcManagerSignInfos = Lists.newArrayList();
        }
        List<WCExecManagerSignInfo> tempWCExecManagerSignInfos = Lists.newArrayList();
        groupWcManagerSignInfos.forEach(
                item -> {
                    if (item.getSid().equals(wcExecManagerSignInfo.getSid())) {
                        return;
                    }
                    tempWCExecManagerSignInfos.add(item);
                });
        if (wcExecManagerSignInfo.getStatus() != null
                && !BpmStatus.DELETE.equals(wcExecManagerSignInfo.getStatus())) {
            tempWCExecManagerSignInfos.add(wcExecManagerSignInfo);
        }
        wcExecManagerSignInfos.put(wcExecManagerSignInfo.getGroupSeq(), tempWCExecManagerSignInfos);
        wcExecManagerSignInfoMap.put(wcExecManagerSignInfo.getWcSid(), wcExecManagerSignInfos);
        singleManagerSignInfoMap.put(
                wcExecManagerSignInfo.getBpmId(),
                new SingleManagerSignInfo(
                        wcExecManagerSignInfo.getSid(),
                        wcExecManagerSignInfo.getBpmId(),
                        wcExecManagerSignInfo.getUser_sid(),
                        wcExecManagerSignInfo.getDep_sid(),
                        wcExecManagerSignInfo.getSignType(),
                        wcExecManagerSignInfo.getStatus(),
                        wcExecManagerSignInfo.getBpmDefaultSignedName(),
                        wcExecManagerSignInfo.getCreate_user_sid(),
                        wcExecManagerSignInfo.getGroupSeq()));
    }

    /**
     * 取得目前Seq 最大值
     *
     * @param groupSeq 群組Seq
     * @param wc_ID    工作聯絡單Sid
     * @return
     */
    public int getMaxSeq(int groupSeq, String wc_ID) {
        Map<Integer, List<WCExecManagerSignInfo>> selWsis = wcExecManagerSignInfoMap.get(wc_ID);
        if (selWsis == null || selWsis.values().isEmpty()) {
            return 0;
        }
        List<WCExecManagerSignInfo> grouMsis = selWsis.get(groupSeq);
        if (grouMsis == null || grouMsis.isEmpty()) {
            return 0;
        }
        int seq = 0;
        for (WCExecManagerSignInfo ws : grouMsis) {
            if (seq < ws.getSeq()) {
                seq = ws.getSeq();
            }
        }
        return seq + 1;
    }

    /**
     * 取得目前GroupSeq最大值
     *
     * @param wc_ID 工作聯絡單Sid
     * @return
     */
    public int getMaxGroupSeq(String wc_ID) {
        Map<Integer, List<WCExecManagerSignInfo>> selWsis = wcExecManagerSignInfoMap.get(wc_ID);
        if (selWsis == null || selWsis.values().isEmpty()) {
            return 0;
        }
        int groupSeq = 0;
        for (Integer gs : selWsis.keySet()) {
            if (groupSeq < gs) {
                groupSeq = gs;
            }
        }
        return groupSeq + 1;
    }

    /**
     * 排程更新Cache資料
     */
    @Scheduled(cron = "0 0 0 ? * *")
    public void updateCache() {
        produceTime = Boolean.TRUE;
        log.debug("建立執行方簽核資訊Cache");
        try {
            Map<String, Map<Integer, List<WCExecManagerSignInfo>>> tempWCExecManagerSignInfoMap = Maps.newConcurrentMap();
            Map<String, SingleManagerSignInfo> tempSingleManagerSignInfoMap = Maps.newConcurrentMap();
            wcExecManagerSignInfoRepository
                    .findAll()
                    .forEach(
                            item -> {
                                if (item.getGroupSeq() == null
                                        || item.getStatus() == null
                                        || BpmStatus.DELETE.equals(item.getStatus())) {
                                    return;
                                }
                                tempSingleManagerSignInfoMap.put(
                                        item.getBpmId(),
                                        new SingleManagerSignInfo(
                                                item.getSid(),
                                                item.getBpmId(),
                                                item.getUser_sid(),
                                                item.getDep_sid(),
                                                item.getSignType(),
                                                item.getStatus(),
                                                item.getBpmDefaultSignedName(),
                                                item.getCreate_user_sid(),
                                                item.getGroupSeq()));
                                Map<Integer, List<WCExecManagerSignInfo>> wcManagerSignInfos = tempWCExecManagerSignInfoMap.get(item.getWcSid());
                                if (wcManagerSignInfos == null) {
                                    wcManagerSignInfos = Maps.newConcurrentMap();
                                }
                                List<WCExecManagerSignInfo> groupWcManagerSignInfos = wcManagerSignInfos.get(item.getGroupSeq());
                                if (groupWcManagerSignInfos == null) {
                                    groupWcManagerSignInfos = Lists.newArrayList();
                                }
                                groupWcManagerSignInfos.add(item);
                                wcManagerSignInfos.put(item.getGroupSeq(), groupWcManagerSignInfos);
                                tempWCExecManagerSignInfoMap.put(item.getWcSid(), wcManagerSignInfos);
                            });
            wcExecManagerSignInfoMap = tempWCExecManagerSignInfoMap;
            singleManagerSignInfoMap = tempSingleManagerSignInfoMap;
        } catch (Exception e) {
            log.warn("updateCache", e);
        }
        log.debug("建立執行方簽核資訊Cache結束");
        produceTime = Boolean.FALSE;
    }

    @Scheduled(cron = "0 0 0 ? * *") // 每隔四小時觸發 0 4 8 12...
    public void syncUpdateBpm() {
        if (isRunning) {
            return;
        }
        isRunning = true;
        log.debug("建立執行方簽核資訊同步資訊");

        for (Map<Integer, List<WCExecManagerSignInfo>> execManagerSignInfoMap : wcExecManagerSignInfoMap.values()) {

            for (List<WCExecManagerSignInfo> execManagerSignInfos : execManagerSignInfoMap.values()) {

                for (WCExecManagerSignInfo execManagerSignInfo : execManagerSignInfos) {

                    try {

                        WCExecManagerSignInfo wsi = wcExecManagerSignInfoRepository.findOne(execManagerSignInfo.getSid());
                        if (wsi == null) {
                            log.warn("wc_exec_manager_sign_info.sid:[" + execManagerSignInfo.getSid() + "]找不到");
                            continue;
                        }

                        if (WkStringUtils.isEmpty(wsi.getWcSid())) {
                            continue;
                        }

                        // ====================================
                        // pass 不需要處理的BPM狀態
                        // ====================================
                        List<BpmStatus> passBpmStatus = Lists.newArrayList(
                                BpmStatus.DELETE,
                                BpmStatus.APPROVED,
                                BpmStatus.INVALID);

                        if (passBpmStatus.contains(wsi.getStatus())) {
                            continue;
                        }

                        // ====================================
                        // pass 不需要處理的主單狀態
                        // ====================================
                        List<WCStatus> passStatus = Lists.newArrayList(
                                WCStatus.CLOSE,
                                WCStatus.CLOSE_STOP,
                                WCStatus.INVALID);

                        if (passStatus.contains(this.wcMasterManager.findWcStatusByWcSid(wsi.getWcSid()))) {
                            continue;
                        }

                        doSyncBpmData(wsi, null, null);
                    } catch (Exception e) {
                        log.warn("同步BPM資訊有誤-bpm_instance_id:" + execManagerSignInfo.getBpmId(), e);
                    }
                }
            }
        }

        log.debug("建立執行方簽核資訊同步資訊結束");
        updateCache();
        isRunning = false;
    }

    /**
     * 更新簽名記錄
     *
     * @param wcManagerSignInfo 執行單位簽核物件
     * @param tasks             水管圖資訊
     */
    public void updateSignData(
            WCExecManagerSignInfo wcManagerSignInfo,
            List<ProcessTaskBase> tasks,
            SignActionType signActionType,
            Integer signUserSid) {
        try {
            // 將舊資料修改成無效資料
            wcExecManagerSignInfoDetailRepository.updateStatus(
                    Activation.INACTIVE, wcManagerSignInfo.getSid());
            // 由於復原及退回在單據歷史紀錄是無法查詢的,所以必須更新狀態
            if (signActionType != null
                    && (signActionType.equals(SignActionType.RECOVERY)
                            || signActionType.equals(SignActionType.ROLLBACK))
                    && signUserSid != null) {
                wcExecManagerSignInfoDetailRepository.updateSignActionType(
                        signActionType, wcManagerSignInfo.getSid(), signUserSid);
            }
            if (tasks != null && !tasks.isEmpty()) {
                tasks.forEach(
                        item -> {
                            try {
                                if (item instanceof ProcessTaskHistory) {
                                    ProcessTaskHistory ph = (ProcessTaskHistory) item;
                                    User user = userManager.findById(ph.getExecutorID());
                                    WCExecManagerSignInfoDetail wd = new WCExecManagerSignInfoDetail();
                                    wd.setSign_dt(ph.getTaskEndTime());
                                    wd.setStatus(Activation.ACTIVE);
                                    wd.setUser_sid(user.getSid());
                                    wd.setWc_exec_manager_sign_info_sid(wcManagerSignInfo.getSid());
                                    wcExecManagerSignInfoDetailRepository.save(wd);
                                }
                            } catch (Exception e) {
                                log.warn("updateSignData ERROR", e);
                            }
                        });
            }
        } catch (Exception e) {
            log.warn("updateSignData ERROR", e);
        }
    }

    /**
     * 從BPM REST 更新資訊至Fusion DB
     *
     * @param wcManagerSignInfo 執行單位簽核物件
     * @return
     * @throws ProcessRestException
     */
    @SuppressWarnings("deprecation")
    public WCExecManagerSignInfo doSyncBpmData(
            WCExecManagerSignInfo wcManagerSignInfo, SignActionType signActionType, Integer signUserSid)
            throws ProcessRestException {
        List<ProcessTaskBase> tasks = Lists.newArrayList();

        try {
            if (!Strings.isNullOrEmpty(wcManagerSignInfo.getBpmId())) {
                tasks = taskClient.findSimulationChart(wcManagerSignInfo.getBpmId());
            }
        } catch (Exception ex) {
            if(ex!=null 
                    && ex instanceof ProcessRestException
                    && WkStringUtils.isEmpty(ex.getMessage())) {
                log.warn("findSimulationChart ERROR (ProcessRestException and null message) bpmID:[{}]" , wcManagerSignInfo.getBpmId());
            }else {
                log.warn("findSimulationChart ERROR", ex);
            }
        }

        updateSignData(wcManagerSignInfo, tasks, signActionType, signUserSid);

        wcManagerSignInfo.setBpmDefaultSignedName("");

        if (tasks != null && !tasks.isEmpty()) {
            wcManagerSignInfo.setBpmDefaultSignedName(
                    bpmManager.findTaskDefaultUserName(tasks.get(tasks.size() - 1)));
        }
        return wcExecManagerSignInfoRepository.save(wcManagerSignInfo);
    }

    /**
     * 等待 建立執行方簽核資訊Cache結束
     */
    private synchronized void processWait() {
        long startTime = System.currentTimeMillis();
        while (produceTime) {
            synchronized (this) {
                log.debug("正在執行建立執行方簽核資訊Cache...");
                if (!produceTime) {
                    break;
                }
                if (System.currentTimeMillis() - startTime > 15000) {
                    log.warn("建立執行方簽核資訊Cache,並未等到執行值,最多等待15秒");
                    break;
                }
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    log.warn("Wait Error", e);
                }
            }
        }
    }
}
