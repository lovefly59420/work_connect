/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.cy.work.connect.logic.helper.LogHelper;
import com.cy.work.connect.logic.helper.PermissionLogicForExecDep;
import com.cy.work.connect.logic.helper.WcSkypeAlertHelper;
import com.cy.work.connect.repository.WCExceDepRepository;
import com.cy.work.connect.repository.WCMasterRepository;
import com.cy.work.connect.vo.WCExceDep;
import com.cy.work.connect.vo.WCExecDepSetting;
import com.cy.work.connect.vo.WCExecManagerSignInfo;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.WCTrace;
import com.cy.work.connect.vo.converter.to.UsersTo;
import com.cy.work.connect.vo.enums.ExecDepCreateType;
import com.cy.work.connect.vo.enums.ExecDepType;
import com.cy.work.connect.vo.enums.WCExceDepStatus;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 執行單位Manager
 *
 * @author brain0925_liao
 */
@Slf4j
@Service
public class WCExecDepManager implements InitializingBean, Serializable {

    /**
    *
    */
    private static final long serialVersionUID = -8314303571927236261L;

    private static WCExecDepManager instance;

    public static WCExecDepManager getInstance() { return instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        WCExecDepManager.instance = this;
    }

    @Autowired
    private WCExceDepRepository wcExceDepRepository;
    @Autowired
    private WCTraceCustomLogicManager wcTraceCustomLogicManager;
    @Autowired
    private WCMasterCustomLogicManager wcMasterCustomLogicManager;
    @Autowired
    private WCMasterRepository wcMasterRepository;
    @Autowired
    private WCExecDepSettingManager execDepSettingManager;
    @Autowired
    private WCTagManager wcTagManager;
    @Autowired
    private WkUserCache wkUserCache;
    @Autowired
    private WCExecDepHistoryManager wcExecDepHistoryManager;
    @Autowired
    private WCAlertManager wcAlertManager;
    @Autowired
    private WkOrgCache orgManager;
    @Autowired
    private WCExecManagerSignInfoManager wcExecManagerSignInfoManager;
    @Autowired
    private LogHelper logHelper;
    @Autowired
    private WCTraceManager wcTraceManager;
    @Autowired
    private WCMasterManager wcMasterManager;

    /**
     * 以 sid查詢
     * 
     * @param sids
     * @return
     */
    public List<WCExceDep> findBySids(Set<String> sids) {
        return wcExceDepRepository.findBySidIn(sids);
    }

    public List<WCExceDep> findByDepSid(Integer dep_sid) {
        return wcExceDepRepository.findByDepSid(dep_sid);
    }

    public List<WCExceDep> findByExceUserSid(Integer execUserSid) {
        return wcExceDepRepository.findByExceUserSid(execUserSid);
    }

    /**
     * 取得執行單位List By 工作聯絡單Sid
     *
     * @param wcSid 工作聯絡單Sid
     * @return WCExceDep List
     */
    public List<WCExceDep> findActiveByWcSid(String wcSid) {
        return this.wcExceDepRepository.findByWcSidAndStatus(wcSid, Activation.ACTIVE);
    }

    /**
     * 以下列條件查詢
     * 
     * @param wcSid         工作聯絡單Sid
     * @param execDepStatus 執行單位狀態
     * @return WCExceDep List
     */
    List<WCExceDep> findByWcSidAndExecDepStatus(
            String wcSid,
            WCExceDepStatus execDepStatus) {
        return this.wcExceDepRepository.findByWcSidAndExecDepStatusAndStatus(wcSid, execDepStatus, Activation.ACTIVE);
    }

    /**
     * 以下列條件查詢
     * 
     * @param wcSids 主單 sid list
     * @return WCExceDep list
     */
    public List<WCExceDep> findByActiveWcSidIn(List<String> wcSids) {
        return this.wcExceDepRepository.findByWcSidInAndStatus(wcSids, Activation.ACTIVE);
    };

    /**
     * 取得執行單位 By 執行單位設定檔Sid
     *
     * @param execDepSettingSid 執行單位設定檔Sid
     * @return
     */
    public List<WCExceDep> getByExecDepSettingSid(String execDepSettingSid) {
        return wcExceDepRepository.findOneByExecDepSettingSid(execDepSettingSid);
    }

    /**
     * 取得執行單位 By 工作聯絡單Sid And 群組Seq
     *
     * @param wcSid    工作聯絡單Sid
     * @param groupSeq 群組Seq
     * @return
     */
    public List<WCExceDep> getWCExceDepByWcIDAndGroupSeq(String wcSid, Integer groupSeq) {
        return this.wcExceDepRepository.findByWcSidAndExecManagerSignInfoGroupSeqAndStatus(
                wcSid,
                groupSeq,
                Activation.ACTIVE);
    }

    /**
     * 取得執行單位 By 工作聯絡單Sid And 執行人員Sid
     *
     * @param wcSid   工作聯絡單Sid
     * @param userSid 執行人員Sid
     * @return
     */
    public List<WCExceDep> getWCExceDepByWcIDAndUserSid(String wcSid, Integer userSid) {
        return this.wcExceDepRepository.findByWcSidAndExecUserSidAndStatus(
                wcSid,
                userSid,
                Activation.ACTIVE);
    }

    /**
     * 更新執行單位(DB & Cache)
     *
     * @param wcExceDep 執行單位物件
     */
    public void updateWCExceDep(WCExceDep wcExceDep) {
        this.wcExceDepRepository.save(wcExceDep);
    }

    public void save(List<WCExceDep> wcExceDeps) {
        this.wcExceDepRepository.save(wcExceDeps);
    }

    /**
     * 更新執行單位狀態
     *
     * @param wcSid        簽呈Sid
     * @param wcExceDep    執行單位物件
     * @param afterStatus  執行單位狀態
     * @param loginUserSid 登入人員
     */
    public void updateWCExceDepStatus(
            String wcSid,
            WCExceDep wcExceDep,
            WCExceDepStatus afterStatus,
            Integer execUserSid,
            Integer loginUserSid) {
        // ====================================
        // update WCExceDep
        // ====================================
        wcExceDep.setUpdate_dt(new Date());
        wcExceDep.setExecDepStatus(afterStatus);
        wcExceDep.setUpdate_usr(loginUserSid);
        wcExceDep.setExecUserSid(execUserSid);
        this.updateWCExceDep(wcExceDep);

        // ====================================
        // 建立執行單位記錄
        // ====================================
        this.wcExecDepHistoryManager.createWCExecDepHistory(wcSid, wcExceDep.getSid(), execUserSid,
                loginUserSid);
        try {
            logHelper.addLogInfo(
                    wcSid,
                    loginUserSid,
                    "更新執行單位狀態["
                            + WkOrgUtils.findNameBySid(wcExceDep.getDep_sid())
                            + "] 執行狀態變為["
                            + afterStatus.getValue()
                            + "] 執行人員["
                            + WkUserUtils.findNameBySid(execUserSid)
                            + "]");
        } catch (Exception e) {
            log.warn("updateWCExceDepStatus ERROR", e);
        }
    }

    /**
     * 執行確認完成
     *
     * @param wcSid        工作聯絡單Sid
     * @param tagMaps      該人可領單的執行單位及執行項目 Map
     * @param loginUserSid 執行確認完成人員Sid
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void saveExecFinish(
            String wcSid, Integer loginUserSid, String content) throws UserMessageException {

        // ====================================
        // 檢查
        // ====================================
        if (WkStringUtils.isEmpty(content)) {
            throw new UserMessageException("請輸入執行完成內容", InfomationLevel.WARN);
        }

        // ====================================
        // 取得可進行『執行完成』的執行單位檔
        // ====================================
        List<WCExceDep> canExecFinishExecDeps = PermissionLogicForExecDep.getInstance().findCanExecFinish(
                wcSid, loginUserSid);

        if (WkStringUtils.isEmpty(canExecFinishExecDeps)) {
            throw new UserMessageException(WkMessage.NEED_RELOAD + "(找不到可以執行完成的項目)", InfomationLevel.WARN);
        }

        // ====================================
        // 更新執行單位檔
        // ====================================
        // 更新欄位
        for (WCExceDep currWcExceDep : canExecFinishExecDeps) {
            currWcExceDep.setUpdate_dt(new Date());
            currWcExceDep.setUpdate_usr(loginUserSid);
            currWcExceDep.setExecDepStatus(WCExceDepStatus.FINISH);

            logHelper.addLogInfo(
                    wcSid,
                    loginUserSid,
                    "更新執行單位狀態["
                            + WkOrgUtils.findNameBySid(currWcExceDep.getDep_sid())
                            + "] 執行狀態變為["
                            + currWcExceDep.getExecDepStatus()
                            + "] 執行人員["
                            + WkUserUtils.findNameBySid(loginUserSid)
                            + "]");

        }
        // update
        this.wcExceDepRepository.save(canExecFinishExecDeps);

        // ====================================
        // 新增執行單位記錄
        // ====================================
        this.wcExecDepHistoryManager.createWCExecDepHistory(canExecFinishExecDeps, loginUserSid);

        // ====================================
        // 新增追蹤
        // ====================================
        WCMaster wcMaster = wcMasterRepository.findBySid(wcSid);
        WCTrace wcTrace = wcTraceCustomLogicManager.getTraceExecFinish(
                wcMaster.getSid(),
                wcMaster.getWc_no(),
                content,
                canExecFinishExecDeps);

        this.wcTraceManager.create(wcTrace, loginUserSid);

        // ====================================
        // 通知申請人
        // ====================================
        Set<Integer> execDepSids = WkCommonUtils.safeStream(canExecFinishExecDeps)
                .map(wcExecDep -> wcExecDep.getDep_sid())
                .collect(Collectors.toSet());

        this.wcAlertManager.createAlertsByExecFinish(
                wcMaster.getSid(),
                wcMaster.getWc_no(),
                Lists.newArrayList(wcMaster.getCreate_usr()),
                WkOrgUtils.findNameBySid(execDepSids, "、"),
                wcMaster.getTheme(),
                loginUserSid,
                wcTrace.getSid());

        // ====================================
        // 推進主單流程
        // ====================================
        this.changeToExecFinish(wcSid, loginUserSid);
    }

    public void changeToExecFinish(String wcSid, Integer loginUserSid) {
        try {
            List<WCExceDep> wcExecDeps = this.findActiveByWcSid(wcSid);
            if (wcExecDeps == null || wcExecDeps.isEmpty()) {
                return;
            }

            boolean isFinish = true;
            for (WCExceDep execDep : wcExecDeps) {
                if (execDep.getExecDepStatus() != null
                        && !execDep.getExecDepStatus().isFinish()) {
                    isFinish = false;
                    break;
                }
            }

            if (isFinish) {
                wcMasterCustomLogicManager.updateToExecFinish(wcSid, loginUserSid);
            } else {
                WCMaster wcMaster = wcMasterRepository.findBySid(wcSid);
                if (wcMaster.getWc_status().equals(WCStatus.EXEC_FINISH)) {
                    wcMasterCustomLogicManager.updateToExec(wcSid, loginUserSid);
                }
            }
        } catch (Exception e) {
            log.warn("changeToExecFinish ERROR", e);
        }
    }

    /**
     * 是否全數執行單位執行完畢
     *
     * @param wcSid 工作聯絡單Sid
     * @return
     */
    public boolean isAllExecDepFinish(String wcSid) {

        List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
        // 若無執行單位,代表該單據需求單位流程尚未簽核完畢,故認定執行單位尚未執行完畢
        if (WkStringUtils.isEmpty(wcExceDeps)) {
            return false;
        }

        // 過濾出不可結案的 (not Finish)
        Optional<WCExceDep> optional = WkCommonUtils.safeStream(wcExceDeps)
                .filter(wcExceDep -> wcExceDep.getExecDepStatus() == null || !wcExceDep.getExecDepStatus().isFinish())
                .findAny();

        return !optional.isPresent();
    }

    /**
     * 執行結案動作
     *
     * @param wcSid        工作聯絡單Sid
     * @param wcNo         工作聯絡單單號
     * @param loginUserSid 執行者Sid
     */
    public void doClose(String wcSid, String wcNo, Integer loginUserSid) {
        this.doExecClose(wcSid, wcNo, loginUserSid);
    }

    /**
     * 執行結案動作
     *
     * @param isFinish       是否執行單位已完成
     * @param hasStop        是否執行單位有被終止
     * @param wcSid          工作聯絡單Sid
     * @param wcNo           工作聯絡單單號
     * @param loginUserSid   登入者Sid
     * @param activeExecDeps 有效的執行單位List
     */
    private void doExecClose(
            boolean isFinish,
            boolean hasStop,
            String wcSid,
            String wcNo,
            Integer loginUserSid,
            List<WCExceDep> activeExecDeps) {
        if (isFinish) {
            try {
                // 結案一併將狀態更新成結案
                for (WCExceDep item : activeExecDeps) {
                    if (item.getExecDepStatus().equals(WCExceDepStatus.FINISH)) {
                        this.updateWCExceDepStatus(
                                wcSid, item, WCExceDepStatus.CLOSE, item.getExecUserSid(),
                                loginUserSid);
                    }
                }
            } catch (Exception e) {
                log.warn("checkDepStatus ERROR", e);
            }

            WCTrace wcTrace = null;
            if (hasStop) {
                wcMasterCustomLogicManager.updateToCloseStop(wcSid, loginUserSid);
                wcTrace = wcTraceCustomLogicManager.getWCTrace_CLOSE_STOP(loginUserSid, wcSid, wcNo);
            } else {
                wcMasterCustomLogicManager.updateToClose(wcSid, loginUserSid);
                wcTrace = wcTraceCustomLogicManager.getWCTrace_CLOSE(loginUserSid, wcSid, wcNo);
            }
            this.wcTraceManager.create(wcTrace, loginUserSid);
        }
    }

    /**
     * 執行結案動作
     *
     * @param wcSid        工作聯絡單Sid
     * @param wcNo         工作聯絡單單號
     * @param loginUserSid 登入者Sid
     */
    public void doExecClose(String wcSid, String wcNo, Integer loginUserSid) {
        try {
            List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
            List<WCExceDep> activeExecDeps = wcExceDeps.stream()
                    .filter(each -> each.getStatus().equals(Activation.ACTIVE))
                    .collect(Collectors.toList());
            // 若無執行單位,代表該單據需求單位流程尚未簽核完畢,故認定執行單位尚未執行完畢
            if (activeExecDeps == null || activeExecDeps.isEmpty()) {
                return;
            }
            boolean isFinish = true;
            boolean hasStop = false;
            for (WCExceDep item : activeExecDeps) {
                if (!item.getExecDepStatus().equals(WCExceDepStatus.FINISH)
                        && !item.getExecDepStatus().equals(WCExceDepStatus.STOP)) {
                    isFinish = false;
                }
                if (item.getExecDepStatus().equals(WCExceDepStatus.STOP)) {
                    hasStop = true;
                }
            }
            this.doExecClose(isFinish, hasStop, wcSid, wcNo, loginUserSid, activeExecDeps);
        } catch (Exception e) {
            log.warn("checkDepStatus ERROR", e);
        }
    }

    /**
     * 執行轉指派
     *
     * @param wcSid         工作聯絡單Sid
     * @param selTagSids    可進行領單執行項目Sid
     * @param wcExceDeps    該工作聯絡單所有執行單位
     * @param selExecDepSid 被轉指派的執行單位
     * @param tagSids       有被轉指派的執行項目
     * @param sb            追蹤顯示字串
     * @param selUserSid    欲轉指派的人員Sid
     * @param toUser        欲轉指派的人員
     * @param loginUserSid  執行人員Sid
     */
    private void doTransPerson(
            String wcSid,
            List<String> selTagSids,
            List<WCExceDep> wcExceDeps,
            Integer selExecDepSid,
            List<String> tagSids,
            StringBuilder sb,
            Integer selUserSid,
            User toUser,
            Integer loginUserSid) {
        List<WCExceDep> waitSendDeps = wcExceDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getDep_sid().equals(selExecDepSid))
                .collect(Collectors.toList());
        waitSendDeps.forEach(
                item -> {
                    // 若是無執行單位設定檔,代表是加派,有代表是系統產生
                    if (!Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
                        WCExecDepSetting execDepSetting = execDepSettingManager.findBySidFromCache(item.getExecDepSettingSid());
                        if (!selTagSids.contains(execDepSetting.getWc_tag_sid())) {
                            return;
                        }
                        if (!Strings.isNullOrEmpty(sb.toString())) {
                            sb.append("\n");
                        }
                        WCTag wcTag = wcTagManager.getWCTagBySid(execDepSetting.getWc_tag_sid());
                        User oldUser = wkUserCache.findBySid(item.getExecUserSid());
                        tagSids.add(execDepSetting.getWc_tag_sid());
                        sb.append(
                                "類別[" + wcTag.getTagName() + "]" + oldUser.getName() + "變更為"
                                        + toUser.getName());
                    } else {
                        if (!Strings.isNullOrEmpty(sb.toString())) {
                            sb.append("\n");
                        }
                        Org execDep = orgManager.findBySid(item.getDep_sid());
                        User oldUser = wkUserCache.findBySid(item.getExecUserSid());
                        sb.append(
                                "執行單位[" + execDep.getName() + "]" + oldUser.getName() + "變更為"
                                        + toUser.getName());
                    }
                    this.updateWCExceDepStatus(
                            wcSid, item, WCExceDepStatus.PROCEDD, selUserSid, loginUserSid);
                });
    }

    /**
     * 執行轉指派
     *
     * @param wcSid        工作聯絡單Sid
     * @param tagMaps      該人可轉指派的執行單位及執行項目 Map
     * @param selUserSid   欲轉指派的人員Sid
     * @param loginUserSid 執行人員Sid
     */
    public void saveTransPersonExecDep(
            String wcSid, Map<Integer, List<WCTag>> tagMaps, Integer selUserSid, Integer loginUserSid) {
        WCMaster wcMaster = wcMasterRepository.findOne(wcSid);
        List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
        StringBuilder sb = new StringBuilder();
        tagMaps
                .keySet()
                .forEach(
                        selExecDepSid -> {
                            // List<WCTrace> traces = Lists.newArrayList();
                            List<String> tagSids = Lists.newArrayList();
                            User toUser = wkUserCache.findBySid(selUserSid);
                            List<String> selTagSids = Lists.newArrayList();
                            tagMaps
                                    .get(selExecDepSid)
                                    .forEach(
                                            item -> {
                                                selTagSids.add(item.getSid());
                                            });
                            this.doTransPerson(
                                    wcSid,
                                    selTagSids,
                                    wcExceDeps,
                                    selExecDepSid,
                                    tagSids,
                                    sb,
                                    selUserSid,
                                    toUser,
                                    loginUserSid);
                        });
        if (!Strings.isNullOrEmpty(sb.toString())) {
            WCTrace wcTrace = wcTraceCustomLogicManager.getTraceTransWork(wcSid, wcMaster.getWc_no(),
                    sb.toString());
            this.wcTraceManager.create(wcTrace, loginUserSid);
        }
        if (wcMaster.getWc_status().equals(WCStatus.EXEC_FINISH)) {
            wcMasterCustomLogicManager.updateToExec(wcSid, loginUserSid);
        }
    }

    /**
     * 執行分派
     *
     * @param wcSid         工作聯絡單Sid
     * @param selTagSids    可進行分派執行項目Sid
     * @param wcExceDeps    該工作聯絡單所有執行單位
     * @param selExecDepSid 被分派的執行單位
     * @param selUserSid    欲分派的人員Sid
     * @param depSids       有被分派的執行單位SID
     * @param tagSids       有被分派的執行項目Sid
     * @param loginUserSid  執行者Sid
     */
    private void doSendPerson(
            String wcSid,
            List<String> selTagSids,
            List<WCExceDep> wcExceDeps,
            Integer selExecDepSid,
            Integer selUserSid,
            List<Integer> depSids,
            List<String> tagSids,
            Integer loginUserSid) {
        List<WCExceDep> waitSendDeps = wcExceDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getDep_sid().equals(selExecDepSid))
                .collect(Collectors.toList());
        waitSendDeps.forEach(
                item -> {
                    // 若是無執行單位設定檔,代表是加派,有代表是系統產生
                    if (!Strings.isNullOrEmpty(item.getExecDepSettingSid())) {
                        WCExecDepSetting execDepSetting = execDepSettingManager.findBySidFromCache(item.getExecDepSettingSid());
                        if (!selTagSids.contains(execDepSetting.getWc_tag_sid())) {
                            return;
                        }
                        tagSids.add(execDepSetting.getWc_tag_sid());
                    } else {
                        depSids.add(item.getDep_sid());
                    }
                    this.updateWCExceDepStatus(
                            wcSid, item, WCExceDepStatus.PROCEDD, selUserSid, loginUserSid);
                });
    }

    /**
     * 執行分派
     *
     * @param wcSid        工作聯絡單Sid
     * @param tagMaps      該人可轉指派的執行單位及執行項目 Map
     * @param selUserSid   欲分派的人員Sid
     * @param loginUserSid 執行者Sid
     */
    public void saveSendPersonExecDep(
            String wcSid, Map<Integer, List<WCTag>> tagMaps, Integer selUserSid, Integer loginUserSid) {
        WCMaster wcMaster = this.wcMasterManager.findBySid(wcSid);
        List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
        List<String> tagSids = Lists.newArrayList();
        List<Integer> depSids = Lists.newArrayList();
        tagMaps
                .keySet()
                .forEach(
                        selExecDepSid -> {
                            List<String> selTagSids = Lists.newArrayList();
                            tagMaps
                                    .get(selExecDepSid)
                                    .forEach(
                                            item -> {
                                                selTagSids.add(item.getSid());
                                            });
                            this.doSendPerson(
                                    wcSid,
                                    selTagSids,
                                    wcExceDeps,
                                    selExecDepSid,
                                    selUserSid,
                                    depSids,
                                    tagSids,
                                    loginUserSid);
                        });
        // 若工作聯絡單狀態為核准,需改為執行中
        if (wcMaster.getWc_status().equals(WCStatus.APPROVED)) {
            wcMasterCustomLogicManager.updateToExec(wcMaster.getSid(), loginUserSid);
        }
        if (!tagSids.isEmpty() || !depSids.isEmpty()) {
            WCTrace wcTrace = wcTraceCustomLogicManager.getTraceSendWork(
                    wcSid, wcMaster.getWc_no(), loginUserSid, selUserSid, tagSids, depSids);
            this.wcTraceManager.create(wcTrace, loginUserSid);
        }
    }

    /**
     * 建立手動加派的執行單位
     *
     * @param wcMaster      工作聯絡單物件
     * @param addExecDepSid 欲新增的加派部門sid
     * @param loginUserSid  加派人員Sid
     */
    private void createCustomerExecDep(
            WCMaster wcMaster, Integer addExecDepSid, Integer loginUserSid) {

        // ====================================
        // entity
        // ====================================
        // WORKCOMMU-559【加派單位】加派單位執行流程調整，一律改為不簽核、需派工模式
        WCExceDep wcExceDep = new WCExceDep();
        wcExceDep.setStatus(Activation.ACTIVE);
        wcExceDep.setCreate_usr(loginUserSid);
        wcExceDep.setCreate_dt(new Date());

        wcExceDep.setWcSid(wcMaster.getSid());
        wcExceDep.setDep_sid(addExecDepSid);
        wcExceDep.setExecDepType(ExecDepType.NO_SIGN);
        wcExceDep.setExecDepStatus(WCExceDepStatus.WAITSEND);
        wcExceDep.setExecDepCreateType(ExecDepCreateType.CUSTOMER);

        wcExceDep.setExecManagerSignInfoGroupSeq(null);
        wcExceDep.setExecManagerSignInfoSid("");

        // ====================================
        // insert
        // ====================================
        this.updateWCExceDep(wcExceDep);
    }

    /**
     * 刪除執行單位
     *
     * @param item 執行單位物件
     * @param user 執行人員
     * @throws UserMessageException
     */
    private void deleteExecDep(WCExceDep item, Integer updateUserSid) throws UserMessageException {

        // ====================================
        // user
        // ====================================
        User updateUser = WkUserCache.getInstance().findBySid(updateUserSid);
        if (updateUser == null) {
            throw new UserMessageException("找不到異動者資料[" + updateUserSid + "]", InfomationLevel.ERROR);
        }

        // ====================================
        // 作廢簽核流程
        // ====================================
        if (WkStringUtils.notEmpty(item.getExecManagerSignInfoSid())) {
            try {
                this.wcExecManagerSignInfoManager.deleteWCExecManagerSignInfo(
                        item.getExecManagerSignInfoSid(), updateUser);
            } catch (Exception e) {
                // BPM作廢失敗..就算了
                log.error("作廢簽核流程失敗![" + e.getMessage() + "]", e);
            }
        }

        // ====================================
        // 異動為停用
        // ====================================
        item.setStatus(Activation.INACTIVE);
        item.setUpdate_dt(new Date());
        item.setUpdate_usr(updateUserSid);
        this.updateWCExceDep(item);
    }

    /**
     * 儲存自訂執行單位
     *
     * @param wcSid        工作聯絡單Sid
     * @param loginUserSid 登入者Sid
     * @param needToRemove 移除執行單位List
     * @param needToAddOrg 新增執行單位List
     * @throws UserMessageException
     */
    @Transactional(rollbackForClassName = { "Exception" })
    public void saveCustomerExceDep(
            String wcSid,
            Integer loginUserSid,
            Collection<Integer> addExecDepSids,
            List<WCExceDep> removeExecDeps) throws UserMessageException {

        // 沒有需要處理的資料
        if (WkStringUtils.isEmpty(removeExecDeps)
                && WkStringUtils.isEmpty(addExecDepSids)) {
            log.debug("未異動加派單位");
            return;
        }

        // ====================================
        // 主檔
        // ====================================
        WCMaster wcMaster = wcMasterRepository.findBySid(wcSid);
        if (wcMaster == null) {
            String userMessage = WkMessage.NEED_RELOAD + "(主檔找不到)";
            log.warn(userMessage + "[{}]", wcSid);
            throw new UserMessageException(userMessage, InfomationLevel.ERROR);
        }

        // ====================================
        // 刪除(停用)加派的執行單位
        // ====================================
        if (WkStringUtils.notEmpty(removeExecDeps)) {
            for (WCExceDep wcExceDep : removeExecDeps) {
                this.deleteExecDep(wcExceDep, loginUserSid);
            }
        }

        // ====================================
        // 新增加派的執行單位
        // ====================================
        for (Integer addExecDepSid : addExecDepSids) {
            this.createCustomerExecDep(wcMaster, addExecDepSid, loginUserSid);
        }

        // ====================================
        // 寫追蹤
        // ====================================
        Set<Integer> removeDepSids = WkCommonUtils.safeStream(removeExecDeps)
                .map(WCExceDep::getDep_sid)
                .collect(Collectors.toSet());

        WCTrace wcTrace = wcTraceCustomLogicManager.getWCTrace_MODIFY_CUSTOMER_EXCEDEP(
                wcMaster.getSid(),
                wcMaster.getWc_no(),
                loginUserSid,
                addExecDepSids,
                removeDepSids);

        this.wcTraceManager.create(wcTrace, loginUserSid);

        // ====================================
        // 異動主單狀態
        // ====================================
        this.changeToExecFinish(wcSid, loginUserSid);
    }

    public List<WCExceDep> getMySelfExecDpe(String wcSid, User loginUser) {
        List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
        List<WCExceDep> mySelfExecDeps = wcExceDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                                && each.getExecDepType().equals(ExecDepType.NO_SIGN)
                                && each.getExecUserSid() != null
                                && each.getExecUserSid().equals(loginUser.getSid()))
                .collect(Collectors.toList());
        return mySelfExecDeps;
    }

    /**
     * 判斷領單者是否可顯示退回按鈕
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者
     * @return
     */
    public boolean isShowExecDepRollBackPermisison(String wcSid, User loginUser) {
        boolean result = false;
        try {
            List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
            // if 領單 , 領單人＝自己 and 沒有其他人領單,或者沒有其他人簽核過 , 退回按鈕enable
            List<WCExceDep> mySelfExecDeps = this.getMySelfExecDpe(wcSid, loginUser);
            if (mySelfExecDeps != null && !mySelfExecDeps.isEmpty()) {
                List<WCExceDep> otherExecDeps = wcExceDeps.stream()
                        .filter(
                                each -> each.getStatus().equals(Activation.ACTIVE)
                                        // 確認是否有其他人已領單
                                        && (( // 不需簽核單據
                                        (each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                                                || each.getExecDepStatus().equals(WCExceDepStatus.STOP)
                                                || each.getExecDepStatus().equals(WCExceDepStatus.FINISH)
                                                || each.getExecDepStatus().equals(WCExceDepStatus.CLOSE))
                                                && each.getExecDepType().equals(ExecDepType.NO_SIGN)
                                                && each.getExecUserSid() != null
                                                && !each.getExecUserSid().equals(loginUser.getSid()))
                                                // 確認是否已有其他主管已簽核,待派單,或已被派單
                                                || ( // 需簽核單據
                                                (each.getExecDepStatus().equals(WCExceDepStatus.WAITSEND)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.STOP)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.FINISH)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.CLOSE))
                                                        && each.getExecDepType().equals(ExecDepType.NEED_SIGN))))
                        .collect(Collectors.toList());

                // 1.其他單位都沒人執行
                // 2.會簽都還沒有簽
                // 以上兩者成立, 才成立
                if (otherExecDeps == null || otherExecDeps.isEmpty()) {
                    if (!wcExecManagerSignInfoManager.isCheckAnyContinueSign(wcSid)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            log.warn("isShowExecDepRollBackPermisison ERROR", e);
        }
        return result;
    }

    /**
     * 判斷領單者是否可顯示不執行按鈕
     *
     * @param wcSid     工作聯絡單Sid
     * @param loginUser 登入者
     * @return
     */
    public boolean isShowExecDepStopPermisison(String wcSid, User loginUser) {

        try {
            List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);

            // if 領單 , 領單人＝自己 and 沒有其他人領單,或者沒有其他人簽核過 , 退回按鈕enable
            List<WCExceDep> mySelfExecDeps = this.getMySelfExecDpe(wcSid, loginUser);
            if (mySelfExecDeps != null && !mySelfExecDeps.isEmpty()) {
                List<WCExceDep> otherExecDeps = wcExceDeps.stream()
                        .filter(
                                each -> each.getStatus().equals(Activation.ACTIVE)
                                        // 確認是否有其他人已領單
                                        && (( // 不需簽核單據
                                        (each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                                                || each.getExecDepStatus().equals(WCExceDepStatus.STOP)
                                                || each.getExecDepStatus().equals(WCExceDepStatus.FINISH)
                                                || each.getExecDepStatus().equals(WCExceDepStatus.CLOSE))
                                                && each.getExecDepType().equals(ExecDepType.NO_SIGN)
                                                && each.getExecUserSid() != null
                                                && !each.getExecUserSid().equals(loginUser.getSid()))
                                                // 確認是否已有其他主管已簽核,待派單,或已被派單
                                                || ( // 需簽核單據
                                                (each.getExecDepStatus().equals(WCExceDepStatus.WAITSEND)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.STOP)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.FINISH)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.PROCEDD)
                                                        || each.getExecDepStatus().equals(WCExceDepStatus.CLOSE))
                                                        && each.getExecDepType().equals(ExecDepType.NEED_SIGN))))
                        .collect(Collectors.toList());

                // 1.其他單位有人執行, 就可以按不執行
                if (WkStringUtils.notEmpty(otherExecDeps)) {
                    return true;
                }

                // 2.其他單位都沒人執行的狀況下, 有人會簽, 才可以按不執行
                if (WkStringUtils.isEmpty(otherExecDeps)) {
                    if (wcExecManagerSignInfoManager.isCheckAnyContinueSign(wcSid)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            log.warn("isShowExecDepStopPermisison ERROR", e);
        }
        return false;
    }

    /**
     * 是否已有執行單位正在執行
     *
     * @param wcSid 工作聯絡單SID
     * @return
     */
    public boolean isAnyExecDepWorking(String wcSid) {
        List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wcSid);
        List<WCExceDep> workingDeps = wcExceDeps.stream()
                .filter(
                        each -> Activation.ACTIVE.equals(each.getStatus())
                                && !(WCExceDepStatus.WAITRECEIVCE.equals(each.getExecDepStatus())
                                        || WCExceDepStatus.APPROVING.equals(each.getExecDepStatus())
                                        || WCExceDepStatus.WAITSEND.equals(each.getExecDepStatus())))
                .collect(Collectors.toList());
        return workingDeps != null && !workingDeps.isEmpty();
    }

    /**
     * 刪除該工作聯絡單的執行單位
     *
     * @param wc_ID     工作聯絡單Sid
     * @param loginUser 執行者Sid
     */
    public void stopExecDep(String wc_ID, User loginUser) {
        List<WCExceDep> wcExceDeps = this.findActiveByWcSid(wc_ID);
        for (WCExceDep wcExceDep : wcExceDeps) {
            wcExceDep.setExecDepStatus(WCExceDepStatus.STOP);
            wcExceDep.setStatus(Activation.INACTIVE);
            this.updateWCExceDep(wcExceDep);
        }
    }

    /**
     * 建立執行單位物件(系統預設)
     *
     * @param wcSid                       工作聯絡單Sid
     * @param execManagerSignInfoGroupSeq 簽核群組Seq
     * @param execManagerSignInfoSid      簽核Sid
     * @param execDepSettingSid           執行單位設定檔Sid
     * @param depSid                      執行單位 Sid
     * @param execDepType                 簽核類型
     * @param wcExceDepStatus             執行單位狀態
     * @param receviceUser                僅領單人員可閱名單
     */
    public void createExceDep(
            String wcSid,
            Integer execManagerSignInfoGroupSeq,
            String execManagerSignInfoSid,
            String execDepSettingSid,
            Integer depSid,
            ExecDepType execDepType,
            WCExceDepStatus wcExceDepStatus,
            UsersTo receviceUser) {
        WCExceDep wcExceDep = new WCExceDep();
        wcExceDep.setExecManagerSignInfoGroupSeq(execManagerSignInfoGroupSeq);
        wcExceDep.setExecManagerSignInfoSid(execManagerSignInfoSid);
        wcExceDep.setWcSid(wcSid);
        wcExceDep.setExecDepType(execDepType);
        wcExceDep.setExecDepStatus(wcExceDepStatus);
        wcExceDep.setStatus(Activation.ACTIVE);
        wcExceDep.setCreate_usr(null);
        wcExceDep.setExecDepSettingSid(execDepSettingSid);
        wcExceDep.setCreate_dt(new Date());
        wcExceDep.setDep_sid(depSid);
        wcExceDep.setExecUserSid(null);
        wcExceDep.setReceviceUser(receviceUser);
        this.updateWCExceDep(wcExceDep);
    }

    /**
     * 該簽核物件對應的執行單位是否有正在執行中
     *
     * @param wcSid                 工作聯絡單Sid
     * @param wcExecManagerSignInfo 執行單位簽核物件
     * @return
     */
    public boolean isExecWorking(String wcSid, WCExecManagerSignInfo wcExecManagerSignInfo) {
        List<WCExceDep> wcExecpDeps = this.findActiveByWcSid(wcSid);
        if (wcExecpDeps == null) {
            return false;
        }
        List<WCExceDep> workingExecDeps = wcExecpDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getExecManagerSignInfoGroupSeq() != null
                                && each.getExecManagerSignInfoGroupSeq()
                                        .equals(wcExecManagerSignInfo.getGroupSeq())
                                && !each.getExecDepStatus().equals(WCExceDepStatus.APPROVING)
                                && !each.getExecDepStatus().equals(WCExceDepStatus.WAITSEND)
                                && !each.getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE))
                .collect(Collectors.toList());

        return workingExecDeps != null && !workingExecDeps.isEmpty();
    }

    /**
     * 將執行單位狀態改為簽核中
     *
     * @param wcSid                 工作聯絡單Sid
     * @param wcExecManagerSignInfo 執行單位簽核物件
     */
    public void updateExecWorkingApproving(
            String wcSid,
            WCExecManagerSignInfo wcExecManagerSignInfo) {

        List<WCExceDep> wcExecpDeps = this.findActiveByWcSid(wcSid);
        if (WkStringUtils.isEmpty(wcExecpDeps)) {
            return;
        }
        List<WCExceDep> workingExecDeps = wcExecpDeps.stream()
                .filter(
                        each -> each.getStatus().equals(Activation.ACTIVE)
                                && each.getExecManagerSignInfoGroupSeq() != null
                                && each.getExecManagerSignInfoGroupSeq()
                                        .equals(wcExecManagerSignInfo.getGroupSeq())
                                && (each.getExecDepStatus().equals(WCExceDepStatus.WAITSEND)
                                        || each.getExecDepStatus().equals(WCExceDepStatus.WAITRECEIVCE)))
                .collect(Collectors.toList());

        if (workingExecDeps == null || workingExecDeps.isEmpty()) {
            return;
        }
        workingExecDeps.forEach(
                exceDep -> {
                    exceDep.setExecDepStatus(WCExceDepStatus.APPROVING);
                    this.updateWCExceDep(exceDep);
                });
    }

    /**
     * 取得簽核中執行單位
     *
     * @param wcSid                  工作聯絡單Sid
     * @param execManagerSignInfoSid 執行單位簽核物件Sid
     * @return
     */
    public List<WCExceDep> getApprovingExecDeps(String wcSid, String execManagerSignInfoSid) {
        List<WCExceDep> wcExecpDeps = this.findActiveByWcSid(wcSid);

        List<WCExceDep> approvingExecDeps = wcExecpDeps.stream()
                // 執行單位狀態為APPROVING
                .filter(wcExceDep -> wcExceDep.getExecDepStatus().equals(WCExceDepStatus.APPROVING))
                // 對應流程的執行單位
                .filter(wcExceDep -> WkCommonUtils.compareByStr(execManagerSignInfoSid, wcExceDep.getExecManagerSignInfoSid()))
                .collect(Collectors.toList());

        return approvingExecDeps;
    }

    /**
     * 根據類型進行更新執行單位執行狀態
     *
     * @param wcExceDep 執行單位
     */
    public void updateWaitWorkStatus(WCExceDep wcExceDep) {

        // ====================================
        // 為一般執行單位
        // ====================================
        if (ExecDepCreateType.SYSTEM.equals(wcExceDep.getExecDepCreateType())) {

            // 取得設定檔
            WCExecDepSetting execDepSetting = execDepSettingManager.findBySidFromCache(wcExceDep.getExecDepSettingSid());
            // 為派工模式
            if (execDepSetting != null && execDepSetting.isNeedAssigned()) {
                wcExceDep.setExecDepStatus(WCExceDepStatus.WAITSEND);
            }
            // 為領單模式
            else {
                wcExceDep.setExecDepStatus(WCExceDepStatus.WAITRECEIVCE);
            }
        }

        // ====================================
        // 為加派單位
        // ====================================
        // WORKCOMMU-559 【加派單位】加派單位執行流程調整，一律改為不簽核、需派工模式
        if (ExecDepCreateType.CUSTOMER.equals(wcExceDep.getExecDepCreateType())) {
            wcExceDep.setExecDepStatus(WCExceDepStatus.WAITSEND);
        }

        // ====================================
        // update
        // ====================================
        // 防呆
        if (wcExceDep.getExecDepCreateType() == null) {
            String message = "無法判斷『執行單位』為領單或派工!";
            log.error(message);
            log.error(WkJsonUtils.getInstance().toPettyJson(wcExceDep));
            WcSkypeAlertHelper.getInstance().sendSkypeAlert(message);
            throw new SystemDevelopException(message, InfomationLevel.ERROR);
        }

        // update
        this.updateWCExceDep(wcExceDep);
    }

    /**
     * @param wcSid
     * @return
     */
    public Set<Integer> prepareReceiveExecDepSidForExecRollBack(String wcSid) {
        // ====================================
        // 查詢執行單位檔
        // ====================================
        List<WCExceDep> activeExecpDeps = this.findActiveByWcSid(wcSid);

        if (WkStringUtils.isEmpty(activeExecpDeps)) {
            return Sets.newHashSet();
        }

        // ====================================
        // 收集已領單單位
        // ====================================

        Set<Integer> receiveExecDepSids = Sets.newHashSet();

        for (WCExceDep exceDep : activeExecpDeps) {

            // 需為非簽核模式 (簽核者退回，執行單位不用看到)
            if (ExecDepType.NEED_SIGN.equals(exceDep.getExecDepType())) {
                continue;
            }

            // 需為執行中單位 (已領單、已分派)
            // 可退件邏輯下，應該只會找到一個單位 (但執行單位檔可能會有多筆 [不同小類])
            if (!WCExceDepStatus.PROCEDD.equals(exceDep.getExecDepStatus())) {
                continue;
            }

            // 排除僅領單人員可閱 (此狀況下只有領單的退件者可在退件一覽表查的到, 名單中其他人員不行)
            if (WkStringUtils.notEmpty(exceDep.getReceviceUserSids())) {
                continue;
            }

            // 防呆-加派不算
            // 此狀況應該不會有, 可退回狀況下，不可能有加派的執行單位 只是順便擋一下，否則對不回設定檔會出錯
            if (!ExecDepCreateType.SYSTEM.equals(exceDep.getExecDepCreateType())) {
                log.error("邏輯有誤：加派單位已產生，但是被執行方退件？wcsid:[{}]", wcSid);
                continue;
            }

            // 防呆
            if (WkStringUtils.isEmpty(exceDep.getExecDepSettingSid())) {
                log.error("邏輯有誤：非加派的 wc_exec_dep 的 wc_exec_dep_setting_sid 不應為空！sid:[{}]", exceDep.getSid());
                continue;
            }

            // 查詢對應設定檔
            WCExecDepSetting execDepSetting = this.execDepSettingManager.findBySidWithoutCache(
                    exceDep.getExecDepSettingSid());

            if (execDepSetting == null) {
                log.error("邏輯有誤：找不到對應的 wc_exec_dep_setting !!sid:[{}]", exceDep.getSid());
            }

            // 排除派工模式
            if (execDepSetting.isNeedAssigned()) {
                continue;
            }

            // 收集執行單位 sid (不重複)
            receiveExecDepSids.add(exceDep.getDep_sid());
        }

        return receiveExecDepSids;
    }

}
