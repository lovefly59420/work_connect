/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.work.connect.logic.manager.BpmManager;
import com.cy.work.connect.logic.vo.SingleManagerSignInfo;
import com.cy.work.connect.vo.WCManagerSignInfo;
import com.cy.work.connect.vo.enums.BpmStatus;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 需求單位Helper
 *
 * @author brain0925_liao
 */
@Component
public class WCManagerSignInfoHelper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2595001409536215883L;

    @Autowired
    private BpmManager bpmManager;

    /**
     * 將需求單位簽核DB物件 轉換成 自訂物件
     *
     * @param wCManagerSignInfos 需求單位簽核物件
     * @return
     */
    public List<SingleManagerSignInfo> settingSingleManagerSignInfo(
        List<WCManagerSignInfo> wCManagerSignInfos) {
        if (wCManagerSignInfos == null) {
            return Lists.newArrayList();
        }
        // 以序號排序
        Collections.sort(
            wCManagerSignInfos,
            new Comparator<WCManagerSignInfo>() {
                @Override
                public int compare(WCManagerSignInfo o1, WCManagerSignInfo o2) {
                    return o1.getSeq().compareTo(o2.getSeq());
                }
            });
        // 過濾
        List<SingleManagerSignInfo> results = Lists.newArrayList();
        wCManagerSignInfos.forEach(
            item -> {

                // 排除作廢
                if (item.getStatus() == null || BpmStatus.DELETE.equals(item.getStatus())) {
                    return;
                }
                SingleManagerSignInfo singleManagerSignInfo =
                    new SingleManagerSignInfo(
                        item.getSid(),
                        item.getBpmId(),
                        item.getUser_sid(),
                        item.getDep_sid(),
                        item.getSignType(),
                        item.getStatus(),
                        item.getBpmDefaultSignedName(),
                        null,
                        item.getGroupSeq());

                results.add(singleManagerSignInfo);
            });
        return results;
    }

    /**
     * 取得水管圖 By 需求單位簽核物件
     *
     * @param wcManagerSignInfo 需求單位簽核物件
     * @return
     */
    public List<ProcessTaskBase> settingProcessTaskBase(WCManagerSignInfo wcManagerSignInfo) {
        //        if (wcManagerSignInfo.getTaskTo() == null || wcManagerSignInfo.getTaskTo().getTasks()
        // == null) {
        //            return Lists.newArrayList();
        //        }
        //        return wcManagerSignInfo.getTaskTo().getTasks();

        return bpmManager.findSimulationChart("", wcManagerSignInfo.getBpmId());
    }

    /**
     * 轉換水管圖 By 需求單位簽核物件
     *
     * @param wCManagerSignInfos 需求單位簽核物件List
     * @return
     */
    public List<ProcessTaskBase> settingProcessTaskBase(
        List<WCManagerSignInfo> wCManagerSignInfos) {
        if (wCManagerSignInfos == null) {
            return Lists.newArrayList();
        }
        List<ProcessTaskBase> results = Lists.newArrayList();

        Collections.sort(
            wCManagerSignInfos,
            new Comparator<WCManagerSignInfo>() {
                @Override
                public int compare(WCManagerSignInfo o1, WCManagerSignInfo o2) {
                    return o1.getSeq().compareTo(o2.getSeq());
                }
            });
        wCManagerSignInfos.forEach(
            item -> {
                results.addAll(settingProcessTaskBase(item));
            });
        return results;
    }
}
