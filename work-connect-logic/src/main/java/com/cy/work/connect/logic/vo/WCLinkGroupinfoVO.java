/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * @author brain0925_liao
 */
public class WCLinkGroupinfoVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3710800334376892815L;

    @Getter
    private final String wc_sid;
    @Getter
    private final String theme;
    @Getter
    private final Date update_dt;
    @Getter
    private final String wc_no;
    @Getter
    private final Date create_dt;
    @Getter
    private final String link_group_sid;
    @Setter
    @Getter
    private Date groupCreateDate;
    @Setter
    @Getter
    private Integer createGroupUserSid;

    public WCLinkGroupinfoVO(
        String wc_sid,
        String theme,
        Date update_dt,
        String wc_no,
        Date create_dt,
        String link_group_sid) {
        this.wc_sid = wc_sid;
        this.theme = theme;
        this.update_dt = update_dt;
        this.wc_no = wc_no;
        this.create_dt = create_dt;
        this.link_group_sid = link_group_sid;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.wc_sid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WCLinkGroupinfoVO other = (WCLinkGroupinfoVO) obj;
        return Objects.equals(this.wc_sid,
            other.wc_sid);
    }
}
