/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.manager;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.connect.logic.cache.WCBasePersonAccessInfoCache;
import com.cy.work.connect.repository.WCBasePersonAccessInfoRepository;
import com.cy.work.connect.vo.WCBasePersonAccessInfo;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 基礎單位成員可閱設定
 *
 * @author brain0925_liao
 */
@Component
public class WCBasePersonAccessInfoManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5369037257298941909L;
    /**
     * 基礎單位成員可閱設定Cache
     */
    @Autowired
    private WCBasePersonAccessInfoCache wcBasePersonAccessInfoCache;
    /**
     * WCBasePersonAccessInfoRepository
     */
    @Autowired
    private WCBasePersonAccessInfoRepository wcBasePersonAccessInfoRepository;
    /**
     * WorkSettingUserManager
     */
    @Autowired
    private WkUserCache userManager;

    /**
     * 取得基礎單位成員可閱設定
     *
     * @param userSid 使用者Sid
     * @return
     */
    public WCBasePersonAccessInfo getByUserSid(Integer userSid) {
        return wcBasePersonAccessInfoCache.getByUserSid(userSid);
    }

    /**
     * 取得限制基礎單位成員可閱人員
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getLimitBaseAccessViewUser(Integer userSid) {
        WCBasePersonAccessInfo wcBasePersonAccessInfo = getByUserSid(userSid);
        if (wcBasePersonAccessInfo == null
            || wcBasePersonAccessInfo.getLimitBaseAccessViewPerson() == null
            || wcBasePersonAccessInfo.getLimitBaseAccessViewPerson().getUserTos() == null) {
            return null;
        }
        List<User> limitBaseAccessViewUsers = Lists.newArrayList();
        wcBasePersonAccessInfo
            .getLimitBaseAccessViewPerson()
            .getUserTos()
            .forEach(
                item -> {
                    limitBaseAccessViewUsers.add(userManager.findBySid(item.getSid()));
                });
        return limitBaseAccessViewUsers;
    }

    /**
     * 取得增加基礎單位成員可閱人員
     *
     * @param userSid 使用者Sid
     * @return
     */
    public List<User> getOtherBaseAccessViewUser(Integer userSid) {
        WCBasePersonAccessInfo wcBasePersonAccessInfo = getByUserSid(userSid);
        if (wcBasePersonAccessInfo == null
            || wcBasePersonAccessInfo.getOtherBaseAccessViewPerson() == null
            || wcBasePersonAccessInfo.getOtherBaseAccessViewPerson().getUserTos() == null) {
            return Lists.newArrayList();
        }
        List<User> otherBaseAccessViewUsers = Lists.newArrayList();
        wcBasePersonAccessInfo
            .getOtherBaseAccessViewPerson()
            .getUserTos()
            .forEach(
                item -> {
                    otherBaseAccessViewUsers.add(userManager.findBySid(item.getSid()));
                });
        return otherBaseAccessViewUsers;
    }

    /**
     * 建立基礎單位成員可閱設定
     *
     * @param wcBasePersonAccessInfo 基礎單位成員可閱設定物件
     * @param loginUserSid           登入者Sid
     */
    public void createWCBasePersonAccessInfo(
        WCBasePersonAccessInfo wcBasePersonAccessInfo, Integer loginUserSid) {
        wcBasePersonAccessInfo.setCreate_dt(new Date());
        wcBasePersonAccessInfo.setCreate_usr_sid(loginUserSid);
        wcBasePersonAccessInfo = wcBasePersonAccessInfoRepository.save(wcBasePersonAccessInfo);
        wcBasePersonAccessInfoCache.updateCacheBySid(wcBasePersonAccessInfo);
    }

    /**
     * 更新基礎單位成員可閱設定
     *
     * @param wcBasePersonAccessInfo 基礎單位成員可閱設定物件
     * @param loginUserSid           更新者Sid
     */
    public void updateWCBasePersonAccessInfo(
        WCBasePersonAccessInfo wcBasePersonAccessInfo, Integer loginUserSid) {
        wcBasePersonAccessInfo.setUpdate_dt(new Date());
        wcBasePersonAccessInfo.setUpdate_usr_sid(loginUserSid);
        wcBasePersonAccessInfo = wcBasePersonAccessInfoRepository.save(wcBasePersonAccessInfo);
        wcBasePersonAccessInfoCache.updateCacheBySid(wcBasePersonAccessInfo);
    }

    /**
     * 取得單位成員可閱權限增加(聯集)已設定成員
     *
     * @return
     */
    public List<Integer> getSettingOtherBasePerson() {
        return wcBasePersonAccessInfoCache.getSettingOtherBasePerson();
    }
}
