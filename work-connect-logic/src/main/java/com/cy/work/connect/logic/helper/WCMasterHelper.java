/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.connect.logic.helper;

import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.UserMessageException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.connect.logic.manager.WCAttachmentManager;
import com.cy.work.connect.logic.vo.AttachmentVO;
import com.cy.work.connect.vo.WCMaster;
import com.cy.work.connect.vo.WCTag;
import com.cy.work.connect.vo.enums.WCStatus;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class WCMasterHelper implements Serializable {

    @Autowired
    private WCAttachmentManager wcAttachmentManager;

    /**
     *
     */
    private static final long serialVersionUID = -3433354050927203593L;

    /**
     * 檢測工作聯絡單可被動作的狀態
     *
     * @param wcMaster 工作聯絡單物件
     * @throws UserMessageException
     */
    public void checkWCMasterStatus(WCMaster wcMaster) throws UserMessageException {

        // ====================================
        // 準備檢查狀態
        // ====================================
        Set<WCStatus> rejectionStatus = Sets.newHashSet(
                WCStatus.INVALID,
                WCStatus.CLOSE,
                WCStatus.CLOSE_STOP);

        // ====================================
        // 檢查
        // ====================================
        this.checkWcMasterStatus(wcMaster, rejectionStatus);
    }

    /**
     * 檢測需求流程可簽名、可復原的狀態
     *
     * @param wcMaster   主檔
     * @param isRecovery 為復原
     * @throws UserMessageException 檢核錯誤時拋出
     */
    public void checkReqFlowSignWCMasterStatus(WCMaster wcMaster, boolean isRecovery) throws UserMessageException {

        // ====================================
        // 準備檢查狀態
        // ====================================
        Set<WCStatus> rejectionStatus = Sets.newHashSet(
                WCStatus.INVALID,
                WCStatus.EXEC,
                WCStatus.CLOSE,
                WCStatus.CLOSE_STOP);

        // 為簽名時，加上核准狀態
        if (!isRecovery) {
            rejectionStatus.add(WCStatus.APPROVED);
        }

        // ====================================
        // 檢查
        // ====================================
        this.checkWcMasterStatus(wcMaster, rejectionStatus);

    }

    /**
     * 檢查主檔狀態
     *
     * @param wcMaster        主檔
     * @param rejectionStatus 拒絕的主檔狀態
     * @throws UserMessageException 檢核錯誤時拋出
     */
    private void checkWcMasterStatus(WCMaster wcMaster, Set<WCStatus> rejectionStatus) throws UserMessageException {
        // ====================================
        // 防呆，應該不會發生
        // ====================================
        if (wcMaster == null || wcMaster.getWc_status() == null) {
            String errorMessage = WkMessage.NEED_RELOAD + "(主檔資料不正確!)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.ERROR);
        }
        // ====================================
        // 檢查單據狀態
        // ====================================
        if (rejectionStatus.contains(wcMaster.getWc_status())) {
            String errorMessage = WkMessage.NEED_RELOAD + "(單號：" + wcMaster.getWc_no() + "，已【" + wcMaster.getWc_status().getVal() + "】)";
            log.error(errorMessage);
            throw new UserMessageException(errorMessage, InfomationLevel.WARN);
        }
    }

    /**
     * @param wcTags 小類設定檔資料
     * @return
     */
    private boolean isNeedCheckUpdateFile(List<WCTag> wcTags) {

        // 查詢
        // boolean isNeedUpdateFile = this.wcTagManager.isNeedUpdateFile(wcMaster.getCategoryTagTo().getTagSids());

        // 檢查小類中，是否有設定為需要上傳檔案者
        Optional<WCTag> optional = wcTags.stream()
                .filter(wcTag -> wcTag.getUploadFile() == true)
                .findAny();

        return optional.isPresent();

    }

    /**
     * 判斷包含需要上傳的小類時，未上傳附件
     *
     * @param wcSid  聯絡單 sid
     * @param wcTags 小類設定檔資料
     * @throws UserMessageException 無附加檔案時拋出
     */
    public void checkNeedUpdateFile(String wcSid, List<WCTag> wcTags) throws UserMessageException {
        // ====================================
        // 確認單據中的所有小類，是否有任何一個需要上傳檔案
        // ====================================
        if (!this.isNeedCheckUpdateFile(wcTags)) {
            return;
        }

        // ====================================
        // 判斷是否已上傳檔案
        // ====================================
        // 查詢已上傳的檔案數量
        Integer fileCount = this.wcAttachmentManager.countByWcSid(wcSid);

        if (fileCount == 0) {
            throw new UserMessageException("需上傳附加檔案", InfomationLevel.WARN);
        }
    }

    /**
     * 判斷包含需要上傳的小類時，未上傳附件
     *
     * @param wcTags        小類設定檔資料
     * @param attachmentVOs 附加檔案
     * @throws UserMessageException
     */
    public void checkNeedUpdateFile(List<WCTag> wcTags, List<AttachmentVO> attachmentVOs) throws UserMessageException {
        // ====================================
        // 確認單據中的所有小類，是否有任何一個需要上傳檔案
        // ====================================
        if (!this.isNeedCheckUpdateFile(wcTags)) {
            return;
        }

        // ====================================
        // 判斷是否已上傳檔案
        // ====================================
        if (WkStringUtils.isEmpty(attachmentVOs)) {
            throw new UserMessageException("需上傳附加檔案", InfomationLevel.WARN);
        }
    }

    /**
     * 判斷模糊字串是否為單號
     *
     * @param compId
     * @param fuzzyText
     * @return
     */
    public Boolean isWcNo(String compId, String fuzzyText) {

        fuzzyText = WkStringUtils.safeTrim(fuzzyText);

        if (fuzzyText.length() != 15) {
            return Boolean.FALSE;
        }

        if (!fuzzyText.startsWith(compId + "WC")) {
            return Boolean.FALSE;
        }

        if (!WkStringUtils.isNumber(fuzzyText.substring(4, 15))) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
