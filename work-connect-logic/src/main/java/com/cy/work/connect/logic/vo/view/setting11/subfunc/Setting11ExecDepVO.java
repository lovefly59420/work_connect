package com.cy.work.connect.logic.vo.view.setting11.subfunc;

import com.cy.commons.vo.User;
import com.cy.work.connect.repository.vo.Setting11BaseDtVO;
import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

/**
 * @author jimmy_chou
 */
public class Setting11ExecDepVO extends Setting11BaseDtVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1070457104974713680L;

    /**
     * 領單人員
     */
    @Getter
    @Setter
    List<User> receviceUsers;

    /**
     * 領單人員(顯示)
     */
    @Getter
    @Setter
    String receviceUsersShow;

    /**
     * 需求方簽核
     */
    @Getter
    @Setter
    String reqSignInfo;

    /**
     * 執行模式
     */
    @Getter
    @Setter
    String execMode;
    
    @Getter
    @Setter
    boolean selectAble = true;

    /**
     * @param baseVO
     */
    public Setting11ExecDepVO(Setting11BaseDtVO baseVO) {
        BeanUtils.copyProperties(baseVO, this);
    }
}
